<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" type="image/x-icon" href="https://gementerprise.uk/images/logos/favicon-org.gif" />
<link rel="canonical" href="https://gementerprise.uk/" />
<meta name="description" content="GEM Enterprise is one of the leading construction companies in UK. We can help your next residential or commercial project. We offer all aspects of planning, feasibility, design and supervision within Structural & Civil Engineering.">
<meta name="keywords" content="GEM Enterprise,Planning Applications, planning permission, architectural, Interior Design, Structural Engineering, Structural Engineer, Civil Engineering, Civil Engineer, Project Management, Construction Company London, Construction Company,GEM Enterprise building company, London building companies, London construction companies">
<meta property="og:locale" content="en_GB" />
<meta property="og:title" content="GEM Enterprise" />
<meta property="og:description" content="GEM Enterprise is one of the leading construction companies in UK. We can help your next residential or commercial project. We offer all aspects of planning, feasibility, design and supervision within Structural & Civil Engineering." />
<meta property="og:url" content="https://gementerprise.uk/" />
<meta name="twitter:description" content="GEM Enterprise is one of the leading construction companies in UK. We can help your next residential or commercial project. We offer all aspects of planning, feasibility, design and supervision within Structural & Civil Engineering." />
<meta name="twitter:title" content="GEM Enterprise" />
<meta property="og:site_name" content="GEM Enterprise" />
<meta name="msvalidate.01" content="BA6870789A290138AF733935907BCDB8" />
<title>GEM Enterprise</title>
<link href="/css/all.css" rel="stylesheet">
<link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Crimson+Text|Merriweather|Montserrat|Nanum+Gothic|Poppins|Quicksand|Raleway|Oswald|Roboto|Open+Sans|Source+Sans+Pro" rel="stylesheet">
<link href="/css/main.css" rel="stylesheet">
<link href="/css/index.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" defer>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="/js/all.js" type="eda324bfff62388b65ec5a29-text/javascript"></script>
<script src="/js/eu.js" type="eda324bfff62388b65ec5a29-text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67746060-1" type="eda324bfff62388b65ec5a29-text/javascript"></script>
<script type="eda324bfff62388b65ec5a29-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67746060-1');
</script>
</head>
<body>
<script type="eda324bfff62388b65ec5a29-text/javascript">
    if (window.matchMedia("(max-width: 1000px)").matches){}
    else {$("body").niceScroll({cursorcolor:"#596372", zindex: "10000",cursorwidth: "10px",background: "#1b2330",cursorborder: "1px solid #1b2330",cursorborderradius: "5px"});}
    </script>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
<div class="container">
<div id="mhainvest" class="mhainvest-default">
<a class="navbar-brand" href="/">
<img id="gemlogo" src="https://gementerprise.uk/images/logos/logo.png">
</a>
</div>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
Menu
<i class="fas fa-bars"></i>
</button>
<div class="collapse navbar-collapse navbar-initial" id="navbarResponsive">
<ul class="navbar-nav ml-auto">
<li class="nav-item">
<a class="nav-link activemenu" href="https://gementerprise.uk"><i class="fa fa-home"></i> Home</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-angle-double-down"></i> Services</a>
<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
<a class="dropdown-item" href="https://gementerprise.uk/structural-engineering">Structural Engineering</a>
<a class="dropdown-item" href="https://gementerprise.uk/architectural">Architectural</a>
<a class="dropdown-item" href="https://gementerprise.uk/interior-design">Interior Design</a>
<a class="dropdown-item" href="https://gementerprise.uk/project-managers">Project Management</a>
<a class="dropdown-item" href="https://gementerprise.uk/civil-engineering">Civil Engineering</a>
<a class="dropdown-item" href="https://gementerprise.uk/quantity-surveyors">Quantity Surveyors</a>
<a class="dropdown-item" href="https://gementerprise.uk/building-services">Building Services</a>
<a class="dropdown-item" href="https://gementerprise.uk/sustainable-development">Sustainable Development</a>
<a class="dropdown-item" href="https://gementerprise.uk/planning-applications">Planning Applications</a>
</div>
</li>
<li class="nav-item">
<a class="nav-link " href="https://gementerprise.uk/bim"><i class="fas fa-object-group"></i> BIM</a>
</li>
<li class="nav-item">
<a class="nav-link " href="https://gementerprise.uk/contact"><i class="fa fa-phone"></i> Contact</a>
</li>
</ul>
</div>
</div>
</nav>
<header class="masthead">
<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
<div class="carousel-inner">
<div class="carousel-item active">
<img class="d-block w-100 imgSlider img-fluid" src="https://gementerprise.uk/images/civil/architects-designing2-0.jpg" alt="First slide">
<div class="carousel-caption">
<span class="animated slideInUp first-span">Think Forward. Rise Above.</span>
<h1 class="animated slideInDown deepshadow investment-quote">You'll Love</h1>
<h1 class="animated slideInDown deepshadow">The Way We Work</h1>
<span class="animated slideInUp">Delivering Professional, Quality, Service combined with unbeatable value for our clients</span>
<p class="animated fadeInUp" style="text-align: center;"><a href="https://gementerprise.uk/contact" class="btn white-btn green-btn">PARTNER WITH US</a><a href="https://gementerprise.uk/contact" class="btn white-btn">CONTACT US</a></p>
</div>
</div>
<div class="carousel-item">
<img class="d-block w-100 imgSlider img-fluid" src="https://gementerprise.uk/images/structural-engineering/structural-engineering1.jpg" alt="Second slide">
<div class="carousel-caption">
<h1 class="animated fadeInLeft deepshadow">Structural Engineering</h1>
<p class="animated fadeInRight" style="text-align: center;">Advanced design solutions which balance practicality with innovation</p>
<p class="animated fadeInUp p-button" style="text-align: center;"><a href="https://gementerprise.uk/structural-engineering" class="btn white-btn green-btn">MORE DETAILS</a></p>
</div>
</div>
<div class="carousel-item">
<img class="d-block w-100 imgSlider img-fluid" src="https://gementerprise.uk/images/architectural/architectural3.jpg" alt="Second slide">
<div class="carousel-caption">
<h1 class="animated fadeInRight deepshadow">Architectural</h1>
 <p class="animated fadeInLeft" style="text-align: center;">We make your dream come true</p>
<p class="animated fadeInUp p-button" style="text-align: center;"><a href="https://gementerprise.uk/architectural" class="btn white-btn green-btn">MORE DETAILS</a></p>
</div>
</div>
<div class="carousel-item">
<img class="d-block w-100 imgSlider img-fluid" src="https://gementerprise.uk/images/interior/interior3.jpg" alt="Second slide">
<div class="carousel-caption">
<h1 class="animated fadeInLeft deepshadow">Interior Design</h1>
<p class="animated fadeInRight" style="text-align: center;">Transform any property into a beautiful space</p>
<p class="animated fadeInUp p-button" style="text-align: center;"><a href="https://gementerprise.uk/interior-design" class="btn white-btn green-btn">MORE DETAILS</a></p>
</div>
</div>
</div>
<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
<div class="arrow bounce"></div>
</div>
</header>

<div class="maincontainer">
<div id="preloader">
<div id="status">
<div class="status-mes"></div>
</div>
</div>
<section class="aboutus">
<div class="container">
<div class="row">
<div class="cms-custom-heading-wrap  heading  vc_custom_heading aos-init" data-aos="fade-up"><h3 style="text-align: center" class="cmsc-custom-heading vc_custom_heading"><span class="head">WHO </span> <span class="final">WE ARE</span></h3></div>
</div>
<div class="row flex-column-reverse flex-lg-row">
<div class="col-sm-6">
<img src="https://gementerprise.uk/images/ourstory/architect.jpg" class="story-image" data-aos="flip-right">
</div>
<div class="col-sm-6 ">
<div class="p-aboutus" data-aos="flip-left"><div class="text-justify about-column"> <p class="rtejustify" style="text-align: justify; line-height: 150%;">We&rsquo;re driven by fresh ideas and a clearly defined process. We cultivate collaboration and innovation through a positive and inclusive work culture, a mutual journey of discovery combined with an integrated process of delivery.</p>
<p class="rtejustify" style="text-align: justify; line-height: 150%;">Our aim has always been to create places and spaces that are unique and make a positive contribution to the fabric of society.</p>
<p><label>&nbsp;</label></p></div>
</div>
</div>
</div>
<br><br>
<div class="row">
<div class="col-sm-6">
<div class="p-aboutus" data-aos="flip-right">
<div class="text-justify">
<p>Everything we do is uphold by our core values of responsibility, dynamism and teamwork.
We are responsible and deliver on our promises, we are dynamic and bring energy to our projects and we work as a team to achieve shared goals. This means our projects gets finished on time, on budget and to the highest standards possible.
</p>
</div>
</div>
</div>
<div class="col-sm-6">
<img src="https://gementerprise.uk/images/civil/team-0.jpg" class="story-image" data-aos="flip-left">
</div>
</div>
</div>
</section>
<section class="secondarybg bg-light">
<div class="container-fluid">
<div class="cms-custom-heading-wrap  heading  vc_custom_heading aos-init" data-aos="fade-up">
<h3 style="text-align: center" class="cmsc-custom-heading vc_custom_heading">
<span class="head">WHAT </span> <span class="final">WE DO?</span></h3>
</div>
<div class="row whatwedo whatWeDo">
<div class="col-md-3">
<a href="https://gementerprise.uk/architectural">
<div class="card mb-3 aos-init" data-aos="zoom-in-up">
<div class="card-body mt-4 mb-1 text-center"><i class="pb-3 text-primary fas fa-hotel fa-3x middlei"></i>
<div class="h4 pb-3">Architectural</div>
<p style="text-align: center;">Site appraisals, design brief development, concept proposals, planning submissions, construction drawing packages, tendering and contract administration.
</p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="https://gementerprise.uk/planning-applications">
<div class="card mb-3 aos-init" data-aos="zoom-in-up">
<div class="card-body mt-4 mb-1 text-center"><i class="pb-3 text-primary fas fa-clipboard-list fa-3x middlei"></i>
<div class="h4 pb-3">Planning Permission</div>
<p style="text-align: center;">Planning applications, building regulations, party wall agreements, quantity surveying and project management.
</p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="https://gementerprise.uk/structural-engineering">
<div class="card mb-3 aos-init" data-aos="flip-left">
<div class="card-body mt-4 mb-1 text-center">
<i class="pb-3 text-primary fa fa-building fa-3x"></i>
<div class="h4 pb-3">Structural Design</div>
<p style="text-align: center;">Building design of new and existing residential, industrial and commercial.
</p>
</div>
</div>
</a>
</div>
<div class="col-md-3">
<a href="https://gementerprise.uk/architectural-and-interior-design">
<div class="card mb-3 aos-init" data-aos="flip-right">
<div class="card-body mt-4 mb-1 text-center">
<i class="pb-3 text-primary fas fa-home fa-3x"></i>
<div class="h4 pb-3">Interior Design</div>
<p style="text-align: center;">Transform any property new build or renovation into an award winning design for any taste.
</p>
</div>
</div>
</a>
</div>
</div>
</div>
<br><br><br>
<div class="container">
<div class="row flex-column-reverse flex-lg-row">
<div class="col-sm-6">
<img src="https://gementerprise.uk/images/ourstory/interior_house.jpg" class="story-image" data-aos="flip-right">
<img src="https://gementerprise.uk/images/civil/handshake2-0.jpg" class="story-image cn-image" data-aos="flip-left">
</div>
<div class="col-sm-6">
<h2 class="" data-aos="flip-left">PROFESSIONAL MANAGEMENT</h2>
<div class="p-aboutus aos-init" data-aos="flip-left">
<div class="text-justify">
<p>We have built and maintained a solid reputation for construction and project management excellence across world</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="figures">
<div class="cms-custom-heading-wrap  heading  vc_custom_heading aos-init" data-aos="fade-up"><h3 style="text-align: center" class="cmsc-custom-heading vc_custom_heading"><span class="head">WORK</span> <span class="final">IN FIGURES</span></h3></div>
<br><br>
<div class="container">
<div class="row text-center">
<div class="col-sm-4">
<div class="row aos-init" data-aos="fade-up">
<div class="col-sm-12"><i class="fas fa-users work-figures"></i></div>
<div class="col-sm-12"><div class="work-counter">15+</div></div>
<div class="col-sm-12"><div class="work-title">Experienced Professional </div></div>
</div>
</div>
<div class="col-sm-4">
<div class="row aos-init" data-aos="fade-up">
<div class="col-sm-12"><i class="fas fa-book work-figures"></i></div>
<div class="col-sm-12"><div class="work-counter">20+</div></div>
<div class="col-sm-12"><div class="work-title">Years Of Experience</div></div>
</div>
</div>
<div class="col-sm-4">
<div class="row aos-init" data-aos="fade-up">
<div class="col-sm-12"><i class="fas fa-headset work-figures"></i></div>
<div class="col-sm-12"><div class="work-counter">500+</div></div>
<div class="col-sm-12"><div class="work-title">Satisfied Customers</div></div>
</div>
</div>
</div>
</div>
</section>
</div>

<footer>
<div class="top">
<div class="container  text-center text-light">
<div class="top-top">
<div class="row">
<div class="col-md-4 footer-block footer-first">
<img id="gemlogo" class="mainlogo-footer" src="https://gementerprise.uk/images/logos/logo.png"><br><br>
<span>We Are Your Ultimate Business Partner</span>
</div>
<div class="col-md-4 footer-block">
<div class="footer-widget widget_text">
<h3>Get Connected</h3>
<ul class="list-inline social-links">
<li><a href="https://www.facebook.com/GEMEnterprise" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
<li><a href="https://www.instagram.com/GEM.Enterprise" target="_blank"><i class="fa fa-instagram"></i></a></li>
<li><a href="https://twitter.com/GEM_Enterprise" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="https://www.linkedin.com/company/gem-enterprise" target="_blank"><i class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-4 footer-block last">
<div class="footer-widget widget_text">
<address>
<i class="fa fa-envelope-o"></i>&nbsp; <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="fb92959d94bb9c9e969e958f9e898b8992889ed58e90">[email&#160;protected]</a><br>
<i class="fa fa-phone"></i>&nbsp; 0345 338 7002<br>
<i class="fa fa-phone"></i>&nbsp; 0845 338 7002<br>
<i class="fa fa-phone"></i>&nbsp; 020 3538 7002<br><br>
<i class="fa fa-location-arrow"></i>&nbsp; GEM Enterprise<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 107 Cheapside<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; London<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; EC2V 6DN
</address>
</div>
</div>
</div>
</div>
<div class="top-bottom">
<div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-2 rule-column text-right"><a href="https://gementerprise.uk/about" class="rules-link">About us</a></div>
<div class="col-sm-2 rule-column"><a href="#" class="rules-link"></a>
</div>
<div class="col-sm-2 rule-column text-left"><a href="https://gementerprise.uk/privacy" class="rules-link">Privacy Policy</a></div>
<div class="col-sm-3"></div>
</div>
</div>
</div>
</div>
<div class="bottom">
<div class="container">
<div class="col-lg-12 text-center">
<span id="copyrightmir"> &copy; GEM Enterprise 2019 - All rights reserved </span>
</div>
</div>
</div>
</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="eda324bfff62388b65ec5a29-text/javascript">
  AOS.init();
</script>
<script src="/js/main.js?t=1575989648" type="eda324bfff62388b65ec5a29-text/javascript"></script>

<script type="eda324bfff62388b65ec5a29-text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cf6b556267b2e578530e154/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>


<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="eda324bfff62388b65ec5a29-|49" defer=""></script></body>
</html>