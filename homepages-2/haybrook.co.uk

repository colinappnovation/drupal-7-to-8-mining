<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:og="http://ogp.me/ns#"
  xmlns:article="http://ogp.me/ns/article#"
  xmlns:book="http://ogp.me/ns/book#"
  xmlns:profile="http://ogp.me/ns/profile#"
  xmlns:video="http://ogp.me/ns/video#"
  xmlns:product="http://ogp.me/ns/product#"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
  xmlns:schema="http://schema.org/">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.haybrook.co.uk/sites/default/files/favicon.gif" type="image/gif" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1, user-scalable=yes" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.haybrook.co.uk/" />
<link rel="shortlink" href="https://www.haybrook.co.uk/" />
<meta property="og:site_name" content="Haybrook" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.haybrook.co.uk/" />
<meta property="og:title" content="Haybrook" />
<meta property="og:description" content="The leading IT recruitment specialists for Oxfordshire and the surrounding counties" />
  <title>Haybrook | The leading IT recruitment specialists for Oxfordshire and the surrounding counties</title>  
  <style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/modules/system/system.base.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/system/system.menus.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/system/system.messages.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/system/system.theme.css?pmjwpn");
</style>
<style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/sites/all/modules/collapsiblock/collapsiblock.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/field/theme/field.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/node/node.css?pmjwpn");
@import url("https://www.haybrook.co.uk/modules/user/user.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/modules/views/css/views.css?pmjwpn");
</style>
<style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/sites/all/modules/ctools/css/ctools.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/modules/nice_menus/css/nice_menus.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/modules/nice_menus/css/nice_menus_default.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/modules/eu_cookie_compliance/css/eu_cookie_compliance.css?pmjwpn");
</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#0779BF;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,.eu-cookie-compliance-more-button,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#ffffff !important;}.eu-cookie-withdraw-tab{border-color:#ffffff;}

/*]]>*/-->
</style>
<style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/alpha-reset.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/alpha-mobile.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/alpha-alpha.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/formalize.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/omega-text.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/omega-branding.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/omega-menu.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/omega-forms.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/omega/css/omega-visuals.css?pmjwpn");
</style>

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.haybrook.co.uk/sites/all/themes/haybrook/css/ie-lte-9.css?pmjwpn" media="all" />
<![endif]-->

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.haybrook.co.uk/sites/all/themes/haybrook/css/ie-lte-7.css?pmjwpn" media="all" />
<![endif]-->
<style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/global.css?pmjwpn");
</style>

<!--[if (lt IE 9)&(!IEMobile)]>
<style type="text/css" media="all">
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default-normal.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/normal/alpha-default-normal-12.css?pmjwpn");
</style>
<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)">
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default-narrow.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/narrow/alpha-default-narrow-12.css?pmjwpn");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)">
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default-normal.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/normal/alpha-default-normal-12.css?pmjwpn");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 1220px)">
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/haybrook/css/haybrook-alpha-default-wide.css?pmjwpn");
@import url("https://www.haybrook.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/wide/alpha-default-wide-12.css?pmjwpn");
</style>
<!--<![endif]-->
  <script type="text/javascript" src="https://www.haybrook.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/misc/drupal.js?pmjwpn"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/eu_cookie_compliance/js/jquery.cookie-1.4.1.min.js?v=1.4.1"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/nice_menus/js/jquery.bgiframe.js?v=2.1"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/nice_menus/js/jquery.hoverIntent.js?v=0.5"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/nice_menus/js/superfish.js?v=1.4.8"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/nice_menus/js/nice_menus.js?v=1.0"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/collapsiblock/collapsiblock.js?pmjwpn"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/google_analytics/googleanalytics.js?pmjwpn"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-52087678-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/themes/haybrook/js/haybrook.js?pmjwpn"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/themes/omega/omega/js/jquery.formalize.js?pmjwpn"></script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/themes/omega/omega/js/omega-mediaqueries.js?pmjwpn"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"haybrook","theme_token":"iDks_36-nfnKyonhJ-PQHUU1GP3l0LDEeZaRJlLPNyg","js":{"0":1,"1":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.bgiframe.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.hoverIntent.js":1,"sites\/all\/modules\/nice_menus\/js\/superfish.js":1,"sites\/all\/modules\/nice_menus\/js\/nice_menus.js":1,"sites\/all\/modules\/collapsiblock\/collapsiblock.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"2":1,"sites\/all\/themes\/haybrook\/js\/haybrook.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/collapsiblock\/collapsiblock.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus_default.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/all\/themes\/haybrook\/css\/ie-lte-9.css":1,"sites\/all\/themes\/haybrook\/css\/ie-lte-7.css":1,"sites\/all\/themes\/haybrook\/css\/global.css":1,"ie::normal::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default.css":1,"ie::normal::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default.css":1,"narrow::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default-narrow.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default.css":1,"normal::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default-normal.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default.css":1,"wide::sites\/all\/themes\/haybrook\/css\/haybrook-alpha-default-wide.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"collapsiblock":{"blocks":{"block-views-job-details-block-5":"3","block-block-3":"1","block-block-16":"3","block-nice-menus-2":"3","block-menu-menu-mobile-menu":"1","block-block-14":"1","block-cck-blocks-field-sidebar-quote":"1","block-cck-blocks-field-sidebar-image":"1","block-views-job-details-block-6":"1","block-block-4":"1","block-views-job-details-block-7":"1","block-views-job-details-block-8":"1","block-block-8":"1","block-block-15":"1","block-block-11":"1","block-menu-block-1":"1","block-block-6":"1"},"default_state":"1","slide_type":"1","slide_speed":"200","block_title":":header:first","block":".block","block_content":"div.content"},"nice_menus_options":{"delay":"200","speed":"normal"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":1,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-and-cookies","popup_link_new_window":1,"popup_position":null,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","popup_eu_only_js":0,"cookie_lifetime":100,"cookie_session":false,"disagree_do_not_show_popup":0,"method":"default","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>
</script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in page-node page-node- page-node-43 node-type-homepage">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
  <div class="region region-page-top" id="region-page-top">
  <div class="region-inner region-page-top-inner">
      </div>
</div>  <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-user-wrapper" class="zone-wrapper zone-user-wrapper clearfix">  
  <div id="zone-user" class="zone zone-user clearfix container-12">
    <div class="grid-4 region region-user-second" id="region-user-second">
  <div class="region-inner region-user-second-inner">
    <div class="block block-block block-7 block-block-7 odd block-without-title" id="block-block-7">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="/register-vacancy">Register a vacancy</a></p>
    </div>
  </div>
</div><div class="block block-block block-8 block-block-8 even block-without-title" id="block-block-8">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="/current-vacancies">Apply for a job</a></p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix container-12">
    <div class="grid-12 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="branding-data clearfix">
            <div class="logo-img">
        <a href="/" rel="home" title="" class="active"><img src="https://www.haybrook.co.uk/sites/default/files/haybrook_logo_new.png" alt="" id="logo" /></a>      </div>
                        <hgroup class="site-name-slogan">        
                                <h6 class="site-slogan">The leading IT recruitment specialists for Oxfordshire and the surrounding counties</h6>
              </hgroup>
          </div>
        <section class="block block-nice-menus block-2 block-nice-menus-2 odd" id="block-nice-menus-2">
  <div class="block-inner clearfix">
              <h2 class="block-title">Mobile menu</h2>
            
    <div class="content clearfix">
      <ul class="nice-menu nice-menu-right nice-menu-menu-mobile-menu" id="nice-menu-2"><li class="menu-687 menu-path-node-44 first odd "><a href="/candidates" title="">For candidates</a></li>
<li class="menu-688 menu-path-node-54  even "><a href="/employers" title="">For employers</a></li>
<li class="menu-689 menu-path-node-48  odd "><a href="/about-haybrook-it" title="">About us</a></li>
<li class="menu-690 menu-path-node-49  even last"><a href="/contact" title="">Contact</a></li>
</ul>
    </div>
  </div>
</section>  </div>
</div>  </div>
</div><div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">  
  <div id="zone-menu" class="zone zone-menu clearfix container-12">
    <div class="grid-12 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
        <div class="block block-nice-menus block-1 block-nice-menus-1 odd block-without-title" id="block-nice-menus-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="nice-menu nice-menu-down nice-menu-main-menu" id="nice-menu-1"><li class="menu-674 menuparent  menu-path-nolink first odd "><span title="" class="nolink">For candidates</span><ul><li class="menu-993 menu-path-node-5314 first odd "><a href="/current-vacancies" title="">Current vacancies</a></li>
<li class="menu-673 menu-path-node-45  even "><a href="/register-your-cv">Register your CV</a></li>
<li class="menu-679 menu-path-node-51  odd "><a href="/candidate-advice">Candidate advice</a></li>
<li class="menu-615 menu-path-node-30  even "><a href="/services-candidates">Services</a></li>
<li class="menu-680 menu-path-node-52  odd "><a href="/why-haybrook-0">Why Haybrook</a></li>
<li class="menu-655 menu-path-node-31  even "><a href="/refer-friend-scheme">Refer a friend scheme</a></li>
<li class="menu-678 menu-path-node-50  odd last"><a href="/useful-links">Useful links</a></li>
</ul></li>
<li class="menu-547 menuparent  menu-path-nolink  even "><span title="" class="nolink">For employers</span><ul><li class="menu-994 menu-path-node-5314 first odd "><a href="/current-vacancies" title="">Current vacancies</a></li>
<li class="menu-616 menu-path-node-27  even "><a href="/register-vacancy">Register a vacancy</a></li>
<li class="menu-617 menu-path-node-32  odd "><a href="/services-employers">Services</a></li>
<li class="menu-618 menu-path-node-33  even "><a href="/haybrook-difference">The Haybrook difference</a></li>
<li class="menu-682 menu-path-node-50  odd last"><a href="/useful-links" title="">Useful links</a></li>
</ul></li>
<li class="menu-992 menu-path-node-5314  odd "><a href="/current-vacancies">Current vacancies</a></li>
<li class="menu-675 menuparent  menu-path-nolink  even "><span title="" class="nolink">About us</span><ul><li class="menu-621 menu-path-node-37 first odd "><a href="/mission-statement-and-values">Mission Statement and Values</a></li>
<li class="menu-633 menu-path-node-35  even "><a href="/careers-haybrook">Careers at Haybrook</a></li>
<li class="menu-620 menu-path-node-36  odd "><a href="/about-haybrook-services">Services</a></li>
<li class="menu-683 menu-path-node-49  even last"><a href="/contact">Contact us</a></li>
</ul></li>
<li class="menu-676 menuparent  menu-path-nolink  odd last"><span title="" class="nolink">Contact</span><ul><li class="menu-685 menu-path-node-49 first odd "><a href="/contact" title="">Contact details</a></li>
<li class="menu-639 menu-path-node-42  even last"><a href="/address-and-map">Address and map</a></li>
</ul></li>
</ul>
    </div>
  </div>
</div>  </div>
</div>
  </div>
</div><div id="zone-header-wrapper" class="zone-wrapper zone-header-wrapper clearfix">  
  <div id="zone-header" class="zone zone-header clearfix container-12">
    <div class="grid-6 region region-header-first" id="region-header-first">
  <div class="region-inner region-header-first-inner">
      </div>
</div>  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-12">    
        
        <div class="grid-9 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                <h1 class="title" id="page-title">Welcome to Haybrook IT Resourcing</h1>
                        <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <article about="/welcome-haybrook-it-resourcing" typeof="schema:WebPage sioc:Item foaf:Document" class="node node-homepage node-published node-not-promoted node-not-sticky author-ner odd clearfix" id="node-homepage-43">
        <span property="dc:title" content="Welcome to Haybrook IT Resourcing" class="rdf-meta element-hidden"></span>    
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Haybrook IT Resourcing is the leading IT recruitment specialist for Oxfordshire and the wider south of England region. Celebrating our 31st anniversary in 2019, we’ve spent many years building up our excellent reputation and we’re proud to have successfully helped thousands of IT professionals into new careers.</p>
<p>We offer jobseekers access to a huge range of jobs with some of the country’s leading employers, many of whom are exclusive to us. We provide free support and guidance throughout the recruitment process and beyond, to candidates of all ages and experience levels.</p>
<p>Among employers, we’re known for our deep knowledge of the IT sector, and for our commitment to maintaining the highest level of quality. We make use of innovative candidate sourcing methods, which enable us to find the best candidates for all levels of IT jobs - from support roles to complex technical positions.</p>
</div></div></div><div class="field field-name-field-homepage-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.haybrook.co.uk/sites/default/files/uploads/homepeople.jpg" width="457" height="270" alt="" /></div></div></div><span property="dc:title" content="Welcome to Haybrook IT Resourcing" class="rdf-meta element-hidden"></span>  </div>
  
  <div class="clearfix">
          <nav class="links node-links clearfix"></nav>
    
      </div>
</article>    </div>
  </div>
</div>      </div>
</div><aside class="grid-3 region region-sidebar-second" id="region-sidebar-second">
  <div class="region-inner region-sidebar-second-inner">
    <section class="block block-views block-latest-vacancies-block block-views-latest-vacancies-block odd" id="block-views-latest-vacancies-block">
  <div class="block-inner clearfix">
              <h2 class="block-title">Latest Vacancies</h2>
            
    <div class="content clearfix">
      <div class="view view-latest-vacancies view-id-latest_vacancies view-display-id-block view-dom-id-80deec0d410a67edc5f51f604d7a84c7">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <span class="views-field views-field-title">        <span class="field-content"><a href="/a21462-b">Systems Administrator, Infrastructure, VMWare, CentOS, Linux, Exchange - A2/1462 B</a></span>  </span>      ,  
  <span class="views-field views-field-field-location">        <span class="field-content">Wallingford, Oxfordshire</span>  </span>      ,  
  <span class="views-field views-field-field-salary">        <span class="field-content">Up to £45,00 + Benefits</span>  </span>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <span class="views-field views-field-title">        <span class="field-content"><a href="/n11152-c">Senior Django Developer – Python – Open-source, NonProfit – N1/1152 C</a></span>  </span>      ,  
  <span class="views-field views-field-field-location">        <span class="field-content">Cambridge</span>  </span>      ,  
  <span class="views-field views-field-field-salary">        <span class="field-content">£50,000 - £65,000 - Performance + company bonuses, Ethical pension fund</span>  </span>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <span class="views-field views-field-title">        <span class="field-content"><a href="/n11152-b">Senior Django Developer – Python – Open-source, NonProfit – N1/1152 B</a></span>  </span>      ,  
  <span class="views-field views-field-field-location">        <span class="field-content">Bristol</span>  </span>      ,  
  <span class="views-field views-field-field-salary">        <span class="field-content">£50,000 - £65,000 - Performance + company bonuses, Ethical pension fund</span>  </span>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <span class="views-field views-field-title">        <span class="field-content"><a href="/n11152">Senior Django Developer – Python – Open-source, NonProfit – N1/1152 a</a></span>  </span>      ,  
  <span class="views-field views-field-field-location">        <span class="field-content">North Oxfordshire</span>  </span>      ,  
  <span class="views-field views-field-field-salary">        <span class="field-content">£50,000 - £65,000 - Performance + company bonuses, Ethical pension fund</span>  </span>  </div>
    </div>
  
  
  
  
      <div class="view-footer">
      <p><a href="/current-vacancies">Search our vacancies»</a></p>
    </div>
  
  
</div>    </div>
  </div>
</section><div class="block block-block block-13 block-block-13 even block-without-title" id="block-block-13">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="/register-vacancy">Register a vacancy</a></p>
    </div>
  </div>
</div><div class="block block-block block-15 block-block-15 odd block-without-title" id="block-block-15">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="/current-vacancies">Apply for a job</a></p>
    </div>
  </div>
</div>  </div>
</aside>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-3 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <div class="block block-block block-3 block-block-3 odd block-without-title" id="block-block-3">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <a class="twitter-timeline" data-dnt=true href="https://twitter.com/HaybrookIT" data-widget-id="288633891466649600">Tweets by @HaybrookIT</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
  </div>
</div>  </div>
</div><div class="grid-3 region region-footer-second" id="region-footer-second">
  <div class="region-inner region-footer-second-inner">
    <div class="block block-block block-4 block-block-4 odd block-without-title" id="block-block-4">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="/contact">contact</a></p>
<p><a href="/privacy-and-cookies">cookie policy</a></p>
<p><a href="/sites/default/files/Privacy-Policy-and-Candidate-Terms-of-Use.pdf" target="_blank">privacy policy and candidate terms of use</a></p>
<p><a href="http://www.versantus.co.uk/" title="Web design and Drupal development by  Versantus">website by versantus</a></p>
<p><a href="/user/login">login</a></p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-3 region region-footer-third" id="region-footer-third">
  <div class="region-inner region-footer-third-inner">
    <div class="block block-block block-5 block-block-5 odd block-without-title" id="block-block-5">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><strong>Haybrook IT Resourcing Ltd</strong> </p>
<p>The Long Barn, </p>
<p>Little Baldon, </p>
<p>Oxford, </p>
<p>Oxfordshire. </p>
<p>OX44 9PU</p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-3 region region-footer-fourth" id="region-footer-fourth">
  <div class="region-inner region-footer-fourth-inner">
    <div class="block block-block block-6 block-block-6 odd block-without-title" id="block-block-6">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><strong>Telephone:</strong> +44 (0)1865 343 112</p>
<p><strong style="font-size: 13.008px;">Email:</strong><span style="font-size: 13.008px;"> <a href="mailto:admin@haybrook.co.uk">admin@haybrook.co.uk</a></span></p>
    </div>
  </div>
</div><div class="block block-block block-14 block-block-14 even block-without-title" id="block-block-14">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul><li><a href="https://twitter.com/haybrookit" id="twitter_link" title="twitter">twitter</a></li>
<li><a href="http://www.linkedin.com/company/853823" id="linkedin_link" title="linkedIn">linkedIn</a></li>
</ul>    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <script type="text/javascript">
<!--//--><![CDATA[//><!--
function euCookieComplianceLoadScripts() {}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.haybrook.co.uk/sites/all/modules/eu_cookie_compliance/js/eu_cookie_compliance.js?pmjwpn"></script>
</body>
</html>