<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <title>Home - Komixx Entertainment</title>

    <script>document.documentElement.className = document.documentElement.className.replace('no-js','js')</script>
    <script>document.createElement('picture')</script>

    <link href="https://komixx.com/wp-content/themes/transmedia/css/style.css" media="screen" rel="stylesheet" />
    <link href="https://komixx.com/wp-content/themes/transmedia/css/print.css" media="print" rel="stylesheet" />

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
        <script src="https://komixx.com/wp-content/themes/transmedia/js/lib/matchMedia-addListener.js"></script>
        <script src="https://komixx.com/wp-content/themes/transmedia/js/lib/matchMedia.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="shortcut icon" href="https://komixx.com/wp-content/themes/transmedia/img/favicon.ico">
    <link rel="apple-touch-icon" href="https://komixx.com/wp-content/themes/transmedia/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://komixx.com/wp-content/themes/transmedia/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://komixx.com/wp-content/themes/transmedia/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://komixx.com/wp-content/themes/transmedia/img/apple-touch-icon-144x144.png">

    <!-- Made by Humaan http://humaan.com @wearehumaan -->

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900" rel="stylesheet">

    <!-- Critical path CSS: -->
    <style>
        html.js #content,
        html.js .not-visible { opacity: 0; }
    </style>

    
<!-- This site is optimized with the Yoast SEO plugin v4.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://komixx.com/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Home - Komixx Entertainment" />
<meta property="og:url" content="https://komixx.com/" />
<meta property="og:site_name" content="Komixx Entertainment" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Home - Komixx Entertainment" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/komixx.com\/","name":"Komixx Entertainment","potentialAction":{"@type":"SearchAction","target":"https:\/\/komixx.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<script type='text/javascript' src='https://komixx.com/wp-content/themes/transmedia/js/jquery.min.js'></script>
<link rel='https://api.w.org/' href='https://komixx.com/wp-json/' />
<meta name="generator" content="WordPress 5.2.4" />
</head>

<body class="dark">
    <div class="page-container">
        <h1 id="h1" class="u-vis-hide">Home - Komixx Entertainment</h1>

        <header class="main clearfix js-push" role="banner">
            <nav class="bebas">
                <h2 class="u-vis-hide">Header Navigation</h2>

                <a href="/" class="logo">
                    <span class="u-vis-hide">Komixx Entertainment</span>
                    <img src="https://komixx.com/wp-content/themes/transmedia/img/logo-komixx.svg" alt="" width="135" height="39">
                </a>

                <ul>
                    <li><a href="https://komixx.com/" class="internal-link nav-home">Home</a></li>
                    <li><a href="https://komixx.com/projects/" class="internal-link nav-project">Film &amp; TV</a></li>
                    <li><a href="https://komixx.com/news/" class="internal-link nav-news">News</a></li>
                    <li><a href="https://komixx.com/about/" class="internal-link nav-about">About</a></li>
                    <li><a href="https://komixx.com/contact/" class="internal-link nav-contact">Contact</a></li>
                    <li>
	                    <a href="https://www.facebook.com/komixxEntertainment/" target="_blank">
		                    <span class="icon icon-icn_10-soc-fb"></span>
		                    <span class="u-hide">Facebook</span>
		                </a>
		            </li>
                    <li>
                    	<a href="https://www.instagram.com/komixxentertainment/" target="_blank">
	                    	<span class="icon icon-icn_18-soc-instagram"></span>
	                    	<span class="u-hide">Instagram</span>
	                    </a>
                    </li>
                                    </ul>

                <a href="javascript:void 0" class="hamburger">
                    <div class="lines">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>

                    <span class="u-vis-hide">Click here to open the mobile menu</span>
                </a>
            </nav>
        </header>
<div id="ajax" style="height: 100%;">            <div id="content" data-page="home">

            <main id="pagepiling" class="hero js-push">
                
                                                                        <a href="https://komixx.com/project/the-kissing-booth/" class="internal-link section cover films" id="panel2" data-anchor="panel2" style="background-image:url(https://komixx.com/wp-content/uploads/2019/06/KB_background.jpg)">
                        <div class="overlay">
                            <div class="title">
                                <img width="500" height="43" src="https://komixx.com/wp-content/uploads/2019/07/logo-500x43.png" class="attachment-home-title-image size-home-title-image" alt="" srcset="https://komixx.com/wp-content/uploads/2019/07/logo-500x43.png 500w, https://komixx.com/wp-content/uploads/2019/07/logo-300x26.png 300w, https://komixx.com/wp-content/uploads/2019/07/logo-768x66.png 768w, https://komixx.com/wp-content/uploads/2019/07/logo-1024x88.png 1024w, https://komixx.com/wp-content/uploads/2019/07/logo-128x11.png 128w, https://komixx.com/wp-content/uploads/2019/07/logo-1162x100.png 1162w, https://komixx.com/wp-content/uploads/2019/07/logo-334x29.png 334w, https://komixx.com/wp-content/uploads/2019/07/logo.png 1278w" sizes="(max-width: 500px) 100vw, 500px" />                            </div> 
                            <p>She Can Tell Her Best Friend Anything Except This One Thing</p>                            
                            <span class="btn arrow">Find out more</span>
                        </div>

                                                    <div class="pager-wrap">
                                <span class="btn icon smooth-scroll pager-next"><span class="u-vis-hide">Next slide</span></span>
                            </div>
                        
                                                <div class="hero__video">
                            <iframe src="https://player.vimeo.com/video/344745601?autoplay=1&amp;mute=1&amp;background=1" width="1900" height="915" frameborder="0" title="HOMEPAGE VIDEO" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                                            </a>
                                                        <a href="https://komixx.com/project/itch/" class="internal-link section cover shows" id="panel3" data-anchor="panel3" style="background-image:url(https://komixx.com/wp-content/uploads/2019/08/ITCH-KEYART_ME-LOGO_lowerres-1500x1279.jpg)">
                        <div class="overlay">
                            <div class="title">
                                <img width="500" height="354" src="https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-500x354.png" class="attachment-home-title-image size-home-title-image" alt="" srcset="https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-500x354.png 500w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-300x212.png 300w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-768x543.png 768w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-1024x724.png 1024w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-128x91.png 128w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-141x100.png 141w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-900x636.png 900w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-450x318.png 450w, https://komixx.com/wp-content/uploads/2019/08/SC060_ITCH_TITLE_01-334x236.png 334w" sizes="(max-width: 500px) 100vw, 500px" />                            </div> 
                            <p>What happens when a source of unimaginable power falls into the hands of a teenage boy?</p>                            
                            <span class="btn arrow">Find out more</span>
                        </div>

                                                    <div class="pager-wrap">
                                <span class="btn icon smooth-scroll pager-next"><span class="u-vis-hide">Next slide</span></span>
                            </div>
                        
                                            </a>
                                                        <a href="https://komixx.com/project/dog-loves-books/" class="internal-link section cover shows" id="panel4" data-anchor="panel4" style="background-image:url(https://komixx.com/wp-content/uploads/2019/06/doglovesbooksHero-1.jpg)">
                        <div class="overlay">
                            <div class="title">
                                                                    <h2 class="title">
                                        Dog Loves Books                                    </h2>
                                                            </div> 
                            <p>Because every book is an adventure!</p>                            
                            <span class="btn arrow">Find out more</span>
                        </div>

                        
                                                <div class="hero__video">
                            <iframe src="https://player.vimeo.com/video/300020728?autoplay=1&amp;mute=1&amp;background=1" width="1900" height="915" frameborder="0" title="HOMEPAGE VIDEO" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                                            </a>
                
                <div id="pp-nav">
                    <ul>
                                                    <li data-tooltip="">
	                            <a href="#panel2">
		                            <span style="border-color:rgb(0,0,0)"></span>
		                            <span class="u-vis-hide">nav2</span>
		                        </a>
	                        </li>
                                                    <li data-tooltip="">
	                            <a href="#panel3">
		                            <span style="border-color:rgb(0,0,0)"></span>
		                            <span class="u-vis-hide">nav3</span>
		                        </a>
	                        </li>
                                                    <li data-tooltip="">
	                            <a href="#panel4">
		                            <span style="border-color:rgb(0,0,0)"></span>
		                            <span class="u-vis-hide">nav4</span>
		                        </a>
	                        </li>
                                            </ul>
                </div>
            </main>

        </div>

            </div>            <footer class="main clearfix" role="contentinfo">
                <div id="extra-footer">
                    
                                    </div>

                <div class="container">
                    <p class="copyright">&copy; 2019 <a href="https://komixx.com/" class="internal-link" title="Komixx Entertainment">Komixx Entertainment</a></p>
					<p><a href="/terms" class="internal-link terms">Terms &amp; Conditions</a></p>
                    <p class="credits">Website by <a href="http://humaan.com" target="_blank" title="Websites, Mobile, eCommerce &ndash; Digital experiences for humans.">Humaan</a></p>
                </div>
            </footer>

            <div class="loader page-loader"></div>

        </div>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90841848-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">var ajax_var = {"url":"https:\/\/komixx.com\/wp-admin\/admin-ajax.php"}</script>
    <script type='text/javascript' src='https://komixx.com/wp-content/themes/transmedia/js/build.js'></script>

    </body>
</html>
