<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="google-site-verification" content="JaolqPgD-4_cGNpHjKL_0jUr3E69nV6rRZ4LZXTGIgY" />
            <meta name="robots" content="index, follow">
        <title>Forces Mutual | Military Insurance, Money Advice &amp; Healthcare</title>
        <meta name="description" content="Committed to offering a competitive range of insurance, money and health products to meet the needs of the Army, RAF, Royal Navy and Royal Marines." />
        <meta name="keywords" content="" />
        <meta property="og:site_name" content="Forces Mutual" />
        <meta property="og:url" content="https://www.forcesmutual.org/" />
        <meta property="og:title" content="Forces Mutual | Military Insurance, Money Advice &amp; Healthcare | Forces Mutual" />
        <meta property="og:image" content="https://www.forcesmutual.org/media/1719/north-wales-group-scheme.png" />
        <meta property="og:description" content="Committed to offering a competitive range of insurance, money and health products to meet the needs of the Army, RAF, Royal Navy and Royal Marines." />
        <meta property="og:type" content="article" />
        <meta property="og:determiner" content="auto" />
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="&#64;ForcesMutual">
        <meta name="twitter:creator" content="&#64;ForcesMutual">
        <meta name="twitter:title" content="Forces Mutual | Military Insurance, Money Advice &amp; Healthcare | Forces Mutual">
        <meta name="twitter:description" content="Committed to offering a competitive range of insurance, money and health products to meet the needs of the Army, RAF, Royal Navy and Royal Marines.">
        <meta name="twitter:image" content="https://www.forcesmutual.org/media/1719/north-wales-group-scheme.png">
        <meta name="application-name" content="&nbsp;" />
        <meta property="fb:app_id" content="1392725964387228">
        <link rel="apple-touch-icon" sizes="180x180" href="/images/fm/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/fm/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/fm/favicons/favicon-16x16.png">
        <link rel="manifest" href="/images/fm/favicons/site.webmanifest">
        <link rel="mask-icon" href="/images/fm/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/images/fm/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-config" content="/images/fm/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <link rel="canonical" href="https://www.forcesmutual.org/" />
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/forcesmutual.css?date=20191206" rel="stylesheet">
        <script type="text/javascript">
            (function () {
                if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
                    var msViewportStyle = document.createElement("style");
                    msViewportStyle.appendChild(
                        document.createTextNode("@-ms-viewport{width:auto!important}")
                    );
                    document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
                }
            })();
        </script>
        <script src="/scripts/jquery-2.2.3.min.js"></script>
        <script src="/scripts/restive.min.js"></script>
        <script src="/scripts/ui.js"></script>
<style>
img {
max-width:100%;
}

#hero{
background-position: center top;
}
#hero h1{
margin:40px 0;
}
#hero img.quote-img {
width:80%; max-width:350px; display: block; margin: 0 auto;
}
#hero-cta {
margin-top:20px;
}
#hero.mega-menu-bg #hero-cta {
display:none;
}

.u1400 #hero h1 {
margin:15px 0;
}
.u1400 #hero img.quote-img {
width:450px;
height:auto;
}
.u1400 #hero-cta {
margin:0;
}
.u960 #hero h1 {
margin:0 0;
}
.u960 #hero img.quote-img {
width:250px;
height:auto;
}
.u960 #hero-cta {
margin:0;
}
.u640 #hero h1 {
margin:0 0;
}
.u640 #hero img.quote-img {
width:250px;
height:auto;
}
.u640 #hero-cta {
margin:0;
}
.u480 #hero h1 {
margin:0 0;
}
.u480 #hero img.quote-img {
width:250px;
height:auto;
}
.u480 #hero-cta {
margin:0;
}





@media all and (max-width:480px){
h1 {
word-spacing: 400px;
}
}
#hero.mega-menu-bg p {
display:none;
}
#hero.mega-menu-bg div p {
display:block;
}
#mob-nav {
z-index: 100;
}
#mega-search, #mega-search-mob {
z-index:10;
background:#3e5488;
}

#hero .carousel-headline, .u1400 #hero .carousel-headline  {
font-size:6rem;
line-height:1.3;
}
.u2000 #hero .carousel-headline  {
font-size:7rem;
}

.u960 #hero .carousel-headline, .u640 #hero .carousel-headline, .u480 #hero .carousel-headline {
   font-size:3rem;
}

.christmas-image {
max-width: 95%; width: 1000px; margin: 0 auto; display: block;
}
.christmas-image.mobile {
display:none;
}
.u480 .christmas-image, .u640 .christmas-image{
display:none;
}
.u480 .christmas-image.mobile, .u640 .christmas-image.mobile {
display:block;
width:90%;
}
</style>        <!--[if lte IE 8]>
            <script type="text/javascript">
                    document.createElement('header');
                    document.createElement('nav');
                    document.createElement('menu');
                    document.createElement('section');
                    document.createElement('article');
                    document.createElement('aside');
                    document.createElement('footer');
            </script>
            <script src="/Scripts/jquery-1.9.1.min.js"></script>
            <link href="/css/ie8.css" rel="stylesheet" />
        <![endif]-->
        <!--[if IE 9]>  <link href="/css/ie9.css" rel="stylesheet" /> <![endif]-->
        <!--SOCIAL SIGNIN-->
        <script type="text/javascript">var ssiSocialTrackingCode = '1cb84bac5368655a';</script>
        <script type="text/javascript" src="https://app.socialsignin.net/assets/v1/js/social-tracking.js"></script>
        <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5491a67728ed17ed" async="async"></script>
        <!--STRUCTURED DATA-->
        <script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name" : "Forces Mutual",
            "url": "https://www.forcesmutual.org",
            "sameAs": [
            "http://www.facebook.com/forcesmutual",
            "http://www.twitter.com/forcesmutual"
            ],
            "logo": "https://www.forcesmutual.org/images/fm/logo-blue.png",
            "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+800-0001-0203",
            "contactType": "customer service"
            }]
            }
        </script>

</head>


<body>
        <!--ANALYTICS-->
    <style>
        .async-hide {
            opacity: 0 !important;
        }
    </style>
    <script>
(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-NKRQGCW':true});</script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76512384-1', 'auto');
  ga('require', 'GTM-NKRQGCW');  ga('send', 'pageview');

    </script>
   <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7532KK"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window, document, 'script', 'dataLayer', 'GTM-N7532KK');</script>
    <!-- End Google Tag Manager -->

            <div id="hero" style="background-image:url('/media/3252/about.jpg'); background-repeat:no-repeat;" data-id="/media/3252/about.jpg">
            

    <header>
        <div class="content-width">
            <!--[if IE 7]><div style="padding:20px 0;"><![endif]-->
            <!--[if IE 8]><div style="padding:20px 0;"><![endif]-->
            <a href="/"><img class="logo" src="/images/fm/logo.png" alt="Forces Mutual"></a>




            <nav>
                <ul id="nav">
                    <li class="drop">
                        <a href="#" class="">
                            Products
                            <img src="/images/fm/icons/chevron.png" alt="Chev">


                        </a>
                    </li>
                    <li><a href="/contact/" class="">Start Enquiry / Contact Us</a></li>
                </ul>
                <div id="mega-menu">
                    <div class="content-width container clearfix">
                        <div class="ins-col">
                            <p><a href="/insurance/"><span>Insurance <img src="/images/fm/icons/arrow-right.png" alt="insurance"></span></a></p>
                            <ul>
                                <li><a href="/insurance/car-insurance/">Car Insurance</a></li>
                                <li><a href="/insurance/motorbike-insurance/">Motorbike</a></li>
                                <li><a href="/insurance/personal-accident-insurance/">Personal Accident</a></li>
                                <li><a href="/insurance/life-insurance/">Life &amp; Serious Illness</a></li>
                                <li><a href="/about/claims/">Make a claim</a></li>
                            </ul>
                            <ul>
                                <li><a href="/insurance/kit-insurance/">Kit Insurance</a></li>
                                <li><a href="/insurance/home-and-contents-insurance/">Home &amp; Contents</a></li>
                                <li><a href="/insurance/travel-insurance/">Travel Insurance</a></li>
                                <li><a href="/insurance/service-funds-insurance/">Service Funds</a></li>
                            </ul>
                        </div>
                        <div class="col">
                            <p><a href="/money/"><span>Money <img src="/images/fm/icons/arrow-right.png" alt="money"></span></a></p>
                            <ul>
                                <li><a href="/money/mortgages/">Mortgages</a></li>
                                <li><a href="/money/mortgages/forces-help-to-buy/">Forces Help to Buy</a></li>
                                <li><a href="/money/my-sovereign-isa/">My Sovereign ISA</a></li>
                                <!--<li><a href="/contact/branches/#german">German Branches</a></li>-->
                            </ul>
                        </div>
                        <div class="col">
                            <p><a href="/health/"><span>Health <img src="/images/fm/icons/arrow-right.png" alt="insurance"></span></a></p>
                            <ul class="">
                                <li><a href="/health/healthcare/">Healthcare</a></li>
                                <li><a href="/health/health-cash-plan/">Health Cash Plan</a></li>
                            </ul>
                        </div>
                        <div class="col last">
                            <p><a href="/about/"><span>About <img src="/images/fm/icons/arrow-right.png" alt="about"></span></a></p>
                            <ul>
                                <li><a href="/news/">Latest News</a></li>
                                <li><a href="/military-foundation/">Military Foundation</a></li>
                                <li><a href="/forces-community/">For The Community</a></li>
                                <li><a href="/contact/">Contact Us</a></li>
                                <li><a href="/contact/find-us-locally/">Find Us Locally</a></li>
                                <li><a href="/contact/branches/">Our Branches</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div id="mobile-menu">
                            <div class="mob-nav-icons">
                                <img class="mob-nav-call mob-nav-icon" src="/images/fm/icons/mobile-menu.png" alt="Menu">
                                <img style="display:none;" class="mob-nav-call" src="/images/fm/icons/cross.png" alt="Menu">
                            </div>
                            <div class="mag-search">
                                <img class="magnify-glass mob-mag-icon" src="/images/fm/icons/magnify.png" alt="Search">
                                <img style="display:none;" class="magnify-glass" src="/images/fm/icons/cross.png" alt="Search">
                            </div>

                    
                </div>

                <div id="mob-nav">
                    <div class="pmenu">
                        <p><a href="/insurance/">Insurance</a> <img class="p1" src="/images/fm/icons/arrow-right.png" alt="More"></p>
                        <p><a href="/money/">Money</a> <img class="p2" src="/images/fm/icons/arrow-right.png" alt="More"></p>
                        <p><a href="/health/">Health</a> <img class="p3" src="/images/fm/icons/arrow-right.png" alt="More"></p>
                        <p><a href="/about/">About</a> <img class="p4" src="/images/fm/icons/arrow-right.png" alt="More"></p>
                    </div>

                    <div class="c1">
                        <p class="back"><img src="/images/fm/icons/arrow-left.png" alt="More">Insurance</p>
                        <a href="/insurance/car-insurance/">Car Insurance</a>
                        <a href="/insurance/home-and-contents-insurance/">Home &amp; Contents</a>
                        <a href="/insurance/kit-insurance/">Kit Insurance</a>
                        <a href="/insurance/travel-insurance/">Travel Insurance</a>
                        <a href="/insurance/personal-accident-insurance/">Personal Accident</a>
                        <a href="/insurance/motorbike-insurance/">Motorbike</a>
                        <a href="/insurance/life-insurance/">Life &amp; Serious Illness</a>
                        <a href="/about/claims/">Make a Claim</a>
                    </div>

                    <div class="c2">
                        <p class="back"><img src="/images/fm/icons/arrow-left.png" alt="More">Money</p>
                        <a href="/money/mortgages/">Mortgages</a>
                        <a href="/money/mortgages/forces-help-to-buy/">Forces Help to Buy</a>
                        <a href="/money/my-sovereign-isa/">ISA</a>
                        <!--<a href="/contact/branches/#german">German Branches</a>-->
                        
                    </div>

                    <div class="c3">
                        <p class="back"><img src="/images/fm/icons/arrow-left.png" alt="More">Health</p>
                        <a href="/health/healthcare/">Healthcare</a>
                        <a href="/health/health-cash-plan/">Health Cash Plan</a>
                    </div>

                    <div class="c4">
                        <p class="back"><img src="/images/fm/icons/arrow-left.png" alt="More">About</p>
                        <a href="/news/">Latest News</a>
                        <a href="/military-foundation/">Military Foundation</a>
                        <a href="/forces-community/">For The Community</a>
                        <a href="/contact/">Contact Us</a>
                        <a href="/contact/find-us-locally/">Find Us Locally</a>
                        <a href="/contact/branches/">Our Branches</a>
                    </div>

                </div>

            </nav>
            <div id="search-bar" class="">
                Search 
                        <img src="/images/fm/icons/search.png" alt="Search">

            </div>
            <div id="mega-search">
                <form action="/search" method="get">
                    <input id="squery" type="text" name="s" placeholder="What are you looking for?">
                </form>
            </div>
            <div id="mega-search-mob">
                <form action="/search" method="get">
                    <input id="squery-mob" type="text" name="s" placeholder="What are you looking for?">
                </form>
            </div>
            <!--[if IE 7]></div><![endif]-->
            <!--[if IE 8]></div><![endif]-->
        </div>
    </header>

            <div class="jcarousel-wrapper">
                <div class="jcarousel" style="overflow:hidden;height:700px;width:100%;position:relative;padding-top:25px;">
                    <ul style="overflow: hidden; position: relative;width:200%;">
                            <li style="width:50%;position:relative;float:left;background-image:url(''); background-repeat:no-repeat;">
                                <p><a href="/christmas/" onclick="ga('send', 'event', 'Homepage', 'Click', 'Christmas');"><img alt="Christmas banner" src="/media/4641/fm-christmas-banner.png" data-udi="umb://media/da4297c293c14fefa5a903fd085c391d" class="christmas-image" /><img alt="Win yourself a Merry Little Christmas" src="/media/4644/fm-christmas-mobile-2.png" data-udi="umb://media/f7022ca000e441539e685bfbbfc1ced9" class="christmas-image mobile" /></a></p>
                            </li>
                            <li style="width:50%;position:relative;float:left;background-image:url(''); background-repeat:no-repeat;">
                                <div id="hero-cta">
<h2 class="carousel-headline">We've updated our <br /><strong>Privacy Policy</strong></h2>
<p>We wanted to let you know about some changes <br />we have made to our Privacy Policy</p>
<p style="text-align: center;"><br /><a class="red-btn" style="padding: 20px 30px; background-image: none;" href="/about/privacy-policy/" onclick="ga('send', 'event', 'Homepage', 'Click', 'Privacy Policy');"><strong>Find out more</strong></a></p>
</div>
                            </li>
                    </ul>
                </div>
            </div>
        </div>







    <div id="stripes">
        
<section class="row no-margin" role="main">
    <section class="row no-padding no-margin" style=";background-size:cover;background-position:;background-color:;">
                            <div class="col-xs-12">
                                <div class="row no-padding">
<div id="content-top" class="clearfix content-width">
<div class="box-left">
<h2>Insurance</h2>
<p>From military kit<br />to life insurance.</p>
<ul>
<li><a href="/insurance/car-insurance/"><strong>Car Insurance</strong></a></li>
<li><a href="/insurance/kit-insurance/"><strong>Kit Insurance</strong></a></li>
<li><a href="/insurance/home-and-contents-insurance/"><strong>Home &amp; Contents</strong></a></li>
</ul>
<ul>
<li><a href="/insurance/personal-accident-insurance/"><strong>Personal Accident</strong></a></li>
<li><a href="/insurance/travel-insurance/"><strong>Travel Insurance</strong></a></li>
<li><a href="/insurance/life-insurance/"><strong>Life &amp; Serious Illness</strong></a></li>
</ul>
<a class="btn" href="/insurance/">View full range</a></div>
<div class="box-right">
<h2>Money</h2>
<p>From mortgages<br />to savings.</p>
<ul>
<li><a href="/money/mortgages/"><strong>Mortgages</strong></a></li>
<li><a href="/money/mortgages/forces-help-to-buy/"><strong>Forces Help to Buy</strong></a></li>
<li> </li>
</ul>
<ul>
<li><a href="/money/my-sovereign-isa/"><strong>My Sovereign ISA</strong></a></li>
<li> </li>
<li> </li>
</ul>
<a class="btn" href="/money/">View full range</a></div>
<div class="box-full">
<h2>Health</h2>
<p>From dental to private health.</p>
<ul>
<li><a href="/health/health-cash-plan/"><strong>Health Cash Plan</strong></a></li>
</ul>
<ul>
<li><a href="/health/healthcare/"><strong>Healthcare</strong></a></li>
</ul>
<a class="btn" href="/health/">View full range</a></div>
</div>
<div id="blue-cta-strip">
<h2>Want to meet us in person or start online?</h2>
<a class="blue-btn" href="/contact/find-us-locally/" onclick="ga('send', 'event', 'Homepage', 'Find us locally', 'Top CTA strip');">Find us locally</a> <a class="red-btn" href="/contact/" onclick="ga('send', 'event', 'Homepage', 'Enquire', 'Top CTA strip');">Start an enquiry</a>
<p>or call us on <strong>0151 363 5290 </strong></p>
</div>
</div>
                            </div>
                        </section>
                        <section class="row no-padding no-margin" style="background-image:url(/media/3178/navy.jpg);;background-size:auto;background-position:bottom;background-color:;">
                            <div class="col-xs-12">
                                <div id="homepage-bottom-content" class="clearfix">
<div class="content-width clearfix">
<div class="left">
<h3 style="font-size: 70px; line-height: 72px;">We exist to improve the lives of the Military family</h3>
<p>In 2018 we supported the wellbeing of the Military family in many ways.</p>
</div>
<div class="right">
<div class="stat">
<h4>3,344</h4>
<p>Military personnel attended our financial education sessions</p>
<div></div>
</div>
<div class="stat">
<h4>1,299</h4>
<p>Service personnel chose to protect themselves through our Personal Accident Insurance</p>
<div></div>
</div>
<div class="stat">
<h4>43</h4>
<p>families received free respite breaks</p>
<div></div>
</div>
<div class="stat">
<h4>Over 50</h4>
<p>sports teams, events and dinners benefitted from our sponsorship</p>
<div></div>
</div>
</div>
</div>
<a class="btn" onclick="ga('send', 'event', 'Homepage', 'Making a difference 2016', 'Button');" href="/about/making-a-difference/">Find out how we make a difference   </a>
<div class="clearfix content-width">
<div id="promo-box-left"><img alt="Community" src="/media/3186/community.jpg" data-udi="umb://media/a69593da2247443ea7e1ab4dcaa8504c" /><br />
<h3>For the<br />Community</h3>
<p>From forces discounts and holidays <span>to much more</span></p>
<a class="btn" href="/forces-community/">Find out more</a></div>
<div id="promo-box-right"><img alt="Military Foundation" src="/media/3190/foundation.jpg" data-udi="umb://media/099a375d73844c22a856d4050b880bde" /><br />
<h3>Military<br />Foundation</h3>
<p>What we do to improve life at home, <span>on base and on tour</span></p>
<a class="btn" href="/military-foundation/">Find out more</a></div>
</div>
</div>
                            </div>
                        </section>
                        <section class="row no-padding no-margin" style=";background-size:cover;background-position:;background-color:;">
                            <div class="col-xs-12">
                                <div id="news-strip" class="content-width clearfix">
                <div class="article-promo ">
                    <p class="date">21 November 2019</p>
                    <p class="title">Tips for winter driving</p>
                    <p class="link"><a href="https://www.forcesmutual.org/news/posts/tips-for-winter-driving/">Read more &gt;</a></p>
                </div>
                <div class="article-promo middle">
                    <p class="date">21 November 2019</p>
                    <p class="title">How to keep your phone protected</p>
                    <p class="link"><a href="https://www.forcesmutual.org/news/posts/how-to-keep-your-phone-protected/">Read more &gt;</a></p>
                </div>
                <div class="article-promo middle">
                    <p class="date">21 November 2019</p>
                    <p class="title">What’s in store for the UK housing market post Brexit?</p>
                    <p class="link"><a href="https://www.forcesmutual.org/news/posts/what-s-in-store-for-the-uk-housing-market-post-brexit/">Read more &gt;</a></p>
                </div>
        </div>
                            </div>
                        </section>
</section>
    </div>
    <footer>
    <div id="footer" class="content-width clearfix" style="padding-bottom:110px;">



        <div class="visible-xs row">
            <div class="col-xs-12">
                <a href="http://www.twitter.com/forcesmutual" rel="me" target="_blank">
                    <img class="social" src="/images/fm/icons/tw.png" alt="Forces Mutual Twitter">
                </a>
                <a href="http://www.facebook.com/forcesmutual" rel="me" target="_blank">
                    <img src="/images/fm/icons/fb.png" alt="Forces Mutual Facebook">
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <a href="/">
                    <img class="logo-cc" src="/images/fm/logo-blue.png" alt="Forces Mutual">
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <p>
                    <span>Your Financial Ally</span>
                    <strong class="line">|</strong>
                            <span>0151 363 5290  <strong>|</strong> <span class="p0345">0044 (0)151 363 5290</span></span>

                    <a class="so-mo" href="http://www.twitter.com/forcesmutual" target="_blank"><img class="social" src="/images/fm/icons/tw.png" alt="Forces Mutual Twitter"></a>
                    <a class="so-mo" href="http://www.facebook.com/forcesmutual" target="_blank"><img src="/images/fm/icons/fb.png" alt="Forces Mutual Facebook"></a>

                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="legal-links">
                    <div class="legal-links"><a href="/about/accessibility/">Accessibility</a> | <a href="/about/legal/">Legal</a> | <a href="/about/privacy-policy/">Privacy Policy</a>  | <a href="/about/security-policy/">Security Policy</a> | <a href="/about/cookie-policy/">Cookies Policy</a>  |  <a href="/about/social-media-policy/">Social Media Policy</a></div>
                </div>
            </div>



    </div>
</footer>

<!--GOOGLE TRACKING-->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1007849663;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1007849663/?value=0&amp;guid=ON&amp;script=0" />
    </div>
</noscript>
<!--BING UET-->
<script>(function (w, d, t, r, u) { var f, n, i; w[u] = w[u] || [], f = function () { var o = { ti: "5522614" }; o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad") }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () { var s = this.readyState; s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null) }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i) })(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5522614&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

    <div class="container-fluid cookie-popup" style="display:none;    position: fixed;    bottom: 0;    left: 0;    width: 100%;    background: white;    border-top: 2px solid #db0b18;    border-bottom: 2px solid #db0b18;">
    <div class="container">
        <div class="row">
            <div class="row no-margin">
                <div class="col-xs-12 col-sm-10 col-md-11">
                    <p style="color: #8e8e8e; font-size: 19px; font-weight: 400;">www.forcesmutual.org uses cookies to help your user experience and get the best out of the site. By continuing to use this website you are consenting to our use of cookies. For further information and to manage your cookie preferences, view our <a href="/about/cookie-policy/" title="Our Cookie Policy">cookie policy.</a></p>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-1">
                    <div class="btn-warning-magenta btn-smaller close-cookies no-arrow text-center no-margin" style="background: #3e5488;color: white;padding: 13px;font-size: 18px;cursor: pointer;">Close</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        var acceptCookie = readCookie("accept-cookies");
        if (acceptCookie == "cookies-accepted") {

        } else {
            $(".cookie-popup").show();
        }
        var fiveYears = 5 * 365;
        $(".close-cookies").click(function () {
            createCookie("accept-cookies", "cookies-accepted", fiveYears);
            $(".cookie-popup").hide();
        });
    });
</script>
    <script type="text/javascript">
    $(function () {
        var url = [location.protocol, '//', location.host, location.pathname].join('');
        $("form.clickdimensions input, form.clickdimensions textarea").not(':input[type=hidden], :input[type=button], :input[type=submit], :input[type=reset]').blur(function () {
            createCookie(url + " " + $(this).attr("id"), $(this).val(), 1);
        });

        $("form.clickdimensions input, form.clickdimensions textarea").not(':input[type=hidden], :input[type=button], :input[type=submit], :input[type=reset]').each(function () {
            var id = $(this).attr("id");
            var cookieValue = readCookie(url + " " + id);
            $(this).val(cookieValue);
        });


        $("form.clickdimensions select").not(':input[type=hidden], :input[type=button], :input[type=submit], :input[type=reset]').change(function () {
            createCookie(url + " " + $(this).attr("id"), $(this).val(), 1);
        });

        $("form.clickdimensions select").not(':input[type=hidden], :input[type=button], :input[type=submit], :input[type=reset]').each(function () {
            var id = $(this).attr("id");
            var cookieValue = readCookie(url + " " + id);
            $(this).val(cookieValue);
        });
        if ($("form").hasClass("clickdimensions")) {
            dropDownChangeActions();
        }
    });


</script>
    <script src="/scripts/jquery.cookie.js" defer="defer"></script>
<script src="/scripts/bootstrap.min.js" defer="defer"></script>
<script src="/scripts/jquery.ui.touch-punch.min.js" defer="defer"></script>
<script src="/scripts/modernizr-latest.js" defer="defer"></script>

<!--[if lt IE 9]>
    <script src="/scripts/respond.min.js" defer="defer"></script>
<![endif]-->

    <script type="text/javascript" src="/scripts/jquery.jcarousel.min.js"></script>
<script type="text/javascript">
        $(function () {
            $(".jcarousel").jcarousel({
                wrap: 'both'
                
            });
$(".jcarousel").jcarouselAutoscroll({
interval: 6000
}
);
});
</script>
</body>
</html>
