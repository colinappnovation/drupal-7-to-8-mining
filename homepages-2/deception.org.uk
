<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="shortcut icon" href="https://www.deception.org.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
  <title>deception research | Our perception of deception</title>
  <style type="text/css" media="all">
@import url("https://www.deception.org.uk/modules/system/system.base.css?pxywr8");
@import url("https://www.deception.org.uk/modules/system/system.menus.css?pxywr8");
@import url("https://www.deception.org.uk/modules/system/system.messages.css?pxywr8");
@import url("https://www.deception.org.uk/modules/system/system.theme.css?pxywr8");
</style>
<style type="text/css" media="all">
@import url("https://www.deception.org.uk/sites/all/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.core.min.css?pxywr8");
@import url("https://www.deception.org.uk/sites/all/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.theme.min.css?pxywr8");
</style>
<style type="text/css" media="all">
@import url("https://www.deception.org.uk/modules/book/book.css?pxywr8");
@import url("https://www.deception.org.uk/modules/comment/comment.css?pxywr8");
@import url("https://www.deception.org.uk/modules/field/theme/field.css?pxywr8");
@import url("https://www.deception.org.uk/modules/node/node.css?pxywr8");
@import url("https://www.deception.org.uk/modules/search/search.css?pxywr8");
@import url("https://www.deception.org.uk/modules/user/user.css?pxywr8");
@import url("https://www.deception.org.uk/sites/all/modules/views/css/views.css?pxywr8");
</style>
<style type="text/css" media="all">
@import url("https://www.deception.org.uk/sites/all/modules/ctools/css/ctools.css?pxywr8");
@import url("https://www.deception.org.uk/sites/all/modules/panels/css/panels.css?pxywr8");
@import url("https://www.deception.org.uk/sites/all/libraries/jscrollpane/jquery.jscrollpane.css?pxywr8");
</style>
<style type="text/css" media="all">
@import url("https://www.deception.org.uk/sites/all/themes/mayo/css/layout.css?pxywr8");
@import url("https://www.deception.org.uk/sites/all/themes/mayo/css/style.css?pxywr8");
@import url("https://www.deception.org.uk/sites/research.deception.org.uk/files/color/mayo-4bbac43a/colors.css?pxywr8");
</style>

<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.deception.org.uk/sites/all/themes/mayo/css/ie8.css?pxywr8" media="all" />
<![endif]-->

<!--[if  IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.deception.org.uk/sites/all/themes/mayo/css/ie.css?pxywr8" media="all" />
<![endif]-->

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.deception.org.uk/sites/all/themes/mayo/css/ie6.css?pxywr8" media="all" />
<![endif]-->
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
body{font-size:87.5%;font-family:Georgia,'Palatino Linotype','Book Antiqua','URW Palladio L',Baskerville,Meiryo,'Hiragino Mincho Pro','MS PMincho',serif;}
h1,h2,h3,h4,h5{font-family:Georgia,'Palatino Linotype','Book Antiqua','URW Palladio L',Baskerville,Meiryo,'Hiragino Mincho Pro','MS PMincho',serif;}

/*]]>*/-->
</style>
  <script type="text/javascript" src="https://www.deception.org.uk/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.deception.org.uk/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.deception.org.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.deception.org.uk/misc/drupal.js?pxywr8"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/modules/jquery_update/replace/ui/ui/minified/jquery.ui.core.min.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/modules/admin_menu/admin_devel/admin_devel.js?pxywr8"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/libraries/jscrollpane/jquery.mousewheel.js?pxywr8"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/libraries/jscrollpane/mwheelIntent.js?pxywr8"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/libraries/jscrollpane/jquery.jscrollpane.min.js?pxywr8"></script>
<script type="text/javascript" src="https://www.deception.org.uk/sites/all/modules/scrollbar/scrollbar.js?pxywr8"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"mayo","theme_token":"6V1byEM6OYJ4IR5Qe9ti6rkkZr22PRS1O_iyNzMRhaA","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/libraries\/jscrollpane\/jquery.mousewheel.js":1,"sites\/all\/libraries\/jscrollpane\/mwheelIntent.js":1,"sites\/all\/libraries\/jscrollpane\/jquery.jscrollpane.min.js":1,"sites\/all\/modules\/scrollbar\/scrollbar.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/libraries\/jscrollpane\/jquery.jscrollpane.css":1,"sites\/all\/themes\/mayo\/css\/layout.css":1,"sites\/all\/themes\/mayo\/css\/style.css":1,"sites\/all\/themes\/mayo\/css\/colors.css":1,"sites\/all\/themes\/mayo\/css\/ie8.css":1,"sites\/all\/themes\/mayo\/css\/ie.css":1,"sites\/all\/themes\/mayo\/css\/ie6.css":1,"0":1,"1":1}},"scrollbar":{"element":".field-name-body","showArrows":true,"mouseWheelSpeed":10,"arrowButtonSpeed":10,"arrowRepeatFreq":100,"horizontialGutter":4,"verticalGutter":4,"verticalDragMinHeight":0,"verticalDragMaxHeight":99999,"verticalDragMinWidth":0,"verticalDragMaxWidth":99999,"horizontialDragMinHeight":0,"horizontialDragMaxHeight":99999,"horizontialDragMinWidth":0,"horizontialDragMaxWidth":99999,"arrowScrollOnHover":true,"verticalArrowPositions":"split","horizontialArrowPositions":"split","autoReinitialise":false,"autoReinitialiseDelay":500},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in no-sidebars page-front-page" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    

<div id="page-wrapper" style="width: 90%; margin-top: 14px; margin-bottom: 14px;">
  <div id="page" style="padding: 10px;">

    <div id="header" style="height: 109px;filter:;background: url(/sites/research.deception.org.uk/files/deception_header06.png) repeat top left;border: none;">
    <div id="header-watermark" style="">
    <div class="section clearfix">

              <div id="logo" style="padding-left: 10px; padding-top: 10px;">
        <a href="/" title="Home" rel="home">
          <img src="https://www.deception.org.uk/sites/research.deception.org.uk/files/deception87.png" alt="Home" />
        </a>
        </div> <!-- /#logo -->
      
              <div id="name-and-slogan" style="padding-left: 20px; padding-top: 15px;">
                                    <h1 id="site-name">
                <a href="/" title="Home" rel="home"><span>deception research</span></a>
              </h1>
                      
                      <div id="site-slogan">Our perception of deception</div>
                  </div> <!-- /#name-and-slogan -->
      
              <div id="header-searchbox" style="padding-right: 15px; padding-top: 20px;">
      <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input onblur="if (this.value == &#039;&#039;) { this.value = &#039;search this site&#039;; }" onfocus="if (this.value == &#039;search this site&#039;) { this.value = &#039;&#039;; }" type="text" id="edit-search-block-form--2" name="search_block_form" value="search this site" size="24" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="image" src="/sites/all/themes/mayo/images/search-submit.png" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-ChGlH8iiezV9q5lj9s2LSDSPCjhJjkh24pfLsh7BhsA" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>        </div>
      
      
      <div class="clearfix cfie"></div>

      
    </div> <!-- /.section -->
    </div> <!-- /#header-watermark -->
    </div> <!-- /#header -->

          <div id="navigation"><div class="section">
        <ul id="main-menu" class="links inline clearfix"><li class="menu-218 first active"><a href="/" title="" class="active">Home</a></li>
<li class="menu-536"><a href="/research" title="">Research</a></li>
<li class="menu-535"><a href="/researchers" title="">Researchers</a></li>
<li class="menu-4912 last"><a href="/repo" title="Public Access to Data Files">Public Data Repository</a></li>
</ul>              </div></div> <!-- /.section, /#navigation -->
    
    <div class="clearfix cfie"></div>

    <!-- for nice_menus, superfish -->
        
    <!-- space between menus and contents -->
    <div class="spacer clearfix cfie"></div>


    <div id="main-wrapper">
    <div id="main" class="clearfix" style="">

      
      
      
      <div class="clearfix cfie"></div>


      <!-- sidebars (left) -->
            

      <!-- main content -->
      <div id="content" class="column" style="width: 100%;"><div class="section" style="margin-left: 0px; margin-right: 0px;">

        
        
        <a id="main-content"></a>
                                <div class="tabs"></div>                          <div class="region region-content">
    <div id="block-system-main" class="block block-system clearfix">

    
  <div class="content">
    <div class="view view-front-page-changes view-id-front_page_changes view-display-id-page view-dom-id-daa98b94ab56e131069343face87963b">
            <div class="view-header">
      <h2>HELLO AND WELCOME TO DECEPTION RESEARCH</h2><div><table><tbody><tr><td><img alt="" src="/sites/research.deception.org.uk/files/u2/frontpage_left.png"></td><td><p></p><p>This page is a result of a joint project funded by <a href="http://www.epsrc.ac.uk/Pages/default.aspx">EPSRC</a> and conducted by <a href="http://www.cam.ac.uk">Cambridge University</a>, <a href="http://www.ucl.ac.uk/">UCL</a>, <a href="http://www.ncl.ac.uk/">Newcastle University</a> and the <a href="http://www.port.ac.uk/">University of Portsmouth</a>. Here you will find info on <a href="/researchers">researchers</a> and <a href="/research">literature on deception</a>. Simply follow the links above.</p><p>We are eager to hear from you if you wish to contribute to this repository.</p><p>Drop us a line at: david.modic[@]cl.cam.ac.uk (as is usual, remove the square brackets).</p></td><td><img alt="" src="/sites/research.deception.org.uk/files/u2/frontpage_right.png"></td></tr></tbody></table><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>    </div>
  
  
  
      <div class="view-content">
      <table  class="views-table cols-2">
        <caption><h2>Latest contributions in the "Research" category sorted by date - added (desc)</h2></caption>
        <thead>
      <tr>
                  <th  class="views-field views-field-title view-news-title-label" scope="col">
                      </th>
                  <th  class="views-field views-field-body view-news-body-label" scope="col">
            Summary          </th>
              </tr>
    </thead>
    <tbody>
          <tr  class="odd view-news-row views-row-first">
                  <td  class="views-field views-field-title view-news-title-class">
            <a href="/content/machiavelli-poker-mate">Machiavelli as a poker mate</a>          </td>
                  <td  class="views-field views-field-body view-news-body-class">
            <p>Machiavellianism has been considered in the literature as the symbol for manipulative strategies in social conduct. However, it has been rarely studied via behavioural experiments outside the laboratory, in more naturalistic settings. We report the first behavioural study (N=490) evaluating whether Machiavellian individuals, high Machs, deceive more than low Machs in online poker, where deception is ethically acceptable and strategically beneficial. Specifically, we evaluated Machiavellianism, bluffing patterns, and emotional sensitivity to getting “slow-played” (“stepping into a trap”). Bluffing was assessed by realistic poker tasks wherein participants made decisions to bluff or not, and sensitivity to slow-play by a self-report measure. We found that high Machs had higher average bluffsizes than low Machs (but not higher bluffing frequency) and were more distraught by getting slow-played. The Machiavellian sub-trait “desire for control” also positively predicted bluffing frequency. We show that online poker can be utilized to investigate the psychology of deception and Machiavellianism. The results also illustrate a conceptual link between unethical and ethical types of deception, as Machiavellianism is implicated in both.</p>
          </td>
              </tr>
          <tr  class="even view-news-row">
                  <td  class="views-field views-field-title view-news-title-class">
            <a href="/content/european-association-psychology-and-law-eapl-conference-2014-summary">European Association of Psychology and Law (EAPL) Conference 2014 Summary</a>          </td>
                  <td  class="views-field views-field-body view-news-body-class">
            <p>The European Association of Psychology and Law (EAPL) annually organises a conference to bring together researchers and practitioners operating in a forensic context. Combining different disciplines, such as psychology, criminology and law leads to a multidisciplinary conference with presentations on topics like detecting deception, false memories, presenting forensic evidence in court, investigative interviewing, risk assessment, offenders, victims and eyewitness identification (see program). This year’s conference took place during the 24-27th of June 2014 in St. Petersburg and I (Sophie Van Der Zee) summarised a selection of talks given during this conference.</p>
          </td>
              </tr>
          <tr  class="odd view-news-row">
                  <td  class="views-field views-field-title view-news-title-class">
            <a href="/content/we-will-make-you-our-research-development-susceptibility-persuasion-scale">We Will Make You Like Our Research: The Development of a Susceptibility-to-Persuasion Scale</a>          </td>
                  <td  class="views-field views-field-body view-news-body-class">
            <p>Link to full text at ssrn: <a href="http://ssrn.com/abstract=2446971">http://ssrn.com/abstract=2446971</a> <strong>Abstract:</strong> <span style="font-family: Trebuchet, Tahoma, 'Myriad Roman', Arial, Helvetica, sans-serif; font-size: small; line-height: normal;">Social-psychological and other persuasive mechanisms across diverse contexts are well researched, yet in general the research focusses on the effectiveness of a specific persuasive technique. In the present paper, our specific interest lies in the development of a generalized modular psychometric tool that measures individual susceptibility to persuasion.</span></p>          </td>
              </tr>
          <tr  class="even view-news-row">
                  <td  class="views-field views-field-title view-news-title-class">
            <a href="/content/moral-rationalization-and-integration-situational-factors-and-psychological-processes">Moral rationalization and the integration of situational factors and psychological processes in immoral behavior</a>          </td>
                  <td  class="views-field views-field-body view-news-body-class">
            <p class="p1"><a href="http://94.23.146.173/ficheros/9d0a260f4986d8780804fb111107e031.pdf">Link to full text from the Author's site.</a> <strong style="line-height: 1.538em;">Abstract. </strong><span style="line-height: 1.538em;">Moral rationalization is an individual's ability to reinterpret his or her immoral actions as, in fact, moral. It arises out of a conflict of motivations and a need to see the self as moral. This article presents a model of evil behavior demonstrating how situational factors that obscure moral relevance can interact with moral rationalization and lead to a violation of moral principles.</span></p>          </td>
              </tr>
          <tr  class="odd view-news-row views-row-last">
                  <td  class="views-field views-field-title view-news-title-class">
            <a href="/content/deception-experiments-costs-alleged-method-last-resort">Deception in Experiments: The Costs of an Alleged Method of Last Resort</a>          </td>
                  <td  class="views-field views-field-body view-news-body-class">
            <p><strong>Abstract: </strong>In psychology, deception is commonly used to increase experimental control. Yet, its use has provoked concerns that it raises participants' suspicions, prompts second-guessing of experimenters' true intentions, and ultimately distorts behavior and the control it is meant to achieve. These concerns can and have been subjected to empirical analysis. Our review of the evidence yielded two key results: First, there is evidence that participants who experienced deception firsthand are likely to become suspicious and that there are non-negligible differences between suspicious and reportedly na‹ve participants.</p>          </td>
              </tr>
      </tbody>
</table>
    </div>
  
      <h2 class="element-invisible">Pages</h2><div class="item-list"><ul class="pager"><li class="pager-current first">1</li>
<li class="pager-item"><a title="Go to page 2" href="/front-page?page=1">2</a></li>
<li class="pager-item"><a title="Go to page 3" href="/front-page?page=2">3</a></li>
<li class="pager-item"><a title="Go to page 4" href="/front-page?page=3">4</a></li>
<li class="pager-item"><a title="Go to page 5" href="/front-page?page=4">5</a></li>
<li class="pager-item"><a title="Go to page 6" href="/front-page?page=5">6</a></li>
<li class="pager-item"><a title="Go to page 7" href="/front-page?page=6">7</a></li>
<li class="pager-item"><a title="Go to page 8" href="/front-page?page=7">8</a></li>
<li class="pager-next"><a title="Go to next page" href="/front-page?page=1">next ›</a></li>
<li class="pager-last last"><a title="Go to last page" href="/front-page?page=7">last »</a></li>
</ul></div>  
  
  
  
  
</div>  </div>
</div>
  </div>
        
      </div></div> <!-- /.section, /#content -->


      <!-- sidebars (right) -->
            

      <div class="clearfix cfie"></div>

      
      <div class="clearfix cfie"></div>


            <div id="spacer" class="clearfix cfie"></div>
      <div id="banner-bottom" class="banner clearfix">  <div class="region region-banner-bottom">
    <div id="block-menu-menu-intranet" class="block block-menu clearfix">

    
  <div class="content">
    <ul class="menu"><li class="first last leaf"><a href="/user" title="just a login">intranet</a></li>
</ul>  </div>
</div>
  </div>
</div>
      
    </div> <!-- /#main -->
    </div> <!-- /#main-wrapper -->

    <!-- space between contents and footer -->
    <div id="spacer" class="clearfix cfie"></div>

    <div id="footer-wrapper">
      
            <div id="footer"><div class="section">
          <div class="region region-footer">
    <div id="block-system-powered-by" class="block block-system clearfix">

    
  <div class="content">
    <span>Powered by <a href="https://www.drupal.org">Drupal</a></span>  </div>
</div>
  </div>
      </div></div> <!-- /.section, /#footer -->
      
    </div> <!-- /#footer-wrapper -->


  </div> <!-- /#page -->
</div> <!-- /#page-wrapper -->
  </body>
</html>
