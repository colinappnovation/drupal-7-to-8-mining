<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.davecohen.org.uk/sites/default/files/Banana_0.ico" type="image/vnd.microsoft.icon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.davecohen.org.uk/" />
<link rel="shortlink" href="https://www.davecohen.org.uk/" />
  <title>Dave Cohen | "Guru of the one-liner" (The Scotsman)</title>
  <!-- META FOR IOS & HANDHELD -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <meta name="HandheldFriendly" content="true" />
  <meta name="apple-touch-fullscreen" content="YES" />
  <!-- //META FOR IOS & HANDHELD -->  
  <style type="text/css" media="all">
@import url("https://www.davecohen.org.uk/modules/system/system.base.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/system/system.menus.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/system/system.messages.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/system/system.theme.css?ppwldn");
</style>
<style type="text/css" media="all">
@import url("https://www.davecohen.org.uk/modules/aggregator/aggregator.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/comment/comment.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/modules/date/date_api/date.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/field/theme/field.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/node/node.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/search/search.css?ppwldn");
@import url("https://www.davecohen.org.uk/modules/user/user.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/modules/youtube/css/youtube.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/modules/views/css/views.css?ppwldn");
</style>
<style type="text/css" media="all">
@import url("https://www.davecohen.org.uk/sites/all/modules/ctools/css/ctools.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/modules/lightbox2/css/lightbox.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/modules/panels/css/panels.css?ppwldn");
</style>
<style type="text/css" media="all">
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/light/bootstrap/css/bootstrap.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/light/bootstrap/css/bootstrap-responsive.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/light/css/light-base.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/layout.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/region.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/block.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/menu.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/screen.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/color.css?ppwldn");
</style>
<style type="text/css" media="only screen and (min-width: 1200px)">
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/screens/wide.css?ppwldn");
</style>
<style type="text/css" media="only screen and (min-width:768px) and (max-width:979px)">
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/screens/tablet.css?ppwldn");
</style>
<style type="text/css" media="only screen and (max-width:767px)">
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/screens/mobile.css?ppwldn");
</style>
<style type="text/css" media="only screen and (max-width:480px)">
@import url("https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/css/screens/mobile-vertical.css?ppwldn");
</style>
<style type="text/css" media="all">
@import url("https://www.davecohen.org.uk/sites/default/files/css_injector/css_injector_1.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/default/files/css_injector/css_injector_2.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/default/files/css_injector/css_injector_3.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/default/files/css_injector/css_injector_5.css?ppwldn");
@import url("https://www.davecohen.org.uk/sites/default/files/css_injector/css_injector_6.css?ppwldn");
</style>
  <script type="text/javascript" src="https://www.davecohen.org.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/misc/drupal.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/modules/admin_menu/admin_devel/admin_devel.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/modules/lightbox2/js/lightbox.js?ppwldn"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

	(function ($) {
	  if (Drupal.Nucleus == undefined) {
		Drupal.Nucleus = {};
	  }
	  Drupal.behaviors.skinMenuAction = {
		attach: function (context) {
		  jQuery(".change-skin-button").click(function() {
			parts = this.href.split("/");
			style = parts[parts.length - 1];
			jQuery.cookie("light_skin", style, {path: "/"});
			window.location.reload();
			return false;
		  });
		}
	  }
	})(jQuery);
  
//--><!]]>
</script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/themes/personal_blog/light/js/jquery.cookie.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/themes/personal_blog/light/js/light.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/js/jquery.smooth-scroll.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/js/responsive.js?ppwldn"></script>
<script type="text/javascript" src="https://www.davecohen.org.uk/sites/all/themes/personal_blog/weeblog/js/weeblog.js?ppwldn"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"weeblog","theme_token":"hHcOmcxYizLLlewXodetAfICef_qcOFYU8zwVrY2zdM","js":{"0":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"1":1,"sites\/all\/themes\/personal_blog\/light\/js\/jquery.cookie.js":1,"sites\/all\/themes\/personal_blog\/light\/js\/light.js":1,"sites\/all\/themes\/personal_blog\/weeblog\/js\/jquery.smooth-scroll.js":1,"sites\/all\/themes\/personal_blog\/weeblog\/js\/responsive.js":1,"sites\/all\/themes\/personal_blog\/weeblog\/js\/weeblog.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/youtube\/css\/youtube.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/themes\/personal_blog\/light\/bootstrap\/css\/bootstrap.css":1,"sites\/all\/themes\/personal_blog\/light\/bootstrap\/css\/bootstrap-responsive.css":1,"sites\/all\/themes\/personal_blog\/light\/css\/light-base.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/layout.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/region.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/block.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/menu.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/screen.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/color.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/screens\/wide.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/screens\/tablet.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/screens\/mobile.css":1,"sites\/all\/themes\/personal_blog\/weeblog\/css\/screens\/mobile-vertical.css":1,"public:\/\/css_injector\/css_injector_1.css":1,"public:\/\/css_injector\/css_injector_2.css":1,"public:\/\/css_injector\/css_injector_3.css":1,"public:\/\/css_injector\/css_injector_5.css":1,"public:\/\/css_injector\/css_injector_6.css":1}},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":true,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":false,"disable_resize":false,"disable_zoom":false,"force_show_nav":false,"show_caption":true,"loop_items":false,"node_link_text":"View Image Details","node_link_target":false,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":false,"useragent":"Mozilla\/5.0"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-7 node-type-article" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
<div id="page" class="page-default"> <a name="Top" id="Top"></a>
      
  <!-- HEADER -->
  <div id="header-wrapper" class="wrapper">
    <div class="container">
      <div class="row">
        <div class="span12 clearfix">
          <div id="header" class="clearfix">
                        <a href="/" title="Home" id="logo"> <img src="https://www.davecohen.org.uk/sites/default/files/dcohen_chortle_0.jpg" alt="Home" /> </a>
                                    <div id="name-and-slogan" class="hgroup">
                            <h1 class="site-name"> <a href="/" title="Home"> Dave Cohen </a> </h1>
                                          <p class="site-slogan">"Guru of the one-liner" (The Scotsman)</p>
                          </div>
                                                          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- //HEADER -->
  
    <div id="featured-wrapper" class="wrapper">
    <div class="featured-inner">
      <div class="featured-inner2">
        <div class="container">
          <div class="row">
            <div class="span12 clearfix">                              <h1 id="page-title">HO-ME</h1>
                             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <div id="main-wrapper" class="wrapper">
    <div class="container">
      <div class="row clearfix"> 
        <!-- MAIN CONTENT -->
        <div id="main-content" class="span8">
          <div class="grid-inner clearfix">
                                                  <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div class="two-33-66 at-panel panel-display clearfix" >
    <div class="region region-two-33-66-first">
    <div class="region-inner clearfix">
      <div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.davecohen.org.uk/sites/default/files/styles/medium/public/field/image/book%20cover%20final%20%281%29.jpg?itok=reHqlYw8"><img typeof="foaf:Image" src="https://www.davecohen.org.uk/sites/default/files/styles/medium/public/field/image/book%20cover%20final%20%281%29.jpg?itok=reHqlYw8" width="149" height="220" alt="" /></div></div></div>    </div>
  </div>
  <div class="region region-two-33-66-second">
    <div class="region-inner clearfix">
      <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"> <h2 style="text-align: center;"><span style="font-size: medium;"><strong><span style="font-family: arial, helvetica, sans-serif; line-height: 48px;">&nbsp;Hello, welcome to my Website. I'm Dave Cohen and since 1983 I've been writing, performing and teaching comedy... here's what I'm currently up to:</span></strong></span></h2>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333;"><span style="font-size: xx-large;">MY NEW BOOK</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333;"><span style="font-size: xx-large;">THE COMPLETE COMEDY WRITER</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333;"><span style="font-size: xx-large;">AVAILABLE NOW</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: xx-small; color: #333333;"><a href="https://www.amazon.co.uk/Complete-Comedy-Writer-stand-up-screenplay-ebook/dp/B07L12TSN1">https://www.amazon.co.uk/Complete-Comedy-Writer-stand-up-screenplay-ebook/dp/B07L12TSN1</a></span></div>
<p style="text-align: center;"><span style="font-family: arial, helvetica, sans-serif; font-size: large; line-height: 48px;">LEARN COMEDY WRITING... MAKE YOUR SCREENPLAY FUNNIER... MAKE YOUR NOVEL FUNNIER... MAKE YOUR STAND-UP FUNNIER</span></p>
<p style="text-align: center;"><a href="https://www.amazon.co.uk/Complete-Comedy-Writer-stand-up-screenplay-ebook/product-reviews/B07L12TSN1/ref=cm_cr_dp_d_show_all_top?ie=UTF8&amp;reviewerType=all_reviews"><span style="font-family: arial, helvetica, sans-serif; font-size: large; line-height: 48px;">book reviews <span style="text-decoration: underline;">here</span></span></a></p>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><a href="https://www.amazon.co.uk/Complete-Comedy-Writer-stand-up-screenplay-ebook/product-reviews/B07L12TSN1/ref=cm_cr_dp_d_show_all_top?ie=UTF8&amp;reviewerType=all_reviews"><span style="font-size: 18.6667px;"><i></i></span><span style="background-color: #f5f8fa; color: #14171a; font-family: 'Segoe UI', Arial, sans-serif; font-size: 14px; white-space: pre-wrap;">"A studied assessment of the job and the craft that should spark the little grey cells if you’re serious about entering this vastly oversubscribed profession." CHORTLE ****</span></a></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="background-color: #f5f8fa; color: #14171a; font-family: 'Segoe UI', Arial, sans-serif; font-size: 14px; white-space: pre-wrap;">"Fabulous book written by someone who knows what he's talking about. Hugely recommended." Simon Wright *****</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="background-color: #f5f8fa; color: #14171a; font-family: 'Segoe UI', Arial, sans-serif; font-size: 14px; white-space: pre-wrap;">"I've read a lot of comedy books but this one stands out, packed full of pertinent and up-to-date information." D Stokes *****</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="background-color: #f5f8fa; color: #14171a; font-family: 'Segoe UI', Arial, sans-serif; font-size: medium; white-space: pre-wrap;"><span style="color: #333333; font-family: 'arial black', 'avant garde'; white-space: normal; background-color: #f0f0f0;">SIGN UP HERE AND GET MY PREVIOUS BOOK FOR FREE!</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="background-color: #f5f8fa; color: #14171a; font-family: 'Segoe UI', Arial, sans-serif; font-size: 14px; white-space: pre-wrap;"><font color="#14171a" face="Segoe UI, Arial, sans-serif"><a href="http://davecohen.org.uk/signup">http://davecohen.org.uk/signup</a></font></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333;"><span style="font-size: xx-large;">&nbsp;</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: xx-large; color: #333333;">-------</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: xx-large; color: #333333;">COMEDY SCRIPT READING</span>&nbsp;</div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><font size="3">Do you have a script that you think is nearly ready to send off? I offer professional script reading services for sitcom, comedy drama and sketches...</font></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><font color="#222222" face="arial, sans-serif" size="3"><a href="http://davecohen.org.uk/content/contact-me">http://davecohen.org.uk/content/contact-me</a></font></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;">-----------------------------------------------------------------------------------------------------------------------------------------------------</div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333; background-color: #ffff00;"><span style="font-size: xx-large;">COMEDY WRITING COURSES</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333; background-color: #ffff00;"><span style="font-size: xx-large;">NOVEMBER 2019</span></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; background-color: #ffff00;"><font size="6"></font><font color="#222222" size="2"><a href="http://davecohen.org.uk/comedy-writer-all-courses">http://davecohen.org.uk/comedy-writer-all-courses</a></font></span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="background-color: #ffff00;"><span style="background-color: #ffff00;"></span></span>
<p>-----------------------------------------------------------------------------------------------------------------------------------------------</p>
</div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: x-large; color: #333333;"><span style="font-size: xx-large;">SITCOM GEEKS - THE PODCAST</span></span><em>&nbsp; &nbsp; &nbsp;</em></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><b style="font-family: 'arial black', 'avant garde'; font-size: small;">Join me and James Cary every fortnight (for now) as we discuss everything you need to know about writing sitcom - and tons more...&nbsp;<a href="https://www.comedy.co.uk/podcasts/sitcom_geeks/">comedy.co.uk/podcasts/sitcom_geeks/</a>&nbsp;</b></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: small; text-align: center;"><b style="font-family: 'arial black', 'avant garde'; font-size: small;">Sign up to Patreon and HELP US CREATE A BRAND NEW SITCOM&nbsp;<a href="https://www.patreon.com/SitcomGeeks">https://www.patreon.com/SitcomGeeks</a></b></div>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;-----------------------------------------------------------------------</p>
<p style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: xx-large; line-height: 48px;"><span style="font-size: large;"></span></span><span style="font-family: 'arial black', 'avant garde'; font-size: large; line-height: 48px;">HORRIBLE HISTORIES - THE MOVIE opening 26 JULY...&nbsp;</span><span style="background-color: #ffffff; color: #222222; font-family: arial, sans-serif; font-size: small;"><span style="color: #333333; font-family: 'arial black', 'avant garde'; font-size: large; background-color: #f0f0f0;">HORRIBLE HISTORIES TV SERIES 8: COMING SOON</span></span></p>
</p>
<p><span style="line-height: 1.5;">Writer, comedian, Perrier nominee in a previous century, founder member of the Comedy Store Players - in the 1980s I was an averagely successful stand-up comedian. Hey, let's not be over-modest here - </span><strong style="line-height: 1.5;">Britain's&nbsp;Most Averagely Successful stand-up comedian.&nbsp;</strong></p>
<p style="text-align: left;">In recent years I've written for&nbsp;<a href="http://www.comedy.org.uk/guide/tv/hignfy/">Have I Got News For You</a>&nbsp;and&nbsp;<a href="http://www.comedy.org.uk/guide/tv/not_going_out">Not Going Out</a>, and as a comic songwriter,&nbsp;co-creating the Radio 4 hit&nbsp;<a href="http://www.comedy.org.uk/guide/radio/15_minute_musical/">15 Minute Musical</a>&nbsp;and writing loads of songs for the hit BBC Childrens' Show&nbsp;<u><a href="http://www.bbc.co.uk/cbbc/horriblehistories/">Horrible Histories</a></u>.&nbsp;<strong>&nbsp;(see link&nbsp;</strong><a href="/doh-re-me/"><strong>Doh Re Me</strong></a><strong>)</strong>&nbsp;also for the ITV topical comedy show 'Newzoids'. <strong>'Horrible Histories'&nbsp;</strong>was named Best Comedy by Childrens' BAFTAS 2010 and 2011, and Best Sketch Show two years running&nbsp;at the&nbsp;British Comedy Awards. And was nominated AGAIN at the 2014 British Comedy Awards. Here's a song about Charles II</p>
<p style="text-align: left;"><a href="https://www.youtube.com/watch?v=FA5abHKvUBQ" title="Charles II King of Bling">https://www.youtube.com/watch?v=FA5abHKvUBQ</a></p>
<p><span style="line-height: 1.5; font-size: 12px;">Shift your gaze rightwards for my me-nu.</span></p>
<div style="background: #eee; border: 1px solid #ccc; padding: 5px 10px;">...latest... HORRIBLE HISTORIES - THE MOVIE coming soon (ish)... HORRIBLE HISTORIES RETURNS TO CBBC JUNE 2019...&nbsp; now up to EIGHT Baftas... HAVE I GOT NEWS wins lifetime achievement...&nbsp; . SITCOM GEEKS BCG PODCASTS BRITISH COMEDY GUIDE... ebook of How To Be Averagely Successful At Comedy launched... latest...&nbsp;</div>
 </div></div></div>    </div>
  </div>
      <div class="region region-two-33-66-bottom region-conditional-stack">
      <div class="region-inner clearfix">
        <div class="field field-name-field-body-2 field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"> <h2>&nbsp;</h2>
<h2></h2>
 </div></div></div>      </div>
    </div>
  </div>
  </div>
</div>
  </div>
          </div>
        </div>
        <!-- //MAIN CONTENT -->
        
                <div id="sidebar-first-wrapper" class="span4">
          <div class="grid-inner clearfix">   <div class="region region-sidebar-first">
    <div id="block-blog-recent--2" class="block block-blog">

    <h2>Me-Mos</h2>
  
  <div class="content">
    <div class="item-list"><ul><li class="first"><a href="/the-monster-you-know">The Monster You Know</a></li>
<li><a href="/drama-out-of-a-comedy">Can You Make A Drama Out Of A Comedy?</a></li>
<li class="last"><a href="/about-that-idea">Day 2: About That Idea...</a></li>
</ul></div><div class="more-link"><a href="/blog" title="Read the latest blog entries.">More</a></div>  </div>
</div>
<div id="block-system-navigation--2" class="block block-system block-menu">

    <h2>Me-Nu</h2>
  
  <div class="content">
    <ul class="menu"><li class="first expanded"><a href="/comedy-writer-all-courses" title="">BE A COMEDY WRITER</a><ul class="menu"><li class="first last leaf"><a href="http://davecohen.org.uk/blog/basac#" title="">Be Averagely Successful At Comedy (Blog)</a></li>
</ul></li>
<li class="leaf"><a href="/doh-re-me" title="">Doh-Re-Me</a></li>
<li class="leaf active-trail"><a href="/content/dave-cohen-britains-most-averagely-successful-comedian" title="" class="active-trail active">Ho-Me</a></li>
<li class="leaf"><a href="/content/leave-me-links" title="">Leave Me (Links)</a></li>
<li class="leaf"><a href="/me-tube" title="">Me Tube</a></li>
<li class="leaf"><a href="/content/me-rchandise" title="">Me-rchandise</a></li>
<li class="leaf"><a href="/content/contact-me" title="">PROFESSIONAL SCRIPT READING</a></li>
<li class="leaf"><a href="/content/read-me" title="">Read Me</a></li>
<li class="leaf"><a href="/content/resu-me" title="">Resu-Me</a></li>
<li class="last leaf"><a href="/content/see-me-hear-me" title="">See-Me-Hear-Me</a></li>
</ul>  </div>
</div>
<div id="block-block-1--2" class="block block-block">

    
  <div class="content">
     <p><a class="twitter-timeline" href="https://twitter.com/SitcomGeeks?ref_src=twsrc%5Etfw">Tweets by SitcomGeeks</a> </p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>   </div>
</div>
  </div>
 </div>
        </div>
                      </div>
    </div>
  </div>
      <!-- FOOTER -->
  <div id="footer-wrapper" class="wrapper">
    <div class="container">
      <div class="row">
                <div class="span12 clearfix">
          <div id="footer" class="clearfix">   <div class="region region-footer">
    <div id="block-system-powered-by" class="block block-system">

    
  <div class="content">
    <span>Powered by <a href="https://www.drupal.org">Drupal</a></span>  </div>
</div>
<div id="block-block-2" class="block block-block">

    
  <div class="content">
     <p><strong>Dave Cohen - Comedian With A Guitar</strong></p>
   </div>
</div>
  </div>
 </div>
        </div>
      </div>
    </div>
  </div>
  <!-- //FOOTER -->
  </div>
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");



//--><!]]>
</script>
</body>
</html>
