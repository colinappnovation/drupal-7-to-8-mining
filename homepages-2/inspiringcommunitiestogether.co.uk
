<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <meta charset="UTF-8">
        <title>Home - Inspiring Communities Together</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>document.documentElement.classList.remove('no-js');</script>
        <link rel="shortcut icon" href="https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/favicon.ico">

        
<!-- This site is optimized with the Yoast SEO plugin v8.2 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://www.inspiringcommunitiestogether.co.uk/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Home - Inspiring Communities Together" />
<meta property="og:url" content="https://www.inspiringcommunitiestogether.co.uk/" />
<meta property="og:site_name" content="Inspiring Communities Together" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Home - Inspiring Communities Together" />
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.inspiringcommunitiestogether.co.uk\/","name":"Inspiring Communities Together","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.inspiringcommunitiestogether.co.uk\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' type='text/css' media='all' />
<link rel='stylesheet' id='main-css'  href='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/styles/dist/main.css?ver=7d80f59' type='text/css' media='all' />
<script async='async' src='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/bower_components/lazysizes/plugins/unveilhooks/ls.unveilhooks.min.js'></script>
<script async='async' src='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/bower_components/lazysizes/lazysizes.min.js'></script>
<link rel='https://api.w.org/' href='https://www.inspiringcommunitiestogether.co.uk/wp-json/' />
<link rel='shortlink' href='https://www.inspiringcommunitiestogether.co.uk/' />
<link rel="alternate" type="application/json+oembed" href="https://www.inspiringcommunitiestogether.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.inspiringcommunitiestogether.co.uk%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.inspiringcommunitiestogether.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.inspiringcommunitiestogether.co.uk%2F&#038;format=xml" />

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126402500-1"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-126402500-1');
        </script>
            </head>

    <body class="body--home">
        <!--[if lt IE 10]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <header class="c-header">
        <div class="c-header__wrap">
            <div class="c-header__logo">
                <a class="c-header__brand" href="https://www.inspiringcommunitiestogether.co.uk">Inspiring Communities Together</a>
            </div>

            <a class="c-header__toggle">
                <span class="c-header__toggle__button">
                    <span class="c-header__toggle__burger"></span>
                </span>
            </a>

            <div class="c-navigation">

                <ul class="c-navigation__list"><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/our-team/" class="c-navigation__link">
                                                                    Our Team
                                                                </a></li><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/events/" class="c-navigation__link">
                                                                    Events / What&#039;s on
                                                                </a></li><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/" class="c-navigation__link">
                                                                    What We Do
                                                                </a></li><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/get-involved/" class="c-navigation__link">
                                                                    Get Involved
                                                                </a></li><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/news/" class="c-navigation__link">
                                                                    News
                                                                </a></li><li class="c-navigation__item">
                                                                <a href="https://www.inspiringcommunitiestogether.co.uk/contact-us/" class="c-navigation__link">
                                                                    Contact us
                                                                </a></li>
                        <li class="c-navigation__item c-navigation__item--phone">
                            <a class="c-navigation__link c-navigation__link--number" href="tel:01617433625">0161 743 3625</a>
                        </li></ul>            </div>
        </div>
    </header>
	<div class="c-hero__slider c-fade-in">
					<div class="c-hero">
				<div class="c-hero__image c-fade-in c-object-fit">
					<img width="4446" height="2501" class="c-team__post-image lazyload lazyload" alt="" data-src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/Inspiring-Communities-Together-074-N586.jpg" data-srcset="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/Inspiring-Communities-Together-074-N586.jpg 4446w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/Inspiring-Communities-Together-074-N586-300x169.jpg 300w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/Inspiring-Communities-Together-074-N586-768x432.jpg 768w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/Inspiring-Communities-Together-074-N586-1024x576.jpg 1024w" data-sizes="(max-width: 4446px) 100vw, 4446px" />				</div>
			</div>
					<div class="c-hero">
				<div class="c-hero__image c-fade-in c-object-fit">
					<img width="3720" height="1248" class="c-team__post-image lazyload lazyload" alt="" data-src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/inspiring-communities-together-097-n-586-web@2x-min.jpg" data-srcset="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/inspiring-communities-together-097-n-586-web@2x-min.jpg 3720w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/inspiring-communities-together-097-n-586-web@2x-min-300x101.jpg 300w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/inspiring-communities-together-097-n-586-web@2x-min-768x258.jpg 768w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/inspiring-communities-together-097-n-586-web@2x-min-1024x344.jpg 1024w" data-sizes="(max-width: 3720px) 100vw, 3720px" />				</div>
			</div>
					<div class="c-hero">
				<div class="c-hero__image c-fade-in c-object-fit">
					<img width="1200" height="900" class="c-team__post-image lazyload lazyload" alt="" data-src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/problem-solving-in-tech-and-tea.jpg" data-srcset="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/problem-solving-in-tech-and-tea.jpg 1200w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/problem-solving-in-tech-and-tea-300x225.jpg 300w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/problem-solving-in-tech-and-tea-768x576.jpg 768w, https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/problem-solving-in-tech-and-tea-1024x768.jpg 1024w" data-sizes="(max-width: 1200px) 100vw, 1200px" />				</div>
			</div>
			</div>
<div class="o-page__small-green-dots">
<div class="c-banner c-banner--whats-on c-banner--frontpage">
    <div class="c-banner__wrap">
        <div class="c-banner__heading">
            What's on
        </div>
        <div class="c-banner__content">
            We're helping make a difference in Charlestown and<br> Lower Kersal. Find out how you can be involved.
        </div>
        <div class="c-banner__cta">
            <a class="c-banner__button" href="https://www.inspiringcommunitiestogether.co.uk/events/">View events</a>
        </div>
    </div>
</div>


	<section class="c-zigzag">
		<div class="c-zigzag__wrap">
											<article class="c-zigzag__item c-zigzag__item--n1">
                    <div class="c-zigzag__item__image">
                                                <img class="" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/start-well.png" alt="Start Well">
                                            </div>
					<div class="c-zigzag__item__info">
                        
						<h2 class="c-zigzag__item__heading">Start Well</h2>

						<p><img class="post-img alignnone size-medium" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2019/02/Ofsted_Good_GP_Colour-300x300.jpg" alt="" /></p>
<p>Inspiring Communities Together is committed to ensure all children in Charlestown and Lower Kersal have access to the best possible start in life.</p>
<p>Our childcare provision, Yogurt Pots, based in Salford Sports Village provides good quality child care by local people (100% of the staff employed in childcare live in Charlestown or Lower Kersal). The service has been consistently rated Good by Ofsted and offers both paid and funded places for those who are eligible.</p>

                        						<p class="c-zigzag__item__link"><a href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/start-well/">Read More</a></p>
                        					</div>
				</article>
												<article class="c-zigzag__item c-zigzag__item--n2">
                    <div class="c-zigzag__item__image">
                                                <img class="" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/live-well.png" alt="Live Well">
                                            </div>
					<div class="c-zigzag__item__info">
                        
						<h2 class="c-zigzag__item__heading">Live Well</h2>

						<p><img class="post-img alignnone size-medium" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/12/ASDAN_RegisteredCentre_logo_colour_web-300x69.jpg" alt="" /></p>
<p>Our live well programme of work provides opportunities for local people to develop their skills and knowledge as well as offer the skills and knowledge they have about where they live to help make their neighbourhood a better place to live.</p>
<p>Our team of Development Workers, most of whom are themselves local residents work with local people to build on their own strengths to develop projects and facilitate connections with other services being offered within the area.</p>

                        						<p class="c-zigzag__item__link"><a href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/live-well/">Read More</a></p>
                        					</div>
				</article>
												<article class="c-zigzag__item c-zigzag__item--n3">
                    <div class="c-zigzag__item__image">
                                                <img class="" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2018/09/age-well.png" alt="Age Well">
                                            </div>
					<div class="c-zigzag__item__info">
                        
						<h2 class="c-zigzag__item__heading">Age Well</h2>

						<p><img class="post-img alignnone size-medium" src="https://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2019/02/Age-Friendly-Mark-01-300x176.jpg" alt="" /></p>
<p>Our age well programme of work helps older people feel more connected with their community. Our work is part of the wider Salford City commitment to ensuring our older residents can live healthy happy lives in their own homes for as long as they choose and access support close to home when they need it.</p>

                        						<p class="c-zigzag__item__link"><a href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/age-well/">Read More</a></p>
                        					</div>
				</article>
													</div>
	</section>


	<div class="c-content-video">
		<div class="c-content-video__wrap">
			<iframe width="640" height="360" src="https://www.youtube.com/embed/-SDPv-IvLyM?start=2&feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>		</div>
	</div>

</div>
	<div class="c-banner c-banner--member c-banner--frontpage">
		<div class="c-banner__wrap">
			<div class="c-banner__heading">
				Join us
			</div>
			<div class="c-banner__content">
				Do you live or work in Charlestown or Lower Kersal?<br>
                <strong>Why not become a member and help us with our work?</strong>
			</div>
			<div class="c-banner__cta">
				<a class="c-banner__button" href="https://www.inspiringcommunitiestogether.co.uk/get-involved/">Become a member</a>
			</div>
		</div>
	</div>
	<section class="c-twitter-feed">
        <div class="c-twitter-feed__wrap">

            <h2 class="c-twitter-feed__heading"><span>Tweets</span> by <a href="https://twitter.com/ictsalford">@ictsalford</a></h2>

            <div class="c-twitter-feed__list">

                                            <div class="c-twitter-feed__item">
                    Roasted butternut squash and cajun spice soup, with some home made biscuits yummy yummy <a target="_new" href="http://twitter.com/search?q=keepingwarm" class="c-twitter__tweet-tag">#keepingwarm</a> <a target="_new" href="http://twitter.com/search?q=homemade" class="c-twitter__tweet-tag">#homemade</a> <a target="_blank" href="https://t.co/ceQcEFYucM" class="c-twitter__tweet-link">https://t.co/ceQcEFYucM</a>                </div>
                                <div class="c-twitter-feed__item">
                    Day 3 of common purpose what an absolute amazing experience of learning, working with new people visiting new organ… <a target="_blank" href="https://t.co/st7CUovC0q" class="c-twitter__tweet-link">https://t.co/st7CUovC0q</a>                </div>
                                <div class="c-twitter-feed__item">
                    Thank you so much to our student nurse Jess, for your very informative presentation on the importance of the flu ja… <a target="_blank" href="https://t.co/3n6RjRyp1b" class="c-twitter__tweet-link">https://t.co/3n6RjRyp1b</a>                </div>
                            
            </div>

        </div>
	</section>

	<div class="c-panel-contact">
		<span class="c-panel-contact__icon">
			<img src="https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/assets/svg/icons/get-in-touch.svg" alt="Get in touch">
		</span>

		<h3 class="c-panel-contact__heading">Get in touch</h3>

		<p class="c-panel-contact__content">
			E-mail us at <a href="mailto:office@inspiringcommunitiestogether.co.uk">office@inspiringcommunitiestogether.co.uk</a><br>
			 or telephone on <a href="tel:01617433625">0161 743 3625</a><br>
			<span class="c-panel-contact__content__">We look forward to hearing from you!</span>
		</p>

	</div>
<div class="c-panel-dual">
	<div class="c-panel-download c-panel-download--newsletter">
		<span class="c-panel-download__icon">
			<img src="https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/assets/svg/icons/newsletter.svg" alt="What's on guide">
		</span>

		<h3 class="c-panel-download__heading">What's On Guide</h3>

		<p class="c-panel-download__content">
			<a target="_blank" href="http://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2019/07/A4-4pp-events-guide-summer-2019.pdf">Click here</a> for the latest edition of our Inspiring Communities Together What's On Guide
		</p>

	</div>
    <div class="c-panel-download c-panel-download--report">
        <span class="c-panel-download__icon">
            <img src="https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/assets/svg/icons/annual-report.svg" alt="Annual report">
        </span>

        <h3 class="c-panel-download__heading">Annual Report</h3>

        <p class="c-panel-download__content">
            <a target="_blank" href="http://www.inspiringcommunitiestogether.co.uk/wp-content/uploads/2019/07/Annual-Report-2019.pdf">Click here</a> to download your copy of the 2018/2019 annual report
        </p>

    </div>
</div>
    <footer class="c-footer">
        <div class="c-footer__wrap">

            <div class="c-footer__top">
                <div class="c-footer__left">
	                                    <nav class="c-footer-nav">
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/our-team/">Our Team</a>
                                </div>
                                                                <ul class="c-footer-nav__children">
                                    <li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/our-team/our-history/">Our History</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/our-team/our-objectives/">Our Objectives</a></li>                                </ul>
                                                            </div>
                        </div>
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/get-involved/">Get Involved</a>
                                </div>
                                                                <ul class="c-footer-nav__children">
                                    <li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/get-involved/job-vacancies/">Job Vacancies</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/get-involved/volunteering-opportunities/">Volunteering Opportunities</a></li>                                </ul>
                                                            </div>
                        </div>
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/events/">What&#039;s on</a>
                                </div>
                                                            </div>
                        </div>
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/news/">News</a>
                                </div>
                                                            </div>
                        </div>
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/">What We Do</a>
                                </div>
                                                                <ul class="c-footer-nav__children">
                                    <li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/start-well/">Start Well</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/live-well/">Live Well</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/age-well/">Age Well</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/what-we-do/salford-and-beyond/">Salford and Beyond</a></li>                                </ul>
                                                            </div>
                        </div>
                                                <div class="c-footer-nav__wrap">
                            <div class="c-footer-nav__item">
                                <div class="c-footer-nav__item__title">
                                    <a href="https://www.inspiringcommunitiestogether.co.uk/contact-us/">Contact us</a>
                                </div>
                                                                <ul class="c-footer-nav__children">
                                    <li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/privacy-policy/">Privacy Policy</a></li><li class="c-footer-nav__children__item"><a class="c-footer-nav__children__link" href="https://www.inspiringcommunitiestogether.co.uk/terms-and-conditions/">Terms and Conditions</a></li>                                </ul>
                                                            </div>
                        </div>
                                            </nav>
	                                                        <div class="c-footer-address">
                        <div class="c-footer-address__title">How to find us</div>
                        <div class="c-footer-address__details">
                        Inspiring Communities Together<br>Innovation Forum<br>Frederick Road<br>Salford<br>M6 6FP<br>                        </div>
                    </div>
                                    </div>
                <div class="c-footer__right">
	                                        <div class="c-footer-social__heading">Follow us:</div>
                        <ul class="c-footer-social">
			                                                <li class="c-footer-social__item c-footer-social__item--facebook">
                                    <a target="_blank" href="https://www.facebook.com/ictchalk" title="Facebook : ictsalford">
						                                                    </a>
                                </li>
			                                                <li class="c-footer-social__item c-footer-social__item--twitter">
                                    <a target="_blank" href="https://twitter.com/ictsalford" title="Twitter : ictsalford">
						                                                    </a>
                                </li>
			                                        </ul>
	                                </div>
            </div>

            <div class="c-footer__end">
                <div class="c-footer__left">
                    Inspiring Communities Together is a Charitable Incorporated Organisation, registration number 1157053
                </div>
                <div class="c-footer__right">
                    <a class="c-footer__carbon" href="https://www.carboncreative.net/" target="_blank">Website Design Manchester</a> by Carbon Creative
                </div>
            </div>

        </div>
    </footer>


        <script src='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/bower_components/jquery/dist/jquery.min.js?ver=3.3.1'></script>
<script src='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/bower_components/slick-carousel/slick/slick.min.js?ver=1.8.1'></script>
<script async='async' src='https://www.inspiringcommunitiestogether.co.uk/wp-content/themes/inspiringcommunities/js/dist/main.js?ver=7d80f59'></script>
<script async='async' src='https://www.inspiringcommunitiestogether.co.uk/wordpress/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
    </body>
</html>
