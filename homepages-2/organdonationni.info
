<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="apple-touch-icon" href="https://www.organdonationni.info/sites/default/files/Apple-Touch-Icons.png" type="image/png" />
<link rel="shortcut icon" href="https://www.organdonationni.info/sites/default/files/Apple-Touch-Icons_0.png" type="image/png" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<meta name="description" content="Every year, around 15 people in Northern Ireland die waiting for an organ transplant. By signing the register and talking to your family about your wishes you could help save the lives of up to seven people." />
<meta name="abstract" content="Organ donation gives the precious gift of life to others. Let us explain exactly what’s involved, how you can register and dispel any myths you might have heard." />
<meta name="keywords" content="organ donation, organ donor register, nhs, northern ireland, ni, register, transplants, organ transplant" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.organdonationni.info/" />
<meta name="rights" content="Public Health Agency" />
<link rel="shortlink" href="https://www.organdonationni.info/" />
<meta name="dcterms.title" content="Organ Donation Northern Ireland" />
<meta name="dcterms.creator" content="Stephen Cousins" />
<meta name="dcterms.subject" content="Organ donation" />
<meta name="dcterms.publisher" content="Public Health Agency" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.identifier" content="https://www.organdonationni.info/" />
<meta name="dcterms.format" content="text/html" />
  <title>Organ Donation Northern Ireland |</title>  
  <link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_vZ_wrMQ9Og-YPPxa1q4us3N7DsZMJa-14jShHgRoRNo.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_hYCLW089C9S9sP3ZYkuG6R-Q5ZHbEhblZBFjwZ_bE_I.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_581PNnG9bWto2RYCffqw_cXFhCaV7l_-86WfOOGg7Eo.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_PseM54vd7tdTkVwB6ITbslGJI8W6F1ktaBCJIsDFOj4.css" media="print" />
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_3uyV-erSogWsC6M3NLOKbQoJANG_mRWBZpdUtaBenyU.css" media="all" />

<!--[if (lt IE 9)&(!IEMobile)]>
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_hxDzeWYTQqq5a7pFed-trAcrTIFXWBW9fLUXuQ-xpuM.css" media="all" />
<![endif]-->

<!--[if gte IE 9]><!-->
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_lUhAO-6n9fQQUJNnmlZVLOEBmM5p17mo2wWtCX2CyKU.css" media="all" />
<!--<![endif]-->

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.organdonationni.info/sites/default/files/css/css_9BoOYXTSrSqZ6O7Vf5xsEjVLgzHTjtFHSafibHVqefY.css" media="all" />
<![endif]-->
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='/sites/all/modules/contrib/jquery_update/replace/jquery/1.7/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_dWhBODswdXXk1M5Z5nyqNfGljmqwxUwAK9i6D0YSDNs.js"></script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_obFUyTbqtcoGQtszVI46l1_oD927PzlDtlr_ilpYBqE.js"></script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_SOzrMB1iCuYxJNwJj3P3YOPnLbC_75cpSCSXXa8TbmI.js"></script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_7qeIP5x1pp65EYN9l3ZhemvVP_ivW4oh0RbOXRf7TX4.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var switchTo5x = false;var __st_loadLate = true;
//--><!]]>
</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
if (stLight !== undefined) { stLight.options({"publisher":"dr-c3e09a3b-fd51-3d0f-938-d610462b30a3","version":"4x"}); }
//--><!]]>
</script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_xI_XRBJXC9e5IFQC5_BEW-HpViMPNCdnT682VdwwabI.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create", "UA-1723473-23", {"cookieDomain":".organdonationni.info"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://use.typekit.com/swj7par.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
try{Typekit.load();}catch(e){}
//--><!]]>
</script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_4F5jej5kDQ7U9MW1jUrjjESxfxLh23wwuPIyGwHYk6E.js"></script>
<script type="text/javascript" src="https://www.organdonationni.info/sites/default/files/js/js_43n5FBy8pZxQHxPXkf-sQF7ZiacVZke14b0VlvSA554.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"odni","theme_token":"Mi5ivgkQv1wIV7dK1BX9BnIw_lg5e9vjr30zhVFlP-g","js":{"0":1,"1":1,"2":1,"3":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.7.2\/jquery.min.js":1,"4":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/modules\/contrib\/cookiecontrol\/js\/cookieControl-5.1.min.js":1,"sites\/all\/modules\/custom\/odni_nhsbt\/js\/odni_nhsbt.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.js":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"5":1,"https:\/\/ws.sharethis.com\/button\/buttons.js":1,"6":1,"sites\/all\/modules\/contrib\/responsive_menus\/styles\/responsive_menus_simple\/js\/responsive_menus_simple.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"7":1,"https:\/\/use.typekit.com\/swj7par.js":1,"8":1,"sites\/all\/themes\/ODNI\/js\/odni.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/views_slideshow.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/custom\/odni_campaign\/campaign.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/modules\/contrib\/responsive_menus\/styles\/responsive_menus_simple\/css\/responsive_menus_simple.css":1,"sites\/all\/themes\/ODNI\/css\/print.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/default\/files\/fontyourface\/font.css":1,"sites\/all\/themes\/ODNI\/css\/global.css":1,"ie::normal::sites\/all\/themes\/ODNI\/css\/odni-alpha-default.css":1,"ie::normal::sites\/all\/themes\/ODNI\/css\/odni-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-24.css":1,"narrow::sites\/all\/themes\/ODNI\/css\/odni-alpha-default.css":1,"narrow::sites\/all\/themes\/ODNI\/css\/odni-alpha-default-narrow.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-24.css":1,"normal::sites\/all\/themes\/ODNI\/css\/odni-alpha-default.css":1,"normal::sites\/all\/themes\/ODNI\/css\/odni-alpha-default-normal.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-24.css":1,"wide::sites\/all\/themes\/ODNI\/css\/odni-alpha-default.css":1,"wide::sites\/all\/themes\/ODNI\/css\/odni-alpha-default-wide.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-24.css":1,"sites\/all\/themes\/ODNI\/css\/ie8.css":1}},"viewsSlideshow":{"statistics-block":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_statistics-block":{"num_divs":5,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"statistics-block","effect":"fade","transition_advanced":0,"timeout":5000,"speed":700,"delay":0,"sync":1,"random":0,"pause":0,"pause_on_click":0,"action_advanced":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":3000,"cleartype":1,"cleartypenobg":0,"advanced_options":"{}"}},"responsive_menus":[{"toggler_text":"\u2630 Menu","selectors":[".menu-block-1"],"media_size":"768","media_unit":"px","absolute":"1","remove_attributes":"1","responsive_menus_style":"responsive_menus_simple"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackDomainMode":"1"},"urlIsAjaxTrusted":{"\/":true},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>
</script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script>
</head>
<body class="html front not-logged-in page-node page-node- page-node-1 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix container-12">
    <div class="grid-6 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="branding-data clearfix">
                        <hgroup class="site-name-slogan">        
                                
        <h1 class="site-name"><a href="/" title="Home" class="active">Organ Donation Northern Ireland</a></h1>
                              </hgroup>
          </div>
          </div>
</div><div class="grid-6 region region-search" id="region-search">
  <div class="region-inner region-search-inner">
    <div class="block block-search block-form block-search-form odd block-without-title" id="block-search-form">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search this site </label>
 <input title="Enter the terms you wish to search for." placeholder="Search this site" type="text" id="edit-search-block-form--2" name="search_block_form" value="Search this site" size="18" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="image" id="edit-submit" name="submit" src="/sites/all/themes/ODNI/images/search.png" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-y8aGvM52keJYXR1F_GcG8tcDz_vBTzxrVa1kvG3wF7Y" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-header-wrapper" class="zone-wrapper zone-header-wrapper clearfix">  
  <div id="zone-header" class="zone zone-header clearfix container-12">
    <div class="grid-12 region region-header" id="region-header">
  <div class="region-inner region-header-inner">
    <div class="block block-odni-campaign block-campaign block-odni-campaign-campaign odd block-without-title" id="block-odni-campaign-campaign">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div id="odni-campaign-text">
  <h2>Speak up and save a life</h2>
  <p>The next time you're talking to your family tell them you want to be an organ donor.</p>
  <div class="mobile"><a href="https://www.youtube.com/watch?v=WOMOp1Q2YqM">View the organ donation campaign on YouTube.</a></div>
</div>
<div id="odni-campaign-video">
  <iframe width="100%" height="100%" src="//www.youtube.com/embed/WOMOp1Q2YqM?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-24">    
        
        <aside class="grid-5 region region-sidebar-first" id="region-sidebar-first">
  <div class="region-inner region-sidebar-first-inner">
    <div class="block block-menu-block block-1 block-menu-block-1 odd block-without-title" id="block-menu-block-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="menu-block-wrapper menu-block-1 menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first leaf active-trail active menu-mlid-466"><a href="/homepage" class="active-trail active">Home</a></li>
<li class="collapsed menu-mlid-629"><a href="/did-you-know">Did you know?</a></li>
<li class="collapsed menu-mlid-631"><a href="/how-register">How to register</a></li>
<li class="leaf menu-mlid-642"><a href="/talk-about-your-wishes">Talk about your wishes</a></li>
<li class="leaf menu-mlid-1206"><a href="/testimonials" title="">Testimonials</a></li>
<li class="leaf menu-mlid-991"><a href="/resources">Resources</a></li>
<li class="last leaf menu-mlid-1716"><a href="/register/online" title="">Register to be a donor</a></li>
</ul></div>
    </div>
  </div>
</div><div class="block block-sharethis block-sharethis-block block-sharethis-sharethis-block even block-without-title" id="block-sharethis-sharethis-block">
  <div class="block-inner clearfix">
            
    <div class="content clearfix">
      <h5>Show your support with friends and followers</h5>
      <div class="sharethis-wrapper"><span st_url="https://www.organdonationni.info/" st_title="Homepage" class="st_facebook_large" displayText="facebook"></span>
<span st_url="https://www.organdonationni.info/" st_title="Homepage" class="st_twitter_large" displayText="twitter"></span>
<span st_url="https://www.organdonationni.info/" st_title="Homepage" class="st_googleplus_large" displayText="googleplus"></span>
</div>    </div>
  </div>
</div>
  </div>
</aside><div class="grid-10 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
            <div class="element-invisible">    <h1 class="title" id="page-title">Homepage</h1>
    </div>                    <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <article about="/homepage" typeof="foaf:Document" class="node node-page node-published node-not-promoted node-not-sticky author-feargal odd clearfix" id="node-page-1">
        <span property="dc:title" content="Homepage" class="rdf-meta element-hidden"></span>    
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><h2>Have you thought about it?</h2>
<p>Most people would accept an organ if they needed one. Not surprising really. Yet only 46% of us have signed the NHS Organ Donor Register.</p>
<p>Last year nine people died in Northern Ireland waiting for an organ transplant. By signing the register and talking to your family about your wishes you could help save the lives of up to nine people.</p>
</div></div></div>  </div>
  
  <div class="clearfix">
          <nav class="links node-links clearfix"></nav>
    
      </div>
</article>    </div>
  </div>
</div><section class="block block-bean block-homepage---how-to-register-block block-bean-homepage-how-to-register-block even" id="block-bean-homepage-how-to-register-block">
  <div class="block-inner clearfix">
              <h2 class="block-title">Sign the Organ Donor Register</h2>
        
    <div class="content clearfix">
      <div class="entity entity-bean bean-how-to-register clearfix" about="/block/homepage---how-to-register-block" typeof="" class="entity entity-bean bean-how-to-register">

  <div class="content">
    <div class="field field-name-field-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>There are lots of simple ways to register. Or maybe you need to check if you are already registered.<br />Find out how to and what happens afterwards.</p>
</div></div></div><div class="field field-name-field-register-online-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.organdonationni.info/register">Online</a></div></div></div><div class="field field-name-field-register-phone-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.organdonationni.info/how-register">By phone</a></div></div></div><div class="field field-name-field-register-post-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.organdonationni.info/how-register">By post</a></div></div></div>  </div>
</div>
    </div>
    <a href="/register" class="register-link"><span class="icon"></span>Register now</a>
  </div>
</section>
<section class="block block-bean block-homepage---talk-to-your-family block-bean-homepage-talk-to-your-family odd" id="block-bean-homepage-talk-to-your-family">
  <div class="block-inner clearfix">
              <h2 class="block-title">Talk about your wishes</h2>
            
    <div class="content clearfix">
      <div class="entity entity-bean bean-rich-content-with-link-block clearfix" about="/block/homepage---talk-to-your-family" typeof="" class="entity entity-bean bean-rich-content-with-link-block">

  <div class="content">
    <div class="field field-name-field-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>The death of someone close can be a difficult time. <a href="http://organdonationni.info/talk-about-your-wishes">Telling your family or friends </a>you want to be an organ donor means they will know what to do if the time comes.</p>
</div></div></div>  </div>
</div>
    </div>
  </div>
</section><div class="block block-views block-statistics-block block-views-statistics-block even block-without-title" id="block-views-statistics-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-statistics view-id-statistics view-display-id-block view-dom-id-4ed9fa980a065b15fc65ba6e3074482d">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
    <div id="views_slideshow_cycle_main_statistics-block" class="views_slideshow_cycle_main views_slideshow_main"><div id="views_slideshow_cycle_teaser_section_statistics-block" class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
  <div id="views_slideshow_cycle_div_statistics-block_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
    
  <div class="views-field views-field-field-percentage">        46%  </div>  
  <div class="views-field views-field-quote">        of the population in Northern Ireland are on the Organ Donor Register  </div></div>
</div>
<div id="views_slideshow_cycle_div_statistics-block_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-1 views-row-even">
    
  <div class="views-field views-field-field-number">        9  </div>  
  <div class="views-field views-field-quote">        people died last year in Northern Ireland waiting for an organ transplant.  </div></div>
</div>
<div id="views_slideshow_cycle_div_statistics-block_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-2 views-row-odd">
    
  <div class="views-field views-field-field-percentage">        93%  </div>  
  <div class="views-field views-field-quote">        of families agree to donation if they know their loved ones’ wishes  </div></div>
</div>
<div id="views_slideshow_cycle_div_statistics-block_3" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-3 views-row-even">
    
  <div class="views-field views-field-field-percentage">        84%  </div>  
  <div class="views-field views-field-quote">        of people in Northern Ireland are in support of organ donation  </div></div>
</div>
<div id="views_slideshow_cycle_div_statistics-block_4" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-5 views_slideshow_cycle_hidden views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-4 views-row-odd views-row-last">
    
  <div class="views-field views-field-field-percentage">        51%  </div>  
  <div class="views-field views-field-quote">        of families agree to donation if they don&#039;t knwo their loved one&#039;s wishes  </div></div>
</div>
</div>
</div>
      </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div>
</div>      </div>
</div><aside class="grid-9 region region-sidebar-second" id="region-sidebar-second">
  <div class="region-inner region-sidebar-second-inner">
    <section class="block block-bean block-homepage---what-is-organ-donatio block-bean-homepage-what-is-organ-donatio odd" id="block-bean-homepage-what-is-organ-donatio">
  <div class="block-inner clearfix">
              <h2 class="block-title">What is organ donation?</h2>
            
    <div class="content clearfix">
      <div class="entity entity-bean bean-rich-content-with-link-block clearfix" about="/block/homepage---what-is-organ-donatio" typeof="" class="entity entity-bean bean-rich-content-with-link-block">

  <div class="content">
    <div class="field field-name-field-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>Organ donation gives the precious gift of life to others. Let us explain exactly what’s involved, <a href="/how-register">how you can register</a> and dispel any myths you might have heard.</p>
</div></div></div>  </div>
</div>
    </div>
  </div>
</section><section class="block block-bean block-homepage---where-are-we-now block-bean-homepage-where-are-we-now even" id="block-bean-homepage-where-are-we-now">
  <div class="block-inner clearfix">
              <h2 class="block-title">Where are we now?</h2>
            
    <div class="content clearfix">
      <div class="entity entity-bean bean-rich-content-with-link-block clearfix" about="/block/homepage---where-are-we-now" typeof="" class="entity entity-bean bean-rich-content-with-link-block">

  <div class="content">
    <div class="field field-name-field-body field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>With only 46% of the population in Northern Ireland on the NHS Organ Donor Register, and the current system under review, <a href="/where-are-we-now">find out what’s been happening with organ donation</a>.</p>
</div></div></div>  </div>
</div>
    </div>
  </div>
</section><div class="block block-views block-testimonials-block block-views-testimonials-block odd block-without-title" id="block-views-testimonials-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-testimonials view-id-testimonials view-display-id-block view-dom-id-ff1a1ae8a55e4acda746f690888ed4de">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-testimonial-photo">        <div class="field-content"><img typeof="foaf:Image" src="https://www.organdonationni.info/sites/default/files/styles/testimonial_photo/public/testimonials/images/Lucia%20Mee%20Quiney%20pic.jpg?itok=qNmCY8tY" width="120" height="120" alt="" /></div>  </div>  
  <div class="testimonial-text">        <span><h3>Lucia</h3>
<h4>from Ballycastle</h4>
<p>To people who are not already on the Organ Donor Register, I would say: “Please do it!”</p>
 </span>  </div>  </div>
    </div>
  
  
  
  
      <div class="view-footer">
      <p><a href="/testimonials/7">Read Lucia's full story</a></p>
    </div>
  
  
</div>    </div>
  </div>
</div>  </div>
</aside>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-12 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <div class="block block-menu block-menu-footer-menu block-menu-menu-footer-menu odd block-without-title" id="block-menu-menu-footer-menu">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="menu"><li class="first leaf"><a href="/accessibility">Accessibility</a></li>
<li class="leaf"><a href="/cookies">Cookies</a></li>
<li class="leaf"><a href="/privacy">Privacy Policy</a></li>
<li class="last leaf"><a href="/terms-use-and-disclaimer">Terms of use and disclaimer</a></li>
</ul>    </div>
  </div>
</div><div class="block block-block block-1 block-block-1 even block-without-title" id="block-block-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="http://www.publichealth.hscni.net" id="pha-logo">Public Health Agency</a></p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-12 region region-footer-second" id="region-footer-second">
  <div class="region-inner region-footer-second-inner">
    <div class="block block-block block-2 block-block-2 odd block-without-title" id="block-block-2">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>© Copyright 2013. Designed and produced by the Public Health Agency</p>
    </div>
  </div>
</div><div class="block block-doubleclick-tags block-doubleclick-tags block-doubleclick-tags-doubleclick-tags even block-without-title" id="block-doubleclick-tags-doubleclick-tags">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <!-- Start of DoubleClick Floodlight Tag -->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
var src = 3371523;
var type = 'organ919';
var cat = 'organ034';
document.write('<iframe src="https://' + src + '.fls.doubleclick.net/activityi;src=' + src + ';type=' + type + ';cat=' + cat + ';ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://3371523.fls.doubleclick.net/activityi;src=3371523;type=organ919;cat=organ034;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe></noscript>
<!-- End of DoubleClick Floodlight Tag -->
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <script type="text/javascript">
<!--//--><![CDATA[//><!--

    jQuery(document).ready(function($) {
    cookieControl({
        introText: '<p>This site uses cookies to store information on your computer. Please see our <a href=\"/privacy-policy\">privacy policy</a> for more information.</p>',
        fullText: '<p>Some cookies on this site are essential, and the site won\'t work as expected without them. These cookies are set when you submit a form, login or interact with the site by doing something that goes beyond clicking on simple links.</p><p>We also use some non-essential cookies to anonymously track visitors or enhance your experience of the site. If you\'re not happy with this, we won\'t set these cookies but some nice features of the site may be unavailable.</p><p>By using our site you accept the terms of our <a href="/privacy">Privacy Policy</a>.',
        theme: 'dark',
        html: '<div id="cccwr"><div id="ccc-state" class="ccc-pause"><div id="ccc-icon"><button><span>Cookie Control</span></button></div><div class="ccc-widget"><div class="ccc-outer"><div class="ccc-inner"><h2>Cookie Control</h2><div class="ccc-content"><p class="ccc-intro"></p><div class="ccc-expanded"></div><div id="ccc-cookies-switch" style="background-position-x: 0;"><a id="cctoggle" href="#" style="background-position-x: 0;" name="cctoggle"><span id="cctoggle-text">Cookies test</span></a></div><div id="ccc-implicit-warning">(One cookie will be set to store your preference)</div><div id="ccc-explicit-checkbox"><label><input id="cchide-popup" type="checkbox" name="ccc-hide-popup" value="Y" /> Do not ask me again<br /></label> (Ticking this sets a cookie to hide this popup if you then hit close. This will not store any personal information)</div><p class="ccc-about"><small><a href="http://www.civicuk.com/cookie-law" target="_blank">About this tool</a></small></p><a class="ccc-icon" href="http://www.civicuk.com/cookie-law" target="_blank"title="About Cookie Control">About Cookie Control</a><button class="ccc-close">Close</button></div></div></div><button class="ccc-expand">read more</button></div></div></div>',
        position: 'left',
        shape: 'triangle',
        startOpen: true,
        autoHide: 60000,
        onAccept: function(cc){cookiecontrol_accepted(cc)},
        onReady: function(){},
        onCookiesAllowed: function(cc){cookiecontrol_cookiesallowed(cc)},
        onCookiesNotAllowed: function(cc){cookiecontrol_cookiesnotallowed(cc)},
        countries: '',
        subdomains: true,
        cookieName: 'organ-donation-northern-ireland_cookiecontrol',
        iconStatusCookieName: 'ccShowCookieIcon',
        consentModel: 'information_only'
        });
      });
    
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_accepted(cc) {
cc.setCookie('ccShowCookieIcon', 'no');jQuery('#ccc-icon').hide();
}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_cookiesallowed(cc) {

}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_cookiesnotallowed(cc) {

}
//--><!]]>
</script>
</body>
</html>
