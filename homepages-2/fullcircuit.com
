<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.fullcircuit.com/sites/all/themes/fcl/images/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="description" content="Full Circuit - B2B Electronic Engineers, bespoke electronics design, analogue, digital, vhdl, micro, pic, arm; bespoke software, C++, C and assembler" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.fullcircuit.com/" />
<link rel="shortlink" href="https://www.fullcircuit.com/" />
<meta property="fb:admins" content="190985991111626" />
<meta property="og:site_name" content="Full Circuit Ltd" />
<meta property="og:type" content="company" />
<meta property="og:url" content="https://www.fullcircuit.com/" />
<meta property="og:title" content="Full Circuit" />
<meta property="og:description" content="Full Circuit - B2B Electronic Engineers, bespoke electronics design, analogue, digital, vhdl, micro, pic, arm; bespoke software, C++, C and assembler" />
<meta property="og:image" content="https://www.fullcircuit.com/fcl-logo2.png" />
<meta name="dcterms.title" content="Full Circuit" />
<meta name="dcterms.creator" content="Malcolm Reeves" />
<meta name="dcterms.description" content="Full Circuit - B2B Electronic Engineers, bespoke electronics design, analogue, digital, vhdl, micro, pic, arm; bespoke software, C++, C and assembler" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.format" content="text/html" />
<meta name="dcterms.identifier" content="https://www.fullcircuit.com/" />
<meta name="dcterms.language" content="en" />
<title>Full Circuit | Elegant Solutions to Difficult Problems</title>
<style>
@import url("https://www.fullcircuit.com/modules/system/system.base.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/system/system.menus.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/system/system.messages.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/system/system.theme.css?pk2zck");
</style>
<style>
@import url("https://www.fullcircuit.com/modules/comment/comment.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/field/theme/field.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/node/node.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/search/search.css?pk2zck");
@import url("https://www.fullcircuit.com/modules/user/user.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/modules/views/css/views.css?pk2zck");
</style>
<style>
@import url("https://www.fullcircuit.com/sites/all/modules/ctools/css/ctools.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/modules/lightbox2/css/lightbox.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/modules/widgets/widgets.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/libraries/superfish/css/superfish.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/libraries/superfish/style/default.css?pk2zck");
</style>
<style media="screen">
@import url("https://www.fullcircuit.com/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.image.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?pk2zck");
</style>
<style>
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/html-elements.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/forms.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/tables.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/page.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/articles.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/comments.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/fields.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/blocks.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/navigation.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/corolla.settings.style.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/default/files/color/fcl-af625a26/colors.css?pk2zck");
@import url("https://www.fullcircuit.com/sites/all/themes/fcl/css/fcl.css?pk2zck");
</style>
<style media="print">
@import url("https://www.fullcircuit.com/sites/all/themes/corolla/css/print.css?pk2zck");
</style>
<link type="text/css" rel="stylesheet" href="https://www.fullcircuit.com/sites/default/files/adaptivetheme/fcl_files/fcl.responsive.layout.css?pk2zck" media="only screen" />
<style media="screen">
@import url("https://www.fullcircuit.com/sites/default/files/adaptivetheme/fcl_files/fcl.fonts.css?pk2zck");
</style>

<!--[if lt IE 9]>
<style media="screen">
@import url("https://www.fullcircuit.com/sites/default/files/adaptivetheme/fcl_files/fcl.lt-ie9.layout.css?pk2zck");
</style>
<![endif]-->
<script src="https://www.fullcircuit.com/misc/jquery.js?v=1.4.4"></script>
<script src="https://www.fullcircuit.com/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.fullcircuit.com/misc/drupal.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/lightbox2/js/lightbox.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/service_links/js/favorite_services.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/service_links/js/twitter_button.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/service_links/js/facebook_like.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/service_links/js/google_plus_one.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/libraries/superfish/sfsmallscreen.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/libraries/superfish/supposition.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/libraries/superfish/superfish.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/libraries/superfish/supersubs.js?pk2zck"></script>
<script src="https://www.fullcircuit.com/sites/all/modules/superfish/superfish.js?pk2zck"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"fcl","theme_token":"6XIIhtMnuM2mjMHjLN2mvWZkw6oe9OCPZyOdLpQcCoo","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/modules\/service_links\/js\/favorite_services.js":1,"sites\/all\/modules\/service_links\/js\/twitter_button.js":1,"sites\/all\/modules\/service_links\/js\/facebook_like.js":1,"sites\/all\/modules\/service_links\/js\/google_plus_one.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/widgets\/widgets.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/default.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/corolla\/css\/html-elements.css":1,"sites\/all\/themes\/corolla\/css\/forms.css":1,"sites\/all\/themes\/corolla\/css\/tables.css":1,"sites\/all\/themes\/corolla\/css\/page.css":1,"sites\/all\/themes\/corolla\/css\/articles.css":1,"sites\/all\/themes\/corolla\/css\/comments.css":1,"sites\/all\/themes\/corolla\/css\/fields.css":1,"sites\/all\/themes\/corolla\/css\/blocks.css":1,"sites\/all\/themes\/corolla\/css\/navigation.css":1,"sites\/all\/themes\/corolla\/css\/fonts.css":1,"sites\/all\/themes\/corolla\/css\/corolla.settings.style.css":1,"sites\/all\/themes\/fcl\/color\/colors.css":1,"sites\/all\/themes\/fcl\/css\/fcl.css":1,"sites\/all\/themes\/corolla\/css\/print.css":1,"public:\/\/adaptivetheme\/fcl_files\/fcl.responsive.layout.css":1,"public:\/\/adaptivetheme\/fcl_files\/fcl.fonts.css":1,"public:\/\/adaptivetheme\/fcl_files\/fcl.lt-ie9.layout.css":1}},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":1,"resize_sequence":0,"resize_speed":200,"fade_in_speed":200,"slide_down_speed":200,"use_alt_layout":0,"disable_resize":0,"disable_zoom":0,"force_show_nav":0,"show_caption":1,"loop_items":1,"node_link_text":"View Image Details","node_link_target":0,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":0,"useragent":"Mozilla\/5.0"},"ws_fl":{"width":100,"height":21},"ws_gpo":{"size":"","annotation":"","lang":"","callback":"","width":300},"urlIsAjaxTrusted":{"\/search\/node":true,"\/":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":true,"dropShadows":true,"disableHI":false},"plugins":{"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"Main menu"},"supposition":true,"bgiframe":false,"supersubs":{"minWidth":"12","maxWidth":"27","extraWidth":1}}}},"adaptivetheme":{"fcl":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)"}}}});</script>
<!--[if lt IE 9]>
<script src="https://www.fullcircuit.com/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?pk2zck"></script>
<![endif]-->
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-1 node-type-page site-name-full-circuit color-scheme-custom fcl bs-l bb-n mb-dd rc-6 rct-6">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page snc-n snw-n sna-l sns-d ssc-n ssw-n ssa-l sss-d btc-n btw-b bta-l bts-d ntc-n ntw-b nta-l nts-d ctc-n ctw-b cta-l cts-d ptc-n ptw-b pta-l pts-d">

          <div id="menu-bar-wrapper">
        <div class="container clearfix">
          <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1" >  
      <h2 class="element-invisible block-title">Main menu</h2>
  
  <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-default sf-total-items-5 sf-parent-items-3 sf-single-items-2"><li id="menu-218-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-1028-1" class="middle even sf-item-2 sf-depth-1 sf-total-children-6 sf-parent-children-3 sf-single-children-3 menuparent"><a href="/content/profile" class="sf-depth-1 menuparent">Profile</a><ul><li id="menu-595-1" class="first odd sf-item-1 sf-depth-2 sf-total-children-2 sf-parent-children-0 sf-single-children-2 menuparent"><a href="/content/full-circuit-service" title="Company info" class="sf-depth-2 menuparent">The Full Circuit Service</a><ul><li id="menu-1490-1" class="first odd sf-item-1 sf-depth-3 sf-no-children"><a href="/content/full-circuit-service" title="Company info" class="sf-depth-3">The Full Circuit Service</a></li><li id="menu-1363-1" class="last even sf-item-2 sf-depth-3 sf-no-children"><a href="/content/employee-cost-calculator" class="sf-depth-3">Employee Cost Calculator</a></li></ul></li><li id="menu-594-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/content/curriculum-vitae" title="aka Resumé" class="sf-depth-2">Curriculum Vitae</a></li><li id="menu-596-1" class="middle odd sf-item-3 sf-depth-2 sf-total-children-4 sf-parent-children-0 sf-single-children-4 menuparent"><a href="/content/projects-patents" class="sf-depth-2 menuparent">Projects</a><ul><li id="menu-597-1" class="first odd sf-item-1 sf-depth-3 sf-no-children"><a href="/content/fs2600-project" class="sf-depth-3">FS2600 Project</a></li><li id="menu-462-1" class="middle even sf-item-2 sf-depth-3 sf-no-children"><a href="/content/tms9902-replacement" title="example project" class="sf-depth-3">TMS9902 Replacement</a></li><li id="menu-593-1" class="middle odd sf-item-3 sf-depth-3 sf-no-children"><a href="/content/led-lamp-regulator" title="example project" class="sf-depth-3">LED Lamp Regulator</a></li><li id="menu-461-1" class="last even sf-item-4 sf-depth-3 sf-no-children"><a href="/content/vlm-optimisation" title="example project" class="sf-depth-3">VLM Optimisation</a></li></ul></li><li id="menu-598-1" class="middle even sf-item-4 sf-depth-2 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent"><a href="/content/projects-patents" class="sf-depth-2 menuparent">Patents</a><ul><li id="menu-599-1" class="first odd sf-item-1 sf-depth-3 sf-no-children"><a href="http://worldwide.espacenet.com/publicationDetails/biblio?FT=D&amp;date=19900815&amp;DB=worldwide.espacenet.com&amp;locale=en_EP&amp;CC=GB&amp;NR=2228121A&amp;KC=A&amp;ND=4" title="Fail safe isolated voltage detector" target="_blank" class="sf-depth-3">GB2228121</a></li><li id="menu-600-1" class="middle even sf-item-2 sf-depth-3 sf-no-children"><a href="http://worldwide.espacenet.com/publicationDetails/biblio?FT=D&amp;date=19900815&amp;DB=EPODOC&amp;locale=en_EP&amp;CC=GB&amp;NR=2228091A&amp;KC=A&amp;ND=5" title="Fail safe current detector" target="_blank" class="sf-depth-3">GB2228091</a></li><li id="menu-601-1" class="middle odd sf-item-3 sf-depth-3 sf-no-children"><a href="http://worldwide.espacenet.com/publicationDetails/biblio?FT=D&amp;date=19970122&amp;DB=EPODOC&amp;locale=en_EP&amp;CC=GB&amp;NR=2278219B&amp;KC=B&amp;ND=5" title="Active termination transceiver track circuit" target="_blank" class="sf-depth-3">GB2278219</a></li><li id="menu-602-1" class="middle even sf-item-4 sf-depth-3 sf-no-children"><a href="http://worldwide.espacenet.com/publicationDetails/biblio?FT=D&amp;date=19941116&amp;DB=EPODOC&amp;locale=en_EP&amp;CC=GB&amp;NR=2278005A&amp;KC=A&amp;ND=4" title="Active impedance bond" target="_blank" class="sf-depth-3">GB2278005</a></li><li id="menu-603-1" class="last odd sf-item-5 sf-depth-3 sf-no-children"><a href="http://worldwide.espacenet.com/publicationDetails/biblio?FT=D&amp;date=19960605&amp;DB=EPODOC&amp;locale=en_EP&amp;CC=GB&amp;NR=2295700A&amp;KC=A&amp;ND=4" title="Fail safe digital data testing (aka ROM Maze)" target="_blank" class="sf-depth-3">GB2295700</a></li></ul></li><li id="menu-762-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/content/references" title="also see linked in" class="sf-depth-2">References</a></li><li id="menu-760-1" class="last even sf-item-6 sf-depth-2 sf-no-children"><a href="http://uk.linkedin.com/pub/malcolm-reeves/7/304/94" title="Malcolm Reeves&#039; Linked In public profile (note linkedin hide detailed info unless you login)." target="_blank" class="sf-depth-2">Linked In</a></li></ul></li><li id="menu-1029-1" class="middle odd sf-item-3 sf-depth-1 sf-total-children-2 sf-parent-children-0 sf-single-children-2 menuparent"><a href="/content/freeware" class="sf-depth-1 menuparent">Freeware</a><ul><li id="menu-464-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/content/utilities" class="sf-depth-2">Utilities</a></li><li id="menu-604-1" class="last even sf-item-2 sf-depth-2 sf-no-children"><a href="/content/vhdl-testbench-tool" class="sf-depth-2">VHDL Testbench Tool</a></li></ul></li><li id="menu-1030-1" class="middle even sf-item-4 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent"><a href="/content/help" class="sf-depth-1 menuparent">Help</a><ul><li id="menu-553-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/search/node" class="sf-depth-2">Search</a></li><li id="menu-503-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/contact" class="sf-depth-2">Contact</a></li><li id="menu-764-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/site-map" class="sf-depth-2">Site Map</a></li><li id="menu-1102-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/recent-content" title="site content with latest 1st" class="sf-depth-2">Recent Content</a></li><li id="menu-1103-1" class="last odd sf-item-5 sf-depth-2 sf-no-children"><a href="/popular" title="Site pages with most visited (from site statistics) first" class="sf-depth-2">Popular Content</a></li></ul></li><li id="menu-587-1" class="last odd sf-item-5 sf-depth-1 sf-no-children"><a href="/blog" class="sf-depth-1">Blog</a></li></ul>
  </nav></div>        </div>
      </div>
    
    <div id="header-wrapper">
      <div class="container clearfix">

        <header class="clearfix with-logo" role="banner">

                      <div id="branding" class="branding-elements clearfix">

                              <div id="logo">
                  <a href="/" class="active"><img class="site-logo" src="https://www.fullcircuit.com/sites/all/themes/fcl/images/fcl-logo2.png" alt="Full Circuit" /></a>                </div>
              
                              <div class="h-group" id="name-and-slogan">

                                      <h1 id="site-name"><a href="/" title="Home page" class="active">Full Circuit</a></h1>
                  
                                      <h2 id="site-slogan">Elegant Solutions to Difficult Problems</h2>
                  
                </div>
              
            </div>
          
          
        </header>

      </div>
    </div>

    
    
    
    <div id="content-wrapper">
      <div class="container">

        <div id="columns">
          <div class="columns-inner clearfix">

            <div id="content-column">
              <div class="content-inner">

                
                <section id="main-content" role="main">

                  
                  <div class="content-margin">
                    <div class="content-style">

                      
                      
                      
                      
                      
                      
                      <div id="content">
                        <div class="region region-content"><div id="block-system-main" class="block block-system no-title odd first last block-count-2 block-region-content block-main" >  
  
  <article id="node-1" class="node node-page article odd node-full ia-n clearfix" role="article">
  <div class="node-inner">

    
              <header class="node-header">

        
        
      </header>
        
    <div class="node-content">
        <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even"><div id="home">
<div class="tab-right"><h2>•&nbsp;Bespoke&nbsp;Electronics&nbsp;•</h2><h3>Analogue, Digital, VHDL</h3><h2>•&nbsp;Bespoke&nbsp;Software&nbsp;•</h2><h3>C++, C, Assembler</h3><h2>•&nbsp;Consultancy&nbsp;•</h2><h3>Simulation<br>Analysis</h3><h2>•&nbsp;Software&nbsp;•</h2><h3>CAE, VHDL,<br>Freeware</h3><h2>•&nbsp;Competitive•</h2><h3>Fixed Price,<br>Daily Rate,<br>Percentage</h3></div>

<div class="tab-left">
<div id="homephoto"><img src="/sites/default/files/images/mePhoto2.jpg" alt="Malcolm Reeves" title="Full Circuit Director" width="188" height="268"></div>
<p>
As director I will personally be designing and overseeing all aspects of the work you put with Full Circuit, my electronics engineering company.  I am a retired chartered electronics engineer with over 25 years experience in the design of electronics and software, including in the harsher industrial environment, as well as more specialist expertise in railway electronics and analysis.

You can also can also find me listed on the <a href="http://lnkd.in/JUHuaF"  target="_blank"
title="professionals network" >linked-in</a>, among others.
</p><p>
The company is located in <a href="https://plus.google.com/+Fullcircuit/about" title="map" target="_blank">Chippenham</a>, Wiltshire, UK, which lies between Bristol, Bath and Swindon, on the south-west side of the south of England (tel: 01249 720161). However, it is not limited to consultancy just in this area and has done work for companies as far afield as the USA and Australia. More details of expertise and experience can be found under the profile menu above.
</p><p>
Prices are competitive so please ask for a quote. Prices can be fixed for a well defined job, by the day for jobs that are more unknown or a combination of the two. Working for a percentage of sales or similar arrangement might also be considered.  But, when comparing Full Circuit's day rate (should you opt for that) it is important to consider Full Circuit's <a href="/content/projects-patents" title="projects and patents list">history</a> of "Right 1st Time" designs and the wealth of <a href="/content/curriculum-vitae" title="resumé">experience</a> which results in a far shorter design cycle (a silicon valley hiring company <a href="http://www.fastcolabs.com/3018568/why-your-startup-cant-find-developers" target="_blank" title="fastcolabs article">advise</a>
their customers that offering 20% more will bring it 30% more applicants, and those 30% will be 2-3 times more productive).
</p><p>
Also on the menu to the left are the company's freeware software offering, Test Bench Tool and some useful utilities  like printer changer, to-do list, and calendar.
</p>
<ul>
<li>None of the software is adware or spyware.</li>
<li>Email addresses will not be revealed to other parties.</li>
<li>Note: Company published email is aggressively spam filtered so please use the more reliable
 <a href="/contact" title="contact form">contact form</a> (or phone of course).</li>
</ul>
<p>&nbsp;</p>
<div class="sig">Malcolm Reeves BSc (retired <a href="http://www.engc.org.uk" target="_blank" title="Engineering Council, national register for CEng">CEng</a> <a href="http://www.theiet.org" target="_blank" title="Institution of Engineering and Technology">MIET</a>
<a href="http://www.irse.org" target="_blank" title="Institution of Railway Signal Engineers">MIRSE</a>)</div>
<!--
<div class="ceng"><a href="http://www.engc.org.uk/ceng.aspx" target="_blank" title="Engineering Council, national register for CEng"><img src="/sites/default/files/images/CharteredEngineerC13.png" alt="Chartered Engineer Logo"
title="Registered Chartered Engineer" width="151" height="47"></a></div>
-->
<p>&nbsp;</p>
</div></div>
<div style="clear:both"></div></div></div></div>    </div>

    
    
  </div>
</article>

  </div></div>                      </div>

                      
                    </div>
                  </div>

                </section>

                
              </div>
            </div>

                        <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><div id="block-search-form" class="block block-search no-title odd first block-count-3 block-region-sidebar-second block-form"  role="search"><div class="block-inner clearfix">  
  
  <div class="block-content content"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-j_IDzXvWoqV679YmtdcR0J4qycdqt7QOYyzEfupkiWk" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div></div><section id="block-views-my-recent-content-block" class="block block-views even block-count-4 block-region-sidebar-second block-my-recent-content-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Recent Content</h2>
  
  <div class="block-content content"><div class="view view-my-recent-content view-id-my_recent_content view-display-id-block view-dom-id-2058305573373d280cd49d13277b3ee5">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/blog/fixing-annoying-blue-standby-lights">Fixing Annoying Blue Standby Lights</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/blog/proof-heating-247-can-be-cheaper">Proof that Heating 24/7 can be Cheaper</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/blog/virtualmin-webmin-vps-mail-problems">Virtualmin-Webmin VPS Mail Problems</a></span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/blog/backup-and-restore-webmin-virtualmin-vps">Backup and Restore for Webmin-Virtualmin VPS</a></span>  </div></li>
          <li class="views-row views-row-5 views-row-odd views-row-last">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/blog/comparing-configuration-files">Comparing Configuration Files</a></span>  </div></li>
      </ul></div>    </div>
  
  
  
      
<div class="more-link">
  <a href="/recent-content">
    more  </a>
</div>
  
  
  
</div></div>
  </div></section><section id="block-views-my-popular-content-block" class="block block-views odd block-count-5 block-region-sidebar-second block-my-popular-content-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Popular content</h2>
  
  <div class="block-content content"><div class="view view-my-popular-content view-id-my_popular_content view-display-id-block view-dom-id-7b0f1d9827f9aeaad9c0eedf7c398e7e">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/home" class="active">Full Circuit Ltd</a></span>  </span></li>
          <li class="views-row views-row-2 views-row-even">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/installing-phpmyadmin-webmin-virtualmin">Installing phpMyAdmin on Webmin-Virtualmin</a></span>  </span></li>
          <li class="views-row views-row-3 views-row-odd">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/vps-websites-using-free-webmin-virtualmin">VPS Websites using free Webmin-Virtualmin</a></span>  </span></li>
          <li class="views-row views-row-4 views-row-even">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/installing-free-ssl-webmin-virtualmin-vps">Installing Free SSL on Webmin-Virtualmin VPS</a></span>  </span></li>
          <li class="views-row views-row-5 views-row-odd">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/backup-and-restore-webmin-virtualmin-vps">Backup and Restore for Webmin-Virtualmin VPS</a></span>  </span></li>
          <li class="views-row views-row-6 views-row-even">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/virtualmin-webmin-vps-mail-problems">Virtualmin-Webmin VPS Mail Problems</a></span>  </span></li>
          <li class="views-row views-row-7 views-row-odd">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/content/fs2600-project">FS2600 Project</a></span>  </span></li>
          <li class="views-row views-row-8 views-row-even">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/blog/fixing-annoying-blue-standby-lights">Fixing Annoying Blue Standby Lights</a></span>  </span></li>
          <li class="views-row views-row-9 views-row-odd">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/content/vhdl-testbench-tool">VHDL Testbench Tool</a></span>  </span></li>
          <li class="views-row views-row-10 views-row-even views-row-last">  
  <span class="views-field views-field-title">        <span class="field-content"><a href="/content/curriculum-vitae">Curriculum Vitae</a></span>  </span></li>
      </ul></div>    </div>
  
  
  
      
<div class="more-link">
  <a href="/popular">
    more  </a>
</div>
  
  
  
</div></div>
  </div></section><section id="block-views-comments-recent-block" class="block block-views even last block-count-6 block-region-sidebar-second block-comments-recent-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Recent comments</h2>
  
  <div class="block-content content"><div class="view view-comments-recent view-id-comments_recent view-display-id-block view-dom-id-7b1cd91bae292e715020b678ec5f244b">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-subject">        <span class="field-content"><a href="/comment/23#comment-23">GCE </a></span>  </div>  
  <div class="views-field views-field-timestamp">        <span class="field-content"><em class="placeholder">7 months 3 days</em> ago</span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-subject">        <span class="field-content"><a href="/comment/15#comment-15">The validation tool from</a></span>  </div>  
  <div class="views-field views-field-timestamp">        <span class="field-content"><em class="placeholder">1 year 2 weeks</em> ago</span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-subject">        <span class="field-content"><a href="/comment/13#comment-13">Thanks</a></span>  </div>  
  <div class="views-field views-field-timestamp">        <span class="field-content"><em class="placeholder">3 years 9 months</em> ago</span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-subject">        <span class="field-content"><a href="/comment/12#comment-12">You can&#039;t avoid DNS setup. </a></span>  </div>  
  <div class="views-field views-field-timestamp">        <span class="field-content"><em class="placeholder">3 years 9 months</em> ago</span>  </div></li>
          <li class="views-row views-row-5 views-row-odd views-row-last">  
  <div class="views-field views-field-subject">        <span class="field-content"><a href="/comment/11#comment-11">About the mail setup and dns records</a></span>  </div>  
  <div class="views-field views-field-timestamp">        <span class="field-content"><em class="placeholder">3 years 9 months</em> ago</span>  </div></li>
      </ul></div>    </div>
  
  
  
      
<div class="more-link">
  <a href="/comments/recent">
    more  </a>
</div>
  
  
  
</div></div>
  </div></section></div></div>
          </div>
        </div>

      </div>
    </div>

    
    
          <div id="footer-wrapper">
        <div class="container clearfix">
          <footer class="clearfix" role="contentinfo">
            <div class="region region-footer"><div class="region-inner clearfix"><div id="block-widgets-s-fcl1" class="block block-widgets no-title odd first block-count-7 block-region-footer block-s-fcl1" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><a href="http://www.stumbleupon.com/submit?url=http%3A//www.fullcircuit.com/&amp;title=Full%20Circuit%20Ltd" title="Thumb this up at StumbleUpon" class="service-links-stumbleupon" rel="nofollow" target="_blank"><img class="image-style-none" src="http://www.fullcircuit.com/sites/all/modules/service_links/images/stumbleit.png" alt="StumbleUpon logo" /> StumbleUpon</a><a href="http://www.facebook.com/sharer.php?u=http%3A//www.fullcircuit.com/&amp;t=Full%20Circuit%20Ltd" title="Share on Facebook" class="service-links-facebook" rel="nofollow" target="_blank"><img class="image-style-none" src="http://www.fullcircuit.com/sites/all/modules/service_links/images/facebook.png" alt="Facebook logo" /> Facebook</a><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A//www.fullcircuit.com/&amp;title=Full%20Circuit%20Ltd&amp;summary=&amp;source=Full%20Circuit" title="Publish this post to LinkedIn" class="service-links-linkedin" rel="nofollow" target="_blank"><img class="image-style-none" src="http://www.fullcircuit.com/sites/all/modules/service_links/images/linkedin.png" alt="LinkedIn logo" /> LinkedIn</a></div>
  </div></div><div id="block-widgets-s-fcl2" class="block block-widgets no-title even block-count-8 block-region-footer block-s-fcl2" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><a href="http://www.facebook.com/plugins/like.php?href=http%3A//www.fullcircuit.com/&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;width=100&amp;height=21&amp;font=arial&amp;locale=en_US" title="I Like it" class="service-links-facebook-like" rel="nofollow" target="_blank"><span class="element-invisible">Facebook Like</span></a><a href="http://www.fullcircuit.com/" title="Plus it" class="service-links-google-plus-one" rel="nofollow" target="_blank"><span class="element-invisible">Google Plus One</span></a><a href="http://twitter.com/share?url=http%3A//tinyurl.com/oglojlr&amp;count=horizontal&amp;via=&amp;text=Full%20Circuit%20Ltd&amp;counturl=http%3A//www.fullcircuit.com/" class="twitter-share-button service-links-twitter-widget" title="Tweet This" rel="nofollow" target="_blank"><span class="element-invisible">Tweet Widget</span></a></div>
  </div></div><div id="block-block-1" class="block block-block no-title odd last block-count-9 block-region-footer block-1" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p><em><a href="/contact" title="contact form">The Full Circuit Service - Elegant solutions to difficult problems</a> <a href="/content/copyright">©</a></em></p></div>
  </div></div></div></div>            <p class="attribute-creator"></p>
          </footer>
        </div>
      </div>
    
  </div>
</div>
  </body>
</html>
