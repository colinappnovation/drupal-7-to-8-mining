<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.duckweed.co.uk/sites/default/files/frog_4.jpg" type="image/jpeg" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="1" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.duckweed.co.uk/" />
<link rel="shortlink" href="https://www.duckweed.co.uk/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.duckweed.co.uk/" />
<meta name="twitter:title" content="duckweed.co.uk" />
<meta name="twitter:description" content="A modest plant with great potential" />
<meta itemprop="name" content="duckweed.co.uk" />
<meta itemprop="description" content="A modest plant with great potential" />
<title>duckweed.co.uk | A modest plant with great potential</title>
<style type="text/css" media="all">
@import url("https://www.duckweed.co.uk/modules/system/system.base.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/system/system.menus.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/system/system.messages.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/system/system.theme.css?p7mr0n");
</style>
<style type="text/css" media="all">
@import url("https://www.duckweed.co.uk/modules/aggregator/aggregator.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/comment/comment.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/field/theme/field.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/node/node.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/search/search.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/user/user.css?p7mr0n");
@import url("https://www.duckweed.co.uk/modules/forum/forum.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/views/css/views.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?p7mr0n");
</style>
<style type="text/css" media="all">
@import url("https://www.duckweed.co.uk/sites/all/modules/ctools/css/ctools.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/panels/css/panels.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/video/css/video.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/responsive_menus/styles/responsive_menus_simple/css/responsive_menus_simple.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/default/files/ctools/css/ad57ff1546b4e3e493a0d16eb09e3ad9.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/modules/panels/plugins/layouts/flexible/flexible.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/default/files/ctools/css/915add3f9d6608987603343073afa90a.css?p7mr0n");
</style>
<style type="text/css" media="screen">
@import url("https://www.duckweed.co.uk/sites/all/themes/adaptivetheme/adaptivetheme/adaptivetheme/at_core/css/at.layout.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/global.base.css?p7mr0n");
@import url("https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/global.styles.css?p7mr0n");
</style>
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/default/files/adaptivetheme/at_panels_everywhere_files/at_panels_everywhere.responsive.layout.css?p7mr0n" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/responsive.smartphone.portrait.css?p7mr0n" media="only screen and (max-width:320px)" />
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/responsive.smartphone.landscape.css?p7mr0n" media="only screen and (min-width:321px) and (max-width:480px)" />
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/responsive.tablet.portrait.css?p7mr0n" media="only screen and (min-width:481px) and (max-width:768px)" />
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/responsive.tablet.landscape.css?p7mr0n" media="only screen and (min-width:769px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://www.duckweed.co.uk/sites/all/themes/at_panels_everywhere/css/responsive.desktop.css?p7mr0n" media="only screen and (min-width:1025px)" />

<!--[if lt IE 9]>
<style type="text/css" media="screen">
@import url("https://www.duckweed.co.uk/sites/default/files/adaptivetheme/at_panels_everywhere_files/at_panels_everywhere.lt-ie9.layout.css?p7mr0n");
</style>
<![endif]-->
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.min.js?v=1.7.2"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/misc/drupal.js?p7mr0n"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/admin_menu/admin_devel/admin_devel.js?p7mr0n"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/cookiecontrol/js/cookieControl-5.1.min.js?p7mr0n"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/video/js/video.js?p7mr0n"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/responsive_menus/styles/responsive_menus_simple/js/responsive_menus_simple.js?p7mr0n"></script>
<script type="text/javascript" src="https://www.duckweed.co.uk/sites/all/modules/google_analytics/googleanalytics.js?p7mr0n"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-100296131-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_panels_everywhere","theme_token":"n7VE9AXYkTF21xYK1FidARo8hdCpOKu5jbR6geGiNVw","js":{"0":1,"1":1,"2":1,"3":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/cookiecontrol\/js\/cookieControl-5.1.min.js":1,"sites\/all\/modules\/video\/js\/video.js":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/js\/responsive_menus_simple.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"4":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"modules\/forum\/forum.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/video\/css\/video.css":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/css\/responsive_menus_simple.css":1,"public:\/\/ctools\/css\/ad57ff1546b4e3e493a0d16eb09e3ad9.css":1,"sites\/all\/modules\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"public:\/\/ctools\/css\/915add3f9d6608987603343073afa90a.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/global.base.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/global.styles.css":1,"public:\/\/adaptivetheme\/at_panels_everywhere_files\/at_panels_everywhere.responsive.layout.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/responsive.smartphone.portrait.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/responsive.smartphone.landscape.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/at_panels_everywhere\/css\/responsive.desktop.css":1,"public:\/\/adaptivetheme\/at_panels_everywhere_files\/at_panels_everywhere.lt-ie9.layout.css":1}},"responsive_menus":[{"toggler_text":"\u2630 Menu","selectors":["#main-menu"],"media_size":"768","media_unit":"px","absolute":true,"remove_attributes":true,"responsive_menus_style":"responsive_menus_simple"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true,"\/Index?destination=node\/14":true},"adaptivetheme":{"at_panels_everywhere":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});
//--><!]]>
</script>
<!--[if lt IE 9]>
<script src="https://www.duckweed.co.uk/sites/all/themes/adaptivetheme/adaptivetheme/adaptivetheme/at_core/scripts/html5.js?p7mr0n"></script>
<![endif]-->
</head>
<body class="html front not-logged-in page-node page-node- page-node-14 node-type-page atr-7.x-3.x atv-7.x-3.0-rc1 at-panels-everywhere">
  <div id="skip-link" class="nocontent">
    <a href="#content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="panel-flexible panels-flexible-1 clearfix" id="page-wrapper">
<div class="panel-flexible-inside panels-flexible-1-inside">
<div class="panels-flexible-row panels-flexible-row-1-1 panels-flexible-row-first clearfix header">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-1-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-1-header panels-flexible-region-first panels-flexible-region-last">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-header-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-pane-header no-title block" >
  <div class="pane-inner clearfix">
    
            
    
    <div class="pane-content">
      <header id="header" class="clearfix" role="banner">

      <!-- start: Branding -->
    <div id="branding" class="branding-elements clearfix">

              <div id="logo">
          <a href="/" title="Home page" class="active"><img class="site-logo" typeof="foaf:Image" src="https://www.duckweed.co.uk/sites/default/files/frog_0_1.jpg" alt="duckweed.co.uk" /></a>        </div>
      
              <!-- start: Site name and Slogan hgroup -->
        <hgroup id="name-and-slogan">

                      <h1 id="site-name"><a href="/" title="Home page" class="active">duckweed.co.uk</a></h1>
          
                      <h2 id="site-slogan">A modest plant with great potential</h2>
          
        </hgroup><!-- /end #name-and-slogan -->
      
    </div><!-- /end #branding -->
  
</header>
    </div>

    
        
  </div>
</div>
<div class="panel-separator"></div><div class="panel-pane pane-pane-navigation no-title block" >
  <div class="pane-inner clearfix">
    
            
    
    <div class="pane-content">
      <div id="primary-menu-bar" class="nav clearfix"><nav  class="primary-menu-wrapper menu-wrapper clearfix" role="navigation" aria-labelledby="primary-menu"><h2 class="element-invisible">Main menu</h2><ul class="menu primary-menu clearfix"><li class="menu-742 active-trail first active"><a href="/Index" class="active-trail active">Index Page</a></li><li class="menu-495 active"><a href="/" title="" class="active">What is Duckweed</a></li><li class="menu-497"><a href="/Project">Project Details</a></li><li class="menu-713"><a href="/uses">Potential Uses of Duckweed</a></li><li class="menu-892"><a href="/contacconntact">Contact</a></li><li class="menu-2372 last"><a href="/node/34" title="useful">tech links</a></li></ul></nav></div>    </div>

    
        
  </div>
</div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-search-form no-title block" >
  <div class="pane-inner clearfix">
    
            
    
    <div class="pane-content">
      <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-1kVPtKIWLUSDxYpTJEzBIhdiq3oDZNncFwoRC3IdtYU" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>    </div>

    
        
  </div>
</div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-main-row panels-flexible-row-last clearfix main-content">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-main-row-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-row-1-main-row-middle"><div class="panels-flexible-region panels-flexible-region-1-content panels-flexible-region-first ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-content-inside panels-flexible-region-inside-first">
<div class="panel-pane pane-page-content no-title block" >
  <div class="pane-inner clearfix">
    
            
    
    <div class="pane-content">
      <article id="node-14" class="node node-page node-promoted article odd node-lang-en node-full clearfix" about="/Index" typeof="foaf:Document" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><h3><img alt="Frog in Dukweed pond" src="https://duckweed.co.uk/sites/default/files/frog.jpg" style="float:left; height:461px; margin-left:5px; margin-right:5px; width:400px" />The nature of duckweed(<em>synopsis of general characteristics</em>)</h3>
<p>Duckweed has a wide global distribution and grows readily on fresh and brackish still water surfaces. It is capable of rapid growth (as high as doubling its mass every 16 hours with the highest reported yield of 90 tonnes DM per hectare water area per year. It is highly effective in extracting nitrogen, phosphate, potassium, minerals and trace implements from it's water substrate. It is composed of high quality vegetable protein (up to 46% DM) with an amino acid profile comparable to soya. It produces carbohydrate under certain condition (40% DM)<br />
It has a simple genome (Spirodela DNA is sequenced) which offers opportunities for genetic modulation and modification It is easy to handle (harvest and process)-more so than algae. <br />
It's reproduction is predominantly vegetative and it should be more readily containable for genetic modification than micro organisms or field crops.</p>
<p>Two native species (collected from the wild in locations close to NP16 6QT)<br /><em>Lemna minor</em> and <em>Sprodela polyrhiza</em> are currently being cultivated.</p>
<p> </p>
<p> </p>
<p><a href="http://duckweed.co.uk/sites/default/files/Fruit_and_vegetables_in_season_.pdf">link<img alt="" src="http://duckweed.co.uk/sites/default/files/Fruit_and_vegetables_in_season_.pdf" /></a></p>
</div></div></div>  </div>

  
  
  <span property="dc:title" content="Index Page" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span></article>
    </div>

    
        
  </div>
</div>
  </div>
</div>
</div><div class="panels-flexible-row-1-main-row-right"><div class="panels-flexible-region panels-flexible-region-1-sidebar panels-flexible-region-last">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-sidebar-inside panels-flexible-region-inside-last">
<section class="panel-pane pane-block pane-user-login block" >
  <div class="pane-inner clearfix">
    
              <h2 class="pane-title block-title">User login</h2>
        
    
    <div class="pane-content">
      <form action="/Index?destination=node/14" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list"><ul><li class="even first last"><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li></ul></div><input type="hidden" name="form_build_id" value="form-0sEq7lp0EMjNYaOycZKIS6Q8eYlbtJILkArHlYHUOZ8" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Log in" class="form-submit" /></div></div></form>    </div>

    
        
  </div>
</section>
  </div>
</div>
</div>  </div>
</div>
</div>
</div>
  <script type="text/javascript">
<!--//--><![CDATA[//><!--

    jQuery(document).ready(function($) {
    cookieControl({
        introText: '<p>This site uses cookies to store information on your computer.</p>',
        fullText: '<p>Some cookies on this site are essential, and the site won\'t work as expected without them. These cookies are set when you submit a form, login or interact with the site by doing something that goes beyond clicking on simple links.</p><p>We also use some non-essential cookies to anonymously track visitors or enhance your experience of the site. If you\'re not happy with this, we won\'t set these cookies but some nice features of the site may be unavailable.</p>',
        theme: 'dark',
        html: '<div id="cccwr"><div id="ccc-state" class="ccc-pause"><div id="ccc-icon"><button><span>Cookie Control</span></button></div><div class="ccc-widget"><div class="ccc-outer"><div class="ccc-inner"><h2>Cookie Control</h2><div class="ccc-content"><p class="ccc-intro"></p><div class="ccc-expanded"></div><div id="ccc-cookies-switch" style="background-position-x: 0;"><a id="cctoggle" href="#" style="background-position-x: 0;" name="cctoggle"><span id="cctoggle-text">Cookies test</span></a></div><div id="ccc-implicit-warning">(One cookie will be set to store your preference)</div><div id="ccc-explicit-checkbox"><label><input id="cchide-popup" type="checkbox" name="ccc-hide-popup" value="Y" /> Do not ask me again<br /></label> (Ticking this sets a cookie to hide this popup if you then hit close. This will not store any personal information)</div><p class="ccc-about"><small><a href="http://www.civicuk.com/cookie-law" target="_blank">About this tool</a></small></p><a class="ccc-icon" href="http://www.civicuk.com/cookie-law" target="_blank"title="About Cookie Control">About Cookie Control</a><button class="ccc-close">Close</button></div></div></div><button class="ccc-expand">read more</button></div></div></div>',
        position: 'left',
        shape: 'triangle',
        startOpen: true,
        autoHide: 60000,
        onAccept: function(cc){cookiecontrol_accepted(cc)},
        onReady: function(){},
        onCookiesAllowed: function(cc){cookiecontrol_cookiesallowed(cc)},
        onCookiesNotAllowed: function(cc){cookiecontrol_cookiesnotallowed(cc)},
        countries: '',
        subdomains: true,
        cookieName: 'duckweedcouk_cookiecontrol',
        iconStatusCookieName: 'ccShowCookieIcon',
        consentModel: 'implicit'
        });
      });
    
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_accepted(cc) {
cc.setCookie('ccShowCookieIcon', 'no');jQuery('#ccc-icon').hide();
}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_cookiesallowed(cc) {

}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
function cookiecontrol_cookiesnotallowed(cc) {

}
//--><!]]>
</script>
</body>
</html>
