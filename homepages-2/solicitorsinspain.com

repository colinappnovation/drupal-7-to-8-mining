<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.solicitorsinspain.com/sites/default/files/eandg-favicon.png" type="image/png" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<link rel="canonical" href="https://www.solicitorsinspain.com/" />
<meta name="keywords" content="Spanish lawyers, Spanish law, Spanish tax, attorney in Spain, Spain lawyers, solicitors in Spain, buying a Spanish property, buying a property in Spain, making a Spanish will, Spanish wills, debt recovery in Spain, power of attorney in Spain" />
<link rel="shortlink" href="/node/3" />
<meta name="description" content="Spanish lawyers in the UK, buying and selling property in Spain, inheritance in Spain, doing business in Spain, debt recovery in Spain and more" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
  <title>Solicitors in Spain | Spanish legal advice in plain English</title>  
  <link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_cvIzKcrVDBLXMdTEPeFDF7hsOQyNPG5SGybZzdjcJLI.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_h42KrNynvrQ0IkwgFC5cBRBgayoO1e29TTzlbDLYdlI.css" media="all" />
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,900,400italic" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_3I0ihgg8sU7y5NvECa6ajmQkrtWhS2DYMHu-gDcD0hM.css" media="all" />

<!--[if (lt IE 9)&(!IEMobile)]>
<link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_mqAoTZsHQgpklv9aR49cS79bXKf2t20xn-hyNGirEm8.css" media="all" />
<![endif]-->

<!--[if gte IE 9]><!-->
<link type="text/css" rel="stylesheet" href="https://www.solicitorsinspain.com/sites/default/files/css/css_FQ0itvdx1PVhLPHETMc_6GZc-OtaxQUIZYSDOzNSs5o.css" media="all" />
<!--<![endif]-->
  <script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_vDrW3Ry_4gtSYaLsh77lWhWjIC6ml2QNkcfvfP5CVFs.js"></script>
<script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_WKKlNro6SumHNTciX15xgnW7dp65U0rA1zKWPv6HcLs.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-29324550-1"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(w, d) {
  var s = d.createElement("script"), el = d.getElementsByTagName("script")[0]; s.async = true;
  s.src = "https://www.solicitorsinspain.com/sites/all/libraries/implied-consent/dist/implied-consent.min.js"; el.parentNode.insertBefore(s, el);
})(this, this.document);
//--><!]]>
</script>
<script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_4lYeY6E5zeARk9RwRDf7By5Cy9QqSFa0N5PphC8vE8Y.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_xFeZE5DFe-kA0PA1fu2QpvnUfI1lw6iX3UQKtS7gR3Q.js"></script>
<script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_uwrfBOJCFVq42muTMjrq2bD-RUh8NNq_fZRTb6dsNb4.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"eandg","theme_token":"KGhZA79K51fn0HgmumpTPh-EVrtZNtHpa579KZojxn4","js":{"0":1,"sites\/all\/modules\/flexslider\/assets\/js\/flexslider.load.js":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/js\/responsive_menus_simple.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"2":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"3":1,"sites\/all\/themes\/eandg\/js\/mainmenu.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-equalheights.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/css\/responsive_menus_simple.css":1,"sites\/all\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/modules\/field_collection\/field_collection.theme.css":1,"\/\/fonts.googleapis.com\/css?family=Lato:400,900,400italic":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/eandg\/css\/global.css":1,"ie::normal::sites\/all\/themes\/eandg\/css\/eandg-alpha-default.css":1,"ie::normal::sites\/all\/themes\/eandg\/css\/eandg-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/all\/themes\/eandg\/css\/eandg-alpha-default.css":1,"narrow::sites\/all\/themes\/eandg\/css\/eandg-alpha-default-narrow.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/all\/themes\/eandg\/css\/eandg-alpha-default.css":1,"normal::sites\/all\/themes\/eandg\/css\/eandg-alpha-default-normal.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/all\/themes\/eandg\/css\/eandg-alpha-default.css":1,"wide::sites\/all\/themes\/eandg\/css\/eandg-alpha-default-wide.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"jcarousel":{"ajaxPath":"\/jcarousel\/ajax\/views"},"responsive_menus":[{"toggler_text":"\u2630 Menu","selectors":["#block-menu-block-2 .menu"],"media_size":"960","absolute":true,"remove_attributes":true,"responsive_menus_style":"responsive_menus_simple"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"},"flexslider":{"optionsets":{"carousel":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":0,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"slide","slideshow":true,"slideshowSpeed":"7000","directionNav":true,"controlNav":true,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":true,"manualControls":""}},"instances":{"flexslider-1":"carousel"}},"urlIsAjaxTrusted":{"\/":true},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>
</script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in page-node page-node- page-node-3 node-type-page carousel-active">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-user-wrapper" class="zone-wrapper zone-user-wrapper clearfix">  
  <div id="zone-user" class="zone zone-user clearfix container-12">
    <div class="grid-12 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="branding-data clearfix">
                        <hgroup class="site-name-slogan">        
                                
        <h1 class="site-name"><a href="/" title="Home" class="active">Solicitors in Spain</a></h1>
                              </hgroup>
          </div>
        <div class="block block-eandg-block-placement block-header-block block-eandg-block-placement-header-block odd block-without-title" id="block-eandg-block-placement-header-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      
    <div class="social-links">
      <a class="link-twitter" href="https://twitter.com/EshkeriGrau" target="_blank">Twitter</a>
      <a class="link-facebook" href="http://www.facebook.com/SolicitorsInSpain" target="_blank">Facebook</a>
      <a class="link-linkedin" href="http://www.linkedin.com/company/eshkeri-&amp;-grau-solicitors" target="_blank">LinkedIn</a>
      <a class="link-youtube" href="https://www.youtube.com/channel/UC5tiviNhbabQWiK298CXPBw" target="_blank">YouTube</a>
    </div>
  
    <div class="contact-details">
      <div class="contact-email"><label>Email:</label><span> <a href="mailto:info@solicitorsinspain.com">info@solicitorsinspain.com</a></span></div>
      <div class="contact-london"><label>London:</label><span> +44 (0)20 3478 1420</span></div>
      <div class="contact-tarragona"><label>Tarragona:</label><span> +34 977 249 960</span></div>
    </div>
      </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">  
  <div id="zone-menu" class="zone zone-menu clearfix container-12">
    <div class="grid-12 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
    <div class="block block-menu-block block-2 block-menu-block-2 odd block-without-title" id="block-menu-block-2">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="menu-block-wrapper menu-block-2 menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first expanded menu-mlid-440"><a href="/about-us" title="Spanish Lawyers in London and in Spain, About Us">About Us</a><ul class="menu"><li class="first leaf menu-mlid-804"><a href="/faqs">FAQs</a></li>
<li class="leaf menu-mlid-763"><a href="/meet-team">Meet the Team</a></li>
<li class="last leaf menu-mlid-790"><a href="/testimonials" title="Spanish lawyer reviews">What our Clients Think</a></li>
</ul></li>
<li class="expanded menu-mlid-345"><a href="/our-legal-services" title="Spanish legal services for individuals">Our Legal Services</a><ul class="menu"><li class="first expanded menu-mlid-860"><a href="/our-services-individuals" title="E&amp;G Solicitors in Spain - legal services for individuals">Our Services for Individuals</a><ul class="menu"><li class="first expanded menu-mlid-770"><a href="/buying-property-spain" title="">Buying Property in Spain</a><ul class="menu"><li class="first leaf menu-mlid-803"><a href="/checklist">Checklist</a></li>
<li class="last leaf menu-mlid-806"><a href="/free-guide-buying-property-spain" title="E&amp;G Solicitors in Spain - Free Guide to Buying Property in Spain">Free Guide to Buying Property in Spain</a></li>
</ul></li>
<li class="leaf menu-mlid-772"><a href="/spanish-planning-law" title="">Spanish Planning Law</a></li>
<li class="leaf menu-mlid-771"><a href="/selling-property-spain" title="">Selling Property in Spain</a></li>
<li class="leaf menu-mlid-773"><a href="/spanish-wills" title="">Spanish Wills</a></li>
<li class="expanded menu-mlid-774"><a href="/succession-and-probate-spain" title="">Succession and Probate in Spain</a><ul class="menu"><li class="first last leaf menu-mlid-925"><a href="/free-guide-inheritance-spain" title="A free guide to all aspects of inheriting in Spain, whether there is a will or not">Free guide to inheriting in Spain</a></li>
</ul></li>
<li class="leaf menu-mlid-809"><a href="/brexit" title="Brexit and the future of Brits in Spain">Brexit</a></li>
<li class="leaf menu-mlid-776"><a href="/business-law-spain" title="">Business Law in Spain</a></li>
<li class="leaf menu-mlid-775"><a href="/debt-and-asset-recovery-spain" title="">Debt and Asset Recovery in Spain</a></li>
<li class="leaf menu-mlid-777"><a href="/moving-spain" title="">Moving to Spain</a></li>
<li class="leaf menu-mlid-801"><a href="/reclaiming-spanish-inheritance-tax" title="">Reclaiming Spanish Inheritance Tax</a></li>
<li class="leaf menu-mlid-805"><a href="/renting-out-your-property-spain" title="">Renting out your property in Spain</a></li>
<li class="last leaf menu-mlid-778"><a href="/taxation-spain" title="">Taxation in Spain</a></li>
</ul></li>
<li class="last expanded menu-mlid-346"><a href="/our-services-businesses" title="Spanish Business legal advice in English">Our Legal Services for Businesses</a><ul class="menu"><li class="first leaf menu-mlid-779"><a href="/real-estate-spain">Real Estate in Spain</a></li>
<li class="leaf menu-mlid-780"><a href="/charities-0">Charities - Inheriting Assets in Spain</a></li>
<li class="leaf menu-mlid-783"><a href="/community-fees-spain">Community Fees in Spain</a></li>
<li class="leaf menu-mlid-781"><a href="/business-law-spain-0">Business Law in Spain</a></li>
<li class="leaf menu-mlid-782"><a href="/tax-businesses-spain">Tax for Businesses in Spain</a></li>
<li class="leaf menu-mlid-785"><a href="/debt-recovery-spain">Debt Recovery in Spain</a></li>
<li class="leaf menu-mlid-784"><a href="/asset-tracing-recovery-spain">Asset Tracing and Recovery in Spain</a></li>
<li class="leaf menu-mlid-787"><a href="/immigration-spain">Immigration to Spain</a></li>
<li class="last leaf menu-mlid-788"><a href="/obtaining-security-loans-and-other-debts-spain">Obtaining Security for Loans and Other Debts in Spain</a></li>
</ul></li>
</ul></li>
<li class="leaf menu-mlid-859"><a href="/videos" title="E&amp;G Solicitors in Spain video guides">Videos</a></li>
<li class="last expanded menu-mlid-402"><a href="/articles" title="Spanish legal news">News</a></li>
</ul></div>
    </div>
  </div>
</div>  </div>
</div>
  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix equal-height-container container-12">    
        
        <div class="grid-12 region region-content equal-height-element" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                <h1 class="title" id="page-title"><span>Spanish lawyers in the UK and in Spain</span></h1>
                        <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <article about="/spanish-lawyers-uk-and-spain" typeof="foaf:Document" class="node node-page node-published node-not-promoted node-not-sticky author-uniqueadminuser-1 odd clearfix node-full" id="node-page-3">
        <span property="dc:title" content="Spanish lawyers in the UK and in Spain" class="rdf-meta element-hidden"></span>    
  
  <div class="content clearfix">
    <div class="field field-name-field-carousel field-type-field-collection field-label-hidden"><div class="field-items"><div class="field-item even"><div  id="flexslider-1" class="flexslider">
  <ul class="slides"><li><div class="entity entity-field-collection-item field-collection-item-field-carousel clearfix" about="/field-collection/field-carousel/2" typeof="" class="entity entity-field-collection-item field-collection-item-field-carousel">
  <div class="content">
    <div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/carousel_main/public/images/Sunset%20Carousel.jpg?itok=pVoxh96F" width="1200" height="510" alt="Buying Property in Spain" /></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Buying Property in Spain</h2>
<p>If you're thinking about buying a property in Spain, research is key.  Find out all you need to know about buying and owning property in Spain, from costs, to process and everything in between, by reading our Buying Property in Spain pages.    </p>
<p><a href="http://www.solicitorsinspain.com/buying-property-spain"><b><u>Read more...</u></b></a></p>
</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/buying-property-spain">Buying Property in Spain</a></div></div></div>  </div>
</div>
</li>
<li><div class="entity entity-field-collection-item field-collection-item-field-carousel clearfix" about="/field-collection/field-carousel/34" typeof="" class="entity entity-field-collection-item field-collection-item-field-carousel">
  <div class="content">
    <div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/carousel_main/public/images/Yachts%201200%20x%20510.jpg?itok=YP2T7Piu" width="1200" height="510" alt="Your guide to making a Spanish Will" /></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Your guide to making a Spanish Will</h2>
<p>If you own property or assets in Spain it's very important that you make a Spanish will.  Find out why and how by reading our comprehensive guides to all things Spanish wills. </p>
<p><a href="http://www.solicitorsinspain.com/spanish-wills"><u><strong>Click here to read more</strong></u></a></p>
</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/spanish-wills">Your guide to making a Spanish Will</a></div></div></div>  </div>
</div>
</li>
<li><div class="entity entity-field-collection-item field-collection-item-field-carousel clearfix" about="/field-collection/field-carousel/35" typeof="" class="entity entity-field-collection-item field-collection-item-field-carousel">
  <div class="content">
    <div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/carousel_main/public/images/Ibiza%20Beach%201200%20x%20510%20carousel.jpg?itok=PEY7imxz" width="1200" height="510" alt="Your guide to inheriting in Spain" /></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Your guide to inheriting in Spain</h2>
<p>If you're feeling confused about inheritance in Spain, help is at hand.  Take a look at our pages on inheritance in Spain for the lowdown on Spanish inheritance tax, and how to get past Spanish legal jargon.  </p>
<p><a href="http://www.solicitorsinspain.com/succession-and-probate-spain"><strong><u>Read now</u></strong><strong><u>...</u></strong></a></p>
</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/succession-and-probate-spain">Your guide to inheriting in Spain</a></div></div></div>  </div>
</div>
</li>
</ul></div>
</div></div></div><div class="field-collection-container clearfix"><div class="field field-name-field-content-blocks field-type-field-collection field-label-hidden"><div class="field-items"><div class="field-item even"><div class="field-collection-view clearfix view-mode-full"><div class="entity entity-field-collection-item field-collection-item-field-content-blocks clearfix" about="/field-collection/field-content-blocks/8" typeof="" class="entity entity-field-collection-item field-collection-item-field-content-blocks">
  <div class="content">
    <div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-10-23T00:00:00+01:00">23 Oct 2019</span></div></div></div><div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/content_block_image/public/images/Euros%20-%20Content%20Block%20%281%29_1.jpg?itok=VaACxua-" width="460" height="240" alt="Death and taxes in Spain" /></div></div></div><div class="field field-name-field-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even"><h3 class="title"><a href="/articles/death-and-taxes-spain">Death and taxes in Spain</a></h3></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even">A guide to the important changes made to the tax rules in Spain; in particular, mortgage tax, inheritance tax for non EU residents and gift tax.
</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.solicitorsinspain.com/articles/death-and-taxes-spain">Read now</a></div></div></div>  </div>
</div>
</div></div><div class="field-item odd"><div class="field-collection-view clearfix view-mode-full"><div class="entity entity-field-collection-item field-collection-item-field-content-blocks clearfix video-block node-colorbox-link"><div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-10-23T00:00:00+01:00">23 Oct 2019</span></div></div></div><div class="block-link"><a class="test colorbox-load" href="https://www.solicitorsinspain.com/vef/load/e83dd22c5d6d2510dc4fbb57b736e233?width=640&height=365"></a></div><div class="field field-name-field-video field-type-video-embed-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.solicitorsinspain.com/vef/load/e83dd22c5d6d2510dc4fbb57b736e233?width=640&amp;height=365" class="colorbox-load"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/content_block_image/public/video_embed_field_thumbnails/youtube/Mhh_emMnG7g.jpg?itok=LKv9WxX7" width="460" height="240" alt="" /></a></div></div></div><div class="field field-name-field-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even"><h3 class="title"><a href="/videos/inheritance-spain-where-there-will">Inheritance in Spain where there is a will</a></h3></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even">Our guide to the inheritance process in Spain where there is a will.</div></div></div></div></div></div><div class="field-item even"><div class="field-collection-view clearfix view-mode-full"><div class="entity entity-field-collection-item field-collection-item-field-content-blocks clearfix" about="/field-collection/field-content-blocks/9" typeof="" class="entity entity-field-collection-item field-collection-item-field-content-blocks">
  <div class="content">
    <div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-10-23T00:00:00+01:00">23 Oct 2019</span></div></div></div><div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/content_block_image/public/images/White%20Village%20460x240_4.jpg?itok=07DqSVk6" width="460" height="240" alt="Buying property in Camposol" /></div></div></div><div class="field field-name-field-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even"><h3 class="title"><a href="/articles/buying-property-camposol">Buying property in Camposol</a></h3></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even">If you are thinking about buying a property in the Camposol development in Spain, you need to be aware of some issues.</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.solicitorsinspain.com/articles/buying-property-camposol">Read now</a></div></div></div>  </div>
</div>
</div></div><div class="field-item odd"><div class="field-collection-view clearfix view-mode-full field-collection-view-final"><div class="entity entity-field-collection-item field-collection-item-field-content-blocks clearfix" about="/field-collection/field-content-blocks/11" typeof="" class="entity entity-field-collection-item field-collection-item-field-content-blocks">
  <div class="content">
    <div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-10-23T00:00:00+01:00">23 Oct 2019</span></div></div></div><div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.solicitorsinspain.com/sites/default/files/styles/content_block_image/public/images/menorca%20villa%20460%20x%20240_1_1.jpg?itok=BUCKf6NM" width="460" height="240" alt="Five key things you need to know when buying a place in Spain" /></div></div></div><div class="field field-name-field-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even"><h3 class="title"><a href="/articles/5-key-things-you-need-know-when-buying-property-spain">Five key things you need to know when buying a place in Spain</a></h3></div></div></div><div class="field field-name-field-intro-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even">A rundown of five post completion matters you need to be aware of when buying a place in Spain.</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://www.solicitorsinspain.com/articles/5-key-things-you-need-know-when-buying-property-spain">Read now</a></div></div></div>  </div>
</div>
</div></div></div></div></div>  </div>
  
  <div class="clearfix">
          <nav class="links node-links clearfix"></nav>
    
      </div>
</article>    </div>
  </div>
</div><section class="block block-views block-testimonials-block block-views-testimonials-block even" id="block-views-testimonials-block">
  <div class="block-inner clearfix">
              <h2 class="block-title"><span>Testimonials</span></h2>
            
    <div class="content clearfix">
      <div class="view view-testimonials view-id-testimonials view-display-id-block view-dom-id-84e40c3f249a8673739d8cf44e808a31">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-intro-text">        <div class="field-content"><blockquote>I thoroughly recommend them.  E&amp;G Solicitors in Spain are the fourth Spanish-based legal firm I have used in 7 years.  They are competent, effective and given the high quality of care and the advice they give, they are inexpensive.  In my experience, their service is on a par with some of the best international law firms at half to a third of the price. </blockquote></div>  </div>  
  <div class="views-field views-field-title">        <h4 class="field-content">D.S. Bowers, London</h4>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/testimonials">
    Read more testimonials  </a>
</div>
  
  
  
</div>    </div>
  </div>
</section>      </div>
</div>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-12 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <section class="block block-mailchimp-signup block-mailing-list block-mailchimp-signup-mailing-list odd" id="block-mailchimp-signup-mailing-list">
  <div class="block-inner clearfix">
              <h2 class="block-title"><span>Mailing List</span></h2>
            
    <div class="content clearfix">
      <form class="mailchimp-signup-subscribe-form" action="/" method="post" id="mailchimp-signup-subscribe-block-mailing-list-form" accept-charset="UTF-8"><div><div class="mailchimp-signup-subscribe-form-description">Sign up for the latest Spanish legal news</div><div id="mailchimp-newsletter-136db1a6d0-mergefields" class="mailchimp-newsletter-mergefields"><div class="form-item form-type-textfield form-item-mergevars-EMAIL">
  <label for="edit-mergevars-email">Email Address <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-mergevars-email" name="mergevars[EMAIL]" value="" size="25" maxlength="128" class="form-text required" />
</div>
<div class="form-item form-type-textfield form-item-mergevars-FNAME">
  <label for="edit-mergevars-fname">First Name </label>
 <input type="text" id="edit-mergevars-fname" name="mergevars[FNAME]" value="" size="25" maxlength="128" class="form-text" />
</div>
<div class="form-item form-type-textfield form-item-mergevars-LNAME">
  <label for="edit-mergevars-lname">Last Name </label>
 <input type="text" id="edit-mergevars-lname" name="mergevars[LNAME]" value="" size="25" maxlength="128" class="form-text" />
</div>
<div class="form-item form-type-select form-item-mergevars-MMERGE3">
  <label for="edit-mergevars-mmerge3">I am interested in... </label>
 <select id="edit-mergevars-mmerge3" name="mergevars[MMERGE3]" class="form-select"><option value="" selected="selected"></option><option value="Property News">Property News</option><option value="Succession News">Succession News</option><option value="Debt and Asset Recovery News">Debt and Asset Recovery News</option></select>
</div>
</div><input type="hidden" name="form_build_id" value="form-Sq16pioIh4wft1__KOOePlwknlkWqdFzdVWlBp_-c3Q" />
<input type="hidden" name="form_id" value="mailchimp_signup_subscribe_block_mailing_list_form" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Submit" class="form-submit" /></div></div></form>    </div>
  </div>
</section><div class="block block-block block-1 block-block-1 even block-without-title" id="block-block-1">

  <a class="twitter-timeline" href="https://twitter.com/EshkeriGrau" data-widget-id="690532150668562433">Tweets by @EshkeriGrau</a>
  </div><section class="block block-eandg-block-placement block-footer-block block-eandg-block-placement-footer-block odd" id="block-eandg-block-placement-footer-block">
  <div class="block-inner clearfix">
              <h2 class="block-title"><span>Spanish Legal Advice In Plain English</span></h2>
            
    <div class="content clearfix">
      
    <div class="contact-details">
      <div class="contact-email"><label>Email:</label><span> <a href="mailto:info@solicitorsinspain.com">info@solicitorsinspain.com</a></span></div>
      <div class="contact-london"><label>London:</label><span> +44 (0)20 3478 1420</span></div>
      <div class="contact-tarragona"><label>Tarragona:</label><span> +34 977 249 960</span></div>
    </div>
  
    <div class="sra-logo">
      <div style="max-width:200px;max-height:119px;"><div style="position: relative;padding-bottom: 59.1%;height: auto;overflow: hidden;"><iframe frameborder="0" scrolling="no" allowTransparency="true" src="https://cdn.yoshki.com/iframe/55845r.html" style="border:0px; margin:0px; padding:0px; backgroundColor:transparent; top:0px; left:0px; width:100%; height:100%; position: absolute;"></iframe></div></div>
    </div>
      </div>
  </div>
</section>  </div>
</div><div class="grid-12 region region-footer-second" id="region-footer-second">
  <div class="region-inner region-footer-second-inner">
    <div class="block block-menu block-menu-bottom-menu block-menu-menu-bottom-menu odd block-without-title" id="block-menu-menu-bottom-menu">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="menu"><li class="first leaf"><a href="/content/contact-us">Contact us</a></li>
<li class="leaf"><a href="/about-us">About us</a></li>
<li class="leaf"><a href="/find-us">Find us</a></li>
<li class="leaf"><a href="/legal-notice">Legal Notice</a></li>
<li class="leaf"><a href="/complaints">Complaints</a></li>
<li class="last leaf"><a href="/sitemap" title="">Site Map</a></li>
</ul>      <div id="copyright-notice">© Eshkeri & Grau Ltd.&nbsp;2019</div>
    </div>
  </div>
</div><div class="block block-eandg-block-placement block-contact-block block-eandg-block-placement-contact-block even block-without-title" id="block-eandg-block-placement-contact-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      
        <h4 class="block-title"><a href="/content/contact-us">Contact</a></h4>
          </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <div class="region region-page-bottom" id="region-page-bottom">
  <div class="region-inner region-page-bottom-inner">
      </div>
</div><script type="text/javascript">
<!--//--><![CDATA[//><!--
var impliedConsent = impliedConsent || {}; impliedConsent.q = impliedConsent.q || [];
impliedConsent.q.push(["init", {"noticeText":"\u003Cp\u003EWe use cookies as set out in our privacy policy. By using this website, you agree we may place these cookies on your device.\u003C\/p\u003E\n","confirmText":"Close","validateByClick":true}]);
//--><!]]>
</script>
<script type="text/javascript" src="https://www.solicitorsinspain.com/sites/default/files/js/js_5idECjjAo-X5YdkT65CaIiodkWmZlZv-WjSkHlWhoYk.js"></script>
  <!-- Google Code for Remarketing Tag -->
  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 995954147;
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
  <div style="display:inline;">
  <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/995954147/?guid=ON&amp;script=0"/>
  </div>
  </noscript>
  <script type="text/javascript" src="https://secure.lope4refl.com/js/154424.js" ></script>
  <noscript><img alt="" src="https://secure.lope4refl.com/154424.png" style="display:none;" /></noscript>
</body>
</html>