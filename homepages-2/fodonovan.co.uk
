<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.fodonovan.co.uk/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<meta name="description" content="Psychotherapy should feel supportive &amp; challenging. Working together we will identify your negative thinking patterns to open your life to new possibilities." />
<meta name="keywords" content="therapy wimbledon, counselling wimbledon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.fodonovan.co.uk/" />
<link rel="shortlink" href="https://www.fodonovan.co.uk/" />
  <title>Therapist & Counsellor | Wimbledon & Borough | London | F O'Donovan</title>  
  <style type="text/css" media="all">
@import url("https://www.fodonovan.co.uk/modules/system/system.base.css?py4jjo");
@import url("https://www.fodonovan.co.uk/modules/system/system.menus.css?py4jjo");
@import url("https://www.fodonovan.co.uk/modules/system/system.messages.css?py4jjo");
</style>
<style type="text/css" media="all">
@import url("https://www.fodonovan.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?py4jjo");
</style>
<style type="text/css" media="all">
@import url("https://www.fodonovan.co.uk/sites/all/modules/ctools/css/ctools.css?py4jjo");
@import url("https://www.fodonovan.co.uk/sites/all/modules/responsive_dropdown_menus/theme/responsive-dropdown-menus.css?py4jjo");
</style>
<style type="text/css" media="all">
@import url("https://www.fodonovan.co.uk/sites/all/themes/omega/alpha/css/alpha-reset.css?py4jjo");
@import url("https://www.fodonovan.co.uk/sites/all/themes/omega/alpha/css/alpha-mobile.css?py4jjo");
@import url("https://www.fodonovan.co.uk/sites/all/themes/omega/omega/css/formalize.css?py4jjo");
@import url("https://www.fodonovan.co.uk/sites/all/themes/custom_theme_1/css/global.css?py4jjo");
</style>

<!--[if (lt IE 9)&(!IEMobile)]>
<style type="text/css" media="all">
@import url("https://www.fodonovan.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_fluid/normal/alpha-fluid-normal-12.css?py4jjo");
</style>
<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)">
@import url("https://www.fodonovan.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_fluid/normal/alpha-fluid-normal-12.css?py4jjo");
</style>
<!--<![endif]-->
  <script type="text/javascript" src="https://www.fodonovan.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/misc/drupal.js?py4jjo"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/sites/all/modules/spamspan/spamspan.js?py4jjo"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/sites/all/modules/google_analytics/googleanalytics.js?py4jjo"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-146894561-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/sites/all/modules/responsive_dropdown_menus/theme/responsive-dropdown-menus.js?py4jjo"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/sites/all/themes/omega/omega/js/jquery.formalize.js?py4jjo"></script>
<script type="text/javascript" src="https://www.fodonovan.co.uk/sites/all/themes/omega/omega/js/omega-mediaqueries.js?py4jjo"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"custom_theme_1","theme_token":"rowT5rSmox0dp9-rPLNKGv7CR0JVcwubSGwiZFm41JE","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/spamspan\/spamspan.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/modules\/responsive_dropdown_menus\/theme\/responsive-dropdown-menus.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/responsive_dropdown_menus\/theme\/responsive-dropdown-menus.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/custom_theme_1\/css\/global.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_fluid\/normal\/alpha-fluid-normal-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_fluid\/normal\/alpha-fluid-normal-12.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"responsive_dropdown_menus":{"menu-info":"Info","main-menu":"Main menu","management":"Management","navigation":"Navigation","user-menu":"User menu"},"omega":{"layouts":{"primary":"normal","order":["normal"],"queries":{"normal":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)"}}}});
//--><!]]>
</script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,700" rel="stylesheet">
</head>
<body class="html front not-logged-in page-node page-node- page-node-1 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix container-12">
    <div class="grid-12 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="branding-data clearfix">
                        <hgroup class="site-name-slogan">        
                                
        <h1 class="site-name"><a href="/" title="Home" class="active">Frank O'Donovan</a></h1>
                                        <h6 class="site-slogan">UKCP Psychotherapist &amp; Clinical Supervisor</h6>
              </hgroup>
          </div>
          </div>
</div>  </div>
</div><div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">  
  <div id="zone-menu" class="zone zone-menu clearfix container-12">
    <div class="grid-12 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
        <div class="block block-responsive-dropdown-menus block-main-menu block-responsive-dropdown-menus-main-menu odd block-without-title" id="block-responsive-dropdown-menus-main-menu">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul id="main-menu" class="main-menu responsive-menu links"><li id="menu-item-434" class="menu-item menu-item-primary first"><a href="/issues">Issues</a></li>
<li id="menu-item-438" class="menu-item menu-item-primary"><a href="/how-therapy-works">How Therapy Works</a></li>
<li id="menu-item-444" class="menu-item menu-item-primary"><a href="/clinical-supervision">Clinical Supervision</a></li>
<li id="menu-item-437" class="menu-item menu-item-primary"><a href="/about">About</a></li>
<li id="menu-item-435" class="menu-item menu-item-primary"><a href="/fees">Fees</a></li>
<li id="menu-item-436" class="menu-item menu-item-primary last"><a href="/contact">Contact</a></li>
</ul>    </div>
  </div>
</div>  </div>
</div>
  </div>
</div><div id="zone-header-wrapper" class="zone-wrapper zone-header-wrapper clearfix">  
  <div id="zone-header" class="zone zone-header clearfix container-12">
    <div class="grid-12 region region-header-first" id="region-header-first">
  <div class="region-inner region-header-first-inner">
    <div class="block block-views front-banner-text-overlay block-quote-block block-views-quote-block odd block-without-title" id="block-views-quote-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-quote view-id-quote view-display-id-block view-dom-id-d782b4316c17f2682bfc06a7aedee9cb">
        
  
  
      <div class="view-content">
        <div>
      
          <p class="quote">You cannot create experience. You must undergo it.
</p>
<p class="quote-author">Albert Camus</p>    </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div>
</div>  </div>
</div>  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-12">    
        
        <div class="grid-12 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                <h1 class="title" id="page-title">Home</h1>
                        <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <article about="/home" typeof="foaf:Document" class="node node-page node-published node-not-promoted node-not-sticky author-moniquehanford odd clearfix" id="node-page-1">
        <span property="dc:title" content="Home" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>    
  
  <div class="content clearfix">
    
  <div class="field-body">
    <p>Psychotherapy should feel supportive but also challenging - how else will your life take on a different course? Working together we will identify your negative thinking patterns and work on solutions that can open your life to new possibilities.</p>
  </div>
  </div>
  
  <div class="clearfix">
          <nav class="links node-links clearfix"></nav>
    
      </div>
</article>    </div>
  </div>
</div><div class="block block-block block-5 block-block-5 even block-without-title" id="block-block-5">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="front-box-wrapper">
<div class="box one"><a href="issues">Issues</a></div>
<div class="box two"><a href="how-therapy-works">How Therapy Works</a></div>
<div class="box three"><a href="clinical-supervision">Clinical Supervision</a></div>
</div>
    </div>
  </div>
</div>      </div>
</div>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-4 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <section class="block block-block block-1 block-block-1 odd" id="block-block-1">
  <div class="block-inner clearfix">
              <h2 class="block-title">Contact</h2>
            
    <div class="content clearfix">
      <p>E: <span class="spamspan"><span class="u">frank</span> [at] <span class="d">fodonovan.co.uk</span></span><br />
T: 07807 051162</p>
    </div>
  </div>
</section>  </div>
</div><div class="grid-4 region region-footer-second" id="region-footer-second">
  <div class="region-inner region-footer-second-inner">
    <section class="block block-block block-2 block-block-2 odd" id="block-block-2">
  <div class="block-inner clearfix">
              <h2 class="block-title">Locations</h2>
            
    <div class="content clearfix">
      <p class="first">Coombe Lane<br />
Wimbledon<br />
London SW20 0QT</p>
<p>Borough High St<br />
London SE1 1LL</p>
    </div>
  </div>
</section>  </div>
</div><div class="grid-4 region region-footer-third" id="region-footer-third">
  <div class="region-inner region-footer-third-inner">
    <section class="block block-menu block-menu-info block-menu-menu-info odd" id="block-menu-menu-info">
  <div class="block-inner clearfix">
              <h2 class="block-title">Info</h2>
            
    <div class="content clearfix">
      <ul class="menu"><li class="first leaf"><a href="/about" title="">About</a></li>
<li class="leaf"><a href="/clinical-supervision" title="">Clinical Supervision</a></li>
<li class="leaf"><a href="/contact" title="">Contact</a></li>
<li class="leaf"><a href="/fees" title="">Fees</a></li>
<li class="leaf"><a href="/how-therapy-works" title="">How Therapy Works</a></li>
<li class="leaf"><a href="/issues" title="">Issues</a></li>
<li class="last leaf"><a href="/articles" title="">Articles</a></li>
</ul>    </div>
  </div>
</section>  </div>
</div><div class="grid-12 region region-footer-fourth" id="region-footer-fourth">
  <div class="region-inner region-footer-fourth-inner">
    <div class="block block-views block-professional-logos-block block-views-professional-logos-block odd block-without-title" id="block-views-professional-logos-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-professional-logos view-id-professional_logos view-display-id-block view-dom-id-2fbb06976b803e54bdba8d9194b9ea0c">
        
  
  
      <div class="view-content">
        <div>
      
          <span><img typeof="foaf:Image" src="https://www.fodonovan.co.uk/sites/default/files/styles/professional_logo/public/universities-psychotherapy-and-counselling-association.png?itok=xfsKiY6p" width="177" height="60" alt="Universities Psychotherapy and Counselling Association" title="Universities Psychotherapy and Counselling Association" /></span>    </div>
  <div>
      
          <span><img typeof="foaf:Image" src="https://www.fodonovan.co.uk/sites/default/files/styles/professional_logo/public/uk-council-for-psychotherapy.jpg?itok=eq-63Whg" width="111" height="60" alt="UK Council for Psychotherapy" title="UK Council for Psychotherapy" /></span>    </div>
  <div>
      
          <span><img typeof="foaf:Image" src="https://www.fodonovan.co.uk/sites/default/files/styles/professional_logo/public/society-for-existential-analysis.png?itok=Ee_JRTpc" width="135" height="60" alt="Society for Existential Analysis" title="Society for Existential Analysis" /></span>    </div>
  <div>
      
          <span><img typeof="foaf:Image" src="https://www.fodonovan.co.uk/sites/default/files/styles/professional_logo/public/professional-standards-authority.png?itok=P4r_Cij7" width="124" height="60" alt="Professional Standards Authority" title="Professional Standards Authority" /></span>    </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div>
</div><div class="block block-copyright-block block-copyright-block block-copyright-block-copyright-block even block-without-title" id="block-copyright-block-copyright-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>Copyright © Frank O&#039;Donovan, 2018-2019.</p>
    </div>
  </div>
</div><div class="block block-block credit block-4 block-block-4 odd block-without-title" id="block-block-4">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>Website design by <a href="https://www.pinkspider.co.uk" target="_blank">Pink Spider</a></p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <div class="region region-page-bottom" id="region-page-bottom">
  <div class="region-inner region-page-bottom-inner">
      </div>
</div></body>
</html>