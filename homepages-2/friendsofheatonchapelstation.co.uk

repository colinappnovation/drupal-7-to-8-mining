<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="https://www.friendsofheatonchapelstation.co.uk/xmlrpc.php" />
	<title>The Friends of Heaton Chapel Station Improving Our Local Station</title>

	<!-- Meta Tag Manager -->
	<meta name="google-site-verification" content="CKBR2TGtx6q_xedoh2tKbuHHXtXHmASV1Wic94khp-8" />
	<!-- / Meta Tag Manager -->

<!-- This site is optimized with the Yoast SEO plugin v11.7 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="The Friends of Heaton Chapel Station are dedicated to the promotion and improvement of our local station in Stockport, Greater Manchester"/>
<link rel="canonical" href="https://www.friendsofheatonchapelstation.co.uk/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="The Friends of Heaton Chapel Station Improving Our Local Station" />
<meta property="og:description" content="The Friends of Heaton Chapel Station are dedicated to the promotion and improvement of our local station in Stockport, Greater Manchester" />
<meta property="og:url" content="https://www.friendsofheatonchapelstation.co.uk/" />
<meta property="og:site_name" content="Friends Of Heaton Chapel Station" />
<meta property="og:image" content="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l1600.jpg" />
<meta property="og:image:secure_url" content="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l1600.jpg" />
<meta property="og:image:width" content="840" />
<meta property="og:image:height" content="544" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="The Friends of Heaton Chapel Station are dedicated to the promotion and improvement of our local station in Stockport, Greater Manchester" />
<meta name="twitter:title" content="The Friends of Heaton Chapel Station Improving Our Local Station" />
<meta name="twitter:site" content="@HeatonChplStatn" />
<meta name="twitter:image" content="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l1600.jpg" />
<meta name="twitter:creator" content="@HeatonChplStatn" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.friendsofheatonchapelstation.co.uk/#organization","name":"Friends Of Heaton Chapel Station","url":"https://www.friendsofheatonchapelstation.co.uk/","sameAs":["https://www.facebook.com/groups/325588554161377/","https://www.pinterest.co.uk/coircat/friends-of-heaton-chapel-station/","https://twitter.com/HeatonChplStatn"],"logo":{"@type":"ImageObject","@id":"https://www.friendsofheatonchapelstation.co.uk/#logo","url":"https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/FoHCS_Logo_Wide_Bar_2019_XL_maroon.png","width":714,"height":215,"caption":"Friends Of Heaton Chapel Station"},"image":{"@id":"https://www.friendsofheatonchapelstation.co.uk/#logo"}},{"@type":"WebSite","@id":"https://www.friendsofheatonchapelstation.co.uk/#website","url":"https://www.friendsofheatonchapelstation.co.uk/","name":"Friends Of Heaton Chapel Station","publisher":{"@id":"https://www.friendsofheatonchapelstation.co.uk/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.friendsofheatonchapelstation.co.uk/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://www.friendsofheatonchapelstation.co.uk/#primaryimage","url":"https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l1600.jpg","width":840,"height":544},{"@type":"WebPage","@id":"https://www.friendsofheatonchapelstation.co.uk/#webpage","url":"https://www.friendsofheatonchapelstation.co.uk/","inLanguage":"en-US","name":"The Friends of Heaton Chapel Station Improving Our Local Station","isPartOf":{"@id":"https://www.friendsofheatonchapelstation.co.uk/#website"},"about":{"@id":"https://www.friendsofheatonchapelstation.co.uk/#organization"},"primaryImageOfPage":{"@id":"https://www.friendsofheatonchapelstation.co.uk/#primaryimage"},"datePublished":"2019-01-15T11:14:41+00:00","dateModified":"2019-03-23T10:42:43+00:00","description":"The Friends of Heaton Chapel Station are dedicated to the promotion and improvement of our local station in Stockport, Greater Manchester"}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Friends Of Heaton Chapel Station &raquo; Feed" href="https://www.friendsofheatonchapelstation.co.uk/feed/" />
<link rel="alternate" type="application/rss+xml" title="Friends Of Heaton Chapel Station &raquo; Comments Feed" href="https://www.friendsofheatonchapelstation.co.uk/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.friendsofheatonchapelstation.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='wp-block-library-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='pdfemb-gutenberg-block-backend-js-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/pdf-embedder/css/pdfemb-blocks.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='siteorigin-panels-front-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/siteorigin-panels/css/front-flex.min.css?ver=2.10.6' type='text/css' media='all' />
<link rel='stylesheet' id='vantage-style-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/style.css?ver=1.10' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/fontawesome/css/font-awesome.css?ver=4.6.2' type='text/css' media='all' />
<link rel='stylesheet' id='siteorigin-mobilenav-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/inc/mobilenav/css/mobilenav.css?ver=1.10' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-responsive-tables-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/tablepress-responsive-tables/css/responsive.dataTables.min.css?ver=1.5' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/tablepress/css/default.min.css?ver=1.9.2' type='text/css' media='all' />
<!--[if !IE]><!-->
<link rel='stylesheet' id='tablepress-responsive-tables-flip-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/tablepress-responsive-tables/css/tablepress-responsive-flip.min.css?ver=1.5' type='text/css' media='all' />
<!--<![endif]-->
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/jquery.flexslider.min.js?ver=2.1'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/jquery.touchSwipe.min.js?ver=1.6.6'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/jquery.theme-main.min.js?ver=1.10'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/jquery.fitvids.min.js?ver=1.0'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/html5.min.js?ver=3.7.3'></script>
<![endif]-->
<!--[if (gte IE 6)&(lte IE 8)]>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/js/selectivizr.min.js?ver=1.0.2'></script>
<![endif]-->
<script type='text/javascript'>
/* <![CDATA[ */
var mobileNav = {"search":{"url":"https:\/\/www.friendsofheatonchapelstation.co.uk","placeholder":"Search"},"text":{"navigate":"Menu","back":"Back","close":"Close"},"nextIconUrl":"https:\/\/www.friendsofheatonchapelstation.co.uk\/wp-content\/themes\/vantage\/inc\/mobilenav\/images\/next.png","mobileMenuClose":"<i class=\"fa fa-times\"><\/i>"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/themes/vantage/inc/mobilenav/js/mobilenav.min.js?ver=1.10'></script>
<link rel='https://api.w.org/' href='https://www.friendsofheatonchapelstation.co.uk/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.friendsofheatonchapelstation.co.uk/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.friendsofheatonchapelstation.co.uk/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.0.3" />
<link rel='shortlink' href='https://www.friendsofheatonchapelstation.co.uk/' />
<link rel="alternate" type="application/json+oembed" href="https://www.friendsofheatonchapelstation.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.friendsofheatonchapelstation.co.uk%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.friendsofheatonchapelstation.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.friendsofheatonchapelstation.co.uk%2F&#038;format=xml" />
<!-- <meta name="NextGEN" version="3.2.10" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1" />		<style type="text/css">
			.so-mobilenav-mobile + * { display: none; }
			@media screen and (max-width: 480px) { .so-mobilenav-mobile + * { display: block; } .so-mobilenav-standard + * { display: none; } .site-navigation #search-icon { display: none; } }
		</style>
			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
			<style type="text/css" media="screen">
		#footer-widgets .widget { width: 100%; }
		#masthead-widgets .widget { width: 100%; }
	</style>
	                <style type="text/css" media="all"
                       id="siteorigin-panels-layouts-head">/* Layout 154 */ #pgc-154-0-0 { width:100%;width:calc(100% - ( 0 * 30px ) ) } #pl-154 #panel-154-0-0-0 , #pl-154 #panel-154-1-0-0 , #pl-154 #panel-154-1-1-0 , #pl-154 #panel-154-1-2-0 {  } #pg-154-0 , #pl-154 .so-panel { margin-bottom:35px } #pgc-154-1-0 , #pgc-154-1-1 , #pgc-154-1-2 { width:33.3333%;width:calc(33.3333% - ( 0.666666666667 * 30px ) ) } #pl-154 .so-panel:last-child { margin-bottom:0px } #pg-154-0.panel-no-style, #pg-154-0.panel-has-style > .panel-row-style , #pg-154-1.panel-no-style, #pg-154-1.panel-has-style > .panel-row-style { -webkit-align-items:flex-start;align-items:flex-start } @media (max-width:780px){ #pg-154-0.panel-no-style, #pg-154-0.panel-has-style > .panel-row-style , #pg-154-1.panel-no-style, #pg-154-1.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-154-0 > .panel-grid-cell , #pg-154-0 > .panel-row-style > .panel-grid-cell , #pg-154-1 > .panel-grid-cell , #pg-154-1 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-154-1-0 , #pgc-154-1-1 { margin-bottom:35px } #pl-154 .panel-grid-cell { padding:0 } #pl-154 .panel-grid .panel-grid-cell-empty { display:none } #pl-154 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } </style><style type="text/css" id="customizer-css">#masthead .hgroup h1, #masthead.masthead-logo-in-menu .logo > h1 { font-size: 36px; color: #ffffff } #masthead .hgroup .support-text, #masthead .hgroup .site-description { color: #ffffff } #page-title, article.post .entry-header h1.entry-title, article.page .entry-header h1.entry-title { color: #ffffff } .entry-content, #comments .commentlist article .comment-meta a { color: #ffffff } .entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4, .entry-content h5, .entry-content h6, #comments .commentlist article .comment-author a, #comments .commentlist article .comment-author, #comments-title, #reply-title, #commentform label { color: #ffffff } #comments-title, #reply-title { border-bottom-color: #ffffff } #masthead .hgroup { padding-top: 0px; padding-bottom: 0px } #masthead .hgroup .logo { text-align: center } #masthead .hgroup .logo, #masthead .hgroup .site-logo-link { float: none } #masthead .hgroup .logo img, #masthead .hgroup .site-logo-link img { display: block; margin: 0 auto } .entry-content a, .entry-content a:visited, article.post .author-box .box-content .author-posts a:hover, #secondary a, #secondary a:visited, #masthead .hgroup a, #masthead .hgroup a:visited, .comment-form .logged-in-as a, .comment-form .logged-in-as a:visited { color: #25cec3 } .entry-content a, .textwidget a { text-decoration: none } .entry-content a:hover, .textwidget a:hover { text-decoration: underline } .main-navigation { background-color: #780006 } .main-navigation a { color: #ffffff } .main-navigation ul ul { background-color: #780006 } .main-navigation ul ul a { color: #ffffff } .main-navigation ul li:hover > a, .main-navigation ul li:focus > a, #search-icon #search-icon-icon:hover, #search-icon #search-icon-icon:focus { background-color: #63001a } .main-navigation ul ul li:hover > a { background-color: #63001a } .main-navigation ul li.current-menu-item > a, .main-navigation ul li.current_page_item > a  { background-color: #63001a } #search-icon #search-icon-icon { background-color: #8e003d } .main-navigation ul li { font-size: 15px } .mobile-nav-frame { background-color: #780006 } .mobile-nav-frame .title { background-color: #780006 } .mobile-nav-frame form.search input[type=search] { background-color: #780006 } .mobile-nav-frame ul { background-color: #780006; border-color: #780006 } a.button, button, html input[type="button"], input[type="reset"], input[type="submit"], .post-navigation a, #image-navigation a, article.post .more-link, article.page .more-link, .paging-navigation a, .woocommerce #page-wrapper .button, .woocommerce a.button, .woocommerce .checkout-button, .woocommerce input.button, #infinite-handle span { background: #780006; border-color: #ffffff } a.button:hover, button:hover, html input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .post-navigation a:hover, #image-navigation a:hover, article.post .more-link:hover, article.page .more-link:hover, .paging-navigation a:hover, .woocommerce #page-wrapper .button:hover, .woocommerce a.button:hover, .woocommerce .checkout-button:hover, .woocommerce input.button:hover, .woocommerce input.button:disabled:hover, .woocommerce input.button:disabled[disabled]:hover, #infinite-handle span:hover { background: #780006 } a.button:hover, button:hover, html input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .post-navigation a:hover, #image-navigation a:hover, article.post .more-link:hover, article.page .more-link:hover, .paging-navigation a:hover, .woocommerce #page-wrapper .button:hover, .woocommerce a.button:hover, .woocommerce .checkout-button:hover, .woocommerce input.button:hover, #infinite-handle span:hover { opacity: 0.75; border-color: #ffffff } a.button:focus, button:focus, html input[type="button"]:focus, input[type="reset"]:focus, input[type="submit"]:focus, .post-navigation a:focus, #image-navigation a:focus, article.post .more-link:focus, article.page .more-link:focus, .paging-navigation a:focus, .woocommerce #page-wrapper .button:focus, .woocommerce a.button:focus, .woocommerce .checkout-button:focus, .woocommerce input.button:focus, .woocommerce input.button:disabled:focus, .woocommerce input.button:disabled[disabled]:focus, #infinite-handle span:focus { background: #780006 } a.button:focus, button:focus, html input[type="button"]:focus, input[type="reset"]:focus, input[type="submit"]:focus, .post-navigation a:focus, #image-navigation a:focus, article.post .more-link:focus, article.page .more-link:focus, .paging-navigation a:focus, .woocommerce #page-wrapper .button:focus, .woocommerce a.button:focus, .woocommerce .checkout-button:focus, .woocommerce input.button:focus, #infinite-handle span:focus { opacity: 0.75; border-color: #ffffff } a.button, button, html input[type="button"], input[type="reset"], input[type="submit"], .post-navigation a, #image-navigation a, article.post .more-link, article.page .more-link, .paging-navigation a, .woocommerce #page-wrapper .button, .woocommerce a.button, .woocommerce .checkout-button, .woocommerce input.button, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled], .woocommerce input.button:disabled:hover, .woocommerce input.button:disabled[disabled]:hover, #infinite-handle span button { color: #ffffff } a.button, button, html input[type="button"], input[type="reset"], input[type="submit"], .post-navigation a, #image-navigation a, article.post .more-link, article.page .more-link, .paging-navigation a, .woocommerce #page-wrapper .button, .woocommerce a.button, .woocommerce .checkout-button, .woocommerce input.button, #infinite-handle span button { text-shadow: none } a.button, button, html input[type="button"], input[type="reset"], input[type="submit"], .post-navigation a, #image-navigation a, article.post .more-link, article.page .more-link, .paging-navigation a, .woocommerce #page-wrapper .button, .woocommerce a.button, .woocommerce .checkout-button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, #infinite-handle span { -webkit-box-shadow: none; -moz-box-shadow: none; box-shadow: none } #masthead-widgets .widget .widget-title { color: #ffffff } .widget_circleicon-widget .circle-icon-box .circle-icon:not(.icon-style-set) { background-color: #780006 } #masthead { background-color: #780006 } #main { background-color: #780006 } #colophon, body.layout-full { background-color: #780006 } </style><link rel="icon" href="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/cropped-friends_logo-4-32x32.jpg" sizes="32x32" />
<link rel="icon" href="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/cropped-friends_logo-4-192x192.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/cropped-friends_logo-4-180x180.jpg" />
<meta name="msapplication-TileImage" content="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/cropped-friends_logo-4-270x270.jpg" />
<link rel='stylesheet' id='so-css-vantage-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/so-css/so-css-vantage.css?ver=1551174795' type='text/css' media='all' />
<link rel='stylesheet' id='metaslider-nivo-slider-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/ml-slider/assets/sliders/nivoslider/nivo-slider.css?ver=3.13.1' type='text/css' media='all' property='stylesheet' />
<link rel='stylesheet' id='metaslider-public-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/ml-slider/assets/metaslider/public.css?ver=3.13.1' type='text/css' media='all' property='stylesheet' />
<link rel='stylesheet' id='metaslider-nivo-slider-default-css'  href='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/ml-slider/assets/sliders/nivoslider/themes/default/default.css?ver=3.13.1' type='text/css' media='all' property='stylesheet' />
</head>

<body class="home page-template page-template-home-panels page-template-home-panels-php page page-id-154 wp-custom-logo siteorigin-panels siteorigin-panels-before-js siteorigin-panels-home group-blog responsive layout-full no-js no-sidebar page-layout-default not-default-page mobilenav">


<div id="page-wrapper">

	
	
		<header id="masthead" class="site-header" role="banner">

	<div class="hgroup full-container ">

		
			<a href="https://www.friendsofheatonchapelstation.co.uk/" title="Friends Of Heaton Chapel Station" rel="home" class="logo">
				<img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/02/FoHCS_Logo_Wide_Bar_2019_XL_Burgundy.png"  class="logo-height-constrain"  width="714"  height="215"  alt="Friends Of Heaton Chapel Station Logo"  />			</a>
			
				<div class="support-text">
									</div>

			
		
	</div><!-- .hgroup.full-container -->

	
<nav role="navigation" class="site-navigation main-navigation primary use-sticky-menu">

	<div class="full-container">
				
					<div id="so-mobilenav-standard-1" data-id="1" class="so-mobilenav-standard"></div><div class="menu-fohcs-container"><ul id="menu-fohcs" class="menu"><li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-128"><a href="http://friendsofheatonchapelstation.co.uk"><span class="icon"></span>The Friends</a>
<ul class="sub-menu">
	<li id="menu-item-1133" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1133"><a href="https://www.friendsofheatonchapelstation.co.uk/what-fofhcs-do/">What FofHCS do</a></li>
	<li id="menu-item-1731" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1731"><a href="https://www.friendsofheatonchapelstation.co.uk/membership/">Annual Membership</a></li>
	<li id="menu-item-521" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-521"><a href="https://www.friendsofheatonchapelstation.co.uk/fofhcs-at-work/">Volunteers at work</a></li>
	<li id="menu-item-669" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-669"><a href="https://www.friendsofheatonchapelstation.co.uk/before-fohcs/">Before FofHCS</a></li>
	<li id="menu-item-610" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-610"><a href="https://www.friendsofheatonchapelstation.co.uk/the-travellers-library/">The Traveller&#8217;s Library</a></li>
</ul>
</li>
<li id="menu-item-187" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-187"><a href="#"><span class="icon"></span>The Station</a>
<ul class="sub-menu">
	<li id="menu-item-197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197"><a href="https://www.friendsofheatonchapelstation.co.uk/fac2/">Station Facilities</a></li>
	<li id="menu-item-846" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-846"><a href="https://www.friendsofheatonchapelstation.co.uk/disabled-access-lack-of/">Disabled Access    <BR>   (lack of)</a></li>
	<li id="menu-item-131" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-131"><a href="https://www.friendsofheatonchapelstation.co.uk/history/">History</a></li>
	<li id="menu-item-1505" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1505"><a href="https://www.friendsofheatonchapelstation.co.uk/passenger-statistics/">Passenger Statistics</a></li>
</ul>
</li>
<li id="menu-item-762" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-762"><a href="https://www.friendsofheatonchapelstation.co.uk/running-man/">The Sponsors</a></li>
<li id="menu-item-1728" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1728"><a href="https://www.friendsofheatonchapelstation.co.uk/supportus/">Support Us</a></li>
<li id="menu-item-208" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-208"><a href="https://www.friendsofheatonchapelstation.co.uk/news/">News</a></li>
<li id="menu-item-833" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-833"><a href="https://www.friendsofheatonchapelstation.co.uk/contact-us/">Contact Us</a></li>
</ul></div><div id="so-mobilenav-mobile-1" data-id="1" class="so-mobilenav-mobile"></div><div class="menu-mobilenav-container"><ul id="mobile-nav-item-wrap-1" class="menu"><li><a href="#" class="mobilenav-main-link" data-id="1"><span class="mobile-nav-icon"></span>Menu</a></li></ul></div>			</div>
</nav><!-- .site-navigation .main-navigation -->

</header><!-- #masthead .site-header -->

	
	
			<div id="main-slider" data-stretch="true">
			<div style="width: 100%; margin: 0 auto;" class="ml-slider-3-13-1 metaslider metaslider-nivo metaslider-162 ml-slider">
    
    <div id="metaslider_container_162">
        <div class='slider-wrapper theme-default'><div class='ribbon'></div><div id='metaslider_162' class='nivoSlider'><img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l1600-700x300.jpg" height="300" width="700" title="s-l1600" alt="" class="slider-162 slide-1436" /><img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/Snow_2010-700x300.jpg" height="300" width="700" title="Snow_2010" alt="" class="slider-162 slide-418" /><img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/s-l500-e1551175191350-462x198.jpg" height="300" width="700" title="victorian heaton chapel station" alt="" class="slider-162 slide-1434" /><img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/01/steamtrainjpg-615x263.jpg" height="300" width="700" title="steamtrainjpg" alt="" class="slider-162 slide-163" /><img src="https://www.friendsofheatonchapelstation.co.uk/wp-content/uploads/2019/02/slider4-700x300.jpg" height="300" width="700" title="slider4" alt="" class="slider-162 slide-1749" /></div></div>
        
    </div>
</div>		</div>
	
	<div id="main" class="site-main">
		<div class="full-container">
			
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<div class="entry-content">
			<div id="pl-154"  class="panel-layout" ><div id="pg-154-0"  class="panel-grid panel-no-style" ><div id="pgc-154-0-0"  class="panel-grid-cell" ><div id="panel-154-0-0-0" class="so-panel widget widget_headline-widget panel-first-child panel-last-child" data-index="0" >		<h1>The Friends of Heaton Chapel Station</h1>
		<div class="decoration"><div class="decoration-inside"></div></div>
		<h3>Dedicated to the promotion and improvement of our local station in Stockport, Greater Manchester</h3>
		</div></div></div><div id="pg-154-1"  class="panel-grid panel-no-style" ><div id="pgc-154-1-0"  class="panel-grid-cell" ><div id="panel-154-1-0-0" class="so-panel widget widget_circleicon-widget panel-first-child panel-last-child" data-index="1" >		<div class="circle-icon-box circle-icon-position-top circle-icon-hide-box circle-icon-size-large ">
							<div class="circle-icon-wrapper">
					<a href="http://friendsofheatonchapelstation.co.uk/what-fofhcs-do" class="link-icon" >					<div class="circle-icon icon-style-set" style="background-color: #ffffff">
						<div class="fa fa-train icon-color-set" style="color: #780006"></div>					</div>
					</a>				</div>
			
			<a href="http://friendsofheatonchapelstation.co.uk/what-fofhcs-do" class="link-title" >			<h4 style="color: #ffffff">What FofHCS do</h4>			</a>
			<p class="text" style="color: #ffffff">We are a group who are committed to the improvement of our local station.  We do this by gardening, painting, raising funds for improvements, and lobbying the relevant authorities.</p>							<a href="http://friendsofheatonchapelstation.co.uk/what-fofhcs-do" class="more-button" >More Info <i></i></a>
					</div>
		</div></div><div id="pgc-154-1-1"  class="panel-grid-cell" ><div id="panel-154-1-1-0" class="so-panel widget widget_circleicon-widget panel-first-child panel-last-child" data-index="2" >		<div class="circle-icon-box circle-icon-position-top circle-icon-hide-box circle-icon-size-large ">
							<div class="circle-icon-wrapper">
					<a href="http://friendsofheatonchapelstation.co.uk/fofhcs-at-work/" class="link-icon" >					<div class="circle-icon icon-style-set" style="background-color: #ffffff">
						<div class="fa fa-pagelines icon-color-set" style="color: #780006"></div>					</div>
					</a>				</div>
			
			<a href="http://friendsofheatonchapelstation.co.uk/fofhcs-at-work/" class="link-title" >			<h4 style="color: #ffffff">Volunteering with FofHCS</h4>			</a>
			<p class="text" style="color: #ffffff">We improve the station's aesthetics by landscaping, gardening, painting etc.   Why not come and help us?</p>							<a href="http://friendsofheatonchapelstation.co.uk/fofhcs-at-work/" class="more-button" >Find out How <i></i></a>
					</div>
		</div></div><div id="pgc-154-1-2"  class="panel-grid-cell" ><div id="panel-154-1-2-0" class="so-panel widget widget_circleicon-widget panel-first-child panel-last-child" data-index="3" >		<div class="circle-icon-box circle-icon-position-top circle-icon-hide-box circle-icon-size-large ">
							<div class="circle-icon-wrapper">
					<a href="http://friendsofheatonchapelstation.co.uk/membership/" class="link-icon" >					<div class="circle-icon icon-style-set" style="background-color: #ffffff">
						<div class="fa fa-gbp icon-color-set" style="color: #780006"></div>					</div>
					</a>				</div>
			
			<a href="http://friendsofheatonchapelstation.co.uk/membership/" class="link-title" >			<h4 style="color: #ffffff">Become a Member</h4>			</a>
			<p class="text" style="color: #ffffff">The annual subscription is £2.00 per person, renewed on 1st June, part years are rounded up.   <BR><BR> All the money is used to improve that station, buying paint, flowers, tools etc.</p>							<a href="http://friendsofheatonchapelstation.co.uk/membership/" class="more-button" >Become a member <i></i></a>
					</div>
		</div></div></div></div>		</div>
	</div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

					</div><!-- .full-container -->
	</div><!-- #main .site-main -->

	
	
	<footer id="colophon" class="site-footer" role="contentinfo">

			<div id="footer-widgets" class="full-container">
					</div><!-- #footer-widgets -->
	
	
	<div id="theme-attribution">A <a href="https://siteorigin.com">SiteOrigin</a> Theme</div>
</footer><!-- #colophon .site-footer -->

	
</div><!-- #page-wrapper -->


<!-- ngg_resource_manager_marker --><!--[if !IE]><!-->
<!--<![endif]-->
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-includes/js/wp-embed.min.js?ver=5.0.3'></script>
<script type='text/javascript' src='https://www.friendsofheatonchapelstation.co.uk/wp-content/plugins/ml-slider/assets/sliders/nivoslider/jquery.nivo.slider.pack.js?ver=3.13.1'></script>
<script type='text/javascript'>
var metaslider_162 = function($) {
            $('#metaslider_162').nivoSlider({ 
                boxCols:7,
                boxRows:5,
                pauseTime:3000,
                effect:"fade",
                controlNav:true,
                directionNav:true,
                pauseOnHover:true,
                animSpeed:600,
                prevText:"Previous",
                nextText:"Next",
                slices:15,
                manualAdvance:false
            });
            $(document).trigger('metaslider/initialized', '#metaslider_162');
        };
        var timer_metaslider_162 = function() {
            var slider = !window.jQuery ? window.setTimeout(timer_metaslider_162, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_162, 1) : metaslider_162(window.jQuery);
        };
        timer_metaslider_162();
</script>
<a href="#" id="scroll-to-top" class="scroll-to-top" title="Back To Top"><span class="vantage-icon-arrow-up"></span></a><script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js","");</script>
</body>
</html>
