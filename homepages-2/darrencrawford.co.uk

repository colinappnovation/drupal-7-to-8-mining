<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Darren Crawford - Web Developer</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendor/devicons/css/devicons.min.css" rel="stylesheet">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/resume.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none text-white" style="font-family: Saira Extra Condensed, serif; text-transform: uppercase; font-size: 1.6em;">Darren Crawford<img class="img-fluid img-profile rounded-circle ml-4" src="img/profile.jpg" alt="" style="height:50px" title="Take me to the Top"></span>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/profile.jpg" alt="" title="Take me to the Top">
        </span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about" title="What he is">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#experience" title="What he knows">Experience</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#education" title="What he has learned">Education</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#skills" title="What he can do">Skills</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#interests" title="What he likes">Interests</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#portfolio" title="What he has done">Portfolio</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
        <div class="my-auto">
          <h1 class="mb-0">Darren
            <span class="text-primary">Crawford</span>
          </h1>
          <div class="subheading mb-5">Ballymena, County Antrim, N. Ireland ·
            <a href="mailto:name@email.com">mail@darrencrawford.co.uk</a>
          </div>
          <p class="mb-5">I am experienced in creating responsive dynamic web systems, from the design stage to the implementation and hosting stages.</p>
		  <p class="mb-5">My ethos is that a good website starts with good code.<br/>
		  <img src="img/w3c-validated.png" alt="W3C HTML5 Validation pass" /></p>
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="experience">
        <div class="my-auto">
          <h2 class="mb-5">Experience</h2>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Web Programmer</h3>
              <div class="subheading mb-3">AVX Electronics</div>
              <p>Developing an online portal for a global network of salespersons.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">August 2017 - Present</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Web Developer, Digital Marketing & IT Manager</h3>
              <div class="subheading mb-3">McCartney & Crawford</div>
              <p>Managing the online presence and marketing for an established estate agency.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">January 2009 - July 2017</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Senior Web Developer</h3>
              <div class="subheading mb-3">Univeristy of Edinburgh Business School</div>
              <p>In charge of all dynamic online systems for this prestigious education department.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">October 2007 - January 2009</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Support Web Developer</h3>
              <div class="subheading mb-3">Edinburgh University HSS Web Team</div>
              <p>A part of a 6 strong web team who designed, developed and supported the websites for all schools in the HSS college.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">June 2006 - October 2007</span>
            </div>
          </div>

        </div>

      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="education">
        <div class="my-auto">
          <h2 class="mb-5">Education</h2>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Napier University, Edinburgh</h3>
              <div class="subheading mb-3">Bachelor of Engineering Honours (2:1)</div>
              <div>Internet Computing</div>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">September 2001 - June 2006</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Ballymena Academy Grammar School</h3>
              <div class="subheading mb-3">A-Levels</div>
              <p>Computing, Maths & Physics</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">September 1999 - June 2001</span>
            </div>
          </div>
		  
		  <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Ballymena Academy Grammar School</h3>
              <div class="subheading mb-3">GCSEs</div>
              <p>Additional Maths, Maths, Computing, Double Award Science, English, Media Studies, French & Geography</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">September 1993 - June 1999</span>
            </div>
          </div>
		  
		  <div class="my-auto">
		    <h2 class="mb-5">Certifications</h2>
		    <ul class="fa-ul mb-0">
			  <li>
			    <i class="fa-li fa fa-trophy text-warning"></i>
			    Global Edulink - Digital Marketing (2017)</li>
			  <li>
			    <i class="fa-li fa fa-trophy text-warning"></i>
			    Google - Online Marketing Fundamentals (2016)</li>
			  <li>
			    <i class="fa-li fa fa-trophy text-warning"></i>
			    Netskills Training - PHP & CSS (2007)</li>
		    </ul>
		  </div>

        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="skills">
        <div class="my-auto">
          <h2 class="mb-5">Skills</h2>

          <div class="subheading mb-3">Programming Languages &amp; Tools</div>
          <ul class="list-inline list-icons">
            <li class="list-inline-item">
              <i class="devicons devicons-php" title="PHP"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-html5" title="HTML 5"></i>
            </li>
            <li class="list-inline-item" title="CSS 3">
              <i class="devicons devicons-css3"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-javascript" title="Javascript"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-jquery" title="JQuery"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-bootstrap" title="Bootstrap"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-wordpress" title="Wordpress"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-mysql" title="MySQL"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-photoshop" title="Photoshop"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-zend" title="Zend"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-git" title="Git"></i>
            </li>
			<li class="list-inline-item">
              <i class="devicons devicons-composer" title="Composer"></i>
            </li>
          </ul>

          <div class="subheading mb-3">Workflow</div>
          <ul class="fa-ul mb-0">
            <li>
              <i class="fa-li fa fa-check"></i>
              Mobile-First, Responsive Design</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Cross Browser Testing &amp; Debugging</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Agile Development &amp; Scrum</li>
          </ul>
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="interests">
        <div class="my-auto">
          <h2 class="mb-5">Interests</h2>
          <p>Apart from being a web developer, I have a passion for music, both the creation and playing.</p>
          <p>Since 2016 I have been the head of programming for an annual local charity radio staion <a href="www.radiocracker.org.uk" title="Radio Cracker Ballymena">Radio Cracker</a>, and I also present a radio show.</p>
		      <p>When the radio station is not broadcasting, I enjoy music production using Logic X on an Apple iMac.</p>
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="portfolio">
        <div class="my-auto">
          <h2 class="mb-5">Portfolio</h2>
          <div class="row">
		    <div class="col-md-6">
			  <h4>AVX Online (user restriced portal)</h4>
			  <img src="img/avx.jpg" alt="AVX portal screenshot"  class="img-thumbnail">
			</div>
			<div class="col-md-6">
			    <ul>
			      <li>Creation and testing of web systems using OO PHP 7 with Zend Framework 3, HTML 5, CSS 3, Bootstrap 4, JQuery, dataTables, Ajax and JSON</li>
				  <li>Database interaction with IBM iSeries Databases using DB2  for iSQL</li>
				  <li>Git version control with Composer</li>
				  <li>Cross browser testing</li>
				  <li>Multiple Device & Resolution testing</li>
				  <li>Project progess meetings with management</li>
				  <li>Agile project management</li>
			    </ul>
			</div>
		  </div>
		   <div class="row">
		    <div class="col-md-6">
			  <h4><a href="http://www.asfengineering.com" title="ASF Engineering">www.asfengineering.com</a></h4>
			  <img src="img/asf.jpg" alt="ASF Engineering website screenshot"  class="img-thumbnail">
			</div>
			<div class="col-md-6">
			    <ul>
			      <li>Bespoke CMS website</li>
				  <li>HTML coding</li>
				  <li>PHP coding</li>
				  <li>CSS coding</li>
				  <li>Photoshop Editing</li>
				  <li>Apache Hosting</li>
			    </ul>
			</div>
		  </div>
		  <div class="row">
		    <div class="col-md-6">
			  <h4><a href="http://www.danceselecta.co.uk" title="Dance Selecta - Radio Show">www.danceselecta.co.uk</a></h4>
			  <img src="img/danceselecta.jpg" alt="Dance Selecta website screenshot"  class="img-thumbnail">
			</div>
			<div class="col-md-6">
			    <ul>
			      <li>Bootstrap 4 template</li>
				  <li>HTML 5 coding</li>
				  <li>PHP coding</li>
				  <li>Photoshop Editing</li>
				  <li>Apache Hosting</li>
			    </ul>
			</div>
		  </div>
		  <div class="row">
		    <div class="col-md-6">
			  <h4><a href="http://www.dar-cee.co.uk" title="DJ Dar-Cee - Local DJ">www.dar-cee.co.uk</a></h4>
			  <img src="img/dar-cee.jpg" alt="DJ Dar-Cee website screenshot"  class="img-thumbnail">
			</div>
			<div class="col-md-6">
			    <ul>
			      <li>Wordpress Template</li>
				  <li>CMS management</li>
				  <li>Widget installation</li>
				  <li>Photoshop Editing</li>
				  <li>Apache Hosting</li>
			    </ul>
			</div>
		  </div>
		  <div class="row">
		    <div class="col-md-6">
			  <h4><a href="http://www.darrencrawford.co.uk" title="Darren Crawford - Web Developer">www.darrencrawford.co.uk</a></h4>
			  <img src="img/darrencrawford.jpg" alt="Darren Crawford website screenshot"  class="img-thumbnail">
			</div>
			<div class="col-md-6">
			    <ul>
			      <li>Bootstrap 4 template</li>
				  <li>HTML 5 coding</li>
				  <li>Photoshop Editing</li>
				  <li>Apache Hosting</li>
			    </ul>
			</div>
		  </div>
        </div>
      </section>

    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/resume.min.js"></script>

  </body>

</html>
