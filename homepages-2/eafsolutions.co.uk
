<!DOCTYPE html SYSTEM "about:legacy-compat">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="googlebot" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-touch-fullscreen" content="YES">
<link rel="apple-touch-icon" href="/images/mobile/apple-touch-icon.png">
<meta property="og:site_name" content="EA Financial Solutions">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.eafsolutions.co.uk/">
<link rel="canonical" href="https://www.eafsolutions.co.uk/">
<meta property="og:title" content="Welcome to EA Financial Planning Limited ">
<title>Welcome to EA Financial Planning Limited </title>
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="Welcome to EA Financial Planning Limited ">
<meta name="description" content="Welcome to EA Financial Planning Limited       EA Financial Solutions Limited are an Independent Financial Advisory firm based in North London.  We have been...">
<meta property="og:description" content="Welcome to EA Financial Planning Limited       EA Financial Solutions Limited are an Independent Financial Advisory firm based in North London.  We have been...">
<meta name="twitter:description" content="Welcome to EA Financial Planning Limited       EA Financial Solutions Limited are an Independent Financial Advisory firm based in North London.  We have been...">
<link href="/favicon.ico" rel="shortcut icon">
<link href="//fonts.googleapis.com/css?family=Nunito:300,700" rel="stylesheet">
<!--[info:API/4]-->

<link rel="stylesheet" type="text/css" media="screen" href="/css/compiled/main2.css">
<link rel="stylesheet" type="text/css" media="print" href="/css/print.css">

<meta name="robots" content="index,follow">
<!--select custom CSS for node NB:inherits all linked css on parent nodes, from root node to ensure same styling-->

<link rel="stylesheet" type="text/css" media="screen" href="/css/home.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script type="text/javascript" src="/global/cms4/js/cms_minified/cms_core.js"></script><!--Compiled JS file last generated --><script type="text/javascript" src="/javascript/compiled/all.js"></script><!--Yahoo analytics is not enabled on this site.--><script>
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-5B8MFNC');
            </script><script>var isMobile = false,isTablet = true,isDesktop = false;</script>
</head>
<body class="section-home section type-home alt-ver-spd" id="home">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5B8MFNC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!--UdmComment--><div id="wrapper" class="wrapper">
<header id="header" class="header cmstarget"><div id="header-inner" class="header-inner inner">
<a id="company-logo-link" class="company-logo-link" href="/"><img id="company-logo" class="company-logo" alt="EA Financial Solutions" src="/images/logos/logo.svg"></a><ul class="calls-to-action" id="calls-to-action">
<li class="action-search search has-icon">
<span class="icon-content">search</span><svg class="icon icon-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-search"></use></svg>
</li>
<li class="action enquiry has-icon">
<a class="icon-link" href="/site/contact/make-an-enquiry/" data-trigger="popup">Make an enquiry</a><svg class="icon icon-enquiry"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-enquiry"></use></svg>
</li>
<li class="action profile has-icon">
<a class="icon-link" href="https://eafsolutions.mypfp.co.uk">Portal</a><svg class="icon icon-profile"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-profile"></use></svg>
</li>
<li class="action phone has-icon">
<span class="icon-content">020 8446 3231</span><svg class="icon icon-phone"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-phone"></use></svg>
</li>
</ul>
<div class="search-container" id="search-container"><form method="GET" action="/site/search/" id="search-form" name="search-form" class="search-form">
<label class="search-label" for="search-input">Search</label><div class="search-bar">
<input placeholder="Search..." type="search" class="search-input" id="search-input" value="" name="q" size="10"><input type="hidden" name="m" value="any"><label class="search-button has-icon"><button id="search-submit" name="search-submit" type="submit" class="search-submit"></button><span class="hidden icon-link">Search</span><svg class="icon icon-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-search"></use></svg></label>
</div>
</form></div>
<nav id="nav" class="nav">
<ul class="menu menu-level-1" data-level="1">
<li class="item item-home"><a class="link level-1 active" href="/">Home</a></li>
<li class="item item-about has-sub-menu">
<a class="link level-1" href="/site/about/">About Us</a><ul class="sub-menu sub-menu-about menu-level-2" data-level="2">
<li class="item item-our-fees"><a class="link level-2" href="/site/about/our-fees/">Our Fees</a></li>
<li class="item item-our-advice"><a class="link level-2" href="/site/about/our-advice/">Our Advice</a></li>
<li class="item item-providers"><a class="link level-2" href="/site/about/providers/">Other Providers</a></li>
<li class="item item-testimonials"><a class="link level-2" href="/site/about/testimonials/">Testimonials</a></li>
<li class="item item-intelliflo-pfp"><a class="link level-2" href="/site/about/intelliflo-pfp/">Intelliflo PFP</a></li>
</ul>
</li>
<li class="item item-our-services has-sub-menu">
<a class="link level-1" href="/site/our-services/">Our Services</a><ul class="sub-menu sub-menu-our-services menu-level-2" data-level="2">
<li class="item item-wealth-management-in-london"><a class="link level-2" href="/site/our-services/wealth-management-in-london/">Wealth Management</a></li>
<li class="item item-financial-planning-in-london"><a class="link level-2" href="/site/our-services/financial-planning-in-london/">Financial Planning</a></li>
<li class="item item-retirement-planning"><a class="link level-2" href="/site/our-services/retirement-planning/">Retirement Planning</a></li>
<li class="item item-company-pensions-in-london"><a class="link level-2" href="/site/our-services/company-pensions-in-london/">Company Pensions</a></li>
<li class="item item-savings-and-investments"><a class="link level-2" href="/site/our-services/savings-and-investments/">Savings and Investments</a></li>
<li class="item item-family-and-business-protection"><a class="link level-2" href="/site/our-services/family-and-business-protection/">Family and Business Protection</a></li>
</ul>
</li>
<li class="item item-publications has-sub-menu">
<a class="link level-1" href="/site/publications/">Publications</a><ul class="sub-menu sub-menu-publications menu-level-2" data-level="2">
<li class="item item-business_planning_publications"><a class="link level-2" href="/site/publications/business_planning_publications/">Business Planning</a></li>
<li class="item item-facts_figures"><a class="link level-2" href="/site/publications/facts_figures/">Facts and Figures</a></li>
<li class="item item-financial_planning_publications"><a class="link level-2" href="/site/publications/financial_planning_publications/">Financial Planning</a></li>
<li class="item item-investment_publications"><a class="link level-2" href="/site/publications/investment_publications/">Investments</a></li>
<li class="item item-property_mortgages_publications"><a class="link level-2" href="/site/publications/property_mortgages_publications/">Property and Mortgages</a></li>
<li class="item item-retirement_planning_publications"><a class="link level-2" href="/site/publications/retirement_planning_publications/">Retirement Planning</a></li>
<li class="item item-tax_publications"><a class="link level-2" href="/site/publications/tax_publications/">Tax</a></li>
</ul>
</li>
<li class="item item-people"><a class="link level-1" href="/site/people/">Our People</a></li>
<li class="item item-news"><a class="link level-1" href="/site/news/">News</a></li>
<li class="item item-blog has-sub-menu">
<a class="link level-1" href="/site/blog/">Blog</a><ul class="sub-menu sub-menu-blog menu-level-2" data-level="2">
<li class="item item-pensions-blog"><a class="link level-2" href="/site/blog/pensions-blog/">Pensions</a></li>
<li class="item item-eaf-blog"><a class="link level-2" href="/site/blog/eaf-blog/">Blog</a></li>
</ul>
</li>
<li class="item item-contact"><a class="link level-1" href="/site/contact/financial-advisors-in-london/">Contact Us</a></li>
</ul>
</nav>
</div></header><div id="hero" class="hero cmstarget">
<!--/UdmComment--><div id="hero-inner" class="hero-inner inner">
<div class="cms tagline-container block" id="Object_610295"><div class="content"><p class="tagline">An all-round financial advisory service</p></div></div>
<div class="home-services-container" id="home-services-container"><div class="home-services inner cmstarget" id="home-services" data-cms-target-desc="Home Services"><div class="cms default block" id="Object_610296"><div class="content"><ul><li><a href="/site/our-services/wealth-management-in-london/"><span>Wealth Management</span></a></li><li><a href="/site/our-services/financial-planning-in-london/"><span>Financial Planning</span></a></li><li><a href="/site/our-services/savings-and-investments/"><span>Saving and Investements</span></a></li><li><a href="/site/our-services/retirement-planning/"><span>Retirement Planning </span></a></li><li><a href="/site/our-services/company-pensions-in-london/"><span>Company Pensions/ Autoenrolments</span></a></li><li><a href="/site/our-services/family-and-business-protection/"><span>Family Business and Protection</span></a></li></ul></div></div></div></div>
</div>
<!--UdmComment-->
</div>
<main id="main" class="main inner"><section id="container-centre" class="cmstarget column centre"><!--/UdmComment--><div class="cms home-intro-slider block" id="Object_610131"><div class="content"><h1>Welcome to <strong>EA Financial Planning Limited</strong></h1><ul><li>EA Financial Solutions Limited are an Independent Financial Advisory firm based in North London.&nbsp; We have been delivering the highest quality financial planning for individuals and companies since 2003.</li><li>We are directly authorised and regulated by the Financial Conduct Authority and pride ourselves for providing the highest quality financial advice.&nbsp; Our clients value our knowledge, transparency, integrity and commitment.</li><li>Please give us a call on&nbsp;0208 4463231&nbsp;or email <a href="/cdn-cgi/l/email-protection#264f4840496643474055494a53524f49485508454908534d"><span class="__cf_email__" data-cfemail="6900070f06290c080f1a06051c1d0006071a470a06471c02">[email&#160;protected]</span></a>&nbsp;to arrange an initial meeting to discuss your requirements.</li></ul></div></div>
<!--UdmComment--><!--prevent self closing div--></section><aside id="container-right" class="cmstarget right column"><!--/UdmComment--><!--UdmComment--><!--prevent self closing div--></aside><aside id="container-left" class="cmstarget left column"><!--UdmComment--><!--prevent self closing div--></aside></main><div id="footer-wrapper" class="footer-wrapper">
<div id="pre-footer" class="pre-footer cmstarget">
<!--/UdmComment--><div class="team-form-container" id="team-form-container"><div class="team-form inner" id="team-form">
<div class="team-container cmstarget" id="team-container" data-cms-target-desc="Team Block"></div>
<div class="form-container cmstarget" id="form-container" data-cms-target-desc="Form Block"><div class="cms block block-form" id="Object_610294">
<div class="block-header"><p class="title h3">How can we help?</p></div>
<div class="content"><p class="form-intro">We are here to help. Please, fill in the form below and we&#39;ll get back to you as soon as possible.</p><form action="/cms/formmail/" class="form form-question" id="form-question" method="post" name="form-question"><div class="hidden"><input name="recipient" type="hidden" value="info@eafsolutions.co.uk" /> <input name="subject" type="hidden" value="Ask A Question" /> <input name="url" type="hidden" value="/site/contact/" /> <input name="redirect" type="hidden" value="/site/contact/mail_success.html" /> <input name="attachment" type="hidden" value="csv" /> <label for="channel-form-question">Channel</label> <input id="channel-form-question" name="channel" type="text" /> <label for="referrer-form-question">Referrer</label> <input id="referrer-form-question" name="referrer" type="text" /> <label for="device-form-question">Device</label> <input id="device-form-question" name="device" type="text" /></div><a class="close-form" href="#" id="close-form"><span>Close</span> <svg class="icon icon-close"> <use xlink:href="/images/icons/icon-library.svg#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"></use> </svg> </a><fieldset class="fieldset" id="form-details"><div class="row required"><label for="name">Name</label> <input class="input" id="name" name="name" placeholder="Your name" type="text" /> <span class="helper">Please enter your name</span></div><div class="row required"><label for="email">Email</label> <input autocorrect="off" class="input email" id="email" name="email" placeholder="Your email" type="email" /> <span class="helper">Please enter your email address</span></div><div class="form-reveal"><div class="row required"><label for="telephone">Telephone</label> <input class="input" id="telephone" name="telephone" placeholder="Your phone" type="tel" /> <span class="helper">Please enter your telephone number</span></div></div><div class="form-reveal"><div class="row"><label for="question">Your question</label><textarea class="input" id="question" name="question" placeholder="Your question"></textarea> <span class="helper">Please enter a question</span></div><div class="row required"><label for="hear-about-us">How did you hear about us?</label><div class="select-wrapper"><select class="select" id="hear-about-us" name="hear-about-us"><option value="">How did you hear about us?</option><option value="Google Search">Google Search</option><option value="Bing Search">Bing Search</option><option value="Google Advert">Google Advert</option><option value="Law Society Website">Law Society Website</option><option value="Personal/Friend Recommendation">Personal/Friend Recommendation</option><option value="Professional Recommendation">Professional Recommendation</option><option value="Social Media">Social Media</option><option value="Thomson Local">Thomson Local</option><option value="Yellow Pages/Yell.com">Yellow Pages/Yell.com</option><option value="Can't Remember">Can&#39;t Remember</option> </select></div> <span class="helper">Please let us know how you heard about us</span></div><p class="privacy-policy-text">Please read <a href="/site/help/privacy/">our Privacy Policy</a></p><div class="row captcha-wrapper" id="captcha-wrapper"><label class="captcha-label" for="captcha"><span class="hidden">Captcha</span> <img alt="Captcha" src="/cms/captcha/securityimage.png" /> </label> <input autocapitalize="none" autocomplete="off" autocorrect="off" class="input captcha" id="captcha" name="captcha" placeholder="Verification code" type="text" /> <span class="helper">Please enter the verification code</span></div></div><div class="row form-submit"><input class="submit" id="submit" name="B1" type="submit" value="Send form" /></div></fieldset></form></div>
</div></div>
</div></div>
<div class="latest-news-container" id="latest-news-container"><div class="latest-news inner cmstarget" id="latest-news" data-cms-target-desc="Latest News">
<div class="cms block promo-block" id="Object_610293">
<div class="block-header"><p class="title h3">Our Services</p></div>
<div class="content"><p>We are an all-round financial advisory service in Finchley, London offering an initial thirty minutes consultation free of charge. Find out more about our services and how we can help you.</p><p><a class="read-more" href="/site/our-services/">Find out more</a></p></div>
</div>
<div class="cms block promo-block" id="Object_610297">
<div class="block-header"><p class="title h3">Latest News</p></div>
<div class="content"><p>Keep up to date with the latest financial industry-related news by reading our informative and detailed articles.</p><p><a class="read-more" href="/site/news/">Read More</a></p></div>
</div>
</div></div>
<div class="testimonials-block-container" id="testimonials-block-container"><div id="testimonials-block-default" class="testimonials-block-default cmstarget"><div class="cms default block" id="Object_610168" data-cms-global-obj="1"><div class="content"><blockquote><p>Minesh Patel is considerate of what my needs and potential needs are, and advises me accordingly. His advice has generated very good profits for me since I started to use him as my adviser.</p><cite>Richard, Avon</cite></blockquote><blockquote><p>Minesh always gives sound logical advice, with very clear communication of ideas and reasoning for suggested planning. Approachable and a pleasure to work with.</p><cite>Tim, West Essex</cite></blockquote><blockquote><p>Always contactable, his advice has always been sound.</p><cite>Stuart, Hertfordshire</cite></blockquote></div></div></div></div>
<div class="news-block-container" id="news-block-container"><div class="news-block inner cmstarget" id="news-block" data-cms-target-desc="Footer News Block">
<a class="anchorTag" id="610300"><!--prevents self closing div--></a><div class="cms list news-items footer-block footer-articles">
<h2>Articles</h2>
<article class="item news ln_blogs789_795"><div class="list-header">
<h2 class="title h3"><a href="/site/blog/pensions-blog/tory-autumn-conference-2014">Tory Autumn Conference 2014</a></h2>
<div id="date_ctrl_610300_1" class="sortdate" style="display:none">2015-07-06 00:00</div>
<time id="dt6103001" datetime="2015-07-06 00:00" class="datetime"><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">var dt=doDate('2015-07-06 00:00','d-NNN','1');if(document.getElementById('dt6103001').innerHTML !=dt){document.getElementById('dt6103001').innerHTML=dt};</script></time>
</div>
<div class="content latestnews7"> George Osborne is continuing the coalition&rsquo;s pension revolution with the latest proposal...</div>
<div class="meta"><a class="read-more" href="/site/blog/pensions-blog/tory-autumn-conference-2014"><span>Read more</span></a></div></article><div class="link"><a class="morelink" href="/site/about/"><span>Read More</span></a></div>
</div>
<div class="cms block doc footer-guides  footer-block" id="Object_610299">
<div class="block-header"><p class="title h3">Guides</p></div>
<div class="content"><h3>Our knowledge bank</h3><p>We offer useful information and guides on a wide range of financial topics to help business owners, managers and private investors understand the tax system.</p><div class="link"><a class="morelink" href="/site/publications"><span>Read More</span></a></div></div>
</div>
<a class="anchorTag" id="610291"><!--prevents self closing div--></a><div class="cms list news-items footer-block footer-blogs">
<h2>Blogs</h2>
<article class="item news ln_blogs790_795"><div class="list-header">
<h2 class="title h3"><a href="/site/blog/eaf-blog/adviser-views">Adviser Views </a></h2>
<div id="date_ctrl_610291_1" class="sortdate" style="display:none">2019-10-01 17:18</div>
<time id="dt6102911" datetime="2019-10-01 17:18" class="datetime"><script type="text/javascript">var dt=doDate('2019-10-01 17:18','d-NNN','1');if(document.getElementById('dt6102911').innerHTML !=dt){document.getElementById('dt6102911').innerHTML=dt};</script></time>
</div>
<div class="content latestnews7"> We asked financial advisers what they thought of multi-asset funds and how they were using them in...</div>
<div class="meta"><a class="read-more" href="/site/blog/eaf-blog/adviser-views"><span>Read more</span></a></div></article><div class="link"><a class="morelink" href="/site/about/"><span>Read More</span></a></div>
</div>
</div></div>
<div id="pre-footer-inner" class="pre-footer-inner inner"></div>
<!--UdmComment-->
</div>
<footer id="footerContainer" class="cmstarget footer"><div class="top-footer-container" id="top-footer-container"><div class="top-footer inner cmstarget" id="top-footer" data-cms-target-desc="Top Footer"><div class="cms default block" id="Object_610162" data-cms-global-obj="1"><div class="content"><p>EA Financial Solutions Limited is authorised and regulated by the Financial Conduct Authority and is entered on the <a href="https://www.fca.org.uk/register" target="_blank">Financial Services Register</a>&nbsp;under reference 219600. EA Financial Solutions Limited is registered in England and Wales, No. 4207655. Registered Address: 869 High Road, Finchley, London N12 8QA. The information contained within this site is subject to the UK regulatory regime and is therefore targeted primarily at consumers based in the UK. Please read our <a href="/cms/document/EAFS_GDPR_Privacy_Notice_20180521.pdf">Privacy Notice</a> before completing any enquiry form or before sending an email to us.&nbsp;</p></div></div></div></div>
<div id="footer-inner" class="footer-inner inner">
<div class="footer-menu footer-block"><div class="footer-menu footer-block"></div></div>
<div class="footer-offices footer-block"><ul class="office-list office-multi"><li class="office"><ul class="office-meta">
<li class="office-title"><a class="title" href="/site/contact/financial-advisors-in-london/">London</a></li>
<li class="office-address"><ul>
<li class="address-1">869 High Road</li>
<li class="address-2">Finchley</li>
<li class="city">London</li>
<li class="post-code">N12 8QA</li>
</ul></li>
<li class="office-contact"><ul>
<li class="phone">020 8446 3231</li>
<li class="email"><a href="/cdn-cgi/l/email-protection#d7beb9b1b897b2b6b1a4b8bba2a3beb8b9a4f9b4b8f9a2bc"><span class="__cf_email__" data-cfemail="f0999e969fb0959196839f9c8584999f9e83de939fde859b">[email&#160;protected]</span></a></li>
</ul></li>
</ul></li></ul></div>
<div class="cms footer-links footer-block block" id="Object_610167" data-cms-global-obj="1"><div class="content"><ul class="footer-links"><li><a href="/site/help/accessibility/">Accessibility</a></li><li><a href="/site/help/complaints/">Complaints Procedure</a></li><li><a href="/site/help/disclaimer/">Legal Disclaimer</a></li><li><a href="/cms/document/EAFS_GDPR_Privacy_Notice_20191126.pdf">Privacy Notice</a></li><li><a data-trigger="cookies" href="/site/help/privacy_help.html">Cookie Policy</a></li><li><a href="/site/sitemap/">Sitemap</a></li></ul></div></div>
<div class="cms footer-block block" id="Object_610164" data-cms-global-obj="1"><div class="content"><p>&copy; Copyright &#169; 2019 EA Financial Solutions Ltd</p><p><a href="https://www.fca.org.uk/" target="FCA"><img alt="FCA" src="/cms/photo/logos/FCA_logo.png" style="margin-top:1.5rem" /></a></p></div></div>
</div>
<!--prevent self closing div--></footer><!--/UdmComment-->
</div>
</div>
<div id="cookies" class="modal cookies

				
						cookies-none
					">
<div class="modal-inner inner">
<div class="modal-close has-icon" id="cookies-close">
<span class="icon-link"><span class="hidden">Close</span></span><svg class="icon icon-close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-close"></use></svg>
</div>
<div class="modal-content">
<p class="h1 title">Our Cookie Policy</p>
<p class="cookies-text cookies-allowed" id="cookies-allowed">
						On
						<span id="cookies-date-allowed">-</span>
						you agreed to accept cookies from this website - thank you.
					</p>
<p class="cookies-text cookies-denied" id="cookies-denied">
						On
						<span id="cookies-date-denied">-</span>
						you disabled cookies on this website - some functions will not operate as intended.
					</p>
<p class="cookies-text cookies-none" id="cookies-none">
We use a range of cookies to improve your experience of our site.
						<a title="e-Privacy Directive" href="/site/help/privacy_help.html">Find out more.</a></p>
<ul class="cookies-buttons" id="cookies-buttons">
<li class="button deny" id="cookies-deny"><a class="cookie-button" href="/cms/cookieprivacy/deny">Disable</a></li>
<li class="button allow" id="cookies-allow"><a class="cookie-button" href="/cms/cookieprivacy/allow">Accept</a></li>
</ul>
</div>
</div>
<span class="hidden" id="cookieValue">unspecified</span>
</div>
<div class="overlay" id="overlay"></div>
<!--UdmComment--><div id="mobile-ui" class="mobile-ui active">
<ul class="mobile-ui-buttons">
<li class="mobile-ui-button"><button id="ui-button-search" data-panel="search" class="ui-button ui-button-search"><span class="ui-button-inner"><svg class="icon icon-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-search"></use></svg><span class="icon-content">search</span></span></button></li>
<li class="mobile-ui-button"><button id="ui-button-phone" data-panel="phone" class="ui-button ui-button-phone"><span class="ui-button-inner"><svg class="icon icon-phone"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-phone"></use></svg><span class="icon-content">phone</span></span></button></li>
<li class="mobile-ui-button"><button id="ui-button-enquiry" data-panel="enquiry" class="ui-button ui-button-enquiry"><span class="ui-button-inner"><svg class="icon icon-enquiry"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-enquiry"></use></svg><span class="icon-content">enquiry</span></span></button></li>
<li class="mobile-ui-button"><button id="ui-button-menu" data-panel="menu" class="ui-button ui-button-menu"><span class="ui-button-inner"><svg class="icon icon-menu"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-menu"></use></svg><span class="icon-content">menu</span></span></button></li>
</ul>
<div id="mobile-ui-panels-container" class="mobile-ui-panels-container">
<div class="mobile-ui-panel-close has-icon"><svg class="icon icon-close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-close"></use></svg></div>
<ul id="mobile-ui-panels" class="mobile-ui-panels">
<li id="mobile-ui-panel-search" class="mobile-ui-panel mobile-ui-panel-search">
<p class="title h1">Search site</p>
<div class="mobile-ui-panel-content"><div class="search-container"><form method="GET" action="/site/search/" name="search-form" class="search-form">
<label class="search-label" for="mobile-ui-search-input">Search</label><div class="search-bar">
<input placeholder="Search our site" type="search" class="search-input" id="mobile-ui-search-input" value="" name="q" size="10"><input type="hidden" name="ul" value=""><input type="hidden" name="m" value="any"><label class="search-button has-icon"><button name="search-submit" type="submit" class="search-submit"></button><span class="hidden icon-link">Search</span><svg class="icon icon-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons/icon-library.svg#icon-search"></use></svg></label>
</div>
</form></div></div>
</li>
<li id="mobile-ui-panel-phone" class="mobile-ui-panel mobile-ui-panel-phone">
<p class="title h1">Contact our office</p>
<div class="mobile-ui-panel-content block"><ul class="office-list office-multi"><li class="office"><ul class="office-meta">
<li class="office-title"><a class="title" href="/site/contact/financial-advisors-in-london/">London</a></li>
<li class="office-contact"><ul><li class="phone">020 8446 3231</li></ul></li>
</ul></li></ul></div>
</li>
<li id="mobile-ui-panel-enquiry" class="mobile-ui-panel mobile-ui-panel-enquiry">
<p class="title h1">Make an enquiry</p>
<div class="mobile-ui-panel-content block"><div class="loading">
<span class="dot"></span><span class="dot"></span><span class="dot"></span>
</div></div>
</li>
<li id="mobile-ui-panel-menu" class="mobile-ui-panel mobile-ui-panel-menu"><nav id="mobile-ui-nav" class="mobile-ui-nav mobile-ui-panel-content" data-depth="1" data-parent=""><div class="loading">
<span class="dot"></span><span class="dot"></span><span class="dot"></span>
</div></nav></li>
</ul>
</div>
</div>
<!--/UdmComment-->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script></body>
</html>
