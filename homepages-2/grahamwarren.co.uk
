<!DOCTYPE html>
  <!--[if IEMobile 7]><html class="no-js ie iem7" lang="en" dir="ltr"><![endif]-->
  <!--[if lte IE 6]><html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
  <!--[if (IE 7)&(!IEMobile)]><html class="no-js ie lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
  <!--[if IE 8]><html class="no-js ie lt-ie9" lang="en" dir="ltr"><![endif]-->
  <!--[if (gte IE 9)|(gt IEMobile 7)]><html class="no-js ie" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><![endif]-->
  <!--[if !IE]><!--><html class="no-js" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
  <meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/content/about-graham-warren-ltd" />
<link rel="shortlink" href="/node/1" />
<link rel="shortcut icon" href="https://www.grahamwarren.co.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="profile" href="https://www.w3.org/1999/xhtml/vocab" />
<meta name="HandheldFriendly" content="true" />
<meta name="MobileOptimized" content="width" />
<meta http-equiv="cleartype" content="on" />
<link rel="apple-touch-icon-precomposed" href="https://www.grahamwarren.co.uk/sites/all/themes/omega/omega/apple-touch-icon-precomposed-144x144.png" sizes="144x144" />
<link rel="apple-touch-icon-precomposed" href="https://www.grahamwarren.co.uk/sites/all/themes/omega/omega/apple-touch-icon-precomposed-114x114.png" sizes="114x114" />
<link rel="apple-touch-icon-precomposed" href="https://www.grahamwarren.co.uk/sites/all/themes/omega/omega/apple-touch-icon-precomposed-72x72.png" sizes="72x72" />
<link rel="apple-touch-icon-precomposed" href="https://www.grahamwarren.co.uk/sites/all/themes/omega/omega/apple-touch-icon-precomposed.png" />
<meta name="viewport" content="width=device-width" />
  <title>About Graham Warren Ltd | Graham Warren Ltd</title>
  <link type="text/css" rel="stylesheet" href="https://www.grahamwarren.co.uk/sites/default/files/css/css_Anlmk-ldYMPvI1kQSva8aJ9oAQ7aYtkb0AD2FQEnFA8.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.grahamwarren.co.uk/sites/default/files/css/css_mPUd9APjGvmzsMfgVCx2rrRMgEhxwRsPDWmEXXoEk38.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.grahamwarren.co.uk/sites/default/files/css/css_7QTuzGU8EHnmzloQcoqCa8xHn0rWg9VDH2rGNjHIujI.css" media="all" />

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.grahamwarren.co.uk/sites/default/files/css/css_AHcXRV9UCHoDEGMBegmTgS2fipuX-POb-ittYYVR2pI.css" media="all" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="https://www.grahamwarren.co.uk/sites/default/files/css/css_GCy509PWJNRLuu6PoxFmkRvvgCpNQDuYQst9KVboJuk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor:regular|Monda:regular&amp;subset=latin" media="all" />
  <script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_EpdE-P7KB89RybxATieT_rmub5Evz48clHFKFbJ7ASw.js"></script>
<script>document.createElement( "picture" );</script>
<script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_Tik8PIaz_eQ5I4FMzmjkWoPEs9jKBgTSauo1jgsNa6g.js"></script>

<!--[if (gte IE 6)&(lte IE 8)]>
<script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_l1iEl-hY65c79QTBBcl2tmNsnQFuMrbhOOFZUO_dkyw.js"></script>
<![endif]-->
<script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_6BG5e4flEAMDyinnTOlVGecZ5egGM3zZQNQiH0wGnD0.js"></script>
<script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_UBB2Pn9IkE4ixWpaSBB_Hp31NgmDBMYKVy_AmCEWA1o.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","extlink":{"extTarget":"_blank","extClass":"ext","extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":"","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":"mailto","mailtoLabel":"(link sends e-mail)"},"responsive_dropdown_menus":{"main-menu":"Main menu","management":"Management","navigation":"Navigation","user-menu":"User menu"}});</script>
</head>
<body class="html front not-logged-in page-node page-node- page-node-1 node-type-page">
  <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
    <div class="l-page">
  <header class="l-header" role="banner">
    <div class="l-branding">
      
      
          </div>

          <div class="l-region l-region--navigation">
    <div id="block-responsive-dropdown-menus-main-menu" class="block block--responsive-dropdown-menus block--responsive-dropdown-menus-main-menu">
        <div class="block__content">
    <ul id="main-menu" class="main-menu responsive-menu links"><li id="menu-item-717" class="menu-item active-trail active menu-item-primary first"><a href="/content/about-graham-warren-ltd" title="" class="active">About</a></li>
<li id="menu-item-718" class="menu-item menu-item-primary"><a href="/content/previous-experience" title="">Experience</a></li>
<li id="menu-item-728" class="menu-item menu-item-primary"><a href="/content/projects">Projects</a></li>
<li id="menu-item-727" class="menu-item menu-parent menu-item-primary"><span title="" class="nolink">Services +</span><ul class="sub-menu tier-two"><li id="menu-item-1120" class="menu-item menu-item-secondary first"><a href="/content/certificates-lawful-use-development">Certificates Of Lawful Use</a></li>
<li id="menu-item-1117" class="menu-item menu-item-secondary"><a href="/content/planning-permission">Planning Permission</a></li>
<li id="menu-item-1119" class="menu-item menu-item-secondary"><a href="/content/agricultural-occupancy-conditions">Agricultural Occupancy</a></li>
<li id="menu-item-1118" class="menu-item menu-item-secondary"><a href="/content/planning-appeals">Planning Appeals</a></li>
<li id="menu-item-1122" class="menu-item menu-item-secondary"><a href="/content/listed-buildings">Listed Buildings</a></li>
<li id="menu-item-1116" class="menu-item menu-item-secondary"><a href="/content/localism-and-promoting-development-interests">Localism</a></li>
<li id="menu-item-1121" class="menu-item menu-item-secondary"><a href="/content/enforcement">Enforcement</a></li>
<li id="menu-item-1123" class="menu-item menu-item-secondary last"><a href="/content/litigation">Litigation</a></li>
</ul></li>
<li id="menu-item-719" class="menu-item menu-item-primary last"><a href="/contact" title="">Contact</a></li>
</ul>  </div>
</div>
  </div>
  </header>

  <div class="l-main">
    <div class="l-content" role="main">
        <div class="l-region l-region--highlighted">
    <div id="block-block-2" class="block block--block block--block-2">
        <div class="block__content">
    <div class="hero_wrapper_about">
<div class="hero_overlay">
<h1>Graham Warren Ltd</h1>
<h3>Town Planning &amp; Environmental Consultants</h3>
</div>
</div>
  </div>
</div>
  </div>
            <a id="main-content"></a>
                                                <article about="/content/about-graham-warren-ltd" typeof="foaf:Document" role="article" class="node node--page node--promoted node--full node--page--full">
      <header>
                  <span property="dc:title" content="About Graham Warren Ltd" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>    </header>
  
  
  <div class="node__content">
    <div class="field field--name-body field--type-text-with-summary field--label-hidden"><div class="field__items"><div class="field__item even" property="content:encoded"><div class="header_section">
<div class="p1 center">The Objective of the Business is to cost effectively achieve planning permission for environmentally sustainable development in a highly structured administrative and political climate, so adding value to land and buildings.</div>
</div>
<div class="main_section justify">
Graham Warren Ltd was established in 2005 and is a niche practice specialising in producing Development and Environmental solutions in the context of Town Planning and related legislation. Clientele includes commercial organisations and private individuals. Projects range from major planned urban extensions to issues affecting a single dwelling house, commercial &amp; industrial property.
<h2>Graham Warren</h2>
<p>Graham is a Master of Arts in Town and Regional Planning, a Fellow of the Royal Institution of Chartered Surveyors and a Member of the Royal Town Planning Institute. He has extensive experience in the business beginning in local government in London. In 1981 he co-founded Chapman Warren which went on to become one of the UK’s leading Town Planning and Development consultancies employing 120 staff in eight offices. In September 2000 the business was sold to the RPS Group.</p>
<p>Graham has relinquished his wider corporate responsibilities but continues in business as a niche practice with established access to other skills. ie. Master Planning, Urban Design, Landscaping, Transportation etc.</p>
<h2>Sandie Warren</h2>
<p>Sandie is a qualified Personal Assistant with many years experience and administers the day to day running of the business.
</p></div>
</div></div></div>  </div>

    </article>
          </div>

          </div>

  <footer class="l-footer" role="contentinfo">
      <div class="l-region l-region--footer">
    <div id="block-block-1" class="block block--block block--block-1">
        <div class="block__content">
    <div class="foot1">
<h3>Registered Office</h3>
<p>18 Bath Road, Swindon<br />
Wiltshire, SN1 4BA<br />
+44 1304 806 850
</p></div>
<div class="foot2">
<h3>Legal Information</h3>
<p>©2015 Graham Warren Ltd.<br />
A Company Registered in England &amp; Wales.<br />
Reg. No. 6514741. Regulated by RICS.
</p></div>
<div class="foot3">
<h3>Site Designed By</h3>
<p><a href="http://www.thedesignshop.biz">...thedesignshop...</a>
</p></div>
  </div>
</div>
  </div>
  </footer>
</div>
  <script src="https://www.grahamwarren.co.uk/sites/default/files/js/js_7Ukqb3ierdBEL0eowfOKzTkNu-Le97OPm-UqTS5NENU.js"></script>
</body>
</html>
