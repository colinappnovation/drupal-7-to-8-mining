<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link href="/en" rel="alternate" hreflang="en" />
<link rel="shortlink" href="/en/node/22057" />
<link rel="shortcut icon" href="https://www.hanson-sustainability.co.uk/sites/all/themes/hc_hanson/images/icon/hanson.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta content="ie=edge, chrome=1" http-equiv="x-ua-compatible" />
<meta http-equiv="ImageToolbar" content="false" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.hanson-sustainability.co.uk/en/node/22057" />
<meta property="og:title" content="Sustainability 2019" />
<meta property="og:description" content="Introduction | Progress update | 2018 highlights and awards | Corporate governanceManaging sustainability | Our business | Scope and structure" />
<meta property="og:image" content="https://www.hanson-sustainability.co.uk/sites/default/files/assets/images/ad/f6/enabling-1920x720.jpg" />

  <title>Hanson Sustainability 2019 | Hanson Sustainability 2019</title>

  <link type="text/css" rel="stylesheet" href="https://www.hanson-sustainability.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" />
<link type="text/css" rel="stylesheet" href="https://www.hanson-sustainability.co.uk/sites/default/files/css/css_ZwQ4JegYk9_vB7LtvEKjfjeVcLzDAa88SNWeshZo5Jw.css" />
<link type="text/css" rel="stylesheet" href="https://www.hanson-sustainability.co.uk/sites/default/files/css/css_lys7hSdWhGs7DjI4d6I4iXZuk9WqaeA4M8ZRWyCr-NE.css" />
<link type="text/css" rel="stylesheet" href="https://www.hanson-sustainability.co.uk/sites/default/files/css/css_T5FpHa12-tChU448_v_8A7I68gblylToldZd1ubbJRk.css" />
<link type="text/css" rel="stylesheet" href="https://www.hanson-sustainability.co.uk/sites/default/files/css/css_laX6QLTjEU6nygKyPdhbb43X0dXfI-lGPKVVn1uhWGM.css" />

  
  
    <!--[IF IE 11]> -->
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es5%2Ces6%2CArray.prototype.includes%2Cfetch%2CCustomEvent%2CElement.prototype.closest"></script>
    <!-- <![ENDIF]-->

  <script type="text/javascript" src="https://www.hanson-sustainability.co.uk/sites/default/files/js/js_fr3riGu8isvo4MHwyc679X6XLRaCZF0rhDt90G8y2oI.js"></script>
<script type="text/javascript">(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-58988266-13", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>

  <script type="text/javascript">
    var MTIProjectId='1a126532-99b9-489b-94e6-dd8a82f2506a';
    (function() {
      var mtiTracking = document.createElement('script');
      mtiTracking.type='text/javascript';
      mtiTracking.async='true';
      mtiTracking.src='https://www.hanson-sustainability.co.uk/sites/all/themes/hc_base/js/vendor/mtiFontTrackingCode.js';
      (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild( mtiTracking );
    })();
  </script>
</head>

<body class="html front not-logged-in no-sidebars page-node page-node- page-node-22057 node-type-hub i18n-en lang-en" >

  
  
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner">
      <div class="off-canvas-content">
        <div class="skip-link">
          <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
        </div>
        <div id="page" class="page" role="document">
  <section class="section-header">
    <div class="row">
  <div class="large-12 columns">
    <div class="logo-container">
      <button class="main-menu-toggle show-for-small"></button>
      <div class="logo-background">
        <a class="logo-link" href="/en">
          <img class="logo" src="https://www.hanson-sustainability.co.uk/sites/all/themes/hc_base/images/logo/hc_hanson.svg" alt="Hanson Sustainability Report 2019"/>
        </a>
      </div>
    </div>
    <div class="content-container">
      <div class="header-meta">
        <div class="row">
          <div class="small-12 medium-6 columns">
            <ul class="inline-list">
                            <li>&nbsp;</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="header-search">
          <section id="block-hc-search-search-form" class="block block-hc-search block-hc-search-search-form" data-block-id="block-hc-search-search-form">

    
      <form action="/en/search" method="GET" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-keywords">
  <label class="element-invisible">Keywords </label>
 <input placeholder="Keywords" type="text" name="keywords" size="60" maxlength="128" class="form-text" />
</div>
<div><button name="" value="Find" type="submit" class="form-submit">Find</button>
</div></div></form>  
</section>
      </div>
      <div class="header-menu">
        <ul id="main-menu" class="main-menu off-canvas inline-list" role="navigation"><li class="first expanded"><a href="/en/overview" title="Overview" data-megalayer="megalayer-18481"><span>Overview</span></a><ul class="menu"><li class="first leaf"><a href="/en/overview/introduction"><span>Introduction</span></a></li>
<li class="leaf"><a href="/en/overview/progress-update"><span>Progress update</span></a></li>
<li class="leaf"><a href="/en/overview/2018-highlights-and-awards"><span>2018 highlights and awards</span></a></li>
<li class="leaf"><a href="/en/overview/corporate-governance"><span>Corporate governance</span></a></li>
<li class="leaf"><a href="/en/overview/managing-sustainability"><span>Managing sustainability</span></a></li>
<li class="leaf"><a href="/en/overview/our-business"><span>Our business</span></a></li>
<li class="last leaf"><a href="/en/overview/scope-and-structure-of-the-report"><span>Scope and structure of the report</span></a></li>
</ul></li>
<li class="expanded"><a href="/en/sustainable-construction" title="Enabling sustainable construction" data-megalayer="megalayer-18482"><span>Sustainable construction</span></a><ul class="menu"><li class="first leaf"><a href="/en/sustainable-construction/customer-and-industry-engagement"><span>Customer and industry engagement</span></a></li>
<li class="leaf"><a href="/en/sustainable-construction/sustainable-construction"><span>Sustainable construction</span></a></li>
<li class="last leaf"><a href="/en/sustainable-construction/contract-collaboration"><span>Contract collaboration</span></a></li>
</ul></li>
<li class="expanded"><a href="/en/people" title="People and communities" data-megalayer="megalayer-18483"><span>People</span></a><ul class="menu"><li class="first leaf"><a href="/en/people/health-safety-and-well-being"><span>Health, safety and well being</span></a></li>
<li class="leaf"><a href="/en/people/employment-and-skills"><span>Employment and skills</span></a></li>
<li class="leaf"><a href="/en/people/local-community"><span>Local community</span></a></li>
<li class="last leaf"><a href="/en/people/environmental-incidents-and-emmisions"><span>Environmental incidents and emissions</span></a></li>
</ul></li>
<li class="expanded"><a href="/en/carbon" title="Carbon and energy" data-megalayer="megalayer-18484"><span>Carbon</span></a><ul class="menu"><li class="first leaf"><a href="/en/carbon/energy-efficiency"><span>Energy efficiency</span></a></li>
<li class="leaf"><a href="/en/carbon/waste-as-fuel"><span>Waste as fuel</span></a></li>
<li class="leaf"><a href="/en/carbon/co2e-emissions-from-production"><span>CO2e emissions from production</span></a></li>
<li class="last leaf"><a href="/en/carbon/co2e-emissions-from-transport"><span>CO2e emissions from transport</span></a></li>
</ul></li>
<li class="expanded"><a href="/en/waste" title="Waste and raw materials" data-megalayer="megalayer-18485"><span>Waste</span></a><ul class="menu"><li class="first leaf"><a href="/en/waste/waste-minimisation"><span>Waste minimisation</span></a></li>
<li class="last leaf"><a href="/en/waste/materials-efficiency-and-recycling"><span>Materials efficiency and recycling</span></a></li>
</ul></li>
<li class="expanded"><a href="/en/water" title="Water and biodiversity" data-megalayer="megalayer-18486"><span>Water</span></a><ul class="menu"><li class="first leaf"><a href="/en/water/water"><span>Water</span></a></li>
<li class="leaf"><a href="/en/water/biodiversity-and-site-stewardship"><span>Biodiversity and site stewardship</span></a></li>
<li class="last leaf"><a href="/en/water/site-biodiversity-action-plans"><span>Site biodiversity action plans</span></a></li>
</ul></li>
<li class="last expanded"><a href="/en/quality" title="Quality processes and systems" data-megalayer="megalayer-18487"><span>Quality</span></a><ul class="menu"><li class="first leaf"><a href="/en/quality/systems-and-processes"><span>Systems and processes</span></a></li>
<li class="last leaf"><a href="/en/quality/product-quality-and-performance"><span>Product quality and performance</span></a></li>
</ul></li>
</ul>      </div>
      <a class="header-menu-sticky"></a>
    </div>
  </div>
</div>

<div  id="megalayer-18481" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/overview/introduction">Introduction</a></li><li class="leaf"><a href="/en/overview/progress-update">Progress update</a></li><li class="leaf"><a href="/en/overview/2018-highlights-and-awards">2018 highlights and awards</a></li><li class="leaf"><a href="/en/overview/corporate-governance">Corporate governance</a></li><li class="leaf"><a href="/en/overview/managing-sustainability">Managing sustainability</a></li><li class="leaf"><a href="/en/overview/our-business">Our business</a></li><li class="last leaf"><a href="/en/overview/scope-and-structure-of-the-report">Scope and structure of the report</a></li></ul></div><div  id="megalayer-18482" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/sustainable-construction/customer-and-industry-engagement">Customer and industry engagement</a></li><li class="leaf"><a href="/en/sustainable-construction/sustainable-construction">Sustainable construction</a></li><li class="last leaf"><a href="/en/sustainable-construction/contract-collaboration">Contract collaboration</a></li></ul></div><div  id="megalayer-18483" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/people/health-safety-and-well-being">Health, safety and well being</a></li><li class="leaf"><a href="/en/people/employment-and-skills">Employment and skills</a></li><li class="leaf"><a href="/en/people/local-community">Local community</a></li><li class="last leaf"><a href="/en/people/environmental-incidents-and-emmisions">Environmental incidents and emissions</a></li></ul></div><div  id="megalayer-18484" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/carbon/energy-efficiency">Energy efficiency</a></li><li class="leaf"><a href="/en/carbon/waste-as-fuel">Waste as fuel</a></li><li class="leaf"><a href="/en/carbon/co2e-emissions-from-production">CO2e emissions from production</a></li><li class="last leaf"><a href="/en/carbon/co2e-emissions-from-transport">CO2e emissions from transport</a></li></ul></div><div  id="megalayer-18485" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/waste/waste-minimisation">Waste minimisation</a></li><li class="last leaf"><a href="/en/waste/materials-efficiency-and-recycling">Materials efficiency and recycling</a></li></ul></div><div  id="megalayer-18486" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/water/water">Water</a></li><li class="leaf"><a href="/en/water/biodiversity-and-site-stewardship">Biodiversity and site stewardship</a></li><li class="last leaf"><a href="/en/water/site-biodiversity-action-plans">Site biodiversity action plans</a></li></ul></div><div  id="megalayer-18487" class="megalayer"><ul class="menu level-1"><li class="first leaf"><a href="/en/quality/systems-and-processes">Systems and processes</a></li><li class="last leaf"><a href="/en/quality/product-quality-and-performance">Product quality and performance</a></li></ul></div>  </section>

  <section class="section-header_content" role="banner">
    <header class="content-header">
  <div class="slider mode-slider ">
      <ul class="slide-container">
                  <li class="slide slide--first">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/ad/f6/enabling-1920x720.jpg?itok=TOE6vRCV" width="1920" height="720" alt="Enabling sustainable construction. " title="Enabling sustainable construction. " />                                  </li>
                  <li class="slide ">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/39/3c/people-1920x720.jpg?itok=5vXMYmxC" width="1920" height="720" alt="People and communities. " title="People and communities. " />                                  </li>
                  <li class="slide ">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/20/56/carbon-1920x720.jpg?itok=DRfqgS9R" width="1920" height="720" alt="Carbon and energy. " title="Carbon and energy. " />                                  </li>
                  <li class="slide ">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/ee/31/waste-1920x720.jpg?itok=6mL_URnp" width="1920" height="720" alt="Waste and raw materials. " title="Waste and raw materials. " />                                  </li>
                  <li class="slide ">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/90/66/water-1920x720.jpg?itok=BtjjgiHf" width="1920" height="720" alt="Water and biodiversity. " title="Water and biodiversity. " />                                  </li>
                  <li class="slide ">
                          <img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/header_slider/public/assets/images/0a/12/quality-1920x720.jpg?itok=psKxIbZE" width="1920" height="720" alt="Quality processes and systems. " title="Quality processes and systems. " />                                  </li>
              </ul>
      </div>
</header>
  </section>

  <section class="section-meta">
    <div class="row">
      <div class="medium-9 columns">
              </div>
      <div class="medium-3 columns">
        <div class="page-meta">
          <div class="social-container"></div>          <a href="/en/node/22057#print_paper" title="Print this page" class="icon-print-paper active" onclick="window.print(); return false;" rel="nofollow"></a>                    <a href="mailto:?body=https%3A//www.hanson-sustainability.co.uk/en/node/22057&amp;subject=Link%20to%20page%3A%20Sustainability%202019%20%7C%20Hanson%20Sustainability%20Report%202019" title="Send link via email" class="icon-paper-plane" rel="nofollow"></a>        </div>
      </div>
    </div>
  </section>

  <section class="section-info">
          </section>

  <section class="section-content" role="main">
    
    <!--  Tabs and title go here. Wrapped in the grid  -->
    <div class="row">
      <div class="large-12 columns">
                <div class="editor-tabs">
                  </div>
                                          <h1 class="page-title">
            Sustainability 2019          </h1>
              </div>
    </div>

    <!-- Print content without grid wrapper if node is of type flexipage -->
            <div class="row">
          <div class="large-12 columns">
              <div  class="node node-hub view-mode-full clearfix">

  <!-- Needed to activate contextual links -->
  
  <div class="row">
    <div class="medium-12 columns">
      <div class="hc-top column-full">
        <div class="body field"><h6 style="text-align: center;"><strong style="text-align: center;"><a href="/en/overview/introduction">Introduction </a></strong><strong style="text-align: center;">| <a href="/en/overview/progress-update">Progress update</a> | <a href="/en/overview/2018-highlights-and-awards">2018 highlights and awards</a> | <a href="/en/overview/corporate-governance">Corporate governance</a><br /><a href="/en/overview/managing-sustainability">Managing sustainability</a> | <a href="/en/overview/our-business">Our business</a> | <a href="/en/overview/scope-and-structure-of-the-report">Scope and structure</a></strong></h6></div>      </div>
    </div>
  </div>
  <div class="row">
    <div class="medium-5 large-4 columns">
      <div class="hc-bottom-first column-small">
              </div>
    </div>
    <div class="medium-7 large-8 columns">
      <div class="hc-bottom-second column-large">
              </div>
    </div>
  </div>

</div>

<!-- Needed to activate display suite support on forms -->
<div class="hc-100-30-30-30 clearfix">

  <!-- Needed to activate contextual links -->
  
  <div class="row">
    <div class="medium-12 columns">
      <div class="hc-top column-full">
              </div>
    </div>
  </div>
  <div class="row">
    <div class="medium-4 columns">
      <div class="hc-bottom-first column-small">
          <section id="block-bean-1533ce5d4ac72f5457db82a0945ef29d" class="block block-bean block-bean-1533ce5d4ac72f5457db82a0945ef29d" data-block-id="block-bean-1533ce5d4ac72f5457db82a0945ef29d">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53641 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/sustainable-construction"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/d9/b5/sustainable-construction-block_0.jpg?itok=sY5PbRyD" width="686" height="686" alt="© Photo credit: Pete Bucktrout, British Antarctic Survey. " title="© Photo credit: Pete Bucktrout, British Antarctic Survey. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/sustainable-construction">Enabling sustainable construction</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/sustainable-construction">Our vision is to be the clear and sustainable market leader, focused on exceeding customer expectations through an engaged team that is responsible, reliable and safe.</a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
<section id="block-bean-4f82be93b089019f4a859dbfa29e469a" class="block block-bean block-bean-4f82be93b089019f4a859dbfa29e469a" data-block-id="block-bean-4f82be93b089019f4a859dbfa29e469a">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53645 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/waste"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/f5/b1/waste-block.jpg?itok=T6vr91KU" width="686" height="686" alt="Waste and raw materials. " title="Waste and raw materials. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/waste">Waste and raw materials</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/waste">Our vision it to be recognised as a responsible business which uses both raw materials and waste beneficially with a minimal impact on the environment. </a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
      </div>
    </div>
    <div class="medium-4 columns">
      <div class="hc-bottom-second column-small">
          <section id="block-bean-94e078d2748b511332592484f11d08bf" class="block block-bean block-bean-94e078d2748b511332592484f11d08bf" data-block-id="block-bean-94e078d2748b511332592484f11d08bf">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53643 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/people"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/b7/48/people-block.jpg?itok=nQJIvtCw" width="686" height="686" alt="People and communities. " title="People and communities. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/people">People and communities</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/people">Our vision is Zero harm in the workplace, compliance with all legislation and a positive impact on communities around our sites through effective partnerships and dialogue.</a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
<section id="block-bean-30cc5a032cca92727efaf4021b68cc0f" class="block block-bean block-bean-30cc5a032cca92727efaf4021b68cc0f" data-block-id="block-bean-30cc5a032cca92727efaf4021b68cc0f">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53646 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/water"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/17/67/water-block_0.jpg?itok=O9r026mJ" width="686" height="686" alt="Water and biodiversity. " title="Water and biodiversity. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/water">Water and biodiversity</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/water">Our vision is to be acknowledged as a business that values water as a vital natural resource and recognises the impact of its extraction sites on the local environment both during and after use. </a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
      </div>
    </div>
    <div class="medium-4 columns">
      <div class="hc-bottom-third column-small">
        <section id="block-bean-62ecfeaa3e68f1a2a1179debd6a564b7" class="block block-bean block-bean-62ecfeaa3e68f1a2a1179debd6a564b7" data-block-id="block-bean-62ecfeaa3e68f1a2a1179debd6a564b7">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53644 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/carbon"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/95/fd/carbon-block.jpg?itok=6qu9wl2-" width="686" height="686" alt="Carbon and energy. " title="Carbon and energy. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/carbon">Carbon and energy</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/carbon">Our vision is to be recognised as a leading force in the delivery of a low carbon environment.</a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
<section id="block-bean-a683974de8fc96003afcb41e6bb2434d" class="block block-bean block-bean-a683974de8fc96003afcb41e6bb2434d" data-block-id="block-bean-a683974de8fc96003afcb41e6bb2434d">

    
      

<article class="entity entity-bean bean-text bean-text--teaser story">
    <div class="story__image">
    <div class="field field-name-field-block-image field-type-entityreference field-label-hidden field-wrapper"><div  class="asset-wrapper asset aid-53647 asset-image asset-mode-block">
<div class="content">
  <div class="field field-name-field-asset-image field-type-image field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/quality"><img src="https://www.hanson-sustainability.co.uk/sites/default/files/styles/story_teaser_medium/public/assets/images/39/f6/quality-block_1.jpg?itok=mIq5wtiG" width="686" height="686" alt="Quality processes and systems. " title="Quality processes and systems. " /></a></div></div>
</div>
</div>  </div>
  <div class="story__content-wrapper">
    <span class="story__content">
      <h4><div class="field field-name-field-title field-type-text field-label-hidden field-wrapper"><a href="https://www.hanson-sustainability.co.uk/en/quality">Quality processes and systems</a></div></h4>
      <div class="story__text">
        <div class="field field-name-field-text field-type-text-long field-label-hidden field-wrapper"><p><a href="/en/quality">Our vision is a robust integrated management system (IMS) firmly established at our core to deliver improvements in compliance, competency and sustainable performance.</a></p></div>      </div>
    </span>
  </div>
</article>
  
</section>
    </div>
    </div>
  </div>

</div>

<!-- Needed to activate display suite support on forms -->
          </div>
        </div>
      
  </section>

  <section class="section-footer">
    <footer class="footer">
      <div class="back-to-top">
        <a href="#" class="back-to-top-link">
          <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1395 1184q0 13-10 23l-50 50q-10 10-23 10t-23-10l-393-393-393 393q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l466 466q10 10 10 23z" fill="CurrentColor"/></svg>
        </a>
      </div>
      <div class="row">
        <div class="large-12 columns">
            <section id="block-hc-menu-footer" class="block block-hc-menu block-hc-menu-footer" data-block-id="block-hc-menu-footer">

    
      <ul class="large-block-grid-4 medium-block-grid-2 small-block-grid-1"><li class="level-1"><span title="" class="nolink" tabindex="0">Sustainability Report 2019</span><ul class="menu"><li><a href="https://www.hanson-sustainability.co.uk/sites/default/files/assets/document/49/3a/summary-of-kpi-performance-against-2020-targets_2.pdf" title="">Summary of KPI performance</a></li>
<li><a href="http://2019.hanson-sustainability.co.uk.hc.stage.ueberbit.de/sites/default/files/assets/document/01/eb/lucideon-assurance-statement-2019.pdf" title="">Assurance statement - verification</a></li>
<li><a href="https://www.hanson-sustainability.co.uk/sites/default/files/assets/document/22/b6/hanson-sustainability-report-2019.pdf" title="">Download printed report</a></li>
<li><a href="/en/report-archive">Report Archive</a></li>
<li><a href="https://www.hanson.co.uk/sites/default/files/assets/document/4c/3a/sustainability-policy.pdf" title="">Sustainability policy</a></li>
</ul></li>
<li class="level-1"><span title="" class="nolink" tabindex="0">Legal</span><ul class="menu"><li><a href="https://www.hanson.co.uk/privacy-policy" title="">Privacy policy</a></li>
<li><a href="https://www.hanson.co.uk/en/cookie-policy" title="">Cookie policy</a></li>
<li><a href="https://www.hanson.co.uk/en/gender-pay-gap-report" title="">Gender pay gap report</a></li>
<li><a href="https://www.hanson.co.uk/en/modern-slavery-statement" title="">Modern slavery statement</a></li>
<li><a href="https://www.hanson.co.uk/en/terms-and-conditions-of-use-policy" title="">Terms and conditions of use</a></li>
</ul></li>
<li class="level-1"><span title="" class="nolink" tabindex="0">Our websites</span><ul class="menu"><li><a href="https://www.hanson.co.uk/" title="">Hanson UK</a></li>
<li><a href="https://www.hanson-careers.co.uk/en" title="">Hanson Careers</a></li>
<li><a href="https://www.hanson-communities.co.uk/" title="">Hanson Communities</a></li>
<li><a href="https://www.hanson-drivers.co.uk/" title="">Hanson Drivers</a></li>
<li><a href="https://www.hansonpensions.co.uk/" title="">Hanson Pensions</a></li>
<li><a href="https://www.hanson-packedproducts.co.uk/" title="">Hanson Packed Products</a></li>
<li><a href="https://www.irvine-whitlock.co.uk/" title="">Irvine Whitlock</a></li>
</ul></li>
<li class="level-1"><span title="" class="nolink" tabindex="0">Get connected</span><ul class="menu"><li><a href="https://twitter.com/Hanson_UK" title="">Twitter</a></li>
<li><a href="https://www.linkedin.com/company/hanson-uk" title="">LinkedIn</a></li>
<li><a href="https://www.facebook.com/HansonUnitedKingdom" title="">Facebook</a></li>
<li><a href="https://www.youtube.com/HansonUK" title="">Youtube</a></li>
</ul></li>
</ul>  
</section>
        </div>
      </div>
    </footer>
  </section>

  </div>

        <script type="text/javascript" src="https://www.hanson-sustainability.co.uk/sites/default/files/js/js_DDIK1qPXFj1o-EqpbQygYAJJHx6IQm4PMCqv3M7nP_Y.js"></script>
<script type="text/javascript" src="https://www.hanson-sustainability.co.uk/sites/default/files/js/js_x_l9sNc8PgAVP16YEoylO9WB8rT3vBIWFZXAshN4lmI.js"></script>
<script type="text/javascript" src="https://www.hanson-sustainability.co.uk/sites/default/files/js/js_z4ZBHlOeUK4vHqGFgRrm9DlUn5EQbBNpq5AYYmNE6F4.js"></script>
<script type="text/javascript" src="https://www.hanson-sustainability.co.uk/sites/default/files/js/js_jef53JYQHysTRlGsP1qYq_RORY2YNDjXZo2J5eYskgA.js"></script>
<script type="text/javascript">jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"en\/","ajaxPageState":{"theme":"hc_hanson","theme_token":"4wWHCepEFR6aZclXCc-QfaOPSDTq9qJhTw1EOrQUi-w"},"language":{"content":"en","interface":"en"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"analytics_switch":{"gaProperty":"UA-58988266-13"},"currentPath":"node\/22057","currentPathIsAdmin":false});</script>
              </div>
    </div>
  </div>
</body>
</html>
