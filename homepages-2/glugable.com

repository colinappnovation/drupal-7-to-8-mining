<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Glugable - Creating Brands People Love</title>
        <meta name="description" content="Creative and Passionate.  We help startups and established companies invent, build & launch their next product or venture">
        <link href="https://www.glugable.com/" rel="canonical" />
        <meta name="p:domain_verify" content="080fe256566950b3b464dff2988ee6ae"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="img/assets/glugable.png" rel="icon" type="image/png"> 
        <link href="css/init.css" rel="stylesheet" type="text/css">
        <link href="css/ion-icons.min.css" rel="stylesheet" type="text/css">
        <link href="css/etline-icons.min.css" rel="stylesheet" type="text/css">
        <link href="css/theme.css" rel="stylesheet" type="text/css">  
        <link href="css/custom.css" rel="stylesheet" type="text/css"> 
        <link href="css/colors/blue.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville|Libre+Franklin" rel="stylesheet" type="text/css">
        
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-85819410-1', 'auto');
          ga('send', 'pageview');

        </script>

    </head>
    <body data-fade-in="true">
        
        <div class="pre-loader"><div></div></div>
        
        <!-- Start Header -->
        <nav class="navbar nav-down" data-fullwidth="true" data-menu-style="transparent" data-animation="shrink"><!-- Styles: light, dark, transparent | Animation: hiding, shrink -->
            <div class="container">
                
                <div class="navbar-header">
                    <div class="container">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar top-bar"></span>
                            <span class="icon-bar middle-bar"></span>
                            <span class="icon-bar bottom-bar"></span>
                        </button>
                        <a class="navbar-brand to-top" href="#"><img src="img/assets/logo-light.png" class="logo-light" alt="#"><img src="img/assets/logo-dark2.png" class="logo-dark" alt="#"></a> 
                    </div>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <div class="container">
                        <ul class="nav navbar-nav menu-right">                            
                            
                            <!-- Each section must have corresponding ID ( #hero -> id="hero" ) -->
                            <li><a href="#hero">Home</a></li>
                            <li><a href="#services">Services</a></li>
                         
                            <li><a href="#contact-info">Contact</a></li>
                             
                            <li class="nav-separator"></li>
                            <li  class="nav-icon"><a href="https://www.facebook.com/glugable" target="_blank"><i class="ion-social-facebook"></i></a></li>
                            <li  class="nav-icon"><a href="https://www.twitter.com/glugable" target="_blank"><i class="ion-social-twitter"></i></a></li>
                            <li  class="nav-icon"><a href="https://www.instagram.com/glugable" target="_blank"><i class="ion-social-instagram"></i></a></li>
                        </ul>
                        
                    </div>
                </div> 
            </div>
        </nav>
        <!-- End Header -->
        
        <!-- Hero -->
        <section id="hero" class="hero-fullscreen parallax" data-overlay-dark="7">
            <div class="background-image">
                <img src="img/backgrounds/marketing.jpg" alt="#">
            </div>
            
            <div class="container">
                <div class="row">
                    
                    <div class="hero-content-slider mt20" data-autoplay="true" data-speed="8000">
                         

                        <div>
                            <h1>We are <span class="color">Glugable</span><br><strong>Creative & Passionate</strong></h1>
                            <p class="lead">We work with start ups & companies that we beleive in.</p>
                            <a href="mailto:dave@glugable.com" class="btn btn-lg btn-primary btn-scroll"><span><i class="ion-paper-airplane"></i> Email us today </span></a>  
                        </div>
                      
                        <div>
                            <h1>We help startups to build<br><strong>& <span class="color">launch</span> their next venture.</strong></h1>
                            <p class="lead">(& established companies invent their next product)</p>
                            <a href="#services" class="btn btn-lg btn-primary btn-scroll"><span><i class="ion-link"></i> Our Services </span></a>  
                        </div>
                        
                        <div>
                            <h1><br><strong>Lets talk <span class="color">strategy</span>!</strong></h1>
                            <p class="lead">Face to face, or on the phone...</p>
                            <a href="tel:01133189046" class="btn btn-lg btn-primary btn-scroll"><span><i class="ion-android-call"></i> Call us today </span></a>  
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- End Hero -->
        
              <!-- Services -->  
        <section id="services" class="pt100 pb90">
            <div class="container">                      
                <div class="row">

                    <div class="col-md-12 text-center pb20">   
                        <h2 class="">What We Do<br><strong>Day-To-Day</strong></h2>
                        <p class="lead">Skilled service with a <span class="color">creative </span>touch.</p>
                    </div> 
                    
                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-circle-compass size-3x color"></i>
                        <i class="icon-circle-compass back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>Graphic Design</strong><br>Crafted With Love</h4>
                            <p>Creative branding and marketing campaigns designed to combine digital & print.  We deliver targeted multi platform marketing, converting leads into customers with ease. With real time ROI reporting as standard.</p>
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-genius size-3x color"></i>
                        <i class="icon-genius back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>Web Optimisation</strong><br>Lightning Fast Web</h4>
                            <p>Using the latest techniques to widen your online presence.  We create beautiful customer journeys, responsive to mobile, tablet and desktop. Search engine and social media friendly straight out of the box.</p> 
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-layers  size-3x color"></i>
                        <i class="icon-layers  back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>Enterprise Grade Hardware</strong><br>Scalable Architecture</h4>
                            <p>Automated load balancing, lightning fast SSD hard drives and instantly scalable processing on demand.  Hosting that's incredibly secure digitally and physically (located 100m underground in an ex MOD facility.)</p> 
                        </div> 
                    </div> 

                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-streetsign size-3x color"></i>
                        <i class="icon-streetsign back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>InBound Marketing Strategy</strong><br>Design, Development & Deployment</h4>
                            <p>Producing excellent content -published strategically to drive a targetted audience to your websites' landing pages.  Delight your new and returning customers by delivering relevant and focused experiences.</p>
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-gears size-3x color"></i>
                        <i class="icon-gears back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>Analyse. Learn. Win.</strong><br>Competitive Analysis</h4>
                            <p>In-depth analysis of your competitors' digital marketing, -what, why and how they're operating. Lets understand what they're doing, then we'll help you do it better.  Make your digital channels work harder.  </p> 
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-6 feature-left">
                        <i class="icon-globe size-3x color"></i>
                        <i class="icon-globe back-icon"></i> 
                        <div class="feature-left-content">
                            <h4><strong>Intelligent Interfacing</strong><br>A/B Split Testing</h4>
                            <p>Let the data decide. Converting data-driven insights into increased site performance.  Deliver exceptional customer experiences, reduce bounce rates and increase time on site. Drive results—and more revenue—without increasing your acquisition costs.</p> 
                        </div> 
                    </div> 

                </div>
            </div>
        </section>
        <!-- End Services -->
        
    
        <!-- Who We Are -->
        <section id="who-we-are" class="parallax pt40 pb40" data-overlay-dark="8">
            <div class="background-image">
                <img src="img/backgrounds/branding.jpg" alt="#">
            </div>
            <div class="container">   
                <div class="row vertical-align">

                    <div class="col-md-6 pr30 mt40 mb40">   
                        <h2><strong>Design-Obsessed</strong><br><span class="color">Future-Forward</span></h2>   
                        <p>We build and launch our own ventures, but we also work with founders and companies to transform good ideas into great products and services.</p>
                        <p>Making great products is why we get up in the morning. With an obsession for good design and a relentless attention to detail, we will always put the product first.</p>
                        <p>We turn sketches into a prototype quickly. Our aim is to spend less time fiddling with plans and specs, and more time producing. Our approach is experimental and collaborative. Above all, we iterate quickly for a short time to launch.</p> 
                    </div>

                    <div class="col-md-6 pt40 pb30">                        
                        <div class="progress-bars standard transparent-bars" data-animate-on-scroll="on">
                            <h5 class="bold">Fun</h5>
                            <div class="progress" data-percent="15%">
                                <div class="progress-bar">
                                    <span class="progress-bar-tooltip">15%</span>
                                </div>
                            </div>
                            <h5 class="bold">Passion</h5>
                            <div class="progress" data-percent="25%">
                                <div class="progress-bar progress-bar-primary">
                                    <span class="progress-bar-tooltip">25%</span>
                                </div>
                            </div>
                            <h5 class="bold">Research</h5>
                            <div class="progress" data-percent="15%">
                                <div class="progress-bar progress-bar-primary">
                                    <span class="progress-bar-tooltip">15%</span>
                                </div>
                            </div>
                            <h5 class="bold">Iterative Development</h5>
                            <div class="progress" data-percent="45%">
                                <div class="progress-bar progress-bar-primary">
                                    <span class="progress-bar-tooltip">45%</span>
                                </div>
                            </div> 
                              <h5 class="bold"> Combined they deliver - RESULTS</h5>
                            <div class="progress" data-percent="100%">
                                <div class="progress-bar progress-bar-primary">
                                    <span class="progress-bar-tooltip">100%</span>
                                </div>
                            </div> 
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End Who We Are -->
        
        <!-- Callout -->
        <section class="parallax light bg-img-9" data-overlay-light="9">
            <div class="background-image">
                <img src="img/backgrounds/webdesign.jpg" alt="#">
            </div>
            <div class="container pt100 pb100"> 
                <div class="row pt20"> 						

                    <div class="col-md-12 text-center"> 
                        <h2>Ready to <span class="color">Kickstart</span> Your Project?</h2> 
                        <a href="tel:01133189046" target="_blank" class="btn btn-md btn-primary btn-appear mt30"><span>Call Us Now<i class="ion-checkmark"></i></span></a>
                    </div> 

                </div>
            </div>
        </section>
        <!-- End Callout --> 
        
       
    
        
        <!-- Contact Info -->
        <section id="contact-info" class="parallax pt110 pb70" data-overlay-dark="8">
            <div class="background-image">
                <img src="img/backgrounds/webdesign.jpg" alt="#">
            </div>
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-12 details white text-center">
                        <div class="phone-number mb10">
                            <h1 href="tel:01133189046" class="bold">0113 318 9046</h1>
                        </div>
                        <div class="col-lg-12">
                            <h3>dave@<span class="color">glugable.com</span></h3>
                            <h4>Leeds <span class="color">United Kingdom</span></h4>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- End Contact Info -->
                

        <!-- Clients Section -->
        <section class="pt70 pb70">
            <div class="container"> 
                <div class="row">	 

                    <div class="clients-slider" data-autoplay="true" data-speed="2000"> 
                        <div><img src="img/clients/1.png" class=" " alt="#"></div>  
                        <div><img src="img/clients/2.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/3.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/4.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/5.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/6.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/7.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/8.png" class="img-responsive" alt="#"></div> 
                        <div><img src="img/clients/9.png" class="img-responsive" alt="#"></div> 
                    </div>

                </div>
            </div>
        </section>
        <!-- End Clients -->  
        
    
        
        <!-- Start Footer -->
        <footer id="footer" class="footer style-1 dark">

            <a href="index.html"><img src="img/assets/logo-light.png" alt="#" class="mr-auto img-responsive"></a>
                                    

            <ul>
                <li><a href="https://www.twitter.com/glugable" target="_blank" class="color"><i class="ion-social-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/glugable" target="_blank" class="color"><i class="ion-social-facebook"></i></a></li>
                <li><a href="https://www.linkedin.com/" target="_blank" class="color"><i class="ion-social-linkedin"></i></a></li>
                <li><a href="https://www.pinterest.com/glugable" target="_blank" class="color"><i class="ion-social-pinterest"></i></a></li> 
                <li><a href="https://plus.google.com/109581085686025800948" target="_blank" class="color"><i class="ion-social-googleplus"></i></a></li>
                <li><a href="https://instagram.com/glugable" target="_blank" class="color"><i class="ion-social-instagram"></i></a></li> 

            </ul>
            <a href="https://glugable.com/" target="_blank"><strong>© Glugable 2015</strong></a>
            <p>Made with love, for great people.</p>
            
            <!-- Back To Top Button -->
            <span><a class="scroll-top"><i class="ion-chevron-up"></i></a></span>
            
        </footer>
        <!-- End Footer -->
        
        <script src="js/jquery.js"></script>
        <script src="js/init.js"></script>
        <script src="js/scripts.js"></script>       
        
    </body>
</html>