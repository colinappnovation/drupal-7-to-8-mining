<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" />
<meta name="title" content="London Elects | London Elects" />
<link rel="shortlink" href="https://www.londonelects.org.uk/" />
<link rel="canonical" href="https://www.londonelects.org.uk/" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="/core/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="revision" href="https://www.londonelects.org.uk/london-elects" />

    <title>London Elects | London Elects</title>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KXBTQP');</script>
    <!-- End Google Tag Manager -->

    <link rel="stylesheet" media="all" href="/modules/contrib/responsive_table_filter/css/responsive-table-filter.css?pzthz0" />
<link rel="stylesheet" media="all" href="/modules/contrib/paragraphs/css/paragraphs.unpublished.css?pzthz0" />
<link rel="stylesheet" media="screen" href="/themes/custom/numiko/dist/css/style.css?pzthz0" />
<link rel="stylesheet" media="print" href="/themes/custom/numiko/dist/css/print.css?pzthz0" />

    
<!--[if lte IE 8]>
<script src="/core/assets/vendor/html5shiv/html5shiv.min.js?v=3.7.3"></script>
<![endif]-->


    <link rel="shortcut icon" href="/themes/custom/numiko/favicons/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/themes/custom/numiko/favicons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/themes/custom/numiko/favicons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/themes/custom/numiko/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/themes/custom/numiko/favicons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/themes/custom/numiko/favicons/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/themes/custom/numiko/favicons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/themes/custom/numiko/favicons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/themes/custom/numiko/favicons/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/themes/custom/numiko/favicons/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/themes/custom/numiko/favicons/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/themes/custom/numiko/favicons/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/themes/custom/numiko/favicons/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/themes/custom/numiko/favicons/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
  </head>

  <body class="path-frontpage page-node-type-landing-page no-js" style="padding: 0px; margin: 0px;">

  <!-- Google Tag Manager (noscript) -->

  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXBTQP"
  height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>

  <!-- End Google Tag Manager (noscript) -->

    
    <a href="#main-content" class="visually-hidden visually-hidden-focusable focusable skip-link">
      Skip to main content
    </a>

    <div class="svg-symbols" style='display:none;'>
      <svg xmlns="http://www.w3.org/2000/svg"><symbol id="sprite-icon-facebook" viewBox="0 0 20 20"><path d="M11.84 4.06a1.73 1.73 0 011.34-.41H15v-3A19.44 19.44 0 0012.36.5a4.39 4.39 0 00-3.2 1.17A4.36 4.36 0 008 5v2.45H5v3.38h3v8.67h3.54v-8.67h2.94l.45-3.38h-3.44V5.3a1.91 1.91 0 01.35-1.24z"/></symbol><symbol id="sprite-icon-linkedin" viewBox="0 0 50 50"><path d="M12 6.67a5.1 5.1 0 01-1.64 4A6.12 6.12 0 016 12.24h-.08a5.83 5.83 0 01-4.29-1.6A5.3 5.3 0 010 6.67a5.23 5.23 0 011.68-4 6.06 6.06 0 014.37-1.56 5.9 5.9 0 014.33 1.58A5.39 5.39 0 0112 6.67zm-.68 10v32.22H.62V16.63zM50 30.4v18.49H39.29V31.64A9.39 9.39 0 0038 26.29a4.61 4.61 0 00-4.12-1.94 5.26 5.26 0 00-3.43 1.12 7 7 0 00-2.07 2.79 7.81 7.81 0 00-.38 2.63v18H17.29q.06-13 .06-21.06t0-9.63v-1.57H28v4.69h-.06a14.54 14.54 0 011.33-1.82 13.73 13.73 0 011.84-1.69 8.5 8.5 0 012.84-1.42 12.73 12.73 0 013.72-.5 11.56 11.56 0 019 3.69Q50 23.27 50 30.4z"/></symbol><symbol id="sprite-icon-twitter" viewBox="0 0 20 20"><path d="M19.61 4.47a7.87 7.87 0 01-2 1.89v.48a9.69 9.69 0 01-.46 2.94 10.15 10.15 0 01-1.41 2.81A11.33 11.33 0 0113.53 15a10.22 10.22 0 01-3.15 1.65 12.37 12.37 0 01-3.94.62 11.59 11.59 0 01-6.05-1.66 9.13 9.13 0 001 0 8.1 8.1 0 004.89-1.56 4.07 4.07 0 01-2.29-.73 3.57 3.57 0 01-1.39-1.81 4.91 4.91 0 00.74.06 4.06 4.06 0 001-.13 3.93 3.93 0 01-2.27-1.26 3.41 3.41 0 01-.89-2.33A4.16 4.16 0 003 8.31 3.62 3.62 0 011.67 7a3.42 3.42 0 01.06-3.59 11.14 11.14 0 003.59 2.7 11.64 11.64 0 004.53 1.14 4.14 4.14 0 01-.09-.84 3.4 3.4 0 011.15-2.59 4 4 0 012.79-1.07 3.94 3.94 0 012.88 1.16 8.21 8.21 0 002.5-.91 3.62 3.62 0 01-1.74 2 8.5 8.5 0 002.27-.53z"/></symbol><symbol id="sprite-icon-whatsapp" viewBox="0 0 50 50"><path d="M.244 49.777c1.347-4.35 2.909-8.485 4.336-12.754C-4.883 20.473 6.828 1.841 22.69.293 37.666-1.168 49.945 9.59 49.983 24.27c.039 15.49-14.985 28.498-31.884 23.21-1.275-.398-2.852-1.697-4.337-1.785-2.074-.122-4.792 1.367-6.886 2.041-2.296.738-4.292 1.479-6.632 2.04zm14.029-8.673c13.646 8.282 29.987-1.007 31.373-14.539 1.255-12.25-8.526-23.149-21.68-22.19C8.946 5.467-.075 23.394 9.17 36.512A130.026 130.026 0 006.876 43.4c2.505-.726 4.815-1.647 7.397-2.296z"/><path d="M19.63 12.792c1.435 3.002 3.655 7.443 0 9.437 1.856 3.925 4.926 6.637 8.927 8.418 1.33-.88 2.204-2.217 3.316-3.316 2.322.908 4.493 1.968 6.376 3.316.194 4.388-3.504 6.01-6.631 5.866-6.599-.303-15.667-8.64-17.855-14.794-1.299-3.652.195-10.636 5.866-8.927z"/></symbol><symbol id="sprite-icon-youtube" viewBox="0 0 28 28"><path d="M11.11 17.69v-8l7.56 4zm16.61-9.38a6.17 6.17 0 00-1.11-2.82 4 4 0 00-2.81-1.2C19.88 4 14 4 14 4s-5.88 0-9.8.29a4 4 0 00-2.81 1.2A6.17 6.17 0 00.28 8.31 43.94 43.94 0 000 12.92v2.15a43.69 43.69 0 00.28 4.61 6.17 6.17 0 001.11 2.82 4.68 4.68 0 003.09 1.21C6.72 23.93 14 24 14 24s5.88 0 9.8-.3a4 4 0 002.81-1.2 6.17 6.17 0 001.11-2.82 43.69 43.69 0 00.28-4.61v-2.15a43.94 43.94 0 00-.28-4.61z"/></symbol><symbol id="sprite-icon-arrow" viewBox="0 0 28 28"><path fill="none" d="M18.7 21.55l7.5-7.5-7.5-7.5m7.5 7.5H1.1"/></symbol><symbol id="sprite-icon-chevron-solid" viewBox="0 0 12 4.67"><path d="M0 0l6 4.67L12 0H0z"/></symbol><symbol id="sprite-icon-chevron" viewBox="0 0 20 11"><path d="M17.4.4a1.45 1.45 0 012.1 0 1.34 1.34 0 010 2L11 10.5a1.45 1.45 0 01-2.1 0l-8.5-8a1.34 1.34 0 010-2A1.68 1.68 0 012.6.4L10 7.1z"/></symbol><symbol id="sprite-icon-download" viewBox="0 0 28 28"><path fill="none" d="M7.7 16.05l6.3 6.3 6.3-6.3m-6.3 6.3V1.25"/><path fill="none" d="M24.1 16.05v10.2H3.9v-10.2"/></symbol><symbol id="sprite-icon-email" viewBox="0 0 50 50"><path d="M26.41 28.24L48.64 6A4 4 0 0046 5H4a4 4 0 00-2.64 1l22.23 22.24a2 2 0 002.82 0z"/><path d="M29.24 31.07a6 6 0 01-8.48 0L0 10.31V41a4 4 0 004 4h42a4 4 0 004-4V10.31z"/></symbol><symbol id="sprite-icon-film" viewBox="0 0 28 28"><path d="M26.44 12.79v10.65a2.29 2.29 0 01-2.25 2.25H4.65a2.27 2.27 0 01-2.24-2.25V12.79zm-24.03 0l-.75-3.28A2.26 2.26 0 013.34 6.8l19-4.39A2.24 2.24 0 0125 4.08l.75 3.27z" fill="none"/></symbol><symbol id="sprite-icon-location" viewBox="0 0 14 18"><defs><style>.cls-1{fill:none;stroke:#242424}</style></defs><g id="Current-Designs"><g id="LE_Homepage_Desktop_01" data-name="LE Homepage Desktop 01"><g id="Group-5"><path id="Stroke-1" class="cls-1" d="M9.74 6.27a2.47 2.47 0 10-2.47 2.47 2.47 2.47 0 002.47-2.47z"/><path id="Stroke-3" class="cls-1" d="M1.5 6.27c0 5.48 5.77 11.23 5.77 11.23s5.78-5.75 5.78-11.23a5.78 5.78 0 00-11.55 0z"/></g></g></g></symbol><symbol id="sprite-icon-mail" viewBox="0 0 28 28"><path d="M23 7a1.65 1.65 0 00-1.2-.49H6.2A1.65 1.65 0 005 7a1.7 1.7 0 00-.5 1.2v11.57A1.67 1.67 0 005 21a1.65 1.65 0 001.2.49h15.6A1.65 1.65 0 0023 21a1.7 1.7 0 00.5-1.2V8.23A1.67 1.67 0 0023 7zm-.86 12.74A.31.31 0 0122 20a.35.35 0 01-.24.1H6.2a.35.35 0 01-.3-.34v-8.14a5.36 5.36 0 00.73.7q2.84 2.19 4.52 3.59c.36.3.65.54.88.71a5.06 5.06 0 00.91.51 2.57 2.57 0 001.09.26 2.57 2.57 0 001.09-.26 5.06 5.06 0 00.91-.51c.23-.17.52-.41.88-.71 1.12-.93 2.63-2.12 4.52-3.59a5.36 5.36 0 00.73-.7zm0-11.15a2.55 2.55 0 01-.49 1.41 4.8 4.8 0 01-1.07 1.22l-4.25 3.38-.37.32c-.2.17-.37.3-.49.39a5.09 5.09 0 01-.47.34 2.47 2.47 0 01-.53.29A1.24 1.24 0 0114 16a1.46 1.46 0 01-.46-.1 2.65 2.65 0 01-.54-.25 5.09 5.09 0 01-.47-.34c-.12-.09-.29-.22-.49-.39l-.37-.32-4.25-3.36a3.72 3.72 0 01-1.56-3A.33.33 0 016 8a.35.35 0 01.24-.1H21.8a.38.38 0 01.15 0 .16.16 0 01.1.08l.06.09v.53z"/></symbol><symbol id="sprite-icon-play" viewBox="0 0 14 18"><path d="M.58.02l12.63 8.59L.58 17.19V.02z" data-name="Desktop/Slices/Video-Large"/></symbol><symbol id="sprite-icon-pointer" viewBox="0 0 28 28"><circle cx="14" cy="9.9" r="3.6" fill="none"/><path d="M5.5 9.9c0 8 8.5 16.5 8.5 16.5s8.5-8.5 8.5-16.5a8.5 8.5 0 00-17 0z" fill="none"/></symbol><symbol id="sprite-icon-print" viewBox="0 0 20 20"><path d="M19 3a1.63 1.63 0 00-1.2-.49H2.2A1.63 1.63 0 001 3a1.67 1.67 0 00-.5 1.2v11.57A1.67 1.67 0 001 17a1.63 1.63 0 001.2.49h15.6A1.63 1.63 0 0019 17a1.67 1.67 0 00.5-1.2V4.23A1.67 1.67 0 0019 3zm-.86 12.74A.33.33 0 0118 16a.33.33 0 01-.24.1H2.2A.35.35 0 012 16a.33.33 0 01-.1-.24V7.62a5.36 5.36 0 00.73.7q2.84 2.19 4.52 3.59c.36.3.65.54.88.71a4.61 4.61 0 00.91.51 2.58 2.58 0 001.09.26 2.58 2.58 0 001.09-.26 4.61 4.61 0 00.91-.51c.23-.17.52-.41.88-.71q1.68-1.39 4.52-3.59a5.36 5.36 0 00.73-.7zm0-11.15A2.63 2.63 0 0117.65 6a4.8 4.8 0 01-1.07 1.22l-4.25 3.38-.37.32c-.2.17-.37.3-.49.39a4.91 4.91 0 01-.47.34 2.58 2.58 0 01-.53.29A1.42 1.42 0 0110 12a1.42 1.42 0 01-.46-.1 2.58 2.58 0 01-.54-.25 4.91 4.91 0 01-.47-.34c-.12-.09-.29-.22-.49-.39l-.37-.32-4.25-3.36a3.7 3.7 0 01-1.56-3A.33.33 0 012 4a.35.35 0 01.24-.1H17.8a.33.33 0 01.15 0 .2.2 0 01.1.08.7.7 0 00.06.09 1.06 1.06 0 010 .14v.39z"/></symbol><symbol id="sprite-icon-search" viewBox="0 0 28 28"><circle cx="11.57" cy="11.57" r="10" fill="none"/><path fill="none" d="M18.57 18.57l8 8"/></symbol><symbol id="sprite-icon-sms" viewBox="0 0 22 22"><path d="M19.667 1.667h-16c-1.1 0-1.99.9-1.99 2l-.01 18 4-4h14c1.1 0 2-.9 2-2v-12c0-1.1-.9-2-2-2zm-11 9h-2v-2h2v2zm4 0h-2v-2h2v2zm4 0h-2v-2h2v2z"/></symbol><symbol id="sprite-icon-warning" viewBox="0 0 28 28"><path d="M12.44 3.74L1.64 22.48a1.79 1.79 0 00.66 2.44 1.73 1.73 0 00.86.23h21.68a1.78 1.78 0 001.76-1.81 1.85 1.85 0 00-.24-.86L15.56 3.74a1.84 1.84 0 00-3.12 0zm1.52 13.38V7.76m0 14.45v-2.86" fill="none"/></symbol></svg>    </div>

    <!-- No JS Warning -->
    <noscript>
      <div class="c-info-bar -warning" role="alert" style="display: flex;">
        <div class="c-info-bar__content">
          <p><strong>Please enable JavaScript in your web browser to get the best experience.</strong></p>
        </div>
      </div>
    </noscript>
      <!-- End of No JS Warning -->

      
        <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    


<div class="page">

  <div class="notifications">
          

<div id="block-cookiemessage" class="alert js-alert" role="status" aria-live="polite">
    
<div class="container">
  <div class="alert__inner">
    <div class="alert__content">
      <p class="js-alert-text">  We use cookies to ensure that we give you the best experience on our website.
  <a href="/cookies">Read more about our cookie policy</a>
</p>
    </div>

    <button type="button" class="alert__btn button | js-alert-close">
      <span>  OK, got it
</span>
    </button>
  </div>
</div>


</div>


<div id="block-sitewidealert" class="alert alert--bottom js-alert" role="status" aria-live="polite">
  
</div>

  
  </div>

  <header class='header' role="banner">
  <div class='container container-fluid | header__container'>
          <a class='header__logo' href="/" title="Home" rel="home">
  <img src="/themes/custom/numiko/logo.png" alt="London Elects" />
</a>

<h1 class='header__slogan'>
   MAYOR OF LONDON &amp; LONDON ASSEMBLY ELECTIONS
</h1>

  

        <div class='header__search'>
      <a href='/search' class="button button--inverted">
        <span>Site search</span>
        <svg class='icon icon--stroke'>
          <use xlink:href='#sprite-icon-search'></use>  
        </svg>
      </a>
    </div>

    <button class="hamburger | js-hamburger" type="button" aria-hidden="true" aria-expanded="true">
      <span class="hamburger__inner">
        <span class="hamburger__lines"></span>
      </span>
      <span class="hamburger__label | js-hamburger-label">Menu</span>
    </button>
  </div>
<div class='menu-primary-revealer | js-menu-revealer'>
        <nav class='menu-primary' aria-labelledby="block-numiko-mainnavigation-menu" id="block-numiko-mainnavigation">
            
  <h2 class="visually-hidden" id="block-numiko-mainnavigation-menu">Main navigation</h2>
  

          


        
          <ul class="menu menu--level-0 js-drilldown-root">
    
          
      
                  <li class="menu__item menu__item--collapsed menu__item--level-0 menu__item--search">
        <a  class="menu__link menu__link--level-0" href="/search">
          <span class='animate-underline'>Search</span>
          <svg class='icon icon--stroke'><use xlink:href='#sprite-icon-search'></use></svg>
        </a>
      </li>
      
      <li class="menu__item menu__item--collapsed menu__item--level-0">
        
        
        <a  class="menu__link menu__link--level-0" href="/" ><span class='animate-underline'>Home</span>
        </a>
                        <!-- Mobile menu -->
                <!-- Desktop menu -->
              </li>
          
      
            
      <li class="menu__item menu__item--collapsed menu__item--level-0">
        
        
        <a  class="menu__link menu__link--level-0" href="/im-voter" ><span class='animate-underline'>I&#039;m a voter</span>
        </a>
                        <!-- Mobile menu -->
                <!-- Desktop menu -->
              </li>
          
      
            
      <li class="menu__item menu__item--collapsed menu__item--level-0">
        
        
        <a  class="menu__link menu__link--level-0" href="/im-candidate" ><span class='animate-underline'>I&#039;m a candidate</span>
        </a>
                        <!-- Mobile menu -->
                <!-- Desktop menu -->
              </li>
          
      
            
      <li class="menu__item menu__item--collapsed menu__item--level-0">
        
        
        <a  class="menu__link menu__link--level-0" href="/media-centre" ><span class='animate-underline'>Media centre</span>
        </a>
                        <!-- Mobile menu -->
                <!-- Desktop menu -->
              </li>
              </ul>
      



  </nav>

  
</div>
</header> 
  
  
  
  
  <main role="main">
    <a id="main-content" tabindex="-1"></a>
          <div id="block-pagemessage">
  
    
        <div class="page-message">
  <div class="container">
            BETA This is a new website

      </div>
</div>


  </div>
<div class='hero-promo'>
      <div class='hero-promo__media'>        <img class="lazyload" data-sizes="auto" data-srcset="/sites/default/files/styles/12_6_media_tiny/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=8xIDKJb1 400w, /sites/default/files/styles/12_6_media_small/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=DcAjGSjs 750w, /sites/default/files/styles/12_6_media_medium/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=-fc_ZW7_ 1000w, /sites/default/files/styles/12_6_media_large/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=_yAlrt08 1300w, /sites/default/files/styles/12_6_media_huge/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=JBC6o4Rg 1600w" alt="City hall " typeof="foaf:Image" />

      <noscript>
      <img srcset="/sites/default/files/styles/12_6_media_tiny/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=8xIDKJb1 400w, /sites/default/files/styles/12_6_media_small/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=DcAjGSjs 750w, /sites/default/files/styles/12_6_media_medium/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=-fc_ZW7_ 1000w, /sites/default/files/styles/12_6_media_large/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=_yAlrt08 1300w, /sites/default/files/styles/12_6_media_huge/public/2019-09/general-london-175--1-.jpg?h=cb4d3a66&amp;itok=JBC6o4Rg 1600w" data-sizes="auto" alt="City hall " typeof="foaf:Image" />

    </noscript>
  




</div>
  
  <div class='container'>
    <div class='hero-promo__content'>
      <div class='hero-promo__content-left'>
        <h1 class='hero-promo__heading'>  Have your say on Thursday 7 May 2020
</h1>
      </div>
      <div class='hero-promo__content-right'>
        <p>
          <span class="hero-promo__summary">
              Londoners can vote for the Mayor and 25 Members of the London Assembly next year

          </span>

          <a href='/im-voter/what-do-mayor-london-and-london-assembly-do' class='button button--inverted'>
            <span>Find out what they do</span>
            <svg class='icon icon--stroke icon--arrow' focusable='false' role='presentation'><use xline: href="#sprite-icon-arrow"></use></svg>
          </a>
        </p>
      </div>
    </div>
  </div>
</div>


  
    
          


  


    <div class="container">
            <div data-drupal-messages-fallback class="hidden"></div><div id="block-numiko-mainpagecontent">
  
    
      <div about="/london-elects">

    
  <div>
            





      <section class="slice slice-teaser slice-teaser--default" aria-labelledby="paragraph-64-title" >
                    <h2 id="paragraph-64-title" class="visually-hidden">
                        Key information

                  </h2>
      
              <div class="l-grid l-grid--2-col l-grid--w-v-gutter l-grid--w-h-gutter">
      <div class="l-grid__item">
      
<article  about="/im-voter/registering-vote" typeof="schema:WebPage" class="teaser">

      <div class="teaser__img">
      <div class="teaser__img-inner">
                    <figure class="media media-teaser_landscape media-image">
  
  <div class="media__wrapper">
                <img class="lazyload" data-sizes="auto" data-srcset="/sites/default/files/styles/16_9_media_small/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=WOsELUfa 750w, /sites/default/files/styles/16_9_media_medium/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=DeDnQpjP 1000w, /sites/default/files/styles/16_9_media_large/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=Xt34Mo-O 1300w" alt="city hall interior " typeof="foaf:Image" />

      <noscript>
      <img srcset="/sites/default/files/styles/16_9_media_small/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=WOsELUfa 750w, /sites/default/files/styles/16_9_media_medium/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=DeDnQpjP 1000w, /sites/default/files/styles/16_9_media_large/public/2019-09/holocaust-memorial-service_056%20%281%29%20teaser.jpg?h=8d16384a&amp;itok=Xt34Mo-O 1300w" data-sizes="auto" alt="city hall interior " typeof="foaf:Image" />

    </noscript>
  




      </div>
</figure>

         
      </div>
    </div>
  

  <div class="teaser__content">
         

          <h3 class="teaser__title">
        <a href="/im-voter/registering-vote" class="teaser__link">
          <span class="animate-underline"><span property="schema:name">Registering to vote</span>
</span>
        </a>
      </h3>

              <div class="teaser__desc">
            <p>Find out how to register to vote, plus details about postal and proxy voting.</p>


        </div>
           
  </div>

</article>


    </div>
      <div class="l-grid__item">
      
<article  about="/im-candidate/nominations" typeof="schema:WebPage" class="teaser">

      <div class="teaser__img">
      <div class="teaser__img-inner">
                    <figure class="media media-teaser_landscape media-image">
  
  <div class="media__wrapper">
                <img class="lazyload" data-sizes="auto" data-srcset="/sites/default/files/styles/16_9_media_small/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=_2aPzjH- 750w, /sites/default/files/styles/16_9_media_medium/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=gorRrZHL 1000w, /sites/default/files/styles/16_9_media_large/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=Zlbymh0C 1300w" alt="" typeof="foaf:Image" />

      <noscript>
      <img srcset="/sites/default/files/styles/16_9_media_small/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=_2aPzjH- 750w, /sites/default/files/styles/16_9_media_medium/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=gorRrZHL 1000w, /sites/default/files/styles/16_9_media_large/public/2019-11/5130%20%281%29%20teaser.jpg?h=da73eb00&amp;itok=Zlbymh0C 1300w" data-sizes="auto" alt="" typeof="foaf:Image" />

    </noscript>
  




      </div>
</figure>

         
      </div>
    </div>
  

  <div class="teaser__content">
         

          <h3 class="teaser__title">
        <a href="/im-candidate/nominations" class="teaser__link">
          <span class="animate-underline"><span property="schema:name">Nominations</span>
</span>
        </a>
      </h3>

              <div class="teaser__desc">
            <p>Find out how to stand for Mayor or the London Assembly.</p>


        </div>
           
  </div>

</article>


    </div>
      <div class="l-grid__item">
      
<article  about="/im-candidate/statutory-notices-and-elections-timetable" typeof="schema:WebPage" class="teaser">

      <div class="teaser__img">
      <div class="teaser__img-inner">
                    <figure class="media media-teaser_landscape media-image">
  
  <div class="media__wrapper">
                <img class="lazyload" data-sizes="auto" data-srcset="/sites/default/files/styles/16_9_media_small/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=trung8VX 750w, /sites/default/files/styles/16_9_media_medium/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=cDj-uH2R 1000w, /sites/default/files/styles/16_9_media_large/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=lxplm7NM 1300w" alt="" typeof="foaf:Image" />

      <noscript>
      <img srcset="/sites/default/files/styles/16_9_media_small/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=trung8VX 750w, /sites/default/files/styles/16_9_media_medium/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=cDj-uH2R 1000w, /sites/default/files/styles/16_9_media_large/public/2019-11/6023%20%281%29%20teaser.jpg?h=b7ceca4b&amp;itok=lxplm7NM 1300w" data-sizes="auto" alt="" typeof="foaf:Image" />

    </noscript>
  




      </div>
</figure>

         
      </div>
    </div>
  

  <div class="teaser__content">
         

          <h3 class="teaser__title">
        <a href="/im-candidate/statutory-notices-and-elections-timetable" class="teaser__link">
          <span class="animate-underline"><span property="schema:name">Statutory notices and elections timetable</span>
</span>
        </a>
      </h3>

              <div class="teaser__desc">
            <p>See all the important dates in the electoral calendar.</p>


        </div>
           
  </div>

</article>


    </div>
      <div class="l-grid__item">
      
<article  about="/im-voter/how-vote-polling-day" typeof="schema:WebPage" class="teaser">

      <div class="teaser__img">
      <div class="teaser__img-inner">
                    <figure class="media media-teaser_landscape media-image">
  
  <div class="media__wrapper">
                <img class="lazyload" data-sizes="auto" data-srcset="/sites/default/files/styles/16_9_media_small/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=2ht3nqOs 750w, /sites/default/files/styles/16_9_media_medium/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=WluR1dac 1000w, /sites/default/files/styles/16_9_media_large/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=jK8QG1W_ 1300w" alt="london skyline" typeof="foaf:Image" />

      <noscript>
      <img srcset="/sites/default/files/styles/16_9_media_small/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=2ht3nqOs 750w, /sites/default/files/styles/16_9_media_medium/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=WluR1dac 1000w, /sites/default/files/styles/16_9_media_large/public/2019-09/llr_sunset_0006%20teaser.jpg?h=8d16384a&amp;itok=jK8QG1W_ 1300w" data-sizes="auto" alt="london skyline" typeof="foaf:Image" />

    </noscript>
  




      </div>
</figure>

         
      </div>
    </div>
  

  <div class="teaser__content">
         

          <h3 class="teaser__title">
        <a href="/im-voter/how-vote-polling-day" class="teaser__link">
          <span class="animate-underline"><span property="schema:name">How to vote on polling day</span>
</span>
        </a>
      </h3>

              <div class="teaser__desc">
            <p>You will be given three ballot papers on polling day - discover how they work. </p>


        </div>
           
  </div>

</article>


    </div>
  </div>

                </section>
  

      </div>

    
</div>

  </div>

  
    </div>
    
    

  </main>


  <footer class="footer" role="contentinfo">
  <div class="footer__upper">
    <div class="container">
      <div class="footer__upper-wrapper">
        <a class="footer__hyperlink" href="/" title="Home" rel="home">
                    <img src="/themes/custom/numiko/logo.png" class="footer__logo" alt="London Elects" />
        </a>
        <div class="footer__content">
                <nav role="navigation" class="footer__menu" aria-labelledby="block-footer-menu" id="block-footer">
      
  <h2 id="block-footer-menu">Quick links</h2>
  

        
              <ul class="menu">
                    <li class="menu-item">
              <a  class="menu__link menu__link--level-0" href="/about-glro-and-london-elects" ><span class="animate-underline">About London Elects</span>
        </a>
                      </li>
                <li class="menu-item">
              <a  class="menu__link menu__link--level-0" href="/contact-us" ><span class="animate-underline">Contact us</span>
        </a>
                      </li>
                <li class="menu-item">
              <a  class="menu__link menu__link--level-0" href="/im-voter/languages" ><span class="animate-underline">Languages</span>
        </a>
                      </li>
                <li class="menu-item">
              <a  class="menu__link menu__link--level-0" href="/home/legal-information" ><span class="animate-underline">Legal</span>
        </a>
                      </li>
                <li class="menu-item">
              <a  class="menu__link menu__link--level-0" href="/site-accessibility" ><span class="animate-underline">Site accessibility</span>
        </a>
                      </li>
        </ul>
  


  </nav>
<div id="block-followus">
  
      <h2>Follow us</h2>
    
      
              <ul class="u-list-reset social-list">
              <li class="social-list__item">
        <a href='https://www.facebook.com/londonelects/' class="social-list__hyperlink social-list__hyperlink--facebook" target="_blank" rel="nofollow">
          <span class="visually-hidden">Follow us onFacebook</span>

                      <svg class='icon icon--large icon--facebook' focusable="false" role="presentation"><use xlink:href='#sprite-icon-facebook'></use></svg>
                  </a>

              </li>
          <li class="social-list__item">
        <a href='https://twitter.com/londonelects' class="social-list__hyperlink social-list__hyperlink--twitter" target="_blank" rel="nofollow">
          <span class="visually-hidden">Follow us onTwitter</span>

                      <svg class='icon icon--large icon--twitter' focusable="false" role="presentation"><use xlink:href='#sprite-icon-twitter'></use></svg>
                  </a>

              </li>
          <li class="social-list__item">
        <a href='https://www.youtube.com/channel/UCr870xKSUVdtHH1AwIl2zwQ' class="social-list__hyperlink social-list__hyperlink--youtube" target="_blank" rel="nofollow">
          <span class="visually-hidden">Follow us onYouTube</span>

                      <svg class='icon icon--large icon--youtube' focusable="false" role="presentation"><use xlink:href='#sprite-icon-youtube'></use></svg>
                  </a>

              </li>
        </ul>
  


  </div>

  
        </div>
      </div>
    </div>
  </div>
  <div class="footer__lower">
    <div class="container">
      <span>Copyright &copy; London Elects 2019</span>
    </div>
  </div>
</footer> 
</div>

  </div>

      

    <script src="/core/assets/vendor/jquery/jquery.min.js?v=3.2.1"></script>
<script src="/core/assets/vendor/jquery/jquery-extend-3.4.0.js?v=3.2.1"></script>
<script src="/core/assets/vendor/jquery-once/jquery.once.min.js?v=2.2.0"></script>
<script src="/core/assets/vendor/picturefill/picturefill.min.js?v=3.0.1"></script>
<script src="/themes/custom/numiko/dist/js/bundle.js?pzthz0"></script>
<script src="/core/assets/vendor/matchMedia/matchMedia.min.js?v=0.2.0"></script>

  </body>
</html>
