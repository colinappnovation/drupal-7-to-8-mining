<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8"/>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create","UA-46232990-1",{"cookieDomain":"auto"});ga("require","linkid","linkid.js");ga("set","anonymizeIp",true);ga("set","page",location.pathname+location.search+location.hash);ga("send","pageview");</script>
<meta name="title" content="Salix Finance"/>
<link rel="shortlink" href="https://www.salixfinance.co.uk/"/>
<link rel="canonical" href="https://www.salixfinance.co.uk/"/>
<meta name="description" content="Salix Finance provides interest-free Government funding to the public sector to improve energy efficiency, reduce carbon emissions and lower energy bills."/>
<meta name="keywords" content="Salix Finance, Salix"/>
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)"/>
<meta name="MobileOptimized" content="width"/>
<meta name="HandheldFriendly" content="true"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" href="/themes/salix/favicon.ico" type="image/vnd.microsoft.icon"/>
<link rel="revision" href="https://www.salixfinance.co.uk/node/87"/>

    <title>Salix Finance</title>
    <link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/ajax-progress.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/align.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/autocomplete-loading.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/fieldgroup.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/container-inline.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/clearfix.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/details.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/hidden.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/item-list.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/js.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/nowrap.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/position-container.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/progress.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/reset-appearance.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/resize.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/sticky-header.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/system-status-counter.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/system-status-report-counters.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/system-status-report-general-info.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/tabledrag.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/tablesort.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/system/components/tree-child.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/core/themes/stable/css/views/views.module.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/themes/salix/css/style.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/themes/salix/vendor/jquery-ui-toooltips/jquery-ui.min.css?pzivhs"/>
<link rel="stylesheet" media="all" href="/themes/salix/vendor/OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css?pzivhs"/>

    
<!--[if lte IE 8]>
<script src="/core/assets/vendor/html5shiv/html5shiv.min.js?v=3.7.3"></script>
<![endif]-->

<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="f53060b0-f9f9-432a-bcee-2e1f5aa286ba" type="text/javascript" async></script>
  </head>
  <body class="layout-no-sidebars page-node-87 path-frontpage wide page-node-type-static-page">
        <a href="#main-content" class="visually-hidden focusable skip-link">
      Skip to main content
    </a>
    
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    
	<header class="page-header">
	    <div class="container">
	          <div class="region region-header">
    <div id="block-salix-branding" class="block block-system block-system-branding-block">
  
    
        <a href="/" title="Home" rel="home">
      <img src="/themes/salix/logo.png" alt="Home"/>
    </a>
      
</div>

    
    
    <button class="page-header__contact-trigger"><i class="fa fa-bars"></i> Contact</button>
    <div class="page-header__contact">
		<div class="page-header__contact-details">
			<p><strong>e:</strong> <a href="/cdn-cgi/l/email-protection#f39a9d959cb380929f9a8b959a9d929d9096dd909cdd8698" target="_top"><span class="__cf_email__" data-cfemail="a6cfc8c0c9e6d5c7cacfdec0cfc8c7c8c5c388c5c988d3cd">[email&#160;protected]</span></a></p>
			<p><strong>t:</strong> <a href="tel:02031026900">020 3102 6900</a></p>
		</div>
		<div class="page-header__contact-social">
			<a href="https://twitter.com/SalixFinance" target="_blank" title="Find Salix on Twitter"><i class="fa fa-twitter" aria-hidden="true"></i>
		</a>
		</div>
	</div>    
    
  </div>

	    </div>
	</header>
<div class="primary-navigation">
	<div class="container">
		<button class="primary-navigation-trigger">
			<i class="fa fa-bars fa-fw fa-lg"></i> 
			<i class="fa fa-times fa-fw fa-lg"></i>
			Menu
		</button>
		  <div class="region region-navigation">
    <nav role="navigation" aria-labelledby="block-salix-main-menu-menu" id="block-salix-main-menu">
            
  <h2 class="visually-hidden" id="block-salix-main-menu-menu">Main navigation</h2>
  

        
      	      <ul>
      
          <li>
        <a href="/loans" data-drupal-link-system-path="node/213">England</a>
                        	      <ul>
      
          <li>
        <a href="/loans/england-loans/local-authorities" data-drupal-link-system-path="node/16649">Local Authority</a>
              </li>
          <li>
        <a href="/loans/england-loans/hei" data-drupal-link-system-path="node/16651">Higher Education Institutions</a>
              </li>
          <li>
        <a href="/loans/further-education-institutions" data-drupal-link-system-path="node/16655">Further Education Institutions</a>
              </li>
          <li>
        <a href="/loans/england-loans/nhs" data-drupal-link-system-path="node/16650">NHS Trusts and Foundation Trusts</a>
              </li>
          <li>
        <a href="/loans/maintained-schools-and-sixth-form-colleges" data-drupal-link-system-path="node/16656">Maintained Schools &amp; Sixth Form Colleges</a>
              </li>
          <li>
        <a href="/loans/academies" data-drupal-link-system-path="node/16657">Academies &amp; Academy Sixth Forms</a>
              </li>
            </ul>
      
              </li>
          <li>
        <a href="/loans/scotland-loans" data-drupal-link-system-path="node/214">Scotland</a>
                        	      <ul>
      
          <li>
        <a href="/loans/scotland-loans" data-drupal-link-system-path="node/214">All Public Sector Bodies</a>
              </li>
          <li>
        <a href="/recycling-fund/scotland-recycling-fund" data-drupal-link-system-path="node/16652">Scotland Recycling Fund</a>
              </li>
          <li>
        <a href="/scotland/ucrf" data-drupal-link-system-path="node/16653">University Carbon Reduction Fund</a>
              </li>
          <li>
        <a href="/UFTF" data-drupal-link-system-path="node/17091">Universities For The Future: Decarbonising Scotland</a>
              </li>
            </ul>
      
              </li>
          <li>
        <a href="/loans/welsh-loans" data-drupal-link-system-path="node/215">Wales</a>
              </li>
          <li>
        <a href="/recycling-fund" data-drupal-link-system-path="node/216">Recycling Fund</a>
                        	      <ul>
      
          <li>
        <a href="https://sers.salixfinance.co.uk/sun/(S(snykas55o0te4fqnge40jr45))/login.aspx">Connect to SERS</a>
              </li>
          <li>
        <a href="/recycling-fund/hefce-rf" data-drupal-link-system-path="node/16717">HEFCE SALIX RGF1 Clients</a>
              </li>
          <li>
        <a href="/recycling-fund/scotland-recycling-fund" data-drupal-link-system-path="node/16652">Scotland Recycling Fund</a>
              </li>
            </ul>
      
              </li>
          <li>
        <a href="/knowledge-share" data-drupal-link-system-path="node/110">Knowledge Share</a>
                        	      <ul>
      
          <li>
        <a href="/knowledge-share/eligible-technologies" data-drupal-link-system-path="node/16648">Eligible Technologies</a>
              </li>
          <li>
        <a href="/knowledge-share/case-studies" data-drupal-link-system-path="knowledge-share/case-studies">Case Studies</a>
              </li>
          <li>
        <a href="/knowledge-share/pks" data-drupal-link-system-path="knowledge-share/pks">Project Knowledge Slides</a>
              </li>
          <li>
        <a href="/knowledge-share/compliance-tools" data-drupal-link-system-path="knowledge-share/compliance-tools">Compliance Tools</a>
              </li>
          <li>
        <a href="/knowledge-share/best-practice-calculations" data-drupal-link-system-path="knowledge-share/best-practice-calculations">Best Practice Calculations</a>
              </li>
          <li>
        <a href="/knowledge-share/client-support-material" data-drupal-link-system-path="knowledge-share/client-support-material">Client Support Material</a>
              </li>
          <li>
        <a href="/knowledge-share/regional-meeting-material" data-drupal-link-system-path="knowledge-share/regional-meeting-material">Regional Meeting Material</a>
              </li>
            </ul>
      
              </li>
          <li>
        <a href="/news" data-drupal-link-system-path="news">News</a>
              </li>
          <li>
        <a href="/events" data-drupal-link-system-path="node/16654">Events</a>
              </li>
          <li>
        <a href="/about-us" data-drupal-link-system-path="node/83">About Us</a>
                        	      <ul>
      
          <li>
        <a href="/about-us/our-core-funders" data-drupal-link-system-path="about-us/our-core-funders">Our Core Funders</a>
              </li>
          <li>
        <a href="/about-us/our-partners" data-drupal-link-system-path="about-us/our-partners">Our Partners</a>
              </li>
          <li>
        <a href="/about-us/our-people" data-drupal-link-system-path="node/229">Our People</a>
              </li>
          <li>
        <a href="/about-us/salix-values" data-drupal-link-system-path="node/17055">Our Values</a>
              </li>
          <li>
        <a href="/about-us/successes" data-drupal-link-system-path="node/16663">Successes To Date</a>
              </li>
          <li>
        <a href="/about-us/media" data-drupal-link-system-path="about-us/media">Media</a>
              </li>
          <li>
        <a href="/about-us/info-consultants-contractors-suppliers" data-drupal-link-system-path="node/228">Info for Suppliers, Consultants and Contractors</a>
              </li>
          <li>
        <a href="/about-us/vacancies" data-drupal-link-system-path="node/16965">Vacancies</a>
              </li>
          <li>
        <a href="/contact-us" data-drupal-link-system-path="webform/contact_us">Contact Us</a>
              </li>
            </ul>
      
              </li>
          



	<li class="separator">|</li>
	
			<li>
			<a href="/user/login">
				Login
			</a>
		</li>
	</ul>

  </nav>

  </div>

	</div>
</div>
<div class="admin-actions">
	<div class="container">
    	  <div class="region region-admin-actions">
    
  </div>

    </div>
</div>
<main class="page-main">
							<div class="container highlighted">
				  <div class="region region-highlighted">
    <div data-drupal-messages-fallback class="hidden"></div>

  </div>

			</div>
			    
    	
			  <div class="region region-front-primary-block-1">
    <div class="views-element-container block block-views block-views-blockhero-carousel-hero-carousel" id="block-views-block-hero-carousel-hero-carousel-2">
  
    
      <div><div class="js-view-dom-id-b89349a7d48dd8ab123745f692ebd652e6dc548cc09996ed7a75ac1f4b2e8c4b">
  
  
  

  
  
  

      <div class="owl-carousel hero-carousel">
                    <div class="item">
                  <img src="/sites/default/files/2019-10/AdobeStock_209439761_carousel_0.jpg" width="2400" height="700" alt="" typeof="foaf:Image"/>


                <div class="hero-carousel__content">
                    <div class="banner-caption">
                        <h3>Salix Colleges Workshops 2019 - Dates Announced!</h3>
                        <p>For more information and to reserve your place, click here. </p>
                    </div>
                </div>
                <a href="https://www.salixfinance.co.uk/node/17246">https://www.salixfinance.co.uk/node/17246</a>
            </div>
                    <div class="item">
                  <img src="/sites/default/files/2019-07/IIP%20Gold.jpg" width="2400" height="700" alt="" typeof="foaf:Image"/>


                <div class="hero-carousel__content">
                    <div class="banner-caption">
                        <h3>Salix Awarded Gold!</h3>
                        <p>Salix are delighted to announce that the organisation has been awarded the Investors in People Gold accreditation.</p>
                    </div>
                </div>
                <a href="https://www.salixfinance.co.uk/node/17212">https://www.salixfinance.co.uk/node/17212</a>
            </div>
                    <div class="item">
                  <img src="/sites/default/files/2019-08/Carousel%20Compliance%20Tool%20Update%202019.jpg" width="2400" height="700" alt="" typeof="foaf:Image"/>


                <div class="hero-carousel__content">
                    <div class="banner-caption">
                        <h3>Salix introduce new combined compliance tool and business case</h3>
                        <p>To view the tool, click here. </p>
                    </div>
                </div>
                <a href="https://www.salixfinance.co.uk/knowledge-share/compliance-tools">https://www.salixfinance.co.uk/knowledge-share/compliance-tools</a>
            </div>
                    <div class="item">
                  <img src="/sites/default/files/2019-07/PSN%20Carousel.jpg" width="2400" height="700" alt="" typeof="foaf:Image"/>


                <div class="hero-carousel__content">
                    <div class="banner-caption">
                        <h3>Carbon Trust &amp; Salix Partner to Relaunch Public Sector Network</h3>
                        <p>For more information, click here. </p>
                    </div>
                </div>
                <a href="/news/carbon-trust-and-salix-finance-partner-relaunch-public-sector-network-serve-public-bodies">/news/carbon-trust-and-salix-finance-partner-relaunch-public-sector-network-ser…</a>
            </div>
                    <div class="item">
                  <img src="/sites/default/files/2019-07/Referencing%20Salix%20Carousel_0.jpg" width="2400" height="700" alt="" typeof="foaf:Image"/>


                <div class="hero-carousel__content">
                    <div class="banner-caption">
                        <h3>Referencing Salix </h3>
                        <p>If you have seen the use of Salix's name on other websites then please check with us for clarification.</p>
                    </div>
                </div>
                <a href="/about-us/info-consultants-contractors-suppliers">/about-us/info-consultants-contractors-suppliers</a>
            </div>
            </div>

    

  
  

  
  
</div>
</div>

  </div>

  </div>

		
	<div class="container">
				
        <div class="content">
        	        		<div class="front-content-primary cf">
	<div class="front-content-primary__lead-text">
		  <div class="region region-content">
    <div id="block-pagetitle" class="block block-core block-page-title-block">
  
    
      
  <h1><span>Homepage</span>
</h1>


  </div>
<div id="block-migrate-drupal-7" class="block block-system block-system-main-block">
  
    
      <article data-history-node-id="87" role="article" about="/node/87">

  
    

  
  <div>
    
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p><strong>Salix Finance Ltd.</strong>  provides interest-free Government funding to the public sector to improve energy efficiency, reduce carbon emissions and lower energy bills. Salix is funded by the Department for Business, Energy and Industrial Strategy, the Department for Education, the Welsh Government and the Scottish Government and was established in 2004 as an independent, publicly funded company, dedicated to providing the public sector with loans for energy efficiency projects. We currently have funding available for England, Scotland and Wales and more information can be accessed <a href="/loans">here.</a></p>
</div>
      
  </div>

</article>

  </div>

  </div>

	</div>
	<div class="front-content-primary__sidebar">
					  <div class="region region-front-primary-block-2">
    <div id="block-delivery-2" class="block block-block-content block-block-content70d59d22-403a-467a-ae70-9c69a179a9da block-feature">
	
			<div class="block-feature__header">
		  <h4>Delivery</h4>
		</div>
		
	<div class="block-feature__content">
	  
		  
		  
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><table><tbody><tr><td class="left">Number of projects funded</td>
			<td class="right">17,799</td>
		</tr><tr><td class="left">Value of projects funded</td>
			<td class="right">£ 842 million</td>
		</tr><tr><td class="left">Value of annual financial savings</td>
			<td class="right">£ 181 million</td>
		</tr><tr><td class="left">Value of annual carbon savings</td>
			<td class="right">821,583 tonnes of CO<sub>2</sub></td>
		</tr></tbody></table></div>
      
		  
  </div>
</div>

  </div>

			</div>
</div>

<div class="front-content-secondary">
			  <div class="region region-front-secondary-block-1">
    <div class="views-element-container block block-views block-views-blocklatest-news-front-block-1 block-feature" id="block-views-block-latest-news-front-block-1-2">
	
			<div class="block-feature__header">
		  <h4>Latest News</h4>
		</div>
		
	<div class="block-feature__content">
	  
		  
		  <div><div class="news-list-block js-view-dom-id-25054ceb354c26c35816b93417112d9528a73af0719a5adc85fef05cdb7f2f60">
  
  
  

  
  
  

      <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17257" hreflang="en">Green Gown 2019 Winners Leading the Way to a Low Carbon Future</a></span></div><div class="views-field views-field-created"><span class="field-content">05 Dec 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17251" hreflang="en">Movember at Salix by James Garth – Building Resilience </a></span></div><div class="views-field views-field-created"><span class="field-content">14 Nov 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17250" hreflang="en">Movember at Salix by James Garth, Client Support Officer</a></span></div><div class="views-field views-field-created"><span class="field-content">08 Nov 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17248" hreflang="en">Condition Improvement Fund (CIF) for 2020-21 Now Open!</a></span></div><div class="views-field views-field-created"><span class="field-content">30 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17246" hreflang="en">Salix Colleges Workshops 2019 - Dates Announced!</a></span></div><div class="views-field views-field-created"><span class="field-content">29 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17245" hreflang="en">Waltham Forest Council support £2.4m in carbon reduction and energy efficiency upgrades</a></span></div><div class="views-field views-field-created"><span class="field-content">22 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17244" hreflang="en">Tickets available for Salix hosted Private Sector Workshops</a></span></div><div class="views-field views-field-created"><span class="field-content">21 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17243" hreflang="en">£15 million funding programme available for maintained schools </a></span></div><div class="views-field views-field-created"><span class="field-content">16 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17240" hreflang="en">Eleven Scottish Universities are delivering carbon reduction projects through the Scottish Funding Council’s University Carbon Reduction Fund </a></span></div><div class="views-field views-field-created"><span class="field-content">11 Oct 2019</span></div></div>
    <div class="views-row"><div class="views-field views-field-title"><span class="field-content"><a href="/node/17238" hreflang="en">Dates for Salix hosted Private Sector Workshops Announced! </a></span></div><div class="views-field views-field-created"><span class="field-content">30 Sep 2019</span></div></div>

    

  
  

  
  
</div>
</div>

		  
  </div>
</div>

  </div>

	
	
			  <div class="region region-front-secondary-block-3">
    <div id="block-salixtwitterfeedblock" class="block block-salix-twitter-feed block-salix-twitter-feed-block block-feature">
	
			<div class="block-feature__header">
		  <h4>@SalixFinance</h4>
		</div>
		
	<div class="block-feature__content">
	  
		  
		  <ul class="twitter-feed"><li><div class="tweet"><p>From 100% renewable energy powered campuses to zero waste shops, click to find out more about how the @greengowns 2019 winners are leading the way to a low carbon future 🍃🌍<br/>
 <a href="https://t.co/jVBuVVH2yG">https://t.co/jVBuVVH2yG</a> @TheEAUC @EAUCScotland <a href="https://t.co/cVugJ61sDP">https://t.co/cVugJ61sDP</a> <a href="https://t.co/YvezV63qBG">https://t.co/YvezV63qBG</a></p>
</div>
<time class="timeago" datetime="2019-12-05T16:05:08+00:00">05/12/2019 - 16:05</time>
</li><li><div class="tweet"><p>Your energy efficiency upgrades are supporting more than just your estate 🚶 This #WorldSoilDay take a moment to reflect on the importance of reducing your carbon footprint and protecting the earth beneath your feet 👣 and the global food supply 🍎 #ThursdayThoughts <a href="https://t.co/BJlVn4FcsS">https://t.co/BJlVn4FcsS</a> <a href="https://t.co/diEEgvfB3N">https://t.co/diEEgvfB3N</a></p>
</div>
<time class="timeago" datetime="2019-12-05T14:55:09+00:00">05/12/2019 - 14:55</time>
</li><li><div class="tweet"><p>Through a combination of LED lighting and boiler projects @torfaencouncil will make estimated savings of over £297,200 over the lifetime of their #energyefficient technologies, ready to reinvest 💰 on top of reducing over 100 tonnes of CO2 per annum 🍃 #ClimateAction <a href="https://t.co/stR9S04XlV">https://t.co/stR9S04XlV</a> <a href="https://t.co/5bDDyFUUaD">https://t.co/5bDDyFUUaD</a></p>
</div>
<time class="timeago" datetime="2019-12-05T11:45:14+00:00">05/12/2019 - 11:45</time>
</li><li><div class="tweet"><p>This #InternationalVolunteerDay Salix are reflecting on the amazing efforts made by staff to raise money &amp; awareness for several charities 💗 Staff take it upon themselves to volunteer for an inclusive future for all, embodying the core Salix value of selflessness 🤝 #IVD2019 <a href="https://t.co/v9d8Ml31l0">https://t.co/v9d8Ml31l0</a> <a href="https://t.co/aXSHOtxXji">https://t.co/aXSHOtxXji</a></p>
</div>
<time class="timeago" datetime="2019-12-05T11:40:05+00:00">05/12/2019 - 11:40</time>
</li><li><div class="tweet"><p>Provisional data from the @WMO suggests the decade from 2010-2019 is on course to be the warmest ever recorded 🌍 #StateofClimate #ClimateChange <a href="https://t.co/kt8FvaSYNz">https://t.co/kt8FvaSYNz</a> <a href="https://t.co/FRv1PHEk1u">https://t.co/FRv1PHEk1u</a></p>
</div>
<time class="timeago" datetime="2019-12-04T17:42:15+00:00">04/12/2019 - 17:42</time>
</li><li><div class="tweet"><p>Things are heating up 🌡️ @unibirmingham this winter ❄️ after the installation of a £46,000 pipework insulation project. The upgrade is estimated to achieve £178,289 in lifetime financial savings &amp; save around 1,093 tonnes of CO2 per annum! #SalixFunded #EstatesManagement 🍃🌏 <a href="https://t.co/5PoUqD2YD7">https://t.co/5PoUqD2YD7</a></p>
</div>
<time class="timeago" datetime="2019-12-04T11:01:19+00:00">04/12/2019 - 11:01</time>
</li></ul>
		  
  </div>
</div>

  </div>

	</div>			        </div>
             
			</div>
</main>
<footer class="page-footer">
				  <div class="region region-footer">
    <div id="block-websitefooter" class="block block-block-content block-block-content959ba730-72c6-4c20-9d08-465b2438cc04">
  
    
      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><div class="container">
<div class="footer-left">
<p>Salix Finance Ltd is an independent, not for profit company funded by The Department for Business, Energy and Industrial Strategy, The Welsh Assembly Government and The Scottish Government. Salix works in partnership with The Office for Students for England on its Revolving Green Fund.</p>

<div class="copyright">Salix Finance © 2004 - 2019</div>

<div class="footer-links">
<ul><li><a href="/privacy-policy">Privacy Policy</a></li>
	<li><a href="/terms-and-conditions">Terms &amp; Conditions</a></li>
	<li><a href="/acceptableuser">Acceptable User Policy</a></li>
	<li><a href="/cookie-declaration">Cookies</a></li>
	<li><a href="/contact-us">Contact Us</a></li>
</ul></div>
</div>

<div class="footer-right">
<p><strong>Salix Finance Ltd</strong></p>

<div class="footer-email-phone">
<p><span>email:</span> <a href="/cdn-cgi/l/email-protection#157c7b737a556674797c6d737c7b747b76703b767a3b607e"><span class="__cf_email__" data-cfemail="1970777f76596a787570617f707778777a7c377a76376c72">[email&#160;protected]</span></a></p>

<p><span>tel:</span> <a href="tel:02031026900">020 3102 6900</a></p>
</div>

<p>Salix Finance is a company limited by guarantee.</p>

<p>Registered in England and Wales with number 5068355</p>

<p>Office address: 75 King William Street London EC4N 7BE</p>
</div>
</div>

<div class="page-footer__additional-section">
<div class="container"><a href="http://objectsource.co.uk" target="_blank" title="Web design &amp; development by objectsource">Web design &amp; development by objectsource</a></div>
</div>
</div>
      
  </div>

  </div>

	</footer>

  </div>

    
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/87","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","ajaxPageState":{"libraries":"core\/html5shiv,google_analytics\/google_analytics,salix\/global-styling,salix\/home,salix_twitter_feed\/timeago,system\/base,views\/views.ajax,views\/views.module","theme":"salix","theme_token":null},"ajaxTrustedUrl":[],"google_analytics":{"trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackUrlFragments":true},"views":{"ajax_path":"\/views\/ajax","ajaxViews":{"views_dom_id:b89349a7d48dd8ab123745f692ebd652e6dc548cc09996ed7a75ac1f4b2e8c4b":{"view_name":"hero_carousel","view_display_id":"hero_carousel","view_args":"","view_path":"\/node\/87","view_base_path":null,"view_dom_id":"b89349a7d48dd8ab123745f692ebd652e6dc548cc09996ed7a75ac1f4b2e8c4b","pager_element":0}}},"user":{"uid":0,"permissionsHash":"59973893bd4101f95e6caae839e4fcfa8d65fbe7963360da94e1947c50b11819"}}</script>
<script src="/core/assets/vendor/domready/ready.min.js?v=1.0.8"></script>
<script src="/core/assets/vendor/jquery/jquery.min.js?v=3.2.1"></script>
<script src="/core/assets/vendor/jquery/jquery-extend-3.4.0.js?v=3.2.1"></script>
<script src="/core/assets/vendor/jquery-once/jquery.once.min.js?v=2.2.0"></script>
<script src="/core/misc/drupalSettingsLoader.js?v=8.7.7"></script>
<script src="/core/misc/drupal.js?v=8.7.7"></script>
<script src="/core/misc/drupal.init.js?v=8.7.7"></script>
<script src="/modules/contrib/google_analytics/js/google_analytics.js?v=8.7.7"></script>
<script src="/themes/salix/vendor/jquery-ui-toooltips/jquery-ui.js?pzivhs"></script>
<script src="/themes/salix/js/theme.js?pzivhs"></script>
<script src="/modules/custom/salix_twitter_feed/js/salix_twitter_feed.js?v=1.5.2"></script>
<script src="//jquery.timeago.js"></script>
<script src="/core/assets/vendor/jquery-form/jquery.form.min.js?v=4.22"></script>
<script src="/core/misc/progress.js?v=8.7.7"></script>
<script src="/core/misc/ajax.js?v=8.7.7"></script>
<script src="/core/modules/views/js/base.js?v=8.7.7"></script>
<script src="/core/modules/views/js/ajax_view.js?v=8.7.7"></script>
<script src="/themes/salix/vendor/OwlCarousel2-2.2.1/dist/owl.carousel.min.js?pzivhs"></script>
<script src="/themes/salix/js/home.js?pzivhs"></script>

  </body>
</html>
