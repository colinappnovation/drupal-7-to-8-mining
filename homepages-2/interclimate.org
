<!doctype html>
<html lang="en-GB">
<head>
<meta charset="utf-8">
<title>Interclimate Network - Inspiring the next generation of climate leaders</title>
<link href="https://interclimate.org/wp-content/themes/interclimate/img/icons/favicon.ico" rel="shortcut icon">
<link rel="apple-touch-icon" sizes="180x180" href="https://interclimate.org/wp-content/themes/interclimate/img/icons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://interclimate.org/wp-content/themes/interclimate/img/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://interclimate.org/wp-content/themes/interclimate/img/icons/favicon-16x16.png">
<link rel="manifest" href="https://interclimate.org/wp-content/themes/interclimate/img/icons/site.webmanifest">
<link rel="mask-icon" href="https://interclimate.org/wp-content/themes/interclimate/img/icons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-config" content="https://interclimate.org/wp-content/themes/interclimate/img/icons/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Inspiring the next generation of climate leaders">
<!-- This site is optimized with the Yoast SEO plugin v12.6.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Engaging young people with the challenges of Climate Change, inspiring their climate action, and promoting their voices in their own sustainable future."/>
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
<link rel="canonical" href="https://interclimate.org/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Interclimate Network - Inspiring the next generation of climate leaders" />
<meta property="og:description" content="Engaging young people with the challenges of Climate Change, inspiring their climate action, and promoting their voices in their own sustainable future." />
<meta property="og:url" content="https://interclimate.org/" />
<meta property="og:site_name" content="Interclimate Network" />
<meta property="og:image" content="https://interclimate.org/wp-content/uploads/2019/07/student-at-climate-conference.jpg" />
<meta property="og:image:secure_url" content="https://interclimate.org/wp-content/uploads/2019/07/student-at-climate-conference.jpg" />
<meta property="og:image:width" content="2000" />
<meta property="og:image:height" content="1160" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Engaging young people with the challenges of Climate Change, inspiring their climate action, and promoting their voices in their own sustainable future." />
<meta name="twitter:title" content="Interclimate Network - Inspiring the next generation of climate leaders" />
<meta name="twitter:site" content="@ClimateVoicesUK" />
<meta name="twitter:image" content="https://interclimate.org/wp-content/uploads/2019/07/student-at-climate-conference.jpg" />
<meta name="twitter:creator" content="@ClimateVoicesUK" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://interclimate.org/#organization","name":"InterClimate Network","url":"https://interclimate.org/","sameAs":["https://www.facebook.com/InterClimate-Network-205375216158240/","https://www.instagram.com/interclimatenetwork/","https://twitter.com/ClimateVoicesUK"],"logo":{"@type":"ImageObject","@id":"https://interclimate.org/#logo","url":"https://interclimate.org/wp-content/uploads/2019/07/logo.png","width":300,"height":300,"caption":"InterClimate Network"},"image":{"@id":"https://interclimate.org/#logo"}},{"@type":"WebSite","@id":"https://interclimate.org/#website","url":"https://interclimate.org/","name":"Interclimate Network","description":"Inspiring the next generation of climate leaders","publisher":{"@id":"https://interclimate.org/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://interclimate.org/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://interclimate.org/#primaryimage","url":"https://interclimate.org/wp-content/uploads/2019/07/student-at-climate-conference.jpg","width":2000,"height":1160},{"@type":"WebPage","@id":"https://interclimate.org/#webpage","url":"https://interclimate.org/","inLanguage":"en-GB","name":"Interclimate Network - Inspiring the next generation of climate leaders","isPartOf":{"@id":"https://interclimate.org/#website"},"about":{"@id":"https://interclimate.org/#organization"},"primaryImageOfPage":{"@id":"https://interclimate.org/#primaryimage"},"datePublished":"2019-05-30T15:10:43+00:00","dateModified":"2019-11-21T18:06:30+00:00","description":"Engaging young people with the challenges of Climate Change, inspiring their climate action, and promoting their voices in their own sustainable future."}]}</script>
<!-- / Yoast SEO plugin. -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script type="text/javascript" data-cfasync="false">
var mi_version         = '7.10.0';
var mi_track_user      = true;
var mi_no_track_reason = '';
var disableStr = 'ga-disable-UA-32462305-1';
/* Function to detect opted out users */
function __gaTrackerIsOptedOut() {
return document.cookie.indexOf(disableStr + '=true') > -1;
}
/* Disable tracking if the opt-out cookie exists. */
if ( __gaTrackerIsOptedOut() ) {
window[disableStr] = true;
}
/* Opt-out function */
function __gaTrackerOptout() {
document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
window[disableStr] = true;
}
if ( mi_track_user ) {
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');
__gaTracker('create', 'UA-32462305-1', 'auto');
__gaTracker('set', 'forceSSL', true);
__gaTracker('send','pageview');
} else {
console.log( "" );
(function() {
/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
var noopfn = function() {
return null;
};
var noopnullfn = function() {
return null;
};
var Tracker = function() {
return null;
};
var p = Tracker.prototype;
p.get = noopfn;
p.set = noopfn;
p.send = noopfn;
var __gaTracker = function() {
var len = arguments.length;
if ( len === 0 ) {
return;
}
var f = arguments[len-1];
if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
return;
}
try {
f.hitCallback();
} catch (ex) {
}
};
__gaTracker.create = function() {
return new Tracker();
};
__gaTracker.getByName = noopnullfn;
__gaTracker.getAll = function() {
return [];
};
__gaTracker.remove = noopfn;
window['__gaTracker'] = __gaTracker;
})();
}
</script>
<!-- / Google Analytics by MonsterInsights -->
<!-- <link rel='stylesheet' id='wp-block-library-css'  href='https://interclimate.org/wp-includes/css/dist/block-library/style.min.css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='https://interclimate.org/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.5' media='all' /> -->
<!-- <link rel='stylesheet' id='font-awesome-css'  href='https://interclimate.org/wp-content/themes/interclimate/fonts/font-awesome/css/font-awesome.min.css?ver=1.0' media='all' /> -->
<!-- <link rel='stylesheet' id='slick-css'  href='https://interclimate.org/wp-content/themes/interclimate/js/lib/slick/slick.css?ver=1.8.1' media='all' /> -->
<link rel="stylesheet" type="text/css" href="//interclimate.org/wp-content/cache/wpfc-minified/e3n6nzk2/84l4c.css" media="all"/>
<link rel='stylesheet' id='google-fonts-css'  href='https://fonts.googleapis.com/css?family=Montserrat%3A300%2C400%2C500%2C600%7CSanchez&#038;display=swap' media='all' />
<!-- <link rel='stylesheet' id='interclimate-style-css'  href='https://interclimate.org/wp-content/themes/interclimate/style.css?ver=1.0' media='all' /> -->
<link rel="stylesheet" type="text/css" href="//interclimate.org/wp-content/cache/wpfc-minified/m7yhu7ug/84l4b.css" media="all"/>
<!-- <link rel='stylesheet' id='fancybox-css'  href='https://interclimate.org/wp-content/plugins/easy-fancybox/css/jquery.fancybox.min.css?ver=1.3.24' media='screen' /> -->
<link rel="stylesheet" type="text/css" href="//interclimate.org/wp-content/cache/wpfc-minified/f5995qa3/84l4b.css" media="screen"/>
<script src='//interclimate.org/wp-content/cache/wpfc-minified/1cb97max/84l4c.js' type="text/javascript"></script>
<!-- <script type='text/javascript' src='https://interclimate.org/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script> -->
<!-- <script type='text/javascript' src='https://interclimate.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script> -->
<!-- <script type='text/javascript' src='https://interclimate.org/wp-content/themes/interclimate/js/lib/instafeed.min.js?ver=1.0.0'></script> -->
<!-- <script type='text/javascript' src='https://interclimate.org/wp-content/themes/interclimate/js/scripts.js?ver=1.0.0'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,docx,pdf,ppt,zip,xls,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/interclimate.org","hash_tracking":"false"};
/* ]]> */
</script>
<script src='//interclimate.org/wp-content/cache/wpfc-minified/6zqpunoh/84l4c.js' type="text/javascript"></script>
<!-- <script type='text/javascript' src='https://interclimate.org/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.0'></script> -->
<!-- <script type='text/javascript' src='https://interclimate.org/wp-content/themes/interclimate/js/lib/slick/slick.min.js?ver=1.8.1'></script> -->
<!-- <script type='text/javascript' src='https://interclimate.org/wp-content/themes/interclimate/js/scripts-front.js?ver=1.0.0'></script> -->
<link rel='https://api.w.org/' href='https://interclimate.org/wp-json/' />
<link rel="alternate" type="application/json+oembed" href="https://interclimate.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Finterclimate.org%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://interclimate.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Finterclimate.org%2F&#038;format=xml" />
</head>
<body class="home page-template-default page page-id-12">
<!-- wrapper -->
<div class="wrapper clear">
<!-- header -->
<header class="header clear" role="banner">
<!-- logo -->
<div class="logo">
<a href="https://interclimate.org">
<img src="https://interclimate.org/wp-content/themes/interclimate/img/logo.png" alt="Interclimate Network" class="logo-img">
</a>
</div>
<!-- /logo -->
<div class="button_container" id="toggle">
<span class="top"></span>
<span class="middle"></span>
<span class="bottom"></span>
</div>
<!-- nav -->
<nav class="nav" role="navigation">
<ul class="clear"><li id="menu-item-50" class="current-menu-item current_page_item"><a href="https://interclimate.org/" aria-current="page">Home</a></li>
<li id="menu-item-51"><a href="https://interclimate.org/about-us/">About Us</a>
<ul class="sub-menu">
<li id="menu-item-205"><a href="https://interclimate.org/about-us/">What We Do</a></li>
<li id="menu-item-198"><a href="https://interclimate.org/about-us/benefits/">Benefits</a></li>
<li id="menu-item-197"><a href="https://interclimate.org/about-us/impact/">Impact</a></li>
<li id="menu-item-196"><a href="https://interclimate.org/about-us/our-team/">Our Team</a></li>
<li id="menu-item-195"><a href="https://interclimate.org/about-us/our-history/">Our History</a></li>
<li id="menu-item-194"><a href="https://interclimate.org/about-us/governance/">Governance</a></li>
</ul>
</li>
<li id="menu-item-53"><a href="https://interclimate.org/climate-conferences/">Climate Conferences</a>
<ul class="sub-menu">
<li id="menu-item-334"><a href="https://interclimate.org/event_category/climate-conferences/">Events</a></li>
<li id="menu-item-203"><a href="https://interclimate.org/climate-conferences/global-negotiations/">Global Negotiations</a></li>
<li id="menu-item-202"><a href="https://interclimate.org/climate-conferences/local-action-workshops/">Local Action Workshops</a></li>
<li id="menu-item-204"><a href="https://interclimate.org/climate-conferences/resources-briefings/">Resources &#038; Briefings</a></li>
</ul>
</li>
<li id="menu-item-52"><a href="https://interclimate.org/action-in-schools/">Action in Schools</a></li>
<li id="menu-item-57"><a href="https://interclimate.org/stories/">Stories</a></li>
<li id="menu-item-559"><a href="https://interclimate.org/get-involved/">Get Involved</a></li>
<li id="menu-item-458"><a href="https://interclimate.org/resources/">Resources</a></li>
<li id="menu-item-54"><a href="https://interclimate.org/contact-us/">Contact Us</a></li>
</ul>				</nav>
<!-- /nav -->
<div class="overlay" id="overlay">
<nav class="overlay-menu" role="navigation">
<ul class="clear"><li class="current-menu-item current_page_item"><a href="https://interclimate.org/" aria-current="page">Home</a></li>
<li><a href="https://interclimate.org/about-us/">About Us</a>
<ul class="sub-menu">
<li><a href="https://interclimate.org/about-us/">What We Do</a></li>
<li><a href="https://interclimate.org/about-us/benefits/">Benefits</a></li>
<li><a href="https://interclimate.org/about-us/impact/">Impact</a></li>
<li><a href="https://interclimate.org/about-us/our-team/">Our Team</a></li>
<li><a href="https://interclimate.org/about-us/our-history/">Our History</a></li>
<li><a href="https://interclimate.org/about-us/governance/">Governance</a></li>
</ul>
</li>
<li><a href="https://interclimate.org/climate-conferences/">Climate Conferences</a>
<ul class="sub-menu">
<li><a href="https://interclimate.org/event_category/climate-conferences/">Events</a></li>
<li><a href="https://interclimate.org/climate-conferences/global-negotiations/">Global Negotiations</a></li>
<li><a href="https://interclimate.org/climate-conferences/local-action-workshops/">Local Action Workshops</a></li>
<li><a href="https://interclimate.org/climate-conferences/resources-briefings/">Resources &#038; Briefings</a></li>
</ul>
</li>
<li><a href="https://interclimate.org/action-in-schools/">Action in Schools</a></li>
<li><a href="https://interclimate.org/stories/">Stories</a></li>
<li><a href="https://interclimate.org/get-involved/">Get Involved</a></li>
<li><a href="https://interclimate.org/resources/">Resources</a></li>
<li><a href="https://interclimate.org/contact-us/">Contact Us</a></li>
</ul>					</nav>
</div>
<div class="social-icons">
<a href="https://twitter.com/ClimateVoicesUK" target="_blank"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a><a href="https://www.instagram.com/interclimatenetwork/" target="_blank"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a><a href="https://www.facebook.com/InterClimate-Network-205375216158240/" target="_blank"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>				</div>
</header>
<!-- /header -->
<main role="main" class="main">
<header class="banner hero clear">
<div class="banner_image">
<img src="https://interclimate.org/wp-content/uploads/2019/07/student-at-climate-conference.jpg" class="attachment-large size-large wp-post-image" alt="" /></a>
</div>
<div class="constrained">
<h1>Inspiring the next generation of climate leaders</h1>
</div>
</header>
<div class="intro">
<div class="constrained linemax">
<p>InterClimate Network engages young people with the challenges of climate change, inspires their climate action, and promotes their voices in their own sustainable future.</p>
</div>
</div>
<section class="highlights feature_wrap">
<h2 class="feature_wrap__title">Recent Highlights</h2>
<div class="slick-slideshow">
<div class="feature">
<img src="https://interclimate.org/wp-content/uploads/2019/06/Suatainability.jpg" alt="" class="feature__img" />
<div class="feature__info">
<span class="feature__quotemark"><i class="fa fa-quote-left fa-fw" aria-hidden="true"></i></span>
<div class="feature__quote"> ‘In prioritising profit over Climate issues, is Capitalism itself sustainable?’ </div>
<div class="feature__quoted">- Student, Pates Grammar</div>
<div class="feature__summary"><p>
Schools who attended ICN’s Climate Conferences have now taken part in a Vision21 Climate Emergency Question Time, where they had the opportunity to put local politicians on the spot about the Emergency.																			<a href="https://interclimate.org/events/" class="feature__link">read more</a>
</p></div>
</div>
</div>
<div class="feature">
<img src="https://interclimate.org/wp-content/uploads/2019/07/2015-5.jpg" alt="" class="feature__img" />
<div class="feature__info">
<span class="feature__quotemark"><i class="fa fa-quote-left fa-fw" aria-hidden="true"></i></span>
<div class="feature__quote"> This is a truly historic moment in the fight against climate change, and we are really excited to be able to engage young people with our programmes. </div>
<div class="feature__quoted">- James Streeter, ICN Executive Chairman</div>
<div class="feature__summary"><p>
Chairman’s Autumn Update - Young Climate Voices: Young people have found their voice! There is no doubt now about the awareness and determination of the generation that are set to inherit a warmer climate. 																			<a href="https://interclimate.org/young-climate-voices/" class="feature__link">read more</a>
</p></div>
</div>
</div>
<div class="feature">
<img src="https://interclimate.org/wp-content/uploads/2019/10/The-Green-Burger.jpg" alt="" class="feature__img" />
<div class="feature__info">
<span class="feature__quotemark"><i class="fa fa-quote-left fa-fw" aria-hidden="true"></i></span>
<div class="feature__quote"> The feeling grew amongst the students that there are actions they could take.  RGS are introducing a 'pledge’ time, so we're delighted that students can take up their ideas in that forum.</div>
<div class="feature__quoted">- Richard Usher, Just Ideas</div>
<div class="feature__summary"><p>
Reading Girls’ School Climate Action Summit: ICN was delighted to run a Climate Action Workshop with 87 hugely engaged students from Reading Girls’ School Year 8, and forming part of their wider STEM day programme.																			<a href="https://interclimate.org/events/reading-girls-school-climate-action-workshop/" class="feature__link">read more</a>
</p></div>
</div>
</div>
</div>
<div class="button_wrap"><a href="https://interclimate.org/events" class="button">See all recent events</a></div>
</section>
<section class="editor-content">
<article>
</article>
</section>
<div class="social-prompts">
<div class="prompt-box">
<div class="social-icons">
<a href="https://twitter.com/ClimateVoicesUK"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a><a href="https://www.instagram.com/interclimatenetwork/"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a>				</div>
<p>Follow us to share ideas, hear more about climate change news and our latest events</p>
</div>
</div>
<section class="upcoming-events">
<article>
<header>
<h1>Upcoming Events</h1>
</header>
<div class="upcoming-events__list clear">
<ul>
<li class="event no-thumb">
<span class="event__date">
December 4, 2019											</span>
<a href="https://interclimate.org/events/school-climate-conference-bristol/" title="School Climate Conference, Bristol" class="event__title">School Climate Conference, Bristol</a>
<div class="event__summary"><p>Bristol is the location for the 4th in our sequence of five School Climate Conferences in Nov/Dec 2019. Students from across the area will debate global progress and will be inspired to more climate action by meeting local experts.</p>
</div>
<a class="event__more" href="https://interclimate.org/events/school-climate-conference-bristol/" title="School Climate Conference, Bristol">Read more</a>
</li>
</ul>
<div class="button_wrap"><a href="https://interclimate.org/events" class="events_more button">See all events</a></div>
</div>
</article>
</section>
<section id="instafeed" class="instagram-feed">
</section>
<div class="embed-icon">
<a href="https://www.instagram.com/interclimatenetwork/" target="_blank" class="instafeed-button"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i> Follow us on Instagram</a>		</div>
<section class="post-listings recent-posts">
<article>
<header>
<h1>Recent Stories</h1>
</header>
<div class="post-listings__list clear">
<ul>
<li class="post with-thumb not-sticky">
<a class="post__thumb" href="https://interclimate.org/energy-of-the-youth-driving-change/" title="Energy of the Youth Driving Change"><img src="https://interclimate.org/wp-content/uploads/2019/10/UNSG-Youth-Summit-600x400.jpg" class="attachment-featured size-featured wp-post-image" alt="" /></a>
<span class="post__date">October 17, 2019</span>
<a href="https://interclimate.org/energy-of-the-youth-driving-change/" title="Energy of the Youth Driving Change" class="post__title">Energy of the Youth Driving Change</a>
<div class="post__summary"><p class="post_excerpt">One of the key moments in this year’s Climate calendar was the United Nations Climate Action Summit. The Summit, convened by the UN Secretary General Antonio Guterres, was held in... <a class="view-article" href="https://interclimate.org/energy-of-the-youth-driving-change/">read more</a></p></div>
<!--<a class="button event__more" href="https://interclimate.org/energy-of-the-youth-driving-change/" title="Energy of the Youth Driving Change">Read more</a>-->
</li>
<li class="post with-thumb is-sticky">
<a class="post__thumb" href="https://interclimate.org/young-climate-voices/" title="ICN Chairman&#8217;s Autumn Update: Young Climate Voices"><img src="https://interclimate.org/wp-content/uploads/2019/07/2015-5-600x400.jpg" class="attachment-featured size-featured wp-post-image" alt="" srcset="https://interclimate.org/wp-content/uploads/2019/07/2015-5-600x400.jpg 600w, https://interclimate.org/wp-content/uploads/2019/07/2015-5-300x201.jpg 300w, https://interclimate.org/wp-content/uploads/2019/07/2015-5.jpg 700w" sizes="(max-width: 600px) 100vw, 600px" /></a>
<span class="post__date">October 5, 2019</span>
<a href="https://interclimate.org/young-climate-voices/" title="ICN Chairman&#8217;s Autumn Update: Young Climate Voices" class="post__title">ICN Chairman&#8217;s Autumn Update: Young Climate Voices</a>
<div class="post__summary"><p class="post_excerpt">Young people have found their voice! Whether it is through the passionate speeches of Greta Thunberg or the mass movement of the School Strikes 4 Climate, there is no doubt now about the awareness and determination of the generation that are set to inherit a warmer climate.  </p></div>
<!--<a class="button event__more" href="https://interclimate.org/young-climate-voices/" title="ICN Chairman&#8217;s Autumn Update: Young Climate Voices">Read more</a>-->
</li>
<li class="post with-thumb not-sticky">
<a class="post__thumb" href="https://interclimate.org/shaping-readings-new-climate-strategy/" title="Shaping Reading&#8217;s New Climate Strategy"><img src="https://interclimate.org/wp-content/uploads/2019/07/Strategy.jpg" class="attachment-featured size-featured wp-post-image" alt="" srcset="https://interclimate.org/wp-content/uploads/2019/07/Strategy.jpg 500w, https://interclimate.org/wp-content/uploads/2019/07/Strategy-300x200.jpg 300w" sizes="(max-width: 500px) 100vw, 500px" /></a>
<span class="post__date">July 26, 2019</span>
<a href="https://interclimate.org/shaping-readings-new-climate-strategy/" title="Shaping Reading&#8217;s New Climate Strategy" class="post__title">Shaping Reading&#8217;s New Climate Strategy</a>
<div class="post__summary"><p class="post_excerpt">As a sixth former at Reading School, in June I was lucky enough to be a representative at the first step in forming Reading&#8217;s new Climate Strategy and Action Plan.... <a class="view-article" href="https://interclimate.org/shaping-readings-new-climate-strategy/">read more</a></p></div>
<!--<a class="button event__more" href="https://interclimate.org/shaping-readings-new-climate-strategy/" title="Shaping Reading&#8217;s New Climate Strategy">Read more</a>-->
</li>
</ul>
<div class="button_wrap"><a href="https://interclimate.org/stories" class="button">View more stories</a></div>
</div>
</article>
</section>
<style>
.banner.footer_banner { background: url(https://interclimate.org/wp-content/uploads/2019/06/ICC_MCC_19nov2015_019.jpg) no-repeat top center; background-size: cover; background-position-y: 30%; }
</style>
<div class="banner footer_banner">
<div class="constrained linemax">
<div class="caption">
Brilliant conference.  Really opened the students' minds about the seriousness of climate change.				</div>
</div>
</div>
</main>
			
</div>
<!-- /wrapper -->
<!-- footer -->
<footer class="footer" role="contentinfo">
<div class="social-icons">
<a href="https://twitter.com/ClimateVoicesUK"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a><a href="https://www.instagram.com/interclimatenetwork/"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a><a href="https://www.facebook.com/InterClimate-Network-205375216158240/"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>				</div>
<p class="copyright">
&copy; 2019 Copyright Interclimate Network				</p>
</footer>
<!-- /footer -->
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/interclimate.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://interclimate.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.5'></script>
<script type='text/javascript' src='https://interclimate.org/wp-content/plugins/easy-fancybox/js/jquery.fancybox.min.js?ver=1.3.24'></script>
<script type='text/javascript'>
var fb_timeout, fb_opts={'overlayShow':true,'hideOnOverlayClick':true,'overlayOpacity':0.8,'overlayColor':'#000','showCloseButton':true,'margin':20,'centerOnScroll':false,'enableEscapeButton':true,'autoScale':true };
if(typeof easy_fancybox_handler==='undefined'){
var easy_fancybox_handler=function(){
jQuery('.nofancybox,a.wp-block-file__button,a.pin-it-button,a[href*="pinterest.com/pin/create"],a[href*="facebook.com/share"],a[href*="twitter.com/share"]').addClass('nolightbox');
/* IMG */
var fb_IMG_select='a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpg"]:not(.nolightbox),a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a),area[href*=".jpeg"]:not(.nolightbox),a[href*=".png"]:not(.nolightbox,li.nolightbox>a),area[href*=".png"]:not(.nolightbox),a[href*=".webp"]:not(.nolightbox,li.nolightbox>a),area[href*=".webp"]:not(.nolightbox)';
jQuery(fb_IMG_select).addClass('fancybox image');
var fb_IMG_sections=jQuery('.gallery,.wp-block-gallery,.tiled-gallery,.wp-block-jetpack-tiled-gallery');
fb_IMG_sections.each(function(){jQuery(this).find(fb_IMG_select).attr('rel','gallery-'+fb_IMG_sections.index(this));});
jQuery('a.fancybox,area.fancybox,li.fancybox a').each(function(){jQuery(this).fancybox(jQuery.extend({},fb_opts,{'transitionIn':'elastic','transitionOut':'elastic','opacity':false,'hideOnContentClick':false,'titleShow':true,'titlePosition':'over','titleFromAlt':true,'showNavArrows':true,'enableKeyboardNav':true,'cyclic':false}))});};
jQuery('a.fancybox-close').on('click',function(e){e.preventDefault();jQuery.fancybox.close()});
};
var easy_fancybox_auto=function(){setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);};
jQuery(easy_fancybox_handler);jQuery(document).on('post-load',easy_fancybox_handler);
jQuery(easy_fancybox_auto);
</script>
<script type='text/javascript' src='https://interclimate.org/wp-content/plugins/easy-fancybox/js/jquery.easing.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://interclimate.org/wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min.js?ver=3.1.13'></script>
<script type='text/javascript' src='https://interclimate.org/wp-includes/js/wp-embed.min.js'></script>
</body>
</html><!-- WP Fastest Cache file was created in 0.38602018356323 seconds, on 04-12-19 12:28:51 -->