


<!DOCTYPE html>
<html  lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://dfn.org.uk/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Dignity and Freedom for the poor, the marginalised and the outcastes in South Asia. Sponsor A Child&#039;s Education." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://dfn.org.uk/" />
<link rel="shortlink" href="https://dfn.org.uk/" />
  <title>DFN UK | Dignity and Freedom</title>
  <style>
@import url("https://dfn.org.uk/modules/system/system.base.css?pziequ");
</style>
<style>
@import url("https://dfn.org.uk/sites/all/modules/contrib/date/date_api/date.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/date/date_popup/themes/datepicker.1.7.css?pziequ");
@import url("https://dfn.org.uk/modules/field/theme/field.css?pziequ");
@import url("https://dfn.org.uk/modules/node/node.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/views/css/views.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/ckeditor/css/ckeditor.css?pziequ");
</style>
<style>
@import url("https://dfn.org.uk/sites/all/modules/contrib/ctools/css/ctools.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/panels/css/panels.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/panels/plugins/layouts/flexible/flexible.css?pziequ");
@import url("https://dfn.org.uk/sites/default/files/ctools/css/742fe8c6219d46e5fda78208d5420780.css?pziequ");
@import url("https://dfn.org.uk/sites/default/files/honeypot/honeypot.css?pziequ");
@import url("https://dfn.org.uk/sites/all/modules/contrib/eu_cookie_compliance/css/eu_cookie_compliance.css?pziequ");
</style>
<style>#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#0779bf;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}
</style>
<style>
@import url("https://dfn.org.uk/sites/all/themes/starter/css/style.css?pziequ");
@import url("https://dfn.org.uk/sites/all/themes/starter/css/additional.css?pziequ");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="https://dfn.org.uk/sites/all/modules/contrib/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script src="https://dfn.org.uk/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script src="https://dfn.org.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://dfn.org.uk/misc/drupal.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/eu_cookie_compliance/js/jquery.cookie-1.4.1.min.js?v=1.4.1"></script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/jquery_update/replace/misc/jquery.form.min.js?v=2.69"></script>
<script src="https://dfn.org.uk/misc/ajax.js?v=7.66"></script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/jquery_update/js/jquery_update.js?v=0.0.1"></script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/admin_menu/admin_devel/admin_devel.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/bootstrap/js/misc/_progress.js?v=7.66"></script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/google_analytics/googleanalytics.js?pziequ"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-38730817-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://dfn.org.uk/sites/all/themes/starter/js/fontawesome-all.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/affix.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/alert.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/button.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/carousel.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/collapse.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/dropdown.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/modal.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/tooltip.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/popover.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/scrollspy.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/tab.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/bootstrap/assets/javascripts/bootstrap/transition.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/starter/js/custom.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/bootstrap/js/misc/ajax.js?pziequ"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"starter","theme_token":"uGplayBffem_zSzb64DomvRmE4AurOrmtH7-3B7Nn5o","jquery_version":"1.10","js":{"0":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/contrib\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/contrib\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/themes\/bootstrap\/js\/misc\/_progress.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/starter\/js\/fontawesome-all.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/affix.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/alert.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/button.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/carousel.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/collapse.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/dropdown.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/modal.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/tooltip.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/popover.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/scrollspy.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/tab.js":1,"sites\/all\/themes\/starter\/bootstrap\/assets\/javascripts\/bootstrap\/transition.js":1,"sites\/all\/themes\/starter\/js\/custom.js":1,"sites\/all\/themes\/bootstrap\/js\/misc\/ajax.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"public:\/\/ctools\/css\/742fe8c6219d46e5fda78208d5420780.css":1,"public:\/\/honeypot\/honeypot.css":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/all\/themes\/starter\/css\/style.css":1,"sites\/all\/themes\/starter\/css\/additional.css":1}},"ajax":{"edit-submit":{"callback":"button_callback","wrapper":"signupform","event":"mousedown","keypress":true,"prevent":"click","url":"\/system\/ajax","submit":{"_triggering_element_name":"op","_triggering_element_value":"\u003Ci class=\u0022fas fa-angle-right\u0022\u003E\u003C\/i\u003E"}}},"urlIsAjaxTrusted":{"\/system\/ajax":true,"\/":true},"eu_cookie_compliance":{"popup_enabled":true,"popup_agreed_enabled":false,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking the Accept button, you agree to us doing so.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy tapping the Accept button, you agree to us doing so.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":768,"popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-notice-policy","popup_link_new_window":1,"popup_position":null,"fixed_top_position":true,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":100,"cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":0,"cookie_categories":[],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
  <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0027/1619.js" async="async"></script>
</head>
<body  class="html front not-logged-in no-sidebars page-node page-node- page-node-2 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
     <!-- Side bar buttons -->
<div class="fixed-btn">
  </div>
<header id="navbar" role="banner" class="navbar container-fluid navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="https://dfn.org.uk/sites/all/themes/starter/logo.png" alt="Home" />
        </a>
      
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                                  <ul id="main-menu-level-2-id" class="menu nav navbar-nav"><li class="menu-823 first"><a href="/education">education</a></li>
<li class="menu-824"><a href="/healthcare">healthcare</a></li>
<li class="menu-825"><a href="/economic-empowerment">economic empowerment</a></li>
<li class="menu-826"><a href="/trafficking-prevention">Trafficking Prevention</a></li>
<li class="menu-873"><a href="/contact">Contact</a></li>
<li class="menu-950"><a href="https://secure.artezglobal.com/registrant/DonationPage.aspx?eventid=45016&amp;langpref=en-CA&amp;Referrer=mainmenu" title="Donate to the work of DFN UK (Charity No 1122937)">Donate Now</a></li>
<li class="menu-951"><a href="/uniform-appeal">School Uniform Appeal</a></li>
<li class="menu-1189 last"><a href="/education-sponsorship">Education sponsorship</a></li>
</ul>                                      </nav>
      </div>
      </div>
</header>

<div class="main-container container-fluid">

  <header role="banner" id="page-header">
    
      </header> <!-- /#page-header -->
    
    <section class="col-sm-12 col-push">
                  <a id="main-content"></a>
                                                                <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  
<div class="panelizer-view-mode node node-full node-page node-2">
        <div class="panel-flexible panels-flexible-13 clearfix" >
<div class="panel-flexible-inside panels-flexible-13-inside">
<div class="panels-flexible-row panels-flexible-row-13-1 panels-flexible-row-first clearfix top">
  <div class="inside panels-flexible-row-inside panels-flexible-row-13-1-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-13-top_region panels-flexible-region-first panels-flexible-region-last top-region">
  <div class="inside panels-flexible-region-inside panels-flexible-region-13-top_region-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-hero"  >
  
      
  
  <div class="pane-content">
    <div class="banner home" style="background:url(https://dfn.org.uk/sites/default/files/panels-images/education-page-banner_1.jpg) no-repeat">
  <div class="banner-container">
  <div class="banner-content">
  <p>Dignity and Freedom for the poor, the marginalised and the outcastes in South Asia. </p>
  <p><a href="education-sponsorship" class="hero-button">Sponsor a child's education</a></p>
  </div>
  </div>
  </div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-13-main-row panels-flexible-row-last clearfix">
  <div class="inside panels-flexible-row-inside panels-flexible-row-13-main-row-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-region panels-flexible-region-13-center panels-flexible-region-first panels-flexible-region-last">
  <div class="inside panels-flexible-region-inside panels-flexible-region-13-center-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-what-we-do"  >
  
      
  
  <div class="pane-content">
    
      <div class="whatdo relative">
        <div class="container relative">
          <h2 class="title"><div><span>what</span></div> we do</h2>
          <div class="sub-hd">Our aim is to see the poor, marginalised and outcastes of society transition to living in <strong>Dignity</strong> and <strong>Freedom</strong>. Our programmes are designed, led and staffed by nationals, with a <strong>holistic<strong> approach focusing on four key areas…</div>
          <a class="wd-block equal-height" href="/education" target="_self">
            <i class="icon"><img src="/sites/all/themes/starter/images/education-icon.png" alt="education"></i>
            <h3>Education</h3>
            <div class="hover-box">
              <h4>Education</h4>
              <p>Quality, English-medium, values-based education primarily focusing on children from poor and marginalised communities, including those sometimes known as Dalits and Adivasis.</p>

            </div>
          </a>
          <a class="wd-block equal-height" href="/healthcare" target="_self">
            <i class="icon"><img src="/sites/all/themes/starter/images/health-icon.png" alt="health"></i>
            <h3>Healthcare</h3>
            <div class="hover-box">
              <h4>Healthcare</h4>
              <p>Community healthworkers, primary clinics and HIV/AIDS centres to help prevent sickness and disease – a cause and consequence of poverty and inequality.</p>

            </div>
          </a>
          <a class="wd-block equal-height" href="/economic-empowerment" target="_self">
            <i class="icon"><img src="/sites/all/themes/starter/images/economic-icon.png" alt="economic"></i>
            <h3>Economic<br/>empowerment</h3>
            <div class="hover-box">
              <h4>Economic empowerment</h4>
              <p>Business start-up grants, self-help groups and vocational training to enable the poor to earn a livelihood, become self-sufficient and hold their heads up high.</p>

            </div>
          </a>
          <a class="wd-block equal-height" href="/trafficking-prevention" target="_self">
            <i class="icon"><img src="/sites/all/themes/starter/images/trafficking-icon.png" alt="trafficking"></i>
            <h3>trafficking<br/>prevention</h3>
            <div class="hover-box">
              <h4>trafficking prevention</h4>
              <p>Prevention and awareness programmes among at-risk women and girls and their local community.</p>

            </div>
          </a>
        </div>
      </div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-driver-image-left"  >
  
      
  
  <div class="pane-content">
    
<div class="image-content-block relative">
  <div class="img-block left"><img src="https://dfn.org.uk/sites/default/files/panels-images/IMG_5365.jpg" alt="whatwedo"></div>
  <div class="container vertical-middle">
   <div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-6">
      <div class="content-box pad-left clearfix">
        <h2 class="title right"><div><span>why</span></div> we do what we do</h2>
          <p>Over the last 20 years our movement has responded to&nbsp;requests from major community leaders to provide holistic English-medium education so&nbsp;their communities can find true&nbsp;dignity and freedom in the modern world.</p>

<p>We were asked to serve the poor,&nbsp;marginalised and&nbsp;outcastes of South Asia. We&nbsp;focus on&nbsp;restoring the dignity and freedom of women and children who are the most vulnerable to&nbsp;exploitation and abuse.&nbsp;<strong>This is why we do what we do</strong></p>

      </div>
    </div>
   </div>
  </div>
</div>
	     </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-driver-image-right"  >
  
      
  
  <div class="pane-content">
    
    <div class="image-content-block relative">
      <div class="img-block right"><img src="https://dfn.org.uk/sites/default/files/panels-images/ThinkstockPhotos-592643618_230718.jpg" alt="why"></div>
      <div class="container vertical-middle">
       <div class="row">
        <div class="col-xs-12 col-sm-6">
          <div class="content-box pad-right clearfix">
            <h2 class="title"><div><span>who</span></div> we are</h2>
            <p>DFN UK, a development and empowerment charity, is part of a global network supporting&nbsp;indigenous partners*&nbsp;responding to the requests of community leaders to educate their children and protect their women.</p>

<h6><em>*OMIF (Operation Mercy India Foundation), a humanitarian&nbsp;initiative of the Good Shepherd Church of India.</em></h6>

            <div class="small-logo"><img src="/sites/all/themes/starter/images/dfn-small-logo.png" alt="dfn"></div>
          </div>
        </div>
       </div>
      </div>
    </div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-driver-image-left"  >
  
      
  
  <div class="pane-content">
    
<div class="image-content-block relative">
  <div class="img-block left"><img src="https://dfn.org.uk/sites/default/files/panels-images/Jyothi%20CNS%20copy.JPG" alt="whatwedo"></div>
  <div class="container vertical-middle">
   <div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-6">
      <div class="content-box pad-left clearfix">
        <h2 class="title right"><div><span> Jyothi's </span></div> Story </h2>
          <p>Marks and scars on Jyothi’s* body tell&nbsp;of abuse and exploitation. Tortured, sexually abused and threatened with trafficking into&nbsp;servitude, she&nbsp;came to one of our&nbsp;shelters and began the healing process.&nbsp;Given a place in one of our schools, she was soon&nbsp;excelling. “I want to be a doctor”, she says, “I want to treat patients&nbsp;who need help like I did”.&nbsp;Jyothi graduated with distinction&nbsp;and is now studying sciences at college prior to doctor training.&nbsp;<strong>From destitution to top of the class!&nbsp;</strong></p>

<p><span style="font-size:10px;">*name changed and representative image used</span></p>

      </div>
    </div>
   </div>
  </div>
</div>
	     </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-home-make-it"  >
  
      
  
  <div class="pane-content">
    <div class="make-happen relative">
  <div class="container relative">
    <h2 class="title right white"><div><span>you</span></div>can make it happen</h2>
    <div class="row mrg-t50">
      <div class="col-xs-12 col-sm-6">
        <p>We want to empower the most vulnerable to find freedom from exploitation and restore their dignity.</p>

<p><strong>You can help by making a donation or by sponsoring a child's education.</strong></p>

      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6"><a href="/education-sponsorship
" class="dontation">Sponsor a child's education</a></div>
          <div class="col-xs-12 col-sm-12 col-md-6"><a href="https://secure.artezglobal.com/registrant/DonationPage.aspx?eventid=45016&amp;langpref=en-CA&amp;Referrer=https%3a%2f%2fdfn.thedrummeragency.com%2fsupport-us
" class="dontation" target="_blank">Make a donation</a></div>          
        </div>
      </div>
    </div>
  </div>
</div>
	     </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-node-links link-wrapper"  >
  
      
  
  <div class="pane-content">
      </div>

  
  </div>
  </div>
</div>
  </div>
</div>
</div>
</div>
</div>

</section>
  </div>
    </section>

    
</div>

  <footer>
    <div class="foot-top relative clearfix">
     <div class="container">
     <div class="row">
     <div class="col-xs-12 col-sm-8 col-md-9">
          <div class="region region-footer">
    <section id="block-block-2" class="block block-block clearfix">

      
  <div class="row">
<div class="col-xs-12">
<div class="footlogo clearfix"><img alt="foot-logo" src="/sites/all/themes/starter/images/foot-logo.png" /></div>
</div>

<div class="col-xs-12 col-sm-4">
<div class="address clearfix">
<p>DFN UK<br />
PO Box 3560<br />
Stafford<br />
ST16 9QP</p>

<p><a href="tel:01785 785 068">01785 785 068</a> <a href="/contact">Contact us</a></p>
</div>
</div>
 <div class="col-xs-12 col-sm-8 foot-nav">
<ul>
  <li><a href="/education">education</a></li>
  <li><a href="/support-us">support us</a></li>
  <li><a href="/healthcare">Healthcare</a></li>
  <li><a href="https://secure.artezglobal.com/registrant/startup.aspx?eventid=45016" target="_blank">Donate</a></li>
  <li><a href="/economic-empowerment">economic empowerment</a></li>
  <li><a href="/education-sponsorship" target="_blank">Sponsor</a></li>
  <li><a href="/trafficking-prevention">trafficking prevention</a></li>
  <li><a href="/contact">contact</a></li>
</ul>
</div>
</div>
</section>
  </div>
      </div>
       <div class="col-xs-12 col-sm-4 col-md-3">
        <ul class="social">
         <li><a href="https://www.facebook.com/dfnuk" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
         <li><a href="https://twitter.com/dfnuk" target="_blank"><i class="fab fa-twitter"></i></a></li>
         <li><a href="http://www.youtube.com/user/DFNUKfilm/videos?sort=dd&flow=list&view=0" target="_blank"><i class="fab fa-youtube"></i></a></li>
        </ul>
         <div class="news-field clearfix">
          <label>Subscribe for news</label>



		    <div class="region region-footer-form">
    <section id="block-dfn-panes-signup-form" class="block block-dfn-panes clearfix">

      
  <div id="signupform"><form action="/" method="post" id="dfn-signup-form" accept-charset="UTF-8"><div><div class="field-box"><div class="form-item form-item-email form-type-textfield form-group"><input class="form-control form-control form-text" type="text" id="edit-email" name="email" value="" size="60" maxlength="128" /></div><button class="button btn btn-default form-submit" type="submit" id="edit-submit" name="op" value="&lt;i class=&quot;fas fa-angle-right&quot;&gt;&lt;/i&gt;"><i class="fas fa-angle-right"></i></button>
</div><input type="hidden" name="form_build_id" value="form-zpvpiwuZ7fhiISs4t9cjtD6tnyp7TemhG2sNB9mqan0" />
<input type="hidden" name="form_id" value="dfn_signup_form" />
<input type="hidden" name="honeypot_time" value="1575988666|kK7uDwoeIpy4aTewFHW8Ev2rwl2LT9NUHUjWe3eY7NQ" />
<div class="url-textfield"><div class="form-item form-item-url form-type-textfield form-group"> <label class="control-label" for="edit-url">Leave this field blank</label>
<input autocomplete="off" class="form-control form-text" type="text" id="edit-url" name="url" value="" size="20" maxlength="128" /></div></div></div></form></div>
</section>
  </div>

<!--
						           <input name="email" type="email" placeholder="Enter your email" class="form-control">
			           <button type="submit" class="button"><i class="fas fa-angle-right"></i></button>
-->
			

        </div>
        <div><img src="/sites/all/themes/starter/images/FRFundraisingBadge.png" alt="Registered with fundraising regulator" style="width: 100%; height:auto;"></div>
        <p class="text-right small">Registered Charity 1122937</p>
       </div>
     </div>
     </div>
    </div>
      <div class="region region-footer-bottom">
    <section id="block-block-1" class="block block-block clearfix">

      
  <div class="foot-bottom relative clearfix">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6">
<p>Copyright © 2018 DFN UK</p>
</div>
<div class="col-xs-12 col-sm-6">
<ul><li><a href="/privacy-notice-policy">Privacy Policy</a></li>
<li><a href="/terms-and-conditions">Website Terms</a></li>
</ul></div>
</div>
</div>
</div>

</section>
  </div>
  </footer>
  <script>var eu_cookie_compliance_cookie_name = "";</script>
<script src="https://dfn.org.uk/sites/all/modules/contrib/eu_cookie_compliance/js/eu_cookie_compliance.js?pziequ"></script>
<script src="https://dfn.org.uk/sites/all/themes/bootstrap/js/bootstrap.js?pziequ"></script>
</body>
</html>
