<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Red Pepper Events - Red Pepper Events and Management</title>
	<link rel="shortcut icon" href="/media/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/site/themes/redpepperevents/style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600">
	
<!-- Start The SEO Framework by Sybre Waaijer -->
<meta name="description" content="Red Pepper Events is an event management company based in Dundee. Working throughout Scotland, we manage all types of events from small intimate parties to&#8230;" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Red Pepper Events" />
<meta property="og:description" content="Red Pepper Events is an event management company based in Dundee. Working throughout Scotland, we manage all types of events from small intimate parties to large corporate functions. We also offer a&#8230;" />
<meta property="og:url" content="https://redpepperevents.com/" />
<meta property="og:site_name" content="Red Pepper Events" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Red Pepper Events" />
<meta name="twitter:description" content="Red Pepper Events is an event management company based in Dundee. Working throughout Scotland, we manage all types of events from small intimate parties to large corporate functions. We also offer a&#8230;" />
<link rel="canonical" href="https://redpepperevents.com/" />
<script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","url":"https://redpepperevents.com/","name":"Red Pepper Events","potentialAction":{"@type":"SearchAction","target":"https://redpepperevents.com/search/{search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type="application/ld+json">{"@context":"https://schema.org","@type":"Organization","url":"https://redpepperevents.com/","name":"Red Pepper Events"}</script>
<!-- End The SEO Framework by Sybre Waaijer | 0.00970s -->

<link rel='dns-prefetch' href='//ajax.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Red Pepper Events &raquo; Welcome Comments Feed" href="https://redpepperevents.com/sample-page/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/redpepperevents.com\/system\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://redpepperevents.com/system/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='https://redpepperevents.com/site/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.5.7' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='https://redpepperevents.com/site/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.5.7' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='https://redpepperevents.com/site/plugins/woocommerce/assets/css/woocommerce.css?ver=3.5.7' type='text/css' media='all' />
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js?ver=5.1.1'></script>
<link rel='https://api.w.org/' href='https://redpepperevents.com/wp-json/' />
<link rel="alternate" type="application/json+oembed" href="https://redpepperevents.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fredpepperevents.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://redpepperevents.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fredpepperevents.com%2F&#038;format=xml" />
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136808759-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-136808759-1');
	</script>
</head>

<body class="home page-template page-template-page-home page-template-page-home-php page page-id-2 woocommerce-no-js">

<input type="checkbox" id="burger-box"><label for="burger-box" id="burger"><div class="bar b1"></div><div class="bar b2"></div><div class="bar b3"></div>Menu</label>

<a href="https://booking.redpepperevents.com/" id="rp-hotels" title="Book your hotel today" target="_blank">Hotel Booking<svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 12.8"><path d="M4 4.8h1.6v1.6H4V4.8zM6.4 4.8H8v1.6H6.4V4.8zM8.8 4.8h1.6v1.6H8.8V4.8zM1.6 9.6h1.6v1.6H1.6V9.6zM4 9.6h1.6v1.6H4V9.6zM6.4 9.6H8v1.6H6.4V9.6zM4 7.2h1.6v1.6H4V7.2zM6.4 7.2H8v1.6H6.4V7.2zM8.8 7.2h1.6v1.6H8.8V7.2zM1.6 7.2h1.6v1.6H1.6V7.2zM10.4 0v0.8H8.8V0H3.2v0.8H1.6V0H0v12.8h12V0H10.4zM11.2 12H0.8V3.2h10.4V12z" fill="#FFF"/></svg></a>

<div id="sidebar">
    <div id="sidebarwrap">
        <a href="https://redpepperevents.com" title="Red Pepper Events and Catering Hire" id="logo">
            <svg xmlns="http://www.w3.org/2000/svg" width="160" height="58" viewBox="0 0 160 58.1"><style>.a{fill:#FFF;}</style><path d="M155.6 16.5c-3.1-3-7.5-4.7-12-4.5l-75.9 0c0 0-3.1 0.1-5.8-1 -1.7-0.8-2.4-1.8-2.9-2.9 -0.3-0.9-0.5-2.9 0-3.9 0 0.1 0.1 0.2 0.1 0.3 0.7 1.5 2.1 2 3.6 1.3 1.4-0.6 2-2.3 1.3-3.9 -0.8-1.7-2.8-2.4-4.8-1.4C56.1 1.7 54.8 5 56.3 8.4c0 0 1.5 4.1 8.9 4.7 1.3 0.2 2.3 0.1 2.5 0.1l75.9 0c2.1-0.1 4.2 0.3 6.1 1 3.9 2.4 6.5 7.1 6.5 12.6 0 5.5-2.6 10.2-6.6 12.7 -1.8 0.7-3.9 1.1-6 1.1H16.4c-2.2 0-4.3-0.4-6.2-1.2C6.4 37 3.8 32.3 3.8 26.8c0-5.4 2.6-10 6.3-12.5 2-0.8 4.1-1.2 6.3-1.1h31c1.1 0.1 1.4 0.4 1.4 1.6v6.5c-1.1-1.8-2.6-2.6-4.6-2.6 -3.5 0-5.9 2.8-5.9 7 0 4.2 2.5 6.9 5.9 6.9 2 0 3.6-0.9 4.7-2.7v2.3h4.8v-1.3h-0.7c-1.5 0-1.8-0.3-1.8-1.6l0-15.7c0-1.3-0.3-1.6-1.8-1.5h-3.2v0l-29.7 0c-4.5-0.1-8.9 1.5-12 4.5C1.6 19.3 0 23 0 26.8c0 8.3 7.4 15.1 16.4 15.1h127.1c9.1 0 16.4-6.8 16.4-15.1C160 23 158.4 19.3 155.6 16.5zM44.7 30.9c-2.3 0-3.7-1.9-3.7-5.3 0-3.4 1.4-5.3 3.7-5.3 2.5 0 4.2 2 4.2 5.3C48.9 28.9 47.2 30.9 44.7 30.9z" class="a"/><path d="M19.1 22.2c0 0.8 0.7 1.4 1.5 1.4 1 0 1.7-0.8 1.7-1.9 0-1.6-1.2-2.9-3.1-2.9 -1.7 0-3 1.1-4 3.1v-2.7H10.3v1.3h0.8c1.5 0 1.8 0.3 1.8 1.6v7.2c0 1.3-0.3 1.6-1.8 1.6H10.3v1.4h8v-1.3h-1.3c-1.5 0-1.8-0.3-1.8-1.6v-3.1c0-3.8 1.8-6.1 3.9-6.1 0.6 0 1.1 0.2 1.5 0.8 -0.1 0-0.1 0-0.2 0C19.6 20.8 19.1 21.4 19.1 22.2z" class="a"/><path d="M36.4 25.8c0-4.7-2.3-7.2-6.3-7.2 -3.9 0-6.5 2.8-6.5 7 0 4.3 2.7 7 6.6 7 3.3 0 5.3-1.6 5.9-4.8H34.4c-0.6 2.1-2 3.2-4.1 3.2 -2.6 0-4-1.7-4-5 0-0.1 0-0.1 0-0.2H36.4zM30.1 20c2.5 0 3.6 1.4 3.7 4.4h-7.4C26.5 21.6 27.8 20 30.1 20z" class="a"/><path d="M58.4 36.7h-0.6v1.3h7.4v-1.3h-0.9c-1.5 0-1.8-0.3-1.8-1.6v-5.2c1.1 1.8 2.6 2.6 4.6 2.6 3.5 0 5.9-2.7 5.9-7 0-4.2-2.4-6.9-5.9-6.9 -2 0-3.5 0.9-4.7 2.6v-2.2h-4.7v1.3h0.6c1.5 0 1.8 0.3 1.8 1.6v13.1C60.1 36.4 59.8 36.7 58.4 36.7zM66.7 20.3c2.3 0 3.7 1.9 3.7 5.3 0 3.4-1.4 5.3-3.7 5.3 -2.5 0-4.2-2-4.2-5.3C62.5 22.3 64.1 20.3 66.7 20.3z" class="a"/><path d="M88 25.8c0-4.7-2.3-7.2-6.3-7.2 -3.9 0-6.5 2.8-6.5 7 0 4.3 2.7 7 6.6 7 3.3 0 5.3-1.6 5.9-4.8h-1.6c-0.6 2.1-2 3.2-4.1 3.2 -2.6 0-4-1.7-4-5 0-0.1 0-0.1 0-0.2H88zM81.6 20c2.5 0 3.6 1.4 3.7 4.4h-7.4C78 21.6 79.4 20 81.6 20z" class="a"/><path d="M89.7 36.7h-0.6v1.3h7.4v-1.3h-0.9c-1.5 0-1.8-0.3-1.8-1.6v-5.2c1.1 1.8 2.6 2.6 4.6 2.6 3.5 0 5.9-2.7 5.9-7 0-4.2-2.4-6.9-5.9-6.9 -2 0-3.5 0.9-4.7 2.6v-2.2h-4.7v1.3h0.6c1.5 0 1.8 0.3 1.8 1.6v13.1C91.5 36.4 91.2 36.7 89.7 36.7zM98 20.3c2.3 0 3.7 1.9 3.7 5.3 0 3.4-1.4 5.3-3.7 5.3 -2.5 0-4.1-2-4.1-5.3C93.8 22.3 95.4 20.3 98 20.3z" class="a"/><path d="M106.4 36.7h-0.6v1.3h7.4v-1.3h-0.9c-1.5 0-1.8-0.3-1.8-1.6v-5.2c1.1 1.8 2.6 2.6 4.6 2.6 3.5 0 5.9-2.7 5.9-7 0-4.2-2.4-6.9-5.9-6.9 -2 0-3.5 0.9-4.7 2.6v-2.2h-4.7v1.3h0.6c1.5 0 1.8 0.3 1.8 1.6v13.1C108.2 36.4 107.9 36.7 106.4 36.7zM114.7 20.3c2.3 0 3.7 1.9 3.7 5.3 0 3.4-1.4 5.3-3.7 5.3 -2.5 0-4.1-2-4.1-5.3C110.6 22.3 112.2 20.3 114.7 20.3z" class="a"/><path d="M136 25.8c0-4.7-2.3-7.2-6.3-7.2 -3.9 0-6.5 2.8-6.5 7 0 4.3 2.7 7 6.6 7 3.3 0 5.3-1.6 5.9-4.8h-1.6c-0.6 2.1-2 3.2-4.1 3.2 -2.6 0-4-1.7-4-5 0-0.1 0-0.1 0-0.2H136zM129.6 20c2.5 0 3.6 1.4 3.7 4.4h-7.4C126.1 21.6 127.4 20 129.6 20z" class="a"/><path d="M138.5 30.8h-0.8v1.4h8v-1.3h-1.3c-1.5 0-1.8-0.3-1.8-1.6v-3.1c0-3.8 1.8-6.1 3.9-6.1 0.6 0 1.1 0.2 1.5 0.8 0 0-0.1 0-0.2 0 -0.8 0-1.3 0.6-1.3 1.4 0 0.8 0.7 1.4 1.5 1.4 1 0 1.7-0.8 1.7-1.9 0-1.6-1.2-2.9-3.1-2.9 -1.7 0-3 1.1-4 3.1v-2.7h-4.9v1.3h0.8c1.5 0 1.8 0.3 1.8 1.6v7.2C140.2 30.5 139.9 30.8 138.5 30.8z" class="a"/><path d="M13.8 50.6c-1.9 0-3.1 1.6-3.1 3.8 0 2.2 1.2 3.6 3.2 3.6 1 0 1.7-0.2 2.1-0.4l-0.2-0.9c-0.4 0.2-0.9 0.3-1.7 0.3 -1.1 0-2.2-0.6-2.2-2.5h4.5c0-0.1 0-0.3 0-0.6C16.5 52.5 15.9 50.6 13.8 50.6zM12 53.6c0.1-0.9 0.6-2.1 1.7-2.1 1.2 0 1.6 1.1 1.6 2.1H12z" class="a"/><path d="M22.7 54.8c-0.2 0.6-0.4 1.2-0.5 1.8h0c-0.1-0.6-0.3-1.2-0.5-1.8l-1.2-4h-1.3l2.5 7.1h1.2l2.5-7.1h-1.3L22.7 54.8z" class="a"/><path d="M30.7 50.6c-1.9 0-3.1 1.6-3.1 3.8 0 2.2 1.2 3.6 3.2 3.6 1 0 1.7-0.2 2.1-0.4l-0.2-0.9c-0.4 0.2-0.9 0.3-1.7 0.3 -1.1 0-2.2-0.6-2.2-2.5h4.5c0-0.1 0-0.3 0-0.6C33.4 52.5 32.8 50.6 30.7 50.6zM28.9 53.6c0.1-0.9 0.6-2.1 1.7-2.1 1.2 0 1.6 1.1 1.6 2.1H28.9z" class="a"/><path d="M40.1 50.6c-1 0-1.8 0.6-2.1 1.2H37.9l-0.1-1.1h-1.1c0 0.6 0.1 1.1 0.1 1.9v5.2h1.2v-4.3c0-0.2 0-0.4 0.1-0.6 0.2-0.7 0.8-1.3 1.5-1.3 1.1 0 1.5 0.9 1.5 2v4.3h1.2V53.5C42.3 51.3 41.1 50.6 40.1 50.6z" class="a"/><path d="M47.7 48.8l-1.2 0.3v1.7h-1v0.9h1v4c0 0.9 0.1 1.5 0.5 1.8 0.3 0.3 0.8 0.5 1.3 0.5 0.5 0 0.8-0.1 1.1-0.2l-0.1-0.9c-0.2 0.1-0.4 0.1-0.7 0.1 -0.6 0-0.9-0.4-0.9-1.3v-3.9h1.7v-0.9h-1.7V48.8z" class="a"/><path d="M54.7 53.8c-0.9-0.4-1.2-0.7-1.2-1.2 0-0.5 0.4-1 1.1-1 0.6 0 1 0.2 1.3 0.4l0.3-0.9c-0.4-0.2-0.9-0.4-1.6-0.4 -1.4 0-2.3 0.9-2.3 2.1 0 0.8 0.6 1.5 1.8 2 0.9 0.4 1.2 0.7 1.2 1.3 0 0.6-0.4 1.1-1.3 1.1 -0.6 0-1.2-0.2-1.5-0.5l-0.3 0.9c0.4 0.3 1.1 0.5 1.8 0.5 1.5 0 2.5-0.8 2.5-2.1C56.4 54.9 55.8 54.2 54.7 53.8z" class="a"/><path d="M71.8 52.5h-1.1c-0.1 1.1-0.4 2.1-0.8 2.9 -0.7-0.8-1.6-1.9-2.3-2.8l0 0c1.5-0.8 2-1.6 2-2.6 0-1.3-0.9-2.1-2.1-2.1 -1.5 0-2.4 1.2-2.4 2.4 0 0.7 0.3 1.5 0.8 2.1l0 0c-1.1 0.7-1.8 1.6-1.8 2.9 0 1.5 1 2.8 2.9 2.8 1 0 1.9-0.3 2.7-1.1 0.4 0.4 0.6 0.7 0.9 1h1.5c-0.6-0.7-1.1-1.3-1.5-1.7C71.2 55.3 71.6 54 71.8 52.5zM67.4 48.7c0.8 0 1.1 0.7 1.1 1.3 0 0.8-0.6 1.4-1.5 2 -0.4-0.5-0.7-1.1-0.7-1.8C66.3 49.4 66.7 48.7 67.4 48.7zM67.2 57.1c-1.1 0-1.9-0.9-1.9-2 0-1 0.7-1.6 1.2-2 0.9 1.2 1.9 2.3 2.6 3.1C68.6 56.7 68 57.1 67.2 57.1z" class="a"/><path d="M82.8 51.7c0.6 0 1 0.2 1.3 0.3l0.3-1c-0.3-0.2-0.9-0.3-1.6-0.3 -2.2 0-3.5 1.6-3.5 3.8 0 2.2 1.3 3.6 3.3 3.6 0.8 0 1.5-0.2 1.8-0.4l-0.2-0.9c-0.3 0.2-0.8 0.3-1.4 0.3 -1.3 0-2.2-1-2.2-2.7C80.6 52.9 81.4 51.7 82.8 51.7z" class="a"/><path d="M92.3 53.4c0-1.5-0.5-2.8-2.5-2.8 -0.8 0-1.6 0.3-2.1 0.6l0.3 0.8c0.5-0.3 1.1-0.5 1.7-0.5 1.3 0 1.5 0.9 1.5 1.6v0.2c-2.4 0-3.9 0.9-3.9 2.6 0 1.1 0.7 2 2 2 0.9 0 1.6-0.4 2-1h0l0.1 0.9h1.1c-0.1-0.5-0.1-1.1-0.1-1.7V53.4zM91.1 55.6c0 0.1 0 0.3 0 0.4 -0.2 0.6-0.7 1.2-1.5 1.2 -0.6 0-1.1-0.4-1.1-1.2 0-1.4 1.5-1.6 2.7-1.6V55.6z" class="a"/><path d="M97.6 48.8l-1.2 0.3v1.7h-1v0.9h1v4c0 0.9 0.1 1.5 0.5 1.8 0.3 0.3 0.8 0.5 1.3 0.5 0.5 0 0.8-0.1 1.1-0.2l-0.1-0.9c-0.2 0.1-0.4 0.1-0.7 0.1 -0.6 0-0.9-0.4-0.9-1.3v-3.9h1.7v-0.9h-1.7V48.8z" class="a"/><path d="M105 50.6c-1.9 0-3.1 1.6-3.1 3.8 0 2.2 1.2 3.6 3.2 3.6 1 0 1.7-0.2 2.1-0.4l-0.2-0.9c-0.4 0.2-0.9 0.3-1.7 0.3 -1.1 0-2.2-0.6-2.2-2.5h4.5c0-0.1 0-0.3 0-0.6C107.7 52.5 107.1 50.6 105 50.6zM103.2 53.6c0.1-0.9 0.6-2.1 1.7-2.1 1.2 0 1.6 1.1 1.6 2.1H103.2z" class="a"/><path d="M112.2 52.1h0l0-1.3h-1.1c0 0.6 0.1 1.3 0.1 2.1v5h1.2v-3.8c0-0.2 0-0.4 0.1-0.6 0.2-0.9 0.8-1.6 1.6-1.6 0.2 0 0.3 0 0.4 0v-1.2c-0.1 0-0.2 0-0.3 0C113.2 50.6 112.5 51.2 112.2 52.1z" class="a"/><path d="M125.6 50.6c-0.5 0-0.9 0.1-1.2 0.3 -0.3 0.2-0.6 0.5-0.8 0.8h0v-4.3h-1.2v10.4h1.2v-4.4c0-0.2 0-0.4 0.1-0.6 0.2-0.7 0.8-1.2 1.6-1.2 1.1 0 1.4 0.9 1.4 2v4.3h1.2v-4.4C127.8 51.3 126.5 50.6 125.6 50.6z" class="a"/><path d="M132.3 48c-0.4 0-0.8 0.3-0.8 0.8 0 0.4 0.3 0.8 0.7 0.8 0.5 0 0.8-0.3 0.8-0.8C133 48.4 132.7 48 132.3 48z" class="a"/><rect x="131.7" y="50.8" width="1.2" height="7.1" class="a"/><path d="M137.9 52.1h0l0-1.3h-1.1c0 0.6 0.1 1.3 0.1 2.1v5h1.2v-3.8c0-0.2 0-0.4 0.1-0.6 0.2-0.9 0.8-1.6 1.6-1.6 0.2 0 0.3 0 0.4 0v-1.2c-0.1 0-0.2 0-0.3 0C139 50.6 138.2 51.2 137.9 52.1z" class="a"/><path d="M145.6 50.6c-1.9 0-3.1 1.6-3.1 3.8 0 2.2 1.2 3.6 3.2 3.6 1 0 1.7-0.2 2.1-0.4l-0.2-0.9c-0.4 0.2-0.9 0.3-1.7 0.3 -1.1 0-2.2-0.6-2.2-2.5h4.5c0-0.1 0-0.3 0-0.6C148.3 52.5 147.7 50.6 145.6 50.6zM143.8 53.6c0.1-0.9 0.6-2.1 1.7-2.1 1.2 0 1.6 1.1 1.6 2.1H143.8z" class="a"/></svg>
        </a>

        
        <nav class="menu-main-menu-container"><ul id="menu-main-menu" class="menu"><li id="menu-item-314" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-314"><a href="https://redpepperevents.com/" aria-current="page">Welcome</a></li>
<li id="menu-item-315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-315"><a href="https://redpepperevents.com/about/">About Us</a></li>
<li id="menu-item-319" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-319"><a href="https://redpepperevents.com/event-management/">Event Management</a></li>
<li id="menu-item-336" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-336"><a href="https://redpepperevents.com/hire-catalogue/">Hire Catalogue</a></li>
<li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358"><a href="https://redpepperevents.com/quote-summary/">Quote Summary</a></li>
<li id="menu-item-318" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-318"><a href="https://redpepperevents.com/news/">News</a></li>
<li id="menu-item-322" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-322"><a href="https://redpepperevents.com/gallery/">Gallery</a></li>
<li id="menu-item-323" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323"><a href="https://redpepperevents.com/contact/">Contact Us</a></li>
</ul></nav>
        <div id="profiles">
            <div id="profileswrap">
                <p>Office Number: <strong>01382 660 394</strong></p>
                <p>24/7 Number: <strong>07921 311 743</strong></p>
                <p>
                    <a href="https://www.facebook.com/RedPepperEventsScotland/" target="_blank" title="Follow us on Facebook">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" class="social"><path d="M30 3.2v23.6c0 1.8-1.4 3.2-3.2 3.2h-5.7V18.1h4.1l0.6-4.5h-4.6v-2.9c0-1.3 0.4-2.2 2.2-2.2h2.4V4.5c-0.4 0-1.8-0.2-3.5-0.2 -3.5 0-5.8 2.1-5.8 6v3.3h-4.1v4.5h4.1V30H3.2C1.4 30 0 28.6 0 26.8V3.2C0 1.4 1.4 0 3.2 0h23.6C28.6 0 30 1.4 30 3.2z" fill="#FFF"/></svg>
                    </a>
                    <a href="https://pinterest.com/redpepperinvite/" target="_blank" title="Follow us on Pinterest">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" class="social"><path d="M30 3.2v23.6c0 1.8-1.4 3.2-3.2 3.2H10.3c0.7-1.1 1.5-2.7 1.8-4 0.2-0.8 1-3.9 1-3.9 0.5 1 2.1 1.9 3.8 1.9 5 0 8.5-4.6 8.5-10.2 0-5.4-4.4-9.5-10.1-9.5 -7.1 0-10.9 4.8-10.9 10 0 2.4 1.3 5.4 3.3 6.4 0.3 0.2 0.5 0.1 0.6-0.2 0.1-0.2 0.3-1.3 0.5-1.9 0-0.2 0-0.3-0.1-0.5C8.1 17.2 7.5 15.7 7.5 14.3c0-3.6 2.8-7.1 7.4-7.1 4 0 6.9 2.8 6.9 6.7 0 4.5-2.2 7.5-5.2 7.5 -1.6 0-2.8-1.3-2.4-3 0.5-2 1.4-4.1 1.4-5.5 0-3.5-5.1-3.1-5.1 1.7 0 1.5 0.5 2.4 0.5 2.4 -2.1 8.9-2.4 9-2 12.9L9.2 30H3.2C1.4 30 0 28.6 0 26.8V3.2C0 1.4 1.4 0 3.2 0h23.6C28.6 0 30 1.4 30 3.2z" fill="#FFF"/></svg>
                    </a>
                    <a href="https://twitter.com/RedPepperEvents" target="_blank" title="Follow us on Twitter">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" class="social"><path d="M26.8 0H3.2C1.4 0 0 1.4 0 3.2v23.6C0 28.6 1.4 30 3.2 30h23.6c1.8 0 3.2-1.4 3.2-3.2V3.2C30 1.4 28.6 0 26.8 0zM23.5 10.6c0 0.2 0 0.4 0 0.6 0 5.8-4.4 12.5-12.5 12.5 -2.5 0-4.8-0.7-6.7-2 0.4 0 0.7 0.1 1.1 0.1 2.1 0 3.9-0.7 5.5-1.9 -1.9 0-3.5-1.3-4.1-3 0.7 0.1 1.3 0.1 2-0.1 -2-0.4-3.5-2.2-3.5-4.3v0c0.6 0.3 1.3 0.5 2 0.6 -1.2-0.8-2-2.2-2-3.7 0-0.8 0.2-1.6 0.6-2.2 2.2 2.7 5.4 4.4 9.1 4.6 -0.6-3 1.6-5.4 4.3-5.4 1.3 0 2.4 0.5 3.2 1.4 1-0.2 1.9-0.6 2.8-1.1 -0.3 1-1 1.9-1.9 2.4 0.9-0.1 1.7-0.3 2.5-0.7C25.1 9.2 24.4 10 23.5 10.6z" fill="#FFF"/></svg>
                    </a>
                </p>
            </div>
        </div>

    </div>
</div>
<div id="display">
<div id="rpHome">
	
	
        <div id="story">
            <p>We are a <a href="/about/meet-the-team/">dedicated team</a> who specialise in creating <a href="/event-management/">spectacular events</a>.</p>
        </div>
        
        <a href="https://booking.redpepperevents.com/" class="rpBox rpBox220">
            <div class="rpBoxData">
                <h2>Hotel Booking (NEW)</h2>
                <p>You can now organise hotel accomodation for your delegates and guests.</p>
            </div>
            <img src="media/boxes/hotels.jpg" alt="book hotel" style="z-index:10; position:absolute; top:0; left:0;" />
        </a>
        <a href="/event-management/" class="rpBox rpBox220">
            <div class="rpBoxData">
                <h2>Event Management</h2>
                <p>Our event management is second to none, ensuring your event is perfect.</p>
            </div>
            <img src="media/boxes/eventmanagement.jpg" alt="event management" class="rpBoxImage" />
        </a>
        <a href="/hire-catalogue/" class="rpBox rpBox220 last">
            <div class="rpBoxData">
                <h2>Hire Catalogue</h2>
                <p>We provide event hire services for you to run and manage your own events.</p>
            </div>
            <img src="media/boxes/hirecatalogue.jpg" alt="catering and event hire catalogue" class="rpBoxImage" />
        </a>

        <div id="home_intro" class="rpBox rpBox460">
            <p>Red Pepper Events is an event management company based in Dundee. Working throughout Scotland, we manage all types of events from small intimate parties to large corporate functions.</p>
<p>We also offer a wide range of hire items from crockery and glass wear to linen and marquees. Whatever the occasion, our experienced team will make sure your event is an event to remember.</p>
        </div>

        <div id="mobBox" class="rpBox rpBox220 last">
            <img src="media/boxes/mob.jpg" alt="catering hire" class="rpBoxImage" />
        </div>

        <a href="/about/meet-the-team/" class="rpBox rpBox220 last">
            <div class="rpBoxData">
                <h2>Eleanor Whitby</h2>
                <p>Meet Eleanor and the team!</p>
            </div>
            <img src="media/team/eleanor.jpg" alt="Eleanor" class="rpBoxImage"/>
        </a>

        <a href="/about/meet-the-team/" class="rpBox rpBox220">
            <div class="rpBoxData">
                <h2>Ed Murdoch</h2>
                <p>Meet Ed and the team!</p>
            </div>
            <img src="media/team/ed.jpg" alt="Ed" class="rpBoxImage" />
        </a>

        <div class="rpBox rpBox220">
            <img src="media/boxes/hsbc-1.jpg" alt="" class="rpBoxImage">
        </div>
        <div class="rpBox rpBox220 last">
            <img src="media/boxes/giveaf.jpg" alt="" class="rpBoxImage">
        </div>

        <div class="rpBox rpBox220">
            <img src="media/boxes/fugare1.jpg" alt="formula 1 hospitality" class="rpBoxImage" />
        </div>

        <a href="/about/meet-the-team/" class="rpBox rpBox220">
            <div class="rpBoxData">
                <h2>Meet The Team</h2>
                <p>The Red Pepper team are friendly and fantastic at what they do.</p>
            </div>
            <img src="media/boxes/meettheteam.jpg" alt="meet the team" class="rpBoxImage"/>
        </a>

        <div class="rpBox rpBox220 last">
            <img src="media/boxes/seating.jpg" alt="weddings" class="rpBoxImage" />
        </div>

        <div class="rpBox rpBox220">
            <img src="media/boxes/invite.jpg" alt="event hire and management" class="rpBoxImage" />
        </div>
        <div class="rpBox rpBox220">
            <img src="media/boxes/hsbc-1.jpg" alt="" class="rpBoxImage">
        </div>
        

</div>


    
</div> <!-- /display -->

	<div id="footer">
    	<h4>Who we work with</h4>
        <div id="clients">
            <img src="/media/clients/hologic.png" alt="Hologic">
            <img src="/media/clients/solar.png" alt="Solar">
            <img src="/media/clients/tpas.png" alt="TPAS">
            <img src="/media/clients/ukela.png" alt="Ukela">
			<img src="/site/uploads/2019/06/logo-fugare1.png" alt="Fugare 1" width="100" height="100">
			<img src="/site/uploads/2019/06/logo-dundee-farmer-market.png" alt="Dundee Farmers Market" width="100" height="100">
			<img src="/site/uploads/2019/06/logo-cj-lang.jpg" alt="CJ Lang" width="160" height="100">
			<img src="/site/uploads/2019/06/logo-world-host.jpg" alt="World Host Award" width="140" height="100">
        </div>        
        
        <div id="toes">
        	<div id="copyright">&copy; <a href="https://redpepperevents.com">Red Pepper Events &amp; Productions</a>. 2019</div>
            <div id="credit"><a href="https://zenelements.com">Website design by zenelements</a></div>
        </div>
    </div>





</body>
</html>
