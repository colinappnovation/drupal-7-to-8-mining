<!DOCTYPE html>
<html lang="en" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.lyrica.co.uk/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Lyrica is an all-female ensemble of professional singers providing music for weddings, functions and parties. We provide a stylish soundtrack for all your events, singing all styles from Vintage, to Opera, Traditional, Gospel and Jazz." />
<meta name="abstract" content="Lyrica is an all-female ensemble of professional singers providing music for weddings, functions and parties. We provide a stylish soundtrack for all your events, singing all styles from Vintage, to Opera, Traditional, Gospel and Jazz." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.lyrica.co.uk/" />
<link rel="shortlink" href="https://www.lyrica.co.uk/" />
<meta http-equiv="content-language" content="en" />
<title>Lyrica | Professional singers for your wedding or occasion</title>
<style type="text/css" media="all">
@import url("https://www.lyrica.co.uk/modules/system/system.base.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/system/system.menus.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/system/system.messages.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/system/system.theme.css?pckmyx");
</style>
<style type="text/css" media="all">
@import url("https://www.lyrica.co.uk/modules/comment/comment.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/date/date_api/date.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/field/theme/field.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/node/node.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/search/search.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/ubercart/uc_order/uc_order.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/ubercart/uc_product/uc_product.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/ubercart/uc_store/uc_store.css?pckmyx");
@import url("https://www.lyrica.co.uk/modules/user/user.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/views/css/views.css?pckmyx");
</style>
<style type="text/css" media="all">
@import url("https://www.lyrica.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/ctools/css/ctools.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/modules/footermap/footermap.css?pckmyx");
</style>
<style type="text/css" media="all">
@import url("https://www.lyrica.co.uk/sites/all/themes/business/style.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/default/files/color/business-6e95c7a0/colors.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/themes/business/css/music-and-more.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/themes/business/css/mini-music.css?pckmyx");
@import url("https://www.lyrica.co.uk/sites/all/themes/business/css/what-we-do.css?pckmyx");
</style>
<script type="text/javascript" src="https://www.lyrica.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/misc/drupal.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/themes/business/js/sliding_effect.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/libraries/colorbox/jquery.colorbox-min.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/modules/colorbox/js/colorbox.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/modules/colorbox/js/colorbox_load.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/modules/colorbox/js/colorbox_inline.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/modules/google_analytics/googleanalytics.js?pckmyx"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-56766033-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/themes/business/js/superfish.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/themes/business/js/supersubs.js?pckmyx"></script>
<script type="text/javascript" src="https://www.lyrica.co.uk/sites/all/themes/business/js/sfmenu.js?pckmyx"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"business","theme_token":"ciC_NdUqJ3hJYpAIyc1clPF5coQRRUmseuZKxx6M2TE","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/business\/js\/sliding_effect.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/business\/js\/superfish.js":1,"sites\/all\/themes\/business\/js\/supersubs.js":1,"sites\/all\/themes\/business\/js\/sfmenu.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"sites\/all\/modules\/ubercart\/uc_order\/uc_order.css":1,"sites\/all\/modules\/ubercart\/uc_product\/uc_product.css":1,"sites\/all\/modules\/ubercart\/uc_store\/uc_store.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/footermap\/footermap.css":1,"sites\/all\/themes\/business\/style.css":1,"sites\/all\/themes\/business\/color\/colors.css":1,"sites\/all\/themes\/business\/css\/music-and-more.css":1,"sites\/all\/themes\/business\/css\/mini-music.css":1,"sites\/all\/themes\/business\/css\/what-we-do.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="/sites/all/themes/business/js/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
    
<div id="wrap">

  <header id="header" class="clearfix" role="banner">

    <div>
             <div id="logo">
        <a href="/" title="Home"><img src="https://www.lyrica.co.uk/sites/default/files/logo.png" alt="Home" /></a>
        </div>
            <hgroup id="sitename">
        <h2><a href="/" title="Home">Lyrica</a></h2>
        <p>Professional singers for your wedding or occasion</p><!--site slogan-->
      </hgroup>
    </div>
    <nav id="navigation" class="clearfix" role="navigation">
      <div id="main-menu">
        <ul class="menu"><li class="first leaf active-trail"><a href="/" title="Lyrica - Singers for your wedding or occasion" class="active-trail active">Home</a></li>
<li class="expanded"><a href="/what-we-do" title="Lyrica Singers for your Function, Wedding, Reception or other event.">What We Do</a><ul class="menu"><li class="first leaf"><a href="/what-we-do/wedding-choirs" title="Wedding Choirs">Wedding Choirs</a></li>
<li class="leaf"><a href="/what-we-do/wedding-soloists" title="Wedding Soloists">Wedding Soloists</a></li>
<li class="leaf"><a href="/what-we-do/wedding-reception-music" title="Popular songs for your reception">Wedding Receptions</a></li>
<li class="last leaf"><a href="/what-we-do/client-testimonials" title="Testimonials from Lyrica clients">Client Testimonials</a></li>
</ul></li>
<li class="expanded"><a href="/media" title="Music, CDs, Videos &amp; Photos of Lyrica Singers">Music &amp; More</a><ul class="menu"><li class="first leaf"><a href="/media/music" title="Hear Lyrica">Hear Lyrica</a></li>
<li class="leaf"><a href="/media/photos" title="Lyrica Photos">Photo Gallery</a></li>
<li class="leaf"><a href="/media/discography" title="Lyrica CDs">Lyrica CDs</a></li>
<li class="last leaf"><a href="/media/videos" title="Lyrica Videos">Videos</a></li>
</ul></li>
<li class="leaf"><a href="/events" title="Upcoming Lyrica Concerts and Other Events">Events</a></li>
<li class="leaf"><a href="/about-lyrica" title="About Lyrica">About</a></li>
<li class="last leaf"><a href="/contact" title="Contact Lyrica">Contact</a></li>
</ul>      </div>
    </nav><!-- end main-menu -->
  </header>
  
    
      


  
    
    
      <div id="main" class="clearfix">
      <section id="post-content" role="main">
                        <h1 class="page-title-front">Home</h1>                                        <div class="region region-content">
  <div id="block-block-1" class="block block-block">

      
  <div class="content">
    <link href="https://www.lyrica.co.uk/sites/all/themes/business/css/frontpage.css" rel="stylesheet" type="text/css" />
<p class="frontP">Lyrica singers will provide a stylish soundtrack for all your events. We sing Opera, Vintage Classical, Jazz & Swing music. You can book a wedding soloist or choir for your ceremony; a vocal trio or quartet of singers to create that “Beverley Sisters” sound for your vintage parties or receptions; choose an opera themed evening with our classical soloists, or just relax with our cool jazz trio and singer.  We provide live musicians to accompany us, or sing to quality backings.</p>
<div id="tabular">
	<table class="invisibletable" border="0" cellpadding="5" cellspacing="0" height="300" width="750">
		<tbody>
			<tr>
				<td>
					<a href="/what-we-do/wedding-choirs" id="hover3" alt="Classical, Gospel and Opera songs"><span>Wedding Choirs<p class="smaller">Classical, Gospel and Opera songs.</p></span> </a></td>
				<td>
					<a href="/what-we-do/wedding-soloists" id="hover2" alt="Opera, classical and contemporary songs to enhance your ceremony"><span>Wedding Soloists<p class="smaller">Opera, classical and contemporary songs to enhance your ceremony.</p></span> </a></td>
                <td>
                    <a href="/what-we-do/wedding-reception-music" id="hover1" alt="From Opera to Jazz and Popular songs for your wedding reception"><span>Wedding Reception Music<p class="smaller">Popular songs for your reception. </p></span> </a></td>
			</tr>
		</tbody>
	</table>
</div>

<p class="frontP">Lyrica was formed in 1995, and have sung at hundreds of functions across the U.K. We are based in Bristol, and have sung concerts, weddings and events in venues as varied as The Royal Albert Hall, Glastonbury Abbey, The ICC, Birmingham, The Institute of Directors, and many more castles, churches and hotels. Lyrica is directed by <a href="about-lyrica">BBC Proms soloist Elizabeth Glen.</a></p>



  </div>
  
</div> <!-- /.block -->
<div id="block-system-main" class="block block-system">

      
  <div class="content">
                          <span property="dc:title" content="Home" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>  
      
    
  <div class="content node-page">
      </div>

      <footer>
          </footer>
  
    </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
      </section> <!-- /#main -->

          </div>
    <div class="clear"></div>
    
      <div id="footer-saran" class="clearfix">
     <div id="footer-wrap">
            <div class="footer-box"><div class="region region-footer-first">
  <div id="block-views-featured-video-block-1" class="block block-views">

        <h2 >Featured Video</h2>
    
  <div class="content">
    <div class="view view-featured-video view-id-featured_video view-display-id-block_1 view-dom-id-1ae5ffd209e8364a16aa75931ced0665">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-youtube-video">        <div class="field-content"><a href="/media/video">
<div class="embedded-video">
  <div class="player">
    <iframe class="" width="320" height="180" src="//www.youtube.com/embed/NVmDEYMMnw8?width%3D320%26amp%3Bheight%3D180%26amp%3Btheme%3Dlight%26amp%3Bautoplay%3D0%26amp%3Bvq%3Dlarge%26amp%3Brel%3D0%26amp%3Bshowinfo%3D0%26amp%3Bmodestbranding%3D0%26amp%3Biv_load_policy%3D1%26amp%3Bcontrols%3D1%26amp%3Bautohide%3D1%26amp%3Bwmode%3Dopaque" frameborder="0" allowfullscreen></iframe>  </div>
</div>
</a></div>  </div>  
  <div class="views-field views-field-nothing">        <span class="field-content"><a id="seemore" href="/media/videos">See more videos...</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
                  <div class="footer-box"><div class="region region-footer-second">
  <div id="block-block-4" class="block block-block">

        <h2 >Social Media</h2>
    
  <div class="content">
    <link rel="stylesheet" type="text/css" href="sites/all/themes/business/css/social-icons.css">
<div style="text-align:center; padding-left:5px; padding-right:5px;">
<p>
Don't miss out - click on any of the icons below to like us on <a href="https://www.facebook.com/lyricasingers" target="_blank">Facebook</a>, <a href="https://plus.google.com/101063158560605596672" target="_blank">Google+</a>, <a href="https://soundcloud.com/lyricasingers" target="_blank">Soundcloud</a> or subscribe on <a href="https://www.youtube.com/LyricaSingers" target="_blank">YouTube!</a></p>
<p>
<a href="https://www.facebook.com/lyricasingers" id="fb" target="_blank">
<span>Facebook</span>
</a>
<a href="https://www.youtube.com/LyricaSingers" id="yt" target="_blank">
<span>YouTube</span>
</a>
<a href="https://soundcloud.com/lyricasingers" id="sc" target="_blank">
<span>SoundCloud</span>
</a>
<a href="https://plus.google.com/101063158560605596672" id="gp" target="_blank">
<span>Google Plus</span>
</a>

</p>
</div>
<div style="text-align:center; margin-top:10px;">

<a href="https://www.theweddingsecret.co.uk/all-venues-bristol.htm">
<img src="https://s3.amazonaws.com/images.theweddingsecret.co.uk/posts/partnered-with-the-wedding-secret.png" width="85px" alt="The Wedding Secret" title="The Wedding Secret">
</a>
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
                  <div class="footer-box"><div class="region region-footer-third">
  <div id="block-views-featured-song-block-1" class="block block-views">

        <h2 >Featured Song</h2>
    
  <div class="content">
    <div class="view view-featured-song view-id-featured_song view-display-id-block_1 view-dom-id-53b5edd7c904ecfaf16c1f660837e975">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-featured-track">        <div class="field-content"></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
<div id="block-block-13" class="block block-block">

      
  <div class="content">
    <div style="text-align:center;">
<a class="buttonLink" href="media/music" style="margin-right:auto; margin-left:auto;">Get this track as a free download now, and hear more!</a>
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
                 </div>
    </div>
    <div class="clear"></div>
    
  <!--END footer -->
  <div class="region region-footer">
  <div id="block-footermap-footermap" class="block block-footermap">

      
  <div class="content">
    <div class="footermap">
  
    <div id="footermap-col-main-menu" class="footermap-col">
    <ul  class="footermap-col footermap-header" id="footermap-col-1"><li  class="footermap-item footermap-item-0" id="footermap-item-218"><a href="/" title="Lyrica - Singers for your wedding or occasion" class="active">Home</a></li><li  class="footermap-item footermap-item-0" id="footermap-item-312"><a href="/what-we-do" title="Lyrica Singers for your Function, Wedding, Reception or other event.">What We Do</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-318"><a href="/what-we-do/wedding-choirs" title="Wedding Choirs">Wedding Choirs</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-315"><a href="/what-we-do/wedding-soloists" title="Wedding Soloists">Wedding Soloists</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-317"><a href="/what-we-do/wedding-reception-music" title="Popular songs for your reception">Wedding Receptions</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-900"><a href="/what-we-do/client-testimonials" title="Testimonials from Lyrica clients">Client Testimonials</a></li><li  class="footermap-item footermap-item-0" id="footermap-item-313"><a href="/media" title="Music, CDs, Videos &amp; Photos of Lyrica Singers">Music &amp; More</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-327"><a href="/media/music" title="Hear Lyrica">Hear Lyrica</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-328"><a href="/media/photos" title="Lyrica Photos">Photo Gallery</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-330"><a href="/media/discography" title="Lyrica CDs">Lyrica CDs</a></li><li  class="footermap-item footermap-item-1" id="footermap-item-329"><a href="/media/videos" title="Lyrica Videos">Videos</a></li><li  class="footermap-item footermap-item-0" id="footermap-item-421"><a href="/events" title="Upcoming Lyrica Concerts and Other Events">Events</a></li><li  class="footermap-item footermap-item-0" id="footermap-item-310"><a href="/about-lyrica" title="About Lyrica">About</a></li><li  class="footermap-item footermap-item-0" id="footermap-item-311"><a href="/contact" title="Contact Lyrica">Contact</a></li></ul>  </div>
  </div>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
  
    <div class="clear"></div>
  <div id="copyright">
          Copyright &copy; 2019, Lyrica, All Rights Reserved. By using this site you consent to the use of cookies -  <a href="/legal">Legal</a>. </br> Website: <a href="http://www.lgcs.co">LGCS</a>.
          </div>
  </div>
  </body>
</html>