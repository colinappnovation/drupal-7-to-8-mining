<!DOCTYPE html>
<html>
<head>

<meta name="twitter:card" content="summary">
 <meta name="twitter:url" content="https://about.esd.org.uk">
 <meta name="twitter:title" content="LG Inform Plus">
 <meta name="twitter:description" content="Handy online tools for local government to see data about their people and places.">
 <meta name="twitter:image" content="https://pbs.twimg.com/profile_images/506435765627138049/NX97FN5Z.png">
 <meta name="twitter:site" content="@LGInformPlus">
 <meta name="twitter:creator" content="@LGInformPlus">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href='//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900,300' rel='stylesheet' type='text/css' />
    <meta name="google-site-verification" content="PqvMn_QFPNjuObuAIc0JzHMUdtpWzCOAcqrtJbZ61s0" />

	<link rel="apple-touch-icon" href="/sites/all/themes/esd/images/icon/LG_inform_57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/sites/all/themes/esd/images/icon/LG_inform_72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/sites/all/themes/esd/images/icon/LG_inform_114x114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/sites/all/themes/esd/images/icon/LG_inform_144x144.png" />

    <title>Tools | LG Inform Plus</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://about.esd.org.uk/sites/all/themes/esd/images/common/favicon_1.ico" type="image/vnd.microsoft.icon" />
<link rel="shortlink" href="/node/1" />
<link rel="canonical" href="/tools" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
	<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.min.js?v=1.7.2"></script>
<script type="text/javascript" src="https://about.esd.org.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://about.esd.org.uk/misc/drupal.js?ow6fw1"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/jquery_update/replace/ui/external/jquery.cookie.js?v=67fb34f6a866c40d0570"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/jquery_update/replace/misc/jquery.form.min.js?v=2.69"></script>
<script type="text/javascript" src="https://about.esd.org.uk/misc/ajax.js?v=7.63"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/jquery_update/js/jquery_update.js?v=0.0.1"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/comment_notify/comment_notify.js?ow6fw1"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/views/js/base.js?ow6fw1"></script>
<script type="text/javascript" src="https://about.esd.org.uk/misc/progress.js?v=7.63"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/modules/views/js/ajax_view.js?ow6fw1"></script>
<script type="text/javascript" src="https://resources.esd.org.uk/scripts/common/modernizr-2.6.2.js"></script>
<script type="text/javascript" src="https://resources.esd.org.uk/scripts/common/json3.min.js"></script>
<script type="text/javascript" src="https://resources.esd.org.uk/scripts/common/underscore-min.js"></script>
<script type="text/javascript" src="https://resources.esd.org.uk/scripts/common/esd.core.js"></script>
<script type="text/javascript" src="https://about.esd.org.uk/sites/all/themes/esd/scripts/waypoints.min.js?ow6fw1"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"esd","theme_token":"5n-psf2ufYzvMqxim0ILl-Qy1xqAvK0zl0Xp5uoPGzg","jquery_version":"1.7","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/comment_notify\/comment_notify.js":1,"sites\/all\/modules\/views\/js\/base.js":1,"misc\/progress.js":1,"sites\/all\/modules\/views\/js\/ajax_view.js":1,"https:\/\/resources.esd.org.uk\/scripts\/common\/modernizr-2.6.2.js":1,"https:\/\/resources.esd.org.uk\/scripts\/common\/json3.min.js":1,"https:\/\/resources.esd.org.uk\/scripts\/common\/underscore-min.js":1,"https:\/\/resources.esd.org.uk\/scripts\/common\/esd.core.js":1,"sites\/all\/themes\/esd\/scripts\/waypoints.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/comment_notify\/comment_notify.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/resources.esd.org.uk\/styles\/common\/reset.css":1,"https:\/\/resources.esd.org.uk\/styles\/common\/esd.lga.css":1,"https:\/\/resources.esd.org.uk\/styles\/common\/esd.top-bar.lga.css":1,"https:\/\/resources.esd.org.uk\/styles\/common\/esd.no-js.css":1}},"views":{"ajax_path":"\/views\/ajax","ajaxViews":{"views_dom_id:56eb068b09da584fbd67f2d703f3a59e":{"view_name":"news","view_display_id":"block","view_args":"","view_path":"node\/1","view_base_path":"news","view_dom_id":"56eb068b09da584fbd67f2d703f3a59e","pager_element":0}}},"urlIsAjaxTrusted":{"\/views\/ajax":true,"\/":true}});
//--><!]]>
</script>
	<style type="text/css" media="all">
@import url("https://about.esd.org.uk/modules/system/system.base.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/system/system.menus.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/system/system.messages.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/system/system.theme.css?ow6fw1");
</style>
<style type="text/css" media="all">
@import url("https://about.esd.org.uk/sites/all/modules/comment_notify/comment_notify.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/aggregator/aggregator.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/comment/comment.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/field/theme/field.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/node/node.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/search/search.css?ow6fw1");
@import url("https://about.esd.org.uk/modules/user/user.css?ow6fw1");
@import url("https://about.esd.org.uk/sites/all/modules/views/css/views.css?ow6fw1");
</style>
<style type="text/css" media="all">
@import url("https://about.esd.org.uk/sites/all/modules/ctools/css/ctools.css?ow6fw1");
</style>
<link type="text/css" rel="stylesheet" href="https://resources.esd.org.uk/styles/common/reset.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://resources.esd.org.uk/styles/common/esd.lga.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://resources.esd.org.uk/styles/common/esd.top-bar.lga.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://resources.esd.org.uk/styles/common/esd.no-js.css" media="all" />

		<meta name="apple-mobile-web-app-title" content="LG Inform +" />
	<meta name="description" content="Easy access to up-to-date published data about local government wards and other small areas through a suite of handy online applications. Create reports tailored to your own needs to analyse, predict and improve service outcomes in your local authority. Take a deeper look at the communities that make up your local area through simple, intuitive tools." />
	

</head>
<body class="no-js fixed-width">


                	                	
  <div class="region region-signin">
    <div id="block-connector-one-click-block" class="block block-connector">

    
  <div class="content">
    <form action="/" method="post" id="connector-button-form" accept-charset="UTF-8"><div><span class="connector-button-wrapper oauthconnector-esd"><input class="connector-button oauthconnector-esd form-submit" type="submit" id="edit-oauthconnector-esd" name="op" value="Connect with esd-signin" /></span><input type="hidden" name="form_build_id" value="form-0HZMozFUne7IfDUR_tckp0_yfkntHy4mtRMBBVUS23M" />
<input type="hidden" name="form_id" value="connector_button_form" />
</div></form>  </div>
</div>
  </div>
<script type='text/javascript'>if (jQuery('#connector-button-form').length < 1) { location.reload(true); } </script>    <script type="text/javascript">
        document.body.className = document.body.className.replace('no-js', '').trim();
    </script>
    <div id="background"><div id="background-top-bar"></div><div id="background-header"></div></div>

    <div id="page-content">
<div id="top-bar">
    <ul class="navigation">

    </ul>

<script type="text/javascript">
    jQuery(function() {
        var menu = [
    {
        "title": "Local Government Association",
        "url": "http://www.local.gov.uk/"
    },
    {
        "title": "LG Inform",
        "url": "http://lginform.local.gov.uk/",
        "children": [
             {
                "title": "LG Inform",
                "url": "http://lginform.local.gov.uk"
            },
            {
                "title": "Value for Money",
                "url": "http://vfm.lginform.local.gov.uk"
            }
        ]
    },
    {
        "title": "LG Inform Plus",
        "url": "http://about.esd.org.uk",
        "children": [
            {
                "title": "LG Inform Plus home",
                "url": "http://about.esd.org.uk",
                "cssClass": "home"
            },
            {
                "title": "File exchange",
                "url": "http://files.esd.org.uk/"
            },
            {
                "title": "Neighbourhoods",
                "url": "http://neighbourhoods.esd.org.uk/"
            },
            {
                "title": "Powers and duties",
                "url": "http://powersandduties.esd.org.uk/"
            },
            {
                "title": "Records retention",
                "url": "http://retention.esd.org.uk/"
            },
            {
                "title": "RoPA",
                "url": "http://ropa.esd.org.uk/"
            },			
            {
                "title": "Reports",
                "url": "http://reports.esd.org.uk/",
                "children": [
                    {
                        "title": "Create a report",
                        "url": "http://reports.esd.org.uk/reports/create"
                    },
                    {
                        "title": "Create a table",
                        "url": "http://reports.esd.org.uk/table"
                    },
                    {
                        "title": "Create a bar chart",
                        "url": "http://reports.esd.org.uk/bar"
                    },
                    {
                        "title": "Create a pie chart",
                        "url": "http://reports.esd.org.uk/pie"
                    },
                    {
                        "title": "Create a map",
                        "url": "http://reports.esd.org.uk/map"
                    }
                ]
            }
        ]
    },
    {
        "title": "Data",
        "url": "http://api.esd.org.uk/",
        "children": [
            {
                "title": "API",
                "url": "http://api.esd.org.uk/"
            },
            {
                "title": "Data maturity",
                "url": "http://datamaturity.esd.org.uk/"
            },
            {
                "title": "Data tool",
                "url": "http://developertools.esd.org.uk/dataMethod"
            },
            {
                "title": "Metrics calendar",
                "url": "https://calendar.esd.org.uk/"
            },
            {
                "title": "My Local Metrics",
                "url": "http://mymetrics.esd.org.uk/"
            },
            {
                "title": "Open data",
                "url": "http://opendata.esd.org.uk/"
            },
            {
                "title": "Uploader",
                "url": "http://upload.esd.org.uk/"
            },
            {
                "title": "Web methods",
                "url": "http://developertools.esd.org.uk/methods"
            }
        ]
    },
    {
        "title": "Standards",
        "url": "http://standards.esd.org.uk/",
        "children": [
            {
                "title": "Standards home",
                "url": "http://standards.esd.org.uk/",
                "cssClass": "home"
            },
            {
                "url": "http://id.esd.org.uk/list/az",
                "title": "A to Z"
            },
            {
                "url": "http://id.esd.org.uk/list/areas",
                "title": "Areas"
            },
            {
                "url": "http://id.esd.org.uk/list/channels",
                "title": "Channels"
            },
            {
                "url": "http://id.esd.org.uk/list/circumstances",
                "title": "Circumstances"
            },
            {
                "url": "http://id.esd.org.uk/list/functions",
                "title": "Functions"
            },
            {
                "url": "http://id.esd.org.uk/list/interactions",
                "title": "Interactions"
            },
            {
                "url": "http://id.esd.org.uk/list/issuesAndBenefits",
                "title": "Issues and benefits"
            },
            {
                "url": "http://id.esd.org.uk/list/lifeEvents",
                "title": "Life events"
            },
            {
                "url": "http://id.esd.org.uk/list/metricTypes",
                "title": "Metric types"
            },
            {
                "url": "http://id.esd.org.uk/list/needs",
                "title": "Needs"
            },
            {
                "url": "http://id.esd.org.uk/list/periods",
                "title": "Periods"
            },
            {
                "url": "http://id.esd.org.uk/list/powersAndDuties",
                "title": "Powers and duties"
            },
            {
                "url": "http://id.esd.org.uk/list/processes",
                "title": "Processes"
            },
            {
                "url": "http://id.esd.org.uk/list/proclass",
                "title": "ProClass"
            },
            {
                "url": "http://id.esd.org.uk/list/services",
                "title": "All services",
                "children": [
                    {
                        "url": "http://id.esd.org.uk/list/internalServices",
                        "title": "Internal services"
                    },
                    {
                        "url": "http://id.esd.org.uk/list/englishAndWelshServices",
                        "title": "Local government services (LGSL)"
                    },
                    {
                        "url": "http://id.esd.org.uk/list/scottishServices",
                        "title": "Scottish services"
                    }
                ]
            },
            {
                "url": "http://id.esd.org.uk/list/subjects",
                "title": "Subjects"
            },
            {
                "url": "http://standards.esd.org.uk/?uri=group%2FwebsiteNavigation",
                "title": "Website navigation",
                "children": [
                    {
                        "url": "http://id.esd.org.uk/list/scottishNavigation",
                        "title": "Scottish website navigation"
                    },
                    {
                        "url": "http://id.esd.org.uk/list/websiteNavigation",
                        "title": "Website navigation"
                    }
                ]
            }
        ]
    }
];

        esd.core.buildTopNavigationMenuWithSubdomain(jQuery('#top-bar ul.navigation'), menu);
    })
</script>


    <ul class="credentials">
		<li class="first help"><a href="http://gethelp.esd.org.uk/?category=about" class="help">Help</a></li>
	        <li class="about"><a href="https://about.esd.org.uk/about" class="about">About</a></li>
		<li><a class="signin" onclick="document.getElementById('connector-button-form').submit();">Sign in</a></li>
        <li><a href="https://signin.esd.org.uk/register.html?app=esd" class="register">Register</a></li>
    </ul>
</div>



<div id="container">
    <div id="content">
        <div id="header">
    <div id="page-title">
        <h1>LG Inform Plus</h1><h2>powered by esd</h2>
    </div>

        </div>
        <div id="main">
            <div id="tabs">
                  <div class="region region-sidebar-first">
    <div id="block-system-main-menu" class="block block-system block-menu">

    
  <div class="content">
    <ul class="menu"><li class="first leaf active-trail"><a href="/tools" title="List of esd tools" class="active-trail selected active">tools</a></li>
<li class="collapsed"><a href="/about" title="About esd">about</a></li>
<li class="collapsed"><a href="/news" title="esd news">news</a></li>
<li class="leaf"><a href="http://files.esd.org.uk/" title="files">files</a></li>
<li class="last leaf"><a href="/terms" title="Terms and conditions">terms</a></li>
</ul>  </div>
</div>
  </div>
            </div>

            <div id="documents">
                <div class="document about">
                	  <div class="region region-content">
    <div id="block-views-news-block" class="block block-views">

    
  <div class="content">
    <div class="view view-news view-id-news view-display-id-block view-dom-id-56eb068b09da584fbd67f2d703f3a59e">
            <div class="view-header">
      latest    </div>
  
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/news/lga-place-based-directory-services-loneliness-pilots">LGA Place-based Directory of Services Loneliness Pilots</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
</div>
<div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-1" class="node node-page clearfix" about="/tools" typeof="foaf:Document">

  
        <span property="dc:title" content="Tools" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
  
  <div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><style type="text/css">
<!--/*--><![CDATA[/* ><!--*/
html #documents .toolicons div.tool {margin: 21px 70px;} 
/*--><!]]>*/
</style><div class="toolicons" style="display: table; width: 100%;">
<div style="display: table-row;">
<div style="display: table-cell; width: 100%;">
<div class="tool"><a href="http://reports.esd.org.uk/"><img alt="Reports" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/Report-writer200x200.png" style="width: 100px; height: 100px;" />Reports</a></div>
<div class="tool"><a href="http://neighbourhoods.esd.org.uk"><img alt="Natural neighbourhoods" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/natural_neighbourhood_200x200.png" style="width: 100px; height: 100px;" />Natural neighbourhoods</a></div>
<div class="tool"><a href="http://mymetrics.esd.org.uk"><img alt="My Local Metrics" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/mylocalmetrics_200x200.png" style="width: 100px; height: 100px;" />My Local Metrics</a></div>
</div>
</div>
<div style="display: table-row;">
<div style="display: table-cell; width: 100%;">
<div class="tool"><a href="http://powersandduties.esd.org.uk"><img alt="Powers and duties" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/Powers_Duties_200x200.png" style="width: 100px; height: 100px;" />Powers and duties</a></div>
<div class="tool"><a href="http://retention.esd.org.uk/"><img alt="Records Retention" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/Records-retention200x200.png" style="width: 100px; height: 100px;" />Records Retention</a></div>
<div class="tool"><a href="http://ropa.esd.org.uk/"><img alt="RoPA" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/ropa_200x200.png" style="width: 100px; height: 100px;" />RoPA</a></div>
</div>
</div>
<div style="display: table-row;">
<div style="display: table-cell; width: 100%;">
<div class="tool"><a href="http://opendata.esd.org.uk"><img alt="Open data" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/open_data200x200.png" style="width: 100px; height: 100px;" />Open data</a></div>
<div class="tool"><a href="http://datamaturity.esd.org.uk"><img alt="Data maturity" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/data-maturity200x200.png" style="width: 100px; height: 100px;" />Data maturity</a></div>
<div class="tool"><a href="http://lginform.local.gov.uk/"><img alt="LG Inform" src="/sites/about.esd.org.uk/files/var/www/html/sites/all/themes/esd/images/page/LG_inform_200x200.png" style="width: 100px; height: 100px;" />LG Inform</a></div>
</div>
</div>
</div>
</div></div></div>	    </div>

  <!--  -->

  
</div>
  </div>
</div>
  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
                	

<!-- Begin Google Analytics code, note that this code works with and without SSL and uses the new Google Analytics script -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-765222-3']);
    _gaq.push(['_trackPageview']);
    _gaq.push(['_trackPageview', '/']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<!-- End Google Analytics code -->
</body>
</html>