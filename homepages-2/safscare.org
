<!DOCTYPE html>
<html  lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head><link rel='dns-prefetch' href='//cdn.jsdelivr.net'><link rel='dns-prefetch' href='//use.fontawesome.com'><link rel='dns-prefetch' href='//ajax.googleapis.com'>
    <meta charset="utf-8" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139930016-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments)};gtag("js", new Date());gtag("config", "UA-139930016-1", {"groups":"default","anonymize_ip":true});</script>
<meta name="title" content="SAFS | Local Social Care &amp; Windmill Community Centre" />
<meta name="geo.region" content="GB" />
<meta name="icbm" content="&#039;52.49401,-1.95552&#039;" />
<meta name="geo.position" content="&#039;52.49401,-1.95552&#039;" />
<meta name="geo.placename" content="West Midlands" />
<link rel="shortlink" href="https://www.safscare.org/" />
<meta name="robots" content="index, follow" />
<meta http-equiv="content-language" content="en" />
<link rel="canonical" href="https://www.safscare.org/" />
<meta name="description" content="SAFS provides a range of quality health and social care services in Sandwell, Birmingham and neighbouring boroughs. SAFS are also manage the Windmill Community Centre." />
<meta name="keywords" content="west,midlands,health,social,care,windmill,community,centre" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:regular,700,300&amp;subset=latin" media="all" />
<link rel="shortcut icon" href="/themes/custom/safs/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="revision" href="https://www.safscare.org/home" />

    <title>SAFS | Local Social Care &amp; Windmill Community Centre</title>
    <link rel="stylesheet" media="all" href="/sites/safscare.org/files/css/css_9XhyVglhVtkkvBuR2vruGhky2q-O06uGMRqVwK1bWgY.css?q12q5n" />
<link rel="stylesheet" media="all" href="/sites/safscare.org/files/css/css_6eAWDytS809wUtz24dihsAFJcv5Z4L4M6NxZuj3jlq0.css?q12q5n" />
<link rel="stylesheet" media="all" href="/sites/safscare.org/files/css/css_vUS19LWqD_ylpL74ntmayz7PEfxSAQvQDhqPwHErzZk.css?q12q5n" />
<link rel="stylesheet" media="all" href="/sites/safscare.org/files/css/css_L9Pz2DaHKaQPvWy5bsdrCcekGH5ic_eslFzMwYDRZAo.css?q12q5n" />
<link rel="stylesheet" media="all" href="//cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" />
<link rel="stylesheet" media="all" href="//cdn.jsdelivr.net/npm/@unicorn-fail/drupal-bootstrap-styles@0.0.1/dist/3.4.0/8.x-3.x/drupal-bootstrap.min.css" />
<link rel="stylesheet" media="all" href="/sites/safscare.org/files/css/css__b7Rdnc2x5VigpGeglT_em-puILVjUS7wkPAWs07dyA.css?q12q5n" />

    
<!--[if lte IE 8]>
<script src="/sites/safscare.org/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->
<script src="//use.fontawesome.com/releases/v5.7.2/js/all.js" defer></script>
<script src="//use.fontawesome.com/releases/v5.7.2/js/v4-shims.js" defer></script>

  </head>
  <body class="fontyourface path-frontpage page-node-type-page has-glyphicons">
    <a href="#main-content" class="visually-hidden focusable skip-link">
      Skip to main content
    </a>
    
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    
  <div role="contentinfo" class="header-top">
    <div class="container">
      <div class="row">

        <div class="header-top-left col-md-7 col-sm-6 col-xs-5 container">
                                      <div class="region region-header-top-left">
    <section id="block-topleftnavblock" class="block block-block-content block-block-contentb811b784-a898-43c7-b10f-8c54a95bd79f clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><ul>
	<li class="hidden-xs"><a href="/">Home</a></li>
	<li class="hidden-xs"><a href="/vacancies">Vacancies</a></li>
	<li><a href="/contact">Contact Us</a></li>
	<li class="top-menu-donate"><a href="/donate"><i class="fas fa-star"></i> Donate<span class="hidden-xs"> Now</span></a></li>
</ul>
</div>
      
  </section>


  </div>

                              </div>

        <div class="header-top-right col-md-5 col-sm-6 col-xs-7 container">
                                      <div class="region region-header-top-right">
    <section id="block-toprightsocialblock" class="block block-block-content block-block-content1d2df83f-10d6-4577-980c-f39d2973a11b clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><ul>
	<li class="ts-facebook"><a href="https://www.facebook.com/safscare" style="width: 40px; overflow: hidden;"><i class="fab fa-facebook-f"></i><span class="ts-text">Facebook</span></a></li>
	<li class="ts-twitter"><a href="https://twitter.com/safscare" style="width: 40px; overflow: hidden;"><i class="fab fa-twitter"></i><span class="ts-text">Twitter</span></a></li>
	<li class="ts-phone"><a href="tel:+4401215582198" style="width: 40px; overflow: hidden;"><i class="fas fa-phone"></i><span class="ts-text">+44.121.5582198</span></a></li>
	<li class="ts-mail"><a href="/cdn-cgi/l/email-protection#733a1d151c3300121500101201165d1c0114" style="width: 40px; overflow: hidden;"><i class="fas fa-envelope"></i><span class="ts-text"><span class="__cf_email__" data-cfemail="f1989f979eb18290978292908394df9e8396">[email&#160;protected]</span></span></a></li>
</ul></div>
      
  </section>


  </div>

                              </div>

      </div>
    </div>
  </div>

         <header role="banner">
        <div class="container">
        <div class="row">
      
                              <div class="col-md-3 col-sm-3 col-xs-6" role="heading" id="nav-branding">
              <div class="region region-navigation-branding">
          <a class="logo navbar-btn pull-left" href="/" title="Home" rel="home">
      <img src="/themes/custom/safs/logo.png" alt="Home" />
    </a>
        <a class="name navbar-brand" href="/" title="Home" rel="home">SAFS</a>
    
  </div>

          </div>
              
                              <div class="col-md-9 col-sm-9 col-xs-6" role="heading">
              <div class="region region-navigation-header">
    <section id="block-mainnavigation" class="block block-superfish block-superfishmain clearfix">
  
    

      
<ul id="superfish-main" class="menu sf-menu sf-main sf-horizontal sf-style-none hidden-xs hidden-sm">
  
<li id="main-menu-link-contenta5f1ea81-d5cc-41b7-a470-bd8f3dffd010" class="sf-depth-1 menuparent"><a href="/about" title="Our Success" class="sf-depth-1 menuparent">About Us <span class="sf-description">Our Success</span></a><ul><li id="main-menu-link-content40822758-257a-4217-8926-75c6e0618505" class="sf-depth-2 sf-no-children"><a href="/about/history" class="sf-depth-2">Our History</a></li><li id="main-menu-link-contentcd2ea914-1bb9-4961-a0fb-1c14b2fe712a" class="sf-depth-2 sf-no-children"><a href="/about/stories" class="sf-depth-2">Our Stories</a></li><li id="main-menu-link-content793d49b1-a6cd-4bd4-9ed7-25b044b3a048" class="sf-depth-2 sf-no-children"><a href="/about/board" class="sf-depth-2">Our Board</a></li><li id="main-menu-link-content9c25d9ce-fba3-4e17-9cf7-f7461ac52cfc" class="sf-depth-2 sf-no-children"><a href="/about/associates" class="sf-depth-2">Our Associates</a></li><li id="main-menu-link-content8a143c4f-9e88-4b88-a0d3-ee7c580d5933" class="sf-depth-2 sf-no-children"><a href="/about/our-reports" class="sf-depth-2">Our Reports</a></li><li id="main-menu-link-contentfbf443c8-011a-49ff-8968-51443cd98878" class="sf-depth-2 sf-no-children"><a href="/about/our-opportunities" class="sf-depth-2">Our Opportunities</a></li></ul></li><li id="main-menu-link-content2ef70c29-2512-403a-86a0-c00cebb7d5ae" class="sf-depth-1 menuparent"><a href="/services" title="Our services" class="sf-depth-1 menuparent">What We Do <span class="sf-description">Our services</span></a><ul><li id="main-menu-link-content60a8b1f5-6f89-49f1-9f13-57c7a989f86d" class="sf-depth-2 sf-no-children"><a href="/services/community-based-services" class="sf-depth-2">Community Based Services</a></li><li id="main-menu-link-content98210c9a-e802-416d-b47d-288f0f26bcd5" class="sf-depth-2 sf-no-children"><a href="/services/domiciliary-care" class="sf-depth-2">Domiciliary Care Service</a></li><li id="main-menu-link-content64f16f91-c9a2-48d6-ad0f-fd97b364f875" class="sf-depth-2 sf-no-children"><a href="/services/lifestyles" class="sf-depth-2">Lifestyles Service</a></li><li id="main-menu-link-contentfb2fd162-94b9-4aeb-9c78-d7aceca69781" class="sf-depth-2 sf-no-children"><a href="/services/parents-and-carers-services" class="sf-depth-2">Parents and Carers Services</a></li></ul></li><li id="main-menu-link-contentdcf9f74e-0210-4d85-80b7-d31d9db35075" class="sf-depth-1 menuparent"><a href="/support" title="You Can Help" class="sf-depth-1 menuparent">Support Us <span class="sf-description">You Can Help</span></a><ul><li id="main-menu-link-content6444bf6f-87ef-4e27-a328-44001bcc97cd" class="sf-depth-2 sf-no-children"><a href="/donate" class="sf-depth-2">Donate</a></li></ul></li><li id="main-menu-link-content1e69c975-18ca-4556-8d12-e6a490233ea2" class="sf-depth-1 sf-no-children"><a href="/latest-news" title="Latest News" class="sf-depth-1">Updates <span class="sf-description">Latest News</span></a></li><li id="main-menu-link-contentd75d9a22-3588-439c-93e6-953c07ffb901" class="sf-depth-1 menuparent"><a href="/community-centre" title="Community Centre" class="sf-depth-1 menuparent">Windmill <span class="sf-description">Community Centre</span></a><ul><li id="main-menu-link-content5b9365db-e50a-4b64-82b1-1cac5033e521" class="sf-depth-2 sf-no-children"><a href="/community-centre/community-groups" class="sf-depth-2">Community Groups</a></li><li id="main-menu-link-contentd70587b9-b71d-4a81-be93-63ddfe8ad863" class="sf-depth-2 sf-no-children"><a href="/community-centre/private-bookings" class="sf-depth-2">Private Bookings</a></li><li id="main-menu-link-contentf10c8490-75e2-426f-9a33-792cda6b564e" class="sf-depth-2 sf-no-children"><a href="/community-centre/whats-on" class="sf-depth-2">What’s On</a></li></ul></li>
</ul>

  </section>


  </div>

          </div>
              
              </div>
        </div>
        </header>
      


  <section role="banner" class="container-fluid" id="header_extra">

        <div class="region region-header-extra">
    <section id="block-homepageslider" class="block block-md-slider block-md-slider-blockhomepage-slider clearfix">
  
    

      <div id="md-slider-1-block" class="md-slide-items"  style="min-height:400px" data-thumb-height="100" data-thumb-width="100" >
    <div class="wrap-loader-slider animated">
        <div class="wrap-cube">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
    </div>
            
            <div class="md-slide-item md-layer-1-1"  data-timeout="8000" data-thumb="https://www.safscare.org/sites/safscare.org/files/styles/thumbnail/public/md-slider-image/slide5.jpg?itok=yGon8C0f" data-thumb-type="image" data-thumb-alt="">
<div class="md-main-img" style="">

            <img src="https://www.safscare.org/sites/safscare.org/files/md-slider-image/slide5.jpg" alt="Slide 5"  />
    
</div>
<div class="md-objects">
    </div>
</div>


            <div class="md-slide-item md-layer-1-2"  data-timeout="8000" data-thumb="https://www.safscare.org/sites/safscare.org/files/styles/thumbnail/public/md-slider-image/slide2.jpg?itok=Azz0G7JE" data-thumb-type="image" data-thumb-alt="">
<div class="md-main-img" style="">

            <img src="https://www.safscare.org/sites/safscare.org/files/md-slider-image/slide2.jpg" alt="Slide 2"  />
    
</div>
<div class="md-objects">
    </div>
</div>


            <div class="md-slide-item md-layer-1-3"  data-timeout="8000" data-thumb="https://www.safscare.org/sites/safscare.org/files/styles/thumbnail/public/md-slider-image/slide3.jpg?itok=xJi-vN6N" data-thumb-type="image" data-thumb-alt="">
<div class="md-main-img" style="">

            <img src="https://www.safscare.org/sites/safscare.org/files/md-slider-image/slide3.jpg" alt="Slide 3"  />
    
</div>
<div class="md-objects">
    </div>
</div>


            <div class="md-slide-item md-layer-1-4"  data-timeout="8000" data-thumb="https://www.safscare.org/sites/safscare.org/files/styles/thumbnail/public/md-slider-image/slide4.jpg?itok=eDNg0won" data-thumb-type="image" data-thumb-alt="">
<div class="md-main-img" style="">

            <img src="https://www.safscare.org/sites/safscare.org/files/md-slider-image/slide4.jpg" alt="Slide 4"  />
    
</div>
<div class="md-objects">
    </div>
</div>


            <div class="md-slide-item md-layer-1-5"  data-timeout="8000" data-thumb="https://www.safscare.org/sites/safscare.org/files/styles/thumbnail/public/md-slider-image/slide1.jpg?itok=B3sQl08i" data-thumb-type="image" data-thumb-alt="">
<div class="md-main-img" style="">

            <img src="https://www.safscare.org/sites/safscare.org/files/md-slider-image/slide1.jpg" alt="Slide 1"  />
    
</div>
<div class="md-objects">
    </div>
</div>


    </div>

  </section>


  </div>

  
  </section>

  <div role="main" class="main-container container js-quickedit-main-content">
    <div class="row">

            
                  <section class="col-sm-12">

                                      <div class="highlighted">  <div class="region region-highlighted">
    <div data-drupal-messages-fallback class="hidden"></div>

  </div>
</div>
                  
                
                          <a id="main-content"></a>
            <div class="region region-content">
      <article data-history-node-id="1" role="article" about="/home" typeof="schema:WebPage" class="page full clearfix">

  
      <span property="schema:name" content="Home" class="hidden"></span>


  
  <div class="content">
    
            <div property="schema:text" class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><h4>SAFS provides a range of quality health and social care services in Sandwell, Birmingham and neighbouring boroughs. SAFS also manage the Windmill Community Centre.</h4>

<p> </p>

<p>SAFS Vision is: a world where children, young people and adults with disabilities can reach their full potential with love and care, free from any kind of discrimination and deprivation.</p>

<p> </p>

<p>SAFS Mission is: to enhance the quality of life – and to improve the health and wellbeing of individuals from the South Asian and other communities, who have a disability, by providing quality care and opportunities.</p></div>
      
  </div>

</article>


  </div>

              </section>

                </div>
  </div>

  <div role="contentinfo" class="footer-info">
    <div class="container">
      <div class="row">

        <div class="footer-col-1 col-md-3 container">
                                      <div class="region region-footer-col-1">
    <section id="block-footercolumnaboutblock" class="block block-block-content block-block-content21c14c04-a09f-475b-bbdf-45e67e679781 clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><div class="footer-about-us">
<ul class="fa-ul"><li>Windmill Community Centre<br />
	Messenger Road, Smethwick<br />
	West Midlands, B66 3DX<br />
	United Kingdom</li>
	<li>+44.121.5582198</li>
	<li><a href="/cdn-cgi/l/email-protection#e78e898188a79486819484869582c9889580"><span class="__cf_email__" data-cfemail="6f060109002f1c0e091c0c0e1d0a41001d08">[email&#160;protected]</span></a></li>
</ul></div>
</div>
      
  </section>


  </div>

                              </div>

        <div class="footer-col-2 col-md-6 container">
                                      <div class="region region-footer-col-2">
    <section id="block-footersociallinks" class="block block-block-content block-block-content04f48d95-0097-4dff-9eec-1bc51f6f1424 clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><ul class="footer-social-links col-md-12 col-xs-12"><li class="col-md-4 col-xs-12"><a href="https://www.facebook.com/safscare"><img alt="Facebook Logo" class="img-responsive" src="/sites/safscare.org/files/images/footer/footer_facebook.png" title="Visit our Facebook page" /></a></li>
	<li class="col-md-4 col-xs-12"><a href="https://twitter.com/safscare"><img alt="Twitter Logo" class="img-responsive" src="/sites/safscare.org/files/images/footer/footer_twitter.png" title="Visit our Twitter feed" /></a></li>
	<li class="col-md-4 col-xs-12"><a href="https://www.flickr.com/photos/129962370@N08/"><img alt="Flickr Logo" class="img-responsive" src="/sites/safscare.org/files/images/footer/footer_flickr.png" title="View our photo albums" /></a></li>
</ul></div>
      
  </section>


  </div>

                              </div>

        <div class="footer-col-3 col-md-3 container">
                                      <div class="region region-footer-col-3">
    <section id="block-footercolumnlinksblock" class="block block-block-content block-block-contentc32cc847-644d-4686-922b-fb2bcdc2a2d0 clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p class="align-ul"><strong>Useful Links:</strong></p>

<ul class="fa-ul no-margin-left"><li><a href="https://www.cqc.org.uk/">Care Quality Commission</a></li>
	<li><a href="https://www.gov.uk/government/organisations/ofsted/">Ofsted</a></li>
	<li><a href="http://www.legislation.gov.uk/ukpga/2014/23/contents/enacted">Care Act</a></li>
	<li><a href="https://www.sandwell.gov.uk/">Sandwell Metropolitan Borough Council</a></li>
	<li><a href="https://www.birmingham.gov.uk/">Birmingham City Council</a></li>
</ul></div>
      
  </section>


  </div>

                              </div>

      </div>
    </div>
  </div>

<footer class="footer" role="contentinfo">
  <div class="container">

    <div class="footer-copyright col-md-4 col-xs-12 container">
                          <div class="region region-footer">
    <section id="block-footercopyrightblock" class="block block-block-content block-block-content1e0ab5e7-08a5-44bc-adf4-f443f650b130 clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p>© 2019 SAFS | Registered Charity No: 1098917<br />
Company Registration No: 4258543</p>
</div>
      
  </section>


  </div>

                  </div>

    <div class="footer-accred col-md-4 col-xs-12 container">
                          <div class="region region-footer-accred">
    <section id="block-footeraccredlinks" class="block block-block-content block-block-content6c2c1a46-bd99-4563-9437-320315ccbc4a clearfix">
  
    

      
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><div class="footer-accreds col-md-12"><img alt="Accreditations" class="img-responsive" src="/sites/safscare.org/files/images/footer/footer_accreds.png" title="Accreditations" /></div>
</div>
      
  </section>


  </div>

                  </div>

    <div class="footer-menu col-md-4 col-xs-12 container">
                          <div class="region region-footer-menu">
    <nav role="navigation" aria-labelledby="block-safs-footer-menu" id="block-safs-footer">
            
  <h2 class="visually-hidden" id="block-safs-footer-menu">Footer menu</h2>
  

        
      <ul class="menu menu--footer nav">
                      <li class="first">
                                        <a href="/equality-statement" data-drupal-link-system-path="node/56">Equality, Diversity &amp; Inclusivity Statement</a>
              </li>
                      <li>
                                        <a href="/privacy" data-drupal-link-system-path="node/3">Privacy Notice</a>
              </li>
                      <li class="last">
                                        <a href="/legal" data-drupal-link-system-path="node/2">Terms of Service</a>
              </li>
        </ul>
  

  </nav>

  </div>

                  </div>

  </div>
</footer>

  </div>

    
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/1","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","back_to_top":{"back_to_top_button_trigger":100,"back_to_top_prevent_on_mobile":true,"back_to_top_prevent_in_admin":false,"back_to_top_button_type":"image","back_to_top_button_text":"Back to top"},"google_analytics":{"account":"UA-139930016-1","trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"forms_has_error_value_toggle":1,"modal_animation":1,"modal_backdrop":"true","modal_focus_input":1,"modal_keyboard":1,"modal_select_text":1,"modal_show":1,"modal_size":"","popover_enabled":1,"popover_animation":1,"popover_auto_close":1,"popover_container":"body","popover_content":"","popover_delay":"0","popover_html":0,"popover_placement":"right","popover_selector":"","popover_title":"","popover_trigger":"click","tooltip_enabled":1,"tooltip_animation":1,"tooltip_container":"body","tooltip_delay":"0","tooltip_html":0,"tooltip_placement":"auto left","tooltip_selector":"","tooltip_trigger":"hover"},"superfish":{"superfish-main":{"id":"superfish-main","sf":{"animation":{"opacity":"show","height":"show"},"speed":"fast","dropShadows":false},"plugins":{"smallscreen":{"mode":"window_width","breakpoint":992,"type":"select","title":"- Main Menu -"},"supposition":true,"supersubs":true}}},"md_slider":{"1":{"full_width":1,"width":"960","height":"400","touch_swipe":1,"responsive":true,"videobox":0,"loop":true,"loadingbar":0,"bar_position":"bottom","show_next_prev_button":1,"enable_key_navigation":0,"auto_play":1,"pause_hover":1,"show_bullet":0,"bullet_position":"5","show_thumbnail":0,"thumbnail_position":"1","border_style":"0","dropshadow_style":0,"animation":"fade","delay":"8000","transtime":"800","thumb_style":"thumbnail","create_bg_imgstyle":1,"bg_style":"none","dmf_google":"","on_start":"","on_end":"","color_saved":"ff9900,CC0000","label":"Homepage Slider","id":"homepage_slider","is_new":true,"sls_desc":"","device_width":"460","device_enable":0,"submit":"Create","form_build_id":"form-bTXexr9JMFGnCLa4ehxmozyDha6SWkJGicH0Zo4umnA","form_token":"OIfkeS0VlxpLQ8ucTdx5fDqyMkny-3FfSCJlslhFztA","form_id":"md_slider_config","op":"Create","fullWidth":true,"transitionsSpeed":800,"enableDrag":true,"pauseOnHover":true,"autoPlay":true,"enableLoadingBar":false,"loadingBarPosition":"bottom","enableNextPrevButton":true,"enableKeyNavigation":false,"enableBullet":false,"bulletPosition":"5","enableThumbnail":false,"thumbnailPosition":"1","slideShowDelay":"8000","OnTransitionStart":"","OnTransitionEnd":"","styleShadow":0}},"inEffects":["bounceIn","bounceInDown","bounceInUp","bounceInLeft","bounceInRight","fadeIn","fadeInUp","fadeInDown","fadeInLeft","fadeInRight","fadeInUpBig","fadeInDownBig","fadeInLeftBig","fadeInRightBig","flipInX","flipInY","foolishIn","lightSpeedIn","puffIn","rollIn","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","twisterInDown","twisterInUp","swap","swashIn","tinRightIn","tinLeftIn","tinUpIn","tinDownIn","vanishIn","shuffleLetter"],"outEffects":["bombRightOut","bombLeftOut","bounceOut","bounceOutDown","bounceOutUp","bounceOutLeft","bounceOutRight","fadeOut","fadeOutUp","fadeOutDown","fadeOutLeft","fadeOutRight","fadeOutUpBig","fadeOutDownBig","fadeOutLeftBig","fadeOutRightBig","flipOutX","flipOutY","foolishOut","hinge","holeOut","lightSpeedOut","puffOut","rollOut","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","rotateDown","rotateUp","rotateLeft","rotateRight","swashOut","tinRightOut","tinLeftOut","tinUpOut","tinDownOut","vanishOut"],"user":{"uid":0,"permissionsHash":"645fd86d3a53ece89bd4b4f4f9d4f524d76fdefffa95c14205b810ea2de2abbf"}}</script>
<script src="/sites/safscare.org/files/js/js__CvNvuLA3A5M9IVpenYMDNtigzAusiUwbYh5uu3rAhE.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/sites/safscare.org/files/js/js_T1slKIFdixzZtosaS7H-aJaW-Ny8LEpRBDQ7iG7miCg.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="/sites/safscare.org/files/js/js_Oc4ZmMVchYtRJbholHMpISISxlQqguz87i5l9oW4cXY.js"></script>
<script src="//cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha256-nuL8/2cJ5NDSSwnKD8VqreErSWHtnEP9E7AySL+1ev4=" crossorigin="anonymous"></script>
<script src="/sites/safscare.org/files/js/js_a671aJzIui53P4xwdhJnr1aJCqZMHEHaLiZ-BBEBNls.js"></script>
<script src="/sites/safscare.org/files/js/js_yGcJhoSoBSf4NMb-FL6rwNjEhNT6g2LZvhxIXhsz7LQ.js"></script>

  </body>
</html>
