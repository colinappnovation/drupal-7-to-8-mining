<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/home" />
<link rel="shortlink" href="/node/1" />
<link rel="shortcut icon" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="1" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>Workplace ergonomics and human centred design | enricosmog ergonomic practitioners</title>
<style type="text/css" media="all">
@import url("https://www.enricosmog.com/modules/system/system.base.css?poxqlc");
@import url("https://www.enricosmog.com/modules/system/system.menus.css?poxqlc");
@import url("https://www.enricosmog.com/modules/system/system.messages.css?poxqlc");
@import url("https://www.enricosmog.com/modules/system/system.theme.css?poxqlc");
</style>
<style type="text/css" media="all">
@import url("https://www.enricosmog.com/modules/aggregator/aggregator.css?poxqlc");
@import url("https://www.enricosmog.com/modules/book/book.css?poxqlc");
@import url("https://www.enricosmog.com/modules/field/theme/field.css?poxqlc");
@import url("https://www.enricosmog.com/modules/node/node.css?poxqlc");
@import url("https://www.enricosmog.com/modules/poll/poll.css?poxqlc");
@import url("https://www.enricosmog.com/modules/search/search.css?poxqlc");
@import url("https://www.enricosmog.com/modules/user/user.css?poxqlc");
@import url("https://www.enricosmog.com/sites/all/modules/views/css/views.css?poxqlc");
@import url("https://www.enricosmog.com/sites/all/modules/ckeditor/css/ckeditor.css?poxqlc");
</style>
<style type="text/css" media="all">
@import url("https://www.enricosmog.com/sites/all/modules/ctools/css/ctools.css?poxqlc");
@import url("https://www.enricosmog.com/sites/all/libraries/superfish/css/superfish.css?poxqlc");
@import url("https://www.enricosmog.com/sites/all/libraries/superfish/css/superfish-smallscreen.css?poxqlc");
@import url("https://www.enricosmog.com/sites/all/libraries/superfish/style/light-blue.css?poxqlc");
</style>
<style type="text/css" media="screen">
@import url("https://www.enricosmog.com/sites/all/themes/adaptivetheme/adaptivetheme/at_core/css/at.layout.css?poxqlc");
@import url("https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/global.base.css?poxqlc");
@import url("https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/global.styles.css?poxqlc");
</style>
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/files/adaptivetheme/at_enrico_files/at_enrico.responsive.layout.css?poxqlc" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.custom.css?poxqlc" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.smalltouch.portrait.css?poxqlc" media="only screen and (max-width:320px)" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.smalltouch.landscape.css?poxqlc" media="only screen and (min-width:321px) and (max-width:580px)" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.tablet.portrait.css?poxqlc" media="only screen and (min-width:581px) and (max-width:768px)" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.tablet.landscape.css?poxqlc" media="only screen and (min-width:769px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://www.enricosmog.com/sites/enricosmog.com/themes/at_enrico/css/responsive.desktop.css?poxqlc" media="only screen and (min-width:1025px)" />
<style type="text/css" media="all">
@import url("https://www.enricosmog.com/sites/enricosmog.com/files/fontyourface/font.css?poxqlc");
@import url("https://www.enricosmog.com/sites/enricosmog.com/files/fontyourface/local_fonts/Helvetica_Thin-normal-normal/stylesheet.css?poxqlc");
@import url("https://www.enricosmog.com/sites/enricosmog.com/files/fontyourface/local_fonts/Meta_Plus-normal-normal/stylesheet.css?poxqlc");
</style>

<!--[if lt IE 9]>
<style type="text/css" media="screen">
@import url("https://www.enricosmog.com/sites/enricosmog.com/files/adaptivetheme/at_enrico_files/at_enrico.lt-ie9.layout.css?poxqlc");
</style>
<![endif]-->
<script type="text/javascript" src="https://www.enricosmog.com/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.enricosmog.com/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.enricosmog.com/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.enricosmog.com/misc/drupal.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/modules/google_analytics/googleanalytics.js?poxqlc"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-32910684-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="//mmlhosting.co.uk/sites/default/themes/mmlhosting/scripts/raphael-min.js"></script>
<script type="text/javascript" src="//mmlhosting.co.uk/sites/default/themes/mmlhosting/scripts/raphael-bubbles.js"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/sfautomaticwidth.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/sftouchscreen.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/sfsmallscreen.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/supposition.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/superfish.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/libraries/superfish/supersubs.js?poxqlc"></script>
<script type="text/javascript" src="https://www.enricosmog.com/sites/all/modules/contrib/superfish/superfish.js?poxqlc"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_enrico","theme_token":"7UQ6XuUcBXUZg3tKZ4CyMAtJr2U1_z23GJN_YuQjDDs","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"\/\/mmlhosting.co.uk\/sites\/default\/themes\/mmlhosting\/scripts\/raphael-min.js":1,"\/\/mmlhosting.co.uk\/sites\/default\/themes\/mmlhosting\/scripts\/raphael-bubbles.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfautomaticwidth.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/contrib\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/book\/book.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/poll\/poll.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/css\/superfish-smallscreen.css":1,"sites\/all\/libraries\/superfish\/style\/light-blue.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/global.base.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/global.styles.css":1,"public:\/\/adaptivetheme\/at_enrico_files\/at_enrico.responsive.layout.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.custom.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.smalltouch.portrait.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.smalltouch.landscape.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.tablet.portrait.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.tablet.landscape.css":1,"sites\/enricosmog.com\/themes\/at_enrico\/css\/responsive.desktop.css":1,"sites\/enricosmog.com\/files\/fontyourface\/font.css":1,"public:\/\/fontyourface\/local_fonts\/Helvetica_Thin-normal-normal\/stylesheet.css":1,"public:\/\/fontyourface\/local_fonts\/Meta_Plus-normal-normal\/stylesheet.css":1,"public:\/\/adaptivetheme\/at_enrico_files\/at_enrico.lt-ie9.layout.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/search\/node":true,"\/":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"fast"},"plugins":{"automaticwidth":true,"touchscreen":{"mode":"always_active"},"smallscreen":{"mode":"window_width","breakpointUnit":"px","title":"Main menu"},"supposition":true,"supersubs":true}}},"adaptivetheme":{"at_enrico":{"layout_settings":{"bigscreen":"two-sidebars-right","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:581px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:580px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});
//--><!]]>
</script>
<!--[if lt IE 9]>
<script src="https://www.enricosmog.com/sites/all/themes/adaptivetheme/adaptivetheme/at_core/scripts/html5.js?poxqlc"></script>
<![endif]-->
</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-1 node-type-page site-name-hidden atr-7.x-3.x atv-7.x-3.2+1-dev site-name-enricosmog-ergonomic-practitioners">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page">

    
    <div id="header-wrapper">
      <div class="container clearfix">
        <header id="header" class="clearfix" role="banner">

                      <!-- start: Branding -->
            <div id="branding" class="branding-elements clearfix">

              
                              <!-- start: Site name and Slogan -->
                <div class="element-invisible h-group" id="name-and-slogan">

                                      <h1 class="element-invisible" id="site-name"><a href="/" title="Home page" class="active">enricosmog ergonomic practitioners</a></h1>
                  
                  
                </div><!-- /end #name-and-slogan -->
              

            </div><!-- /end #branding -->
          
        <div class="region region-header"><div class="region-inner clearfix"><section id="block-block-35" class="block block-block odd first last block-count-1 block-region-header block-35" ><div class="block-inner clearfix">  
      <h2 class="block-title">enricoSmog practitioners</h2>
  
  <div class="block-content content"><div id="canvas">&nbsp;</div>
<h2><i>Chartered Ergonomic and Human Factor Specialists</i></h2></div>
  </div></section></div></div>
        </header>
      </div>
    </div>

          <div id="nav-wrapper">
        <div class="container clearfix">
          <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix odd first last block-count-2 block-region-menu-bar block-1" >  
      <h2 class="element-invisible block-title">Main menu</h2>
  
  <ul  id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-light-blue sf-total-items-8 sf-parent-items-3 sf-single-items-5"><li id="menu-144-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" title="Home" class="sf-depth-1 active">Home</a></li><li id="menu-145-1" class="middle even sf-item-2 sf-depth-1 sf-total-children-10 sf-parent-children-0 sf-single-children-10 menuparent"><a href="/assessments" title="Ergonomic assessments, Specialist ergonomic assessments and Advanced DSE assessments. Driver and vehicle assessments." class="sf-depth-1 menuparent">Assessments</a><ul><li id="menu-1414-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/advanced-dse" title="" class="sf-depth-2">Advanced DSE assessment</a></li><li id="menu-1418-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/Advanced-Workplace-Assessments" title="" class="sf-depth-2">Advanced Workplace Asssessments</a></li><li id="menu-1419-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/Agile_worker" title="" class="sf-depth-2">Agile Work risk assessments</a></li><li id="menu-1424-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/dse-assessments" title="" class="sf-depth-2">DSE assessments</a></li><li id="menu-1416-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/ergonomic-risk-assessment-driver-and-vehicle" title="Ergonomic assessment of driver and vehicle" class="sf-depth-2">Driver and vehicle ergonomic assessment</a></li><li id="menu-1415-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/DSE_Seating" title="" class="sf-depth-2">Ergonomic seating assessments</a></li><li id="menu-1421-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/industrial-ergonomic-risk-assessments" title="" class="sf-depth-2">Industrial ergonomic assessments</a></li><li id="menu-1422-1" class="middle even sf-item-8 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/mobile-dse-user" title="" class="sf-depth-2">Mobile DSE Worker and Field Worker</a></li><li id="menu-1423-1" class="middle odd sf-item-9 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/sit-stand-assessments" title="" class="sf-depth-2">Sit and Stand risk assessments</a></li><li id="menu-1420-1" class="last even sf-item-10 sf-depth-2 sf-no-children"><a href="https://www.enricosmog.com/assessments/atw" title="" class="sf-depth-2">Specialist DSE assessements</a></li></ul></li><li id="menu-146-1" class="middle odd sf-item-3 sf-depth-1 sf-total-children-9 sf-parent-children-0 sf-single-children-9 menuparent"><a href="/training" title="DSE assessor and ergonomic assessor training" class="sf-depth-1 menuparent">Training</a><ul><li id="menu-1254-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/training/dse" title="CIEHF accredited DSE Assessor training course covering all areas of display screen equipment use." class="sf-depth-2">CIEHF accredited DSE assessor course</a></li><li id="menu-1255-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/training/advanceddse" title="The first provider of the CIEHF accredited Advanced DSE assessor course in the world. This course is developed against the IEA advanced level core competencies." class="sf-depth-2">CIEHF accredited Advanced DSE assessor course</a></li><li id="menu-1431-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/Ergonomic_risk_assessor_course" title="A course designed to train staff that wish to become ergonomic risk assessors and ergonomic champions. The course is holistic and covers: physical ergonomics and cognitive ergonomics, withe emphasis on organisational and system ergonomics." class="sf-depth-2">Ergonomic risk assessor course</a></li><li id="menu-1256-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/training/ergoproductassessment" title="How to set up a workstation for a DSE user suffering from a specific condition? How to evaluate a DSE chair? Is it safe for a DSE user to sit on a ball chair?" class="sf-depth-2">How to select ergonomic products for workers with specific requirements</a></li><li id="menu-1426-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/stress_risk_assessment_course" title="Holistic approach to stress risk assessments, covering workplace design, wellness, resilience, task design" class="sf-depth-2">Holistic approach to Stress assessments</a></li><li id="menu-1427-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="/Human_Factor_approach_to_wellbeing" title="A holistic approach to Wellness covering:
What is wellbeing and wellness" class="sf-depth-2">Holistic approach to Wellness course</a></li><li id="menu-1413-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="/training/driver-ergo" title="How to undertake ergonomic assessments for drivers and vehicles" class="sf-depth-2">Driver and vehicle ergonomic assessor</a></li><li id="menu-1263-1" class="middle even sf-item-8 sf-depth-2 sf-no-children"><a href="/training/dse_workshop" title="Need a healthy workshop that covers physical health and keeping fit in the office, and mental health and stress management." class="sf-depth-2">Essential office wellbeing - Healthy DSE Workshop</a></li><li id="menu-1432-1" class="last odd sf-item-9 sf-depth-2 sf-no-children"><a href="/training/Advanced-Workplace-Assessment" title="Advanced Workplace assessments.  Future proofing and understanding new ways of working." class="sf-depth-2">Advanced Workplace assessments</a></li></ul></li><li id="menu-147-1" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="/workplace-solutions" title="Chartered Ergonomists, Psychologists and Designers desiging human centred environments where tasks can be performed, we take a holisitic view and thereby offer the most considered solutions that are often the most cost effective" class="sf-depth-1">Workplace Solutions</a></li><li id="menu-461-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/DSE%20FAQ" title="Display Screen Equipment (DSE) FAQ" class="sf-depth-1">FAQ</a></li><li id="menu-149-1" class="middle even sf-item-6 sf-depth-1 sf-no-children"><a href="/free-resources/articles" title="Free resources" class="sf-depth-1">Resources</a></li><li id="menu-483-1" class="middle odd sf-item-7 sf-depth-1 sf-no-children"><a href="/contact" title="" class="sf-depth-1">Contact</a></li><li id="menu-1253-1" class="last even sf-item-8 sf-depth-1 sf-total-children-8 sf-parent-children-0 sf-single-children-8 menuparent"><a href="https://www.enricosmog.com/About_us" title="" class="sf-depth-1 menuparent">About</a><ul><li id="menu-1417-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/about/anti-slavery" title="Anti-Slavery Policy" class="sf-depth-2">Anti-Slavery</a></li><li id="menu-1226-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/Clients" class="sf-depth-2">Clients</a></li><li id="menu-222-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/about/environmental-protection" title="environmental protection" class="sf-depth-2">Environmental Protection</a></li><li id="menu-223-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/about/equality-and-diversity" title="equality and diversity" class="sf-depth-2">Equality and Diversity</a></li><li id="menu-296-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/information" class="sf-depth-2">Information</a></li><li id="menu-1227-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="/Our-consultants" class="sf-depth-2">Our consultants</a></li><li id="menu-221-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="/about/data-protection" title="data protection" class="sf-depth-2">Privacy Policy</a></li><li id="menu-224-1" class="last even sf-item-8 sf-depth-2 sf-no-children"><a href="/about/terms-and-conditions" title="terms and conditions" class="sf-depth-2">Terms and Conditions</a></li></ul></li></ul>
  </nav></div>                            </div>
      </div>
    
    
    
    
    <div id="content-wrapper"><div class="container">
      <div id="columns"><div class="columns-inner clearfix">
        <div id="content-column"><div class="content-inner">

          
          <section id="main-content">

            
                          <header id="main-content-header" class="clearfix">

                                  <h1 id="page-title">Workplace ergonomics and human centred design</h1>
                
                
              </header>
            
                          <div id="content">
                <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-1" class="node node-page node-promoted node-sticky article odd node-full clearfix" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even"><div><img alt="" src="https://www.enricosmog.com/sites/enricosmog.com/files/esp_2.png" style="height:184px; width:342px" /></div>
<h2>enricoSmog ergonomic and wellness practitioners</h2>
<p><em>As </em><em>Chartered Ergonomists and Human Factor Specialists we aim to m</em><em>aximise positive work experience for UK and International clients using a human centred holistic approach to wellbeing, engagement, work and workplace design.</em></p>
<p>In a rapid changing business environment, the need for agility and adaptability is stronger than ever. Businesses require resilient workforces that are equipped with tools, skills and the mindset to deliver positive result for their customers.</p>
<p>Future work, advanced workplaces, physical and mental stress, diversity and inclusivity need to be considered in the context of holistic approaches and not as silos.</p>
<p><strong>Assessments</strong> – individual, team, department or organisation.  We have expertise in many types of tasks performed in a multitude of workplaces for workers with physical or cognitive ill health</p>
<p><strong>Consultancy</strong> – wellbeing, human centred design approaches, agile working, changes in the way of working, engagement, creating inclusive environments that do not discriminate</p>
<p><strong>Learning and development </strong>– development of staff through specific courses and seminars, through game playing, or coaching 1:1 or in teams, facilitation, if you want the best positive experience then train with us</p>
<p><strong>Workplace solutions</strong> - workplaces and tasks are undergoing digital transformation where expectations of success run high,  picking the right adviser is crucial if you are to avoid high costs, loss of reputation, formation of silos and risk of employee disengagement</p>
<p>Today, the goal of every project we are involved in is to make a positive impact to peoples’ experiences.</p>
<p><span style="font-size:10px">(C) enricoSmog ergonomic practitioners</span></p>
</div></div></div>  </div>

  
  
  </article>

  </div>              </div>
            
            
            
          </section>

          
        </div></div>

        <div class="region region-sidebar-first sidebar"><div class="region-inner clearfix"><div id="block-search-form" class="block block-search odd first block-count-4 block-region-sidebar-first block-form"  role="search"><div class="block-inner clearfix">  
      <h2 class="block-title">Search this site</h2>
  
  <div class="block-content content"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-lKn_FSm3jkhe59aq2SK-h03oTZahbcvndiDNgQDhGq0" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div></div><section id="block-block-2" class="block block-block even block-count-5 block-region-sidebar-first block-2" ><div class="block-inner clearfix">  
      <h2 class="block-title">Contact</h2>
  
  <div class="block-content content"><h2><a href="mailto:info@enricosmog.com" style="font-size: larger;">Email Us</a><br />
Tel: 01747 871868</h2>
</div>
  </div></section><section id="block-block-31" class="block block-block odd block-count-6 block-region-sidebar-first block-31" ><div class="block-inner clearfix">  
      <h2 class="block-title">News and events</h2>
  
  <div class="block-content content"><h2><a href="http://www.enricosmog.com/News_and_Events">Latest</a></h2>
</div>
  </div></section><section id="block-block-33" class="block block-block even last block-count-7 block-region-sidebar-first block-33" ><div class="block-inner clearfix">  
      <h2 class="block-title">Clients</h2>
  
  <div class="block-content content"><p>Who we work with</p>
</div>
  </div></section></div></div>        
      </div></div>
    </div></div>

    
          <div id="footer-wrapper">
        <div class="container clearfix">
          <footer id="footer" class="clearfix" role="contentinfo">
            <div class="region region-footer"><div class="region-inner clearfix"><div id="block-views-buttons-block" class="block block-views no-title odd first last block-count-8 block-region-footer block-buttons-block" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><div class="view view-buttons view-id-buttons view-display-id-block view-dom-id-dd802a704b2ef9f9168cdaaf28d6e3a1">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-button">        <div class="field-content"><a href="/What_is_a_CIEHF_accredited_course"><img class="image-style-medium" src="https://www.enricosmog.com/sites/enricosmog.com/files/styles/medium/public/CIEHFRec.jpg?itok=syo6bRia" width="220" height="87" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/node/364">CIEHF recognised course</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-button">        <div class="field-content"><a href="/What_is_an_OHSCR_registered_consultant"><img class="image-style-medium" src="https://www.enricosmog.com/sites/enricosmog.com/files/styles/medium/public/OSHCR_1.jpg?itok=KpaK00fE" width="220" height="84" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/node/363">OSHCR registered consultant</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-button">        <div class="field-content"><a href="https://enricosmog.com/What_is_a_CIEHF_accredited_consultancy"><img class="image-style-medium" src="https://www.enricosmog.com/sites/enricosmog.com/files/styles/medium/public/CIEHFReg.jpg?itok=mdO6AIjh" width="220" height="87" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/node/362">CIEHF Registered Consultancy</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div>
  </div></div></div></div>                      </footer>
        </div>
      </div>
    
  </div>
</div>
  </body>
</html>
