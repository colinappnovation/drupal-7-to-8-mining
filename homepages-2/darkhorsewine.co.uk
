<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie10" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NG2GKZ6');</script>
    <!-- End Google Tag Manager -->

    <link rel="alternate" href="https://www.darkhorsewine.com/" hreflang="en-us"/>
  <link rel="alternate" href="https://www.darkhorsewine.com/" hreflang="x-default"/>
  <link rel="alternate" href="https://www.darkhorsewine.co.uk/" hreflang="en-gb"/>
  <link rel="alternate" href="https://www.darkhorsewine.de/" hreflang="de-de"/>
  <link rel="alternate" href="http://www.darkhorsewine.ie/" hreflang="en-ie"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.darkhorsewine.co.uk/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="Dark Horse Wine RSS" href="https://www.darkhorsewine.co.uk/rss.xml" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.darkhorsewine.co.uk/" />
<link rel="shortlink" href="https://www.darkhorsewine.co.uk/" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="Dark Horse Wine" />
<meta property="og:url" content="https://www.darkhorsewine.co.uk/" />
<meta property="og:title" content="Dark Horse Wine" />
<meta property="og:image" content="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/fire-horse.jpg" />
<meta property="og:image:url" content="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/fire-horse.jpg" />
<meta property="og:image:secure_url" content="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/fire-horse.jpg" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@darkhorsewine" />
<meta name="twitter:url" content="https://www.darkhorsewine.co.uk/" />
<meta name="twitter:title" content="Dark Horse Wines" />
<meta name="twitter:description" content="Our bold wines are meticulously crafted to outperform their price tag." />
<meta name="twitter:image" content="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/fire-horse.jpg" />

    <title>Dark Horse Wine | California Wines by Beth Liston.</title>

      <link rel="apple-touch-icon" sizes="57x57" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/manifest.json">
    <link rel="mask-icon" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/favicons/mstile-144x144.png">

  <style type="text/css" media="all">
@import url("https://www.darkhorsewine.co.uk/modules/system/system.base.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/modules/system/system.messages.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/modules/system/system.theme.css?pzmvxs");
</style>
<style type="text/css" media="all">
@import url("https://www.darkhorsewine.co.uk/sites/all/modules/date/date_api/date.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/modules/field/theme/field.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/modules/node/node.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/modules/user/user.css?pzmvxs");
@import url("https://www.darkhorsewine.co.uk/sites/all/modules/views/css/views.css?pzmvxs");
</style>
<link type="text/css" rel="stylesheet" href="https://cloud.typography.com/7260954/7847752/css/fonts.css" media="all" />
<link type="text/css" rel="stylesheet" href="/BrandSDK-v1.1/BrandSDK.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/css/main.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.darkhorsewine.co.uk/sites/all/themes/da/css/da.min.css" media="all" />
  <script type="text/javascript" src="https://www.darkhorsewine.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.darkhorsewine.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.darkhorsewine.co.uk/misc/drupal.js?pzmvxs"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var $ = jQuery, BASE_URL = "https://www.darkhorsewine.co.uk/", THEME_PATH = "https://www.darkhorsewine.co.uk/sites/all/themes/da/";
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"da","theme_token":"VujnwxZauMStL1M4TzDClmEVanbi-PB3pwovHb1YIVM","js":{"sites\/all\/themes\/da\/js\/gallo.gateway.js":1,"0":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"1":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"https:\/\/cloud.typography.com\/7260954\/7847752\/css\/fonts.css":1,"\/BrandSDK-v1.1\/BrandSDK.min.css":1,"https:\/\/www.darkhorsewine.co.uk\/sites\/all\/themes\/da\/css\/main.min.css":1,"https:\/\/www.darkhorsewine.co.uk\/sites\/all\/themes\/da\/css\/da.min.css":1,"sites\/all\/themes\/da\/css\/system.menus.css":1}}});
//--><!]]>
</script>

    <script type="text/javascript" src="/BrandSDK-v1.1/BrandSDK.min.js" data-appKey="a4fe9f8e-eba2-46b8-bed1-ffd46439d6a4"></script>
    <script type="text/javascript">
        $ = window.jQuery;
    </script>

	<script>
		(function(document) {
			var a = document.createElement('script');
			a.src = 'https://static.constant.co/shopping-tools/darkhorsewine/constant-co.js?' +
				Math.floor(Date.now() / 600000);
			var b = document.querySelector('script');
			b.parentNode.insertBefore(a, b);
		})(document);
	</script>

</head>
<body class="html front not-logged-in no-sidebars page-node node-home" >

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG2GKZ6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<header id="header-main">
	<a href="https://www.darkhorsewine.co.uk/" class="icon-logo" data-gtm="Nav > Main" data-gtm-value="Home"></a>
	
	<nav id="header">
		<div class="toggle">
			<a class="icon-hamburger" data-gtm="Nav > Main > Mobile" data-gtm-value="Open"></a>
			<a class="icon-down-arrow" data-gtm="Nav > Main > Mobile" data-gtm-value="Back"></a>
			<a class="icon-close" data-gtm="Nav > Main > Mobile" data-gtm-value="Close"></a>
		</div>
		<div class="overlay"></div>
					<ul class="main-nav">
									<li>
						<a href="https://www.darkhorsewine.co.uk/wines" data-gtm="Nav > Main" data-gtm-value="Our Wines">Our Wines</a>
													<ul class="subnav">
								<li class="desktop-hide">
									<a href="https://www.darkhorsewine.co.uk/wines" data-gtm="Nav > Main" data-gtm-value="View All Wines">
										View All Wines									</a>
								</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/cabernet-sauvignon" data-gtm="Nav > Main" data-gtm-value="Cabernet Sauvignon">Cabernet Sauvignon</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/merlot" data-gtm="Nav > Main" data-gtm-value="Merlot">Merlot</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/malbec" data-gtm="Nav > Main" data-gtm-value="Malbec">Malbec</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/zinfandel" data-gtm="Nav > Main" data-gtm-value="Zinfandel">Zinfandel</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/chardonnay" data-gtm="Nav > Main" data-gtm-value="Chardonnay">Chardonnay</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/sauvignon-blanc" data-gtm="Nav > Main" data-gtm-value="Sauvignon Blanc">Sauvignon Blanc</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/pinot-grigio" data-gtm="Nav > Main" data-gtm-value="Pinot Grigio">Pinot Grigio</a>
									</li>
																	<li>
										<a href="https://www.darkhorsewine.co.uk/wines/rose" data-gtm="Nav > Main" data-gtm-value="Rosé">Rosé</a>
									</li>
															</ul>
											</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/about-us" data-gtm="Nav > Main" data-gtm-value="About Us">About Us</a>
											</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/purchase" data-gtm="Nav > Main" data-gtm-value="Where to Buy">Where to Buy</a>
											</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/contact" data-gtm="Nav > Main" data-gtm-value="Contact Us">Contact Us</a>
											</li>
							</ul>
			</nav>

</header>

<div id="main"><div id="block-system-main" class="block block-system">

    
  <div class="content">
    
    <div id="cards" class="home">
		
		
		        <div data-anchor="" data-theme="dark" class="active card mobile-gradient-cover video"
             style="background-image: url('https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/DKH_WEB_BG_21.10.19.jpg'); background-position: 50%;" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/DKH_WEB_BG_21.10.19.jpg?itok=6qoeipqa" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/DKH_WEB_BG_21.10.19.jpg">
            <!-- Carve your own path graphic -->
            <img class="desktop-overlay" src="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/FYOP-WHITE%20V2_0.png"/>
            <img class="mobile-overlay" src="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/FYOP-WHITE%20V2_1.png"/>
			
            <div class="content">
				                            </div>


            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
		
		
		        <div data-anchor="" data-theme="dark" class="card standard" style="background-image: url('https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/PL-15_FB_Cabernet%20Dark.jpg'); background-position: 50%;" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/PL-15_FB_Cabernet%20Dark.jpg?itok=Qw46sxuK" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/PL-15_FB_Cabernet%20Dark.jpg">
			
			            <div class="content">
                <h2>Sweat The Details</h2>
                <p>Our bold wines are meticulously crafted to outperform their price tag.</p>
				                    <a href="/wines" class="cta-text" data-gtm="Home > Card > Link" data-gtm-value="What we're pouring">
						What we're pouring                        <span class="icon-down-arrow"></span>
                    </a>
				            </div>
						                <a href="/wines" class="cover-link" data-gtm="Home > Card > Link" data-gtm-value="What we're pouring"></a>
			
            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
		
		        <div data-anchor="follow" data-theme="light" class="card follow" style="background-image: url('https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/smoke.jpg'); background-position: 50%;" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/smoke.jpg?itok=4TA5FD83" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/smoke.jpg">

            <!-- Title of the card -->
            <div class="content">
                <h2 class="script">Follow Us</h2>

                <!-- Grab the social links from global content -->
                <div class="socials">
					                        <a class="icon-instagram round" target="_blank" href="https://www.instagram.com/darkhorsewineuk/"
                           data-gtm="Home > Card > Follow"
                           data-gtm-value="Instagram"></a>
										                        <a class="icon-twitter round" target="_blank" href="https://twitter.com/darkhorsewine"
                           data-gtm="Home > Card > Follow"
                           data-gtm-value="Twitter"></a>
										                        <a class="icon-facebook round" target="_blank" href="https://www.facebook.com/DarkHorseWine/"
                           data-gtm="Home > Card > Follow"
                           data-gtm-value="Twitter"></a>
					                </div>
            </div>

            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
		
				
		        <div data-anchor="spirit" data-theme="dark" class="card standard" style="background-image: url('https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/spirit.jpg'); background-position: 16%;" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/spirit.jpg?itok=Wx01_YY6" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/spirit.jpg">

            <div class="content">
				                <h2>The Spirit Behind Dark Horse</h2>
                <p>Trace the bold originality of Dark Horse to its roots, and you'll find our fearless winemaker, Beth Liston.</p>
				                    <a href="/about-us#beth" class="cta-text" data-gtm="Home > Card > Link" data-gtm-value="Meet Beth">
						Meet Beth                        <span class="icon-down-arrow"></span>
                    </a>
				            </div>
			
						                <a href="/about-us#beth" class="cover-link" data-gtm="Home > Card > Link" data-gtm-value="Meet Beth"></a>
			
            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
		
				
		        <div data-anchor="where" data-theme="dark" class="card overlay" style="background-image: url('https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/searching.jpg'); background-position: 50%;" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/searching.jpg?itok=Qxz9o-oc" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/searching.jpg">

            <!-- everythings coming up rose graphic -->
            <img class="desktop-overlay" src="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/search-overlay.png"/>
            <img class="mobile-overlay" src="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/search-overlay-mobile.png"/>

            <div class="content">
				                <p>Find Dark Horse at your favourite retailer.</p>
				
				                    <a href="/purchase" class="cta-text" data-gtm="Home > Card > Link" data-gtm-value="Where to buy">
						Where to buy                        <span class="icon-down-arrow"></span>
                    </a>
				            </div>
			
						                <a href="/purchase" class="cover-link" data-gtm="Home > Card > Link" data-gtm-value="Where to buy"></a>
			
            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
		
		        <div data-anchor="about" data-theme="dark" class="card end-cap">

            <!-- everythings coming up rose graphic -->
            <a class="col optimize-img" href="https://www.darkhorsewine.co.uk/wines" style="background-image: url(https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/PL-08_FB_Wine%20on%20a%20Bridge.jpg);" data-gtm="Home > Card > Link" data-gtm-value="Our Wines"  data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/PL-08_FB_Wine%20on%20a%20Bridge.jpg?itok=fsJUpLIV" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/PL-08_FB_Wine%20on%20a%20Bridge.jpg">
                <div class="content">
                    <h2>Our Wines</h2>
                </div>
                <div class="overlay"></div>
            </a>

            <a class="col optimize-img" href="/about-us" style="background-image: url(https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/about.jpg);" data-gtm="Home > Card > Link" data-gtm-value="About Us" data-card-bg-mobile="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/styles/card_mobile/public/about.jpg?itok=0DFJ5Dk2" data-card-bg="https://s3.amazonaws.com/darkhorsewine.co.uk/production/s3fs-public/about.jpg">
                <div class="content">
                    <h2>About Us</h2>
                </div>
                <div class="overlay"></div>
            </a>

            <!-- The down arrow that continues to the next card -->
            <div class="next-block"></div>
        </div>
    </div>


<footer id="footer-nav">
	<div class="logo">
		<a class="icon-logo" href="https://www.darkhorsewine.co.uk/" alt="Dark Horse Wine - Home Page" title="Dark Horse Wine - Home Page"
		   data-gtm="Nav > Footer"
		   data-gtm-value="Home"></a>
	</div>
	
	<div class="column column-1">
					<h3>Company</h3>
			<ul>
									<li>
						<a href="https://www.darkhorsewine.co.uk/wines" data-gtm="Nav > Footer" data-gtm-value="Our Wines">
							Our Wines						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/about-us" data-gtm="Nav > Footer" data-gtm-value="About Us">
							About Us						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/purchase" data-gtm="Nav > Footer" data-gtm-value="Where To Buy">
							Where To Buy						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/contact" data-gtm="Nav > Footer" data-gtm-value="Contact Us">
							Contact Us						</a>
					</li>
							</ul>
			</div>
	
	<div class="column column-2">
					<h3>Legal</h3>
			<ul>
									<li>
						<a href="https://www.darkhorsewine.co.uk/legal/terms-of-use" data-gtm="Nav > Footer" data-gtm-value="Terms of Use">
							Terms of Use						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/legal/privacy" data-gtm="Nav > Footer" data-gtm-value="Privacy Policy">
							Privacy Policy						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/legal/cookie-policy" data-gtm="Nav > Footer" data-gtm-value="Cookie Policy">
							Cookie Policy						</a>
					</li>
									<li>
						<a href="https://www.darkhorsewine.co.uk/legal/trademarks" data-gtm="Nav > Footer" data-gtm-value="Trademarks">
							Trademarks						</a>
					</li>
							</ul>
			</div>
	
	<div class="socials social-links">
					<a class="round instagram icon-instagram" target="_blank" href="https://www.instagram.com/darkhorsewineuk/"
			   data-gtm="Nav > Footer > Social"
			   data-gtm-value="Instagram"></a>
							<a class="round twitter icon-twitter" target="_blank" href="https://twitter.com/darkhorsewine"
			   data-gtm="Nav > Footer > Social"
			   data-gtm-value="Twitter"></a>
							<a class="round facebook icon-facebook" target="_blank" href="https://www.facebook.com/DarkHorseWine/"
			   data-gtm="Nav > Footer > Social"
			   data-gtm-value="Facebook"></a>
				<p>&copy;2019 Dark Horse Wines, Modesto, CA. All rights reserved.</p>
        <p>Use of this site is subject to
            <a href="https://www.darkhorsewine.co.uk/legal/terms-of-use">Terms of Use</a>,
            <a href="https://www.darkhorsewine.co.uk/legal/privacy">Privacy Policy</a>,
            <a href="https://www.darkhorsewine.co.uk/legal/cookie-policy">Cookie Policy</a>, and
            <a href="https://www.darkhorsewine.co.uk/legal/trademarks">Trademark</a>.</p>
	</div>
	
	<a class="to-top"
	   data-gtm="Nav > Footer"
	   data-gtm-value="Back to Top">
		<span class="icon-down-arrow"></span>
		<span>TOP</span>
	</a>

</footer>
  </div>
</div>


<script type="text/javascript" src="https://www.darkhorsewine.co.uk/sites/all/themes/da/js/gallo.gateway.js?pzmvxs"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(){
if (window.fpglobals && window.fpglobals.gateway) return;
window.fpglobals = {
	baseurl: '/sites/all/themes/da',
	cookeDomain: 'https://www.darkhorsewine.co.uk/' 
};
window.fpglobals.gateway = new Gallo_Gateway({
	url: fpglobals.cookeDomain + '/agegate',
	cookie: 'dkh-visited'
});
window.fpglobals.gateway.init();
})();
//--><!]]>
</script>

<div class="mobile-warning">
    <div class="rotate">
        <img src="https://www.darkhorsewine.co.uk/sites/all/themes/da/img/rotate.png">
        <h3>Please rotate your phone to portrait.</h3>
    </div>
</div>
</div>  <!-- end #main  -->

<script src="https://www.darkhorsewine.co.uk/sites/all/themes/da/js/require-jquery.js"></script>
<script>
    require.config({
        //urlArgs: "bust=" + (new Date()).getTime(),
        baseUrl: 'https://www.darkhorsewine.co.uk/sites/all/themes/da/js',
        paths: {
            main: 'main',
            da: 'da',
            //brandsdk: location.protocol + '//' + location.hostname + '/BrandSDK-v1.1/BrandSDK.min'
        }
    });
    var oldRequireCreateNodeFunc = requirejs.createNode;
    requirejs.createNode = function(e, t, i) {
        var node = oldRequireCreateNodeFunc(e, t, i);
//        if (t == 'brandsdk')
//            $(node).attr('data-appKey', 'a4fe9f8e-eba2-46b8-bed1-ffd46439d6a4');
        return node;
    };
    requirejs(['main', 'da']);
</script>


<!--<script src="--><!--"></script>-->


<script type="text/javascript">
    $(document).ready(function () {
      setTimeout(addEditButton, 500);
  });

  var addEditButton = function () {
      $ = window.jQuery;
      if ($('#admin-menu-menu')) {
          var menu = $('#admin-menu-menu');
          $('#admin-menu-menu').append('<li class="admin-menu-toolbar-category"><a href="https://www.darkhorsewine.co.uk/node/1/edit">Edit This Page</a></li>');
      } else {
          setTimeout(addEditButton, 500);
      }
  }
</script>

<script type="text/javascript">
    $(document).ready(function () {

    });
</script>
<!--<script type="text/javascript" data-appKey="a4fe9f8e-eba2-46b8-bed1-ffd46439d6a4" src="/BrandSDK-v1.1/BrandSDK.min.js"></script>-->

<!-- Schema Data -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Organization",
    "name": "Dark Horse Wine",
    "url": "https://www.darkhorsewine.co.uk/",
    "logo": "https://www.darkhorsewine.co.uk/resources/images/wheretobuy/wtb-logo.png",
    "contactPoint": {
        "@type": "ContactPoint",
        "telephone": "+18553629463",
        "contactType": "Customer service"
    }
}
</script>

</body>
</html>
