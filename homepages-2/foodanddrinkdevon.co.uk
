<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
	<link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.foodanddrinkdevon.co.uk/sites/all/themes/fooddrinkdevon/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.foodanddrinkdevon.co.uk/" />
<link rel="shortlink" href="https://www.foodanddrinkdevon.co.uk/" />
	<title>Food Drink Devon | Good Quality Local Food & Drink</title>
	<link type="text/css" rel="stylesheet" href="https://www.foodanddrinkdevon.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.foodanddrinkdevon.co.uk/sites/default/files/css/css_oU0ZFN2PX08--TYCPs3lKtUvuLCAuBveyUtRLySe-GQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.foodanddrinkdevon.co.uk/sites/default/files/css/css_gS7VYlXnin44MIkLgwrkujQfWoUMnPDc9AWgnv9nxk4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.5/dist/css/bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.foodanddrinkdevon.co.uk/sites/default/files/css/css_Xzwpw4qY4IGwZ9WX5F2tRS4o1wWFM6poF26ovU59TJ0.css" media="all" />
<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" media="all" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="https://fonts.googleapis.com/css?family=Rokkitt:700|Open+Sans|Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
	<script>window.cookieconsent_options = {"message":"We use cookie files to improve site functionality and personalisation. By continuing to use Food &amp; Drink Devon, you accept our","dismiss":"Close","learnMore":"privacy &amp; cookie policy","link":"/privacy-cookie-policy","theme":false};</script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-274 node-type-homepage role-anonymous-user">
	<div id="skip-link">
		<a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
	</div>
		<div id="site-wrap">
	<div class="container">
    <div id="nav">
        <nav class="navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
								<div id="navbar" class="navbar-collapse collapse">	
				
											  <div class="region region-user-menu">
    <section id="block-system-user-menu" class="block block-system block-menu clearfix">

      
  <ul id="user-menu" class="nav navbar-nav"><li class="first leaf"><a href="/contact">Contact</a></li>
<li class="last leaf"><a href="/user/login">Member Login</a></li>
</ul>
</section>
  </div>
										
											  <div class="region region-navigation">
    <section id="block-system-main-menu" class="block block-system block-menu clearfix">

      
  <ul id="main-menu" class="nav navbar-nav"><li class="first expanded dropdown"><a href="/about-us" id="menu-about-us" data-target="#" class="dropdown-toggle disabled" data-toggle="dropdown">About us</a><ul class="dropdown-menu"><li class="first leaf"><a href="/about-us/about-devon">About Devon</a></li>
<li class="leaf"><a href="/about-us/meet-team">Meet The Team</a></li>
<li class="leaf"><a href="/about-us/partnerships">Partnerships</a></li>
<li class="leaf"><a href="/about-us/testimonials">Testimonials</a></li>
<li class="last leaf"><a href="/about-us/download-guide">Download Guide</a></li>
</ul></li>
<li class="expanded dropdown"><a href="/members" id="menu-our-members" data-target="#" class="dropdown-toggle disabled" data-toggle="dropdown">Members</a><ul class="dropdown-menu"><li class="first leaf"><a href="/members/month">This Months Members</a></li>
<li class="leaf"><a href="/offers">Offers</a></li>
<li class="leaf"><a href="/vacancies">Vacancies</a></li>
<li class="last leaf"><a href="/competitions">Competitions</a></li>
</ul></li>
<li class="expanded dropdown"><a href="/news" id="menu-news" data-target="#" class="dropdown-toggle disabled" data-toggle="dropdown">News</a><ul class="dropdown-menu"><li class="first leaf"><a href="/news/activity">FDD Activity</a></li>
<li class="leaf"><a href="/gallery">Gallery</a></li>
<li class="last leaf"><a href="/news/press-releases">Press Releases</a></li>
</ul></li>
<li class="leaf"><a href="/whats-on" id="menu-whats-on">What&#039;s on</a></li>
<li class="expanded dropdown"><a href="/2019-awards" id="menu-awards" data-target="#" class="dropdown-toggle disabled" data-toggle="dropdown">Awards</a><ul class="dropdown-menu"><li class="first leaf"><a href="/awards/entry-form">2019 Awards Entry Form</a></li>
<li class="leaf"><a href="/awards/2019-awards">Awards News</a></li>
<li class="leaf"><a href="/awards/award-sponsors">Award Sponsors</a></li>
<li class="leaf"><a href="/awards/awards-video">Awards Video</a></li>
<li class="leaf"><a href="/awards/become-award-sponsor">Become an Award Sponsor</a></li>
<li class="leaf"><a href="/awards/results-2019">Results 2019</a></li>
<li class="last leaf"><a href="/awards/results-2018">Results 2018</a></li>
</ul></li>
<li class="expanded dropdown"><a href="/recipes" data-target="#" class="dropdown-toggle disabled" data-toggle="dropdown">Recipes</a><ul class="dropdown-menu"><li class="first last leaf"><a href="/devon-cook-book">The Devon Cook Book</a></li>
</ul></li>
<li class="leaf"><a href="http://eepurl.com/duqUqn" class="highlight-nav">Sign up</a></li>
<li class="last leaf"><a href="/become-member-food-drink-devon" class="highlight-nav">Become a Member</a></li>
</ul>
</section>
  </div>
										
					
																																				

					
				</div>
				 
            </div>
        </nav>
    </div>
</div>	<div class="wrap">
		<div id="main-container">
			<div id="banner" class="banner-home" role="banner">
							<header id="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/" id="logo">
                                <img src="/sites/all/themes/fooddrinkdevon/img/layout/logo.svg" alt="Food Drink Devon" />
                            </a>
                        </div>
                    </div>
                </div>
			</header>				                <div class="slick-slider">
                    <div class="carousel-item">
                        <img src="/sites/all/themes/fooddrinkdevon/img/banner/cookbookchristmas.jpg" class="banner-image" alt="" />
                        <div class="banner-text">
                            <div class="container">
                                <div class="heading"><a href="/devon-cook-book">Out Now! <span>The Devon Cook Book</span></a></div>
                                <a href="/devon-cook-book" class="btn btn-white hidden-xs hidden-sm">Buy It Here</a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="/sites/all/themes/fooddrinkdevon/img/banner/members.jpg" class="banner-image" alt="" />
                        <div class="banner-text">
                            <div class="container">
                                <div class="heading"><a href="/members/month">This Months <span>New Members</span></a></div>
                                <a href="/members/month" class="btn btn-white hidden-xs hidden-sm">Find Out More</a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="/sites/all/themes/fooddrinkdevon/img/banner/news.jpg" class="banner-image" alt="" />
                        <div class="banner-text">
                            <div class="container">
                                <div class="heading"><a href="/news">This Months <span>News</span></a></div>
                                <a href="/news" class="btn btn-white hidden-xs hidden-sm">Find Out More</a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="/sites/all/themes/fooddrinkdevon/img/banner/recipes.jpg" class="banner-image" alt="" />
                        <div class="banner-text">
                            <div class="container">
                                <div class="heading"><a href="/recipes/all/month">This Months <span>Recipes</span></a></div>
                                <a href="/recipes/all/month" class="btn btn-white hidden-xs hidden-sm">Find Out More</a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="/sites/all/themes/fooddrinkdevon/img/banner/competitions.jpg" class="banner-image" alt="" />
                        <div class="banner-text">
                            <div class="container">
                                <div class="heading"><a href="/competitions">This Months <span>Competitions</span></a></div>
                                <a href="/competitions" class="btn btn-white hidden-xs hidden-sm">Find Out More</a>
                            </div>
                        </div>
                    </div>
                </div>
				<div id="homepage-search-wrap">
					<div class="heading">Search All Members:</div>
					<div class="container">
						<form id="homepage-search" action="/search.php" method="get">
							<div class="search-text">
								<input class="form-control form-text" type="text" name="query" value="" size="30" maxlength="128" placeholder="Member name..." />
							</div>
							<div class="search-dropdown">
								<select id="categories-dropdown" class="form-control form-select not-chosen" name="categories">
									<option value="" selected="selected">Category</option>
									<div class="view view-category-select-lists view-id-category_select_lists view-display-id-block view-dom-id-d10140408e94e7ed47b51ae0cc5f6d16">
        
  
  
      <div class="view-content">
        <div>
    <option value="1">Eating Out</option>  </div>
  <div>
    <option value="213">Where To Buy</option>  </div>
  <div>
    <option value="27">Foodie Leisure &amp; Pleasure</option>  </div>
  <div>
    <option value="17">Food Producers</option>  </div>
  <div>
    <option value="19">Drinks &amp; Beverages Producers</option>  </div>
  <div>
    <option value="30">Food Drink Devon Supporters</option>  </div>
    </div>
  
  
  
  
  
  
</div>								</select>
							</div>
							<div class="search-dropdown">
								<select id="territory-dropdown" class="form-control form-select not-chosen" name="territory">
									<option value="" selected="selected">Territory</option>
									<option value="devon_east">East Devon</option>
									<option value="devon_mid">Mid Devon</option>
									<option value="devon_north">North Devon</option>
									<option value="devon_south">South Devon</option>
									<option value="devon_west">West Devon</option>
								</select>
							</div>
							<button type="submit" name="" value="" class="form-submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				<!--<div id="play-icon">
					<a href="https://player.vimeo.com/video/184837370" class="fancybox fancybox.iframe">
						<div class="play-wrap">
							<i class="fa fa-play" aria-hidden="true"></i>
						</div>
					</a>
				</div>-->
			</div>
			<div class="container" role="main">
				<div class="row">
					<div class="section">
						<div class="col-md-12">
																					<a id="main-content"></a>
																																																																																				</div>
						  <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div class="field field-name-field-page-banner field-type-field-collection field-label-hidden">
  
          <div class="view view-page-banner-field-collection view-id-page_banner_field_collection view-display-id-default view-dom-id-156ec47bf09851a1feb57f0307b165dd">
        
  
  
      <div class="view-content">
        <div>
      
          <div class="col-md-12">
	<a href="">
		
	</a>
</div>    </div>
    </div>
  
  
  
  
  
  
</div>    
</div><div class="field field-name-field-quicklinks field-type-field-collection field-label-hidden">
  
          <div class="view view-quicklink-field-collection view-id-quicklink_field_collection view-display-id-default view-dom-id-cf2d3de3f8a2130ceefe52223ed15de2">
        
  
  
      <div class="view-content">
        <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/categories/1" class="quicklink">
		<div class="quicklink-text">
			<h2>Eating Out</h2>
			<p>Enjoy the very best the county can offer at one of our many fine restaurants which source Devon’s best authentic produce for their fabulous dishes.</p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/salutation1-42-credit_0.jpg?itok=DTBz5hRp" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
  <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/categories/213" class="quicklink">
		<div class="quicklink-text">
			<h2>Where to Buy</h2>
			<p>Visit Devon’s finest Farm and Independent Shops and Suppliers who stock a fabulous range of authentic Devon produce. </p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/delis_farm_shops_photo.jpg?itok=n7lg4t5j" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
  <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/categories/27" class="quicklink">
		<div class="quicklink-text">
			<h2>Where to Stay &amp; Things to Do </h2>
			<p>Relax in comfort and enjoy the fantastic settings of many of Devon’s finest accommodation establishments all of whom use the County’s greatest products. Locate Venues offering smaller food related experiences to larger conference and party facilities.</p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/winter_wonderland_dbb.jpg?itok=C-g_kyne" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
  <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/categories/17" class="quicklink">
		<div class="quicklink-text">
			<h2>Food Producers</h2>
			<p>Discover the very best quality Food produced all around Devon.  Fresh, local and full of flavour.</p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/agriculture-basket-beets-533360.jpg?itok=Klq0nb8K" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
  <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/categories/19" class="quicklink">
		<div class="quicklink-text">
			<h2>Drinks &amp; Beverages Producers</h2>
			<p>Discover the very best quality Drinks and Beverages produced all around Devon. Fresh, local and full of flavour.</p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/aroma-art-beverage-1251175.jpg?itok=QNNbAz30" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
  <div>
      
          <div class="col-md-6">
	<a href="https://www.foodanddrinkdevon.co.uk/members/supporter/1" class="quicklink">
		<div class="quicklink-text">
			<h2>Supporter Members</h2>
			<p>Unlock the full potential of you and your business with help from our teams of Devon experts.</p>
		</div>
		<div class="quicklink-image">
			<div class="more-link">View Members</div>
			<img typeof="foaf:Image" class="img-responsive" src="https://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/patrick-tomasso-406271-unsplash.jpg?itok=Kz9hdRiL" width="555" height="370" alt="" />
		</div>
	</a>
</div>    </div>
    </div>
  
  
  
  
  
  
</div>    
</div>
</section>
  </div>
					</div>
				</div>
			</div>
			<div id="latest-news">
				<div class="container">
					<div class="section">
						<div class="row">
							<div class="col-md-7">
								<h2>Latest News</h2>
																																	<div class="view view-news view-id-news view-display-id-block_1 view-dom-id-06acf96e4acb5c41c6cba6e007af6939">
        
  
  
      <div class="view-content">
        <div>
      
          <hr />
<h3><a href="/news/win-devon-cook-book-0">Win a Devon Cook Book</a></h3>
<p>The Devon Cook Book, packed full of fabulous recipes, is the perfect gift for anyone who loves food and Devon. You can order online here for just £14.95 including postage and take...</p>    </div>
  <div>
      
          <hr />
<h3><a href="/member/sharpham-cheese/news/sharpham-cheese-triumphs-two-exceptional-national-awards">Sharpham Cheese Triumphs with Two Exceptional National Awards</a></h3>
<p>Sharpham Cheese has recently won two outstanding awards in the food industry: UK Supreme Cheese at the Global Cheese Awards and British Product of the Year in the Great British...</p>    </div>
    </div>
  
  
  
  
  
  
</div>								<a href="/news" class="btn btn-white">More News</a>
								<div class="view view-mailchimp-newsletter-link view-id-mailchimp_newsletter_link view-display-id-block view-dom-id-3312c6965a7dc8f8d659579409863490">
        
  
  
  
  
  
  
  
  
</div>							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div id="homepage-boxes" class="section">
						<div class="col-md-4">
							<div class="white-box">
								<h2>Upcoming Events</h2>
																	  <div class="region region-whats-on-block">
    <section id="block-views-whats-on-block-1" class="block block-views clearfix">

      
  <div class="view view-whats-on view-id-whats_on view-display-id-block_1 view-dom-id-187c70c40a93900537adec8fbcae2639">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div>        <span><div class="feed">
<h4><a href="/whats-on/exeter-christmas-market-2019">Exeter Christmas Market 2019</a></h4>
<div class="results-details"><em><span class="date-display-range"><span class="date-display-start" property="dc:date" datatype="xsd:dateTime" content="2019-11-16T10:00:00+00:00">16/11/2019 - 10:00</span> to <span class="date-display-end" property="dc:date" datatype="xsd:dateTime" content="2019-12-19T21:00:00+00:00">19/12/2019 - 21:00</span></span></em></div>
<div class="results-details"><p>Exeter Cathedral, 1 The Cloisters, Exeter, EX1 1HS</p></div>
</div></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div>        <span><div class="feed">
<h4><a href="/whats-on/late-night-opening-lyme-bay-winery">Late Night Opening at Lyme Bay Winery</a></h4>
<div class="results-details"><em><span class="date-display-single">06/12/2019 - <span class="date-display-range"><span class="date-display-start" property="dc:date" datatype="xsd:dateTime" content="2019-12-06T10:00:00+00:00">10:00</span> to <span class="date-display-end" property="dc:date" datatype="xsd:dateTime" content="2019-12-06T19:30:00+00:00">19:30</span></span></span></em></div>
<div class="results-details"><p>Shute, Axminster, EX13 7PW</p></div>
</div></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div>        <span><div class="feed">
<h4><a href="/whats-on/malavita-cove">Malavita! at The Cove</a></h4>
<div class="results-details"><em><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-12-06T21:00:00+00:00">06/12/2019 - 21:00</span></em></div>
<div class="results-details"><p>Harbour Lights, Hope Cove, Kingsbridge, TQ7 3HQ</p></div>
</div></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
																<a href="/whats-on" class="btn">More Events</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="white-box">
								<h2>Offers</h2>
																	  <div class="region region-offers-block">
    <section id="block-views-offers-block-1" class="block block-views clearfix">

      
  <div class="view view-offers view-id-offers view-display-id-block_1 view-dom-id-32fe2c432bd53631d3cb8137d928e554">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1">
      
          <div class="feed">
<h4><a href="/member/glazebrook-house-hotel/offer/festive-afternoon-tea">Festive Afternoon Tea</a></h4>
<p>Celebrate this Christmas period with one of Glazebrook's...</br>
<strong class="post-date">Listing: <a href="/member/glazebrook-house-hotel">Glazebrook House Hotel</a></strong><br />
<strong class="post-date">Expires: <span class="date-display-interval">3 weeks 3 days</span></strong></p>
</div>    </div>
  <div class="views-row views-row-2">
      
          <div class="feed">
<h4><a href="/member/clock-tower/offer/local-loyal-christmas-offer">Local &amp; Loyal Christmas Offer</a></h4>
<p>The Clock Tower Sidmouth are offering all ‘Local &amp; Loyal’...</br>
<strong class="post-date">Listing: <a href="/member/clock-tower">The Clock Tower</a></strong><br />
<strong class="post-date">Expires: <span class="date-display-interval">2 weeks 3 days</span></strong></p>
</div>    </div>
  <div class="views-row views-row-3">
      
          <div class="feed">
<h4><a href="/member/lovaton-farm/offer/lovaton-farm-festive-offer">Lovaton Farm Festive Offer</a></h4>
<p>Situated on the northern fringe of Dartmoor, Lovaton Farm is a...</br>
<strong class="post-date">Listing: <a href="/member/lovaton-farm">Lovaton Farm</a></strong><br />
<strong class="post-date">Expires: <span class="date-display-interval">1 week 6 days</span></strong></p>
</div>    </div>
    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
																<a href="/offers" class="btn">More Offers</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="white-box">
								<h2>Vacancies</h2>
																	  <div class="region region-vacancies-block">
    <section id="block-views-vacancies-block" class="block block-views clearfix">

      
  <div class="view view-vacancies view-id-vacancies view-display-id-block view-dom-id-ffd6c0ac273bccf7ddeae560de82742c">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div>        <span><div class="feed">
<h4><a href="/member/angel-taste-devon/vacancy/casual-waiterwaitress">Casual Waiter/Waitress</a></h4>
<p>We are currently recruiting for a Casual/ Part-time Waiter/...</br>
<strong class="post-date">Listing: <a href="/member/angel-taste-devon">The Angel - Taste of Devon</a></strong><br />
<strong class="post-date">Expires: <span class="date-display-interval">1 week 2 days</span></strong></p>
</div></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
																<a href="/vacancies" class="btn">More Vacancies</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

					
					<a href="/become-member-food-drink-devon" id="member-button" class="btn btn-white">Member Benefits <br>&amp; Become a Member</a>
					<a href="http://eepurl.com/duqUqn" id="signup-button" class="btn btn-white fancybox">Sign Up To <br>Our Newsletter</a>

                    <a href="/" id="logo-footer">
                        <img src="/sites/all/themes/fooddrinkdevon/img/layout/logo-small-white.svg" alt="Food Drink Devon">
                    </a>
                    <div id="social">
                        <a href="https://www.facebook.com/fooddrinkdevon/" target="_blank" >
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/fooddrinkdevon" target="_blank" >
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="https://www.instagram.com/fooddrinkdevon/" target="_blank" >
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
											  <div class="region region-footer-menu">
    <section id="block-menu-menu-footer-menu" class="block block-menu clearfix">

      
  <ul id="footer-menu" class="nav navbar-nav"><li class="first leaf"><a href="/privacy-cookie-policy">Privacy &amp; Cookie Policy</a></li>
<li class="last leaf"><a href="/terms-use">Terms of Use</a></li>
</ul>
</section>
  </div>
					                    <div id="inventive">Website by <a href="http://www.inventivedesign.co.uk" target="_blank"><img src="https://www.inventivedesign.co.uk/sites/all/themes/inventive/img/icons/inventive.svg" alt="Inventive"></a></div>
									</div>
            </div>
        </div>
    </footer>	</div>
</div>	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.foodanddrinkdevon.co.uk/sites/default/files/js/js_dWhBODswdXXk1M5Z5nyqNfGljmqwxUwAK9i6D0YSDNs.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.5/dist/js/bootstrap.min.js"></script>
<script src="https://www.foodanddrinkdevon.co.uk/sites/default/files/js/js_q1I5vvhiFyihYv_Ay77IHCmNrCj7eMHBYyKth3UcBXM.js"></script>
<script src="https://www.foodanddrinkdevon.co.uk/sites/default/files/js/js_4DYUx3vjkDlxHeBhgZNTMSUU6b-Gl88AmmfsXIYtf74.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-9383261-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script src="https://www.foodanddrinkdevon.co.uk/sites/default/files/js/js_L8fmUiC3rfCftdlh3nLUBAo645SKp-QJVfubwxn652A.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"fooddrinkdevon","theme_token":"74cgj6KJJQ2v1ZsBIE3fHNQdN2k4LbVjKkdmUjyraVQ","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.10.2\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.5\/dist\/js\/bootstrap.min.js":1,"sites\/all\/modules\/bef_bootstrap_fix\/includes\/bef_bootstrap_fix.js":1,"sites\/all\/modules\/fancybox\/fancybox.js":1,"sites\/all\/libraries\/fancybox\/source\/jquery.fancybox.pack.js":1,"sites\/all\/libraries\/fancybox\/lib\/jquery.mousewheel-3.0.6.pack.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"\/\/cdnjs.cloudflare.com\/ajax\/libs\/select2\/4.0.1\/js\/select2.min.js":1,"sites\/all\/themes\/fooddrinkdevon\/slick\/slick.min.js":1,"sites\/all\/themes\/fooddrinkdevon\/js\/main.js":1,"sites\/all\/themes\/fooddrinkdevon\/js\/ie10-viewport-bug-workaround.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/fancybox\/source\/jquery.fancybox.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.5\/dist\/css\/bootstrap.min.css":1,"sites\/all\/themes\/bootstrap\/css\/3.3.5\/overrides.min.css":1,"sites\/all\/themes\/fooddrinkdevon\/slick\/slick.css":1,"sites\/all\/themes\/fooddrinkdevon\/slick\/slick-theme.css":1,"sites\/all\/themes\/fooddrinkdevon\/css\/main.css":1,"\/\/maxcdn.bootstrapcdn.com\/font-awesome\/4.4.0\/css\/font-awesome.min.css":1,"\/\/cdnjs.cloudflare.com\/ajax\/libs\/select2\/4.0.1\/css\/select2.min.css":1}},"fancybox":{"helpers":{"overlay":{"locked":0}},"selectors":["a:has(img)",".fancybox"]},"better_exposed_filters":{"views":{"quicklink_field_collection":{"displays":{"default":{"filters":[]}}},"page_banner_field_collection":{"displays":{"default":{"filters":[]}}},"news_on_listing_block":{"displays":{"block":{"filters":[]}}},"offers_on_listing_block":{"displays":{"block":{"filters":[]}}},"whats_on_listing_block":{"displays":{"block":{"filters":[]}}},"vacancies_on_listing_block":{"displays":{"block":{"filters":[]}}},"recipes_on_listing_block":{"displays":{"block":{"filters":[]}}},"internal_banner":{"displays":{"block":{"filters":[]}}},"news":{"displays":{"block_1":{"filters":[]}}},"offers":{"displays":{"block_1":{"filters":[]}}},"whats_on":{"displays":{"block_1":{"filters":[]}}},"vacancies":{"displays":{"block":{"filters":[]}}},"category_select_lists":{"displays":{"block":{"filters":[]}}},"mailchimp_newsletter_link":{"displays":{"block":{"filters":[]}}}}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":0,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":0,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
	<script src="https://www.foodanddrinkdevon.co.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
