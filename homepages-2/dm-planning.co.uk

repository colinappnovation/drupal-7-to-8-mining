<!DOCTYPE html>
<html lang="en" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="google-site-verification" content="GBbYkE6LlK0o325yqYYAGwdO83790FemCo768KWpYRA" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="D &amp; M Planning Limited RSS" href="https://www.dm-planning.co.uk/rss.xml" />
<title>D &amp; M Planning Limited | Chartered Town Planners, Godalming, Surrey</title>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/modules/system/system.base.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/system/system.menus.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/system/system.messages.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/system/system.theme.css?q1dqjv");
</style>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/sites/all/modules/scroll_to_top/scroll_to_top.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/field/theme/field.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/node/node.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/search/search.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/modules/user/user.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/all/modules/nodeorder/css/nodeorder.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/all/modules/views/css/views.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/all/modules/media/modules/media_wysiwyg/css/media_wysiwyg.base.css?q1dqjv");
</style>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/sites/all/modules/ctools/css/ctools.css?q1dqjv");
</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#back-top{right:40px;}#back-top span#button{background-color:#0064aa;}#back-top span#button:hover{opacity:1;filter:alpha(opacity = 1);background-color:#777777;}span#link{display :none;}

/*]]>*/-->
</style>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/sites/all/modules/eu_cookie_compliance/css/eu_cookie_compliance.css?q1dqjv");
</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#86909C;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}

/*]]>*/-->
</style>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/style.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/css/responsive.css?q1dqjv");
</style>
<style type="text/css" media="all">
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_2.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_5.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_6.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_8.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_11.css?q1dqjv");
@import url("https://www.dm-planning.co.uk/sites/default/files/css_injector/css_injector_12.css?q1dqjv");
</style>
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet">
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/misc/drupal.js?q1dqjv"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/modules/eu_cookie_compliance/js/jquery.cookie-1.4.1.min.js?v=1.4.1"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/modules/scroll_to_top/scroll_to_top.js?q1dqjv"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/modules/eu_cookie_compliance/js/eu_cookie_compliance.js?q1dqjv"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/js/retina.js?q1dqjv"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/js/plugins.js?q1dqjv"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/js/global.js?q1dqjv"></script>
<script type="text/javascript" src="https://www.dm-planning.co.uk/sites/all/themes/DandM_theme/js/reveal.js?q1dqjv"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"DandM_theme","theme_token":"G6eRwKRwp-dfAnu8zfWK96zIAWX8BtOORqj72fx2B7U","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/modules\/scroll_to_top\/scroll_to_top.js":1,"0":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/themes\/DandM_theme\/js\/retina.js":1,"sites\/all\/themes\/DandM_theme\/js\/plugins.js":1,"sites\/all\/themes\/DandM_theme\/js\/global.js":1,"sites\/all\/themes\/DandM_theme\/js\/reveal.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/scroll_to_top\/scroll_to_top.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/nodeorder\/css\/nodeorder.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"0":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"1":1,"sites\/all\/themes\/DandM_theme\/style.css":1,"sites\/all\/themes\/DandM_theme\/css\/responsive.css":1,"public:\/\/css_injector\/css_injector_2.css":1,"public:\/\/css_injector\/css_injector_5.css":1,"public:\/\/css_injector\/css_injector_6.css":1,"public:\/\/css_injector\/css_injector_8.css":1,"public:\/\/css_injector\/css_injector_11.css":1,"public:\/\/css_injector\/css_injector_12.css":1}},"scroll_to_top":{"label":""},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EView Privacy Policy\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EDecline\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EView Privacy Policy\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EDecline\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EThank you for accepting cookiesYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-policy","popup_link_new_window":1,"popup_position":null,"fixed_top_position":false,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":1,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":1,"withdraw_button_on_info_popup":false,"cookie_categories":[],"enable_save_preferences_button":true,"fix_first_cookie_category":true,"select_all_categories_by_default":false}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="/sites/all/themes/DandM_theme/js/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node">
    
<div id="wrap">
  <div id="header-wrap" class="clr fixed-header">
    <header id="header" class="site-header clr container">
      <div id="header-top-half">
      <div id="logo" class="clr">
                <div id="site-logo"><a href="/" title="Home">
          <img src="https://www.dm-planning.co.uk/sites/default/files/Logo.png" alt="Home" />
        </a></div>              </div>
      <div id="site-contact-wrap">
        <div class="site-contact-info">
          <div class="site-phone-number con-inf"><i class="fa fa-phone" aria-hidden="true"></i>01483 425705</div>
          <div class="site-email-address con-inf"><i class="fa fa-envelope-o" aria-hidden="true"></i><a class="email-link" href="mailto: info@dm-planning.co.uk">info@dm-planning.co.uk</a></div>
        </div>
      </div>
      </div>
      <div id="header-bottom-half"> 
      <div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close"></a></div>
      <div id="site-navigation-wrap">
        <a href="#sidr-main" id="navigation-toggle"><span class="fa fa-bars"></span>Menu</a>
        <nav id="site-navigation" class="navigation main-navigation clr" role="navigation">
          <div id="main-menu" class="menu-main-container">
            <ul class="menu"><li class="first leaf"><a href="/" class="active">Home</a></li>
<li class="leaf"><a href="/our-services" title="Our Services">Our Services</a></li>
<li class="leaf"><a href="/the-team" title="">The Team</a></li>
<li class="leaf"><a href="/case-studies" title="">Case Studies</a></li>
<li class="leaf"><a href="/useful-links">Useful Links</a></li>
<li class="last leaf"><a href="/contact" title="Get in touch with DM planning">Contact Us</a></li>
</ul>          </div>
        </nav>
      </div>
      </div>
    </header>
  </div>

        <div id="homepage-slider-wrap" class="clr flexslider-container">
    <div id="homepage-slider" class="flexslider">
      <ul class="slides clr">
        <li class="homepage-slider-slide">
          <a href="/our-services">
            <div class="homepage-slide-inner container">
                            <div class="homepage-slide-content">
                <div class="homepage-slide-title">Providing town planning advice to public &amp; private sectors</div>
                <div class="clr"></div>
                <div class="homepage-slide-caption">Sed do eiusmod tempor incididunt elit...</div>
              </div>
                          </div>
            <img src="/sites/all/themes/DandM_theme/images/slide-image-1.jpg">
          </a>
        </li>
        <li class="homepage-slider-slide">
          <a href="/node/2">
            <div class="homepage-slide-inner container">
                            <div class="homepage-slide-content">
                <div class="homepage-slide-title">Providing town planning advice to public &amp; private sectors</div>
                <div class="clr"></div>
                <div class="homepage-slide-caption">Lorem ipsum dolor sit amet, consectetur...</div>
              </div>
                          </div>
            <img src="/sites/all/themes/DandM_theme/images/slide-image-2.jpg">
          </a>
        </li>
        <li class="homepage-slider-slide">
          <a href="/useful-links">
            <div class="homepage-slide-inner container">
                            <div class="homepage-slide-content">
                <div class="homepage-slide-title">Providing town planning advice to public &amp; private sectors</div>
                <div class="clr"></div>
                <div class="homepage-slide-caption">Duis aute irure dolor in reprehenderit...</div>
              </div>
                          </div>
            <img src="/sites/all/themes/DandM_theme/images/slide-image-3.jpg">
          </a>
        </li>
      </ul>
    </div>
  </div>
    

    <div id="preface-wrap" class="site-preface clr">
    <div id="preface" class="clr container">
              <div id="preface-block-wrap" class="clr">
          <div id="preface-blocks-title" class="clr">Professional Advice On</div>
          <div class="span_1_of_3 col col-1 preface-block ">
            <div class="region region-preface-first">
  <div id="block-block-1" class="block block-block">

      
  <div class="content">
    <p>Site Appraisal &amp; Planning Potential</p>
<p>Permitted Development</p>
<p>Planning Applications</p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span_1_of_3 col col-2 preface-block ">
            <div class="region region-preface-middle">
  <div id="block-block-2" class="block block-block">

      
  <div class="content">
    <p>Enforcement</p>
<p>Heritage Assets &amp; Statements</p>
<p>Development Plan Representations</p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span_1_of_3 col col-3 preface-block ">
            <div class="region region-preface-last">
  <div id="block-block-3" class="block block-block">

      
  <div class="content">
    <p>Public Consultation &amp; LPA Negotiations</p>
<p>Environmental Impact Assements</p>
<p>Rural Planning</p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>        </div>
                </div>
  </div>
  

  <div id="main" class="site-main container clr">
        <div id="primary" class="content-area clr">
      <section id="content" role="main" class="site-content  clr">
                                <div id="content-wrap">
                                                                      <div class="region region-content">
  <div id="block-block-5" class="block block-block">

        <h2 ><span>Welcome</span></h2>
    
  <div class="content">
    <div class="subtitle">D&amp;M Planning Limited is a firm of Chartered Town Planning consultants based in Godalming, Surrey.</div>
<p>We provide advice to public and private sector clients on aspects of town planning including applications, appeals, Local Development Frameworks representations, site and development appraisals and the co-ordination of environmental statements.</p>
<p>Within this role we develop or advise on estate strategies and the development potential of surplus estate assets.</p>
<p>This expertise is supported by sound technical knowledge and all professional staff are Chartered Town Planners and members of the Royal Town Planning Institute.</p>
  </div>
  
</div> <!-- /.block -->
<div id="block-system-main" class="block block-system">

      
  <div class="content">
      <article id="node-49" class="node node-page node-promoted node-teaser clearfix" about="/dummy-front-page" typeof="foaf:Document">
        <header>
                    <h2 class="title" ><a href="/dummy-front-page">Dummy Front Page</a></h2>
            <span property="dc:title" content="Dummy Front Page" class="rdf-meta element-hidden"></span>  
      
          </header>
  
  <div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Necessary !</p>
</div></div></div>  </div>

      <footer>
      <ul class="links inline"><li class="node-readmore first last"><a href="/dummy-front-page" rel="tag" title="Dummy Front Page">Read more<span class="element-invisible"> about Dummy Front Page</span></a></li>
</ul>    </footer>
  
    </article> <!-- /.node -->
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
        </div>
      </section>

          </div>
  </div>

    <div class = "container clr">
              <div class="span_1_of_1 col col-1">
          <div class="region region-content-bottom">
  <div id="block-views-case-studies-block-block" class="block block-views">

        <h2 ><span>Case Studies</span></h2>
    
  <div class="content">
    <div class="view view-case-studies-block view-id-case_studies_block view-display-id-block view-dom-id-7a74285b9e2048cab387740bc244eaee">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      <article id="node-52" class="node node-case-study node-teaser clearfix" about="/case-studies/august-2019-0" typeof="sioc:Item foaf:Document">
        <header>
                    <h2 class="title" ><a href="/case-studies/august-2019-0">August 2019</a></h2>
            <span property="dc:title" content="August 2019" class="rdf-meta element-hidden"></span>  
      
          </header>
  
  <div class="content">
    <div class="field field-name-field-address field-type-text field-label-hidden"><div class="field-items"><div class="field-item even">Hurtmore</div></div></div><div class="field field-name-field-main-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/case-studies/august-2019-0"><img typeof="foaf:Image" src="https://www.dm-planning.co.uk/sites/default/files/styles/medium/public/default_images/Default.png?itok=fZtJcjhK" width="220" height="147" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>D&amp;M Planning have been successful at achieving planning permission at appeal for the conversion of an existing residential annex to independent dwelling within the metropolitan Green Belt.</p></div></div></div>  </div>

      <footer>
      <ul class="links inline"><li class="node-readmore first last"><a href="/case-studies/august-2019-0" rel="tag" title="August 2019">Read more<span class="element-invisible"> about August 2019</span></a></li>
</ul>    </footer>
  
    </article> <!-- /.node -->
  </div>
  <div class="views-row views-row-2 views-row-even">
      <article id="node-51" class="node node-case-study node-teaser clearfix" about="/case-studies/august-2019" typeof="sioc:Item foaf:Document">
        <header>
                    <h2 class="title" ><a href="/case-studies/august-2019">August 2019</a></h2>
            <span property="dc:title" content="August 2019" class="rdf-meta element-hidden"></span>  
      
          </header>
  
  <div class="content">
    <div class="field field-name-field-address field-type-text field-label-hidden"><div class="field-items"><div class="field-item even">Walton-on-the-Hill</div></div></div><div class="field field-name-field-main-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/case-studies/august-2019"><img typeof="foaf:Image" src="https://www.dm-planning.co.uk/sites/default/files/styles/medium/public/jk.jpg?itok=MOOEqH89" width="220" height="87" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>D&amp;M Planning has been successful in obtaining Planning Permission and Listed Building Consent for the erection of a first floor extension and internal alterations. </p></div></div></div>  </div>

      <footer>
      <ul class="links inline"><li class="node-readmore first last"><a href="/case-studies/august-2019" rel="tag" title="August 2019">Read more<span class="element-invisible"> about August 2019</span></a></li>
</ul>    </footer>
  
    </article> <!-- /.node -->
  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      <article id="node-48" class="node node-case-study node-teaser clearfix" about="/case-studies/november-2018" typeof="sioc:Item foaf:Document">
        <header>
                    <h2 class="title" ><a href="/case-studies/november-2018">November 2018</a></h2>
            <span property="dc:title" content="November 2018" class="rdf-meta element-hidden"></span>  
      
          </header>
  
  <div class="content">
    <div class="field field-name-field-address field-type-text field-label-hidden"><div class="field-items"><div class="field-item even">Hindhead, Surrey</div></div></div><div class="field field-name-field-main-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/case-studies/november-2018"><img typeof="foaf:Image" src="https://www.dm-planning.co.uk/sites/default/files/styles/medium/public/default_images/Default.png?itok=fZtJcjhK" width="220" height="147" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>D&amp;M Planning has been successful in obtaining Planning Permission and Listed Building Consent for the conversion of two Listed barns  with a glazed link to form a new dwelling, erecting a new s</p></div></div></div>  </div>

      <footer>
      <ul class="links inline"><li class="node-readmore first last"><a href="/case-studies/november-2018" rel="tag" title="November 2018">Read more<span class="element-invisible"> about November 2018</span></a></li>
</ul>    </footer>
  
    </article> <!-- /.node -->
  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
        </div>
          </div>

    <div id="footer-wrap" class="site-footer clr">
    <div id="footer" class="clr container">
              <div id="footer-block-wrap" class="clr">
          <div class="span_1_of_3 col col-1 footer-block ">
            <div class="region region-footer-first">
  <div id="block-block-12" class="block block-block">

      
  <div class="content">
    <p><a href="http://dev.dm-planning.co.uk">Home</a><br /><a href="/Our-Services">Our Services</a><br /><a href="/The-Team">The Team</a><br /><a href="/Case-Studies">Case Studies</a><br /><a href="/Useful-Links">Useful Links</a><br /><a href="/Contact">Contact Us</a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span_1_of_3 col col-2 footer-block ">
            <div class="region region-footer-second">
  <div id="block-block-11" class="block block-block">

      
  <div class="content">
    <div id="planning"><img src="https://www.dm-planning.co.uk/sites/default/files/planning.png" style="float: left;" /><a href="sites/all/libraries/documents/education-awards-winners-brochure-2011.pdf"><img src="https://www.dm-planning.co.uk/sites/default/files/waverlydesignawards_0.png" /></a></div>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span_1_of_3 col col-3 footer-block ">
            <div class="region region-footer-third">
  <div id="block-block-14" class="block block-block">

      
  <div class="content">
    <p><a href="Farncombe Youth FC" target="_blank"><img src="https://www.dm-planning.co.uk/sites/default/files/FYFC_footer_0.png" /></a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>        </div>
            
          </div>
  </div>
  
  <footer id="copyright-wrap" class="clear">
    <div id="copyright">Copyright &copy; 2019, <a href="/">D &amp; M Planning Limited</a> | <a href="http://www.dm-planning.co.uk/privacy-policy">Privacy & Cookies</a> | Created by  <a href="http://www.Siric.com" title="Siric" target="_blank">Siric</a>.</div>
  </footer>
</div>  </body>
</html>