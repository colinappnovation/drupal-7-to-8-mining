<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.datacloudcongress.com/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Datacloud Global Congress is now entering its 16th year as the premier leadership summit for critical IT infrastructure" />
<meta name="abstract" content="Over the past 15 years Datacentres Europe has evolved as a recognised beacon of high quality content offering thought leadership across Energy Cloud, Markets and Technologies, and has performed a critical role as an international networking and deal making opportunity for old and new contacts alike." />
<meta name="keywords" content="datacentres, datacenters, datacentres europe, dcg, monaco, dce, dce15, data centres europe, data, data centre, data, center, cloud computing, green datacentres, security, design, operation, infrastructure, coolin, virtualisation, data centre services, market, data centre deals, colocation, modular data centre, data centre management, broadgroup, research, network" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.datacloudcongress.com/" />
<link rel="shortlink" href="https://www.datacloudcongress.com/" />
<meta property="og:site_name" content="Datacloud Global Congress 2020" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.datacloudcongress.com/" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Datacloud Global Congress 2020</title>
  <link type="text/css" rel="stylesheet" href="https://www.datacloudcongress.com/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.datacloudcongress.com/sites/default/files/css/css_hYCLW089C9S9sP3ZYkuG6R-Q5ZHbEhblZBFjwZ_bE_I.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.datacloudcongress.com/sites/default/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.datacloudcongress.com/sites/default/files/css/css_HJGrZA-A2eHP4CSeznsA2hlkRVrKYtrAEpWiTZMY6Fw.css" media="all" />
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-article welcome-datacloud-global-congress-2020" >

    
<div class="cookies">
  <div class="wrapper">
      <p>This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. <a href="/cookie-policy">Find out more here</a></p>
      <a href="#" class="btn">Continue</a>
    </div>
</div>

<header>
  <section class="header-top">
    <div class="logo"><a href="/">Datacloud Global Congress 2020</a><span>2-4 June, Grimaldi Forum, Monaco</span></div>
    <div class="clear"></div>
    <div class="mobile-menu">
      <nav class="navbar">
          <div class="region region-menu">
    <div id="block-system-main-menu" class="block block-system block-menu">

    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/" title="" class="active">Home</a></li>
<li class="leaf"><a href="http://www.datacloudawards.com/" title="">Awards</a></li>
<li class="leaf"><a href="/programme">Programme</a></li>
<li class="leaf"><a href="/sponsors">Sponsors</a></li>
<li class="leaf"><a href="/speakers" title="">Speakers</a></li>
<li class="leaf"><a href="/press-releases" title="">Press</a></li>
<li class="last collapsed"><a href="/venue">Venue</a></li>
</ul>  </div>
</div>
  </div>
      </nav>

      <div class="top-buttons">
        <a href="/enquiries" class="btn">Enquiries</a>
        <a href="/register" class="btn btn-red">Register</a>
      </div>
    </div>
    
    <a class="nav-toggle">
      <div></div>
    </a>
  </section>
</header>


<section class="main-content">
  <div class="wrapper">
      <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-1" class="node node-article clearfix">

  
      <h1>Welcome to Datacloud Global Congress 2020</h1>
  
  
  <div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h2>2-4 June, Grimaldi Forum, Monaco</h2><p>Welcome to Datacloud Global Congress 2020!<br />After another tremendous year of expansion and investment in the sector globally, we welcome back to Monaco an expected audience of &gt;2,000 executives and professionals in datacenter, cloud and Edge, their enterprise customers, solutions providers and Investors.</p><p><a class="btn btn-red" href="/register">Early Bird Registration</a></p></div></div></div>  </div>

  
  
</div>
  </div>
</div>
  </div>
  </div>
</section>

<section class="sct-conference sct-img-text sct-left">
  <div class="bg-img"></div>
  <div class="wrapper">
  <div class="sct-box">
    <div class="sct-category">Conference and Exhibition</div>
      <div class="region region-sct-conference">
    <div id="block-block-50" class="block block-block">

  	  	<h3><span>Opening Minds Closing Deals</span></h3>
    
  <div class="content">
    <p>This year’s extensive programme will be delivered across 4 theatres supported by a larger than ever industry exhibition, meeting lounges, Pods, and a new outside category for connected cars. Demand is very high for floor space so early booking is recommended.</p><p><a class="btn" href="/conference">Conference</a></p>  </div>
</div>
  </div>
  </div>
  </div>
</section>

<section class="sct-awards sct-img-text sct-right">
  <div class="bg-img"></div>
  <div class="wrapper">
  <div class="sct-box">
    <div class="sct-category">Awards</div>
      <div class="region region-sct-awards">
    <div id="block-block-51" class="block block-block">

  	  	<h3><span>Datacloud Global Awards 2020</span></h3>
    
  <div class="content">
    <p>The 2020 Awards presents a unique opportunity for the industry to recognize achievement and innovation. Both existing and new categories will be announced very soon.</p>
<a href="https://www.datacloudawards.com/" target="_blank" class="btn">Awards</a>  </div>
</div>
  </div>
  </div>
  </div>
</section>

<section class="sct-programme sct-img-text sct-left">
  <div class="bg-img"></div>
  <div class="wrapper">
  <div class="sct-box">
    <div class="sct-category">Programme</div>
      <div class="region region-sct-programme">
    <div id="block-block-52" class="block block-block">

  	  	<h3><span>A wide number of brand new topics</span></h3>
    
  <div class="content">
    <p>We will announce programme highlights by early November, which will include keynotes, solos, panels, roundtables covering a wide number of brand new topics reflecting current trends and market outlook.</p>
<p>    <a href="/programme" class="btn">Programme</a></p>
  </div>
</div>
  </div>
  </div>
  </div>
</section>

<section class="sct-registration sct-img-text sct-right">
  <div class="bg-img"></div>
  <div class="wrapper">
  <div class="sct-box">
    <div class="sct-category">Registration</div>
      <div class="region region-sct-registration">
    <div id="block-block-53" class="block block-block">

  	  	<h3><span>3 for 2 tickets available</span></h3>
    
  <div class="content">
    <p>Take advantage of one-time Early Bird price discounts which ends 31 December 2019.<br />The event takes place 2-4 June 2020.</p><p>&nbsp;<img class="fluid-width" src="/sites/default/files/tickets.jpg" alt="Ticket Options" /></p><p><a class="btn btn-red" href="/register">Registration</a></p>  </div>
</div>
  </div>
  </div>
  </div>
</section>


<section id="map"></section>

<section class="sct-upcoming">
  <div class="wrapper">
    <ul class="footer-tabs">
			<li><a href="#fevents">Upcoming Events</a></li>
			<li><a href="#freports">Reports</a></li>
		</ul>
    
    <div class="footer-tabs-content">
      <div id="fevents" class="tab-block">
				<a href="http://www.broad-group.com/events" target="_blank" class="btn">All Events</a>
        <div class="clear"></div>
            <div class="region region-fevents">
    <div id="block-block-38" class="block block-block">

    
  <div class="content">
    <p>Over the last decade BroadGroup has successfully built and hosted a series of pre-eminent events for senior executives in data center, cloud, edge and IT infrastructure.</p>

<article>
<img src="/sites/default/files/event-dc-india.jpg" alt="Datacloud India">
<h4>Datacloud India 2020</h4>
<h5>February 5-6, 2020<br>
Mumbai</h5>
<p>We are bringing the 2nd Datacloud India to Mumbai! The Edge workshop and one-day event provide deep content, outstanding networking and the opportunity to do deals that influence outcomes...</p>
<p><a href="https://www.datacloudindia.com/" target="_blank">Visit website</a></p>
</article>

<article>
<img src="/sites/default/files/event-finvest-global.jpg" alt="Finvest Global Summit 2020">
<h4>Finvest Global Summit 2020</h4>
<h5>February 13, 2020<br>
Zurich</h5>
<p>Finvest is a Data Economy Live Event, the global flagship summit for finance, investment and site selection executives, location agencies, property specialists, and professional and legal intermediaries...</p>
<p><a href="https://www.finvestsummit.com/zurich" target="_blank">Visit website</a></p>
</article>

<article>
<img src="/sites/default/files/edge-europe-20202.jpg" alt="Edge Europe">
<h4>Edge Europe</h4>
<h5>March 25-26, 2020<br>
Mainz</h5>
<p>We believe in a truly connected world. This can only be facilitated by an all-encompassing Edge ecosystem. Edge will be the most transformative opportunity for the next decade.</p>
<p><a href="https://www.edgecongress.com/europe" target="_blank">Visit website</a></p>
</article>

<article>
<img src="/sites/default/files/datacloud-congress-20202.jpg" alt="Datacloud Global Congress">
<h4>Datacloud Global Congress</h4>
<h5>June 2-4, 2020<br>
Monaco</h5>
<p>After another tremendous year of expansion and investment in the sector globally, we welcome back to Monaco an expected audience of >2,000 executives and professionals in datacenter...</p>
<p><a href="https://www.datacloudcongress.com/" target="_blank">Visit website</a></p>
</article>  </div>
</div>
  </div>
        <div class="clear"></div>
			</div>
      
      <div id="freports" class="tab-block">
        <a href="http://www.broad-group.com/events" target="_blank" class="btn">All Reports</a>
        <div class="clear"></div>
            <div class="region region-freports">
    <div id="block-block-39" class="block block-block">

    
  <div class="content">
    <p>Browse our Knowledge Shop for the most in-depth analysis of the sector available. Our reports currently cover over 18 markets.</p>
        
<article>
<img src="/sites/default/files/report-cmq.jpg" alt="Colocation Markets Quarterly">
<h4>Colocation Markets Quarterly</h4>
<h5>Current edition: Q2 2019<br>&nbsp;</h5>
<p>New, timely data covering the four Tier 1 cities in Europe - Amsterdam, Frankfurt, London/M25, and Paris.<br>&nbsp;<br>&nbsp;</p>
<p><a href="https://www.broad-group.com/reports/cmq" target="_blank">View report</a></p>
</article>

<article>
<img src="/sites/default/files/report-india.jpg" alt="Data Centres India">
<h4>Data Centres India</h4>
<h5>Published: February 2019<br>&nbsp;</h5>
<p>Data Centres India is the first comprehensive report covering the country from BroadGroup which includes more than 100 third party facilities...<br>&nbsp;</p>
<p><a href="https://www.broad-group.com/reports/data-centres-india" target="_blank">View report</a></p>
</article>

<article>
<img src="/sites/default/files/report-nordic.jpg" alt="Data Centres Nordic IV and Baltic States">
<h4>Data Centres Nordic IV and Baltic States</h4>
<h5>Published: November 2018</h5>
<p>The Nordic data centre market is now heading towards the USD7 billion mark in total investments made from a mix of M&A activity, hyperscale build out and expansion by third party players...</p>
<p><a href="https://www.broad-group.com/reports/data-centres-nordic-iv" target="_blank">View report</a></p>
</article>

<article>
<img src="/sites/default/files/report-africa.jpg" alt="Data Centers Africa II">
<h4>Data Centers Africa II</h4>
<h5>Published: August 2018<br>&nbsp;</h5>
<p>The new report – Data Centers Africa II – published this August has now been extended to cover 21 countries. Cloud expansion, mobile data and increased international and domestic bandwidth...</p>
<p><a href="https://www.broad-group.com/reports/data-centers-africa-ii" target="_blank">View report</a></p>
</article>  </div>
</div>
  </div>
        <div class="clear"></div>
      </div>
    </div>
    
    <a href="http://www.broad-group.com" target="_blank" class="bglogo"><img src="/sites/all/themes/dce20/images/bg-logo.png" alt="BroadGroup"></a>
    <p class="copyright">&copy; BroadGroup 2005-2019. All rights reserved.</p>
  </div>
</section>

<footer>
  <div class="wrapper">
    <nav>
        <div class="region region-footer-menu">
    <div id="block-menu-menu-footer-menu" class="block block-menu">

    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/privacy-notice" title="">Privacy</a></li>
<li class="leaf"><a href="/terms-conditions" title="">Terms & Conditions</a></li>
<li class="leaf"><a href="/cookie-policy" title="">Cookie Policy</a></li>
<li class="leaf"><a href="https://www.euromoneyplc.com/modern-slavery-act-transparency-statement" title="">Modern Slavery Act Transparency Statement</a></li>
<li class="last leaf"><a href="https://www.datacloudcongress.com/sites/default/files/BroadGroup%20Event%20Participant%20T&amp;CS.pdf" title="">Event Participant Terms and Conditions</a></li>
</ul>  </div>
</div>
  </div>
    </nav>

    <div class="social-buttons">
      <a href="https://twitter.com/DatacloudGlobal" target="_blank"><img src="/sites/all/themes/dce20/images/twitter.png" alt="Twitter"></a>
      <a href="https://www.facebook.com/datacentrescom/" target="_blank"><img src="/sites/all/themes/dce20/images/facebook.png" alt="Facebook"></a>
      <a href="http://www.linkedin.com/company/broadgroup" target="_blank"><img src="/sites/all/themes/dce20/images/linkedin.png" alt="LinkedIn"></a>
      <a href="http://instagram.com/datacloudmonaco/" target="_blank"><img src="/sites/all/themes/dce20/images/instagram.png" alt="Instagram"></a>
      <a href="https://www.flickr.com/photos/broadgroup/albums" target="_blank"><img src="/sites/all/themes/dce20/images/flickr.png" alt="Flickr"></a>
      <a href="https://www.youtube.com/user/datacentrescom/" target="_blank"><img src="/sites/all/themes/dce20/images/youtube.png" alt="Youtube"></a>
    </div>
  </div>
</footer>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY8livRbzNCNBTSbkzoxk-Jvn1L6gQESc&sensor=false&extension=.js"></script>
<script> google.maps.event.addDomListener(window, 'load', init);

var map;

function init() {
    var mapOptions = {
        center: new google.maps.LatLng(43.7394404,7.4145599),
        zoom: 13,
        mapTypeControl: false,
        scrollwheel: false,
				zoomControl: true,
				zoomControlOptions: {
						style: google.maps.ZoomControlStyle.SMALL,
						position: google.maps.ControlPosition.TOP_RIGHT
				},
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP    
    }

    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var locations = [
        ['Grimaldi Forum Monaco', 43.7440075, 7.431415299999999]
    ];

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            icon: '',
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });
    }
}
</script>
  
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-22075651-30"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-22075651-30');
</script>
<script type="text/javascript" src="https://www.datacloudcongress.com/sites/default/files/js/js_ccHkhj0Fhi494oQe6oydJilOtb42NBs3SnjedbIGoOc.js"></script>
<script type="text/javascript" src="https://www.datacloudcongress.com/sites/default/files/js/js_hPLa-NaogrrffoOeca-OLWuOQ-zOAg98rGvEO36ISM0.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"dce20","theme_token":"HziA5E1qQMHeht2L-OoOb1p7yT206dsm0EQEvVcYOBk","js":{"sites\/all\/themes\/dce20\/js\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/dce20\/js\/script.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/dce20\/css\/style.css":1,"sites\/all\/themes\/dce20\/css\/maintenance-page.css":1}}});
//--><!]]>
</script>
<script async type="text/javascript" src="/_Incapsula_Resource?SWJIYLWA=719d34d31c8e3a6e6fffd425f7e032f3&ns=1&cb=1925928221"></script>
</body>
</html>
