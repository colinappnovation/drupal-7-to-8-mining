<!DOCTYPE html>
<html lang="en" dir="ltr">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta name="viewport" content="width=device-width, minimum-scale=1">
  <meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<meta name="google-translate-customization" content="eb72a345cfbd8c11-875276fb43ddba8c-gcbdce10568327e2c-16"></meta>
<link rel="shortcut icon" href="https://publicationethics.org/sites/all/themes/custom/cope/favicon.ico" type="image/vnd.microsoft.icon" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Promoting integrity in research and its publication | Committee on Publication Ethics: COPE</title>
  <link type="text/css" rel="stylesheet" href="https://publicationethics.org/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://publicationethics.org/files/css/css_WjpqomxPBWavamdhvzfJ4bM5_wZEz6r0Ucv1PAcvq_g.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://publicationethics.org/files/css/css_hzsrS9IdBPyinsa9F2l378Ecg5k15WN36krwm3N6KaE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://publicationethics.org/files/css/css_2sR-dWc6Z0Hoq9Qvro3g_G5u6Z8We9ZNkzIOi9WF7Kk.css" media="all" />
  <script type="text/javascript" src="https://publicationethics.org/files/js/js_0RyHJ63yYLuaWsodCPCgSD8dcTIA0dqcDf8-7c2XdBw.js"></script>
<script type="text/javascript" src="https://publicationethics.org/files/js/js_TVTqjz8JHRb2KK9hlzuk0YsjzD013dKyYX_OTz-2VXU.js"></script>
<script type="text/javascript" src="https://publicationethics.org/files/js/js_aLomVuWTEP3-YY4YTfgAtk-N2JcMWzX857yDyeWeEkY.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create", "UA-30535664-1", {"cookieDomain":"auto"});ga("require", "linkid", "linkid.js");ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://publicationethics.org/files/js/js_Jv2f0028cPavADDKycMmdQeDMNs-n_TNsisVZZzFu0U.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"cope","theme_token":"UpgjP_5urKdJUrUuuPknDfsiemVvLNzi44XsZPAKlZo","js":{"0":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/custom\/cope\/assets\/main.js":1,"sites\/all\/themes\/custom\/cope\/j\/jquery.tipTip.js":1,"sites\/all\/themes\/custom\/cope\/j\/site.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/logintoboggan\/logintoboggan.css":1,"sites\/all\/modules\/contrib\/quiz\/quiz.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/contrib\/lexicon\/css\/lexicon.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/book_made_simple\/book_made_simple.css":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.bare.css":1,"sites\/all\/themes\/custom\/cope\/assets\/fonts\/stylesheet.css":1,"sites\/all\/themes\/custom\/cope\/assets\/main.css":1,"sites\/all\/themes\/custom\/cope\/c\/tipTip.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022cookie-notification\u0022\u003E\n  \u003Cdiv class=\u0022cookie-notification__row\u0022\u003E\n    \u003Cdiv class=\u0022cookie-notification__col\u0022\u003E\n      \u003Cdiv class=\u0022cookie-notification__content\u0022\u003E\n\n        \u003Cdiv class=\u0022cookie-notification__description\u0022\u003E\n          \u003Ch2 class=\u0022cookie-notification__title\u0022\u003EThis website uses cookies\u003C\/h2\u003E\n          \u003Cp class=\u0022cookie-notification__body\u0022\u003E\u003Cp\u003EThis website uses cookies to provide us with aggregated information about how users interact with our site, but not to track individual usage. By using this site, you accept the use of cookies.\u003C\/p\u003E\n\u003C\/p\u003E\n        \u003C\/div\u003E\n\n        \n        \u003Cdiv class=\u0022cookie-notification__buttons\u0022\u003E\n          \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button cookie-notification__button--primary\u0022\u003EAccept\u003C\/button\u003E\n                                \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003ERead more\u003C\/button\u003E\n                  \u003C\/div\u003E\n\n      \u003C\/div\u003E\n    \u003C\/div\u003E\n\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022cookie-notification\u0022\u003E\n  \u003Cdiv class=\u0022cookie-notification__row\u0022\u003E\n    \u003Cdiv class=\u0022cookie-notification__col\u0022\u003E\n      \u003Cdiv class=\u0022cookie-notification__content\u0022\u003E\n\n        \u003Cdiv class=\u0022cookie-notification__description\u0022\u003E\n          \u003Ch2 class=\u0022cookie-notification__title\u0022\u003EThis website uses cookies\u003C\/h2\u003E\n          \u003Cp class=\u0022cookie-notification__body\u0022\u003E\u003Cp\u003EWe use cookies on this site to enhance your user experience\u003C\/p\u003E\n\u003Cp\u003EBy tapping the Accept button, you agree to us doing so.\u003C\/p\u003E\n\u003C\/p\u003E\n        \u003C\/div\u003E\n\n        \n        \u003Cdiv class=\u0022cookie-notification__buttons\u0022\u003E\n          \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button cookie-notification__button--primary\u0022\u003EAccept\u003C\/button\u003E\n                                \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003ERead more\u003C\/button\u003E\n                  \u003C\/div\u003E\n\n      \u003C\/div\u003E\n    \u003C\/div\u003E\n\n  \u003C\/div\u003E\n\u003C\/div\u003E","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EThank you for accepting cookies\u003C\/p\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":1,"popup_height":"auto","popup_width":0,"popup_delay":1000,"popup_link":"\/privacy","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":0,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWe use cookies on this site to enhance your user experience\u003C\/p\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":0,"cookie_categories":[],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>

      <!-- Hotjar Tracking Code for http://publicationethics.org/ -->
    <script> (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:207952,hjsv:5}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv='); </script>
  </head>

<body class="html front not-logged-in no-sidebars page-block page-block-homepage" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <header role="banner" class="header">
  <div class="header__row">

    <a class="header__logo" href="/" title="Home" rel="home">COPE</a>

    <input class="header__toggle header__search-toggle" id="header__search-toggle" type="radio" name="header__toggle" />
    <label class="header__toggle-btn header__search-toggle-btn" for="header__search-toggle">Show search</label>

    <input class="header__toggle header__menu-toggle" id="header__menu-toggle"type="radio" name="header__toggle" />
    <label class="header__toggle-btn header__menu-toggle-btn" for="header__menu-toggle">Show menu</label>

    <input class="header__toggle header__close-toggle" id="header__close-toggle" type="radio" name="header__toggle" checked />
    <label class="header__toggle-btn header__close-toggle-btn" for="header__close-toggle">Close</label>

    <div class="header__menu">
      <ul class="menu"><li class="first expanded"><input type="radio" name="cope-menu-expander" id="cope-menu-expander"/><span class="nolink">Guidance</span><span class="description">Search through our collection of resources which make up all guidance issued by COPE.</span><label for="cope-menu-expander">Expand Guidance sub-menu</label><ul class="menu"><li class="first leaf"><a href="/guidance" title="">Find guidance</a></li>
<li class="leaf"><a href="/guidance/Flowcharts" title="">Flowcharts</a></li>
<li class="leaf"><a href="/guidance/Guidelines" title="">Guidelines</a></li>
<li class="last leaf"><a href="/guidance/Case" title="">Cases</a></li>
</ul></li>
<li class="expanded"><input type="radio" name="cope-menu-expander" id="cope-menu-expander--2"/><span class="nolink">Member resources</span><span class="description">COPE offers a wide range of member-only benefits and services.</span><label for="cope-menu-expander--2">Expand Member resources sub-menu</label><ul class="menu"><li class="first leaf"><a href="/copeforum">COPE Forum</a></li>
<li class="last leaf"><a href="/resources/elearning" title="">eLearning</a></li>
</ul></li>
<li class="last expanded"><input type="radio" name="cope-menu-expander" id="cope-menu-expander--3"/><span class="nolink">About COPE</span><span class="description">Become a member, find an existing member, or read more about COPE and what we do.</span><label for="cope-menu-expander--3">Expand About COPE sub-menu</label><ul class="menu"><li class="first collapsed"><a href="/about/our-organisation" title="">Our organisation</a></li>
<li class="leaf"><a href="/core-practices" title="">Core Practices</a></li>
<li class="leaf"><a href="/members" title="">Find a member</a></li>
<li class="last leaf"><a href="/become-member">Become a member</a></li>
</ul></li>
</ul>    </div>

    <ul class="header__user">
              <li><a href="/user/login" class="icon-user">Sign in</a></li>
          </ul>

    <form class="header__search" action="/search" method="get">
      <div class="header__search__inner">
        <label class="header__search__label"for="header__search__input">Search our website</label>
        <input class="header__search__input" title="Enter the terms you wish to search for." placeholder="Search our website" type="text" id="header__search__input" name="t" value="" size="15" maxlength="128">
        <input class="header__search__submit" type="submit" value="Search" class="form-submit">
      </div>
    </form>
  </div>
</header>



<div class="hero hero--dark">
  <div class="hero__row">
    <div class="hero__col">

        
      <div class="hero__breadcrumb">
              </div>

      <div class="hero__title">
        
  <a id="main-content"></a>
        <h1 id="page-title">Promoting integrity in research and its publication</h1>
    
      </div>

              <div class="hero__lead">
          <div class="entity entity-paragraphs-item paragraphs-item-hero">
  <div class="content">
    <div class="field field-name-field-hero-intro field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>COPE provides leadership in thinking on publication ethics and practical resources to educate and support members, and offers a professional voice in current debates.</p>
</div></div></div><div class="field field-name-field-link field-type-link-field field-label-hidden"><div class="field-items"><div class="field-item even"><a href="https://publicationethics.org/about/our-organisation">Read more</a></div></div></div>  </div>
</div>
        </div>
      
      
  
  

      <div class="hero__bottom">
              </div>
    </div>
  </div>
</div>


<div class="page-main">
  <div class="page-main__row">
    <div class="page-main__col">
        
  
  <main>
    
      <section id="content" class="region region-content">
        <div id="block-system-main" class="block block-system clearfix">

    
  <div class="content">
    <div class="entity entity-bean bean-homepage clearfix">

  <div class="content">
    
<div class="paragraphs-items paragraphs-items-field-hot-topic paragraphs-items-field-hot-topic-full paragraphs-items-full">
  <div class="field field-name-field-hot-topic field-type-paragraphs field-label-hidden"><div class="field-items"><div class="field-item even"><div class="hot-topic">
  <div class="hot-topic__box"></div>
  <div class="hot-topic__box-content">
    <div class="hot-topic__category">Hot topic</div>
    <div class="hot-topic__title"><a href="https://publicationethics.org/retraction-guidelines">Retracting articles</a></div>
    <div class="hot-topic__summary">When should a retraction be considered? What should I include in a notice? How quickly should I issue a retraction? Who should issue a retraction? What can I do when there is inconclusive evidence of a retraction?</div>
    <div class="hot-topic__link"><a href="https://publicationethics.org/retraction-guidelines">Read COPE's retraction guidelines</a></div>
  </div>
</div>
</div></div></div></div>

<div class="paragraphs-items paragraphs-items-field-homepage-core-practices paragraphs-items-field-homepage-core-practices-full paragraphs-items-full">
  <div class="field field-name-field-homepage-core-practices field-type-paragraphs field-label-hidden"><div class="field-items"><div class="field-item even"><div class="homepage-core-practices">
  <div class="homepage-core-practices__row homepage-core-practices__row--title">
    <h2 class="homepage-core-practices__title">Our core practices</h2>
  </div>

  <div class="homepage-core-practices__row homepage-core-practices__row--body">
    <div class="homepage-core-practices__col">
      <div class="homepage-core-practices__summary">
        <p>Core practices are the policies and practices journals and publishers need, to reach the highest standards in publication ethics. We include cases with advice, guidance for day-to-day practice, education modules and events on topical issues, to support journals and publishers fulfil their policies.</p>
      </div>
      <div class="homepage-core-practices__cta">
        <a href="https://publicationethics.org/core-practices">Read more about core practices</a>      </div>
    </div>

    <ul class="homepage-core-practices__col homepage-core-practices__col--links">
              <li><a href="/misconduct">Allegations of misconduct</a></li>
              <li><a href="/authorship">Authorship and contributorship</a></li>
              <li><a href="/appeals">Complaints and appeals</a></li>
              <li><a href="/competinginterests">Conflicts of interest</a></li>
              <li><a href="/data">Data and reproducibility</a></li>
              <li><a href="/oversight">Ethical oversight</a></li>
              <li><a href="/intellectualproperty">Intellectual property</a></li>
              <li><a href="/management">Journal management</a></li>
              <li><a href="/peerreview">Peer review processes</a></li>
              <li><a href="/postpublication">Post-publication discussions</a></li>
          </ul>
  </div>
</div>
</div></div></div></div>

<div class="paragraphs-items paragraphs-items-field-forum paragraphs-items-field-forum-full paragraphs-items-full">
  <div class="field field-name-field-forum field-type-paragraphs field-label-hidden"><div class="field-items"><div class="field-item even"><div class="next-cope-forum">

  <div class="next-cope-forum__container">

    <div class="next-cope-forum__row">

      <div class="next-cope-forum__description">
        <h2 class="next-cope-forum__title"><a href="https://publicationethics.org/copeforum">COPE Forum</a></h2>
        <div class="next-cope-forum__date"></div>
        <p class="next-cope-forum__body">Our next Forum, where members discuss and advise on current cases, will be held by webinar in February. If you have a case you can submit it to be discussed at the next Forum</p>
      </div>

      <div class="next-cope-forum__cta"><a href="https://publicationethics.org/copeforum">Submit your case</a></div>

    </div>
  </div>

</div>
</div></div></div></div>

<div class="paragraphs-items paragraphs-items-field-promo-slices paragraphs-items-field-promo-slices-full paragraphs-items-full">
  <div class="field field-name-field-promo-slices field-type-paragraphs field-label-hidden"><div class="field-items"><div class="field-item even"><div class="single-promo-slice single-promo-slice--grey">
    <div class="single-promo-slice__row">
    <div class="single-promo-slice__image">
      <img src="https://publicationethics.org/files/cope_retractions_home_1024x768.png" width="1024" height="768" alt="COPE&#039;s guidelines to retracting articles" title="Retraction Guidelines" />    </div>

    <div class="single-promo-slice__slice">
      <div class="single-promo-slice__title"><a href="https://publicationethics.org/retraction-guidelines">NEW! COPE Retraction Guidelines</a></div>
      
      <div class="single-promo-slice__description">
        <p>NEW! COPE Retraction Guidelines updated. Advice and guidance for editors: when should a retraction be considered, what to include in a notice, how quickly to issue a retraction, who should issue a retraction, and what to do when there is inconclusive evidence of a retraction. #Retractions #PublicationEthics</p></div>
      <div class="single-promo-slice__cta"><a href="https://publicationethics.org/retraction-guidelines">Read guidelines</a></div>
    </div>
  </div>
</div>
</div><div class="field-item odd"><div class="multi-promo-slice multi-promo-slice--white">
  
  <div class="multi-promo-slice__row">
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://publicationethics.org/events/cope-peer-review-workshop-pre-munin-conference-norway-2019">COPE Workshop, Tromsø, Norway</a></div>
                    <div class="multi-promo-slice__description">
            <p>COPE workshop on standards of peer review, 26 November at @UiTromso. With editor, author and reviewer perspectives and a cases workshop with COPE&#039;s Howard Browman and Mirjam Curno.</p></div>
          <div class="multi-promo-slice__cta"><a href="https://publicationethics.org/events/cope-peer-review-workshop-pre-munin-conference-norway-2019">Read more</a></div>
        </div>
      </div>
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://publicationethics.org/resources/seminars-and-webinars/european-seminar-2019-analysis-retractions-initiators-and-reasons">Retractions session at COPE European Seminar</a></div>
                    <div class="multi-promo-slice__description">
            <p>The reasons for retractions, motivation for retraction and who retracts; and a publisher&#039;s perspective. Presentations from Howard Browman, Thed van Leeuwen and Catriona Fennell #C0PE2019</p></div>
          <div class="multi-promo-slice__cta"><a href="https://publicationethics.org/resources/seminars-and-webinars/european-seminar-2019-analysis-retractions-initiators-and-reasons">See presentations</a></div>
        </div>
      </div>
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://publicationethics.org/ai_decision_making_forum_discussion">Artificial intelligence (AI) in decision making</a></div>
                    <div class="multi-promo-slice__description">
            <p>With the advancement of AI and the introduction of data-driven solutions to many processes throughout the publication process, questions surrounding the relevant ethics arise as to if, when, and how AI could/should be used. Join the discussion
</p></div>
          <div class="multi-promo-slice__cta"><a href="https://publicationethics.org/ai_decision_making_forum_discussion">Your comments</a></div>
        </div>
      </div>
      </div>
</div>
</div><div class="field-item even"><div class="single-promo-slice single-promo-slice--white">
      <div class="single-promo-slice__row">
      <div class="single-promo-slice__heading">
        <h2>News &amp; opinion</h2>
      </div>
    </div>
    <div class="single-promo-slice__row">
    <div class="single-promo-slice__image">
      <img src="https://publicationethics.org/files/cope_authorship_home_1024_aw_0.png" width="1024" height="768" alt="Authorship discussion document" title="Authorship discussion document" />    </div>

    <div class="single-promo-slice__slice">
      <div class="single-promo-slice__title"><a href="https://publicationethics.org/authorship-discussion-document">Authorship Discussion Document</a></div>
      
      <div class="single-promo-slice__description">
        <p>Updated Authorship DIscussion Document provides practical advice on addressing the most common issues around authorship with specific guidance where there seems to be consensus and asking for further comments from COPE members in some areas.</p></div>
      <div class="single-promo-slice__cta"><a href="https://publicationethics.org/authorship-discussion-document">Read discussion document</a></div>
    </div>
  </div>
</div>
</div><div class="field-item odd"><div class="multi-promo-slice multi-promo-slice--grey">
  
  <div class="multi-promo-slice__row">
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://mailchi.mp/publicationethics/cope-digest-november-2019-ethical-oversight-retraction-guidelines">COPE Digest: November</a></div>
                    <div class="multi-promo-slice__description">
            <p>Out now! #C0PEDigest focuses on ethical oversight with a case discussion on the ethics of non-active management of a control group. New advice and guidance for editors on retracting articles is now available and we&#039;re asking members to consider joining COPE Council. Plus the monthly roundup of news and events. #EthicalOversight</p></div>
          <div class="multi-promo-slice__cta"><a href="https://mailchi.mp/publicationethics/cope-digest-november-2019-ethical-oversight-retraction-guidelines">Read Digest newsletter</a></div>
        </div>
      </div>
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://publicationethics.org/news/ethics-non-active-management-control-group">Case discussion: Ethical Oversight</a></div>
                    <div class="multi-promo-slice__description">
            <p>The case we discuss in our November issue of COPE Digest explores the ethics of routine antenatal care in pregnant women with a systematic illness compared to active management of the illness in the intervention group.</p></div>
          <div class="multi-promo-slice__cta"><a href="https://publicationethics.org/news/ethics-non-active-management-control-group">Discussion and advice</a></div>
        </div>
      </div>
          <div class="multi-promo-slice__slice-col">
        <div class="multi-promo-slice__slice">
          <div class="multi-promo-slice__title"><a href="https://publicationethics.org/case/retrospective-registration-outcome-switching-and-ethical-approval">Retrospective registration, outcome switching and ethical approval</a></div>
                    <div class="multi-promo-slice__description">
            <p>A case came to the Forum last year, with concerns about the trial registry record and the published paper. The editor asked whether a retraction was necessary. New update on the case from the editor. Read more...  #EthicalOversight</p></div>
          <div class="multi-promo-slice__cta"><a href="https://publicationethics.org/case/retrospective-registration-outcome-switching-and-ethical-approval">Read more</a></div>
        </div>
      </div>
      </div>
</div>
</div><div class="field-item even"><div class="multi-promo-slice multi-promo-slice--white">
  
  <div class="multi-promo-slice__row">
      </div>
</div>
</div></div></div></div>
  </div>
</div>
  </div>
</div>
      </section>

  </main>

      </div>
  </div>
</div>

<footer class="footer">
  <div class="footer__outer">
    <div class="footer__row">
      <div class="footer__col">
                <div class="footer__menu">
          <h2>Useful links</h2>
          <ul class="links menu-site-map"><li class="menu-4141 first"><a href="/become-member" title="">Become a member</a></li>
<li class="menu-4140"><a href="/contact-us" title="">Contact Us</a></li>
<li class="menu-4137"><a href="/events" title="">Events</a></li>
<li class="menu-4139"><a href="/guidance" title="">Guidance</a></li>
<li class="menu-4136"><a href="/members" title="">Membership</a></li>
<li class="menu-4135 last"><a href="/newsopinion" title="">News &amp; Opinion</a></li>
</ul>        </div>
        
        <div class="footer__newsletter">
            <section id="newsletter_footer_widget" class="region region-newsletter-footer-widget">
    <div id="block-mailchimp-signup-newsletter-signup" class="block block-mailchimp-signup clearfix">

    <h2 class="title">Sign up to COPE&#039;s latest news</h2>
  
  <div class="content">
    <form class="mailchimp-signup-subscribe-form drupal-form" action="/" method="post" id="mailchimp-signup-subscribe-block-newsletter-signup-form" accept-charset="UTF-8"><div><div class="mailchimp-signup-subscribe-form-description"></div><div id="mailchimp-newsletter-6eb8aec9d3-mergefields" class="mailchimp-newsletter-mergefields"><div class="form-item form-type-textfield form-item-mergevars-EMAIL">
  <label class="element-invisible" for="edit-mergevars-email">Email Address <span class="form-required" title="This field is required.">*</span></label>
 <input placeholder="Your email address" type="text" id="edit-mergevars-email" name="mergevars[EMAIL]" value="" size="25" maxlength="128" class="form-text required" />
</div>
</div><input type="hidden" name="form_build_id" value="form-0omi82jH4niMMsAlZ03Kybb18y2dCXboNsdO1PHTQeQ" />
<input type="hidden" name="form_id" value="mailchimp_signup_subscribe_block_newsletter_signup_form" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Sign up" class="form-submit" /></div></div></form>  </div>
</div>
  </section>
        </div>
      </div>

      <div class="footer__social">
        <h2>Connect with COPE</h2>
        <ul class="share">
  <li class="facebook"><a href="https://www.facebook.com/publicationethics" target="_blank">Facebook</a></li>
  <li class="twitter"><a href="https://twitter.com/C0PE" target="_blank">Twitter</a></li>
  <li class="linkedin"><a href="https://www.linkedin.com/company/committee-on-publication-ethics" target="_blank">Linkedin</a></li>
</ul>
      </div>
    </div>

    <div class="footer__row">
            <div class="footer__col--full">
        <div class="footer__menu footer__menu--divided">
          <ul class="links menu-footer"><li class="menu-15926 first"><a href="/cope-newsletter" title="">COPE Newsletter</a></li>
<li class="menu-1698"><a href="/about/governance">Governance</a></li>
<li class="menu-1949"><a href="/about/press" title="">Press</a></li>
<li class="menu-2876"><a href="https://www.facebook.com/publicationethics" title="">COPE on Facebook</a></li>
<li class="menu-1946"><a href="/about/press">Journalist</a></li>
<li class="menu-2875"><a href="https://twitter.com/C0PE" title="">COPE on Twitter</a></li>
<li class="menu-9651"><a href="/about/subcommittees">Subcommittees</a></li>
<li class="menu-1753"><a href="/privacy">Privacy Policy</a></li>
<li class="menu-1755"><a href="/termsandconditions">Website Terms and Conditions</a></li>
<li class="menu-9671"><a href="/about/trustees-reports-and-financial-statements">Trustees Reports and Financial Statements</a></li>
<li class="menu-1697 last"><a href="/about/history">History of COPE</a></li>
</ul>        </div>
      </div>
          </div>

    <div class="footer__copyright-container">
      <div class="footer__copyright">
        <p>Registered charity No 1123023, Registered in England and Wales, Company No 6389120, Registered office: COPE, New Kings Court, Tollgate, Chandler's Ford, Eastleigh, Hampshire, SO53 3LG, United Kingdom</p>
        <p>© Committee on Publication Ethics 2019</p>
      </div>
    </div>
  </div>
</footer>

  <script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://publicationethics.org/files/js/js_Llgek5Zasqh0wiimoKH-uIdmSIEO0i9Cbi7UdXEdRgw.js"></script>
</body>
</html>
