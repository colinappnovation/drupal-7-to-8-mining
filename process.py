#!/usr/bin/env python3

import urllib.request as req
import urllib
import http
import pathlib as p
import ssl
import pickle

# bypass SSL certificate errors.
gcontext = ssl.SSLContext()

# Multprocessor capable
import multiprocessing as mp
pool = mp.Pool(mp.cpu_count())

# Open input file
f = open("data2.txt", "r")

# read the lines into memory and close
lines = f.readlines()
f.close()

# SSL failures list
ssl_failures = []
f_ssl_failures = open('ssl_failures.txt','w')

def getHomepage(hp):
    try:

        opener = req.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        req.install_opener(opener)

        # Check for homepage existing
        file = p.Path(f"homepages-2/{hp}")
        if file.exists():
            print(f"Skipping processing: {hp} ...")
        else:
            # Grab the homepage
            req.urlretrieve(f"https://www.{hp}", f"homepages-2/{hp}")
            print(f"Processed: {hp}")
    except urllib.error.HTTPError as err:
        print(f"HTTPError: Cannot process: {hp} with error: {err}")        
    except urllib.error.URLError as err:
        print(f"URLError: Cannot process: {hp} with error: {err}")  
        ssl_failures.append(hp) 
        f_ssl_failures.writelines(hp)        
        f_ssl_failures.flush()

    except http.client.RemoteDisconnected as err:
        print(f"RemoteDisconnected: Cannot process: {hp} with error: {err}")        
    # finally:         
    #     # Write out the failures        
    #     pickle.dump(f, ssl_failures)  
    #     # pool.close()       


pool.map(getHomepage, lines)

# Clean up resources
pool.close()
f_ssl_failures.close()


