#!/usr/bin/env python3

import os
import mmap
from bs4 import BeautifulSoup
import re

# access the directory and search string definition
directory = os.listdir("homepages-2")
searchstring = "drupal"

# list for later
found = []

for fname in directory:
    if os.path.isfile('./homepages-2' + os.sep + fname):
        try:
            with open('./homepages-2' + os.sep + fname, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                soup = BeautifulSoup(s, 'html.parser')
                generator = soup.find('meta', attrs={'name':'generator'})
                
                if generator is not None:
                     m = re.search(r'Drupal\s([0-9])', generator['content'])
                     
                     if m is not None and m.group(0) == 'Drupal 7':
                        found.append(fname)                        
                                    
        except ValueError:
           print(f'Failed processing: {fname} due to zero byte file')
     

# sort alpha
found.sort()

# print number found
print(f"Found Drupal in {len(found)} files out of {len(directory)}")

with open('drupal-matches.txt', 'w') as f:
    print(found, file=f)
