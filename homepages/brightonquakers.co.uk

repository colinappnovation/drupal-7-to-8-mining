<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/content/brighton-friends-meeting-house" />
<link rel="shortlink" href="/node/71" />
<link rel="shortcut icon" href="https://www.brightonquakers.co.uk/sites/default/files/furniture/favicon.ico" type="image/vnd.microsoft.icon" />
  <title>Brighton Friends&#039; Meeting House | Brighton Quakers</title>
  <link type="text/css" rel="stylesheet" href="https://www.brightonquakers.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.brightonquakers.co.uk/sites/default/files/css/css_0rbwqHFVVlno8xnEjn9I_S84Ni2t3NjgWtn4n4Pk6MU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.brightonquakers.co.uk/sites/default/files/css/css_nmTwcoEmoiqvv4zSXCk7dmQDK1uWbtpKtrhh4GcytNk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.brightonquakers.co.uk/sites/default/files/css/css_UqONB1sQ9oQB0m5XCAaUfcl1HESh2BvZgT2h5NPymiw.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="//code.jquery.com/jquery-1.7.2.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.brightonquakers.co.uk/sites/default/files/js/js_dWhBODswdXXk1M5Z5nyqNfGljmqwxUwAK9i6D0YSDNs.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"brighton_quakers","theme_token":"teh7SkEcou20N9jW2Rwh47bj1DTO6QNsJM82kQI4gyY","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/code.jquery.com\/jquery-1.7.2.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/wysiwyg_linebreaks\/wysiwyg_linebreaks.css":1,"sites\/all\/themes\/brighton_quakers\/bootstrap\/css\/bootstrap.min.css":1,"sites\/all\/themes\/brighton_quakers\/css\/style.css":1}},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":1,"placement":"top","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="navbar-is-static-top html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-71 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <header id="navbar" role="banner" class="navbar navbar-static-top navbar-default">
  <div class="container container">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="https://www.brightonquakers.co.uk/sites/default/files/furniture/quaker.png" alt="Home" />
        </a>
      
              <a class="name navbar-brand" href="/" title="Home">Brighton Quakers</a>
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first collapsed"><a href="/content/quakers-brighton" title="Quaker meeting times in Brighton">Quakers in Brighton</a></li>
<li class="collapsed"><a href="/content/hiring-rooms" title="Info. about hiring the rooms in the Quaker Meeting House">Hiring Rooms</a></li>
<li class="collapsed"><a href="/content/room-tour" title="See the inside of the building">Room Tour</a></li>
<li class="last leaf"><a href="/contact" title="Contact Us">Contact</a></li>
</ul>                                      </nav>
      </div>
      </div>
</header>

<div class="main-container container container">

  <header role="banner" id="page-header">
    
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-9">
                  <a id="main-content"></a>
                    <h1 class="page-header">Brighton Friends&#039; Meeting House</h1>
                                                          <div class="region region-content well well-sm">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-71" class="node node-page clearfix" about="/content/brighton-friends-meeting-house" typeof="foaf:Document">
    <header>
            <span property="dc:title" content="Brighton Friends&#039; Meeting House" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><div class="hero" style="background-image: url(/sites/default/files/carousel_images/Brighton_Quaker_Meeting_House_1200x480_0.jpg);">
<div class="blue container">
<div class="hero-inner">
<div class="hero-inner-text">
<h1 class="hero-heading">Brighton Quakers</h1>
</div>
</div>
</div>
</div>
<p>&nbsp;</p>
</div></div></div>    </article>

</section>
  </div>
        <div class="region region-under-content">
    <section id="block-views-home-page-links-block" class="block block-views clearfix">

      
  <div class="view view-home-page-links view-id-home_page_links view-display-id-block view-dom-id-e3b4c9a2033a5b9471380a10fb2e9ae5">
        
  
  
      <div class="view-content">
      <table class="views-view-grid cols-2">
  
  <tbody>
          <tr  class="row-1 row-first">
                  <td  class="col-1 col-first col-md-6 vtop">
            <article id="node-5" class="node node-page node-teaser clearfix" about="/content/quakers-brighton" typeof="foaf:Document">
    <header>
            <h2><a href="/content/quakers-brighton">Quakers in Brighton</a></h2>
        <span property="dc:title" content="Quakers in Brighton" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Quakerism is a way of life rooted in a transforming experience of the Divine. From this we seek to live out our principles of truth, peace, simplicity and equality, recognising that of God in everyone. Our Meeting offers a place of welcome, encounter and spiritual exploration.</p>
</div></div></div>     <footer>
          <ul class="links list-inline"><li class="node-readmore first last"><a href="/content/quakers-brighton" rel="tag" title="Quakers in Brighton">Read more<span class="element-invisible"> about Quakers in Brighton</span></a></li>
</ul>  </footer>
      </article>
          </td>
                  <td  class="col-2 col-last col-md-6 vtop">
            <article id="node-69" class="node node-page node-teaser clearfix" about="/content/newsletter-november-2019" typeof="foaf:Document">
    <header>
            <h2><a href="/content/newsletter-november-2019">Newsletter for November 2019</a></h2>
        <span property="dc:title" content="Newsletter for November 2019" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Newsletter for November 2019</p>
</div></div></div>     <footer>
          <ul class="links list-inline"><li class="node-readmore first last"><a href="/content/newsletter-november-2019" rel="tag" title="Newsletter for November 2019">Read more<span class="element-invisible"> about Newsletter for November 2019</span></a></li>
</ul>  </footer>
      </article>
          </td>
              </tr>
          <tr  class="row-2 row-last">
                  <td  class="col-1 col-first col-md-6 vtop">
            <article id="node-68" class="node node-page node-promoted node-sticky node-teaser clearfix" about="/content/hirings-policy-2019" typeof="foaf:Document">
    <header>
            <h2><a href="/content/hirings-policy-2019">Hirings Policy 2019</a></h2>
        <span property="dc:title" content="Hirings Policy 2019" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><h1> </h1>
<p><strong>Statement from Brighton Quaker Meeting Finance and Property Committee on our bookings policy</strong></p>
<p>The management of the bookings at Brighton Quaker Meeting House is done by our Wardens in the first instance. If the Wardens have doubts about any person or group who wish to hire our building, they will refer the matter to the Finance and Property Committee who will make the final decision</p>
<p>This statement endeavours to explain the criteria used for such decisions</p></div></div></div>     <footer>
          <ul class="links list-inline"><li class="node-readmore first last"><a href="/content/hirings-policy-2019" rel="tag" title="Hirings Policy 2019">Read more<span class="element-invisible"> about Hirings Policy 2019</span></a></li>
</ul>  </footer>
      </article>
          </td>
                  <td  class="col-2 col-last col-md-6 vtop">
            <article id="node-2" class="node node-page node-teaser clearfix" about="/content/hiring-rooms" typeof="foaf:Document">
    <header>
            <h2><a href="/content/hiring-rooms">Hiring Rooms</a></h2>
        <span property="dc:title" content="Hiring Rooms" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Interested in hiring rooms? Here's where to start.</p>
<p>We know there have been strong feelings aroused regarding the booking and cancellation of a talk by Chris Williamson MP. We have not had time to consider this as a whole Quaker community in Brighton and will respond more thoroughly once we have done so.</p>
</div></div></div>     <footer>
          <ul class="links list-inline"><li class="node-readmore first last"><a href="/content/hiring-rooms" rel="tag" title="Hiring Rooms">Read more<span class="element-invisible"> about Hiring Rooms</span></a></li>
</ul>  </footer>
      </article>
          </td>
              </tr>
      </tbody>
</table>
    </div>
  
  
  
  
  
  
</div>
</section>
<section id="block-views-top-downloads-block" class="block block-views clearfix">

        <h2 class="block-title">Top Downloads</h2>
    
  <div class="view view-top-downloads view-id-top_downloads view-display-id-block view-dom-id-324a78d7a4046b74965191b795b1750c">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/booking-form">Booking Form</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
    </section>

          <aside class="col-sm-3" role="complementary">
          <div class="region region-sidebar-second well well-sm">
    <section id="block-block-11" class="block block-block clearfix">

      
  <p><a class="twitter-timeline" data-height="460" href="https://twitter.com/BrightonQuakers">Tweets by BrightonQuakers</a> </p>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p>

</section>
<section id="block-block-10" class="block block-block clearfix">

        <h2 class="block-title">Video</h2>
    
  <p>Watch our home-grown video, &#39;<a href="/content/who-are-quakers-video">Who are the Quakers?</a>&#39;</p>
<p><a href="/content/who-are-quakers-video"><div class="media media-element-container media-media_original"><div id="file-100" class="file file-image file-image-png">

        <h2 class="element-invisible"><a href="/file/100">who-are-the-quakers-video-244.png</a></h2>
    
  
  <div class="content">
    <img alt="" class="media-image img-responsive" height="183" width="244" typeof="foaf:Image" src="https://www.brightonquakers.co.uk/sites/default/files/who-are-the-quakers-video-244.png" title="" />  </div>

  
</div>
</div></a></p>

</section>
<section id="block-block-8" class="block block-block clearfix">

        <h2 class="block-title">Map</h2>
    
  <p style='width:100%'>
<iframe width="100%" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.co.uk/?q=BN1+1AF&amp;ie=UTF8&amp;hq=&amp;hnear=BN1+1AF,+United+Kingdom&amp;ll=50.822486,-0.141454&amp;spn=0.011346,0.026822&amp;t=m&amp;z=14&amp;output=embed" width="42px"></iframe><p><small><a href="https://maps.google.co.uk/?q=BN1+1AF&amp;ie=UTF8&amp;hq=&amp;hnear=BN1+1AF,+United+Kingdom&amp;ll=50.822486,-0.141454&amp;spn=0.011346,0.026822&amp;t=m&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small></p>

</section>
<section id="block-views-top-downloads-block--2" class="block block-views clearfix">

        <h2 class="block-title">Top Downloads</h2>
    
  <div class="view view-top-downloads view-id-top_downloads view-display-id-block view-dom-id-f074ac16ad4e186273642672e3d1b4cc">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/booking-form">Booking Form</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>
</section>
<section id="block-block-4" class="block block-block clearfix">

        <h2 class="block-title">Contact Us</h2>
    
  <p><strong>Friends Meeting House</strong><br />Ship Street<br />Brighton<br />BN1 1AF</p>
<p><a href="mailto:admin@brightonquakers.net?subject=Message%20from%20brightonquakers.co.uk">E-mail link</a><br />Telephone: 01273 770258</p>

</section>
  </div>
      </aside>  <!-- /#sidebar-second -->
    
  </div>
</div>

  <footer class="footer container">
      <div class="region region-footer">
    <section id="block-block-9" class="block block-block clearfix">

      
  <p><a href="/sitemap">Sitemap</a></p>

</section>
<section id="block-system-powered-by" class="block block-system clearfix">

      
  <span>Powered by <a href="https://www.drupal.org">Drupal</a></span>
</section>
  </div>
  </footer>
  <script src="https://www.brightonquakers.co.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
