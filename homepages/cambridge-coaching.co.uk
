<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"

  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

	<head profile="http://www.w3.org/1999/xhtml/vocab"><!--start head section-->
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.cambridge-coaching.co.uk/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="shortlink" href="/node/8" />
<link rel="canonical" href="/what-coaching-offer" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
	  <title>What coaching offers | Cambridge Personal Coaching</title>
	  <style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/reset/reset.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/modules/system/system.base.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/system/system.menus.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/system/system.messages.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/system/system.theme.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/modules/comment/comment.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/field/theme/field.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/search/search.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/modules/user/user.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/modules/views/css/views.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/sites/all/modules/ctools/css/ctools.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/modules/webform/css/webform.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/grid/grid_1000.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/common.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/links.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/typography.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/forms.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/drupal.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/layout.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/primary-links.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/slideshow.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/secondary-links.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/blocks.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/node.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/comments.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/pages/maintenance-page.css?ni6cog");
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/camb-coaching.css?ni6cog");
</style>
<style type="text/css" media="print">
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/print/print.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/css3/css3.css?ni6cog");
</style>
<style type="text/css" media="all">
@import url("https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/css/css3/css3_graphics.css?ni6cog");
</style>
	  <script type="text/javascript" src="https://www.cambridge-coaching.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/misc/drupal.js?ni6cog"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/misc/textarea.js?v=7.34"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/sites/all/modules/webform/js/webform.js?ni6cog"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/js/banner/marinelli_banner_text.js?ni6cog"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/js/modernizer/modernizr.js?ni6cog"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/js/marinelli_marinelli.js?ni6cog"></script>
<script type="text/javascript" src="https://www.cambridge-coaching.co.uk/sites/all/themes/marinelli/js/topregion/marinelli_topregion.js?ni6cog"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"marinelli","theme_token":"fAqoB0pTP874ixq5mlQSBPSO8e3u1SmV1cJRFtGt57o","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/textarea.js":1,"sites\/all\/modules\/webform\/js\/webform.js":1,"sites\/all\/themes\/marinelli\/js\/banner\/marinelli_banner_text.js":1,"sites\/all\/themes\/marinelli\/js\/modernizer\/modernizr.js":1,"sites\/all\/themes\/marinelli\/js\/marinelli_marinelli.js":1,"sites\/all\/themes\/marinelli\/js\/topregion\/marinelli_topregion.js":1},"css":{"sites\/all\/themes\/marinelli\/css\/reset\/reset.css":1,"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/webform\/css\/webform.css":1,"sites\/all\/themes\/marinelli\/css\/grid\/grid_1000.css":1,"sites\/all\/themes\/marinelli\/css\/common.css":1,"sites\/all\/themes\/marinelli\/css\/links.css":1,"sites\/all\/themes\/marinelli\/css\/typography.css":1,"sites\/all\/themes\/marinelli\/css\/forms.css":1,"sites\/all\/themes\/marinelli\/css\/drupal.css":1,"sites\/all\/themes\/marinelli\/css\/layout.css":1,"sites\/all\/themes\/marinelli\/css\/primary-links.css":1,"sites\/all\/themes\/marinelli\/css\/slideshow.css":1,"sites\/all\/themes\/marinelli\/css\/secondary-links.css":1,"sites\/all\/themes\/marinelli\/css\/blocks.css":1,"sites\/all\/themes\/marinelli\/css\/node.css":1,"sites\/all\/themes\/marinelli\/css\/comments.css":1,"sites\/all\/themes\/marinelli\/css\/pages\/maintenance-page.css":1,"sites\/all\/themes\/marinelli\/css\/camb-coaching.css":1,"sites\/all\/themes\/marinelli\/css\/print\/print.css":1,"sites\/all\/themes\/marinelli\/css\/css3\/css3.css":1,"sites\/all\/themes\/marinelli\/css\/css3\/css3_graphics.css":1}},"marinelli":{"bartext":"Slide Down","bartext2":"Slide Up"}});
//--><!]]>
</script>
	</head>
	<!--[if lt IE 7 ]> <body class="marinelli ie6 html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <![endif]-->
    <!--[if IE 7 ]>    <body class="marinelli ie7 html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <![endif]-->
    <!--[if IE 8 ]>    <body class="marinelli ie8 html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <![endif]-->
    <!--[if IE 9 ]>    <body class="marinelli ie9 html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <![endif]-->
    <!--[if gt IE 9]>  <body class="marinelli html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <![endif]-->
    <!--[if !IE]><!--> <body class="marinelli html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-8 node-type-page"> <!--<![endif]-->
	  <div id="skip-link">
	    <a href="#content" title="Jump to the main content of this page" class="element-invisible">Jump to Content</a>
	  </div>
	  	  

<!--start framework container-->
<div class="container_12 width_1" id="totalContainer">
      <!--start top section-->
    <div id="top" class="outsidecontent">

              
      <!--start branding-->
      <div id="branding">

                  <div id="logo-container">
            <a href="/" title="Back to homepage" class="active"><img id="logo" typeof="foaf:Image" src="https://www.cambridge-coaching.co.uk/sites/default/files/logo_0.png" alt="Cambridge Personal Coaching" /></a>          </div>
        
                  <!--start title and slogan-->
          <div id="title-slogan">
                          <h1 id="site-title"><a href="/" title="Back to homepage" class="active">Cambridge Personal Coaching</a></h1>            
                          <h2 id="site-slogan"><a href="/" title="Back to homepage" class="active"><a href="mailto:enquiries@cambridge-coaching.co.uk">enquiries@cambridge-coaching.co.uk</a></a></h2>                      </div>
          <!--end title and slogan-->
        
      </div>
      <!--end branding-->

      
    </div>
    <!--end top section-->
  
      <!--start main menu-->
    <div id="navigation-primary" class="sitemenu">
      <ul id="primary" class="links clearfix main-menu"><li class="menu-218 first active"><a href="/" class="active">Home</a></li>
<li class="menu-566"><a href="/my-profile-marion-saunders">My Profile</a></li>
<li class="menu-633 last"><a href="/past-clients">Past Clients</a></li>
</ul>    </div>
    <!--end main menu-->
  
  <!--border start-->
  <div id="pageBorder" >
          <!--start advertise section-->
      <div id="header-images" >
                  <div id="header-image-text" class="marinelli-hide-no-js"><div id="header-image-text-data"><p id="header-image-description"><a href="#" class="bannerlink" title="See this content">description</a></p><h2 id="header-image-title"><a href="#" class="bannerlink" title="See this content">title</a></h2></div></div>                    <a href="/" class="active"><img class="slide" id="slide-number-0" longdesc="“The first step towards getting somewhere is to decide that you are not going to stay where you are.“ " typeof="foaf:Image" src="https://www.cambridge-coaching.co.uk/sites/default/files/banner/banner4.jpg" alt="“The first step towards getting somewhere is to decide that you are not going to stay where you are.“ " title="Pier Point Morgan" /></a>              </div>
      <!--end advertise-->
    		
		
    <!-- start contentWrapper-->
    <div id="contentWrapper">
      <!--start breadcrumb -->
            <!-- end breadcrumb -->
		
			
      <!--start innercontent-->
			<div id="innerContent">

        <!--start main content-->
				<div class="grid_8" id="siteContent">
						   				
	   					           	
		      
                      <h1 id="page-title">What coaching offers</h1>
          
                    
                      <div class="tab-container">
                          </div>
          
          
          
          <!--start drupal content-->
          <div id="content">
            <!-- start region -->
<div class="region region region-content">
  <div id="block-system-main" class="block block-system">
        <div class="content">
    
<div id="node-8" class="node node-page" about="/what-coaching-offer" typeof="foaf:Document">
  <div class="node-container">
    
    
    <div class="nodecontent">
                        <span property="dc:title" content="What coaching offers" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>      <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"> <div style="float: right; margin-left: 20px; margin:-right: 10px; width: 275px;">
<table>
<tr><td>
<img alt="" class="media-image" height="320" style="float: right; margin-left: 10px; margin-right: 10px" width="240" typeof="foaf:Image" src="https://www.cambridge-coaching.co.uk/sites/default/files/styles/large/public/path.jpg?itok=7_3Dnh5W" />
</td>
</tr>
<tr><td><div class="banner-font">
find your path ... realise your potentional.
</div></td></tr>
</table>
</div>

<p>Coaching is a working partnership where the coach helps the client to identify what they want to achieve and then facilitate their growth, exploring new possibilities in their professional and personal life.</p><p>The coach listens and then asks questions, enabling the client to find the answers within themselves-based upon their values, preferences, and unique perspective.</p><p>Values serve as a compass in our lives, pointing our what it means to be true to one’s self. Important decisions in your life are easier to make, and the outcomes more fulfilling when your personal values are honored on a regular basis.</p><p>I support and encourage people to take charge of their own potential.</p><p>&nbsp;</p><div><em>‘She let me forget the “ hows ” for just a moment and dare to dream.'</em></div><div><strong>Francis – business development</strong></div> </div></div></div>    </div>
          <div class="links-container">
              </div>
              <div class="clearfix">
              </div>
      </div><!--end node container-->
</div><!--end node-->  </div>
</div> <!-- /block --></div>
<!-- end region -->          </div>
          <!--end drupal content-->

          
          
        </div>
        <!--end main content-->
	 			
          		    		
		    		    
                  <!--start second sidebar-->
          <div class="grid_4 sidebar" id="sidebar-second"><!--start second sidebar-->
            <!-- start region -->
<div class="region region region-sidebar-second">
  <div id="block-block-2" class="block block-block">
        <div class="block-title">
      <h2 class="title">Gains to the client from coaching</h2>
    </div>
      <div class="content">
     <ul><li>Career development</li>
<li>Managing confidence and leadership</li>
<li>Finding clarity of direction</li>
<li>Positively coping with change or success</li>
<li>Personal development</li>
<li>Work/life balance</li>
</ul>  </div>
</div> <!-- /block --><div id="block-block-3" class="block block-block">
        <div class="block-title">
      <h2 class="title">Next Steps</h2>
    </div>
      <div class="content">
     <div><span style="font-size: small;"><em>Where change is being sought or imposed ...</em></span></div>
<div><span style="font-size: small;"><em>Engage with it ...</em></span></div>
<div><span style="font-size: small;"><em>Focus on the future ...</em></span></div>
<div><span style="font-size: small;">&nbsp;</span></div>

<ul><li><span style="font-size: small;">Where are you now?</span></li>
<li><span style="font-size: small;">Where do you want to be?</span></li>
<li><span style="font-size: small;">What is your motivation?</span></li></ul>
<div><span style="font-size: small;">&nbsp;</span></div>

<div><span style="font-size: small;"><strong>Free introductory phone call session.</strong></span></div>   </div>
</div> <!-- /block --><div id="block-webform-client-block-9" class="block block-webform">
        <div class="block-title">
      <h2 class="title">Get in touch</h2>
    </div>
      <div class="content">
    <form class="webform-client-form" enctype="multipart/form-data" action="/content/get-touch" method="post" id="webform-client-form-9" accept-charset="UTF-8"><div><div class="form-item webform-component webform-component-textfield webform-container-inline" id="webform-component-sender-name">
  <label for="edit-submitted-sender-name">Name <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-submitted-sender-name" name="submitted[sender_name]" value="" size="43" maxlength="40" class="form-text required" />
</div>
<div class="form-item webform-component webform-component-email webform-container-inline" id="webform-component-senders-e-mail">
  <label for="edit-submitted-senders-e-mail">E-mail <span class="form-required" title="This field is required.">*</span></label>
 <input class="email form-text form-email required" type="email" id="edit-submitted-senders-e-mail" name="submitted[senders_e_mail]" size="42" />
</div>
<div class="form-item webform-component webform-component-textarea" id="webform-component-senders-message">
  <label for="edit-submitted-senders-message">Message <span class="form-required" title="This field is required.">*</span></label>
 <div class="form-textarea-wrapper resizable"><textarea id="edit-submitted-senders-message" name="submitted[senders_message]" cols="40" rows="5" class="form-textarea required"></textarea></div>
</div>
<input type="hidden" name="details[sid]" />
<input type="hidden" name="details[page_num]" value="1" />
<input type="hidden" name="details[page_count]" value="1" />
<input type="hidden" name="details[finished]" value="0" />
<input type="hidden" name="form_build_id" value="form-di2a7a62_6QjQBNL69pzuQ4B5kA9IxOs6scg-0QRjfc" />
<input type="hidden" name="form_id" value="webform_client_form_9" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Submit" class="form-submit" /></div></div></form>  </div>
</div> <!-- /block --></div>
<!-- end region -->          </div>
          <!--end second sidebar-->
        						
						
				
      
      </div>
      <!--end innerContent-->


              <!--start underContent-->
				<div class="grid_12 outofContent" id="underContent">
          <!-- start region -->
<div class="region region region-undercontent">
  <div id="block-block-1" class="block block-block">
        <div class="content">
     <div style="margin-left: auto; margin-right: auto; text-align: center;"><span style="font-size: small;"><strong><span style="color: #333333; font-family: verdana,geneva;">To explore working together, please contact Marion through:</span></strong></span></div>
<div style="margin-left: auto; margin-right: auto; text-align: center;"><span style="font-size: small;"><strong><span style="color: #333333; font-family: verdana,geneva;"><a href="mailto:enquiries@cambridge-coaching.co.uk">enquiries@cambridge-coaching.co.uk</a></span></strong></span></div>
   </div>
</div> <!-- /block --></div>
<!-- end region -->        </div>
        <!--end underContent-->
          </div>
    <!--end contentWrapper-->

	</div>
  <!--close page border Wrapper-->

      <!--start footer-->
    <div id="footer" class="outsidecontent">
      <!-- start region -->
<div class="region region region-footer">
  <div id="block-system-main-menu" class="block block-system block-menu">
        <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/" class="active">Home</a></li>
<li class="leaf"><a href="/my-profile-marion-saunders">My Profile</a></li>
<li class="last leaf"><a href="/past-clients">Past Clients</a></li>
</ul>  </div>
</div> <!-- /block --><div id="block-block-4" class="block block-block">
        <div class="content">
     </br></br>
<div style="text-align:center;">Cambridge-Coaching.co.uk website uses cookies. By continuing to browse the site you are agreeing to our use of cookies.</div>
<div style="color: gray; text-align:center;">
<a style="color: gray;" href="terms-and-conditions">Terms & Conditions</a><span> | </span>
<a style="color: gray;" href="privacy-policy-cookies">Privacy Policy & Cookies</a>
</div>   </div>
</div> <!-- /block --></div>
<!-- end region -->
          </div>
    <!--end footer-->
  
</div>
<!--end framework container-->
	  	</body><!--end body-->
</html>