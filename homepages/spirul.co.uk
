<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<link rel="shortcut icon" href="https://spirul.co.uk/sites/default/files/favicon.png" type="image/png" />
<link rel="canonical" href="/insight-fit-action" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="shortlink" href="/node/5" />
  <title>Insight fit for action | Spirul</title>
  <style type="text/css" media="all">
@import url("https://spirul.co.uk/modules/system/system.base.css?psb5ie");
@import url("https://spirul.co.uk/modules/system/system.menus.css?psb5ie");
@import url("https://spirul.co.uk/modules/system/system.messages.css?psb5ie");
@import url("https://spirul.co.uk/modules/system/system.theme.css?psb5ie");
</style>
<style type="text/css" media="all">
@import url("https://spirul.co.uk/modules/field/theme/field.css?psb5ie");
@import url("https://spirul.co.uk/modules/node/node.css?psb5ie");
@import url("https://spirul.co.uk/modules/search/search.css?psb5ie");
@import url("https://spirul.co.uk/modules/user/user.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/modules/views/css/views.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?psb5ie");
</style>
<style type="text/css" media="all">
@import url("https://spirul.co.uk/sites/all/modules/ctools/css/ctools.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/modules/responsive_menus/styles/responsive_menus_simple/css/responsive_menus_simple.css?psb5ie");
</style>
<style type="text/css" media="all">
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/alpha-reset.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/alpha-mobile.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/alpha-alpha.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/formalize.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/omega-text.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/omega-branding.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/omega-menu.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/omega-forms.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/omega/css/omega-visuals.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/global.css?psb5ie");
</style>

<!--[if (lt IE 9)&(!IEMobile)]>
<style type="text/css" media="all">
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default-normal.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/normal/alpha-default-normal-12.css?psb5ie");
</style>
<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)">
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default-narrow.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/narrow/alpha-default-narrow-12.css?psb5ie");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)">
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default-normal.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/normal/alpha-default-normal-12.css?psb5ie");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 1220px)">
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/spirul/css/spirul-alpha-default-wide.css?psb5ie");
@import url("https://spirul.co.uk/sites/all/themes/omega/alpha/css/grid/alpha_default/wide/alpha-default-wide-12.css?psb5ie");
</style>
<!--<![endif]-->
  <script type="text/javascript" src="https://spirul.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://spirul.co.uk/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script type="text/javascript" src="https://spirul.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://spirul.co.uk/misc/drupal.js?psb5ie"></script>
<script type="text/javascript" src="https://spirul.co.uk/sites/all/modules/admin_menu/admin_devel/admin_devel.js?psb5ie"></script>
<script type="text/javascript" src="https://spirul.co.uk/sites/all/modules/responsive_menus/styles/responsive_menus_simple/js/responsive_menus_simple.js?psb5ie"></script>
<script type="text/javascript" src="https://spirul.co.uk/sites/all/modules/google_analytics/googleanalytics.js?psb5ie"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-56900285-6", {"cookieDomain":"auto"});ga("require", "displayfeatures");ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://spirul.co.uk/sites/all/themes/omega/omega/js/jquery.formalize.js?psb5ie"></script>
<script type="text/javascript" src="https://spirul.co.uk/sites/all/themes/omega/omega/js/omega-mediaqueries.js?psb5ie"></script>
<script type="text/javascript" src="https://spirul.co.uk/sites/default/files/js_injector/js_injector_1.js?psb5ie"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"spirul","theme_token":"JjsyNOz97eciLBncyQBxymTgwr2RY9Mp--FU92O2jxE","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/js\/responsive_menus_simple.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1,"sites\/default\/files\/js_injector\/js_injector_1.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/responsive_menus\/styles\/responsive_menus_simple\/css\/responsive_menus_simple.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/all\/themes\/spirul\/css\/global.css":1,"ie::normal::sites\/all\/themes\/spirul\/css\/spirul-alpha-default.css":1,"ie::normal::sites\/all\/themes\/spirul\/css\/spirul-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/all\/themes\/spirul\/css\/spirul-alpha-default.css":1,"narrow::sites\/all\/themes\/spirul\/css\/spirul-alpha-default-narrow.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/all\/themes\/spirul\/css\/spirul-alpha-default.css":1,"normal::sites\/all\/themes\/spirul\/css\/spirul-alpha-default-normal.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/all\/themes\/spirul\/css\/spirul-alpha-default.css":1,"wide::sites\/all\/themes\/spirul\/css\/spirul-alpha-default-wide.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"responsive_menus":[{"toggler_text":"\u2630 Menu","selectors":["#block-system-main-menu"],"media_size":"1250","media_unit":"px","absolute":"1","remove_attributes":"1","responsive_menus_style":"responsive_menus_simple"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>
</script>
<script src="https://use.typekit.net/nzz3dhv.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body class="html front not-logged-in page-node page-node- page-node-5 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-user-wrapper" class="zone-wrapper zone-user-wrapper clearfix">  
  <div id="zone-user" class="zone zone-user clearfix container-12">
    <div class="grid-8 region region-user-first" id="region-user-first">
  <div class="region-inner region-user-first-inner">
    <div class="block block-block header-address block-1 block-block-1 odd block-without-title" id="block-block-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>Bridge Mills, Holmfirth HD9 3TW</p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-4 region region-user-second" id="region-user-second">
  <div class="region-inner region-user-second-inner">
    <div class="block block-block header-telephone block-3 block-block-3 odd block-without-title" id="block-block-3">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><strong>01484 686377</strong></p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix container-12">
    <div class="grid-4 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="block block-delta-blocks block-branding block-delta-blocks-branding odd block-without-title" id="block-delta-blocks-branding">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="logo-img"><a href="/" id="logo" title="Return to the Spirul home page" class="active"><img typeof="foaf:Image" src="https://spirul.co.uk/sites/all/themes/spirul/logo.png" alt="Spirul" /></a></div><hgroup class="site-name-slogan"><h2 class="site-name"><a href="/" title="Return to the Spirul home page"><span>Spirul</span></a></h2></hgroup>    </div>
  </div>
</div>  </div>
</div><div class="grid-8 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
        <div class="block block-system block-menu block-main-menu block-system-main-menu odd block-without-title" id="block-system-main-menu">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="menu"><li class="first leaf"><a href="/attractions-festival-events">Attractions, Festivals &amp; Events</a></li>
<li class="leaf"><a href="/economic-impact">Economic Impact</a></li>
<li class="leaf"><a href="/brand-insight">Brand Insight</a></li>
<li class="last leaf"><a href="/contact">Contact</a></li>
</ul>    </div>
  </div>
</div>  </div>
</div>
  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-preface-wrapper" class="zone-wrapper zone-preface-wrapper clearfix">  
  <div id="zone-preface" class="zone zone-preface clearfix container-12">
    <div class="grid-12 region region-preface-first" id="region-preface-first">
  <div class="region-inner region-preface-first-inner">
      </div>
</div>  </div>
</div><div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-12">    
        
        <div class="grid-12 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                        <div class="block block-delta-blocks block-page-title block-delta-blocks-page-title odd block-without-title" id="block-delta-blocks-page-title">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <h1 id="page-title" class="title">Insight fit for action</h1>    </div>
  </div>
</div><div class="block block-system block-main block-system-main even block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <article about="/insight-fit-action" typeof="foaf:Document" class="node node-page node-published node-not-promoted node-not-sticky author-spirul-admin odd clearfix" id="node-page-5">
        <span property="dc:title" content="Insight fit for action" class="rdf-meta element-hidden"></span>    
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p><strong>Spirul’s robust and innovative approach to market research delivers a quality of insight that allow our clients to make sound strategic decisions with the highest levels of confidence.</strong></p>
<p>We are an experienced, independent full-service market research agency serving public and private sector clients throughout the UK.</p>
<p>Specialising in visitor attractions, festivals and events, economic development evaluation and brand strategy customer insight - our market-leading clients demand and enjoy a proactive, supportive and collaborative customer service and cost-effective and timely project delivery.</p>
<p>We select the best fit for your needs from a host of our data collection, preparation, qualitative and quantitative research, consultation, analysis and reporting services. Naturally, all work meets the Market Research Society codes of conduct.</p>
</div></div></div>  </div>
  
  <div class="clearfix">
          <nav class="links node-links clearfix"></nav>
    
      </div>
</article>    </div>
  </div>
</div>      </div>
</div>  </div>
</div><div id="zone-postscript-wrapper" class="zone-wrapper zone-postscript-wrapper clearfix">  
  <div id="zone-postscript" class="zone zone-postscript clearfix container-12">
    <div class="grid-12 region region-postscript-first" id="region-postscript-first">
  <div class="region-inner region-postscript-first-inner">
      </div>
</div>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-12 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <section class="block block-block footer-cta block-4 block-block-4 odd" id="block-block-4">
  <div class="block-inner clearfix">
              <h2 class="block-title">If you would like to find out more, please call</h2>
            
    <div class="content clearfix">
      <p>01484 686377</p>
    </div>
  </div>
</section>  </div>
</div>  </div>
</div><div id="zone-footer2-wrapper" class="zone-wrapper zone-footer2-wrapper clearfix">  
  <div id="zone-footer2" class="zone zone-footer2 clearfix container-12">
      </div>
</div><div id="zone-footer3-wrapper" class="zone-wrapper zone-footer3-wrapper clearfix">  
  <div id="zone-footer3" class="zone zone-footer3 clearfix container-12">
    <div class="grid-12 region region-footer-third" id="region-footer-third">
  <div class="region-inner region-footer-third-inner">
    <section class="block block-block keep-in-touch block-5 block-block-5 odd" id="block-block-5">
  <div class="block-inner clearfix">
              <h2 class="block-title">Keep in touch</h2>
            
    <div class="content clearfix">
      <ul>
<li class="rtecenter"><a class="facebook" href="https://www.facebook.com/spirulmarketresearch/" target="_blank">Facebook</a></li>
<li class="rtecenter"><a class="twitter" href="https://twitter.com/spirulresearch" target="_blank">Twitter</a></li>
<li class="rtecenter"><a class="linkedin" href="https://www.facebook.com/spirulmarketresearch/" target="_blank">LinkedIn</a></li>
</ul>
    </div>
  </div>
</section>  </div>
</div>  </div>
</div><div id="zone-footer4-wrapper" class="zone-wrapper zone-footer4-wrapper clearfix">  
  <div id="zone-footer4" class="zone zone-footer4 clearfix container-12">
    <div class="grid-12 region region-footer-fourth" id="region-footer-fourth">
  <div class="region-inner region-footer-fourth-inner">
    <div class="block block-block block-6 block-block-6 odd block-without-title" id="block-block-6">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p><a href="https://www.iubenda.com/privacy-policy/8279773">Privacy Policy</a> </p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <div class="region region-page-bottom" id="region-page-bottom">
  <div class="region-inner region-page-bottom-inner">
      </div>
</div></body>
</html>