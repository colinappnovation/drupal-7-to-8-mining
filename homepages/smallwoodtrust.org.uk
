<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.smallwoodtrust.org.uk/sites/all/themes/foundation/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="The Smallwood Trust has been helping women on low incomes since 1886, more than 130 years. Her Majesty The Queen has been our patron since 1953." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.smallwoodtrust.org.uk/home" />
<link rel="shortlink" href="https://www.smallwoodtrust.org.uk/node/8" />
<meta property="og:site_name" content="Smallwood Trust" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.smallwoodtrust.org.uk/home" />
<meta property="og:title" content="Impact Report 2019" />
<meta property="og:description" content="The Smallwood Trust has been helping women on low incomes since 1886, more than 130 years. Her Majesty The Queen has been our patron since 1953." />
<meta property="og:updated_time" content="2019-10-21T15:34:09+01:00" />
<meta property="article:published_time" content="2014-12-15T10:33:50+00:00" />
<meta property="article:modified_time" content="2019-10-21T15:34:09+01:00" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <title>Impact Report 2019 | Smallwood Trust</title>
  <link type="text/css" rel="stylesheet" href="https://www.smallwoodtrust.org.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.smallwoodtrust.org.uk/sites/default/files/css/css_tFrkhs6tW9ZJbgxRy_ORwSndXv2wHdR2449TDc8KokA.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.smallwoodtrust.org.uk/sites/default/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.smallwoodtrust.org.uk/sites/default/files/css/css_7kCSAns5F6F8plD9I1ZFOqLXQKi2ueiHwVqIpW1Di0g.css" media="all" />
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
document.cookie = 'adaptive_image=' + Math.max(screen.width, screen.height) + '; path=/';
//--><!]]>
</script>
<script type="text/javascript" src="https://www.smallwoodtrust.org.uk/sites/default/files/js/js_qikmINIYTWe4jcTUn8cKiMr8bmSDiZB9LQqvceZ6wlM.js"></script>
<script type="text/javascript" src="https://www.smallwoodtrust.org.uk/sites/default/files/js/js_Tik8PIaz_eQ5I4FMzmjkWoPEs9jKBgTSauo1jgsNa6g.js"></script>
<script type="text/javascript" src="https://www.smallwoodtrust.org.uk/sites/default/files/js/js_s-dNnJSi2xwA9XKIEvgzGt7cNdlvZhJ1d8U7xo_7QCA.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"foundation","theme_token":"eWTvJZ4hXfTApv3y-xuN5pFIqFJmXKIogufag-FlWBE","js":{"sites\/all\/modules\/addthis\/addthis.js":1,"0":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/extlink\/extlink.js":1,"sites\/all\/themes\/foundation\/scripts\/interface.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/adaptive_image\/css\/adaptive-image.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/extlink\/extlink.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/foundation\/styles\/foundation.css":1}},"better_exposed_filters":{"views":{"banner":{"displays":{"block_banner":{"filters":[]}}},"signposts_rc5_":{"displays":{"block_text":{"filters":[]},"block_accreditation":{"filters":[]}}},"news":{"displays":{"block_latest":{"filters":[]}}}}},"extlink":{"extTarget":"_blank","extClass":0,"extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":"","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":0,"mailtoLabel":"(link sends e-mail)"}});
//--><!]]>
</script>

</head>

<body class="html front not-logged-in no-sidebars page-node page-node- page-node-8 node-type-home" >

  <div id="skip-link">
	<a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>

    <div id="wrapper">

			<div id="mobile-header">
			<div class="site-logo">
				<a href="/" title="Home" rel="home"><img src="https://www.smallwoodtrust.org.uk/sites/all/themes/foundation/logo.png" alt="Home" /></a>
			</div>
			<div class="button-container">
				<button type="button" aria-label="Toggle navigation" class="menu-toggle">
					<span class="menu-button"></span>
				</button>
			</div><!-- /.button-container -->
		</div>

		<div id="header" class="section region-container">
			<div class="holder clearfix">

				
				<div class="site-info">
											<a href="/" title="Home" rel="home" id="logo"><img src="https://www.smallwoodtrust.org.uk/sites/all/themes/foundation/logo.png" alt="Home" /></a>
										<div id="site-name">
						<a href="/" title="Home" rel="home">
							<span class="title">Smallwood Trust</span>
													</a>
					</div>
				</div>

				  <div class="region region-header">
    <div id="block-menu-block-1" class="block block-menu-block">

    
  <div class="content">
    <div class="menu-block-wrapper menu-block-1 menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first leaf menu-mlid-713"><a href="/about">About</a></li>
<li class="expanded menu-mlid-3629"><a href="/grants">Grants</a><ul class="menu"><li class="first leaf menu-mlid-3638"><a href="/flexible-response-fund">Flexible Response Fund</a></li>
<li class="leaf menu-mlid-3636"><a href="/women-first">Women First Fund</a></li>
<li class="leaf menu-mlid-3637"><a href="/women-first-policy-support">Women First Policy Support</a></li>
<li class="last leaf menu-mlid-3635"><a href="/monitoring-and-evaluation">Monitoring and evaluation</a></li>
</ul></li>
<li class="leaf menu-mlid-3639"><a href="/cases">Case studies</a></li>
<li class="leaf menu-mlid-3628"><a href="/get-involved">Get involved</a></li>
<li class="leaf menu-mlid-563"><a href="/contact">Contact</a></li>
<li class="last leaf menu-mlid-2341"><a href="/donate" class="highlight">Donate</a></li>
</ul></div>
  </div>
</div>
  </div>

			</div>
		</div>
	<div id="page">

		
		
				<div id="banner" class="section region-container">
			  <div class="region region-banner">
    <div id="block-views-banner-block-banner" class="block block-views">

    
  <div class="content">
    <div class="view view-banner view-id-banner view-display-id-block_banner view-dom-id-7e927a2f8f47fe223837750618b23b4e">

			
	
	
	
	
	  <div class="views-rows clearfix">
  	  <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
    <div id="node-8-banner" class="node node-home node-promoted banner-image node-banner" style="background-image:url('https://www.smallwoodtrust.org.uk/sites/default/files/images/STImpactbanner_0.jpg')" about="/home" typeof="sioc:Item foaf:Document">
	<div class="cover">
		<div class="holder clearfix">
			<div class="content">
				<h1>Impact Report 2019</h1>
				<div class="field field-name-field-introduction field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>Our full annual impact data is summarised in this report so please read more about our journey so far. </p>
<p><a class="button" href="https://stories.smallwoodtrust.org.uk/impactreport2019/index.html">View report</a></p>
</div></div></div>			</div>
		</div>
	</div>
</div>
  </div>
  </div>
	
	
	
	
	
</div>  </div>
</div>
  </div>
		</div>
		
		<div id="main" class="default region-container">
			<div class="holder clearfix">

				
				
				
				<div id="content" class="section clearfix">
					<a id="main-content"></a>
										  <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-8" class="node node-home node-promoted" about="/home" typeof="sioc:Item foaf:Document">

	
	
	<div class="content clearfix">
	<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><div style="margin-left:auto; margin-right:auto; max-width:700px">
<h3 class="rtecenter">The Smallwood Trust has been helping women on low incomes since 1886, more than 130 years. Her Majesty The Queen has been our patron since 1953.</h3>
<p class="rtecenter">Our mission is to enable women to become financially resilient by equipping them with the skills they need to secure a confident financial future. We provide grants to organisations and individuals and work with selected partners to help women overcome financial adversity and to improve their social and emotional well-being.</p>
<p class="rtecenter"> </p>
</div>
<p> </p>
</div></div></div>	</div>

	
</div>
  </div>
</div>
<div id="block-views-signposts-rc5-block-text" class="block block-views">

    
  <div class="content">
    <div class="view view-signposts-rc5- view-id-signposts_rc5_ view-display-id-block_text view-col view-col-1-2 view-stack view-dom-id-3646b1b48be4906294702cd0e2b92be5">

			
	
	
	
	
	  <div class="views-rows clearfix">
  	  <div class="views-row views-row-1 views-row-odd views-row-first">
    <span class="viewtag">signpost</span>
<div class="entity entity-signpost signpost-text-signpost clearfix" about="/signpost/text_signpost/38" typeof="">
	<a href="https://www.smallwoodtrust.org.uk/flexible-response-fund" target="_self">
    		<span class="cover"><span class="border">
  		<span class="content">
  			<span class="title">Grants for individuals</span>
  			<span class="body">Find out about our Flexible Response Fund and enquire</span>  		  	  		</span>
    </span></span>
	</a>
</div>
  </div>
  <div class="views-row views-row-2 views-row-even views-row-last">
    <span class="viewtag">signpost</span>
<div class="entity entity-signpost signpost-text-signpost clearfix" about="/signpost/text_signpost/39" typeof="">
	<a href="https://www.smallwoodtrust.org.uk/grants" target="_self">
    		<span class="cover"><span class="border">
  		<span class="content">
  			<span class="title">Grants for organisations</span>
  			<span class="body">Browse our funds set up especially for organisations </span>  		  	  		</span>
    </span></span>
	</a>
</div>
  </div>
  </div>
	
	
	
	
	
</div>  </div>
</div>
  </div>
									</div>

				
			</div>
		</div>

				<div id="trailer" class="section region-container">
			<div class="holder clearfix">
				  <div class="region region-trailer">
    <div id="block-views-news-block-latest" class="block block-views">

    <h2>Latest news</h2>
  
  <div class="content">
    <div class="view view-news view-id-news view-display-id-block_latest view-col view-col-2-4 view-float view-dom-id-a48f0747840cc3215281352eb7d04d59">

			
	
	
	
	  <div class="views-more">
		
<div class="more-link">
  <a href="/news">
    View all news  </a>
</div>
  </div>
	
	  <div class="views-rows clearfix">
  	  <div class="views-row views-row-1 views-row-odd views-row-first">
    <div id="node-329-tile" class="node node-news node-promoted node-tile" about="/news/case-study-damsel-project" typeof="sioc:Item foaf:Document">
	<div class="imagetitle">
		<a href="/news/case-study-damsel-project">
			<span class="image"><img src="https://www.smallwoodtrust.org.uk/sites/default/files/styles/medium/public/images/hands.jpeg?itok=OhUoIjE2" alt="" /></span>			<span class="title">The Damsel project: supporting female entrepreneurs in Dorset</span>
		</a>
	</div>
	<div class="body">
		<div class="content clearfix">
			<div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-11-22T00:00:00+00:00">22 Nov 2019</span></div></div></div>		</div>
	</div>
</div>
  </div>
  <div class="views-row views-row-2 views-row-even">
    <div id="node-328-tile" class="node node-news node-promoted node-tile" about="/news/coordinating-crisis-support-partnership-childrens-society" typeof="sioc:Item foaf:Document">
	<div class="imagetitle">
		<a href="/news/coordinating-crisis-support-partnership-childrens-society">
			<span class="image"><img src="https://www.smallwoodtrust.org.uk/sites/default/files/styles/medium/public/images/website-logo-280x2092.png?itok=EfJ0omEc" alt="" /></span>			<span class="title">Coordinating crisis support in partnership with The Children&#039;s Society</span>
		</a>
	</div>
	<div class="body">
		<div class="content clearfix">
			<div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-11-15T00:00:00+00:00">15 Nov 2019</span></div></div></div>		</div>
	</div>
</div>
  </div>
  <div class="views-row views-row-3 views-row-odd">
    <div id="node-327-tile" class="node node-news node-promoted node-tile" about="/news/new-community-grant-partnerships-awarded" typeof="sioc:Item foaf:Document">
	<div class="imagetitle">
		<a href="/news/new-community-grant-partnerships-awarded">
			<span class="image"><img src="https://www.smallwoodtrust.org.uk/sites/default/files/styles/medium/public/images/rsz_istock-456172437_0.jpg?itok=B7MBWHJI" alt="" /></span>			<span class="title">New Community Grant Partnerships awarded</span>
		</a>
	</div>
	<div class="body">
		<div class="content clearfix">
			<div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-10-22T00:00:00+01:00">22 Oct 2019</span></div></div></div>		</div>
	</div>
</div>
  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
    <div id="node-322-tile" class="node node-news node-promoted node-tile" about="/news/increasing-household-income-%C2%A315-week-project-zinthiya-trust" typeof="sioc:Item foaf:Document">
	<div class="imagetitle">
		<a href="/news/increasing-household-income-%C2%A315-week-project-zinthiya-trust">
			<span class="image"><img src="https://www.smallwoodtrust.org.uk/sites/default/files/styles/medium/public/images/iStock-480426048.jpg?itok=7-8TDOxp" alt="" /></span>			<span class="title">The £15+ project: Charlotte&#039;s Story</span>
		</a>
	</div>
	<div class="body">
		<div class="content clearfix">
			<div class="field field-name-field-date field-type-datetime field-label-hidden"><div class="field-items"><div class="field-item even"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2019-09-30T00:00:00+01:00">30 Sep 2019</span></div></div></div>		</div>
	</div>
</div>
  </div>
  </div>
	
	
	
	
	
</div>  </div>
</div>
  </div>
			</div>
		</div>
		
	</div>

			<div id="ribbon" class="section region-container">
      <div class="holder clearfix">
	 	 		  <div class="region region-footer">
    <div id="block-menu-block-4" class="block block-menu-block">

    
  <div class="content">
    <div class="menu-block-wrapper menu-block-4 menu-name-menu-useful-links parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first leaf menu-mlid-3633"><a href="/our-history">Our history</a></li>
<li class="leaf menu-mlid-633"><a href="/privacy-policy">Privacy policy</a></li>
<li class="leaf menu-mlid-632"><a href="/accessibility">Accessibility</a></li>
<li class="last leaf menu-mlid-1689"><a href="/user/login">Log in</a></li>
</ul></div>
  </div>
</div>
<div id="block-block-3" class="block block-block">

    
  <div class="content">
    <p>Smallwood Trust is registered in England &amp; Wales under charity number 205798.<br />
at Lancaster House, 25 Hornyold Road, MALVERN, WR14 1QQ.</p>
<p>We use cookies to improve your experience using this website. <a href="/privacy-policy">Learn more</a></p>
  </div>
</div>
  </div>
			</div>
		</div>
		<div id="credit" class="section region-container">
			<div class="holder clearfix">
				  <div class="region region-credit">
    <div id="block-block-16" class="block block-block">

    
  <div class="content">
    <p><a href="http://www.whitefuse.com" rel="nofollow" target="_blank">Website by White Fuse</a></p>
  </div>
</div>
  </div>
			</div>
		</div>

</div>
  <script type="text/javascript" src="https://www.smallwoodtrust.org.uk/sites/default/files/js/js_MLVgtzZ1ORq9krYqkeOsRay6ou_T-0QZytivuM9tTT8.js"></script>

</body>

</html>
