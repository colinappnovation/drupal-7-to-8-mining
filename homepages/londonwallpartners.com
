<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="en" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="en" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="en" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="en" dir="ltr"
  xmlns:fb="http://ogp.me/ns/fb#"
  xmlns:og="http://ogp.me/ns#"
  xmlns:article="http://ogp.me/ns/article#"
  xmlns:book="http://ogp.me/ns/book#"
  xmlns:profile="http://ogp.me/ns/profile#"
  xmlns:video="http://ogp.me/ns/video#"
  xmlns:product="http://ogp.me/ns/product#"><!--<![endif]-->

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.londonwallpartners.com/sites/default/files/favicon_1.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="London Wall Partners LLP is a partnership of complementary skills that brings together all that is needed for the management of personal wealth. Our expertise includes pensions, estate and financial planning, investment strategy and fund selection." />
<meta name="keywords" content="london wall partners, financial advice, city of london, pensions, investment, stocks, shares, independent, fund selection, estate planning, lothbury, wimbledon, nick fletcher, jeremy beckwith" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.londonwallpartners.com/" />
<meta property="og:site_name" content="London Wall Partners | Expert and Independent, Fixed-Fee Financial Planning and Investment Advice" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.londonwallpartners.com/" />
<meta property="og:title" content="London Wall Partners" />
  <title>London Wall Partners | Expert and Independent, Fixed-Fee Financial Planning and Investment Advice</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=960">
    <meta http-equiv="cleartype" content="on">

  <style>
@import url("https://www.londonwallpartners.com/modules/system/system.base.css?pp2t00");
@import url("https://www.londonwallpartners.com/modules/system/system.messages.css?pp2t00");
@import url("https://www.londonwallpartners.com/modules/system/system.theme.css?pp2t00");
</style>
<style>
@import url("https://www.londonwallpartners.com/modules/field/theme/field.css?pp2t00");
@import url("https://www.londonwallpartners.com/modules/node/node.css?pp2t00");
@import url("https://www.londonwallpartners.com/modules/user/user.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/modules/views/css/views.css?pp2t00");
</style>
<style>
@import url("https://www.londonwallpartners.com/sites/all/modules/ctools/css/ctools.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/modules/nice_menus/nice_menus.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/modules/nice_menus/nice_menus_default.css?pp2t00");
</style>
<style>
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/normalize.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/wireframes.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/layouts/fixed-width.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/tabs.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/pages.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/blocks.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/navigation.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/views-styles.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/nodes.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/comments.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/forms.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/fields.css?pp2t00");
@import url("https://www.londonwallpartners.com/sites/all/themes/lwp3/css/print.css?pp2t00");
</style>
  <script src="https://www.londonwallpartners.com/misc/jquery.js?v=1.4.4"></script>
<script src="https://www.londonwallpartners.com/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.londonwallpartners.com/misc/drupal.js?pp2t00"></script>
<script src="https://www.londonwallpartners.com/sites/all/modules/cookiecontrol/js/cookieControl-4.1.min.js?pp2t00"></script>
<script src="https://www.londonwallpartners.com/sites/all/modules/nice_menus/superfish/js/superfish.js?pp2t00"></script>
<script src="https://www.londonwallpartners.com/sites/all/modules/nice_menus/superfish/js/jquery.bgiframe.min.js?pp2t00"></script>
<script src="https://www.londonwallpartners.com/sites/all/modules/nice_menus/superfish/js/jquery.hoverIntent.minified.js?pp2t00"></script>
<script src="https://www.londonwallpartners.com/sites/all/modules/nice_menus/nice_menus.js?pp2t00"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"lwp3","theme_token":"Jz-Qpf_gseqH-VUuEYgs6qJKI6y-Ud7pr92kqnN0lLk","js":{"0":1,"1":1,"2":1,"3":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/cookiecontrol\/js\/cookieControl-4.1.min.js":1,"sites\/all\/modules\/nice_menus\/superfish\/js\/superfish.js":1,"sites\/all\/modules\/nice_menus\/superfish\/js\/jquery.bgiframe.min.js":1,"sites\/all\/modules\/nice_menus\/superfish\/js\/jquery.hoverIntent.minified.js":1,"sites\/all\/modules\/nice_menus\/nice_menus.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/nice_menus\/nice_menus.css":1,"sites\/all\/modules\/nice_menus\/nice_menus_default.css":1,"sites\/all\/themes\/lwp3\/system.menus.css":1,"sites\/all\/themes\/lwp3\/css\/normalize.css":1,"sites\/all\/themes\/lwp3\/css\/wireframes.css":1,"sites\/all\/themes\/lwp3\/css\/layouts\/fixed-width.css":1,"sites\/all\/themes\/lwp3\/css\/page-backgrounds.css":1,"sites\/all\/themes\/lwp3\/css\/tabs.css":1,"sites\/all\/themes\/lwp3\/css\/pages.css":1,"sites\/all\/themes\/lwp3\/css\/blocks.css":1,"sites\/all\/themes\/lwp3\/css\/navigation.css":1,"sites\/all\/themes\/lwp3\/css\/views-styles.css":1,"sites\/all\/themes\/lwp3\/css\/nodes.css":1,"sites\/all\/themes\/lwp3\/css\/comments.css":1,"sites\/all\/themes\/lwp3\/css\/forms.css":1,"sites\/all\/themes\/lwp3\/css\/fields.css":1,"sites\/all\/themes\/lwp3\/css\/print.css":1}},"nice_menus_options":{"delay":"50","speed":"fast"}});</script>
  <link rel="stylesheet" href="/sites/all/themes/lwp3/css/bjqs.css">
  <link rel="stylesheet" href="/sites/all/libraries/mediaelement/build/mediaelementplayer.css">
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5-respond.js"></script>
    <![endif]-->
  </head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-7 node-type-page" >
<div class="wrapper">
    
<div id="page">
  <div id="header-wrapper">
  <header id="header" role="banner">
    <div id="breadcrumb-wrapper"></div>
    <div id="breadcrumb-tel">Tel: +44 (0) 203 696 6800</div>
          <a href="/" title="Home" rel="home" id="logo"><img src="https://www.londonwallpartners.com/sites/default/files/lwp_logo_9.png" alt="Home" /></a>
    
    
    
<div id="navigation">
              <nav id="main-menu" role="navigation">
          <h2 class="element-invisible">Main menu</h2><ul class="links inline clearfix"><li class="menu-217 first"><a href="/about" title="Find out about London Wall Partners LLP">Who We Are</a></li>
<li class="menu-270"><a href="/services" title="Find out how London Wall Partners can help manage your personal wealth.">What We Do</a></li>
<li class="menu-296"><a href="/clients" title="Find out about the type of client we work with.">Clients We Serve</a></li>
<li class="menu-349"><a href="/clients/testimonials" title="Read some of the testimonials received from clients.">Testimonials</a></li>
<li class="menu-257"><a href="/updates" title="Read London Wall Partners&#039; latest updates">News / Views</a></li>
<li class="menu-219 last"><a href="/contact" title="Contact London Wall Partners">Contact</a></li>
</ul>        </nav>
      
      
        </div><!-- /#navigation -->
    
  </header>
  </div>
  <div id="main">
      <div class="region region-highlighted">
    <div id="block-block-17" class="block block-block first last odd">

      
  <div id="front-slider">
<ul class="bjqs">
<li><a href="/services"><img src="/sites/all/themes/lwp3/images/Tower-of-London-Wall-920.jpg" data-caption="<strong>What We Do</strong><br>Guarding your capital..."></a></li>
<li><a href="/services"><img src="/sites/all/themes/lwp3/images/London-Wall-Jigsaw-920.jpg" data-caption="<strong>What We Do</strong><br>Planning your finances strategically..."></a></li>
<li><a href="/services"><img src="/sites/all/themes/lwp3/images/Orchestra-920.jpg" data-caption="<strong>What We Do</strong><br>Orchestrating the different financial instruments (i.e. investments, pensions, protection)..."></a></li>
<li><a href="/services/financial-planning"><img src="/sites/all/themes/lwp3/images/statelyhome-920.jpg" data-caption="<strong>What We Do</strong><br>Looking after your financial estate and inheritance..."></a></li>
<li><a href="/about/ethos"><img src="/sites/all/themes/lwp3/images/Operating-Theatre-920.jpg" data-caption="<strong>How We Do It</strong><br>Clinical attention to detail..."></a></li>
<li><a href="/about/careers"><img src="/sites/all/themes/lwp3/images/Mercedes-F1-920.jpg" data-caption="<strong>How We Do It / Careers</strong><br>Sound back up and operations..."></a></li>
<li><a href="/about/people"><img src="/sites/all/themes/lwp3/images/Iain-Nick-Handshake-920.jpg" data-caption="<strong>How We Do It / Culture</strong><br>A compliant and friendly manner..."></a></li>
<li><a href="/about/ethos"><img src="/sites/all/themes/lwp3/images/Clear-Water-920.jpg" data-caption="<strong>Ethos and Philosophy</strong><br>Open, transparent dealings…shining a torch on the truth"></a></li>
<li><a href="/about"><img src="/sites/all/themes/lwp3/images/RugbySchools2014.jpg" data-caption="<strong>Backing Winners</strong><br>Supporting Dulwich College Rugby: National Schools Cup Triple Champions (Twickenham), 2012, 2013 and 2014"></a></li>
<li><a href="/noa"><img src="/sites/all/themes/lwp3/images/NOA2015E-920x400.jpg" data-caption="<strong>Sponsoring the Arts</strong><br>National Open Art Exhibition 2015: Ronnie Wood  [CLICK IMAGE FOR MORE]"></a></li>
<li><a href="/updates"><img src="/sites/all/themes/lwp3/images/Schoolchildren-920.jpg" data-caption="<strong>...and Socially Responsible</strong><br>The London Wall Foundation"></a></li>
</ul>
<div class="slider-count">/ 11</div>
</div>
</div><!-- /.block -->
  </div><!-- /.region -->
    <div id="content" class="column" role="main">
      <a id="main-content"></a>
                                                


<div class="ds-1col node node-page view-mode-front view-mode-front clearfix ">

  
  <div class="field field-name-front-menu field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><div class="front-menu first">
<div class="front-menu-title"><a href="/about">Who We Are</a></div>
<ul><li><a href="/about/welcome">Welcome</a></li>
<li><a href="/about/ethos">Ethos</a></li>
<li><a href="/about/people">People</a></li>
<li><a href="/about/careers">Careers / Testimonials</a></li>
<li><a href="/about/corporate-social-responsibility">CSR</a></li></ul>
</div>
<div class="front-menu">
<div class="front-menu-title"><a href="/services">What We Do</a></div>
<ul><li><a href="/services/financial-planning">Strategic Financial Planning</a></li>
<li><a href="/services/investments">Global Investment Advice</a></li>
<li><a href="/services/implementation">Implementation</a></li>
<li><a href="/services/performance-evaluation">Performance Evaluation</a></li>
<li><a href="/services/regular-reporting">Regular Reporting</a></li></ul>
</div>
<div class="front-menu">
<div class="front-menu-title"><a href="/clients">Clients We Serve</a></div>
<ul><li><a href="/clients/city-professionals">City Professionals</a></li>
<li><a href="/clients/entrepreneurs">Entrepreneurs</a></li>
<li><a href="/clients/executives">Executives in Industry</a></li>
<li><a href="/clients/government-officials">Government Officials</a></li>
<li><a href="/clients/family-estates-trusts-and-charities">Estates, Trusts and Charities</a></li>
</ul>
</div>
<div class="front-menu last">
<div class="front-menu-title"><a href="/updates">News / Views</a></div>
<ul><li><a href="/updates/notes">Investment Notes</a></li>
<li><a href="/updates/briefings">Investment Briefings</a></li>
<li><a href="/updates/technical">Technical Notes</a></li>
<li><a href="/updates/media">Media Interviews</a></li>
<li><a href="/updates/events">Events and Seminars</a></li></ul>
</div></div></div></div></div><!-- block__no_wrapper -->
<!-- region__no_wrapper -->
          </div><!-- /#content -->



    
    
  </div><!-- /#main -->

  
</div><!-- /#page -->
<div class="push"></div></div>
<div class="footer">
  <div class="region region-bottom">
    <div id="block-block-1" class="block block-block first odd">

      
  <div class="footer-1"><div class="footer-row-title">Legal and Compliance Information</div><div class="footer-row">London Wall Partners LLP</div><div class="footer-row-sub-title">Registered Office</div><div class="footer-row">Salisbury House<br />London Wall<br />London EC2M 5QQ</div><div class="footer-row-sub-title">Partnership Number</div><div class="footer-row">OC375373</div><br><div class="footer-row">Authorised and regulated by the Financial Conduct Authority (583354)</div></div><div class="footer-2"><div class="footer-row"><img style="width: 80px;" src="/sites/all/themes/lwp3/images/CII_100_3.png"/></div></div><div class="footer-3"></div><div class="footer-4"><div class="footer-row-title">Contact Us</div><div class="footer-row-sub-title"></div><div class="footer-row"></br><strong>London Wall Partners</strong></br>Salisbury House</br>London Wall</br>London EC2M 5QQ</div><div class="footer-row"></br>+44 (0) 203 696 6801</div></div>
</div><!-- /.block -->
<div id="block-block-2" class="block block-block last even">

      
  © 2019 London Wall Partners LLP (OC375373). All rights reserved.<span><a href="/about/legal">Legal and Regulatory Information</a> | <a href="/about/privacy-notice">Privacy Notice</a> | <a href="/contact">Contact</a></span>
</div><!-- /.block -->
  </div><!-- /.region -->
</div></div>
  <script>
    jQuery(document).ready(function($) {
    cookieControl({
        introText: '<p>This site uses cookies to store information on your computer.</p>',
        fullText: '<p>We use cookies solely to help provide you with the best possible online experience.</p><p>By accessing this site, you agree that we may store and access cookies on your device. London Wall Partners use cookies to anonymously track visitors or to enhance your experience of this site. It does not provide any information to third parties.</p><p>By using our site you accept the terms of our <a href="/about/legal">Privacy Policy</a>.',
        theme: 'dark',
        position: 'right',
        shape: 'triangle',
        startOpen: true,
        autoHide: 60000,
        onAccept: function(cc){cookiecontrol_accepted(cc)},
        onReady: function(){},
        onCookiesAllowed: function(cc){cookiecontrol_cookiesallowed(cc)},
        onCookiesNotAllowed: function(cc){cookiecontrol_cookiesnotallowed(cc)},
        countries: '',
        subdomains: true,
        html: '<div id="cccwr"><div id="ccc-state" class="ccc-pause"><div id="ccc-icon"><button><span>Cookie Control</span></button></div><div class="ccc-widget"><div class="ccc-outer"><div class="ccc-inner"><h2>Cookie Control</h2><div class="ccc-content"><p class="ccc-intro"></p><div class="ccc-expanded"></div><button id="ccc-go" class="ccc-button ccc-accept">I am happy with this</button><p class="ccc-about"><small><a href="http://www.civicuk.com/cookie-law" target="_blank">About this tool</a></small></p><a class="ccc-icon" href="http://www.civicuk.com/cookie-law" target="_blank"title="About Cookie Control">About Cookie Control</a><button class="ccc-close">Close</button></div></div></div><button class="ccc-expand">read more</button></div></div></div>',
        cookieName: 'london-wall-partners_cookiecontrol',
        iconStatusCookieName: 'ccShowCookieIcon'
        });
      });
    </script>
<script>function cookiecontrol_accepted(cc) {
cc.setCookie('ccShowCookieIcon', 'no');jQuery('#ccc-icon').hide();
}</script>
<script>function cookiecontrol_cookiesallowed(cc) {

}</script>
<script>function cookiecontrol_cookiesnotallowed(cc) {

}</script>
  <script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
  <script src="/sites/all/themes/lwp3/js/bjqs-1.3.js"></script>
  <script src="/sites/all/libraries/mediaelement/build/mediaelement-and-player.js"></script>
  <script class="secret-source">
jQuery(document).ready(function($) {

$('#front-slider').bjqs({
height      : 400,
width       : 920,
responsive  : true
});

});
</script>
</body>
</html>
