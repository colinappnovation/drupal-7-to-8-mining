<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html class="no-js" lang="en">

<head profile="http://www.w3.org/1999/xhtml/vocab">

  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.readingmuseum.org.uk/sites/all/themes/rth/reading-museum.ico" type="image/x-icon" />
<meta name="description" content="Reading Museum is a free and fun day out for all the family. Discover more about Reading&#039;s local history, the Abbey Ruins, animals, art and more." />
<link rel="image_src" href="https://www.readingmuseum.org.uk/sites/default/files/images/bayeux-2_0.jpg" />
<link rel="canonical" href="https://www.readingmuseum.org.uk/reading-museum" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="shortlink" href="https://www.readingmuseum.org.uk/node/7" />
<meta property="og:site_name" content="Reading Museum" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.readingmuseum.org.uk/reading-museum" />
<meta property="og:title" content="Reading Museum" />
<meta property="og:description" content="Reading Museum is a free and fun day out for all the family. Discover more about Reading&#039;s local history, the Abbey Ruins, animals, art and more." />
<meta property="og:updated_time" content="2019-06-21T19:41:28+00:00" />
<meta property="og:image" content="https://www.readingmuseum.org.uk/sites/default/files/images/bayeux-2_0.jpg" />
<meta property="og:image:url" content="https://www.readingmuseum.org.uk/sites/default/files/images/bayeux-2_0.jpg" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@readingtownhall" />
<meta name="twitter:creator" content="@readingtownhall" />
<meta name="twitter:url" content="https://www.readingmuseum.org.uk/reading-museum" />
<meta name="twitter:description" content="Reading Museum is a free and fun day out for all the family. Discover more about Reading&#039;s local history, the Abbey Ruins, animals, art and more." />
<meta name="twitter:title" content="Home" />
<meta name="twitter:image" content="https://www.readingmuseum.org.uk/sites/default/files/images/bayeux-2_0.jpg" />
<meta name="twitter:image:width" content="200" />
<meta name="twitter:image:height" content="200" />
<meta property="article:published_time" content="2017-03-06T10:56:04+00:00" />
<meta property="article:modified_time" content="2019-06-21T19:41:28+00:00" />
  <title>Reading Museum | Reading Museum</title>
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1, user-scalable=0">
  
  <link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css_of6gQj3-xp1unrSYOQwuW91oAmyBQA4jgmFcsIjFpBU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css_xLFDRTFqZTZeUg7Pab0gP4cpz5TWo3PCH-KBo_HKQ6A.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css__l69kcVSpMwHw98SJnXPmkgzxwTAd-ECdnHsgdrxnuE.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css_EuQ47luZLSw9pFdYrUqPjcfaWmtyxIrJldjOJd6J1is.css" media="print" />
<link type="text/css" rel="stylesheet" href="https://www.readingmuseum.org.uk/sites/default/files/css/css_S93w8zcib85OlQcy81Nh7n1xDAMRoDzYxa0YNCpYdy4.css" media="all" />
 
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-7 node-type-homepage domain-museumdev-com" >

    <header class="site-header" role="banner">
  <div class="header__inner">

    <div class="row">  
      <div class="large-12 columns">

        <a href="/" title="Home" rel="home" class="site-logo">rth</a>

        <a id="navToggle" class="nav-toggle">Toggle menu</a>

        <nav id="js-navigation">
            <div class="menu-block-wrapper menu-block-3 menu-name-7902fd3a116f2f77a0d6cd861a41a617 parent-mlid-0 menu-level-1 menu-block-3 menu-name-7902fd3a116f2f77a0d6cd861a41a617 parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first leaf active-trail active menu-mlid-2436"><a href="/reading-museum" class="home active-trail active">Home</a></li>
<li class="leaf has-children menu-mlid-1816"><a href="/your-visit">Your Visit</a></li>
<li class="leaf has-children menu-mlid-1501"><a href="/our-collections">Our Collections</a></li>
<li class="leaf has-children menu-mlid-1561"><a href="/explore">Explore</a></li>
<li class="leaf has-children menu-mlid-5321"><a href="/schools">Schools</a></li>
<li class="last leaf menu-mlid-2151"><a href="/blog">Blog</a></li>
</ul></div>
  
        </nav>

      </div>
    </div>

      <div id="block-bean-header-links" class="block block-bean">
        <a class="account-links" href="#basket">Basket</a><a class="search" href="#search">Search</a><a class="hall" href="http://www.readingtownhall.co.uk">Reading Town Hall</a><a class="abbey" href="http://www.readingabbeyquarter.org.uk">Reading Abbey Quarter</a><a class="museum" href="http://www.readingmuseum.org.uk">Reading Museum</a></div>
  
  </div>

  <nav id="js-mobile-nav" class="mobile">
      <div id="block-menu-block-13" class="block block-menu-block">
        <div class="menu-block-wrapper menu-block-13 menu-name-7902fd3a116f2f77a0d6cd861a41a617 parent-mlid-0 menu-level-1 menu-block-13 menu-name-7902fd3a116f2f77a0d6cd861a41a617 parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="first leaf active-trail active menu-mlid-2436"><a href="/reading-museum" class="home active-trail active">Home</a></li>
<li class="expanded menu-mlid-1816"><a href="/your-visit">Your Visit</a><ul class="menu"><li class="first leaf has-children menu-mlid-1821"><a href="/your-visit/permanent-galleries">Permanent Galleries</a></li>
<li class="leaf menu-mlid-1826"><a href="/your-visit/how-get-here">How to get here</a></li>
<li class="leaf has-children menu-mlid-1661"><a href="/museum/whats-on">What&#039;s on</a></li>
<li class="leaf menu-mlid-1831"><a href="/whats-on/cafe">Cafe</a></li>
<li class="leaf menu-mlid-1836"><a href="/your-visit/reading-museum-shop">Shop</a></li>
<li class="leaf menu-mlid-1841"><a href="/your-visit/access">Access</a></li>
<li class="leaf menu-mlid-1846"><a href="/your-visit/families">Families</a></li>
<li class="leaf menu-mlid-1851"><a href="/your-visit/group-visits">Group Visits</a></li>
<li class="leaf menu-mlid-5366"><a href="/your-visit/corporate-visits">Corporate Visits</a></li>
<li class="last leaf menu-mlid-5371"><a href="/your-visit/great-west-way">The Great West Way</a></li>
</ul></li>
<li class="expanded menu-mlid-1501"><a href="/our-collections">Our Collections</a><ul class="menu"><li class="first leaf menu-mlid-2106"><a href="/our-collections/archaeology">Archaeology</a></li>
<li class="leaf menu-mlid-1876"><a href="/our-collections/art">Art</a></li>
<li class="leaf has-children menu-mlid-1506"><a href="/our-collections/history">History</a></li>
<li class="leaf menu-mlid-2111"><a href="/our-collections/natural-history">Natural History</a></li>
<li class="leaf menu-mlid-2116"><a href="/our-collections/world">World</a></li>
<li class="leaf menu-mlid-2096"><a href="/our-collections/online-collections">Online Collections</a></li>
<li class="leaf menu-mlid-2181"><a href="/our-collections/collections-enquiries">Collections FAQs</a></li>
<li class="last leaf menu-mlid-2176"><a href="/our-collections/image-enquiries">Image Enquiries</a></li>
</ul></li>
<li class="expanded menu-mlid-1561"><a href="/explore">Explore</a><ul class="menu"><li class="first leaf has-children menu-mlid-1861"><a href="/explore/get-involved">Get Involved</a></li>
<li class="leaf has-children menu-mlid-1866"><a href="/explore/memory-boxes">Memory Boxes</a></li>
<li class="last leaf menu-mlid-5306"><a href="/explore/artsmark">ArtsMark</a></li>
</ul></li>
<li class="expanded menu-mlid-5321"><a href="/schools">Schools</a><ul class="menu"><li class="first leaf has-children menu-mlid-5331"><a href="/schools/sessions">Sessions</a></li>
<li class="leaf menu-mlid-1856"><a href="/schools/school-loan-boxes">School Loan Boxes</a></li>
<li class="last leaf has-children menu-mlid-5326"><a href="/schools/information-for-teachers">Information for teachers</a></li>
</ul></li>
<li class="last leaf menu-mlid-2151"><a href="/blog">Blog</a></li>
</ul></div>
</div>
  
    <div id="js-site-links" class="site-links"></div>
  </nav>

  <div id="search" class="search-box">
    <div class="row">
      <div class="large-12 columns">
          <div id="block-search-form" class="block block-search">
        <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-h3aYx7ra9aakRuxakRDwTT2AqF7FJvECaUKhmcPD7ms" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  
      </div>
    </div>
  </div>
  <div id="basket" class="account-box">
    <div class="row">
      <div class="large-12 columns">
          <div id="block-bean-account-links" class="block block-bean">
        <p><a class="account" href="/my-account">My account</a> <a class="basket" href="/basket">View basket</a></p>
</div>
  
      </div>
    </div>
  </div>
</header>

  <div class="subnav">
    <div class="row">
      <div class="large-12 columns">
              </div>
    </div>
  </div>

  


<div id="wrapper" class="wrapper animated fadeIn">

    
    	
<div class="panel-pane pane-node-content" >
  
  
  <div id="js-intro" class="intro clearfix with-extra">
    <div class="intro__pane intro--left" 
	style="background-image:url('https://www.readingmuseum.org.uk/sites/default/files/styles/square_image_r/public/images/bayeux-2_0.jpg?itok=eJsA811y');"
>

      <div class="intro__content">
        <h1>Welcome</h1>

        <p>We are located in the centre of Reading and have served the community since 1883.</p>
<p><a href="/opening-times" target="_blank">Check our opening times</a></p>
      </div>

    </div>
    <div class="intro__pane intro--right" style="background-image:url('https://www.readingmuseum.org.uk/sites/default/files/styles/square_image_r/public/images/Story-of-Reading-gallery-children.jpg?itok=-KJA6OX4');">

      <div class="intro__content">
        <span class="h1">Your Visit</span>
        <p>Visit us at Reading Museum, see <a href="/your-visit/permanent-galleries/bayeux-gallery" target="_blank">Britain's Bayeux Tapestry</a>, then explore the Abbey Quarter and the Riverside Museum.</p>
<p><a href="/your-visit" target="_blank">Find out how to get here and what you can see and do</a></p>
      </div>

    </div>
  </div>

      <div class="row">
      <div class="large-12 columns">
        <div class="intro__extra">
          <h3>Admission is <strong>free</strong></h3>
        </div>
      </div>
    </div>
  
  <div class="row">
    <div class="large-12 columns">
              <ul id="js-signposts" class="signposts clearfix">
			<li>
			
  
  <a href="/museum/whats-on" class="signpost" title="Open What&#039;s on at Reading Museum" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_square/public/images/children-activity-5.jpg?itok=ynXooaur" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/children-activity-5.jpg?itok=uBUd7OZP">
    <div class="signpost__content">
      <h2>What&#039;s on at Reading Museum</h2>
    </div>
  </a>

		</li>
			<li>
			
  
  <a href="/schools" class="signpost" title="Open Schools" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/square_image_crop_r/public/images/children%20in%20Victorian%20Schoolroom1_0.jpg?itok=kzNyqo8V" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/children%20in%20Victorian%20Schoolroom1_0.jpg?itok=xDymW8kc">
    <div class="signpost__content">
      <h2>Schools</h2>
    </div>
  </a>

		</li>
	</ul>          </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
              <ul id="js-signposts-2" class="signposts clearfix">
			<li>
			
  
  <a href="/your-visit/reading-museum-shop" class="signpost" title="Open Reading Museum Shop" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_square/public/images/shop-books-visitor.jpg?itok=NXmaFzHO" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/shop-books-visitor.jpg?itok=tA9Fr48l">
    <div class="signpost__content">
      <h2>Reading Museum Shop</h2>
    </div>
  </a>

		</li>
			<li>
			
  
  <a href="/our-collections" class="signpost" title="Open Our Collections" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/square_image_crop_r/public/images/mosaic-2.jpg?itok=ykt3sQ37" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/mosaic-2.jpg?itok=pHs1XPZI">
    <div class="signpost__content">
      <h2>Our Collections</h2>
    </div>
  </a>

		</li>
			<li>
			
  
  <a href="/explore/get-involved" class="signpost" title="Open Get Involved" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/square_image_crop_r/public/images/_PRP6471.jpg?itok=NtL5eJer" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/_PRP6471.jpg?itok=QeiaXn7X">
    <div class="signpost__content">
      <h2>Get Involved</h2>
    </div>
  </a>

		</li>
	</ul>          </div>
  </div>

      <section>
	<div class="row">
		<div class="large-12 columns">
			<h2>Elsewhere</h2>
			<ul id="js-signpost-single" class="listing clearfix two-col">
			  			    <li>
			      
<a href="http://www.readingtownhall.co.uk" class="signpost" title="Open Reading Town Hall" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_square/public/images/Town%20Hall%20exterior.jpg?itok=wJECfB4a" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/Town%20Hall%20exterior.jpg?itok=i0GjFIMT">
  <div class="signpost__content">
    <h2>Reading Town Hall</h2>
  </div>
</a>			    </li>
			  			    <li>
			      
<a href="http://www.readingabbeyquarter.org.uk" class="signpost" title="Open Abbey Quarter" data-thumbp="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_square/public/images/Matilda-henry-gallery-square.jpg?itok=dVm7Lu7K" data-thumbl="https://www.readingmuseum.org.uk/sites/default/files/styles/signpost_land/public/images/Matilda-henry-gallery-square.jpg?itok=UZ5RfFXS">
  <div class="signpost__content">
    <h2>Abbey Quarter</h2>
  </div>
</a>			    </li>
			  			</ul>
		</div>
	</div>
</section>  
  
 
  
  </div>  
    
  <footer class="footer" role="contentinfo">
    <div class="footer__info">
      <div class="row">
        <div class="large-12 columns">
            <div id="block-bean-museum-social" class="block block-bean">
        <p>Connect with us</p>
<ul><li><a class="facebook" href="https://www.facebook.com/ReadingMuseum" target="_blank">Facebook</a></li>
<li><a class="twitter" href="https://twitter.com/readingmuseum" target="_blank">Twitter</a></li>
</ul></div>
<div id="block-bean-museum-mailing-list" class="block block-bean">
        <p>Stay up to date:</p>
<a class="btn cta mailing" href="/reading-museum-mailing-list" title="Sign up to our mailing list">Sign up to our mailing list</a>
<a class="btn cta mailing" href="/reading-museum-mailing-list-0" title="Teachers Sign Up">Teachers mailing list</a></div>
  
        </div>
      </div>
    </div>
    <div class="footer__content">
      <div class="row">
        <div class="large-12 columns">
            <a href="/" class="footer__logo" title="Back to the homepage">Home</a>
              <div id="block-bean-museum-address" class="block block-bean">
        <p>Reading Museum,<br />
Blagrave Street,<br />
Reading,<br />
RG1 1QH</p>
<p>t: <a href="tel:+441189373400">0118 937 3400</a></p>
</div>
<div id="block-bean-museum-footer-opening-times" class="block block-bean">
        <p>Visiting:</p>
<p><a href="/opening-times">Opening times</a></p>
<p><a href="/your-visit/how-get-here">How to get here</a></p>
<p> </p>
</div>
<div id="block-menu-c3eade27203b6c2c4b0e6502e19bc893" class="block block-menu">
        <ul class="menu"><li class="first leaf"><a href="/museum-contact-us">Contact us</a></li>
<li class="leaf"><a href="/explore/get-involved/about-reading-museum">About us</a></li>
<li class="leaf"><a href="/explore/get-involved/volunteer-reading-museum">Volunteer</a></li>
<li class="leaf"><a href="/your-visit/access">Accessibility</a></li>
<li class="leaf"><a href="/privacy-and-cookies-policy">Privacy and Cookies Policies</a></li>
<li class="last leaf"><a href="/reading-museum-terms-conditions">Terms and conditions</a></li>
</ul></div>
  
        </div>
      </div>
    </div>
    <div class="footer__credits">
      <div class="row">
        <div class="large-12 columns">
            <div id="block-bean-credits" class="block block-bean">
        <p>Crafted by <a href="https://un.titled.co.uk" target="_blank">Un.titled</a></p>
</div>
<div id="block-bean-museum-partners" class="block block-bean">
        	<ul class="listing partners">
	<li><a href="http://www.reading.gov.uk/" target="_blank" class="partner" title="Reading borough council">
	<img typeof="foaf:Image" src="https://www.readingmuseum.org.uk/sites/default/files/logos/reading-council%40x2_0.png" width="400" height="144" alt="Reading borough council" /></a></li>
	<li><a href="http://www.artscouncil.org.uk/accreditation-scheme/about-accreditation" target="_blank" class="partner" title="Accredited museum">
	<img typeof="foaf:Image" src="https://www.readingmuseum.org.uk/sites/default/files/logos/accredited-museum%402x_0.png" width="108" height="100" alt="Accredited museum" /></a></li>
	<li><a href="http://www.artscouncil.org.uk/" target="_blank" class="partner" title="Arts Council England">
	<img typeof="foaf:Image" src="https://www.readingmuseum.org.uk/sites/default/files/logos/arts-council%40x2_0.png" width="398" height="126" alt="Arts Council England" /></a></li>
	<li><a href="http://www.greatwestway.co.uk" target="_blank" class="partner" title="Great West Way">
	<img typeof="foaf:Image" src="https://www.readingmuseum.org.uk/sites/default/files/logos/GWW_landscape_1.jpg" width="372" height="126" alt="" /></a></li>
</ul></div>
  
        </div>
      </div>
    </div>
  </footer>

</div>

<div class="hidden">
  </div>

  <div id="olContainer" class="overlay-wrapper"></div>  <script type="text/javascript" src="https://www.readingmuseum.org.uk/sites/default/files/js/js_7Ukqb3ierdBEL0eowfOKzTkNu-Le97OPm-UqTS5NENU.js"></script>

  <script type="text/javascript" src="https://www.readingmuseum.org.uk/sites/default/files/js/js_dYOGV15VEcvR5zyP3UKzyEH3a6wcYkaWkQCEZ8Wpd8c.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
document.createElement( "picture" );
//--><!]]>
</script>
<script type="text/javascript" src="https://www.readingmuseum.org.uk/sites/default/files/js/js_04pGdQEbPqToN-r5sm0N_in8I-j5tOdVL53pvu0IzCw.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-87586317-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.readingmuseum.org.uk/sites/default/files/js/js_YvB-Q7Wj2aOUe7UKqtsItpbFERiVfCL_3Rer8AO5r7k.js"></script>
<script type="text/javascript" src="https://system.spektrix.com/readingarts/website/scripts/resizeiframe.js"></script>
<script type="text/javascript" src="https://www.readingmuseum.org.uk/sites/default/files/js/js_r2uKDHkqxGyfNY-PMTXky1KKx6pG930ml1KC8hm2J84.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"rth","theme_token":"CHSu8FBGFDthe6JmHMt5OtDC6ETJcBU7AovfNhshA_g","js":{"sites\/all\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"sites\/all\/modules\/contrib\/picture\/picture.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"0":1,"sites\/all\/themes\/rth\/js\/rth-signposts.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/rth\/js\/rth.emmsg.js":1,"sites\/all\/themes\/rth\/js\/libs\/jquery.swipebox.min.js":1,"sites\/all\/themes\/rth\/js\/rth.overlay.js":1,"sites\/all\/themes\/rth\/js\/libs\/jquery.matchHeight-min.js":1,"sites\/all\/themes\/rth\/js\/rth.plugins.js":1,"sites\/all\/themes\/rth\/js\/rth.front.js":1,"https:\/\/system.spektrix.com\/readingarts\/website\/scripts\/resizeiframe.js":1,"sites\/all\/themes\/rth\/js\/libs\/js.cookie.js":1,"sites\/all\/themes\/rth\/js\/libs\/jquery.pjax.min.js":1,"sites\/all\/themes\/rth\/js\/libs\/jquery.velocity.min.js":1,"sites\/all\/themes\/rth\/js\/libs\/hoverIntent.js":1,"sites\/all\/themes\/rth\/js\/libs\/superfish.js":1,"sites\/all\/themes\/rth\/js\/rth.global.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/domain\/domain_nav\/domain_nav.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/themes\/rth\/css\/foundation.css":1,"sites\/all\/themes\/rth\/css\/animate.css":1,"sites\/all\/themes\/rth\/css\/style.css":1,"sites\/all\/themes\/rth\/css\/superfish.css":1,"sites\/all\/themes\/rth\/css\/print.css":1,"sites\/all\/themes\/rth\/css\/overlay.css":1,"sites\/all\/themes\/rth\/css\/swipebox.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true},"swipebox_enabled":true,"overlay_enabled":true,"matchHeight_enabled":true});
//--><!]]>
</script>

</body>
</html>