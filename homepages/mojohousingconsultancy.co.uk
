<!DOCTYPE html>
<html  lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <meta charset="utf-8" />
<script>{"@context":"http:\/\/schema.org","@type":"WebSite","name":"mojohousingconsultancy.co.uk","url":"https:\/\/www.mojohousingconsultancy.co.uk\/","logo":null,"address":{"@type":"PostalAddress"}}</script>
<link rel="shortlink" href="/node/1" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/home" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.mojohousingconsultancy.co.uk/" />
<link rel="shortlink" href="https://www.mojohousingconsultancy.co.uk/" />
<meta property="og:site_name" content="mojohousingconsultancy.co.uk" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.mojohousingconsultancy.co.uk/" />
<meta property="og:title" content="mojohousingconsultancy.co.uk" />
<meta property="og:description" content="Isn’t it time you found your mojo?" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.mojohousingconsultancy.co.uk/" />
<meta name="twitter:title" content="mojohousingconsultancy.co.uk" />
<meta name="twitter:description" content="Isn’t it time you found your mojo?" />
<meta itemprop="name" content="mojohousingconsultancy.co.uk" />
<meta itemprop="description" content="Isn’t it time you found your mojo?" />
<link rel="shortcut icon" href="https://www.mojohousingconsultancy.co.uk/sites/all/themes/mojo/favicon.ico" type="image/vnd.microsoft.icon" />
  <title>mojohousingconsultancy.co.uk | Isn’t it time you found your mojo?</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
  
  <style>
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/modules/simplenews/simplenews.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/modules/field/theme/field.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/modules/search/search.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/modules/user/user.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/modules/views/css/views.css?pz69vc");
</style>
<style>
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/modules/ctools/css/ctools.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/css/superfish.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/css/superfish-smallscreen.css?pz69vc");
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/style/mojo.css?pz69vc");
</style>
<style>
@import url("https://www.mojohousingconsultancy.co.uk/sites/all/themes/mojo/css/styles.css?pz69vc");
</style>
<style>
@import url("https://www.mojohousingconsultancy.co.uk/sites/default/files/fontyourface/font.css?pz69vc");
</style>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:regular&amp;subset=latin-ext" media="all" />
  <script src="https://www.mojohousingconsultancy.co.uk/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.js?v=1.10.2"></script>
<script src="https://www.mojohousingconsultancy.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.mojohousingconsultancy.co.uk/misc/drupal.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/modules/google_analytics/googleanalytics.js?pz69vc"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-109156255-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/sfautomaticwidth.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/sftouchscreen.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/sfsmallscreen.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/supposition.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/superfish.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/libraries/superfish/supersubs.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/modules/superfish/superfish.js?pz69vc"></script>
<script src="https://www.mojohousingconsultancy.co.uk/sites/all/themes/mojo/js/script.js?pz69vc"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"mojo","theme_token":"PJkyLxY5759ZgriAv2801hd0Id0XeSjl7sTHRf_bRXo","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfautomaticwidth.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/mojo\/js\/script.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/css\/superfish-smallscreen.css":1,"sites\/all\/libraries\/superfish\/style\/mojo.css":1,"sites\/all\/themes\/mojo\/system.base.css":1,"sites\/all\/themes\/mojo\/system.menus.css":1,"sites\/all\/themes\/mojo\/system.messages.css":1,"sites\/all\/themes\/mojo\/system.theme.css":1,"sites\/all\/themes\/mojo\/comment.css":1,"sites\/all\/themes\/mojo\/node.css":1,"sites\/all\/themes\/mojo\/css\/styles.css":1,"sites\/default\/files\/fontyourface\/font.css":1,"https:\/\/fonts.googleapis.com\/css?family=PT+Sans:regular\u0026subset=latin-ext":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"autoArrows":false},"plugins":{"automaticwidth":true,"touchscreen":{"behaviour":"0","disableHover":true,"mode":"window_width","breakpoint":777,"breakpointUnit":"px"},"smallscreen":{"mode":"window_width","breakpoint":777,"breakpointUnit":"px","title":"Main menu"},"supposition":true,"supersubs":true}}}});</script>
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page" >
      <p class="skip-link__wrapper">
      <a href="#main-menu" class="skip-link visually-hidden visually-hidden--focusable" id="skip-link">Jump to navigation</a>
    </p>
      
<div class="layout-center">

  <header class="header" role="banner">
    
    
    
    
      <div class="header__region region region-header">
    <div class="block block-superfish first odd" id="block-superfish-1">

      
  <ul  id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-mojo sf-total-items-7 sf-parent-items-1 sf-single-items-6"><li id="menu-219-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-325-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/about" class="sf-depth-1">About Mojo</a></li><li id="menu-326-1" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/who-we-are" class="sf-depth-1">Who We Are</a></li><li id="menu-324-1" class="middle even sf-item-4 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent"><a href="/what-we-do" class="sf-depth-1 menuparent">What We Do</a><ul><li id="menu-435-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/what-we-do/mentoring" class="sf-depth-2">Mentoring</a></li><li id="menu-437-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/what-we-do/quality-assurance" class="sf-depth-2">Quality Assurance</a></li><li id="menu-438-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/what-we-do/scrutiny" class="sf-depth-2">Scrutiny</a></li><li id="menu-436-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/what-we-do/stakeholder-engagement" class="sf-depth-2">Stakeholder Engagement</a></li><li id="menu-439-1" class="last odd sf-item-5 sf-depth-2 sf-no-children"><a href="/what-we-do/training" class="sf-depth-2">Training</a></li></ul></li><li id="menu-323-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/whats-new" title="New initiatives and potential partnering opportunities." class="sf-depth-1">What&#039;s New</a></li><li id="menu-321-1" class="middle even sf-item-6 sf-depth-1 sf-no-children"><a href="/blog" class="sf-depth-1">Blog</a></li><li id="menu-466-1" class="last odd sf-item-7 sf-depth-1 sf-no-children"><a href="/contact" class="sf-depth-1">Contact Us</a></li></ul>
</div>
<div class="block block-block last even" id="block-block-3">

      
  <div class="menu-banner"><img src="/sites/default/files/street-dev.jpg"><span id="scroll-down"></span></div>
</div>
  </div>
  </header>

  <div class="layout-3col layout-swap">

    
    <main class="layout-3col__full" role="main">
                  <a href="#skip-link" class="visually-hidden visually-hidden--focusable" id="main-content">Back to top</a>
                    <h1>Isn’t it time you  found your mojo?</h1>
                                          


<article class="node node-page view-mode-full clearfix node-1" about="/home" typeof="foaf:Document">

      <header>
                  <span property="dc:title" content="Isn’t it time you  found your mojo?" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      
          </header>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Welcome to our home, have a good look around.</p>
<p>Mojo is a niche housing consultancy specialising in customer service, stakeholder and employee engagement and change management.</p>
<p>Mojo has a track record of empowering housing employees, customers and communities to achieve amazing results.</p>
<p>We are committed to working collaboratively with like-minded people to bring about positive change through stakeholder and employee engagement in housing and regeneration.</p>
<p>&nbsp;</p>
</div></div></div>
  
  
</article>
          </main>

    <div class="layout-swap__top layout-3col__full">

      <a href="#skip-link" class="visually-hidden visually-hidden--focusable" id="main-menu" tabindex="-1">Back to top</a>

      

    </div>

    
    
  </div>

    <footer class="footer region region-footer" role="contentinfo">
    <div class="block block-block first last odd" id="block-block-5">

      
  <p>&nbsp;</p>
<div id="jigsaw"><div class="media media-element-container media-default"><div id="file-39" class="file file-image file-image-png">

        <h2 class="element-invisible"><a href="/files/mojo-jigsaw-borderpng">mojo-Jigsaw-border.png</a></h2>
    
  
  <div class="content">
    <img style="height: 303px; width: 500px;" class="media-element file-default" data-delta="3" typeof="foaf:Image" src="https://www.mojohousingconsultancy.co.uk/sites/default/files/mojo-Jigsaw-border.png" width="1532" height="927" alt="" />  </div>

  
</div>
</div></div>

</div>
  </footer>

</div>

  <div class="region region-bottom">
    <div class="block block-block first last odd" id="block-block-2">

      
  <div class="container">
<div class="left">
<p>3 Queensway<br>Thetford<br>Norfolk<br>IP24 3DQ<br><br>07870 573533<br><a href="mailto:info@mojohousingconsultancy.co.uk">info@mojohousingconsultancy.co.uk</a></p>
</div>
<div class="center">
<p>&nbsp;</p>
<p><a href="/node/26">Terms &amp; conditions</a></p>
<p><br><a href="/node/27">Privacy &amp; Cookies</a></p>
</div>
<div class="right">
<p>© 2017&nbsp;Mojo Housing Consultancy<br>Registered in England Number <a href="https://beta.companieshouse.gov.uk/company/05800600">05800600</a></p>
<p><br>Website by Tom Sage<br><br>Banner photos mostly by <a href="http://www.ciaraleeming.co.uk/">Ciara Leeming</a></p>
</div>
</div>

</div>
  </div>
  </body>
</html>
