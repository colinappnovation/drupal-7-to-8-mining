<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/home" />
<link rel="shortlink" href="/node/6" />
<link rel="shortcut icon" href="https://www.paulinepayne.co.uk/sites/default/files/favicon_0.ico" type="image/vnd.microsoft.icon" />
  <title>Designer Jeweller &amp; Tutor | Pauline Payne</title>
  <style type="text/css" media="all">
@import url("https://www.paulinepayne.co.uk/modules/system/system.base.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/system/system.menus.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/system/system.messages.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/system/system.theme.css?q17erk");
</style>
<style type="text/css" media="all">
@import url("https://www.paulinepayne.co.uk/sites/all/modules/simplenews/simplenews.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/date/date_api/date.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/date/date_popup/themes/datepicker.1.7.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/date/date_repeat_field/date_repeat_field.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/field/theme/field.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/node/node.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/search/search.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/user/user.css?q17erk");
@import url("https://www.paulinepayne.co.uk/modules/views/css/views.css?q17erk");
</style>
<style type="text/css" media="all">
@import url("https://www.paulinepayne.co.uk/modules/ctools/css/ctools.css?q17erk");
@import url("https://www.paulinepayne.co.uk/sites/all/modules/eu_cookie_compliance/css/eu_cookie_compliance.css?q17erk");
@import url("https://www.paulinepayne.co.uk/sites/all/themes/danland/js/fancybox/jquery.fancybox.css?q17erk");
</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#0779bf;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}

/*]]>*/-->
</style>
<style type="text/css" media="all">
@import url("https://www.paulinepayne.co.uk/sites/all/themes/danland/style.css?q17erk");
</style>

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.paulinepayne.co.uk/sites/all/themes/danland/style.ie6.css?q17erk" media="all" />
<![endif]-->
  <script type="text/javascript" src="https://www.paulinepayne.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/misc/drupal.js?q17erk"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/modules/eu_cookie_compliance/js/jquery.cookie-1.4.1.min.js?v=1.4.1"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/themes/danland/scripts/jquery.cycle.all.js?q17erk"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/modules/google_analytics/googleanalytics.js?q17erk"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-46708564-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/themes/danland/js/jquery.mousewheel-3.0.6.pack.js?q17erk"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/themes/danland/js/fancybox/jquery.fancybox.pack.js?q17erk"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery(document).ready(function() {
    jQuery('.fancybox').fancybox();
});
//--><!]]>
</script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/themes/danland/scripts/hoverIntent.js?q17erk"></script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/themes/danland/scripts/superfish.js?q17erk"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"danland","theme_token":"hrgSzBjx3-Wm7GURqiPvTyAuoiWqfwasB0QJWbbkPw8","js":{"0":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/themes\/danland\/scripts\/jquery.cycle.all.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/danland\/js\/jquery.mousewheel-3.0.6.pack.js":1,"sites\/all\/themes\/danland\/js\/fancybox\/jquery.fancybox.pack.js":1,"2":1,"sites\/all\/themes\/danland\/scripts\/hoverIntent.js":1,"sites\/all\/themes\/danland\/scripts\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"modules\/date\/date_api\/date.css":1,"modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"modules\/views\/css\/views.css":1,"modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"sites\/all\/themes\/danland\/js\/fancybox\/jquery.fancybox.css":1,"0":1,"sites\/all\/themes\/danland\/style.css":1,"sites\/all\/themes\/danland\/style.ie6.css":1}},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use Google analytics on this site.\u003C\/h2\u003E\n\u003Cp\u003EBy clicking the Accept button, you agree to us doing so.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy tapping the Accept button, you agree to us doing so.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/Privacy","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":0,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":0,"cookie_categories":[],"enable_save_preferences_button":1,"fix_first_cookie_category":1,"select_all_categories_by_default":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-page" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div >
<div id="header">
<div id="header-wrapper">
	 
		<div id="logo-wrapper">
			<div class="logo">
				<a href="/" title="Home"><img src="https://www.paulinepayne.co.uk/sites/default/files/Mum%20Logo%20-%20Long%20100px%20High_1_0.png" alt="Home" /></a>
			</div>
		</div><!-- end logo wrapper -->
			
	
	
      </div><!-- end header-wrapper -->
</div> <!-- /header -->
<div style="clear:both"></div>

<div id="menu">
<div id="rounded-menu-left"></div>
       <div id="nav">
        <ul class="links"><li class="menu-341 active-trail first active"><a href="/home" class="active-trail active">Home</a></li>
<li class="menu-342"><a href="/courses">Courses</a></li>
<li class="menu-340"><a href="/work-on-display">Work On Display</a></li>
<li class="menu-761"><a href="/commissions">Commissions</a></li>
<li class="menu-364"><a href="/contact" title="">Contact</a></li>
<li class="menu-431"><a href="/mailinglist">Mailing List</a></li>
<li class="menu-2576 last"><a href="/Privacy">Privacy</a></li>
</ul>      </div> <!-- end primary -->
    <div id="rounded-menu-right"></div>
</div> <!-- end menu -->
<div style="clear:both"></div>

<div id="slideshow-wrapper">
<div class="slideshow-inner">
<div id="slideshow-preface">
 </div>
<div id="slideshow-bottom">
  <div class="region region-highlighted">
    <div id="block-views-front-images-block" class="block block-views">

  <h2 class="block-title">Front Images</h2>

<div class="content">
  <div class="view view-front-images view-id-front_images view-display-id-block view-dom-id-a2e56e5165ca03c77834e55b4462d784">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
    <div id="node-56" class="slideshow">
	        <img src="https://www.paulinepayne.co.uk/sites/default/files/DSC_0100_0.JPG" width="950" height="355" alt="slideshow" />
            <img src="https://www.paulinepayne.co.uk/sites/default/files/Placing%20Solder.jpg" width="950" height="355" alt="slideshow" />
            <img src="https://www.paulinepayne.co.uk/sites/default/files/Soldering.jpg" width="950" height="355" alt="slideshow" />
    </div>  </div>
    </div>
  
  
  
  
  
  
</div></div> <!-- end block content -->
</div> <!-- end block -->
  </div>
</div></div></div>

 
<div style="clear:both"></div>
<div id="wrapper">

    <div id="content">
			<a id="main-content"></a>
						      		      			        				<h1 class="title" id="page-title">
         			 		Designer Jeweller &amp; Tutor        				</h1>
     				       		      		        			<div class="tabs">
          				        			</div>
      		      		      				      <div class="content-middle">  <div class="region region-content">
    <div id="block-system-main" class="block block-system">


<div class="content">
  <div id="node-6" class="node node-page node-promoted clearfix" about="/home" typeof="foaf:Document">

  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>I am a freelance designer-jeweller, based in Oxfordshire, who studied at Portsmouth College of Art and Design. While at Portsmouth I gained a number of prestigious awards for my jewellery work including: two Royal Society of Arts Commendations and winner of the National Platinum Award. </p>
<p>Working in gold and silver every piece is individually made. Much of my jewellery is inspired by natural forms and is characterised by the contrasting use of different textures.</p>
<p>I also teach silver jewellery in various workshops throughout the year, including Ammerdown, Ardington School of Crafts, Denman and The Oxford Summer School</p>
</div></div></div>  </div>

  
  
</div>
</div> <!-- end block content -->
</div> <!-- end block -->
  </div>
</div>
						
</div> <!-- end content -->

    <div style="clear:both"></div>
</div> <!-- end wrapper -->




 
<div style="clear:both"></div>
<div id="footer-wrapper">
</div> <!-- end footer wrapper -->
</div>  <script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.paulinepayne.co.uk/sites/all/modules/eu_cookie_compliance/js/eu_cookie_compliance.js?q17erk"></script>
</body>
</html>
