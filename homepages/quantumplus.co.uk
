<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="shortcut icon" href="https://www.quantumplus.co.uk/sites/all/themes/quantumplus/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="We are transforming sourcing. Our expertise-driven thought-leadership delivers your desired outcomes for your business using proven advisory services." />
<meta name="abstract" content="We put you first. Our pedigree since 1999 shows how we commit to our clients, bringing challenging and impartial advice that transforms your business operations." />
<meta name="keywords" content="sourcing, outsourcing," />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.quantumplus.co.uk/" />
<link rel="shortlink" href="https://www.quantumplus.co.uk/" />
<meta name="dcterms.title" content="Sourcing Advisory | Outsourcing Consultancy | Quantum Plus Ltd" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.format" content="text/html" />
<meta name="dcterms.identifier" content="https://www.quantumplus.co.uk/" />
<title>Sourcing Advisory | Outsourcing Consultancy | Quantum Plus Ltd |</title>
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_d6EYFodJndtZgSLz4QdWfrDElfWZEYujMHqw0o6Sjns.css" media="screen" />
<style type="text/css" media="print">
<!--/*--><![CDATA[/*><!--*/
#sb-container{position:relative;}#sb-overlay{display:none;}#sb-wrapper{position:relative;top:0;left:0;}#sb-loading{display:none;}

/*]]>*/-->
</style>
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_vZ_wrMQ9Og-YPPxa1q4us3N7DsZMJa-14jShHgRoRNo.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_C1guoaLhZ8u_pLbLHMq1FbYg6g7fjcUB99ss4mAg45k.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_0J1QyyT6_7OugkH0dQ6nz4S0R93HweeBcyU-ngiIIUw.css" media="all" />
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#000000;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#ffffff !important;}.eu-cookie-withdraw-tab{border-color:#ffffff;}.eu-cookie-compliance-more-button{color:#ffffff !important;}

/*]]>*/-->
</style>
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_eA6MGkeGcy4F7dkGqjChSX6s_uve41Upnc2391z__d4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.quantumplus.co.uk/sites/default/files/css/css_t42Bt8Waxnvrh0G4Yc-mcGxNrFqU2Jq7nKQCA4j8NMM.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Play:regular&amp;subset=cyrillic" media="all" />
<script type="text/javascript">
<!--//--><![CDATA[//><!--
document.cookie = 'adaptive_image=' + Math.max(screen.width, screen.height) + '; path=/';
//--><!]]>
</script>
<script type="text/javascript" src="https://www.quantumplus.co.uk/sites/default/files/js/js_0RyHJ63yYLuaWsodCPCgSD8dcTIA0dqcDf8-7c2XdBw.js"></script>
<script type="text/javascript" src="https://www.quantumplus.co.uk/sites/default/files/js/js_VZEUcyaSgD0rnaLpqPJI0KIXfexN1WvDQKPZzTmw7jE.js"></script>
<script type="text/javascript" src="https://www.quantumplus.co.uk/sites/default/files/js/js_Qltqo3c96RPUZeNLQtEF-CD9CkQCzn8HjIQ-X7wNYbI.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-75471115-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.quantumplus.co.uk/sites/default/files/js/js_xQWF4kwe6aq90lPELc1s3JEbKZemZsD-LMS-C8_-A1M.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

          Shadowbox.path = "/sites/all/libraries/shadowbox/";
        
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"quantumplus","theme_token":"AVKk4G9F3FqKKzqj68Uv5snPsUGf1VaSrKqaKGvoRH8","js":{"0":1,"1":1,"\/\/platform.twitter.com\/widgets.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"2":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/shadowbox\/shadowbox.js":1,"sites\/all\/modules\/shadowbox\/shadowbox_auto.js":1,"sites\/all\/modules\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.js":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"3":1,"sites\/all\/themes\/quantumplus\/js\/plugins.js":1,"sites\/all\/themes\/quantumplus\/js\/global.js":1,"4":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/libraries\/shadowbox\/shadowbox.css":1,"0":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/adaptive_image\/css\/adaptive-image.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/poll\/poll.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/panels_extra_layouts\/plugins\/layouts\/mccoppin\/mccoppin.css":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"sites\/all\/modules\/addtoany\/addtoany.css":1,"1":1,"sites\/all\/themes\/quantumplus\/style.css":1,"sites\/all\/themes\/quantumplus\/css\/responsive.css":1,"sites\/default\/files\/fontyourface\/font.css":1,"https:\/\/fonts.googleapis.com\/css?family=Play:regular\u0026subset=cyrillic":1}},"shadowbox":{"animate":1,"animateFade":1,"animSequence":"wh","auto_enable_all_images":0,"auto_gallery":1,"autoplayMovies":true,"continuous":0,"counterLimit":"10","counterType":"default","displayCounter":1,"displayNav":1,"enableKeys":1,"fadeDuration":"0.35","handleOversize":"resize","handleUnsupported":"link","initialHeight":160,"initialWidth":320,"language":"en","modal":false,"overlayColor":"#000","overlayOpacity":"0.85","resizeDuration":"0.35","showMovieControls":1,"slideshowDelay":"0","viewportPadding":20,"useSizzle":0},"viewsSlideshow":{"testimonials_front-block_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_testimonials_front-block_1":{"num_divs":6,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"testimonials_front-block_1","effect":"fade","transition_advanced":1,"timeout":12000,"speed":700,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"play_on_hover":0,"action_advanced":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"pause_after_slideshow":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":3000,"cleartype":1,"cleartypenobg":1,"advanced_options":"{}"}},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":1,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWe use cookies on this site to enhance your user experience.\tBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EThank you for accepting cookies.You can now hide this message or find out more about cookies.\u00a0\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-policy","popup_link_new_window":0,"popup_position":null,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","popup_eu_only_js":0,"cookie_lifetime":100,"cookie_session":false,"disagree_do_not_show_popup":0,"method":"default","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="/sites/all/themes/quantumplus/js/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-panel">
      <div id="header-wrap" class="clr fixed-header">
    <header id="header" class="site-header clr container">
      <div id="logo"><a href="/"><img src="/sites/all/themes/quantumplus/images/logo.png" alt="logo" /></a></div>
      
      
     <div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close"></a></div>
      <div id="site-navigation-wrap">
        <a href="#sidr-main" id="navigation-toggle"><span class="fa fa-bars"></span></a>
        <nav id="site-navigation" class="navigation main-navigation clr">
          <div id="main-menu" class="menu-main-container">
            <ul class="menu"><li class="first leaf active-trail"><a href="/front" title="Home" class="active-trail active">Home</a></li>
<li class="collapsed"><a href="/our-services" title="Our Services">Our Services</a></li>
<li class="leaf"><a href="/our-clients" title="Our Clients">Our Clients</a></li>
<li class="leaf"><a href="/blog" title="Blog">Blog</a></li>
<li class="last leaf"><a href="/contact-us" title="Contact Us">Contact Us</a></li>
</ul>          </div>
        </nav>
      </div>
    </header>
  </div>

<div id="main-banner-wrap"><div id="main-banner" ><div class="region region-main-banner">
  <div id="block-block-4" class="block block-block">

      
  <div class="content">
    <img alt="Blog &amp; News" src="/sites/default/files/images/quantum-plus-banner.jpg" />
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div></div>

  <div id="main" class="site-main container clr">
        <div id="primary" class="content-area clr">
      <section id="content" role="main" class="site-content"  clr">
                <div id="content-wrap">
                    <h1 class="page-title">sourcing . together . to win </h1>                                                  <div class="region region-content">
  <div id="block-system-main" class="block block-system">

      
  <div class="content">
                          
      
    
  <div class="content">
    
<div class="panel-display mccoppin clearfix " >

  <div class="mccoppin-container mccoppin-column-content clearfix">
    <div class="mccoppin-column-content-region mccoppin-column1 panel-panel">
      <div class="mccoppin-column-content-region-inner mccoppin-column1-inner panel-panel-inner">
        <div class="panel-pane pane-custom pane-1"  >
  
      
  
  <div class="pane-content">
    <h1>sourcing</h1>
<div class="panel-left">
<p class="rtejustify"><span style="color:#f39">We are transforming sourcing. </span>Our expertise-driven thought-leadership delivers your desired outcomes for your business using proven advisory services.</p>
</div>
  </div>

  
  </div>
      </div>
    </div>
    <div class="mccoppin-column-content-region mccoppin-column2 panel-panel">
      <div class="mccoppin-column-content-region-inner mccoppin-column2-inner panel-panel-inner">
        <div class="panel-pane pane-custom pane-2"  >
  
      
  
  <div class="pane-content">
    <h1 class="rtecenter">together</h1>
<div class="panel-middle">
<p class="rtejustify"><span style="color:#f39">We put you first. </span>Our pedigree since 1999 shows how we commit to our clients, bringing challenging and impartial advice that transforms your business operations.</p>
</div>
  </div>

  
  </div>
      </div>
    </div>
    <div class="mccoppin-column-content-region mccoppin-column3 panel-panel">
      <div class="mccoppin-column-content-region-inner mccoppin-column3-inner panel-panel-inner">
        <div class="panel-pane pane-custom pane-3"  >
  
      
  
  <div class="pane-content">
    <h1 class="rteright">to win</h1>
<div class="panel-right">
<p class="rtejustify"><span style="color:#f39">We are courageous. </span>Our coaching is our unique difference. We are not afraid to break norms to help you generate exceptional and sustainable results. </p>
</div>
  </div>

  
  </div>
      </div>
    </div>
  </div>
  
</div><!-- /.mccoppin -->
  </div>

      <footer>
          </footer>
  
    </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
        </div>
      </section>

          </div>
  </div></div>
<div id="center-banner-wrap"><div id="center-banner-front" class="container" ><div class="region region-center-banner-front">
  <div id="block-views-testimonials-front-block" class="block block-views">

      
  <div class="content">
    <div class="view view-testimonials-front view-id-testimonials_front view-display-id-block view-dom-id-05b13a8e47275d33a69dbea1b7dc0d13">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
    <div id="views_slideshow_cycle_main_testimonials_front-block_1" class="views_slideshow_cycle_main views_slideshow_main"><div id="views_slideshow_cycle_teaser_section_testimonials_front-block_1" class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
  <div id="views_slideshow_cycle_div_testimonials_front-block_1_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-first views-row-odd" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
    
          <h3 class="field-content"><p>“We have an ambition to change IT to support the business ambitions. The Quantum Plus team provided expert advice, challenged our thinking, and supported us throughout the programme. Without their extensive experience, capability, and breadth of knowledge we would not have been able to make the difference we are now implementing. They are continuing to support us in our ambition to mature the function, and I have no hesitation in recommending them in assisting and leading initiatives related to operating and sourcing models”<br />
<strong>Director of IT Strategic Projects</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/boots-logo.png?itok=a6tiVi0G" width="220" height="114" alt="Boots UK Logo" title="Boots UK Logo" /></h3>  </div>
</div>
<div id="views_slideshow_cycle_div_testimonials_front-block_1_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
          <h3 class="field-content"><p>"Many global businesses face challenges to reduce cost and drive efficiencies, with the aim of providing a more superior customer service. We are excited and looking forward to benefiting from NIIT Technologies’ extensive experience and comprehensive portfolio of managed services which will allow us to focus on our core competencies, improve productivity and enable business innovation"<br />
<strong>CIO</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/eurostar-logo.png?itok=ILvBXCO0" width="220" height="114" alt="Eurostar Logo" title="Eurostar Logo" /></h3>  </div>
</div>
<div id="views_slideshow_cycle_div_testimonials_front-block_1_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
          <h3 class="field-content"><p>“We have a long-standing relationship with Quantum Plus, who has delivered savings to the business many times over using their practical 'hands on' approach. They have helped us navigate the complex commercials of outsourcing, while demonstrating expertise in best practice contract negotiation and a real appreciation of the IT context. I have always been impressed with Quantum Plus' flexibility and people; in fact, I have recommended them to other senior Directors within the Retail sector”<br />
<strong>Business Systems and Change Director</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/matalan-logo.png?itok=YnSaGBe_" width="220" height="114" alt="Matalan Retail Logo" title="Matalan Retail Logo" /></h3>  </div>
</div>
<div id="views_slideshow_cycle_div_testimonials_front-block_1_3" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
          <h3 class="field-content"><p>“We have successfully renewed our relationship with our existing supplier on a significantly improved basis. This was due in no small part to having the sound advice and energetic support of Quantum Plus by our side throughout – from initial planning and specification of ‘step-up’ requirements through to drafting and negotiating the new contract. Quantum Plus proved to be a valuable part of our team and I would recommend them without hesitation”<br />
<strong>CIO, Taylor Wimpey</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/taylor-wimpey-logo_0.png?itok=c8CExB-F" width="220" height="114" alt="Taylor Wimpey Logo" title="Taylor Wimpey Logo" /></h3>  </div>
</div>
<div id="views_slideshow_cycle_div_testimonials_front-block_1_4" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-5 views_slideshow_cycle_hidden views-row-odd" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
          <h3 class="field-content"><p>“The QP team were comprised of high calibre individuals who understood technology, provided positive challenge to our team as a “critical friend” and helped position our technology solutions with the customer to ensure they could be easily consumed and added demonstrable business value to the customer. We would look forward to working with Quantum Plus in the future.”</p>
<p><strong>Director, Advisory Programme HCL</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/hcl-logo.png?itok=DQyyUWxc" width="220" height="114" alt="HCL Logo" title="HCL Logo" /></h3>  </div>
</div>
<div id="views_slideshow_cycle_div_testimonials_front-block_1_5" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-6 views_slideshow_cycle_hidden views-row-last views-row-even" >
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
          <h3 class="field-content"><p>“We’ve been working with the Quantum Plus team on strategic sales initiatives and have found them to be a highly experienced, well connected and professional team.  Their capacity to deliver work sometimes at short notice always with thought leadership, challenge and creativity is impressive. Working with Quantum Plus as a strategic partner has been a key differentiator for us in an number of new opportunities for Unisys and we will continue to work together as a great team.”</p>
<p><strong>EMEA Advisor Relations Director</strong></p>
</h3>    
          <h3 class="field-content"><img src="https://www.quantumplus.co.uk/sites/default/files/styles/medium/public/case-studies/unisys-logo_0.png?itok=n7KshaNE" width="220" height="114" alt="Unisys Logo" title="Unisys Logo" /></h3>  </div>
</div>
</div>
</div>
      </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div><div class="clearfix"></div></div>
<div id="appendix-wrap" class="container" >
<div id="appendix-first" ><div class="region region-appendix-first">
  <div id="block-block-3" class="block block-block">

      
  <div class="content">
    <div class="box">
<p><a href="/desired-outcomes"><strong>Desired Outcomes</strong></a><br />
is our unique approach to delivering your vision of future success– fast.</p>
<p><a class="box-button" href="/desired-outcomes">Read More</a></p>
</div>
<div class="box">
<p><a href="/delivering-results"><strong>Delivering Results</strong></a><br />
we go far beyond the basics to help your services to be the best that they can be.</p>
<p><a class="box-button" href="/delivering-results">Read More</a></p>
</div>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
<div id="appendix-second" ><div class="region region-appendix-second">
  <div id="block-block-9" class="block block-block">

      
  <div class="content">
    <div class="box">
<p><a href="/defining-strategy"><strong>Defining the Strategy</strong></a><br />
our approach for a strategy that is a clear vision and roadmap to the future you want.</p>
<p><a class="box-button" href="/defining-strategy">Read More</a></p>
</div>
<div class="box">
<p><a href="/review-and-remediation"><strong>Review and Remediation</strong></a><br />
identifying and delivering opportunities performance improvement, is our speciality.</p>
<p><a class="box-button" href="/review-and-remediation">Read More</a></p>
</div>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
<div id="appendix-third" ><div class="region region-appendix-third">
  <div id="block-block-18" class="block block-block">

      
  <div class="content">
    <div class="box">
<p><a href="/implementing-change"><strong>Implementing the Change</strong></a><br />
with challenging and impartial advice we will help you deliver effective outcomes.</p>
<p><a class="box-button" href="/implementing-change">Read More</a></p>
</div>
<div class="box">
<p><a href="/supply-side-services"><strong>Supply Side Services</strong></a><br />
we work supply side to drive the best possible results from sourcing.</p>
<p><a class="box-button" href="/supply-side-services">Read More</a></p>
</div>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div><div class="clearfix"></div>
</div>




      <div id="footer-wrap" class="site-footer clr">
    <div id="footer" class="clr container">
              <div id="footer-block-wrap" class="clr">
          <div class="span-1-of-3 col col-1 footer-block ">
            <div class="region region-footer-first">
  <div id="block-twitter-block-1" class="block block-twitter-block">

        <h2 ><span>Follow Us On Twitter</span></h2>
    
  <div class="content">
    <a href="https://twitter.com/QuantumPlusInt" class="twitter-timeline" data-widget-id="459802841486348288" data-theme="dark" data-link-color="#F39" width="285" height="280" data-chrome="noheader nofooter noborders noscrollbar transparent" lang="en" data-tweet-limit="2" data-aria-polite="polite">Tweets by QuantumPlusInt</a>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span-1-of-3 col col-2 footer-block ">
            <div class="region region-footer-second">
  <div id="block-views-latest-news-blog-posts-block" class="block block-views">

        <h2 ><span>Latest News &amp; Blog Posts</span></h2>
    
  <div class="content">
    <div class="view view-latest-news-blog-posts view-id-latest_news_blog_posts view-display-id-block view-dom-id-ac1fc3fa945ce93e02646e98dd258fb2">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <h6 class="field-content"><a href="/blog/agile-sourcing">Agile Sourcing</a></h6>  </div>  
  <div class="views-field views-field-body">        <span class="field-content"><p>As outsourcing relationships evolve to meet the new demands of the age of digital transformation, traditional ways of sourcing procurement...</span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <h6 class="field-content"><a href="/blog/exit-and-exit-management">Exit and Exit Management </a></h6>  </div>  
  <div class="views-field views-field-body">        <span class="field-content"><p>The Dreaded Exit Plan Review!</p>
<p>You can hear it now – “We need to review the Exit Plan” and everyone suddenly has a more pressing...</span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <h6 class="field-content"><a href="/blog/hitting-target-%E2%80%8B-sweet-spot-achieving-value-transformation">Hitting the Target -​The Sweet Spot of Achieving Value &amp; Transformation</a></h6>  </div>  
  <div class="views-field views-field-body">        <span class="field-content"><p>Latest research from Quantum Plus shows clients are unanimous in their disappointment with the value brought to them by their supplier’s mid-...</span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="span-1-of-3 col col-3 footer-block ">
            <div class="region region-footer-third">
  <div id="block-block-5" class="block block-block">

        <h2 ><span>Get In Touch</span></h2>
    
  <div class="content">
    <p><strong>Quantum Plus Limited</strong><br />
Quantum House, 6 Shottery Brook Office Park,<br />
Timothy’s Bridge Road, Stratford-upon-Avon,<br />
Warwickshire, CV37 9NR,<br />
United Kingdom.</p>
<h2><span style="font-size:18px">Call: +44 1789 201 630</span></h2>
<p><strong>Email:</strong>  <a href="mailto:enquiries@quantumplus.co.uk">enquiries@quantumplus.co.uk</a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>        </div>
            
      <div id="footer-links" ><div class="region region-footer-links">
  <div id="block-block-6" class="block block-block">

      
  <div class="content">
    <p>© Copyright Quantum Plus 2019  |  All Rights Reserved    |   <a href="/terms-of-use">Terms of Use</a>  |   <a href="/privacy-policy">Privacy Policy</a>  |  <a href="/cookie-policy">Cookie Policy</a>  |</p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div> <div id="social-media" ><div class="region region-social-media">
  <div id="block-block-12" class="block block-block">

      
  <div class="content">
    <p><a href="https://twitter.com/@QuantumPlusInt"><img alt="Twitter" src="/sites/all/themes/quantumplus/images/twitter.png" style="height:30px; width:30px" /></a>  <a href="http://www.linkedin.com/company/quantum-plus"><img alt="Linkin In" src="/sites/all/themes/quantumplus/images/linked-in.png" style="height:30px; width:30px" /></a>  </p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div> 
    </div>
  </div>
  </div>
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
function euCookieComplianceLoadScripts() {}
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
<script type="text/javascript" src="https://www.quantumplus.co.uk/sites/default/files/js/js_2lJC5gTZ8B3-P19n2LPEd0viiCEmLJ5gcIAFYex1r0U.js"></script>
</body>
</html>