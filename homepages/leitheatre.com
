
<!DOCTYPE html>
<html dir="ltr" lang="zxx">

  <head>
    <meta charset="utf-8">
  <title>Welcome to Leitheatre</title>
    <meta name="description" content="Welcome to Leitheatre">
    <meta name="author" content="1st Class Media">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/images/favicon.png?t=1575285519">

    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Plugins -->
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/settings.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/layers.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/navigation.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/slick/slick.css" rel="stylesheet">
    

    <link href="css/style.css" rel="stylesheet" >
    <link href="css/typography-default.css" rel="stylesheet" >
    <link href="css/leitheatre.css" rel="stylesheet">
    <link href="css/leitheatre_custom.css" rel="stylesheet">
    
  </head>

  <body class="front-page ">

    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">
      <!-- header-container start -->
      <div class="header-container">
        <!-- header-top start -->
        <!-- classes:  -->
        <!-- "dark": dark version of header top e.g. class="header-top dark" -->
        <!-- "colored": colored version of header top e.g. class="header-top colored" -->
        <!-- ================ -->
        <div class="header-top colored">
          <div class="container">
            <div class="row">
              <div class="col-2 col-md-5">
                <!-- header-top-first start -->
                <!-- ================ -->
                <div class="header-top-first clearfix">
                  <ul class="social-links circle small clearfix hidden-sm-down">
                    <li class="facebook"><a href="https://www.facebook.com/Leitheatre/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                  </ul>
                  <div class="social-links hidden-md-up circle small">
                    <div class="btn-group dropdown">
                      <button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                      <ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
                        <li class="facebook"><a href="https://www.facebook.com/Leitheatre/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- header-top-first end -->
              </div>
              <div class="col-10 col-md-7">

                <!-- header-top-second start -->
                <!-- ================ -->
                <div id="header-top-second"  class="clearfix text-right">
                  <ul class="list-inline">
                    <li class="list-inline-item"><i class="far fa-envelope"></i> info@leitheatre.com</li>
					 <li class="list-inline-item"><i class="fas fa-phone-square"></i> 0131 661 2626</li>
                  </ul>
                </div>
                <!-- header-top-second end -->

              </div>
            </div>
          </div>
        </div>
        <!-- header-top end -->
        <header class="header fixed fixed-desktop clearfix">
          <div class="container">
            <div class="row">
              <div class="col-md-auto hidden-md-down pl-3">
                <div class="header-first clearfix">
                  <div id="logo" class="logo">
                    <a href="/"><img id="logo_img" src="/images/leitheatre_logo.png" alt="Leitheatre Logo"></a>
                  </div>
                </div>
                <!-- header-first end -->

              </div>
              <div class="col-lg-8 ml-lg-auto">
          
                <!-- header-second start -->
                <!-- ================ -->
                <div class="header-second clearfix">             
                


<div class="main-navigation main-navigation--mega-menu  animated">
                  <nav class="navbar navbar-expand-lg navbar-light p-0">
                    <div class="navbar-brand clearfix hidden-lg-up">

                      <!-- logo -->
                      <div id="logo-mobile" class="logo">
                        <a href="/"><img id="logo-img-mobile" src="images/leitheatre_logo.png" alt="Leitheatre Logo"></a>
                      </div>
                    </div>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                      <!-- main-menu -->
                      <ul class="navbar-nav ml-xl-auto">
                        <li class="nav-item dropdown active mega-menu mega-menu--wide">
                          <a href="/" class="nav-link" id="first-dropdown">Home</a>
                        </li>
						  <!--about menu-->
						<li class="nav-item dropdown ">
                          <a href="#" class="nav-link dropdown-toggle" id="fifth-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
                          <ul class="dropdown-menu" aria-labelledby="3-dropdown"><li class="nav-item"><a href="/about-leitheatre/our-history/" id="3-dropdown">Our History</a></li><li class="nav-item"><a href="/about-leitheatre/about-leitheatre/" id="3-dropdown">About Leitheatre</a></li></ul>                        </li>
						  <!--about end-->

                        <li class="nav-item dropdown ">
                          <a href="#" class="nav-link dropdown-toggle" id="third-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Productions</a>
                          <ul class="dropdown-menu" aria-labelledby="third-dropdown">
                            <li class="dropdown ">
                              <a  class="dropdown-toggle" data-toggle="dropdown" href="#">Main Productions</a>
                              <ul class="dropdown-menu">
                                <li ><a href="/productions/club-productions/2010/">2010s</a></li><li ><a href="/productions/club-productions/2000/">2000s</a></li><li ><a href="/productions/club-productions/1990/">1990s</a></li><li ><a href="/productions/club-productions/1980/">1980s</a></li><li ><a href="/productions/club-productions/1970/">1970s</a></li><li ><a href="/productions/club-productions/1960/">1960s</a></li>                              </ul>
                            </li>
                            <li class="dropdown ">
                              <a  class="dropdown-toggle" data-toggle="dropdown" href="#">Festival Fringe Productions</a>
                              <ul class="dropdown-menu">
                                <li ><a href="/productions/fringe-productions/2010/">2010s</a></li><li ><a href="/productions/fringe-productions/2000/">2000s</a></li><li ><a href="/productions/fringe-productions/1990/">1990s</a></li><li ><a href="/productions/fringe-productions/1980/">1980s</a></li><li ><a href="/productions/fringe-productions/1970/">1970s</a></li><li ><a href="/productions/fringe-productions/1960/">1960s</a></li>                              </ul>
                            </li>
                            <li class="dropdown ">
                              <a  class="dropdown-toggle" data-toggle="dropdown" href="#">SCDA One-Act Plays</a>
                              <ul class="dropdown-menu">
                                <li ><a href="/productions/one-act-play-productions/2010/">2010s</a></li><li ><a href="/productions/one-act-play-productions/2000/">2000s</a></li><li ><a href="/productions/one-act-play-productions/1990/">1990s</a></li><li ><a href="/productions/one-act-play-productions/1980/">1980s</a></li><li ><a href="/productions/one-act-play-productions/1970/">1970s</a></li><li ><a href="/productions/one-act-play-productions/1960/">1960s</a></li>                              </ul>
                            </li>
                            <!--<li class="dropdown ">
                              <a  class="dropdown-toggle" data-toggle="dropdown" href="#">Other Productions </a>
                              <ul class="dropdown-menu">
                                <li ><a href="/productions/other-productions/2010/">2010s</a></li><li ><a href="/productions/other-productions/2000/">2000s</a></li><li ><a href="/productions/other-productions/1990/">1990s</a></li><li ><a href="/productions/other-productions/1980/">1980s</a></li><li ><a href="/productions/other-productions/1970/">1970s</a></li><li ><a href="/productions/other-productions/1960/">1960s</a></li>                              </ul>
                            </li>-->
							  <li >
                              <a   href="/production/listing/">Production Directory </a>
                              
                            </li>
                          </ul>
                        </li>

                        <li class="nav-item ">
                          <a href="/membership/join-us/" class="nav-link " id="third-dropdown">Join our club</a>
                        </li>
                        <li class="nav-item dropdown ">
                          <a href="" class="nav-link dropdown-toggle" id="seventh-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Find Us</a>
                          <ul class="dropdown-menu" aria-labelledby="seventh-dropdown">
                            <li ><a href="/clubrooms/">Clubrooms Location</a></li>
                            <!--<li ><a href="theatre_locations.php">Production Locations</a></li>-->
                            <li ><a href="https://www.facebook.com/Leitheatre/" target="_blank">Facebook</a></li>
							  <li ><a href="https://instagram.com/leitheatre22" target="_blank">Instagram</a></li>
							  <li ><a href="http://www.twitter.com/LeitheatreEdin" target="_blank">Twitter</a></li>
                          </ul>
                        </li>
						  
                      </ul>
                      <!-- main-menu end -->
                    </div>
                  </nav>
                </div>
                <!-- main-navigation end -->
                </div>
                <!-- header-second end -->

              </div>
              <div class="col-auto hidden-md-down pl-0 pl-md-1">
                <!-- header dropdown buttons -->
                <div class="header-dropdown-buttons">
                  <a href="/contact/" class="btn btn-sm btn-default">Contact Us <i class="fa fa-envelope-o pl-1"></i></a>
                </div>
                <!-- header dropdown buttons end -->
              </div>
            </div>
          </div>
        </header>
        <!-- header end -->
      </div>
      <!-- header-container end -->
      <!-- banner start -->
      <!-- ================ -->
      <div class="banner clearfix">

        <!-- slideshow start -->
        <!-- ================ -->
        <div class="slideshow">

          <!-- slider revolution start -->
          <!-- ================ -->
          <div class="slider-revolution-5-container">
            <div id="slider-banner-fullwidth-big-height" class="slider-banner-fullwidth-big-height rev_slider" data-version="5.0">
              <ul class="slides">
				                 <li data-transition="fadefromright" data-slotamount="default" data-masterspeed="default" data-title="Latest Production - On Golden Pond">
                <img src="/uploads/pagebanners/156882367737108300.jpeg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover"  class="rev-slidebg">
				<div class="tp-caption dark-translucent-bg caption-box text-left"
                  style="background-color: rgba(0, 0, 0, 0.7);"
                  data-x="left"
                  data-y="center"
                  data-start="1000"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;"
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                  <h2 class="title"> <strong>On Golden Pond</strong></h2>
                  <div class="separator-2 clearfix"></div>
                  <p class="hidden-sm-down">Next Production at Church Hill Theatre</p>
					<h3>Wed 20 to Fri 22 November 19:30, Sat 23 at 14:30, Church Hill Theatre</h3>
					                  <div class="text-right"><a target="_blank" class="btn btn-sm btn-gray-transparent margin-clear" href="https://www.thequeenshall.net/whats-on/golden-pond">Buy Tickets</a></div>
															
                </div>

                </li>
				  


              </ul>
              <div class="tp-bannertimer"></div>
            </div>
          </div>

        </div>
      </div>

      <div id="page-start"></div>

      <!-- section start -->
      <!-- ================ -->
      <section class="pv-40">
        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">
              <h3 class="title">Welcome to Leitheatre</h3>
              <div class="separator-2"></div>
              <div class="row">
                <div class="col-lg-6">
                  <p> <p>Reviews are in for On Golden Pond.</p>
<p>&nbsp;<a href="http://www.alledinburghtheatre.com/on-golden-pond-leitheatre-november-2019-review/" target="_blank">All Edinburgh Theatre</a></p>
<p><a href="https://radiosummerhallreviews.wordpress.com/2019/11/21/on-golden-pond-leitheatre-at-churchill-theatre/" target="_blank">Radio Summerhall Reviews</a></p>
<p>Have a look at some production photos from our current production, On Golden Pond. Link here:</p>
<p><a href="https://www.leitheatre.com/production/2010/on-golden-pond-139/" target="_blank">https://www.leitheatre.com/production/2010/on-golden-pond-139/</a></p>
<p>or go to Productions - Main Productions- 2010s</p>
<p>Leitheatre is an amateur theatre group based in Edinburgh. We usually produce three full length productions per year, including one in the Edinburgh Festival Fringe. We also take part in the Scottish Community Drama Association (SCDA) One Act Play Festival. Our productions take place in the Church Hill Theatre,the Studio Theatre and Inverleith St Serf's Church Centre.We also produce a Burns Show in January to celebrate the birth of Scotland's Bard, Robert Burns. This show is performed for other organisations such as Golf Clubs, Care Homes and other social clubs. If you are interested in booking the Burns show, please get in touch via the 'Contact Us' button above.</p>
<p>Details of past productions are being added daily!</p>
<p>You will also&nbsp; findus on&nbsp;<a href="https://www.facebook.com/Leitheatre/" target="_blank">Facebook</a>,&nbsp;<a href="https://instagram.com/leitheatre22" target="_blank">Instagram</a>&nbsp;and <a href="http://www.twitter.com/LeitheatreEdin" target="_blank">Twitter</a>.</p></p>

                </div>
                <div class="col-lg-6">
                  <div class="slick-carousel content-slider-with-controls">
                    <div class="overlay-container overlay-visible">
                      <img src="/images/home-1.jpg" alt="">
                      <div class="overlay-bottom hidden-sm-down">
                        <div class="text">
                          <h3 class="title">Queen Elizabeth Slept Here (1976)</h3>
                        </div>
                      </div>
                      <a href="/images/home-1.jpg" class="slick-carousel--popup-img overlay-link" title="image title"><i class="fas fa-plus"></i></a>
                    </div>
                    <div class="overlay-container overlay-visible">
                      <img src="images/home-2.jpg" alt="">
                      <div class="overlay-bottom hidden-sm-down">
                        <div class="text">
                          <h3 class="title">Night Must Fall (1979)</h3>
                        </div>
                      </div>
                      <a href="/images/home-2.jpg" class="slick-carousel--popup-img overlay-link" title="image title"><i class="fas fa-plus"></i></a>
                    </div>
                    <div class="overlay-container overlay-visible">
                      <img src="/images/home-3.jpg" alt="">
                      <div class="overlay-bottom hidden-sm-down">
                        <div class="text">
                          <h3 class="title">Juno and the Paycock (1980)</h3>
                        </div>
                      </div>
                      <a href="images/home-3.jpg" class="slick-carousel--popup-img overlay-link" title="image title"><i class="fas fa-plus"></i></a>
                    </div>
					 <div class="overlay-container overlay-visible">
                      <img src="/images/home-4.jpg" alt="">
                      <div class="overlay-bottom hidden-sm-down">
                        <div class="text">
                          <h3 class="title">Crucible (1986)</h3>
                        </div>
                      </div>
                      <a href="images/home-4.jpg" class="slick-carousel--popup-img overlay-link" title="image title"><i class="fas fa-plus"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- section end -->
		
		<section class="clearfix pv-40">
        <div class="container">
          <div class="row justify-content-lg-center">

            <div class="col-lg-8 text-center">
              <h1 class="page-title text-center">Past <strong>Productions</strong></h1>
              <div class="separator"></div>
            </div>

            <div class="col-12">


              <div class="isotope-container row grid-space-10">
				  
                <div class="col-lg-3 col-md-6 isotope-item construction-management">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show1.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">An Inspector Calls</a></h3>
                        </div>
                      </div>
						<div class="overlay-bottom">
                              <p class="small">1986</p>
                            </div>

                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item construction-management">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show2.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">A View from the Bridge</a></h3>
                        </div>
                      </div>
						<div class="overlay-bottom">
                              <p class="small">1981</p>
                            </div>

                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item construction-management">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show3.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">The Honours of Drumlie</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1979</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item interior-design">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show4.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">Equus</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1987</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item architecture-plans">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show5.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">A Christmas Carol</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1987</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item construction-management">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show9.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">Separate Tables</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1999</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item architecture-plans">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show7.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">The Anatomist</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1988</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 isotope-item interior-design">
                  <div class="image-box hc-shadow bordered text-center mb-20">
                    <div class="overlay-container">
                      <img src="/images/show8.jpg" alt="">
                      <div class="overlay-top">
                        <div class="text">
                          <h3><a href="#">Death of a Salesman</a></h3>
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <p class="small">1991</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<!--  <div class="dark-bg default-hovered footer-top animated-text">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="call-to-action text-center">
                <div class="row">
                  <div class="col-md-8">
                    <h3 class="mt-1"><strong>Next Production:</strong></h3>
                    <h3 class="mt-1">Cat on a Hot Tin Roof. May 15-18 2019, Festival Theatre, Studio</h3>
                  </div>
                  <div class="col-md-1">
                    <p class="mt-2"><a href="https://www.capitaltheatres.com/whats-on/cat-on-a-hot-tin-roof" class="btn btn-animated btn-lg btn-gray-transparent">Get Tickets Here <i class="fas fa-ticket-alt"></i></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->    

<footer id="footer" class="clearfix dark">
        <div class="footer">
          <div class="container">
            <div class="footer-inner">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <div class="footer-content">
                    <div class="logo-footer">
                    <p align="center"><img id="logo-footer" src="/images/leitheatre_logo_footer.png" alt=""></p>
					 </div>
                    <ul class="list-inline mb-20">
                      <li class="list-inline-item"><i class="fas fa-map-marker-alt"></i> 20 Sunnyside, Easter Road<br>Edinburgh, United Kingdom</li>
						 <li class="list-item"><i class="fas fa-phone-square"></i> 0131 661 2626</li>	
						
                      <li class="list-item"><a href="#" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-1"></i><i class="far fa-envelope"></i> info@leitheatre.com</a></li>
                    </ul>

                    <ul class="social-links circle margin-clear animated-effect-1">
                      <li class="facebook"><a href="https://www.facebook.com/Leitheatre/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                      <li class="twitter"><a href="https://twitter.com/leitheatreedin?lang=en" target="_blank"><i class="fab fa-twitter"></i></a></li>
						<li class="instagram"><a href="https://instagram.com/leitheatre22" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
				  </div>
              </div>
            </div>
          </div>
        </div>
        <!-- .footer end -->
        <!-- .subfooter start -->
        <!-- ================ -->
        <div class="subfooter">
          <div class="container">
            <div class="subfooter-inner">
              <div class="row">
                <div class="col-md-12">
                  <p class="text-center">Copyright &copy;2019. All rights reserved. Leitheatre is a registered charity</p>
					<div class="pull-right col-md-2 text-right"><a href="https://www.1stclassmedia.co.uk" title="website designer and developer in Edinburgh, Midlothian" target="_blank"><img class="pull-right" src="https://www.1stclassmedia.co.uk/assets/website-by-1stclass-gray.png"></a></p></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- .subfooter end -->

      </footer>

<SCRIPT LANGUAGE="JAVASCRIPT" TYPE="TEXT/JAVASCRIPT"> 
  //$(document).ready(function() {
  var width = window.screen.availWidth;
  var height = window.screen.availHeight;
  var scres = width+":"+height;

		document.write('<img src="/manager/simplestats/trackit.php?scr='+scres+'&r='+document.referrer+'">'); 
//});
	</SCRIPT>      </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script src="plugins/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery Revolution Slider  -->
    <script src="plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js"></script>
    <script src="plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Isotope javascript -->
    <script src="plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="plugins/waypoints/sticky.min.js"></script>
    <!-- Count To javascript -->
    <script src="plugins/countTo/jquery.countTo.js"></script>
    <!-- Slick carousel javascript -->
    <script src="plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="js/custom.js"></script>

  </body>
</html>
