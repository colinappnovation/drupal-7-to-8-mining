<!DOCTYPE html>
  <!--[if IEMobile 7]><html class="no-js ie iem7" lang="en" dir="ltr"><![endif]-->
  <!--[if lte IE 6]><html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
  <!--[if (IE 7)&(!IEMobile)]><html class="no-js ie lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
  <!--[if IE 8]><html class="no-js ie lt-ie9" lang="en" dir="ltr"><![endif]-->
  <!--[if (gte IE 9)|(gt IEMobile 7)]><html class="no-js ie" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><![endif]-->
  <!--[if !IE]><!--><html class="no-js" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/images/giftofsight-ico.png" type="image/png" />
<link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
<meta name="HandheldFriendly" content="true" />
<meta name="description" content="The Gift of Sight Appeal was established with the aim of funding world-class research into the prevention and treatment of blindness. Our research into Age-related macular degeneration could also provide therapies for other blinding retinal diseases." />
<meta name="viewport" content="width=device-width" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.giftofsight.org.uk/finding-cures-blinding-eye-diseases" />
<link rel="shortlink" href="https://www.giftofsight.org.uk/node/1" />
  <title>Finding cures for blinding eye diseases | Gift of Sight</title>
  <link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_Gzq3zhtw-aGjM43AxGc1YEJKWdD_cZPw_KkPfBAlmW8.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_RnUQ1xAOAfZ--3aVU8OLMaBQegt5GvUP4tkEu7X8vJk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700,400italic&amp;subset=latin,greek" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&amp;subset=greek" media="all" />
<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_lwAY3wC8Nu6aeyvI1SUkJHsLLgv0mvfR7DhYUAETlMw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_32eFU78Ec4B-kOCin2QF-vzOtiFmMdoM__zS1qrGs_E.css" media="print" />
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_fgZvFnTpOeWOuAcOlZLvX2PdvTGaP_jhdgpnAriA1LU.css" media="all" />

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_18n6Xx7ZR4YUfpjKjB3GJEP6xDvDxrVyspia0QquR_I.css" media="all" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_x_Wxz_svyD5LKNMCuR1x1oDGZ1rT38JuWQUrD78w1Yw.css" media="all" />

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/css/css_x_Wxz_svyD5LKNMCuR1x1oDGZ1rT38JuWQUrD78w1Yw.css" media="all" />
<![endif]-->
  <script src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/js/js_rvX88atDavNAF53kmYXXUetdvDCPoVajE-Qm_rkz9xE.js"></script>
<script src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/js/js_Dvc7F7qL7WeJsTsJwx1uoCYbS-7JbWYhZZsq4qDBGCw.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-27847926-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/js/js_WnBf8niVFyswgqej42ZFK8a_dUoBKJi5Gxkh-qoCWDE.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":false,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":7000,"directionNav":true,"controlNav":true,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/":true}});</script>
</head>
<body class="html front not-logged-in page-node page-node- page-node-1 node-type-page">
  <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
    <div class="l-page has-one-sidebar has-sidebar-first">

      <!-- l-wrapper--top-navigation -->
    <div class="l-wrapper--top-navigation">
      <!-- l-region--top-navigation -->
        <div class="l-region l-region--top-navigation">
    <div id="block-block-2" class="block block--block block--accessibility block--block-2">
        <div class="block__content">
    <p><a href="javascript:(function(){d=document;lf=d.createElement('script');lf.type='text/javascript';lf.id='ToolbarStarter';lf.text='var%20StudyBarNoSandbox=true';d.getElementsByTagName('head')[0].appendChild(lf);jf=d.createElement('script');jf.src='https://core.atbar.org/atbar/en/latest/atbar.min.js';jf.type='text/javascript';jf.id='ToolBar';d.getElementsByTagName('head')[0].appendChild(jf);})()" target="" title="">Accessibility Tools</a></p>
  </div>
</div>
  </div>
    </div>
  
  <!-- l-wrapper--branding -->
  <div class="l-wrapper--branding">
    <div class="l-constrained">

      <!-- l-region--logo -->
      <div class="l-region--logo">
                  <a href="/" title="Home" rel="home" class="branding__logo"><img src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/images/logo.png" alt="Home"/></a>
                              </div>

      <!-- l-region--branding -->
        <div class="l-region l-region--branding">
    <div id="block-block-1" class="block block--block block--donate block--block-1">
        <div class="block__content">
    <p><a href="https://donatenow.soton.ac.uk/index.php?fund=Gift%20of%20Sight" target="_blank">Donate now</a></p>
  </div>
</div>
  </div>

    </div>
  </div>

  
  
  
      <!-- l-wrapper--navigation -->
    <div class="l-wrapper--navigation">
      <!-- l-region--navigation -->
        <div class="l-region l-region--navigation">
    <nav id="block-system-main-menu" role="navigation" class="block block--system block--menu block--system-main-menu">
      
  <ul class="menu"><li class="first leaf active-trail"><a href="/finding-cures-blinding-eye-diseases" title="Gift of Sight funds support vital research into blinding eye diseases" class="active-trail active">Home</a></li>
<li class="expanded"><a href="/about-us" title="The Gift of Sight Appeal funds world-class research into the prevention and treatment of blindness at the University of Southampton.">About us</a><ul class="menu"><li class="first leaf"><a href="/about-us/history">History</a></li>
<li class="leaf"><a href="/about-us/professor-andrew-lotery">Professor Lotery</a></li>
<li class="leaf"><a href="/about-us/president-patrons-and-advisory-board">President, Patrons and Advisory Board</a></li>
<li class="last leaf"><a href="/about-us/our-senior-research-team">Our Senior Research Team</a></li>
</ul></li>
<li class="expanded"><a href="/eye-conditions" title="Our scientists have a &#039;bench to bedside&#039; approach, working with patients and clinicians in Southampton Eye Unit">Eye conditions</a><ul class="menu"><li class="first leaf"><a href="/eye-conditions/dry-age-related-macular-degeneration">Dry Age Related Macular Degeneration</a></li>
<li class="leaf"><a href="/eye-conditions/wet-age-related-macular-degeneration">Wet Age Related Macular Degeneration</a></li>
<li class="leaf"><a href="/eye-conditions/childrens-eye-diseases">Childrens Eye research</a></li>
<li class="leaf"><a href="/eye-conditions/corneal-research">Corneal research</a></li>
<li class="leaf"><a href="/eye-conditions/glaucoma-research">Glaucoma research</a></li>
<li class="leaf"><a href="/eye-conditions/how-does-eye-work">How does the eye work</a></li>
<li class="last leaf"><a href="/eye-conditions/checking-your-vision">Checking your vision</a></li>
</ul></li>
<li class="expanded"><a href="/support-us" title="We have been able to expand our vision research due to generous philanthropic gifts and legacies.">Support us</a><ul class="menu"><li class="first leaf"><a href="/support-us/ways-you-can-help" title="We depend on philanthropic gifts to support our research team and introductions to new potential donors is greatly appreciated.">Ways you can help</a></li>
<li class="leaf"><a href="/support-us/legacies-and-memory-donations" title="Some scientific staff and technicians are paid from generous legacy gifts and &#039;memory&#039; donations.">Memory donations</a></li>
<li class="leaf"><a href="/support-us/business-and-trust-support" title="Our thanks to Companies and Trusts who have generously helped Gift of Sight and who are happy for us to acknowledge them in public.">Business and Trust Support</a></li>
<li class="last leaf"><a href="/support-us/your-impact">Your impact</a></li>
</ul></li>
<li class="leaf"><a href="/news" title="Our &#039;news&#039; relates to press releases and some of the papers published by our scientists and students.">News</a></li>
<li class="leaf"><a href="/events" title="Please take a look at forthcoming events, many of which are organised by generous supporters.">Events</a></li>
<li class="leaf"><a href="/gallery" title="Photographs and thank you messages to our generous fundraisers.">Gallery</a></li>
<li class="expanded"><a href="/contact-us" title="Please do telephone us if you have any queries.  Our Privacy Statement is shown here too.">Contact us</a><ul class="menu"><li class="first last leaf"><a href="/gift-sight-privacy-statement" title="Overview of Gift of Sight Privacy Statement">Privacy Statement</a></li>
</ul></li>
<li class="last leaf"><a href="/eye-biobank" title="Southampton Eye Biobank offers individuals the opportunity to donate their eyes for the purpose of clinical research.">Eye Biobank</a></li>
</ul></nav>
  </div>
    </div>
    
    
  <!-- l-wrapper--main -->
  <div class="l-wrapper--main">
    <div class="l-constrained">
            
      <div class="l-content" role="main">
                <a id="main-content"></a>
                                        <div id="block-views-slider-block" class="block block--views block--views-slider-block">
        <div class="block__content">
    <div class="view view-slider view-id-slider view-display-id-block view-dom-id-3987027aba1a95d8996935dfda15e6bc">
        
  
  
      <div class="view-content">
      <div  id="flexslider-1" class="flexslider optionset-default">
  <ul class="slides"><li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/savannah5.jpg?itok=9b4PzV45&amp;c=14493b1b7509bc9d7b8c773c750589ae" alt="Funding postdoctoral scientists" />    
          <div class="summary"><h3>Funding postdoctoral scientists</h3>
</div>  </li>
<li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/jay-and-freddie.jpg?itok=8Fq-NzsM&amp;c=af45ea8ee3ebf2723880a777cb3f81f3" alt="Supporting research into acute bilateral cataracts" />    
          <div class="summary"><h3>Supporting research into acute bilateral cataracts in children</h3>
</div>  </li>
<li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/some-team.jpg?itok=pPdaWf93&amp;c=07be8370fba0b31873bb4e55acf3d1f7" alt="Fundraising trek for children&#039;s eye research" />    
          <div class="summary"><h3>Fundraising trek for children's eye research</h3>
</div>  </li>
<li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/lab-pic-june-2018a2.jpg?itok=cADG2Clu&amp;c=aa15c2285e4d2a362f2b46e68b19ae58" alt="Some members of our vision science research team" />    
          <div class="summary"><h3>Gift of Sight supports students, technicians and provides equipment for use in research studies.</h3>
</div>  </li>
<li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/cropped-adham-team-3.jpg?itok=a21hjtDu&amp;c=81546b3dab9e187babcc774bd6d8a02e" alt="Ophthalmology Clinical Research team with Hospital Heroes award" />    
          <div class="summary"><h3>Supporting the clinical trials teams</h3>
</div>  </li>
<li>  
          <img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/manual_crop_940x280/public/images/slider-images/patient-slit-lamp-1000x298.jpg?itok=Sf_ANzao&amp;c=11ae9b3b9df909abdd0deb103fe3a52c" alt="Viewing eyes through a slit lamp gives initial diagnosis" />    
          <div class="summary"><h3>Funding world-class vision research</h3>
</div>  </li>
</ul></div>
    </div>
  
  
  
  
  
  
</div>  </div>
</div>


  <!-- regular node view template HTML  -->
  <article  about="/finding-cures-blinding-eye-diseases" typeof="sioc:Item foaf:Document" role="article" class="node node--page node--full node--page--full">

          <a class="print-this-button" title="Print this page" href="javascript:window.print()">Print this page</a>
    
    
    <header>
                    <h1 class="node__title">
          <a href="/finding-cures-blinding-eye-diseases" rel="bookmark">Finding cures for blinding eye diseases</a>
        </h1>
            <span property="dc:title" content="Finding cures for blinding eye diseases" class="rdf-meta element-hidden"></span>    </header>


    

    <main class="node__content">
      <div class="field field--name-field-page-intro field--type-text-long field--label-hidden"><div class="field__items"><div class="field__item even"><p>The Gift of Sight Appeal was established with the aim of funding world-class research into the prevention and treatment of blindness. </p>
<p>Generous philanthropic gifts, including Legacies and Direct Debits, have helped us increase the number of staff employed in our vision science research group.  This enables us to train the next generation of academic ophthalmologists and visual scientists and to provide them with the equipment they need to undertake their vital sight saving projects.  </p>
<p><article about="/about-us/professor-andrew-lotery" role="article" typeof="sioc:Item foaf:Document"><br /><main> </main><br /></article></p>
</div></div></div><div class="field field--name-field-page-description field--type-text-long field--label-hidden"><div class="field__items"><div class="field__item even"><h3>The aims of the vision research group are as follows:</h3>
<ul><li>
<p>To perform world class research to create better treatments and thus prevent blindness</p>
</li>
<li>
<p>To achieve this by attracting sufficient funding to allow the research to proceed quickly</p>
</li>
<li>
<p>To train the next generation of academic ophthalmologists and visual scientists</p>
</li>
<li>
<p>To disseminate our research findings in leading research journals</p>
</li>
<li>To work as a team to achieve these goals</li>
</ul><p>Please view our latest   <a href="/sites/giftofsight.org.uk/files/attached_files/node-1/newsletter-july-2019.pdf">Newsletter</a></p>
<h4> </h4>
<p>With your support we aim to ensure vision into the future for those who are suffering today.</p>
<p>Details of our most recent research papers can be found by viewing the <a href="/about-us/our-senior-research-team">relevant staff pages</a>.</p>
<p> </p>
<h3>Appeal by Professor Andrew Lotery</h3>
<p>If you would like to donate to help our work please see our page on <a href="/support-us/ways-you-can-help">'How you can help'</a></p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/uwbgH3NgHB8" width="560"></iframe></p>
</div></div></div><div class="field field--name-field-page-attached-files field--type-file field--label-hidden"><div class="field__items"><div class="field__item even"><span class="file"><img class="file-icon" alt="" title="application/pdf" src="/modules/file/icons/application-pdf.png" /> <a href="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/attached_files/node-1/newsletter-july-2019.pdf" type="application/pdf; length=4577291" title="newsletter-july-2019.pdf" target="_blank">Newsletter July 2019</a></span></div></div></div>    </main>

            
  </article>

  <section class="node-reference">
    <div class="field field--name-field-page-node-reference field--type-entityreference field--label-hidden"><div class="field__items"><div class="field__item even">

  <!-- teaser template HTML -->
  <article nid="2"  about="/support-us" typeof="sioc:Item foaf:Document" role="article" class="node node--page node-teaser node--teaser node--page--teaser">

    <header>
      <h2 class="node__title">
        <a href="/support-us" rel="bookmark">Support us</a>
      </h2>
      <div class="field field--name-field-page-main-image field--type-image field--label-hidden"><div class="field__items"><div class="field__item even"><a href="/support-us"><img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/crop_480x270__16_9_/public/main-image/page/2/andrew-office-960x540.jpg?itok=pPPuQKki" width="480" height="270" alt="" /></a></div></div></div>          </header>

    <div class="field field--name-field-page-teaser field--type-text-long field--label-hidden"><div class="field__items"><div class="field__item even">Your donations will help us to support research into eye problems which affect all age ranges. </div></div></div>
    <div class="teaser-expand">

              <a class="teaser-expand-btn" href="/support-us">Read more</a>
          </div>

  </article>

</div><div class="field__item odd">

  <!-- teaser template HTML -->
  <article nid="9"  about="/eye-conditions" typeof="sioc:Item foaf:Document" role="article" class="node node--page node-teaser node--teaser node--page--teaser">

    <header>
      <h2 class="node__title">
        <a href="/eye-conditions" rel="bookmark">Eye conditions</a>
      </h2>
      <div class="field field--name-field-page-main-image field--type-image field--label-hidden"><div class="field__items"><div class="field__item even"><a href="/eye-conditions"><img typeof="foaf:Image" src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/styles/crop_480x270__16_9_/public/filefield_paths/slitlamp-1000.jpg?itok=ISHn0uMD" width="480" height="270" alt="" /></a></div></div></div>          </header>

    <div class="field field--name-field-page-teaser field--type-text-long field--label-hidden"><div class="field__items"><div class="field__item even">Many patients attend clinics for regular examinations to identify whether or not treatment is necessary.
</div></div></div>
    <div class="teaser-expand">

              <a class="teaser-expand-btn" href="/eye-conditions">Read more</a>
          </div>

  </article>

</div></div></div>      </section>

              </div>

        <aside class="l-region l-region--sidebar-first">
    <div id="block-search-form" role="search" class="block block--search block--search-form">
        <div class="block__content">
    <form class="search-block-form" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />  <label class="option" for="edit-search-block-form--2">Search </label>

</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-8lLh78Tk5AhtG6Xot58FXYayGMTaqUIgNU1L2aJj1kY" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>  </div>
</div>
<div id="block-uos-social-find-social-find-icons" class="block block--uos-social-find block--uos-social-find-social-find-icons">
        <h2 class="block__title">Find us on:</h2>
      <div class="block__content">
    <ul class="uos-social-find-link-list"><li><a class="facebook" href="https://www.facebook.com/Giftofsightappeal" target="_blank">Facebook<span>Like</span></a></li>
<li><a class="twitter" href="https://twitter.com/giftofsight1" target="_blank">Twitter<span>Tweet</span></a></li>
</ul>  </div>
</div>
<div id="block-block-5" class="block block--block block--block-5">
        <h2 class="block__title">Support us</h2>
      <div class="block__content">
    <p style="text-align: center;"><img alt="" src="/sites/giftofsight.org.uk/files/images/justtextgiving.jpg" style="width: 300px; height: 100px;" /></p>
<p style="text-align: center;">Text GOFS01 £3/£5/£10 to 70070</p>
  </div>
</div>
  </aside>
          </div>
  </div>

      <!-- l-wrapper--footer -->
    <footer class="l-wrapper--footer" role="contentinfo">
      <!-- l-region--foter -->
        <div class="l-region l-region--footer">
    <div id="block-block-3" class="block block--block block--block-3">
        <div class="block__content">
    <p>Gift of Sight<br />
Office of Development and Alumni Relations<br />
University of Southampton<br />
Building 37, Highfield<br />
Southampton<br />
Hampshire<br />
United Kingdom<br />
SO17 1BJ</p>
<p><strong>Telephone:</strong> +44 (0)23 8059 9073</p>
  </div>
</div>
<div id="block-block-4" class="block block--block block--block-4">
        <h2 class="block__title">Keep in touch</h2>
      <div class="block__content">
    <p>If you would like to receive our regular newsletter please email us:<br /><a href="mailto:info@giftofsight.org.uk">info@giftofsight.org.uk</a></p>
<p> </p>
<p><a href="/gift-sight-privacy-statement">Privacy Statement</a></p>
  </div>
</div>
  </div>
    </footer>
  
</div>
  <script src="https://www.giftofsight.org.uk/sites/giftofsight.org.uk/files/js/js_uTpGZRbRZm_lrt5640lI88hN-6jGIe3E3hxZcagIuss.js"></script>
</body>
</html>
