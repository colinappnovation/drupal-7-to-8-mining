


<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Shopify Developer Designers in London and New York | We Make Websites</title>
	<meta name="description" content="Shopify web design, development, integration and optimisation agency in London and New York. We help the world’s most ambitious brands sell more online. We blend technical excellence and e-commerce expertise to design, develop and optimise market-leading Shopify websites. Find out how we can grow your business." />

	<link rel="shortcut icon" type="image/x-icon" href="/css/images/favicon.ico" />

	<!-- Vendor Styles -->
	<link rel="stylesheet" href="https://use.typekit.net/xmq3akk.css">
	<link rel="stylesheet" href="/vendor/bootstrap-4.0.0/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/vendor/slick-1.8.0/slick/slick.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/vendor/aos/aos.css" type="text/css" media="all" />
	
	<!-- Our Styles -->
	<link rel="stylesheet" href="/css/style.css" />


</head>
<body>
<div class="wrapper">
	<header class="header">
		<div class="container-fluid">
			<div class="header__content">
				<a href="/" class="logo">
					<img src="/css/images/logos/ours/shopify-plus-developers@2x.png" alt="Shopify Developers We Make Websites" />
				</a>

				<a href="#" class="btn-menu">
					<strong>Menu</strong>

					<span></span>
				</a>
			</div><!-- /.header__content -->

			<div class="header__menu">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<nav class="nav">
								<ul>
	<li>
		<a href="/shopify-plus-portfolio">Work</a>
	</li>
		
	<li>
		<a href="/shopify-services">Services</a>
	</li>

	<li>
		<a href="/shopify-developer">Why us</a>
	</li>
	
	<li>
		<a href="/blog/">Blog</a>
	</li>
	
	<li>
		<a href="/shopify-careers">Careers</a>
	</li>
	
	<li>
		<a href="/contact">Contact</a>
	</li>
</ul>
							</nav><!-- /.nav -->
						</div><!-- /.col-md-6 -->

						<div class="col-md-6">
							<ul class="list-contact">
								<li>
									<h6>London</h6>

									<p>
										Sweeps Building, 6-7 St Cross Street,<br />

										London, EC1N 8UA
									</p>
								</li>
								
								<li>
									<h6>New york</h6>

									<p>
										419 Park Ave South, 3rd Floor,<br />

										New York, NY 10016
									</p>
								</li>
							</ul><!-- /.list-contact -->

							<a href="/shopify-services" class="logo">
								<img src="css/images/temp/logo-shopify-white@2x.png" alt="" />
							</a>
						</div><!-- /.col-md-6 -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div><!-- /.header__menu -->
		</div><!-- /.container -->
	</header><!-- /.header -->

<div class="main">
	<div class="intro">
		<div class="container">
			<div class="intro__inner">
				<div class="intro__aside">
					<div class="slider">
						<div class="slides">


				            <div class="slide">
				              <div class="slide__image"
				                style="background-image: url(/css/images/home-slider/finisterre.jpg);">
				                
				                <p>Finisterre, Shopify Plus</p>
				              </div><!-- /.slide__image -->
				            </div><!-- /.slide -->

				            <div class="slide">
				              <div class="slide__image"
				                style="background-image: url(/css/images/home-slider/skinnydip.jpg);">
				                
				                <p>Skinnydip London, Shopify Plus</p>
				              </div><!-- /.slide__image -->
				            </div><!-- /.slide -->


				            <div class="slide">
				              <div class="slide__image"
				                style="background-image: url(/css/images/home-slider/mavi.jpg);">
				                
				                <p>Mavi Jeans, Shopify Plus</p>
				              </div><!-- /.slide__image -->
				            </div><!-- /.slide -->            


				            <div class="slide">
				              <div class="slide__image"
				                style="background-image: url(/css/images/home-slider/drinkfinity.jpg);">
				                
				                <p>Drinkfinity, PepsiCo, Shopify Plus</p>
				              </div><!-- /.slide__image -->
				            </div><!-- /.slide -->


						</div><!-- /.slides -->
					</div><!-- /.slider -->
				</div><!-- /.intro__aside -->

				<div class="intro__content">
					<h1>Nobody does shopify better</h1>

					<p>We're the Shopify Plus agency for international brands.</p>

					<a href="/contact" class="btn btn--secondary">Get in touch</a>

					<a href="/shopify-services" class="logo">
						<img src="css/images/logos/shopify/shopify-plus-logo.png" alt="" />
					</a>
				</div><!-- /.intro__content -->
			</div><!-- /.intro__inner -->
		</div><!-- /.container -->
	</div><!-- /.intro -->

	<section class="section-articles">
		<div class="section__body">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<article class="article">
							<div class="article__inner">
								<div class="article__image">
									<a href="/shopify-plus/cluse" class="article__image-inner lazy"
										style="background-image: url(css/images/casestudies/cluse/_shopify-developers.jpg);">
									</a><!-- /.article__image-inner -->
								</div><!-- /.article__image -->

								<div class="article__content">
									<h4>Cluse</h4>

									<p>Multi-currency, modern commerce on Shopify Plus.</p>

									<a href="/shopify-plus/cluse" class="link-more link-more--md">Read more</a>
								</div><!-- /.article__content -->
							</div><!-- /.article__inner -->
						</article><!-- /.article -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<article class="article article--gutter-top-md">
							<div class="article__inner">
								<div class="article__image">
									<a href="/shopify-plus/bulletproof" class="article__image-inner lazy"
										style="background-image: url(css/images/casestudies/bulletproof/_shopify-developers.jpg);">
									</a><!-- /.article__image-inner -->
								</div><!-- /.article__image -->

								<div class="article__content">
									<h4>Bulletproof</h4>

									<p>From Magento to Shopify Plus in record time.</p>

									<a href="/shopify-plus/bulletproof" class="link-more link-more--md">Read more</a>
								</div><!-- /.article__content -->
							</div><!-- /.article__inner -->
						</article><!-- /.article -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.section__body -->
	</section><!-- /.section-articles -->

	<section class="section-services" style="background-color: #f2f6f9;">
		<div class="section__body">
			<div class="container">
				<div class="section__content">
					<h2>
						Shopify plus<br />

						at scale
					</h2>

					<p>We design, develop and optimize Shopify Plus websites for international brands.</p>

					<a href="/shopify-services" class="link-more link-more--arrow">Our services</a>
				</div><!-- /.section__content -->

				<div class="section__image">
					<figure>
						<img src="css/images/temp/mountain.jpg" class="lazy" alt="" />

						<figcaption>Finisterre, Shopify Plus</figcaption>
					</figure>

					<a href="/shopify-services" class="logo">
						<img src="css/images/temp/logo-shopify-black@2x.png" class="lazy" alt="" />
					</a>
				</div><!-- /.section__image -->
			</div><!-- /.container -->
		</div><!-- /.section__body -->
	</section><!-- /.section-services -->

	<section class="section-clients">
		<div class="container">
			<header class="section__head">
				<h3>Clients</h3>

				<p>World-class brands work with our London and New York teams to get the most out of Shopify Plus.</p>
			</header><!-- /.section__head -->

			<div class="section__body">
				<div class="section__desktop">
					<ul class="list-clients js-retina">
						<li>
	<a href="#" class="client-logo-hasbro">
		<img src="/css/images/logos/clients/logo-hasbro@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/drinkfinity" class="client-logo-pepsico">
		<img src="/css/images/logos/clients/logo-pepsico@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/skinnydip-london" class="client-logo-skinny-dip">
		<img src="/css/images/logos/clients/logo-skinny-dip@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/mavi" class="client-logo-mavi">
		<img src="/css/images/logos/clients/logo-mavi@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/finisterre" class="client-logo-finisterre">
		<img src="/css/images/logos/clients/logo-finisterre@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/emma-bridgewater" class="client-logo-ema">
		<img src="/css/images/logos/clients/logo-emma-bridgewater@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/hummingbird-bakery" class="client-logo-thb">
		<img src="/css/images/logos/clients/logo-the-hummingbird-bakery@2x.png" alt="" />
	</a>
</li>

<li>
	<img src="/css/images/logos/clients/logo-harper-collins@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-nicce-london@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-the-economist@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-penguin@2x.png" alt="" />
</li>

<li>
	<a href="/shopify-plus/cluse.html">
		<img src="/css/images/logos/clients/logo-cluse@2x.png" alt="" />
	</a>
</li>
					</ul><!-- /.list-clients -->
				</div><!-- /.section__desktop -->

				<div class="section__mobile">
					<div class="slider-clients">
						<div class="slides">
							<div class="slide">
								<ul class="list-clients js-retina">
									<li>
	<a href="#" class="client-logo-hasbro">
		<img src="/css/images/logos/clients/logo-hasbro@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/drinkfinity" class="client-logo-pepsico">
		<img src="/css/images/logos/clients/logo-pepsico@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/skinnydip-london" class="client-logo-skinny-dip">
		<img src="/css/images/logos/clients/logo-skinny-dip@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/mavi" class="client-logo-mavi">
		<img src="/css/images/logos/clients/logo-mavi@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/finisterre" class="client-logo-finisterre">
		<img src="/css/images/logos/clients/logo-finisterre@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/emma-bridgewater" class="client-logo-ema">
		<img src="/css/images/logos/clients/logo-emma-bridgewater@2x.png" alt="" />
	</a>
</li>

<li>
	<a href="/shopify-plus/hummingbird-bakery" class="client-logo-thb">
		<img src="/css/images/logos/clients/logo-the-hummingbird-bakery@2x.png" alt="" />
	</a>
</li>

<li>
	<img src="/css/images/logos/clients/logo-harper-collins@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-nicce-london@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-the-economist@2x.png" alt="" />
</li>

<li>
	<img src="/css/images/logos/clients/logo-penguin@2x.png" alt="" />
</li>

<li>
	<a href="/shopify-plus/cluse.html">
		<img src="/css/images/logos/clients/logo-cluse@2x.png" alt="" />
	</a>
</li>
								</ul><!-- /.list-clients -->
							</div><!-- /.slide -->
						</div><!-- /.slides -->
					</div><!-- /.slider-clients -->
				</div><!-- /.section__mobile -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-clients -->

	<section class="section section--why-us">
		<header class="section__head">
			<div class="container">
				<h3>Why us</h3>

				<p>Our team will make you look good. We deliver best-practice, brand-enhancing e-commerce websites that will change your business. You're in safe hands - we've completed more Shopify Plus builds than any other agency.</p>
			</div><!-- /.container -->
		</header><!-- /.section__head -->

		<div class="section__body">
			<div class="container container--md">
				<div class="row">
					<div class="col-md-6">
						<div class="list-results list-results--no-gutter">
							<ul>
								<li>
	<h2>150</h2>
	
	<strong>Shopify Plus store launches</strong>
</li>

<li>
	<h2>98%</h2>
	
	<strong>Client retention rate</strong>
</li>

<li>
	<h2 class="mobile-small">$500m</h2>
	
	<strong>Annual Client GMV</strong>
</li>


							</ul>
						</div><!-- /.list-results -->
					</div><!-- /.col-md-6 -->
					
					<div class="col-md-6">
						<div class="testimonial testimonial--alt">
							<blockquote class="testimonial__content">
								<p>WeMakeWebsites is leading the way with Shopify Plus. Their team takes world-class brands to new levels with stunning work that delivers great results.</p>
								
								<h6>Harley Finkelstein, COO, Shopify</h6>

								<a href="/contact" class="btn">
									<span>Enquire now</span>
									<i class="ico-arrow-right-white"></i>
								</a>
							</blockquote><!-- /.testimonial__content -->
						</div><!-- /.testimonial -->
					</div><!-- /.col-md-6 -->
				</div>
			</div><!-- /.container container-/-md -->
		</div><!-- /.section__body -->
	</section><!-- /.section -->

	<section class="section-articles-alt section--border-bottom">
		<div class="section__body">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<article class="article article--gutter-top">
							<div class="article__inner" data-aos="fade-up">
								<div class="article__image">
									<a href="/shopify-plus/skinnydip-london" class="article__image-inner lazy"
										style="background-image: url(css/images/casestudies/skinnydiplondon/_shopify-developers.jpg);">
									</a><!-- /.article__image-inner -->
								</div><!-- /.article__image -->

								<div class="article__content">
									<h4>Skinnydip</h4>

									<p>Our team of Shopify Plus experts increased revenues by 70% for Skinnydip.</p>

									<a href="/shopify-plus/skinnydip-london" class="link-more link-more--md">Read more</a>
								</div><!-- /.article__content -->
							</div><!-- /.article__inner -->
						</article><!-- /.article -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<article class="article">
							<div class="article__inner" data-aos="fade-up">
								<div class="article__image">
									<a href="/shopify-plus/emma-bridgewater" class="article__image-inner lazy"
										style="background-image: url(css/images/casestudies/emmabridgewater/_shopify-developers.jpg);">
									</a><!-- /.article__image-inner -->
								</div><!-- /.article__image -->

								<div class="article__content">
									<h4>Emma bridgewater</h4>

									<p>We replatformed this classic English brand from a legacy platform to a charismatic new website on Shopify Plus.</p>

									<a href="/shopify-plus/emma-bridgewater" class="link-more link-more--md">Read more</a>
								</div><!-- /.article__content -->
							</div><!-- /.article__inner -->
						</article><!-- /.article -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.section__body -->
	</section><!-- /.section-articles-alt section-/-border-bottom -->

	<section class="section-clients section-clients--alt">
		<div class="container">
			<header class="section__head">
				<h3>Our partners</h3>

				<p>We partner up with the best in the business to help our clients achieve stratospheric growth.</p>

				<a href="/shopify-services" class="link-more link-more--md link-more--arrow">View all</a>
			</header><!-- /.section__head -->

			<div class="section__body">
				<div class="section__desktop">
					<ul class="list-clients list-clients--secondary js-retina">
						<li>
							<a href="http://www.shopify.com/?ref=wemakewebsites">
								<img src="css/images/temp/logo-shopify-alt@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.nosto.com/">
								<img src="css/images/temp/logo-nosto@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.patchworks.co.uk/">
								<img src="css/images/temp/logo-patchworks@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://loyaltylion.com/">
								<img src="css/images/temp/logo-loyalty-lion@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.yotpo.com/">
								<img src="css/images/temp/logo-yot-po@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="http://boldcommerce.com/">
								<img src="css/images/temp/logo-bold@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.brightpearl.com/">
								<img src="css/images/temp/logo-bright-pearl@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.dotmailer.com/">
								<img src="css/images/temp/logo-dot-mailer@2x.png" alt="" />
							</a>
						</li>
						
						<li>
							<a href="https://www.klevu.com/">
								<img src="css/images/temp/logo-klevu@2x.png" alt="" />
							</a>
						</li>
					</ul><!-- /.list-clients -->
				</div><!-- /.section__desktop -->

				<div class="section__mobile">
					<div class="slider-clients">
						<div class="slides">
							<div class="slide">
								<ul class="list-clients js-retina">
									<li class="width-118">
										<a href="#">
											<img src="css/images/temp/logo-shopify-alt@2x.png" alt="" />
										</a>
									</li>
									
									<li class="width-136 top--7 left--8">
										<a href="#">
											<img src="css/images/temp/logo-nosto@2x.png" alt="" />
										</a>
									</li>
									
									<li class="width-130 top-14">
										<a href="#">
											<img src="css/images/temp/logo-patchworks@2x.png" alt="" />
										</a>
									</li>
									
									<li class="width-77 top-7 left-6">
										<a href="#">
											<img src="css/images/temp/logo-loyalty-lion@2x.png" alt="" />
										</a>
									</li>
								</ul><!-- /.list-clients js-retina -->
							</div><!-- /.slide -->

							<div class="slide">
								<ul class="list-clients js-retina">
									<li>
										<a href="#">
											<img src="css/images/temp/logo-yot-po@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-bold@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-bright-pearl@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-dot-mailer@2x.png" alt="" />
										</a>
									</li>
								</ul><!-- /.list-clients js-retina -->
							</div><!-- /.slide -->

							<div class="slide">
								<ul class="list-clients js-retina">
									<li>
										<a href="#">
											<img src="css/images/temp/logo-klevu@2x.png" alt="" />
										</a>
									</li>

									<li>
										<a href="#">
											<img src="css/images/temp/logo-oracle-bronto@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-optimizely@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-ometria@2x.png" alt="" />
										</a>
									</li>
								</ul><!-- /.list-clients js-retina -->
							</div><!-- /.slide -->

							<div class="slide">
								<ul class="list-clients js-retina">
									<li>
										<a href="#">
											<img src="css/images/temp/logo-netsuite@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-klama@2x.png" alt="" />
										</a>
									</li>
									
									<li>
										<a href="#">
											<img src="css/images/temp/logo-vervaunt@2x.png" alt="" />
										</a>
									</li>
								</ul><!-- /.list-clients js-retina -->
							</div><!-- /.slide -->
						</div><!-- /.slides -->
					</div><!-- /.slider-clients -->
				</div><!-- /.section__mobile -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-clients -->


	<div class="callout" style="background-color: #f2f6f9;">
	<div class="container">
		<div class="callout__content">
			<h3>How can we help?</h3>

			<p>Get in touch to discuss your Shopify Plus ambitions and we’ll help you get there.</p>

			<a href="/enquiry" class="btn">
				<span>Get in touch</span>

				<i class="ico-arrow-right-white"></i>
			</a>
		</div><!-- /.callout__content -->


		<div class="callout__image callout__image--no-shadow">
			<img src="/css/images/temp/pre-footer-image.png" class="lazy callout__img-desktop" alt="Finisterre pages" />
			<img src="/css/images/temp/pre-footer-mobile.png" class="lazy callout__img-mobile" alt="Finisterre pages" />
		</div><!-- /.callout__image -->
	</div><!-- /.container -->
</div><!-- /.callout -->

	
</div><!-- /.main -->


	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="footer__nav">
							<ul>
	<li>
		<a href="/shopify-plus-portfolio">Work</a>
	</li>
		
	<li>
		<a href="/shopify-services">Services</a>
	</li>

	<li>
		<a href="/shopify-developer">Why us</a>
	</li>
	
	<li>
		<a href="/blog/">Blog</a>
	</li>
	
	<li>
		<a href="/shopify-careers">Careers</a>
	</li>
	
	<li>
		<a href="/contact">Contact</a>
	</li>
</ul>
							<div class="footer__social-icons">
									<ul>
										<li>
											<a href="https://www.instagram.com/wemakewebsites_/"  target="_blank">
												<svg width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													<g id="Instagram" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<g id="Instagram-Copy">
																	<path d="M11.3304074,2.00588889 C13.8643704,2.00588889 14.1821111,2.01662963 15.1772963,2.06203704 C16.1704074,2.10733333 16.8486667,2.26507407 17.4421481,2.49574074 C18.0557037,2.73414815 18.576037,3.05318519 19.0947778,3.57188889 C19.6134815,4.09062963 19.9325185,4.61096296 20.170963,5.22451852 C20.4015926,5.818 20.5593333,6.49625926 20.6046296,7.48937037 C20.650037,8.48455556 20.6607778,8.8022963 20.6607778,11.3362963 C20.6607778,13.8702593 20.650037,14.188 20.6046296,15.1831852 C20.5593333,16.1762963 20.4015926,16.8545556 20.170963,17.448037 C19.9325185,18.0615926 19.6134815,18.5819259 19.0947778,19.1006667 C18.576037,19.6193704 18.0557037,19.9384074 17.4421481,20.1768519 C16.8486667,20.4074815 16.1704074,20.5652222 15.1772963,20.6105185 C14.1821111,20.6559259 13.8643704,20.6666667 11.3304074,20.6666667 C8.79640741,20.6666667 8.47866667,20.6559259 7.48348148,20.6105185 C6.49037037,20.5652222 5.81211111,20.4074815 5.21862963,20.1768519 C4.60507407,19.9384074 4.08474074,19.6193704 3.566,19.1006667 C3.0472963,18.5819259 2.72825926,18.0615926 2.48985185,17.448037 C2.25918519,16.8545556 2.10144444,16.1762963 2.05614815,15.1831852 C2.01074074,14.188 2,13.8702593 2,11.3362963 C2,8.8022963 2.01074074,8.48455556 2.05614815,7.48937037 C2.10144444,6.49625926 2.25918519,5.818 2.48985185,5.22451852 C2.72825926,4.61096296 3.0472963,4.09062963 3.566,3.57188889 C4.08474074,3.05318519 4.60507407,2.73414815 5.21862963,2.49574074 C5.81211111,2.26507407 6.49037037,2.10733333 7.48348148,2.06203704 C8.47866667,2.01662963 8.79640741,2.00588889 11.3304074,2.00588889 Z M11.3304074,3.68703704 C8.83907407,3.68703704 8.54396296,3.69655556 7.56011111,3.74144444 C6.65040741,3.78292593 6.15637037,3.93492593 5.82759259,4.0627037 C5.39207407,4.23196296 5.08125926,4.43414815 4.75477778,4.76066667 C4.42825926,5.08714815 4.22607407,5.39796296 4.05681481,5.83348148 C3.92903704,6.16225926 3.77703704,6.6562963 3.73555556,7.566 C3.69066667,8.54985185 3.68114815,8.84496296 3.68114815,11.3362963 C3.68114815,13.8275926 3.69066667,14.1227037 3.73555556,15.1065556 C3.77703704,16.0162593 3.92903704,16.5102963 4.05681481,16.8390741 C4.22607407,17.2745926 4.4282963,17.5854074 4.75477778,17.9118889 C5.08125926,18.2384074 5.39207407,18.4405926 5.82759259,18.6098519 C6.15637037,18.7376296 6.65040741,18.8896296 7.56011111,18.9311111 C8.54385185,18.976 8.83888889,18.9855185 11.3304074,18.9855185 C13.8218889,18.9855185 14.116963,18.976 15.1006667,18.9311111 C16.0103704,18.8896296 16.5044074,18.7376296 16.8331852,18.6098519 C17.2687037,18.4405926 17.5795185,18.2384074 17.906,17.9118889 C18.2325185,17.5854074 18.4347037,17.2745926 18.603963,16.8390741 C18.7317407,16.5102963 18.8837407,16.0162593 18.9252222,15.1065556 C18.9701111,14.1227037 18.9796296,13.8275926 18.9796296,11.3362963 C18.9796296,8.84496296 18.9701111,8.54985185 18.9252222,7.566 C18.8837407,6.6562963 18.7317407,6.16225926 18.603963,5.83348148 C18.4347037,5.39796296 18.2325185,5.08714815 17.906,4.76066667 C17.5795185,4.43414815 17.2687037,4.23196296 16.8331852,4.0627037 C16.5044074,3.93492593 16.0103704,3.78292593 15.1006667,3.74144444 C14.1168148,3.69655556 13.8217037,3.68703704 11.3304074,3.68703704 Z M11.3304074,14.4464074 C13.0480741,14.4464074 14.4405185,13.053963 14.4405185,11.3362963 C14.4405185,9.61859259 13.0480741,8.22614815 11.3304074,8.22614815 C9.6127037,8.22614815 8.22025926,9.61859259 8.22025926,11.3362963 C8.22025926,13.053963 9.6127037,14.4464074 11.3304074,14.4464074 Z M11.3304074,6.545 C13.9765556,6.545 16.1216667,8.69011111 16.1216667,11.3362963 C16.1216667,13.9824444 13.9765556,16.1275556 11.3304074,16.1275556 C8.68422222,16.1275556 6.53911111,13.9824444 6.53911111,11.3362963 C6.53911111,8.69011111 8.68422222,6.545 11.3304074,6.545 Z M17.4306296,6.3557037 C17.4306296,6.97407407 16.9293333,7.47533333 16.310963,7.47533333 C15.6926296,7.47533333 15.1913333,6.97407407 15.1913333,6.3557037 C15.1913333,5.73733333 15.6926296,5.23603704 16.310963,5.23603704 C16.9293333,5.23603704 17.4306296,5.73733333 17.4306296,6.3557037 Z" class="Combined-Shape" fill="#FFFFFF"></path>
																	<rect class="Rectangle" fill="#D8D8D8" opacity="0" x="0" y="0" width="23" height="23"></rect>
															</g>
													</g>
												</svg>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/wemakewebsites_" target="_blank">
												<svg width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													<g id="Twitter" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<g id="Twitter-Copy">
																	<rect class="Rectangle" x="0" y="0" width="23" height="23"></rect>
																	<path d="M7.62033898,20.0402542 C15.5131356,20.0402542 19.8288136,13.5 19.8288136,7.83177966 C19.8288136,7.64491525 19.8288136,7.45805085 19.8199153,7.28008475 C20.6563559,6.675 21.3860169,5.91864407 21.9644068,5.05550847 C21.1991525,5.39364407 20.3716102,5.625 19.4995763,5.73177966 C20.3894068,5.19788136 21.065678,4.36144068 21.3860169,3.3559322 C20.5584746,3.84533898 19.6419492,4.20127119 18.6631356,4.3970339 C17.8800847,3.56059322 16.7677966,3.04449153 15.5309322,3.04449153 C13.1639831,3.04449153 11.2419492,4.96652542 11.2419492,7.33347458 C11.2419492,7.67161017 11.2775424,8.00084746 11.3576271,8.31228814 C7.78940678,8.13432203 4.63050847,6.42584746 2.51271186,3.82754237 C2.14788136,4.45932203 1.93432203,5.19788136 1.93432203,5.9809322 C1.93432203,7.46694915 2.69067797,8.78389831 3.84745763,9.54915254 C3.14449153,9.53135593 2.48601695,9.33559322 1.90762712,9.01525424 C1.90762712,9.03305085 1.90762712,9.05084746 1.90762712,9.06864407 C1.90762712,11.1508475 3.38474576,12.8771186 5.35127119,13.2775424 C4.99533898,13.3754237 4.61271186,13.4288136 4.22118644,13.4288136 C3.94533898,13.4288136 3.67838983,13.4021186 3.41144068,13.3487288 C3.95423729,15.0572034 5.53813559,16.2940678 7.41567797,16.329661 C5.94745763,17.4775424 4.09661017,18.1627119 2.08559322,18.1627119 C1.73855932,18.1627119 1.40042373,18.1449153 1.06228814,18.1004237 C2.93983051,19.3283898 5.2,20.0402542 7.62033898,20.0402542" id="Path" fill="#FFFFFF" fill-rule="nonzero"></path>
															</g>
													</g>
												</svg>
											</a>
										</li>
										<li>
											<a href="https://www.linkedin.com/company/we-make-websites" target="_blank">
												<svg width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
														<g id="Linkedin" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<path d="M4,2 L19,2 C20.1045695,2 21,2.8954305 21,4 L21,19 C21,20.1045695 20.1045695,21 19,21 L4,21 C2.8954305,21 2,20.1045695 2,19 L2,4 C2,2.8954305 2.8954305,2 4,2 Z M4.26564479,8.63026112 L4.26564479,18.2030895 L7.44814995,18.2030895 L7.44814995,8.63026112 L4.26564479,8.63026112 Z M5.87840468,4.01432032 C4.7895467,4.01432032 4.07813334,4.73017352 4.07813334,5.66842268 C4.07813334,6.586952 4.76792407,7.32252504 5.83625497,7.32252504 L5.85683971,7.32252504 C6.96651308,7.32252504 7.65722637,6.586952 7.65722637,5.66842268 C7.63664163,4.73017352 6.96657074,4.01432032 5.87840468,4.01432032 Z M15.2584169,8.4051557 C13.5702949,8.4051557 12.8141371,9.33354494 12.3907372,9.9856258 L12.3907372,8.6302617 L9.20903927,8.6302617 C9.25124665,9.52820627 9.20903927,18.2030901 9.20903927,18.2030901 L12.3906795,18.2030901 L12.3906795,12.8569381 C12.3906795,12.5707698 12.4112643,12.2847168 12.4956214,12.0801955 C12.7252249,11.5087239 13.2491268,10.9167251 14.1281588,10.9167251 C15.2790016,10.9167251 15.7400537,11.7948922 15.7400537,13.0812368 L15.7400537,18.2030901 L18.921867,18.2030901 L18.921867,12.7134791 C18.921867,9.7730321 17.3519487,8.4051557 15.2584169,8.4051557 Z" class="Combined-Shape" fill="#FFFFFF" fill-rule="nonzero"></path>
														</g>
												</svg>
											</a>						
										</li>
									</ul>
							</div>
					</nav><!-- /.footer__nav -->
				</div>
			</div>
			<div class="footer__content">
				<div class="row">
					<div class="col-md-3">
						<ul class="list-contact list-contact--alt">
							<li>
								<h5>New York</h5>
								
								<p>419 Park Ave South, 3rd Floor,<br> 
										New York,<br> 
										NY 10016
								</p>

								<a href="tel:0016469063963">+ 1 646-906-3963</a>

								<br />

							</li>
						</ul><!-- /.list-contact -->
					</div><!-- /.col-md-3 -->
					<div class="col-md-3">
						<ul class="list-contact list-contact--alt">
							<li>
								<h5>London</h5>
								
								<p>6-7 St Cross Street,<br> 
										London,<br> 
										EC1N 8UA
								</p>

								<a href="tel:004402036967828">+ 44 (0)20 3696 7828</a>

								<br />

							</li>
						</ul><!-- /.list-contact -->
					</div><!-- /.col-md-3 -->
					<div class="col-md-6">
							<ul class="list-contact list-contact--alt">
									<li>
										<h5>Join Our Newsletter</h5>
										<p>Hear about our insights, events and collaborations.</p>
										<div class="subscribe-alt">
												<div class="subscribe-alt">
  <form name="signup" id="signup" class="footer-newsletter__form" action="https://email.wemakewebsites.com/signup.ashx" method="post" autocomplete="off" onsubmit="return validate_signup(this, true)">
    <input type="hidden" name="ci_isconsentform" value="true">
    <input type="hidden" name="userid" value="208589">
    <input type="hidden" name="SIGc641d8f2282904154ff4c78f034e7d0fef5b8d127c22ea1d31d0eafd11948274" value="">
    <input type="hidden" name="addressbookid" value="6832152" />
    <input type="hidden" name="ReturnURL" value="">
    <input type="hidden" name="ci_userConsentText" value="By providing my email I agree for We Make Websites to contact me via email with e-commerce advice, events and Shopify launches. Your data is stored securely and we never pass it on to third parties.">
    <input type="hidden" id="ci_consenturl" name="ci_consenturl" value="">
    <div class="subscribe__container">
      <input type="text" name="email" id="email" class="subscribe__field" required aria-required="true" placeholder="Email address">
      <input type="submit" id="btnsubmit" class="btn subscribe__btn" name="btnsubmit" value="Subscribe">
    </div>
    <div class="subscribe__consent"> 
      <label>By providing my email I agree for We Make Websites to contact me via email with e-commerce advice, events and Shopify launches. Your data is stored securely and we never pass it on to third parties.</label>
    </div>
  </form>
</div>
<script type="text/javascript">
  <!--
  var urlInput = document.getElementById("ci_consenturl");
  if (urlInput != null && urlInput != 'undefined') {
      urlInput.value = encodeURI(window.location.href);
  }
  function checkbox_Clicked(element) {
      document.getElementById(element.id + "_unchecked").value = !element.checked;
  }
  function validate_signup(frm, showAlert) {
      var emailAddress = frm.email.value;
      var errorString = '';
      if (emailAddress == '' || emailAddress.indexOf('@') == -1) {
          errorString = 'Please enter your email address';
      }
      var isError = false;
      if (errorString.length > 0) {
          isError = true;
          if (showAlert) alert(errorString);
      }
      return !isError;
  }
  //-->
</script>

										</div><!-- /.subscribe -->
									</li>
							</ul>	
					</div>
				</div><!-- /.row -->
			</div><!-- /.footer__content -->

			<div class="footer__bar">
				<a href="/shopify-services" class="logo">
					<img src="/css/images/logos/shopify/shopify-plus-logo--white-gold.png" alt="" />
				</a>

				<div class="copyrights">
					<p>&copy; 2019 We Make Websites Ltd.</p>
				</div><!-- /.copyrights -->

				<ul class="list-nav">
					<li>
						<a href="/privacy">Privacy policy</a>
					</li>
					
				</ul><!-- /.list-nav -->
			</div><!-- /.footer__bar -->
		</div><!-- /.container -->
	</footer><!-- /.footer -->
</div><!-- /.wrapper -->






<!--[if lt IE 10]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Vendor JS -->
<script src="/vendor/jquery-1.12.4.min.js"></script>
<script src="/vendor/bootstrap-4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/vendor/slick-1.8.0/slick/slick.min.js"></script>
<script type="text/javascript" src="/vendor/aos/aos.js"></script>
<script type="text/javascript" src="/vendor/jquery-cookie/js.cookie.js"></script>
<script type="text/javascript" src="/vendor/lazyload/lazyload.min.js"></script>




<!-- App JS -->
<script src="/js/functions.js"></script>




<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5BJZJ4B');</script>
<!-- End Google Tag Manager -->    

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BJZJ4B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



<!-- DotDigital Tracking -->
<script>
  var userEmail;

  document.addEventListener("DOMContentLoaded", function() {
    var submitForm = document.querySelector('#signup');
    submitForm.addEventListener('submit', function(event) {
      
      var formData = new URLSearchParams(Array.from(new FormData(this))).toString();
      userEmail = formData.split('&email=').pop().split('&').shift().replace('%40', '@');


      var dm_insight_id ='DM-2090317564-01';
      (function(w,d,u,t,o,c){w['dmtrackingobjectname']=o;c=d.createElement(t);c.async=1;c.src=u;t=d.getElementsByTagName
      (t)[0];t.parentNode.insertBefore(c,t);w[o]=w[o]||function(){(w[o].q=w[o].q||[]).push(arguments);};w[o]('track');
      })(window, document, '//static.trackedweb.net/js/_dmptv4.js', 'script', 'dmPt');

      window.dmPt("identify", userEmail);  // inject contact email address.
    }); 

  });
</script>

<script type="application/ld+json">
{
   "@context":{
      "@vocab":"http://schema.org/"
   },
   "@graph":[
      {
         "@id":"#MainOrganization",
         "@type":"Organization",
         "name":"We Make Websites",
         "url":"wemakewebsites.com",
         "logo":"https://wemakewebsites.com/sites/all/themes/wmw/images/logo/shopify-plus-developers@2x.png",
         "sameAs":[
            "https://twitter.com/wemakewebsites_",
            "https://www.instagram.com/wemakewebsites_/",
            "https://www.facebook.com/wemakewebsites/?ref=aymt_homepage_panel",
            "https://www.linkedin.com/company/we-make-websites/"
         ]
      },
      {
         "@type":"LocalBusiness",
         "parentOrganization":{
            "@id":"#MainOrganization"
         },
         "name":"We Make Websites",
         "address":{
            "@type":"PostalAddress",
            "streetAddress":"Sweeps Building, 6-7 St Cross Street",
            "addressLocality":"London",
            "addressRegion":"London",
            "postalCode":"EC1N8UA",
            "telephone":"02036967828"
         },
         "openingHours":[
            "9:30pm - 6:00pm"
         ],
         "image":"https://wemakewebsites.com/sites/all/themes/wmw/images/people-laughing-work.jpg",
         "description":"",
         "hasmap":"https://www.google.co.uk/maps/place/St+Cross+St,+London+EC1N+8UA,+UK/@51.5205221,-0.1176543,15z/data=!4m5!3m4!1s0x48761b4e816e99f9:0x220adf210e757582!8m2!3d51.520522!4d-0.1088996",
      "latitude":51.52038,
      "longitude":-0.1176543
      },
      {
         "@type":"LocalBusiness",
         "parentOrganization":{
            "@id":"#MainOrganization"
         },
         "name":"We Make Websites",
         "address":{
            "@type":"PostalAddress",
            "streetAddress":"419 Park Ave South",
            "addressLocality":"New York",
            "addressRegion":"NY",
            "postalCode":"10016",
            "telephone":"+1 646-906-3963"

         },
         "openingHours":[
            "9:30pm - 6:00pm"
         ],
         "image":"https://wemakewebsites.com/sites/all/themes/wmw/images/people-laughing-work.jpg",
         "description":"",
         "hasmap":"https://www.google.com/maps/place/419+Park+Ave+S+3rd+Floor,+New+York,+NY+10016/@40.7436609,-73.9920633,15z/data=!4m5!3m4!1s0x89c259a7893100e7:0x42b1a36e6048eea5!8m2!3d40.7436608!4d-73.9833086",
      "latitude":40.743705, 
      "longitude":-73.983598      
      }
   ]
}
</script>

</body>
</html>
