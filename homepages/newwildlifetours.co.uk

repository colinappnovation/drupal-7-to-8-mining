<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="MobileOptimized" content="width" />
<meta name="description" content="New Wildlife Tours offer guided day trips for birdwatching and to see butterflies &amp; wildflowers across the South West of England" />
<meta name="HandheldFriendly" content="1" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.newwildlifetours.co.uk/" />
<link rel="shortlink" href="https://www.newwildlifetours.co.uk/" />
<title>Guided Nature Day Trips in Wiltshire & South West England, UK | New Wildlife Tours</title>
<style>
@import url("https://www.newwildlifetours.co.uk/modules/system/system.base.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/system/system.menus.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/system/system.messages.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/system/system.theme.css?oonzcf");
</style>
<style media="screen">
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/views_slideshow/views_slideshow.css?oonzcf");
</style>
<style>
@import url("https://www.newwildlifetours.co.uk/modules/comment/comment.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/date/date_api/date.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/field/theme/field.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/node/node.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/search/search.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/modules/user/user.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/views/css/views.css?oonzcf");
</style>
<style>
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/ctools/css/ctools.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/views_slideshow/views_slideshow_controls_text.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/modules/views_slideshow/contrib/views_slideshow_cycle/views_slideshow_cycle.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/css/superfish.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/css/superfish-smallscreen.css?oonzcf");
</style>
<style media="screen">
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/at_core/css/at.layout.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/global.base.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/global.styles.css?oonzcf");
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/theme.css?oonzcf");
</style>
<style media="print">
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/print.css?oonzcf");
</style>
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/default/files/adaptivetheme/nwt_files/nwt.responsive.layout.css?oonzcf" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.custom.css?oonzcf" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.smalltouch.portrait.css?oonzcf" media="only screen and (max-width:1px)" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.smalltouch.landscape.css?oonzcf" media="only screen and (min-width:2px) and (max-width:767px)" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.tablet.portrait.css?oonzcf" media="only screen and (min-width:1px) and (max-width:1px)" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.tablet.landscape.css?oonzcf" media="only screen and (min-width:768px) and (max-width:1099px)" />
<link type="text/css" rel="stylesheet" href="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/responsive.desktop.css?oonzcf" media="only screen and (min-width:1100px)" />

<!--[if (lt IE 9)&(!IEMobile 7)]>
<style media="screen">
@import url("https://www.newwildlifetours.co.uk/sites/all/themes/nwt/css/lt-ie9.css?oonzcf");
</style>
<![endif]-->
<style type="text/css">
	.sold-out,.sold-out1,.sold-out2,.sold-out3,.sold-out4,.book-btn5 {display:none !important;}
	.waiting-list-contact,.waiting-list-contact1,.waiting-list-contact2,.waiting-list-contact3,.waiting-list-contact4 {display:none !important;}
</style>
<script src="https://www.newwildlifetours.co.uk/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script src="https://www.newwildlifetours.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.newwildlifetours.co.uk/misc/drupal.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/modules/views_slideshow/js/views_slideshow.js?v=1.0"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/jquery.cycle/jquery.cycle.all.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/modules/views_slideshow/contrib/views_slideshow_cycle/js/views_slideshow_cycle.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/sfsmallscreen.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/supposition.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/superfish.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/libraries/superfish/supersubs.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/modules/superfish/superfish.js?oonzcf"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"nwt","theme_token":"eHY4L3OFOvV1gH0X1tOFnasMRyzMlEoqGGON4phs1ag","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.js":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow_controls_text.css":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/css\/superfish-smallscreen.css":1,"sites\/all\/themes\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/nwt\/css\/global.base.css":1,"sites\/all\/themes\/nwt\/css\/global.styles.css":1,"sites\/all\/themes\/nwt\/css\/theme.css":1,"sites\/all\/themes\/nwt\/css\/print.css":1,"public:\/\/adaptivetheme\/nwt_files\/nwt.responsive.layout.css":1,"sites\/all\/themes\/nwt\/css\/responsive.custom.css":1,"sites\/all\/themes\/nwt\/css\/responsive.smalltouch.portrait.css":1,"sites\/all\/themes\/nwt\/css\/responsive.smalltouch.landscape.css":1,"sites\/all\/themes\/nwt\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/nwt\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/nwt\/css\/responsive.desktop.css":1,"sites\/all\/themes\/nwt\/css\/lt-ie9.css":1}},"viewsSlideshow":{"home_slider-block":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowControls":{"home_slider-block":{"top":{"type":"viewsSlideshowControlsText"}}},"viewsSlideshowPager":{"home_slider-block":{"bottom":{"type":"viewsSlideshowPagerFields"}}},"viewsSlideshowPagerFields":{"home_slider-block":{"bottom":{"activatePauseOnHover":0}}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_home_slider-block":{"num_divs":5,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"home_slider-block","effect":"fade","transition_advanced":1,"timeout":6000,"speed":3000,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"action_advanced":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":3000,"cleartype":0,"cleartypenobg":0,"advanced_options":"{}"}},"urlIsAjaxTrusted":{"\/search\/node":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":500,"autoArrows":false,"dropShadows":false},"plugins":{"smallscreen":{"mode":"window_width","breakpointUnit":"px","accordionButton":"2","title":"MENU"},"supposition":true,"supersubs":{"minWidth":"6","maxWidth":"30"}}}},"adaptivetheme":{"nwt":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-stack","smalltouch_landscape":"one-col-stack","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1100px)","tablet_landscape":"only screen and (min-width:768px) and (max-width:1099px)","tablet_portrait":"only screen and (min-width:1px) and (max-width:1px)","smalltouch_landscape":"only screen and (min-width:2px) and (max-width:767px)","smalltouch_portrait":"only screen and (max-width:1px)"}}}});</script>
<script src="/sites/all/themes/nwt/scripts/scripts.js"></script>
<script src="https://use.typekit.net/jir2lsf.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<!--[if lt IE 9]>
<script src="https://www.newwildlifetours.co.uk/sites/all/themes/at_core/scripts/html5.js?oonzcf"></script>
<script src="https://www.newwildlifetours.co.uk/sites/all/themes/at_core/scripts/respond.js?oonzcf"></script>
<![endif]-->
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<meta property="og:type" content="business.business">
<meta property="og:title" content="New Wildlife Tours">
<meta property="og:url" content="http://www.newwildlifetours.co.uk/">
<meta property="og:image" content="http://www.newwildlifetours.co.uk/sites/all/themes/nwt/images/logo-ogfb.png">
<meta property="business:contact_data:street_address" content="1 Wessex Way">
<meta property="business:contact_data:locality" content="Highworth">
<meta property="business:contact_data:region" content="Wiltshire">
<meta property="business:contact_data:postal_code" content="SN6 7NT">
<meta property="business:contact_data:country_name" content="UK">
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-12 node-type-page site-name-hidden atr-7.x-3.x atv-7.x-3.2 site-name-new-wildlife-tours">
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-89377940-1', 'auto');
			ga('send', 'pageview');
		</script>
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page">

    <div id="header-container" class="container">

    <!-- !Leaderboard Region -->
    
    <header id="header" class="clearfix" role="banner">

      <!-- !Navigation -->
      <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1" >  
      <h2 class="element-invisible block-title">Main menu</h2>
  
  <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-7 sf-parent-items-1 sf-single-items-6"><li id="menu-219-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-394-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/about" class="sf-depth-1">About</a></li><li id="menu-395-1" class="middle odd sf-item-3 sf-depth-1 sf-total-children-12 sf-parent-children-0 sf-single-children-12 menuparent"><a href="/tours" title="" class="sf-depth-1 menuparent">Trips</a><ul><li id="menu-400-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/tours/January-01--9999" title="" class="sf-depth-2">January</a></li><li id="menu-401-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/tours/February-01--9999" title="" class="sf-depth-2">February</a></li><li id="menu-402-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/tours/March-01--9999" title="" class="sf-depth-2">March</a></li><li id="menu-408-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/tours/April-01--9999" title="" class="sf-depth-2">April</a></li><li id="menu-409-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/tours/May-01--9999" title="" class="sf-depth-2">May</a></li><li id="menu-410-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="/tours/June-01--9999" title="" class="sf-depth-2">June</a></li><li id="menu-411-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="/tours/July-01--9999" title="" class="sf-depth-2">July</a></li><li id="menu-412-1" class="middle even sf-item-8 sf-depth-2 sf-no-children"><a href="/tours/August-01--9999" title="" class="sf-depth-2">August</a></li><li id="menu-413-1" class="middle odd sf-item-9 sf-depth-2 sf-no-children"><a href="/tours/September-01--9999" title="" class="sf-depth-2">September</a></li><li id="menu-414-1" class="middle even sf-item-10 sf-depth-2 sf-no-children"><a href="/tours/October-01--9999" title="" class="sf-depth-2">October</a></li><li id="menu-415-1" class="middle odd sf-item-11 sf-depth-2 sf-no-children"><a href="/tours/November-01--9999" title="" class="sf-depth-2">November</a></li><li id="menu-416-1" class="last even sf-item-12 sf-depth-2 sf-no-children"><a href="/tours/December-01--9999" title="" class="sf-depth-2">December</a></li></ul></li><li id="menu-396-1" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="/booking" class="sf-depth-1">Booking</a></li><li id="menu-397-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/frequently-asked-questions" class="sf-depth-1">FAQs</a></li><li id="menu-500-1" class="middle even sf-item-6 sf-depth-1 sf-no-children"><a href="/news" title="" class="sf-depth-1">News</a></li><li id="menu-502-1" class="last odd sf-item-7 sf-depth-1 sf-no-children"><a href="/contact-us" class="sf-depth-1">Contact</a></li></ul>
  </nav></div>            
              <!-- !Branding -->
        <div id="branding" class="branding-elements clearfix">

                      <div id="logo">
              <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="https://www.newwildlifetours.co.uk/sites/all/themes/nwt/logo.png" alt="New Wildlife Tours" /></a>            </div>
          
                      <!-- !Site name and Slogan -->
            <div class="element-invisible h-group" id="name-and-slogan">

                              <h1 class="element-invisible" id="site-name"><a href="/" title="Home page" class="active">New Wildlife Tours</a></h1>
              
              
            </div>
          
        </div>
      
      <!-- !Header Region -->
      
    </header>

    </div>

    <div id="carousel-wrapper">
      <div id="carousel">
        <div id="slides">
          <div class="region region-banner"><div class="region-inner clearfix"><div id="block-views-home-slider-block" class="block block-views no-title odd first last block-count-2 block-region-banner block-home-slider-block" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><div class="view view-home-slider view-id-home_slider view-display-id-block view-dom-id-9bad39b722d2c87197f8d51595fb9212">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
          <div class="views-slideshow-controls-top clearfix">
        <div id="views_slideshow_controls_text_home_slider-block" class="views-slideshow-controls-text views_slideshow_controls_text">
  <span id="views_slideshow_controls_text_previous_home_slider-block" class="views-slideshow-controls-text-previous views_slideshow_controls_text_previous"><a href="#">Previous</a></span>
  <span id="views_slideshow_controls_text_pause_home_slider-block" class="views-slideshow-controls-text-pause views_slideshow_controls_text_pause  views-slideshow-controls-text-status-play"><a href="#">Pause</a></span>
  <span id="views_slideshow_controls_text_next_home_slider-block" class="views-slideshow-controls-text-next views_slideshow_controls_text_next"><a href="#">Next</a></span>
</div>
      </div>
    
    <div id="views_slideshow_cycle_main_home_slider-block" class="views_slideshow_cycle_main views_slideshow_main"><div id="views_slideshow_cycle_teaser_section_home_slider-block" class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
  <div id="views_slideshow_cycle_div_home_slider-block_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
    
  <div class="views-field views-field-title">        <span class="field-content"><a href="" class="tk-myriad-pro"><b>Bluebells and woodland, early evening, Oxon</b></a></span>  </div>  
  <div class="views-field views-field-field-background-image">        <div class="field-content"><img typeof="foaf:Image" class="image-style-none" src="https://www.newwildlifetours.co.uk/sites/default/files/slide-backgrounds/bluebells-and-woodland-early-evening-oxon.jpg" width="5280" height="850" alt="" /></div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_home_slider-block_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-1 views-row-even">
    
  <div class="views-field views-field-title">        <span class="field-content"><a href="" class="tk-myriad-pro"><b>Avebury World Heritage site, Wiltshire</b></a></span>  </div>  
  <div class="views-field views-field-field-background-image">        <div class="field-content"><img typeof="foaf:Image" class="image-style-none" src="https://www.newwildlifetours.co.uk/sites/default/files/slide-backgrounds/avebury-world-heritage-site-wiltshire.jpg" width="5280" height="850" alt="" /></div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_home_slider-block_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-2 views-row-odd">
    
  <div class="views-field views-field-title">        <span class="field-content"><a href="" class="tk-myriad-pro"><b>Snake&#039;s head fritillary, early spring</b></a></span>  </div>  
  <div class="views-field views-field-field-background-image">        <div class="field-content"><img typeof="foaf:Image" class="image-style-none" src="https://www.newwildlifetours.co.uk/sites/default/files/slide-backgrounds/snakes-head-fritillary-early-spring.jpg" width="5280" height="850" alt="" /></div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_home_slider-block_3" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-3 views-row-even">
    
  <div class="views-field views-field-title">        <span class="field-content"><a href="" class="tk-myriad-pro"><b>Reed bed, early morning</b></a></span>  </div>  
  <div class="views-field views-field-field-background-image">        <div class="field-content"><img typeof="foaf:Image" class="image-style-none" src="https://www.newwildlifetours.co.uk/sites/default/files/slide-backgrounds/reed-bed-early-morning.jpg" width="5280" height="850" alt="" /></div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_home_slider-block_4" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-5 views_slideshow_cycle_hidden views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-4 views-row-odd views-row-last">
    
  <div class="views-field views-field-title">        <span class="field-content"><a href="" class="tk-myriad-pro"><b>Lulworth Cove, Dorset</b></a></span>  </div>  
  <div class="views-field views-field-field-background-image">        <div class="field-content"><img typeof="foaf:Image" class="image-style-none" src="https://www.newwildlifetours.co.uk/sites/default/files/slide-backgrounds/lulworth-cove-dorset.jpg" width="5280" height="850" alt="" /></div>  </div></div>
</div>
</div>
</div>
          <div class="views-slideshow-controls-bottom clearfix">
        <div id="widget_pager_bottom_home_slider-block" class="views-slideshow-pager-fields widget_pager widget_pager_bottom views_slideshow_pager_field">
  <div id="views_slideshow_pager_field_item_bottom_home_slider-block_0" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd views-row-first">
  <div class="views-field-nothing">
    <div class="views-content-nothing">
    &nbsp;  </div>
</div>
</div>
<div id="views_slideshow_pager_field_item_bottom_home_slider-block_1" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even">
  <div class="views-field-nothing">
    <div class="views-content-nothing">
    &nbsp;  </div>
</div>
</div>
<div id="views_slideshow_pager_field_item_bottom_home_slider-block_2" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd">
  <div class="views-field-nothing">
    <div class="views-content-nothing">
    &nbsp;  </div>
</div>
</div>
<div id="views_slideshow_pager_field_item_bottom_home_slider-block_3" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even">
  <div class="views-field-nothing">
    <div class="views-content-nothing">
    &nbsp;  </div>
</div>
</div>
<div id="views_slideshow_pager_field_item_bottom_home_slider-block_4" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd views-row-last">
  <div class="views-field-nothing">
    <div class="views-content-nothing">
    &nbsp;  </div>
</div>
</div>
</div>
      </div>
      </div>
    </div>
  
  
  
  
  
  
</div></div>
  </div></div></div></div>        </div>
      </div>
    </div>

    <div class="container">

    <!-- !Messages and Help -->
        
    <!-- !Secondary Content Region -->
    
    <div id="columns" class="columns clearfix">
      <main id="content-column" class="content-column" role="main">
        <div class="content-inner">

          <!-- !Highlighted region -->
          		  
		  <section id="main-content">
			
			<!-- !Breadcrumbs -->
						
            
            <!-- !Main Content Header -->
                          <header id="main-content-header" class="clearfix">

                                  <h1 id="page-title">
                    New Wildlife Tours - journey into the natural world                  </h1>
                
                
              </header>
            
            <!-- !Main Content -->
                          <div id="content" class="region">
                <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-12" class="node node-page article odd node-full clearfix" about="/new-wildlife-tours-journey-natural-world" typeof="foaf:Document" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p>New Wildlife Tours offers a selection of day trips to see wildlife, landscapes and archaeology in the South West and selected areas. Tours are available to some of Britain's best wildlife sites such as the South West Coast, Somerset levels, the Cotswolds and Wiltshire's nationally important chalk downland.</p>
<p>All trips are expertly guided and take you to the best sites to ensure you a memorable wildlife experience. Tours focus on all aspects of nature although some tours have a specific interest such as birds, plants and butterflies. We generally visit a couple of different sites during the day but are not rushed and usually involve slow paced gentle walks.</p>
</div></div></div>  </div>

  
  
  <span property="dc:title" content="New Wildlife Tours - journey into the natural world" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span></article>

  </div>              </div>
            
            <!-- !Feed Icons -->
            
            
          </section><!-- /end #main-content -->

          <!-- !Content Aside Region-->
          
        </div><!-- /end .content-inner -->
      </main><!-- /end #content-column -->

      <!-- !Sidebar Regions -->
            <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section id="block-block-1" class="block block-block odd first block-count-4 block-region-sidebar-second block-1" ><div class="block-inner clearfix">  
      <h2 class="block-title">Social Media (Side)</h2>
  
  <div class="block-content content"><p><a href="https://www.facebook.com/New-Wildlife-Tours-1160658553989504/" target="_blank"><img src="/sites/all/themes/nwt/images/facebook.png" width="51" height="51" alt="Like New Wildlife Tours on Facebook" /></a>   <a href="https://twitter.com/NewWildlifeTour" target="_blank"><img src="/sites/all/themes/nwt/images/twitter.png" width="51" height="51" alt="Follow New Wildlife Tours on Twitter" /></a></p>
</div>
  </div></section><section id="block-views-news-block-1" class="block block-views even last block-count-5 block-region-sidebar-second block-news-block-1" ><div class="block-inner clearfix">  
      <h2 class="block-title">Latest News...</h2>
  
  <div class="block-content content"><div class="view view-news view-id-news view-display-id-block_1 view-dom-id-cfc167a98e911d3e0340f746de181269">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/news/back-brink-project">Back from the Brink project </a></span>  </div>  
  <div class="views-field views-field-body">        <span class="field-content">Attended a meeting studying the arable flowers of Wiltshire, it was fascinating. Rough Poppy, Dense and Few-flowered Fumitory and Venus's Looking-glass were wonderful. There were many others with Red... <a href="/news/back-brink-project" class="views-more-link">Read more</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div>
  </div></section></div></div>
    </div><!-- /end #columns -->

    <!-- !Tertiary Content Region -->
    	
	<div id="mob-top"><a href="#page"><img src="/sites/all/themes/nwt/images/top.png" width="40px;"/></a></div>
	
  </div>

  </div>
  
    <!-- !Footer -->
          <footer id="footer" class="clearfix" role="contentinfo">
        <div class="region region-footer"><div class="region-inner clearfix"><section id="block-block-2" class="block block-block odd first block-count-6 block-region-footer block-2" ><div class="block-inner clearfix">  
      <h2 class="block-title">Footer Block</h2>
  
  <div class="block-content content"><div style="float: left; font-weight:600;">
<p>New Wildlife Tours Limited<br />Registered in England and Wales<br />Company Registration Number: 10546835</p>
<p>Registered Address:<br />1 Wessex Way<br />Highworth<br />Wiltshire<br />SN6 7NT</p>
<p><a href="/sites/default/files/terms-and-conditions.pdf" target="_blank">Terms &amp; Conditions</a></p>
</div>
<div style="float: left; font-weight:600;">
<p style="font-size:120%;"><span style="color:#f29200;">Telephone:</span> 07879 061 560</p>
<p style="font-size:120%;"><span style="color:#f29200;">Email:</span> <a href="mailto:info@newwildlifetours.co.uk">info@newwildlifetours.co.uk</a></p>
<p> </p>
<p><a href="https://www.stevedixoncreative.co.uk/" target="_blank">Site by Steve Dixon Creative</a></p>
</div>
<div style="float: left; font-weight:600;">
<p><a href="https://www.facebook.com/New-Wildlife-Tours-1160658553989504/" target="_blank"><img src="/sites/all/themes/nwt/images/facebook2.png" width="33" height="33" alt="Like New Wildlife Tours on Facebook" /></a></p>
<p><a href="https://twitter.com/NewWildlifeTour" target="_blank"><img src="/sites/all/themes/nwt/images/twitter2.png" width="33" height="33" alt="Follow New Wildlife Tours on Twitter" /></a></p>
</div>
</div>
  </div></section><nav id="block-system-main-menu" class="block block-system block-menu no-title even last block-count-7 block-region-footer block-main-menu"  role="navigation"><div class="block-inner clearfix">  
  
  <div class="block-content content"><ul class="menu clearfix"><li class="first leaf menu-depth-1 menu-item-219"><a href="/" class="active">Home</a></li><li class="leaf menu-depth-1 menu-item-394"><a href="/about">About</a></li><li class="collapsed menu-depth-1 menu-item-395"><a href="/tours" title="">Trips</a></li><li class="leaf menu-depth-1 menu-item-396"><a href="/booking">Booking</a></li><li class="leaf menu-depth-1 menu-item-397"><a href="/frequently-asked-questions">FAQs</a></li><li class="leaf menu-depth-1 menu-item-500"><a href="/news" title="">News</a></li><li class="last leaf menu-depth-1 menu-item-502"><a href="/contact-us">Contact</a></li></ul></div>
  </div></nav></div></div>              </footer>
    
</div>
  </body>
</html>
