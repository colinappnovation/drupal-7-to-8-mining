<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

  <meta charset="utf-8" />
<meta name="description" content="Richard Alston Dance Company is a London based touring contemporary dance company that performs &quot;inspirational and beautiful work and possesses a clutch of the very best contemporary dancers in the world” Graham Watts, Londondance.com.
 " />
<meta name="abstract" content="Located in the heart of London, The Place is the UK&#039;s premiere centre for contemporary dance uniting dance training, creation and performance in our unique, purpose-built centre." />
<meta name="keywords" content="London, Theatre, Dance, Contemporary Dance, Gallery, Courses, Café, Bar, Workshop, Classes, University of Kent, School, Degree, undergraduate course, postgraduate course, Conservatoire for Dance and Drama, Richard Alston Dance Company, London Contemporary Dance School, Robin Howard Dance Theatre, The Place, young people, evening school, saturday classes, adult classes, professional class, yoga, pilates, contemporary jazz, choreography, education, performance, apprenticeships, professional choreographers, professional development, academic research, teaching resources, Centre for Advanced Training, studios, studio hire, support dance" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.richardalstondance.com/richard-alston-dance-company-touring-contemporary-dance-company" />
<link rel="shortlink" href="https://www.richardalstondance.com/node/1292" />
<link rel="shortcut icon" href="https://www.richardalstondance.com/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="p:domain_verify" content="3c9eeed2d526064af8c04c021cc41122" />
  <title>Richard Alston Dance Company - touring contemporary dance company | Richard Alston Dance Company</title>
  
  <meta name="viewport" content="width=device-width,initial-scale=1">
  
  <style>.async-hide { opacity: 0 !important} </style>
  <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-W28NJN5':true});</script>

<link rel="stylesheet" href="https://use.typekit.net/smf0wft.css">
<link rel="apple-touch-icon" sizes="180x180" href="/sites/default/themes/theplace/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/sites/default/themes/theplace/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/sites/default/themes/theplace/favicon/favicon-16x16.png">
<link rel="manifest" href="/sites/default/themes/theplace/favicon/site.webmanifest">
<link rel="mask-icon" href="/sites/default/themes/theplace/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/sites/default/themes/theplace/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="/sites/default/themes/theplace/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
  
  <style type="text/css" media="all">
@import url("https://www.richardalstondance.com/modules/system/system.base.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/system/system.menus.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/system/system.messages.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/system/system.theme.css?pyn1eh");
</style>
<style type="text/css" media="all">
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/calendar/css/calendar_multiday.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/date/date_api/date.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/date/date_popup/themes/datepicker.1.7.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/field/theme/field.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/node/node.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/picture/picture_wysiwyg.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/search/search.css?pyn1eh");
@import url("https://www.richardalstondance.com/modules/user/user.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/views/css/views.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/ckeditor/css/ckeditor.css?pyn1eh");
</style>
<style type="text/css" media="all">
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/ctools/css/ctools.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/facebook_pull/facebook_pull.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/panels/css/panels.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/modules/contrib/flexslider/assets/css/flexslider_img.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/libraries/flexslider/flexslider.css?pyn1eh");
</style>
<link type="text/css" rel="stylesheet" href="//cloud.webtype.com/css/b5cc47db-8a32-4d64-a6f4-57b732a7a542.css" media="all" />
<style type="text/css" media="screen">
@import url("https://www.richardalstondance.com/sites/default/themes/theplace/css/foundation.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/themes/theplace/css/style-v2.css?pyn1eh");
@import url("https://www.richardalstondance.com/sites/default/themes/theplace/css/photoswipe.css?pyn1eh");
</style>
<style type="text/css" media="print">
@import url("https://www.richardalstondance.com/sites/default/themes/theplace/css/print.css?pyn1eh");
</style>

<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.richardalstondance.com/sites/default/themes/theplace/css/ie-old.css?pyn1eh" media="all" />
<![endif]-->

<!--[if IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.richardalstondance.com/sites/default/themes/theplace/css/ie9.css?pyn1eh" media="all" />
<![endif]-->

<!--[if gte IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.richardalstondance.com/sites/default/themes/theplace/css/ie.css?pyn1eh" media="all" />
<![endif]-->
  <script type="text/javascript" src="https://www.richardalstondance.com/sites/default/modules/contrib/jquery_update/replace/jquery/1.8/jquery.min.js?v=1.8.2"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/misc/jquery-extend-3.4.0.js?v=1.8.2"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/misc/drupal.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/misc/form.js?v=7.66"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/libraries/flexslider/jquery.flexslider-min.js?pyn1eh"></script>
<script type="text/javascript" src="https://system.spektrix.com/theplace/website/scripts/resizeiframe.js"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/libs/jquery.foundation.navigation.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/libs/klass.min.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/libs/code.photoswipe.jquery-3.0.5.min.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/place.jquery.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/libs/modernizr.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/libs/jquery.matchHeight-min.js?pyn1eh"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/themes/theplace/js/theplace.new.js?pyn1eh"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"theplace","theme_token":"v3Q88gyTJ1tC0nfKeJF1K6h3aLdazTqGhp6aFaOiOks","js":{"sites\/default\/modules\/contrib\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/form.js":1,"sites\/default\/themes\/theplace\/js\/libs\/jquery.foundation.navigation.js":1,"sites\/default\/themes\/theplace\/js\/libs\/klass.min.js":1,"sites\/default\/themes\/theplace\/js\/libs\/code.photoswipe.jquery-3.0.5.min.js":1,"sites\/default\/themes\/theplace\/js\/place.jquery.js":1,"sites\/default\/themes\/theplace\/js\/libs\/modernizr.js":1,"sites\/default\/themes\/theplace\/js\/libs\/jquery.matchHeight-min.js":1,"sites\/default\/themes\/theplace\/js\/theplace.new.js":1,"sites\/default\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/default\/modules\/contrib\/picture\/picturefill\/matchmedia.js":1,"sites\/default\/modules\/contrib\/picture\/picturefill\/picturefill.js":1,"sites\/default\/modules\/contrib\/picture\/picture.js":1,"sites\/default\/modules\/contrib\/flexslider\/assets\/js\/flexslider.load.js":1,"https:\/\/system.spektrix.com\/theplace\/website\/scripts\/resizeiframe.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/default\/modules\/contrib\/calendar\/css\/calendar_multiday.css":1,"sites\/default\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/default\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/default\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/default\/modules\/contrib\/views\/css\/views.css":1,"sites\/default\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/default\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/default\/modules\/contrib\/facebook_pull\/facebook_pull.css":1,"sites\/default\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/default\/modules\/contrib\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/default\/libraries\/flexslider\/flexslider.css":1,"\/\/cloud.webtype.com\/css\/b5cc47db-8a32-4d64-a6f4-57b732a7a542.css":1,"sites\/default\/themes\/theplace\/css\/foundation.css":1,"sites\/default\/themes\/theplace\/css\/style-v2.css":1,"sites\/default\/themes\/theplace\/css\/photoswipe.css":1,"sites\/default\/themes\/theplace\/css\/print.css":1,"sites\/default\/themes\/theplace\/css\/ie-old.css":1,"sites\/default\/themes\/theplace\/css\/ie9.css":1,"sites\/default\/themes\/theplace\/css\/ie.css":1}},"urlIsAjaxTrusted":{"\/newsletter\/subscribe":true,"\/":true},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":false,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":7000,"directionNav":true,"controlNav":true,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}}});
//--><!]]>
</script>
  
<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
    	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
 
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-1292 node-type-landing-page radc no-banner" >
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K88K9T"
 height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script type="text/javascript">
(function(w,d,s,l,i){

  w[l]=w[l]||[];
  w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});
  var f=d.getElementsByTagName(s)[0];
  var j=d.createElement(s);
  var dl=l!='dataLayer'?'&l='+l:'';
  j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;
  j.type='text/javascript';
  j.async=true;
  f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-K88K9T');
</script>
<!-- End Google Tag Manager -->

    <header>
  <div class="row no-padding">
    <div class="twelve columns">
      
	  <div id="site-name">
	  	<a href="http://www.theplace.org.uk/" title="The Place main site">The Place</a>

      <aside class="weare">We are 50</aside>
	  </div>
	  
	  <!-- mobile-nav content starts -->
	  <div id="mobile-nav">
	  <!-- 	  
	    <div class="strapline hide-for-touch"><a href="https://www.richardalstondance.com/" title="Home" rel="home">Richard Alston Dance Company</a></div>
	     -->
	  <div class="user-tools">
	  
	    <!-- 	      <div id="block-search-form" class="block block-search">
        <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit--2" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-i6qJbmtimgbx81c-V_8i33yHV2keDz5YrA1T9MN2Png" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  
 -->
	  		
	    <div class="mobile-menu"><a href="#" id="nav-close">Close menu</a></div>
		
	  </div> <!--./user-tools-->
	  
	  <nav>
	  	<h2 class="element-invisible">Menu</h2><ul id="main-menu" class="nav-bar"><li class="mobile-menu"><a href="https://www.theplace.org.uk/">Home</a></li><li class="has-flyout hide-for-small"><a href="https://www.richardalstondance.com/" class="active-trail">RADC</a><a href="#" class="flyout-toggle"><span> </span></a><ul class="flyout"><li class="place"><a href="https://www.theplace.org.uk/">The Place</a></li><li class="lcds"><a href="https://www.lcds.ac.uk/">London Contemporary Dance School</a></li></ul></li><li class="menu-2201 first"><a href="https://www.richardalstondance.com/performances?category%5B%5D=69" title="">Performances</a></li>
<li class="menu-2183"><a href="https://www.richardalstondance.com/repertoire">Repertoire</a></li>
<li class="menu-1512"><a href="https://www.richardalstondance.com/richard">Richard Alston</a></li>
<li class="menu-1513"><a href="https://www.richardalstondance.com/richard-alston-dance-company-1">Company</a></li>
<li class="menu-1524"><a href="https://www.richardalstondance.com/essential-alston-education-programme">Education</a></li>
<li class="menu-1523"><a href="https://www.richardalstondance.com/contact-us">Keep In Touch</a></li>
<li class="menu-1530 last"><a href="https://www.richardalstondance.com/radc-support">Support</a></li>
<li class="mobile-menu lcds"><a href="https://www.lcds.ac.uk/">London Contemporary Dance School</a></li><li class="mobile-menu radc"><a href="https://www.richardalstondance.com/">Richard Alston Dance Company</a></li></ul>	  </nav>
	  
	  </div>
	  <!-- mobile-nav content ends -->
	  
	    <div id="block-block-10" class="block block-block">
        <nav class="mobile-menu">
  <ul>
  <li><div id="nav-open">Menu</div></li>
  <li><a class="tel" href="tel:+44 (0)20 7121 1100">Call</a></li>
  <li><a href="http://maps.google.com/?q=51.527277,-0.128625">Map</a></li>
</ul>
</nav></div>
  
	  	  
    </div>
  </div>
  </header>


  
<div id="wrapper">

    	  
	  <div id="block-system-main" class="block block-system">
        
<div class="row mainContent">
	  
        <div class="eight columns">
		  
  
  <div class="strapline-mobile"><a href="#section-nav">Richard Alston Dance Company</a></div> 
  
  

  
  <div class="field field-name-field-carousel field-type-entityreference field-label-hidden">
        <div class="field-items"> 
		    <div  id="flexslider-1" class="flexslider">
  <ul class="slides"><li>  
  <div class="flex-caption">
      <p class="date-stamp">      27 Sep - 22 Nov    
</p>
  	<h3><a href="https://www.richardalstondance.com/whats-on/seasons/richard-alston-dance-company-tour">Richard Alston Dance Company Tour </a></h3>
	<h4></h4>
	<ul class="more">
	  <li class="info"><a href="https://www.richardalstondance.com/whats-on/seasons/richard-alston-dance-company-tour" class="button">More Info</a></li>
	</ul>
  </div>
  
    <div class="flex-banner matchHeight">
    <img src="https://www.richardalstondance.com/sites/default/files/styles/flexslider_full/public/images/banner-images/Photo%20Chris%20Nash.jpg?itok=_Ta24GPv" />  </div>  
    


</li>
<li>
  <div class="flex-caption">
    <p class="date-stamp">      <span class="date-display-start">Wed 27 Nov</span> to <span class="date-display-end">Sat 30 Nov</span>    
</p>
	<h3><a href="https://www.richardalstondance.com/whats-on/richard-alston-dance-company-0">Richard Alston Dance Company</a></h3>
	<h4>      Alston At Home    
</h4>
	<ul class="more">
	  	  <li class="info"><a href="https://www.richardalstondance.com/whats-on/richard-alston-dance-company-0" class="button">More Info</a></li>
	</ul>
  </div>
  
    <div class="flex-banner matchHeight">
    <img src="https://www.richardalstondance.com/sites/default/files/styles/flexslider_full/public/images/banner-images/Detour-Joshua-Hariette-and-Monique-Jonas-Photo-by-Chris-Nash.jpg?itok=cBpWy_pk" alt="Detour-Joshua-Hariette-and-Monique-Jonas-Photo-by-Chris-Nash" title="Detour-Joshua-Hariette-and-Monique-Jonas-Photo-by-Chris-Nash" />  </div>  
    


</li>
<li>
  <div class="flex-caption">
   	<h3><a href="https://www.richardalstondance.com/richard-alston-dance-company-close-2020">Richard Alston Dance Company to close in 2020</a></h3>
   	<p>      Richard Alston Dance Company to close in 2020    
</p>
	<ul class="more">
	  <li class="info"><a href="https://www.richardalstondance.com/richard-alston-dance-company-close-2020" class="button">More Info</a></li>
	</ul>
  </div>
  
    <div class="flex-banner matchHeight">
    <img src="https://www.richardalstondance.com/sites/default/files/styles/flexslider_full/public/images/banner-images/Company%20Shot%202019_web.jpg?itok=YTks04-c" />  </div>  
    


</li>
<li>  
  <div class="flex-caption">
   	<h3><a href="https://www.richardalstondance.com/blog/place-blog/knighthood-richard-alston">A Knighthood for Richard Alston</a></h3>
   	<p></p>
	<ul class="more">
	  <li class="info"><a href="https://www.richardalstondance.com/blog/place-blog/knighthood-richard-alston" class="button">More Info</a></li>
	</ul>
  </div>
  
    <div class="flex-banner matchHeight">
    <img src="https://www.richardalstondance.com/sites/default/files/styles/flexslider_full/public/images/banner-images/Richard%20Alston%202014%203%20by%20Hugo%20Glendinning_%20landscape.jpg?itok=vDQGAPNj" alt="Richard Alston 2014 3 by Hugo Glendinning_ landscape.jpg" title="Richard Alston 2014 3 by Hugo Glendinning_ landscape.jpg" />  </div>  
    

  
</li>
</ul></div>
	  
    </div>
</div>  
  
  

  
  <div class="row">
  
  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

        
  <a href="https://www.richardalstondance.com/richard">
    <span class="label top">Richard Alston</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/banner-images/Richard%20Alston%20photo%20by%20Hugo%20Glendinning_web.jpg?itok=xBcH8KK_" />    

  </a>

      </div>
    </div>

  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

        
  <a href="https://www.richardalstondance.com/richard-alston-dance-company-1">
    <span class="label top">Richard Alston Dance Company</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/banner-images/Company%20Shot%202019_web.jpg?itok=bXXLb161" />    

  </a>

      </div>
    </div>

  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

          
  <a href="https://www.richardalstondance.com/repertoire">
    <span class="label top">Repertoire</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/banner-images/Holderlin%20Fragments%202%2C%201120x630%20Liam%20Riddick%20and%20Oihana%20Vesga%20Bujan_0.jpg?itok=xIcR2bPV" alt="Liam Riddick and Oihana Vesga Bujan in Holderlin Fragments. Image by Chris Nash" title="Liam Riddick and Oihana Vesga Bujan in Holderlin Fragments. Image by Chris Nash" />    

  </a>
  
      </div>
    </div>

  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

        
  <a href="https://www.richardalstondance.com/essential-alston-education-programme">
    <span class="label top">Essential Alston Education Programme</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/gallery/Phaedra%201120x630%20-%20Dancers%20Nancy%20Nerantzi%2C%20Ihsaan%20de%20Banya%2C%20Oihana%20Vesga%20Bujan%2C%20James%20Muller.jpg?itok=3CKbLSKx" alt="Richard Alston Dance Company - Nancy Nerantzi, Ihsaan de Banya, Oihana Vesga Bujan and James Muller. Image by Tony Nandi" title="Nancy Nerantzi, Ihsaan de Banya, Oihana Vesga Bujan and James Muller. Image by Tony Nandi" />    

  </a>

      </div>
    </div>

  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

        
  <a href="https://www.richardalstondance.com/resources">
    <span class="label top">DVDs &amp; Resources</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/banner-images/RADC%20DVD%201120x630.jpg?itok=LWbI-Z-4" alt="Essential Alston, All American Alston and In Overdrive DVDs" title="Essential Alston, All American Alston and In Overdrive DVDs" />    

  </a>

      </div>
    </div>

  
    <div class="four columns matchHeight">
      <div class="signpost-block arrow">

          
  <a href="https://www.richardalstondance.com/latest-news">
    <span class="label top">Latest news</span>
    
        <img src="https://www.richardalstondance.com/sites/default/files/styles/thumbnail_233x129/public/images/banner-images/RADC%20Dancing%20around%20Duchamp%20event.jpg?itok=sisecuqN" alt="Merce Cunningham choreography performed by Andy Richard Alston Dance Company as part of the Barbican exhibition: Dancing Around Duchamp: The Bride and the Bachelors 2013. Image by Sidd Khajuria. Courtesy of Barbican Art Gallery" title="Merce Cunningham choreography performed by Andy Richard Alston Dance Company as part of the Barbican exhibition: Dancing Around Duchamp: The Bride and the Bachelors 2013. Image by Sidd Khajuria. Courtesy of Barbican Art Gallery." />    

  </a>
  
      </div>
    </div>

    

</div>
 
  
  
        </div>
		
		<div class="four columns">
          	
			
  
  
            <div class="signpost-block-content">
<h2>Next Performances</h2>  
  
  
      
          <ul class="whats-on">          <li class="">  
          <h3><span class="date-display-single">22 Jan 2020</span></h3>    
          <p><a href="https://www.richardalstondance.com/whats-on/final-edition-new-theatre-oxford-0">Final Edition: New Theatre Oxford</a></p>  </li>
          <li class="">  
          <h3><span class="date-display-single">28 Jan 2020</span></h3>    
          <p><a href="https://www.richardalstondance.com/whats-on/final-edition-exeter-northcott-theatre">Final Edition: Exeter Northcott Theatre</a></p>  </li>
          <li class="">  
          <h3><span class="date-display-start">31 Jan 2020</span> to <span class="date-display-end">1 Feb 2020</span></h3>    
          <p><a href="https://www.richardalstondance.com/whats-on/final-edition-theatre-royal-bath">Final Edition: Theatre Royal Bath</a></p>  </li>
          <li class="">  
          <h3><span class="date-display-start">7 Feb 2020</span> to <span class="date-display-end">8 Feb 2020</span></h3>    
          <p><a href="https://www.richardalstondance.com/whats-on/final-edition-theatre-royal-norwich">Final Edition: Theatre Royal Norwich</a></p>  </li>
      </ul>    
  
  
  
  
     <p class="view-all"><a href="/performances?category[]=69&amp;field_venue_tid=All">View All</a></p>
</div>  
  

 
  
  
			
						  <div class="signpost-block-content block-form">
				<h2 class="subscribe-title">Sign up to our Newsletter</h2>
<div class="page-form subscribe">
  <form class="spektrix-subscribe" action="/newsletter/subscribe" method="post" id="spektrix-subscribe-newsletter" accept-charset="UTF-8"><div><fieldset class="form-wrapper" id="edit-newsletter"><div class="fieldset-wrapper"><div class="form-item form-type-textfield form-item-first-name">
  <label for="edit-first-name">First Name </label>
 <input type="text" id="edit-first-name" name="first_name" value="" size="20" maxlength="20" class="form-text" />
</div>
<div class="form-item form-type-textfield form-item-second-name">
  <label for="edit-second-name">Second Name <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-second-name" name="second_name" value="" size="20" maxlength="20" class="form-text required" />
</div>
<div class="form-item form-type-textfield form-item-email">
  <label for="edit-email">Email address <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-email" name="email" value="" size="20" maxlength="128" class="form-text required" />
</div>
<input type="hidden" name="destination_url" value="node/1292" />
</div></fieldset>
<input type="submit" id="edit-submit" name="op" value="Sign Up " class="form-submit" /><input type="hidden" name="form_build_id" value="form-ulOA3MSToIFO9-GE_U2YtKz_mKQBqf5q1mySw_TGEqg" />
<input type="hidden" name="form_id" value="spektrix_subscribe_newsletter" />
</div></form></div>
			  </div>
						
			  
			  <div class="signpost-block-content social-stream hide-for-small">
				
  
  <p class="social-more"><a href="http://facebook.com/profile.php?id=186680874687977" target="_blank">Find us on Facebook</a></p> 
  
  
			  </div>
				
        </div>
		
		  
		  <div class="row show-for-small">
	        <div class="twelve columns section-nav">
		      
  
  <a name="section-nav"></a>
<h3>In this section:</h3>
<ul class="menu"><li class="first leaf has-children menu-mlid-453"><a href="https://www.richardalstondance.com/performances?venue%5B%5D=54" title="">What&#039;s on</a></li>
<li class="leaf has-children menu-mlid-1396"><a href="https://www.richardalstondance.com/participation" title="">Participation</a></li>
<li class="leaf has-children menu-mlid-1374"><a href="https://www.richardalstondance.com/professionals">Professionals &amp; Artist Development</a></li>
<li class="leaf has-children menu-mlid-674"><a href="https://www.richardalstondance.com/the-place-news" title="" class="menu-firstchild">News &amp; Blogs</a></li>
<li class="leaf has-children menu-mlid-510"><a href="https://www.richardalstondance.com/about-place" title="" class="menu-firstchild">About</a></li>
<li class="leaf has-children menu-mlid-1360"><a href="https://www.richardalstondance.com/support-our-work" title="" class="menu-firstchild">Support Us</a></li>
<li class="last leaf has-children menu-mlid-11431"><a href="https://www.richardalstondance.com/we-are-50-0">We are 50</a></li>
</ul> 
  
  
		    </div>  
		  </div>
			
		
</div> <!-- ./row-->
</div>
<div id="block-block-16" class="block block-block">
        <p><a href="https://plus.google.com/111726566828475747524" rel="publisher"></a></p>
</div>
  

      
			
	<footer>
	  <div class="row">
	  
        <div class="three columns">
          	  <div id="block-block-1" class="block block-block">
        <div id="site-name-footer">
  <a href="http://www.theplace.org.uk" title="The Place">The Place</a>
</div>
<div class="org" itemprop="affiliation" title="Organization" itemscope="itemscope" itemtype="http://www.data-vocabulary.org/Organization/">
				<div class="vcard">
				<span class="organization-name fn hide-for-small" itemprop="name">The Place</span>
				<div class="adr" itemprop="address" itemscope="itemscope" itemtype="http://data-vocabulary.org/Address/">
				<span class="street-address" itemprop="street-address">17 Duke's Road</span>
				<abbr class="region" itemprop="region" title="London">London</abbr>
				<abbr class="postal-code" itemprop="postal-code" title="WC1H 9PY">WC1H 9PY</abbr>
				<abbr class="country-name" itemprop="country-name" title="UK">UK</abbr>
				<!-- Lat/Lon (Metadata) -->
				<span class="geo" itemprop="geo" itemscope="itemscope" itemtype="http://data-vocabulary.org/Geo/">
					<abbr class="latitude" itemprop="latitude" title="51.527277">51.527277</abbr>
					<abbr class="longitude" itemprop="longitude" title="-0.128625">-0.128625</abbr>
				</span>
				</div> <!--/ .adr -->
				</div> <!--/ .vcard -->
</div> <!--/ .org -->
<p class="tel">Box Office: <a href="tel:+442071211100">020 7121 1100</a></p></div>
  
        </div>
		
		<div class="five columns">
			<h2 class="element-invisible">Menu</h2><ul id="main-menu" class="footer-menu"><li class="menu-1862 first"><a href="https://www.richardalstondance.com/privacy-policy" title="">Privacy Policy</a></li>
<li class="menu-604"><a href="https://www.richardalstondance.com/about-place" title="">About The Place</a></li>
<li class="menu-1858"><a href="https://www.richardalstondance.com/accessibility-website" title="">Accessibility</a></li>
<li class="menu-1859"><a href="https://www.richardalstondance.com/help" title="">Help</a></li>
<li class="menu-1860"><a href="https://www.richardalstondance.com/terms-and-conditions" title="">Terms &amp; Conditions</a></li>
<li class="menu-3103"><a href="http://www.theplace.org.uk/my-account" title="">My account</a></li>
<li class="menu-9546"><a href="https://www.theplace.org.uk/contact" title="Contact Page">Contact</a></li>
<li class="menu-1861 last"><a href="https://www.richardalstondance.com/sitemap" title="">Index</a></li>
</ul>        </div>
		
		<div class="four columns">
            <div id="block-block-7" class="block block-block">
        <p class="charity">The Place is a trading name of Contemporary Dance Trust Ltd, a company limited by guarantee. Registered in England and Wales No: 883094. Charity No: 250216. VAT Registration: GB 256 1289 94</p></div>
<div id="block-block-4" class="block block-block">
        <ul class="connect">
  <li><a class="facebook" href="https://www.facebook.com/theplace" target="_blank">Facebook</a></li>
  <li><a class="twitter" href="http://twitter.com/ThePlaceLondon" target="_blank">Twitter</a></li>
  <li><a class="youtube" href="http://www.youtube.com/user/ThePlacefordance" target="_blank">YouTube</a></li>
<li><a class="insta" href="https://www.instagram.com/theplacelondon/" target="_blank">Instagram</a></li>
</ul></div>
  
        </div>
		
      </div> <!-- ./row-->
	</footer>
</div>
  <script type="text/javascript" src="https://www.richardalstondance.com/sites/default/modules/contrib/picture/picturefill/matchmedia.js?v=0.1"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/modules/contrib/picture/picturefill/picturefill.js?v=0.1"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/modules/contrib/picture/picture.js?v=7.66"></script>
<script type="text/javascript" src="https://www.richardalstondance.com/sites/default/modules/contrib/flexslider/assets/js/flexslider.load.js?pyn1eh"></script>

  <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
  <script type="text/javascript">
      window.cookieconsent_options = {"message":"Our website uses cookies. Continue with your visit by closing this message or","dismiss":"Dismiss","learnMore":"find out more","link":"https://www.theplace.org.uk/privacy-policy","theme":"dark-bottom"};
  </script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
  <!-- End Cookie Consent plugin -->

</body>
</html>
