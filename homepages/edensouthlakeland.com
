<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:creator" content="@EdenSLakelandCU" />
<meta name="twitter:image" content="https://www.edensouthlakeland.com/sites/default/files/images/ESLCU-2019-Council-Flyer.png" />
<meta name="twitter:site" content="@EdenSLakelandCU" />
<meta name="twitter:title" content="Welcome to your community bank..." />
<meta name="twitter:description" content="Eden &amp; South Lakeland credit union has affordable loans available for those living and working in Eden District and South Lakeland District, Cumbria." />
<meta property="og:description" content="Eden &amp; South Lakeland credit union has affordable loans available for those living and working in Eden District and South Lakeland District, Cumbria." />
<meta property="og:image" content="https://www.edensouthlakeland.com/sites/default/files/images/ESLCU-2019-Council-Flyer.png" />
<meta property="twitter:image" content="https://www.edensouthlakeland.com/sites/default/files/eslcu-logo-square.jpg" />
<meta property="og:image" content="https://www.edensouthlakeland.com/sites/default/files/eslcu-logo-square.jpg" />
<link rel="shortcut icon" href="https://www.edensouthlakeland.com/sites/default/files/favicon2.ico" type="image/vnd.microsoft.icon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.edensouthlakeland.com/" />
<link rel="shortlink" href="https://www.edensouthlakeland.com/" />
  <title>Eden & South Lakeland Credit Union | Save, Borrow, Invest in Tomorrow</title>
  <link type="text/css" rel="stylesheet" href="https://www.edensouthlakeland.com/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.edensouthlakeland.com/sites/default/files/css/css_JCY_XIQAlFCN_AbvjWfbNrXAa03IOm7FrXUxsDgkibE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.edensouthlakeland.com/sites/default/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.0/dist/css/bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@unicorn-fail/drupal-bootstrap-styles@0.0.2/dist/3.3.1/7.x-3.x/drupal-bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.edensouthlakeland.com/sites/default/files/css/css_MkPX9Cb_OPbAHruzW9yXZTRi9kQkqCFKD1tNEXRCn-M.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="https://www.edensouthlakeland.com/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.0/dist/js/bootstrap.min.js"></script>
<script src="https://www.edensouthlakeland.com/sites/default/files/js/js_Ks7WtC6SGrvYwOJXdEEp9245XhegV8VyqQV4Ks4y50c.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap_eslcu","theme_token":"3VuTUYjNYSg9VWRCRbtH-VAOfbFViCD-mW2OZ5GIkkU","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.4.0\/dist\/js\/bootstrap.min.js":1,"sites\/all\/themes\/bootstrap_eslcu\/js\/eslcu.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.4.0\/dist\/css\/bootstrap.min.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/@unicorn-fail\/drupal-bootstrap-styles@0.0.2\/dist\/3.3.1\/7.x-3.x\/drupal-bootstrap.min.css":1,"sites\/all\/themes\/bootstrap_eslcu\/css\/style-eslcu.css":1,"sites\/all\/themes\/bootstrap_eslcu\/css\/style-eslcu-extra.css":1}},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":0,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="container">
	<div class="esclu-header">
		<a class="logo" href="/" title="Home">
			<img src="https://www.edensouthlakeland.com/sites/default/files/eslcu-logo-tight-noblue.jpg" alt="Home" />
		</a>
		<div id="eslcu-header-facebook">
		<a href="https://www.facebook.com/EdenSouthLakelandCreditUnion">
			<img src="/sites/default/files/images/facebook_60x60px.png" alt="Eden &amp; South Lakeland on Facebook"/>
		</a>
		</div>
		<div id="eslcu-header-twitter">
		<a href="https://twitter.com/EdenSLakelandCU">
			<img src="/sites/default/files/images/twitter_60x60.png" alt="Eden &amp; South Lakeland on Twitter"/>
		</a>
		</div>
	</div>
   	<div class="esclu-slogan">
     Save, Borrow, Invest in Tomorrow	</div>
  </div>

<div id="header-wrapper">
<div id="header-wrapper2" class="container">
<header id="navbar" role="banner" class="navbar container navbar-default">
  <div class="container">
    <div class="navbar-header">

      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      			<a class="navbar-brand" href="#"></a>
    </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first leaf active"><a href="/" accesskey="1" class="active">Home</a></li>
<li class="leaf"><a href="/savings" title="How to save with Eden &amp; South Lakeland credit union">Saving</a></li>
<li class="expanded dropdown"><a href="/loans" title="How to get a loan from Eden &amp; South Lakeland Credit Union" class="dropdown-toggle" data-toggle="dropdown">Borrow <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/loans">Loans</a></li>
<li class="leaf"><a href="/loans/calculator">Loan calculator</a></li>
<li class="leaf"><a href="/loans/apply">Apply for a Loan</a></li>
<li class="last leaf"><a href="/loans/terms">Full details</a></li>
</ul></li>
<li class="expanded dropdown"><a href="/membership" title="How to join Eden &amp; South Lakeland Credit Union - and the benefits it brings" class="dropdown-toggle" data-toggle="dropdown">Join <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/membership">Adult membership</a></li>
<li class="leaf"><a href="/join/junior">Junior accounts</a></li>
<li class="leaf"><a href="/join/corporate">Clubs and Organisations</a></li>
<li class="leaf"><a href="/join/corporate">Corporate accounts</a></li>
<li class="last leaf"><a href="/join/payroll-deduction">Payroll deduction</a></li>
</ul></li>
<li class="leaf"><a href="/branches">Offices</a></li>
<li class="expanded dropdown"><a href="/about" title="Information about Eden &amp; South Lakeland Credit Union Ltd" class="dropdown-toggle" data-toggle="dropdown">About <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/about">About the credit union</a></li>
<li class="leaf"><a href="https://www.edensouthlakeland.com/sites/default/files/EMS-CON-001%20rev01%20Rules%20of%20ESLCU%2019%20Jan%202017.pdf">Rule book</a></li>
<li class="leaf"><a href="/forms">Forms</a></li>
<li class="leaf"><a href="/faq" title="Questions and Answers about Eden &amp; South Lakeland Credit Union">FAQ</a></li>
<li class="leaf"><a href="/news">News</a></li>
<li class="leaf"><a href="/about/meetings" title="Reports. Agendas and Minutes for Eden &amp; South Lakeland Credit Union Ltd">Meetings</a></li>
<li class="leaf"><a href="/about/partners" title="The organisations Eden &amp; South Lakeland credit union are working with and who have helped us">Partners</a></li>
<li class="leaf"><a href="/about/volunteering">Volunteering</a></li>
<li class="leaf"><a href="/search/node" accesskey="4">Search</a></li>
<li class="leaf"><a href="/about/privacy-statement">Privacy Statement</a></li>
<li class="last leaf"><a href="/about/legal" accesskey="8">Privacy, Cookies, Terms and Conditions</a></li>
</ul></li>
<li class="last leaf"><a href="/contact" title="Contact us" accesskey="9">Contact</a></li>
</ul>                                      </nav>
      </div>
      </div>
</header>
</div>
</div>

<div class="main-container container">

  <header role="banner" id="page-header">
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-12">
                  <a id="main-content"></a>
                    <h1 class="page-header">Welcome to your community bank...</h1>
                                                          <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-1" class="node node-page clearfix" about="/home" typeof="foaf:Document">
    <header>
            <span property="dc:title" content="Welcome to your community bank..." class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><div class="homepage">
  <div class="row home-margin">
    <div class="col-md-6">
      <p class=" join joinl">Local finance for our members</p>
    </div>
    <div class="col-md-4 join joinl">
      <a id="home-join" class="btn btn-eslcu-green" href="/membership" role="button">Join us</a>
      <a id="home-contact" class="btn btn-eslcu-green" href="/contact" role="button">Contact us</a>
    </div>
    <div class="col-md-2 join joinr">
      <a href="tel:441768890065">01768 890065</a>
    </div>
  </div>

  <div class="row">
    <a href="/loans" class="home-link">
      <div class="col-md-6" id="home-borrow">
        <div class="panel panel-home home-margin">
          <div class="panel-heading">
            <h4 class="panel-title">
              BORROW
            </h4>
          </div>

          <div class="panel-body">
            <p>Need a new washing machine or kitchen?</p>
            <p>We can help!</p>
            <p>Loans up to £7,000</p>
            <p>
              <img src="/sites/default/files/images/ESLCU-2019-Council-Flyer.png" alt="ESLCU Council Tax flyer 2019" title="ESLCU Council Tax flyer 2019" style="width:200px;border:1px solid green;border-radius:5px;" />
            </p>
          </div>
        </div>
      </div>
    </a>
    <a href="/savings" class="home-link">
      <div class="col-md-6" id="home-save">
        <div class="panel panel-home home-margin">
          <div class="panel-heading">
            <h4 class="panel-title">
              SAVE
            </h4>
          </div>

          <div class="panel-body">
            <p>Need to save for a holiday, Christmas?</p>
            <p>We can help!</p>
            <p>Or save for a rainy day.</p>
            <p>
              <img src="/sites/default/files/images/beach-scene-214x150.png" alt="Get a loan from Eden &amp; South Lakeland Credit Union credit union" title="Get a loan from Eden &amp; South Lakeland credit union" />
            </p>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="row">

    <div class="col-md-6" id="home-borrow">
      <div class="panel panel-home home-margin">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a href="/about/partners">Great offers</a>
          </h4>
        </div>

        <div class="panel-body">
          <p>
            Are you in a Housing Association?
            We have £5 from Eden Housing Assocation, South Lakes Housing, Impact Housing Association and Riverside to help you join!
          </p>
          <p><a id="home-join" class="btn btn-eslcu-green" href="/about/partners" role="button">Find out more</a></p>
        </div>
      </div>

    </div>
    <div class="col-md-6" id="home-borrow">
      <div class="panel panel-home home-margin">
        <div class="panel-heading">
          <h4 class="panel-title">
            A simple way to manage your money
          </h4>
        </div>

        <div class="panel-body">
          <p>
            Once you are a member you don't need to visit our offices to pay back loans or transfer money into or out of your account.
          </p>
          <p>
            Once you've set up a nominated bank account you can request withdrawals by phone or email.
          </p>
        </div>
      </div>

    </div>
  </div>

  <div class="row">
    <div class="col-md-12" id="home-save">
      <div class="panel panel-home home-margin">
        <div class="panel-heading">
          <h4 class="panel-title">
            What our members said
          </h4>
        </div>

        <div class="panel-body">
          <p>"You acted promptly and ... made funds available to us that literally saved the roof from over our heads."</p>
          <p>"We would highly recommend your services to any local person in difficulty and have no hesitation in recommending your union and staff to the local people of Eden."</p>
        </div>
      </div>
    </div>
  </div>

</div>
</div></div></div><div class="field field-name-field-mobile-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even">Welcome</div></div></div>    </article>

</section>
  </div>
    </section>

    
  </div>
</div>

  <footer class="footer container">
      <div class="region region-footer">
    <section id="block-block-2" class="block block-block clearfix">

      
  <div class="row">
  <div class="col-md-7">
    © 2019 Eden &amp; South Lakeland Credit Union Limited.&nbsp;
    Firm reference number: <a href="https://register.fca.org.uk/ShPo_FirmDetailsPage?id=001b000000NMjvsAAD">587639</a>.<br />
    Authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority.
  </div>

  <div class="col-md-2 text-center">
    <a href="http://www.abcul.coop"> <img src="/sites/default/files/images/abcul.gif" alt="Member of ABCUL" title="Member of ABCUL" style="margin-bottom: 10px;" /></a>
  </div>
  <div class="col-md-3 text-center">
    <a href="http://www.fscs.org.uk/"> <img src="/sites/default/files/FSCS_leader_web_banner_499x121px.png" alt="Financial Services Compensation Scheme" title="Financial Services Compensation Scheme" /></a>
  </div>
</div>

</section>
  </div>
  </footer>
  <script src="https://www.edensouthlakeland.com/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
