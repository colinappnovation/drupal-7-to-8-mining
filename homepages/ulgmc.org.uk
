<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.ulgmc.org.uk/sites/default/files/ULGMC%20bw.ico" type="image/vnd.microsoft.icon" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
  <title>University of London Graduate Mountaineering Club (est. 1950) | climb, walk, run</title>
  <link type="text/css" rel="stylesheet" href="https://www.ulgmc.org.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ulgmc.org.uk/sites/default/files/css/css_a1U_9FZJBVvXnahJdF1y_bhS-jNi4Ug3_ZzFmIX0N3E.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ulgmc.org.uk/sites/default/files/css/css_NCOSdpoQnWWQHVcfG2o2skDLf8YSjSAlOAadPoELzbo.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@3.3.0/yeti/bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@unicorn-fail/drupal-bootstrap-styles@0.0.1/dist/3.3.0/7.x-3.x/drupal-bootstrap-yeti.min.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="https://www.ulgmc.org.uk/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.0/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.0/docs/dist/js/bootstrap.min.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap","theme_token":"vRfQI-jvEgDb14HOU2sJWX09x4FoRHdAkbwxweurREI","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.0\/dist\/js\/bootstrap.min.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.0\/docs\/dist\/js\/bootstrap.min.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/autofloat\/css\/autofloat.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootswatch@3.3.0\/yeti\/bootstrap.min.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/@unicorn-fail\/drupal-bootstrap-styles@0.0.1\/dist\/3.3.0\/7.x-3.x\/drupal-bootstrap-yeti.min.css":1}},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-home">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <header id="navbar" role="banner" class="navbar container navbar-default">
  <div class="container">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="https://www.ulgmc.org.uk/sites/default/files/ULGMC%20col_2.png" alt="Home" />
        </a>
      
              <a class="name navbar-brand" href="/" title="Home">University of London Graduate Mountaineering Club (est. 1950)</a>
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first leaf"><a href="/news">News</a></li>
<li class="collapsed"><a href="/meets" title="">Meets and Socials</a></li>
<li class="collapsed"><a href="/huts" title="Book or fins out more about our club huts">Huts</a></li>
<li class="leaf"><a href="/join" title="">Join</a></li>
<li class="collapsed"><a href="/about" title="">About</a></li>
<li class="last leaf"><a href="/contact" title="">Contact</a></li>
</ul>                                <ul class="menu nav navbar-nav secondary"><li class="first last leaf"><a href="/user/login" title="Access the members-only area by logging in">Log in</a></li>
</ul>                            </nav>
      </div>
      </div>
</header>

<div class="main-container container">

  <header role="banner" id="page-header">
          <p class="lead">climb, walk, run</p>
    
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-9">
                  <a id="main-content"></a>
                                                                <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div class="view view-home view-id-home view-display-id-home view-dom-id-38a3840c009bd77d257c2f2a490d9d78">
        
  
  
  
  
  
  
  
  
</div>
</section>
<section id="block-views-calendar-upcoming" class="block block-views clearfix">

        <h2 class="block-title">Upcoming Meets &amp; Socials</h2>
    
  <div class="view view-calendar view-id-calendar view-display-id-upcoming view-dom-id-bfa5a2680715f6f553303598e1b13031">
        
  
  
      <div class="view-content">
      


<table class="views-table cols-4 table table-striped table-0 table-0 table-0" >
        <thead>
    <tr>
              <th class="views-field views-field-field-date views-align-left" >
                  </th>
              <th class="views-field views-field-title views-align-center" >
                  </th>
              <th class="views-field views-field-field-location views-align-right" >
          <a href="/home?order=field_location&amp;sort=asc" title="sort by " class="active"></a>        </th>
              <th class="views-field views-field-field-organiser" >
                  </th>
          </tr>
    </thead>
    <tbody>
      <tr class="odd views-row-first views-row-last">
              <td class="views-field views-field-field-date views-align-left" >
          <span class="date-display-range"><span class="date-display-start" property="dc:date" datatype="xsd:dateTime" content="2019-12-27T17:00:00+00:00">27 Dec 2019</span> to <span class="date-display-end" property="dc:date" datatype="xsd:dateTime" content="2020-01-02T17:00:00+00:00">02 Jan 2020</span></span>        </td>
              <td class="views-field views-field-title views-align-center" >
          <a href="/event/new-year-meet-2">New Year Meet</a>        </td>
              <td class="views-field views-field-field-location views-align-right" >
          Caseg Fraith, Snowdonia        </td>
              <td class="views-field views-field-field-organiser" >
          Elanor Mott        </td>
          </tr>
    </tbody>
</table>

    </div>
  
  
  
  
  
  
</div>
</section>
<section id="block-views-frontpage-recent" class="block block-views clearfix">

        <h2 class="block-title">Recent News</h2>
    
  <div class="view view-frontpage view-id-frontpage view-display-id-recent view-dom-id-c9488e264eec1e0e4ee8845dd565c2db">
        
  
  
      <div class="view-content">
      


<table class="views-table cols-3 table table-0 table-0 table-0 table-0" >
        <thead>
    <tr>
              <th class="views-field views-field-title views-align-left" >
                  </th>
              <th class="views-field views-field-created views-align-center" >
          <a href="/home?order=created&amp;sort=asc" title="sort by " class="active"></a>        </th>
              <th class="views-field views-field-name views-align-right" >
          <a href="/home?order=name&amp;sort=asc" title="sort by " class="active"></a>        </th>
          </tr>
    </thead>
    <tbody>
      <tr class="odd views-row-first">
              <td class="views-field views-field-title views-align-left" >
          <a href="/news/2018/11/27/expedition-and-training-fund-2019">Expedition and Training Fund 2019</a>        </td>
              <td class="views-field views-field-created views-align-center" >
          27 Nov 2018        </td>
              <td class="views-field views-field-name views-align-right" >
          Stephan Tietz        </td>
          </tr>
      <tr class="even views-row-last">
              <td class="views-field views-field-title views-align-left" >
          <a href="/news/2016/09/21/website-progress">Website Progress</a>        </td>
              <td class="views-field views-field-created views-align-center" >
          21 Sep 2016        </td>
              <td class="views-field views-field-name views-align-right" >
          Stephan Tietz        </td>
          </tr>
    </tbody>
</table>

    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
    </section>

          <aside class="col-sm-3" role="complementary">
          <div class="region region-sidebar-second">
    <section id="block-simplenews-1" class="block block-simplenews clearfix">

        <h2 class="block-title">ULGMC News</h2>
    
        <p>Stay informed on our latest news! (Even if you are not a member.)</p>
  
        
  
  
  
</section>
  </div>
      </aside>  <!-- /#sidebar-second -->
    
  </div>
</div>

  <script src="https://www.ulgmc.org.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
