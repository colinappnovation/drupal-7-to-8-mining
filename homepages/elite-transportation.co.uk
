<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta http-equiv="cleartype" content="on" />
<link href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/images/iphone-retina-120x120.png" rel="apple-touch-icon" sizes="120x120" />
<link href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/images/ipad-retina-152x152.png" rel="apple-touch-icon" sizes="152x152" />
<link href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/images/ipad-standard-76x76.png" rel="apple-touch-icon" sizes="76x76" />
<link href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/images/default-60x60.png" rel="apple-touch-icon" />
<link href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/images/default-60x60.png" rel="shortcut icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="1" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.elite-transportation.co.uk/" />
<link rel="shortlink" href="https://www.elite-transportation.co.uk/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.elite-transportation.co.uk/" />
<meta name="twitter:title" content="Elite Transportation" />
<meta name="twitter:description" content="Peter Banfield" />
<meta itemprop="name" content="Elite Transportation" />
<meta itemprop="description" content="Peter Banfield" />
<link rel="shortcut icon" href="https://www.elite-transportation.co.uk/sites/default/files/favicon.jpg" type="image/jpeg" />
<title>Elite Transportation | Peter Banfield</title>
<style type="text/css" media="all">
@import url("https://www.elite-transportation.co.uk/modules/system/system.base.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/system/system.menus.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/system/system.messages.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/system/system.theme.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/comment/comment.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/date/date_api/date.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/field/theme/field.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/fitvids/fitvids.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/node/node.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/search/search.css?phet5w");
@import url("https://www.elite-transportation.co.uk/modules/user/user.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/views/css/views.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/modules/ctools/css/ctools.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/libraries/superfish/css/superfish.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/libraries/superfish/style/light-blue.css?phet5w");
</style>
<style type="text/css" media="screen">
@import url("https://www.elite-transportation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.floatblocks.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/global.base.css?phet5w");
@import url("https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/global.styles.css?phet5w");
</style>
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/generated_files/at_elitetransportation.responsive.layout.css?phet5w" media="only screen" />
<style type="text/css" media="screen">
@import url("https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/generated_files/at_elitetransportation.fonts.css?phet5w");
</style>
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.custom.css?phet5w" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.smalltouch.portrait.css?phet5w" media="only screen and (max-width:320px)" />
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.smalltouch.landscape.css?phet5w" media="only screen and (min-width:321px) and (max-width:580px)" />
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.tablet.portrait.css?phet5w" media="only screen and (min-width:581px) and (max-width:768px)" />
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.tablet.landscape.css?phet5w" media="only screen and (min-width:769px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/css/responsive.desktop.css?phet5w" media="only screen and (min-width:1025px)" />

<!--[if lt IE 9]>
<style type="text/css" media="screen">
@import url("https://www.elite-transportation.co.uk/sites/all/themes/at_elitetransportation/generated_files/at_elitetransportation.lt-ie9.layout.css?phet5w");
</style>
<![endif]-->
<script type="text/javascript" src="https://www.elite-transportation.co.uk/sites/default/files/js/js_R79Hwes5NOYgbbpT_-6wI_cdwYFLOIKuZ_mQrEfhTDs.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-205628-109", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.elite-transportation.co.uk/sites/default/files/js/js_wRtN3XmVvMZm0RUtOswNbVsr44EjNWcB2yDmgjqZDiI.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_elitetransportation","theme_token":"d_sJhkJpVD5lspYajKf0dKhAQO5H92LdUTPlYqvz9Jo","js":{"0":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/fitvids\/jquery.fitvids.js":1,"sites\/all\/modules\/fitvids\/fitvids.js":1,"sites\/all\/modules\/fit_text\/jquery.fittext.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/modules\/backstretch\/js\/jquery.backstretch.min.js":1,"sites\/all\/modules\/backstretch\/js\/backstretch.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/scalefix.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/matchMedia.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/matchMedia.addListener.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/light-blue.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.floatblocks.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/global.base.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/global.styles.css":1,"sites\/all\/themes\/at_elitetransportation\/generated_files\/at_elitetransportation.responsive.layout.css":1,"sites\/all\/themes\/at_elitetransportation\/generated_files\/at_elitetransportation.fonts.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.custom.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.smalltouch.portrait.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.smalltouch.landscape.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/at_elitetransportation\/css\/responsive.desktop.css":1,"sites\/all\/themes\/at_elitetransportation\/generated_files\/at_elitetransportation.lt-ie9.layout.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/search\/node":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"fast"},"plugins":{"supposition":true,"supersubs":true}}},"backstretch":{"node-1":{"items":["https:\/\/www.elite-transportation.co.uk\/sites\/default\/files\/styles\/supersized\/public\/new_main.jpg?itok=hNTaQEQx"]}},"adaptivetheme":{"at_elitetransportation":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:581px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:580px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});
//--><!]]>
</script>
<!--[if lt IE 9]>
<script src="https://www.elite-transportation.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?phet5w"></script>
<![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page atr-7.x-3.x atv-7.x-3.2+15-dev site-name-elite-transportation">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page snc-n snw-b sna-l sns-n ssc-n ssw-b ssa-l sss-n btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ptc-n ptw-b pta-l pts-n">

    
    <div id="header-wrapper">
      <div class="container clearfix">
        <header id="header" class="clearfix" role="banner">

                      <!-- start: Branding -->
            <div id="branding" class="branding-elements clearfix">

              
                              <!-- start: Site name and Slogan -->
                <div class="h-group" id="name-and-slogan">

                                      <h1 id="site-name"><a href="/" title="Home page" class="active">Elite Transportation</a></h1>
                  
                                      <h2 id="site-slogan">Peter Banfield</h2>
                  
                </div><!-- /end #name-and-slogan -->
              

            </div><!-- /end #branding -->
          
        <div class="region region-header"><div class="region-inner clearfix"><div id="block-block-4" class="block block-block no-title odd first last block-count-1 block-region-header block-4" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><a href="tel:07766167353">07766 167353</a></div>
  </div></div></div></div>
        </header>
      </div>
    </div>

          <div id="nav-wrapper">
        <div class="container clearfix">
          <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish no-title menu-wrapper menu-bar-wrapper clearfix odd first last block-count-2 block-region-menu-bar block-1" >  
  
  <ul  id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-light-blue sf-total-items-3 sf-parent-items-0 sf-single-items-3"><li id="menu-219-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-592-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/about" class="sf-depth-1">About</a></li><li id="menu-593-1" class="last odd sf-item-3 sf-depth-1 sf-no-children"><a href="/contact" class="sf-depth-1">Contact</a></li></ul>
  </nav></div>                            </div>
      </div>
    
    
    
    
    <div id="content-wrapper"><div class="container">
      <div id="columns"><div class="columns-inner clearfix">
        <div id="content-column"><div class="content-inner">

          
          <section id="main-content">

            
            
                          <div id="content">
                <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-1" class="node node-page article odd node-full clearfix" about="/home" typeof="foaf:Document" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-field-bg-image field-type-image field-label-hidden view-mode-full"><div class="field-items"><figure class="clearfix field-item even"></figure></div></div>  </div>

  
  
  <span property="dc:title" content="" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span></article>

  </div>              </div>
            
            
            
          </section>

          
        </div></div>

                
      </div></div>
    </div></div>

    
          <div id="footer-wrapper">
        <div class="container clearfix">
          <footer id="footer" class="clearfix" role="contentinfo">
            <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-1" class="block block-block no-title odd first block-count-4 block-region-footer block-1" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p><a href="tel:+447766167353">07766 167353</a></p>
</div>
  </div></div><div id="block-block-2" class="block block-block no-title even last block-count-5 block-region-footer block-2" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p>Developed by <a href="http://www.wiredmonkeys.com" target="_BLANK">WiredMonkeys Ltd</a><br />
	©&nbsp;Elite Transportation</p>
</div>
  </div></div></div></div>                      </footer>
        </div>
      </div>
    
  </div>
</div>
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
// Output by Fit Text module
jQuery("#site-name").fitText(1.4, {minFontSize:  '28px', maxFontSize: '74px' });
jQuery("#site-slogan").fitText(1, {minFontSize:  '26px', maxFontSize: '60px' });
jQuery("#block-block-4").fitText(1, {minFontSize:  '30px', maxFontSize: '60px' });
jQuery("#block-block-1 p").fitText(1, {minFontSize:  '16px', maxFontSize: '80px' });
jQuery("#node-2 .field-name-body p").fitText(2.4, {minFontSize:  '8px', maxFontSize: '30px' });
jQuery("#node-3 .field-name-body p").fitText(2.4, {minFontSize:  '12px', maxFontSize: '30px' });
jQuery("#node-2 .field-name-body table").fitText(2.4, {minFontSize:  '8px', maxFontSize: '28px' });
jQuery("#block-block-3 p").fitText(2, {minFontSize:  '6px', maxFontSize: '24px' });

//--><!]]>
</script>
</body>
</html>
