<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Upward Media stands for quality combined with unconventional thinking. Specialised in web design and development, we offer professional and custom web solutions. Furthermore, we film events and provide assistance with the production of short films, documentaries, promotional videos and music videos. At Upward Media, we strive for excellence in every aspect of our work." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.upwardmedia.co.uk/frontpage" />
<link rel="shortlink" href="https://www.upwardmedia.co.uk/node/150" />
<meta property="og:site_name" content="Upward Media" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.upwardmedia.co.uk/frontpage" />
<meta property="og:title" content="Upward Media - Web Design &amp; Film Production" />
<meta property="og:updated_time" content="2017-08-28T16:39:46+01:00" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.upwardmedia.co.uk/frontpage" />
<meta name="twitter:title" content="Upward Media - Web Design &amp; Film Production" />
<meta property="article:published_time" content="2013-07-01T15:31:13+01:00" />
<meta property="article:modified_time" content="2017-08-28T16:39:46+01:00" />
<meta itemprop="name" content="Upward Media - Web Design &amp; Film Production" />
<meta name="dcterms.title" content="Upward Media - Web Design &amp; Film Production" />
<meta name="dcterms.creator" content="STO" />
<meta name="dcterms.date" content="2013-07-01T15:31+01:00" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.format" content="text/html" />
<meta name="dcterms.identifier" content="https://www.upwardmedia.co.uk/frontpage" />
<link rel="shortcut icon" href="http://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_bootstrap/favicon.ico" type="image/ico" />
<meta name="dcterms.modified" content="2017-08-28T16:39+01:00" />
  <title>Upward Media - Web Design &amp; Film Production</title>
  <style>
@import url("https://www.upwardmedia.co.uk/modules/system/system.base.css?p45n5i");
</style>
<style>
@import url("https://www.upwardmedia.co.uk/sites/upwardmedia/modules/simplenews/simplenews.css?p45n5i");
@import url("https://www.upwardmedia.co.uk/modules/field/theme/field.css?p45n5i");
@import url("https://www.upwardmedia.co.uk/sites/all/modules/views/css/views.css?p45n5i");
@import url("https://www.upwardmedia.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?p45n5i");
</style>
<style>
@import url("https://www.upwardmedia.co.uk/sites/all/modules/ctools/css/ctools.css?p45n5i");
</style>
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.css" media="all" />
<style>
@import url("https://www.upwardmedia.co.uk/sites/upwardmedia/themes/bootstrap/css/3.3.7/overrides.min.css?p45n5i");
@import url("https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/css/style.css?p45n5i");
@import url("https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/css/newsticker.css?p45n5i");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.upwardmedia.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.upwardmedia.co.uk/misc/drupal.js?p45n5i"></script>
<script src="//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="https://www.upwardmedia.co.uk/sites/all/libraries/colorbox/jquery.colorbox-min.js?p45n5i"></script>
<script src="https://www.upwardmedia.co.uk/sites/all/modules/colorbox/js/colorbox.js?p45n5i"></script>
<script src="https://www.upwardmedia.co.uk/sites/all/modules/colorbox/js/colorbox_load.js?p45n5i"></script>
<script src="https://www.upwardmedia.co.uk/sites/all/modules/colorbox/js/colorbox_inline.js?p45n5i"></script>
<script src="https://www.upwardmedia.co.uk/sites/all/modules/google_analytics/googleanalytics.js?p45n5i"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-33341301-1", {"cookieDomain":"auto","allowLinker":true});ga("require", "linker");ga("linker:autoLink", ["upwardmedia.co.uk","upwardmedia.de","horizon-media.net","horizon-media.de"]);ga("send", "pageview");</script>
<script src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/scripts/newsticker.jquery.min.js?p45n5i"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"upwardmedia_responsive_bootstrap","theme_token":"8RJOTcUOBaMHWqO7TrBYUYpiEv3Vf180yTlrBDxqWc4","js":{"sites\/upwardmedia\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/code.jquery.com\/jquery-1.10.2.min.js":1,"0":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.7\/js\/bootstrap.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/scripts\/newsticker.jquery.min.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/upwardmedia\/modules\/simplenews\/simplenews.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.7\/css\/bootstrap.css":1,"sites\/upwardmedia\/themes\/bootstrap\/css\/3.3.7\/overrides.min.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/reset.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/style.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/node.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/pages.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/gallery.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/newsticker.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/portfolio.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/colorbox.css":1,"sites\/upwardmedia\/themes\/upwardmedia_responsive_bootstrap\/css\/content.css":1}},"colorbox":{"transition":"elastic","speed":"350","opacity":"0.85","slideshow":false,"slideshowAuto":true,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","overlayClose":true,"returnFocus":true,"maxWidth":"95%","maxHeight":"95%","initialWidth":"300","initialHeight":"100","fixed":true,"scrolling":false,"mobiledetect":true,"mobiledevicewidth":"480px"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1,"trackDomainMode":2,"trackCrossDomains":["upwardmedia.co.uk","upwardmedia.de","horizon-media.net","horizon-media.de"]},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":"1","popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":"1","tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-150 node-type-page i18n-en">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <!--<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>-->

<div class="container">

  <!--<div class="header">-->
  <header id="navbar">
  
	 <a class="navbar-brand hidden-xs" href="https://www.upwardmedia.co.uk"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/logo.svg" alt="Upward Media" /></a>

	  <div id="icons" class="hidden-xs">    
		<ul class="nav navbar-nav navbar-right">
		   <li><a href="http://www.facebook.com/upwardmediauk" target="_blank"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/buttons/facebook.svg" width="25" height="25" alt="Facebook" /></a></li>
		   <li><a href="https://twitter.com/upwardmediauk" target="_blank"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/buttons/twitter.svg" width="25" height="25" alt="Follow us on Twitter" /></a></li>
		   <li><a href="https://vimeo.com/upwardmedia" target="_blank"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/buttons/vimeo.svg" width="25" height="25" alt="Visit us on Vimeo" /></a></li>
		   <li><a href="/rss.xml"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/buttons/rss.svg" width="25" height="25" alt="Subscribe to our RSS feed" /></a></li>
		</ul>
	  </div> 
  
  <!--</div>--> 	
  </header>	
 	
 			
  <nav class="navbar navbar-default" id="sidebar-nav">
  
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      
      <!-- Logo on mobile devices -->
      <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="https://www.upwardmedia.co.uk"><img src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/logo.svg" alt="Upward Media" /></a>
      
      <!-- Place brand here to: Brand and toggle get grouped for better mobile display -->
    </div>
    
    <!--<p id="language"><a href="/deutsche-version">deutsche Version</a></p>-->
   
    <!-- Collect the nav links, forms, and other content for toggling -->
    <!--<div class="collapse navbar-collapse navbar-right" id="defaultNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#">News</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Services<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
        	<li><a href="#">Web Design</a></li>
        	<li><a href="#">Film Production</a></li>
          </ul>
        </li>
        <li><a href="#">About</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Portfolio<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
        	<li><a href="#">Film Production</a></li>
        	<li><a href="#">Web Design</a></li>
          </ul>
		</li>
        <li><a href="#">Contact</a></li>        
      </ul>
    </div>--><!-- /.navbar-collapse --> 


		 	<div class="collapse navbar-collapse navbar-right" id="defaultNavbar">
		<!--<div id="navi">-->
			  <div class="region region-navigation">
    <section id="block-block-4" class="block block-block clearfix">

      
  <p id="language"><a href="/deutsche-version">deutsche Version</a></p>
</section>
<section id="block-system-main-menu" class="block block-system block-menu clearfix">

      
  <ul class="menu nav"><li class="first leaf"><a href="/news" title="News"><img alt = "News" src = "/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/menu/news.svg" /><span>News</span></a></li>
<li class="expanded"><a href="/services" title="Services"><img alt = "Services" src = "/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/menu/services.svg" /><span>Services</span></a><ul class="menu nav"><li class="first leaf"><a href="/services/webdesign" title="Web Design">Web Design</a></li>
<li class="last leaf"><a href="/services/filmproduction" title="Film Production">Film Production</a></li>
</ul></li>
<li class="leaf"><a href="/about" title="About"><img alt = "About" src = "/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/menu/about.svg" /><span>About</span></a></li>
<li class="expanded"><a href="/portfolio" title="Portfolio"><img alt = "Portfolio" src = "/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/menu/portfolio.svg" /><span>Portfolio</span></a><ul class="menu nav"><li class="first leaf"><a href="/portfolio/film" title="Film Production">Film Production</a></li>
<li class="last leaf"><a href="/portfolio/webdesign" title="Web Design">Web Design</a></li>
</ul></li>
<li class="last leaf"><a href="/contact" title="Contact"><img alt = "Contact" src = "/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/menu/contact.svg" /><span>Contact</span></a></li>
</ul>
</section>
  </div>
		<!--</div>--><!--end navi-->
		</div><!-- /.navbar-collapse --> 
	  
       
                     
  <!--</div>--><!-- /.container --> 
</nav>
       
     
<!--<div class="container">-->
<div class="container-fluid" id="content-wrapper">  
   <div class="row">       
       
        
        <!--<div id="main" class="content">-->
        
			<!--<div id="content-wrapper" class="row">-->
           	
           		<section>
           		
					                
                	<!--<div id="content-main">-->

                                        	
                     
					               
                      
                                              <div class="tabs">
                                                  </div>
                                            
                                            
                                                                    <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-150" class="node node-page clearfix" about="/frontpage" typeof="foaf:Document">
    <header>
            <span property="dc:title" content="Upward Media - Web Design &amp; Film Production" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even first" property="content:encoded"><div class="col-sm-3" id="slogan">
	<img alt="The sky is the limit in terms of possibilities" src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/slogan.svg" style="width: 160px; height: 107px;" />
</div>

<div class="col-sm-9 hidden-xs" id="introduction">
	<p>Upward Media stands for quality combined with unconventional thinking. Specialised in web design and development, we offer professional and custom web solutions. Furthermore, we film events and provide assistance with the production of short films, documentaries, corporates and promotional videos.<br />
	At Upward Media, we strive for excellence in every aspect of our work.</p>
</div>

<div class="col-sm-12" id="slideshow">
	<img alt="Web Design &amp; Development" src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/front-slide-01.jpg" class="img-responsive hidden-xs" />
	<img alt="Web Design &amp; Development" src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/upwardmedia_responsive_bootstrap/images/front-slide-01-xs.jpg" class="img-responsive visible-xs" />
	<h3>Web Design &amp; Development</h3>
	<h4>Concept, Design and Implementation</h4>
</div>

<div class="col-sm-9 visible-xs" id="introduction">
        <h2>The sky is the limit in terms of possibilities</h2>
	<p>Upward Media stands for quality combined with unconventional thinking. Specialised in web design and development, we offer professional and custom web solutions. Furthermore, we film events and provide assistance with the production of short films, documentaries, corporates and promotional videos.</p>
</div>

<div class="col-sm-6 services">
	<h2>Website Creation Services</h2>
	<p>Whether you are aiming for an online portfolio or a more complex website, we are committed to developing successful business solutions.</p>
	<p>Some of our services include:</p>
	<ul>
		<li>Website planning, design and build</li>
		<li>Redesign of existing websites</li>
		<li>Integration of content management systems <span class="hidden-xs">(CMS) </span>such as WordPress or Drupal</li>
		<li>Setup of online shop systems</li>
	</ul>
	<p class="more"><a href="https://www.upwardmedia.co.uk/services/webdesign">&raquo; more information</a></p>
</div>

<div class="col-sm-6 services">
	<h2>Live Event Coverage</h2>
	<p>From single camera solutions to large-scale multi-camera setups with live broadcasts, we provide full coverage of live events including video editing.</p>
	<p class="more"><a href="https://www.upwardmedia.co.uk/live-event-coverage">&raquo; overview of services</a></p>
	<h2>Film and Video Production Services</h2>
	<p>We help producers and directors achieve their vision by offering a wide range of production services, including crewing and equipment rental.</p>
	<p class="more"><a href="https://www.upwardmedia.co.uk/services/filmproduction">&raquo; overview of services</a></p>
</div></div></div></div>    </article>

</section>
<section id="block-views-tweets-block" class="block block-views clearfix">

        <h2 class="block-title"><a href="https://twitter.com/upwardmediauk" target="_blank">Twitter Feed</a></h2>
    
  <div class="view view-tweets view-id-tweets view-display-id-block view-dom-id-7569e593d878a7a16d341d3d47fe210b">
        
  
  
      <div class="view-content">
      
<!--<script id="newsticker-pro" src="/scripts/newsticker.jquery.min.js"></script>-->

<script>
	(function($){
		$(document).ready(function() {
			$('#newsticker').newsticker();
		});
	})(jQuery);
</script>

<div class="newsticker">      <ul id="newsticker">
          <li class="">  
  <span class="views-field views-field-text">        <span class="field-content">STATE OF MIND (written by Pia Storck) has made it into the ScreenCraft 2018 Spring Film Fund quarterfinals: <a href="https://t.co/3jkr42qBav">https://t.co/3jkr42qBav</a></span>  </span>       &#8212;   
  <span class="views-field views-field-created-time">        <span class="field-content"><em class="placeholder">1 year 3 months</em> ago</span>  </span></li>
          <li class="">  
  <span class="views-field views-field-text">        <span class="field-content">On 26th April, we were filming the Music Week Awards 2018 <a target="_blank" rel="nofollow" class="twitter-timeline-link" href="https://twitter.com/search?q=%23filmmaking" class="twitter-hashtag">#filmmaking</a> <a target="_blank" rel="nofollow" class="twitter-timeline-link" href="https://twitter.com/search?q=%23events" class="twitter-hashtag">#events</a> <a href="https://t.co/qUvNlQ8DWi">https://t.co/qUvNlQ8DWi</a></span>  </span>       &#8212;   
  <span class="views-field views-field-created-time">        <span class="field-content"><em class="placeholder">1 year 6 months</em> ago</span>  </span></li>
          <li class="">  
  <span class="views-field views-field-text">        <span class="field-content">One of the vox pops from today filmed and edited by Pia for Earthy Photography at the Frontiers of Engineering for… <a href="https://t.co/zZEZAxxHzc">https://t.co/zZEZAxxHzc</a></span>  </span>       &#8212;   
  <span class="views-field views-field-created-time">        <span class="field-content"><em class="placeholder">1 year 7 months</em> ago</span>  </span></li>
          <li class="">  
  <span class="views-field views-field-text">        <span class="field-content">On the train to Manchester again to do some more filming... <a target="_blank" rel="nofollow" class="twitter-timeline-link" href="https://twitter.com/search?q=%23filmmaking" class="twitter-hashtag">#filmmaking</a></span>  </span>       &#8212;   
  <span class="views-field views-field-created-time">        <span class="field-content"><em class="placeholder">1 year 7 months</em> ago</span>  </span></li>
          <li class="">  
  <span class="views-field views-field-text">        <span class="field-content">New toy arrived just in time for yesterday's shoot:Edelkrone SliderONE Pro, a portable and motorized slider which… <a href="https://t.co/SD7vLzQapl">https://t.co/SD7vLzQapl</a></span>  </span>       &#8212;   
  <span class="views-field views-field-created-time">        <span class="field-content"><em class="placeholder">1 year 8 months</em> ago</span>  </span></li>
        </ul>
</div>    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
                      					  	                
                       
                      
           		</section> 
                           
   </div><!--/.row-->
</div><!--/.container-fluid--> 

<div class="container" id="footer">
  <div class="row">
    <div class="text-center col-sm-12">
      <p>Copyright &copy; 2019 Upward Media. All rights reserved.</p>
    </div>
  </div>
</div><!-- /.footer-->      
       
</div><!-- /.container -->


<!--<script src="/scripts/modernizr-custom.js"></script>
<script src="/scripts/custom.js"></script> -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!--<script src="/js/jquery-1.11.3.min.js"></script>-->

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<!--<script src="/js/bootstrap.js"></script>-->

<script>
	/* Disable Image Title on Hover */
	/*jQuery('document').ready(function($){
		$('img').parent('a[title]').removeAttr('title');
	});*/
</script>  <script src="https://www.upwardmedia.co.uk/sites/upwardmedia/themes/bootstrap/js/bootstrap.js?p45n5i"></script>
</body>
</html>
