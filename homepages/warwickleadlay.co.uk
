<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="shortlink" href="/node/4" />
<link rel="canonical" href="/home" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
  <title>Warwick Leadlay Gallery | antique maps - prints - fine art - framing</title>
  <!--
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  -->
  <style type="text/css" media="all">
@import url("https://www.warwickleadlay.co.uk/modules/system/system.base.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/system/system.menus.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/system/system.messages.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/system/system.theme.css?p6ck3p");
</style>
<style type="text/css" media="all">
@import url("https://www.warwickleadlay.co.uk/modules/comment/comment.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/field/theme/field.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/node/node.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/search/search.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/modules/user/user.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/views/css/views.css?p6ck3p");
</style>
<style type="text/css" media="all">
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/ckeditor/ckeditor.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/colorbox/styles/default/colorbox_default_style.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/ctools/css/ctools.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/dhtml_menu/dhtml_menu.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/modules/custom_search/custom_search.css?p6ck3p");
</style>
<style type="text/css" media="all">
@import url("https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/css/reset.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/css/style.css?p6ck3p");
@import url("https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/css/navigation.css?p6ck3p");
</style>
  <script type="text/javascript" src="https://www.warwickleadlay.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/misc/drupal.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/misc/form.js?v=7.59"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/libraries/colorbox/colorbox/jquery.colorbox-min.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/modules/colorbox/js/colorbox.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/modules/colorbox/styles/default/colorbox_default_style.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/modules/dhtml_menu/dhtml_menu.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/modules/custom_search/js/custom_search.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/modules/google_analytics/googleanalytics.js?p6ck3p"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-35151311-1"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/js/jquery.dumbcrossfade.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/js/fullscreenbg.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/js/goBack.js?p6ck3p"></script>
<script type="text/javascript" src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/js/init.js?p6ck3p"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"wlg_2012","theme_token":"PICp_VXbz8dHnMoAUNS2zzvN7aVnfDjxIPDTKdJiAr0","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/form.js":1,"sites\/all\/libraries\/colorbox\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_default_style.js":1,"sites\/all\/modules\/dhtml_menu\/dhtml_menu.js":1,"sites\/all\/modules\/custom_search\/js\/custom_search.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/wlg_2012\/js\/jquery.dumbcrossfade.js":1,"sites\/all\/themes\/wlg_2012\/js\/fullscreenbg.js":1,"sites\/all\/themes\/wlg_2012\/js\/goBack.js":1,"sites\/all\/themes\/wlg_2012\/js\/init.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_default_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/dhtml_menu\/dhtml_menu.css":1,"sites\/all\/modules\/custom_search\/custom_search.css":1,"sites\/all\/themes\/wlg_2012\/css\/reset.css":1,"sites\/all\/themes\/wlg_2012\/css\/style.css":1,"sites\/all\/themes\/wlg_2012\/css\/navigation.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"100%","maxHeight":"100%","fixed":true,"__drupal_alter_by_ref":["default"]},"dhtmlMenu":{"nav":"none","animation":{"effects":{"height":"height","opacity":"opacity","width":0},"speed":"500"},"effects":{"siblings":"close-same-tree","children":"none","remember":"0"},"filter":{"type":"blacklist","list":{"devel":0,"main-menu":0,"management":0,"navigation":0,"shortcut-set-1":0,"user-menu":0}}},"custom_search":{"form_target":"_self","solr":0},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-4 node-type-home-page" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
    <div id="background-image">
      <img typeof="foaf:Image" src="https://www.warwickleadlay.co.uk/sites/default/files/styles/background_main/public/home_images/Parrot-for-web.jpg?itok=AYAdxx1k" alt="" />    </div>
 


<div id="page-container">  

     <div id="header">
      
        <div id="left-header">
        
        
            <a href="/" title="Home" rel="home">
            <img src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/images/logoSmall.png" alt="Home" />
            </a>
            
              <div class="region region-header">
    <div id="block-search-form" class="block block-search">

    
  <div class="content">
    <form class="search-form" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <input class="custom-search-default-value custom-search-box form-text" type="text" id="edit-search-block-form--2" name="search_block_form" value="search" size="15" maxlength="128" />
</div>
<fieldset class="custom_search-popup form-wrapper" id="edit-popup"><div class="fieldset-wrapper"></div></fieldset>
<div class="form-actions form-wrapper" id="edit-actions"><input alt="" class="custom-search-button form-submit" type="image" id="edit-submit" name="op" value="" src="https://www.warwickleadlay.co.uk/sites/default/files/custom_search/searchbutton.gif" /></div><input type="hidden" name="form_build_id" value="form-rePNEnfl5hVkZszTIq2bcFm2sf4_vEcvdRYDae-NAEg" />
<input type="hidden" name="form_id" value="search_block_form" />
<input class="default-text" type="hidden" name="default_text" value="search" />
</div>
</div></form>  </div>
</div>
  </div>
        </div>
        
                
        <div id="right-header">
        <a href="/" title="Home" rel="home" id="logo">
             <img src="https://www.warwickleadlay.co.uk/sites/all/themes/wlg_2012/logo.png" alt="Home" />	
        </a>
        </div>
            
      </div> <!--end header-->
      
      
      <div id="main-content">
      
      
            
        <div id="sidebar" class="column sidebar">
        
            <div class="region region-sidebar-first">
    <div id="block-system-main-menu" class="block block-system block-menu">

    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/about">about</a></li>
<li class="leaf"><a href="/blog" title="">blog</a></li>
<li class="collapsed"><a href="/" title="" class="active">maps</a></li>
<li class="collapsed"><a href="/" title="" class="active">prints</a></li>
<li class="leaf"><a href="/contemporary-art">contemporary</a></li>
<li class="leaf"><a href="/curios">curios</a></li>
<li class="leaf"><a href="/bronzes">bronzes</a></li>
<li class="leaf"><a href="/books">books</a></li>
<li class="leaf"><a href="/framing">framing</a></li>
<li class="leaf"><a href="/payment">payment</a></li>
<li class="last leaf"><a href="/contact">contact</a></li>
</ul>  </div>
</div>
  </div>
        </div> <!-- end #sidebar-first -->
            
      
      <div id="content">
            <!--[if lt IE 7]>
                <div id="ie6">
                    <div id="alert">
                        <p>Sorry, but it looks like you are using an old version of Internet Explorer, which is not compatible with our website.</p>
                        <p>Please consider <a href="http://www.microsoft.com/windows/internet-explorer/">upgrading</a> to the latest version.</p>
                    </div>
                </div>
                <![endif]-->
            
                        
                            <div class="tabs">
                                  </div>
                        
                      
            
                  
              <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    
<div id="node-4" class="node node-home-page node-promoted clearfix"  about="/home" typeof="sioc:Item foaf:Document" >

		  	  
          
          				
		
                
			
</div>
  </div>
</div>
  </div>
            
      
      </div> <!-- end #content -->
      
            
      
      </div>
    
      <div id="push"></div>
  
</div> 


 <div id="footer" class="clearfix">
  
        <div class="region region-footer">
    <div id="block-block-1" class="block block-block">

    
  <div class="content">
    <p>© Warwick Leadlay Gallery | 1-2 Nelson Arcade, Greenwich, London SE10 9JB | t: 020 8858 0317 e: <a href="mailto:info@warwickleadlay.com">info@warwickleadlay.com</a> | site: <a href="http://www.bumblebeedesign.co.uk" target="_blank">bumblebee</a></p>
  </div>
</div>
  </div>
      <!--<div id="top-link"><a href="#skip-link">back to top</a></div>-->
  </div> <!-- /#footer -->
  
<!--end page-->
  </body>
</html>
