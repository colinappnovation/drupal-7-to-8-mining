<!doctype html>
<html lang="EN"><head><title>HR website</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="robots" content="index,follow"><meta name="description" content="Information relating to Human Resources at UHB.">
<meta name="keywords" content="Human Resources">
<link rel="StyleSheet" href="/WelcomeStyle.aspx.siteid-2675.ctid-6627.minify-true.css" type="text/css"><meta name="google-site-verification" content="7JKqIH0JcEXxWySr4tvMvRmK_eJAR3E3lHTxfsfHbZ0" />
<meta name="language" content="english" />
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<link rel="shortcut icon" type="image/ico" href="/layout/images/hr/favicon.ico" />

<link type="text/css" rel="stylesheet" href="https://fast.fonts.net/cssapi/5db9a6cd-832d-4083-9a28-9b10c26ba200.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/layout/script/hr/hr.js"></script>
</head><body>
<script>(function(s,i,t,e,k,I,T){s['GoogleAnalyticsObject']=k;s[k]=s[k]||function(){(s[k].q=s[k].q||[]).push(arguments)},s[k].l=1*new Date();I=i.createElement(t),T=i.getElementsByTagName(t)[0];I.async=1;I.src=e;T.parentNode.insertBefore(I,T)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-9295558-2');ga('send','pageview');</script><div id="top"></div>

<div id="down-box"><a href="#vision" id="down"><span class="scroll-parts"></span>Scroll down</a></div>

<div id="top-bar"><!-- navigation and search -->


    <div id="search-wrapper"><!-- search -->
      <div id="search">
        <div id="hr-search" class="hr-search"><form action="/hr/search-results.htm" method="get" role="search"><input type="hidden" name="sitekit" value="true" /><input type="hidden" name="task" value="search" /><input type="hidden" name="indexname" value="hr-search" /><label for="SKSearchBox" class="SKSearchLabel"></label><input type="search" class="SKSearchTextbox" id="SKSearchBox" name="search" placeholder="" value="Search" /><input type="submit" class="SKSearchButton" value="Search" /></form></div><script type="application/ld+json">{"@context":"http://schema.org","@type":"WebSite","potentialAction":{"@type":"SearchAction","query-input":"required","target":"https://www.uhb.nhs.uk/hr/search-results.htm?sitekit=true&task=search&indexname=hr-search&search={query}"}}</script>
      </div> 
    </div><!-- /search -->


    <nav id="main-nav"><!-- nav -->

      <ul id="menu">
        <li><a href="/hr/">Home</a></li>
        <li><a href="/hr/employee-relations.htm">Employee relations</a></li>
        <li><a href="/hr/worklife-balance.htm">Work/life balance</a></li>
        <li><a href="/hr/recruitment-and-ongoing-employment.htm">Recruitment and ongoing employment</a></li>
        <li><a href="/hr/pay.htm">Pay</a></li>
        <!-- <li><a href="/hr/news-and-events.htm">News and events</a></li> -->
      </ul>

      <ul id="sub-menu">
        <li><a href="/hr/contact-hr.htm">Contact HR</a></li>
        <li><a href="/hr/faqs.htm">FAQs</a></li>
        <!-- <li><a href="/hr/videos.htm">Videos</a></li> -->
        <li><a href="/hr/useful-links.htm">Useful links</a></li>
      </ul>

    </nav><!-- /nav -->


</div><!-- /navigation and search -->


	<header><!-- header -->
    	<div id="header-inner">
        	<div id="logo"><!-- logo -->
	<img src="https://www.uhb.nhs.uk/layout/images/uhb-logo-stacked-264x124-colour.svg" alt="University Hospitals Birmingham NHS Foundation Trust" id="uhb-small">
	<img src="https://www.uhb.nhs.uk/layout/images/uhb-logo-single-720x144-colour.svg" alt="University Hospitals Birmingham NHS Foundation Trust" id="uhb-large">   
</div><!-- /logo -->
            <h1 class="header-home"><span class="header-dbop">Defined by our people</span><span class="header-hr">HR</span></h1>
        </div>
    </header><!-- /header -->


<div class="home-box-wrap"><!-- boxes -->

  <div class="home-box-row"><!-- box row -->

    <div class="home-box" id="box-employee"><!-- box employee-->
      <div class="home-box-inner">
        <h2>Employee relations</h2>
        <p>Maintaining or restoring positive working relationships between staff and managers.</p>
        <ul>
          <li><a href="/hr/employee-relations.htm" class="read-more-link"><span class="read-more-text">Employee relations</span></a></li>
        </ul>
      </div>
    </div><!-- /box -->

    <div class="home-box" id="box-work-life"><!-- box work/life-->
      <div class="home-box-inner">
        <h2>Work/life balance</h2>
        <p>Working together to find a healthy work-life balance which works for everyone.</p>
        <ul>
          <li><a href="/hr/worklife-balance.htm" class="read-more-link"><span class="read-more-text">Work/life balance</span></a></li>
        </ul>
      </div>
    </div><!-- /box -->

  </div>

  <div class="home-box-row"><!-- box row -->

    <div class="home-box" id="box-recruit"><!-- box recruitment -->
      <div class="home-box-inner">
        <h2>Recruitment and ongoing employment</h2>
        <p>Helping staff and managers through all stages of recruitment and ongoing retention.</p>
        <ul>
          <li><a href="/hr/recruitment-and-ongoing-employment.htm" class="read-more-link"><span class="read-more-text">Recruitment and ongoing employment</span></a></li>
        </ul>
      </div>
    </div><!-- /box -->

    <div class="home-box" id="box-pay"><!-- box pay -->
      <div class="home-box-inner">
        <h2>Pay</h2>
        <p>Guidance for staff and managers on pay transparency for fairness, consistency and equality.</p>
        <ul>
          <li><a href="/hr/pay.htm" class="read-more-link"><span class="read-more-text">Pay</span></a></li>
        </ul>
      </div>
    </div><!-- /box -->

  </div><!-- /boxes -->

</div>	


  
  	<footer class="footer-home-interim">
      
  		<div id="vision"><img src="https://www.uhb.nhs.uk/layout/images/trust-vision-800x184-colour.svg" alt="Building healthier lives"></div>
  		<div id="copyright"><p>&copy; 2019 University Hospitals Birmingham NHS Foundation Trust</p></div>
    
	</footer>





<div id="up-box"><a href="#top" id="up"><span class="scroll-parts"></span>Back to top</a></div></body></html>