<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="LAMBDA Mediation RSS" href="https://lambda-mediation.co.uk/rss.xml" />
<link rel="shortcut icon" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, viewport-fit=cover" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>LAMBDA Mediation | Bringing resolution to people in dispute</title>
<style>
@import url("https://lambda-mediation.co.uk/modules/system/system.base.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/system/system.menus.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/system/system.messages.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/system/system.theme.css?p3s6mr");
</style>
<style>
@import url("https://lambda-mediation.co.uk/modules/field/theme/field.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/node/node.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/search/search.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/modules/user/user.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/modules/views/css/views.css?p3s6mr");
</style>
<style>
@import url("https://lambda-mediation.co.uk/sites/all/modules/ckeditor/ckeditor.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/modules/ctools/css/ctools.css?p3s6mr");
</style>
<style media="screen">
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.image.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.floatblocks.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/global.base.css?p3s6mr");
@import url("https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/global.styles.css?p3s6mr");
</style>
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/default/files/adaptivetheme/at_circles_files/at_circles.responsive.layout.css?p3s6mr" media="only screen" />
<style media="screen">
@import url("https://lambda-mediation.co.uk/sites/default/files/adaptivetheme/at_circles_files/at_circles.fonts.css?p3s6mr");
</style>
<style>
@import url("https://lambda-mediation.co.uk/sites/default/files/adaptivetheme/at_circles_files/at_circles.menutoggle.css?p3s6mr");
</style>
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.custom.css?p3s6mr" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.smalltouch.portrait.css?p3s6mr" media="only screen and (max-width:320px)" />
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.smalltouch.landscape.css?p3s6mr" media="only screen and (min-width:321px) and (max-width:480px)" />
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.tablet.portrait.css?p3s6mr" media="only screen and (min-width:481px) and (max-width:768px)" />
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.tablet.landscape.css?p3s6mr" media="only screen and (min-width:769px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/css/responsive.desktop.css?p3s6mr" media="only screen and (min-width:1025px)" />
<style media="screen">
@import url("https://lambda-mediation.co.uk/sites/default/files/adaptivetheme/at_circles_files/at_circles.custom.css?p3s6mr");
</style>

<!--[if lt IE 9]>
<style media="screen">
@import url("https://lambda-mediation.co.uk/sites/default/files/adaptivetheme/at_circles_files/at_circles.lt-ie9.layout.css?p3s6mr");
</style>
<![endif]-->
<script>window.google_analytics_uacct = "UA-108539080-1";</script>
<script src="https://lambda-mediation.co.uk/misc/jquery.js?v=1.4.4"></script>
<script src="https://lambda-mediation.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://lambda-mediation.co.uk/misc/drupal.js?p3s6mr"></script>
<script src="https://lambda-mediation.co.uk/sites/all/modules/google_analytics/googleanalytics.js?p3s6mr"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-108539080-1", {"cookieDomain":"auto"});ga("require", "linkid", "linkid.js");ga("require", "displayfeatures");ga("set", "anonymizeIp", true);onClick="ga('send', 'event', { eventCategory: 'contact', eventAction: 'call', eventLabel: 'apevent'});"
onClick="ga('send', 'event', { eventCategory: 'contact', eventAction: 'email', eventLabel: 'apevent'});"
onClick="ga('send', 'event', { eventCategory: ‘form', eventAction: 'submit', eventLabel: 'apevent'});"ga("set", "page", location.pathname + location.search + location.hash);ga("send", "pageview");</script>
<script src="https://lambda-mediation.co.uk/sites/all/modules/google_analytics_et/js/google_analytics_et.js?p3s6mr"></script>
<script src="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/outside-events.js?p3s6mr"></script>
<script src="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/menu-toggle.js?p3s6mr"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_circles","theme_token":"_3E81f3sDqZYywpneisYoix19-zehMMkn69CqqaUK4o","js":{"0":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/modules\/google_analytics_et\/js\/google_analytics_et.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/outside-events.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/menu-toggle.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.floatblocks.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/global.base.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/global.styles.css":1,"public:\/\/adaptivetheme\/at_circles_files\/at_circles.responsive.layout.css":1,"public:\/\/adaptivetheme\/at_circles_files\/at_circles.fonts.css":1,"public:\/\/adaptivetheme\/at_circles_files\/at_circles.menutoggle.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.custom.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.smalltouch.portrait.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.smalltouch.landscape.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/adaptivetheme\/at_circles\/css\/responsive.desktop.css":1,"public:\/\/adaptivetheme\/at_circles_files\/at_circles.custom.css":1,"public:\/\/adaptivetheme\/at_circles_files\/at_circles.lt-ie9.layout.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackUrlFragments":1},"googleAnalyticsETSettings":{"selectors":[{"event":"mousedown","selector":"a","category":"main navigation","action":"click","label":"!test","value":0,"noninteraction":true},{"event":"mousedown","selector":"#page-title","category":"main navigation","action":"click","label":"!test","value":0,"noninteraction":true,"options":{"trackOnce":true}},{"event":"mousedown","selector":"a#logo","category":"Home Link","action":"click","label":"Logo","value":0,"noninteraction":true},{"event":"mousedown","selector":"div","category":"Home Link","action":"LAMBDA Mediation","label":"test","value":0,"noninteraction":true},{"event":"blur","selector":"#edit-name","category":"[TEST] blurred from the user login username\/email input field.","action":"BLUR","label":"!test","value":0,"noninteraction":true}],"settings":{"debug":true}},"urlIsAjaxTrusted":{"\/search\/node":true},"adaptivetheme":{"at_circles":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"},"menu_toggle_settings":{"menu_toggle_tablet_portrait":"false","menu_toggle_tablet_landscape":"false"}}}});</script>
<!--[if lt IE 9]>
<script src="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?p3s6mr"></script>
<![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node atr-7.x-3.x atv-7.x-3.1+81-dev site-name-lambda-mediation">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
       <div class="circ02">
  <div class="circ02inner">
</div>
</div> <div class="circ01">
  <div id="dummy"></div>
    <div id="element">
          
    </div>
</div>

<div id="page-wrapper">
  <div id="page" class="container page snc-n snw-b sna-l sns-n ssc-n ssw-n ssa-l sss-w btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-n at-mt">

    <!-- !Leaderboard Region -->
    <div id="headerouter">
    <header id="header" class="clearfix" role="banner">
              <!-- !Branding -->
        <div id="branding" class="branding-elements clearfix">

                      <div id="logo">
              <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="https://lambda-mediation.co.uk/sites/all/themes/adaptivetheme/at_circles/logo.png" alt="LAMBDA Mediation" /></a>            </div>
          
                      <!-- !Site name and Slogan -->
            <hgroup class="h-group" id="name-and-slogan">

                              <h1 id="site-name"><a href="/" title="Home page" class="active">LAMBDA Mediation</a></h1>
              
                              <h2 id="site-slogan">Bringing resolution to people in dispute</h2>
              
            </hgroup>
          
        </div>
      
      <!-- !Header Region -->
       </header>
     </div>
    <!-- !Navigation -->
            
    <!-- !Breadcrumbs -->
    
    <!-- !Messages and Help -->
    <div id="messages"></div>    
    <!-- !Secondary Content Region -->
    
    <div id="columns" class="columns clearfix">
      <div id="content-column" class="content-column" role="main">
        <div class="content-inner">

          <!-- !Highlighted region -->
          
          <div id="main-content">

            
            <!-- !Main Content Header -->
            </headerouter>
            <!-- !Main Content -->

              <div id="content" class="region">

       <ul class="navigation">
                <li class="nav-1">
                    <div class="vc-item vc-img-1">
                        <div class="vc-info-wrap">
                            <div class="vc-info">
                                <div class="vc-info-front vc-img-1">
                                    <span><h3><a href="faq">FAQs</a></h3></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-2">
                    <div class="vc-item vc-img-1">
                        <div class="vc-info-wrap">
                            <div class="vc-info">
                                <div class="vc-info-front vc-img-1">
                                    <span><h3><a href="finances">Finances</a></h3></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>


                <li class="nav-3">
                    <div class="vc-item vc-img-1">
                        <div class="vc-info-wrap">
                            <div class="vc-info">
                                <div class="vc-info-front vc-img-1">
                                    <span><h3>
									<a href="who-we-are">Contact us</a><br>
                                </div>

                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-4">
                    <div class="vc-item vc-img-1">
                        <div class="vc-info-wrap">
                            <div class="vc-info">
                                <div class="vc-info-front vc-img-1">
                                    <span><h3><a href="children">Children</a></h3></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>


  </div>

            
          </div><!-- /end #main-content -->

          <!-- !Content Aside Region-->
          
        </div><!-- /end .content-inner -->

      </div><!-- /end #content-column -->

      <!-- !Sidebar Regions -->
            
    </div><!-- /end #columns -->

    <!-- !Tertiary Content Region -->
    
    <!-- !Footer -->


  </div>
</div>
  </body>
</html>
