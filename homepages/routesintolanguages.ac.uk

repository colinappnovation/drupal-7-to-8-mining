<!DOCTYPE html>
  <!--[if IEMobile 7]><html class="no-js ie iem7" lang="en" dir="ltr"><![endif]-->
  <!--[if lte IE 6]><html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
  <!--[if (IE 7)&(!IEMobile)]><html class="no-js ie lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
  <!--[if IE 8]><html class="no-js ie lt-ie9" lang="en" dir="ltr"><![endif]-->
  <!--[if (gte IE 9)|(gt IEMobile 7)]><html class="no-js ie" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><![endif]-->
  <!--[if !IE]><!--><html class="no-js" lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
  <meta charset="utf-8" />
<link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
<link rel="alternate" type="application/rss+xml" title="Routes into Languages RSS" href="https://www.routesintolanguages.ac.uk/rss.xml" />
<link rel="shortcut icon" href="https://www.routesintolanguages.ac.uk/sites/all/themes/uos_base_theme/images/routes-favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.routesintolanguages.ac.uk/" />
<link rel="shortlink" href="https://www.routesintolanguages.ac.uk/" />
  <meta name=viewport content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/sites/default/files/touch/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" sizes="57x57" href="/sites/default/files/touch/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/sites/default/files/touch/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/sites/default/files/touch/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/sites/default/files/touch/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/sites/default/files/touch/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/sites/default/files/touch/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/sites/default/files/touch/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/sites/default/files/touch/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/sites/default/files/touch/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/sites/default/files/touch/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="/sites/default/files/touch/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/sites/default/files/touch/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/sites/default/files/touch/android-chrome-192x192.png" sizes="192x192">
  <meta name="msapplication-square70x70logo" content="/sites/default/files/touch/smalltile.png" />
  <meta name="msapplication-square150x150logo" content="/sites/default/files/touch/mediumtile.png" />
  <meta name="msapplication-wide310x150logo" content="/sites/default/files/touch/widetile.png" />
  <meta name="msapplication-square310x310logo" content="/sites/default/files/touch/largetile.png" />
  <title>Routes into Languages | Promoting the take-up of languages and student mobility</title>
  <link type="text/css" rel="stylesheet" href="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/css/css_9c-Pr-3zwdfif_-_A7Vo8D_0oiUmpZe3MiMbiRU0pX0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/css/css_Y1RfIYk4KuvnnWrx2og0UQhjvI8NjtUUuZBG5Uf9Is4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/css/css_8vsKRm3CJuSMFyRgpiOeQr2AXYKoiwePZbu7bzRVMrU.css" media="all" />

<!--[if lte IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/css/css_d1z9ntFxJHwh-q3P8pHHt5ab1RoiPI2yS6h7ac7hu1g.css" media="all" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/css/css_CtrUTCt9qQIpe_DPgkrUDAOPk16wrN9XLqiPsbU9JyY.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:regular&amp;subset=devanagari" media="all" />
  <script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_rvX88atDavNAF53kmYXXUetdvDCPoVajE-Qm_rkz9xE.js"></script>
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_qDak_KilyEl9eNomB3TZHBBBNVlF7vOfjKZprNuHF5g.js"></script>

<!--[if (gte IE 6)&(lte IE 8)]>
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_6yMic9M60ZIqVDI5L3EwTamdpkUztNOhG9yu7kbQEpY.js"></script>
<![endif]-->
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_BWFEZ2qMj4YBzPiPSXDns3qBRslFgphqzcQOiacE1JE.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-1657841-2", {"cookieDomain":"auto"});ga("require", "linkid", "linkid.js");ga("set", "anonymizeIp", true);ga("set", "page", location.pathname + location.search + location.hash);ga("send", "pageview");</script>
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_VyMRedvit7J2FkH9AMDR2o4JBl6UJbX-pR-2xaxCu9M.js"></script>
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_HsRUzyLP1Thbv9FCTTEGtjlIMbpBtAA4ys1pWA0CucI.js"></script>
<script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_x_fOwW0tIYjrFjLAsGz67Zyh0UXEcdxzsh-EqfshIWI.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"uos_base_theme","theme_token":"sWzB6SCuu5IsjrBWmssdn5fMJwN4tuqPbQFdN3g3JQU","js":{"sites\/all\/modules\/contrib\/flexslider\/assets\/js\/flexslider.load.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/omega\/omega\/js\/no-js.js":1,"sites\/all\/modules\/contrib\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"misc\/jquery.form.js":1,"misc\/ajax.js":1,"sites\/all\/themes\/uos_base_theme\/libraries\/respond\/respond.min.js":1,"sites\/all\/modules\/contrib\/lightbox2\/js\/lightbox.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/modules\/contrib\/views\/js\/base.js":1,"misc\/progress.js":1,"sites\/all\/modules\/contrib\/views\/js\/ajax_view.js":1,"sites\/all\/modules\/contrib\/ctools\/js\/jump-menu.js":1,"sites\/all\/themes\/uos_base_theme\/js\/uos-base-theme.behaviors.js":1,"sites\/all\/themes\/uos_base_theme\/layouts\/uos-off-canvas\/assets\/off-canvas.js":1},"css":{"sites\/all\/themes\/omega\/omega\/css\/modules\/system\/system.base.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/system\/system.menus.theme.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/system\/system.messages.theme.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/comment\/comment.theme.css":1,"modules\/node\/node.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/field\/field.theme.css":1,"sites\/all\/modules\/contrib\/youtube\/css\/youtube.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/search\/search.theme.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/user\/user.base.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/forum\/forum.theme.css":1,"sites\/all\/themes\/omega\/omega\/css\/modules\/user\/user.theme.css":1,"sites\/all\/modules\/uos_modules\/custom_twitter_timeline\/stylesheets\/twitter.css":1,"sites\/all\/modules\/contrib\/flickr\/flickr.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/contrib\/taxonomy_access\/taxonomy_access.css":1,"sites\/all\/modules\/contrib\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/themes\/uos_base_theme\/css\/uos-base-theme.normalize.css":1,"sites\/all\/themes\/uos_base_theme\/css\/uos-base-theme.hacks.css":1,"sites\/all\/themes\/uos_base_theme\/css\/uos-base-theme.styles.css":1,"sites\/all\/themes\/uos_base_theme\/css\/uos-base-theme.no-query.css":1,"all:0":1,"sites\/routesintolanguages.ac.uk\/files\/fontyourface\/font.css":1,"https:\/\/fonts.googleapis.com\/css?family=Open+Sans:regular\u0026subset=devanagari":1}},"lightbox2":{"rtl":"0","file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/contrib\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"f0f0f0","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":1,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":0,"disable_resize":0,"disable_zoom":1,"force_show_nav":0,"show_caption":1,"loop_items":0,"node_link_text":"View Image Details","node_link_target":0,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":1,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":0,"useragent":"Mozilla\/5.0 (compatible; GoogleDocs; apps-spreadsheets; +http:\/\/docs.google.com)"},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":false,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":7000,"directionNav":true,"controlNav":true,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackUrlFragments":1},"urlIsAjaxTrusted":{"\/":true,"\/views\/ajax":true},"views":{"ajax_path":"\/views\/ajax","ajaxViews":{"views_dom_id:d5edd972362a727e273299b963c8d992":{"view_name":"news_block_list","view_display_id":"views_block_latest_news","view_args":"","view_path":"node","view_base_path":"news","view_dom_id":"d5edd972362a727e273299b963c8d992","pager_element":0}}}});</script>
</head>
<body class="html front not-logged-in page-node">
  <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
    <!-- Main Grid Container -->
<div class="container"  class="l-page has-two-sidebars has-sidebar-first has-sidebar-second">

  <!-- Region: Accessibility -->
  <div class="accessibility">
    <!-- Print: R.Accessibility -->
      </div>
  <!-- /Region: Accessibility -->

  <!-- Region: Branding -->
  <div class="branding">

    <!-- Print: Branding -->
    
    <!-- branding-subarea-1 -->
    <div class="branding-subarea-1">
      <!-- Menu button -->
      <div class="menu-btn">
        <a href="#menu-btn" class="toggle">Show menu</a>
      </div>
      <!-- Print: Logo -->
                        <div class="logo logo-left">
            <a href="/" title="Home" rel="home" class="site-logo">
              <img src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/routes-logo-square.png" alt="Home"/>
            </a>
          </div>
                        </div>

    <!-- branding-subarea-2 -->
    <div class="branding-subarea-2">
              <h2 class="site-slogan">Promoting the take-up of languages and student mobility</h2>
                </div>

    <!-- branding-subarea-3 -->
    <div class="branding-subarea-3">
      <!-- Print: Logo -->
                              <div id="block-search-form" role="search" class="block block--search block--search-form">
        <div class="block__content">
    <form class="search-block-form" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-3mXy-62s1TqvEx0eoyuJJOjcO8joSCwpbaLy0razn9Y" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>  </div>
</div>
    </div>
  </div>
  <!-- /Region: Branding -->

  <!-- Region: Header -->
  <div class="header">
        <div id="block-views-slider-block" class="block block--views block--views-slider-block">
        <div class="block__content">
    <div class="view view-slider view-id-slider view-display-id-block view-dom-id-c45b2b7059d97f628fdd038722e27973">
        
  
  
      <div class="view-content">
      <div  id="flexslider-1" class="flexslider optionset-default">
  <ul class="slides"><li>  
          <img typeof="foaf:Image" src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/styles/manual_crop_1160x400/public/images/slider-images/42-16778692-online.jpg?itok=YMSEJORV&amp;c=66f5efb6ca4e3556628fb23f6963876d" alt="" />  </li>
</ul></div>
    </div>
  
  
  
  
  
  
</div>  </div>
</div>
  </div>
  <!-- /Region: header -->

  <!-- Region: Header Title -->
  <div class="header-title">
                  <h1 class="site-name">Routes into Languages</h1>
                </div>
  <!-- /Region: Header Title -->

  <!-- Region: Left Sidebar -->
  <div class="left-sidebar left-sidebar-front">
        <nav id="block-menu-menu-routes-main-menu" role="navigation" class="block block--menu block--menu-menu-routes-main-menu">
      
  <ul class="menu"><li class="first collapsed"><a href="/" class="active">Home</a></li>
<li class="collapsed"><a href="/about">About</a></li>
<li class="collapsed"><a href="/activities">Activities</a></li>
<li class="leaf"><a href="/resources">Resources</a></li>
<li class="leaf"><a href="/news">News</a></li>
<li class="leaf"><a href="/contact">Contact</a></li>
<li class="last leaf"><a href="/live" class="red">Routes LIVE!</a></li>
</ul></nav>
  </div>
  <!-- /Region: Left Sidebar -->

  <!-- Region: Content -->
  <div class="main-container">
    
    <!-- invisible link to jump here using Skip to main content, by pressing tab -->
    <a id="main-content"></a>

    <!-- Print: Drupal messages -->
    
    <!-- Print: Editing tabs -->
    
    <!-- Print: render main content -->
    
      <div class="block latest-news">
      <h3><a href="/news">Latest news</a></h3>
      <div class="view view-news-block-list view-id-news_block_list view-display-id-views_block_latest_news view-dom-id-d5edd972362a727e273299b963c8d992">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div>        <h4><a href="/news/language-ambassador-applications-now-open">Language Ambassador Applications Now Open</a></h4>  </div>  
  <div class="views-field views-field-field-news-description">        <span class="field-content small">Are you a language student at SOAS, University of Westminster, UCL, King's College London,... <a href="/news/language-ambassador-applications-now-open" class="views-more-link">Read more</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div>        <h4><a href="/news/upcoming-poetry-cpd-days">Upcoming poetry CPD Days</a></h4>  </div>  
  <div class="views-field views-field-field-news-description">        <span class="field-content small">Manchester Metropolitan has some very exciting CPD events coming up for teachers to support... <a href="/news/upcoming-poetry-cpd-days" class="views-more-link">Read more</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div>        <h4><a href="/news/manchester-met-launches-spanish-level-translation-competition">Manchester Met Launches Spanish A-Level Translation Competition </a></h4>  </div>  
  <div class="views-field views-field-field-news-description">        <span class="field-content small">The Spanish Section of the Department of Languages, Information and Communications at Manchester... <a href="/news/manchester-met-launches-spanish-level-translation-competition" class="views-more-link">Read more</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div>        <h4><a href="/news/manchester-met-launches-french-level-translation-competition">Manchester Met Launches French A-Level Translation Competition</a></h4>  </div>  
  <div class="views-field views-field-field-news-description">        <span class="field-content small">The French section of the Department of Languages, Information and Communications at Manchester... <a href="/news/manchester-met-launches-french-level-translation-competition" class="views-more-link">Read more</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div>        <h4><a href="/news/over-300-attendees-manchester-mets-sixth-form-languages-day">Over 300 attendees at Manchester Met&#039;s Sixth Form Languages Day</a></h4>  </div>  
  <div class="views-field views-field-field-news-description">        <span class="field-content small">Manchester Met's Sixth Form Languages Day which supported both pupils studying French and Spanish... <a href="/news/over-300-attendees-manchester-mets-sixth-form-languages-day" class="views-more-link">Read more</a></span>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/news">
    View more news  </a>
</div>
  
  
  
</div>    </div>
  
  <div class="ag6">
          <div class="block region-map clearfix">
        <h3>What's going on in your region?</h3>
        <div class="view view-region-list view-id-region_list view-display-id-block_1 regionList view-dom-id-3f52831ccfadd778e6c49a341aca5129">
            <div class="view-header">
      <a href="/activities"><img alt="map of the UK" class="photobox hpMap" src="/sites/all/themes/uos_base_theme/images/uk_map_regional_consortia_web.gif" /></a>    </div>
  
  
  
      <div class="view-content">
      <form class="ctools-jump-menu" action="/" method="post" id="ctools-jump-menu" accept-charset="UTF-8"><div><div class="container-inline"><div class="form-item form-type-select form-item-jump">
 <select class="ctools-jump-menu-select form-select" id="edit-jump" name="jump"><option value="" selected="selected">- What&#039;s going on in your area? -</option><option value="6fad6fe3943d73f5e5d647bdce027afc::/activities/national">  
          National  </option><option value="1eca2cfee4481cdcaf969655fb9ebcec::/activities/national-network-translation">  
          National Network for Translation  </option><option value="62b4a9f4d5569a52770e0f1cc05362d6::/activities/national-network-interpreting">  
          National Network for Interpreting  </option><option value="137bf8f2059da43a093a7d1001e49fd1::/activities/yorkshire-and-humber">  
          Yorkshire and The Humber  </option><option value="139cf8ca8a83995af666ab1e4edb17fd::/activities/west-midlands">  
          West Midlands  </option><option value="97952882905974ea6bf3df746e2dcdb2::/activities/cymru">  
          Wales / Cymru  </option><option value="e134a6166670a1f13ac00bb15fab1aaa::/activities/south-west">  
          South West  </option><option value="31ce47fa67d15823105097c7a1fb6b68::/activities/south-east">  
          South East  </option><option value="52c7a7c7877be8c973dfa10eec3727f0::/activities/north-west">  
          North West  </option><option value="4d3027d6fe36a065825734a58093a852::/activities/north-east">  
          North East  </option><option value="e02c207f1b3475db1112e46dd6d2d3ae::/activities/london">  
          London  </option><option value="8adf84628ffa2eacf45c21c5ba507a81::/activities/east-england">  
          East of England  </option><option value="af62672a3d307db576eafdd615040f45::/activities/east-midlands">  
          East Midlands  </option></select>
</div>
<input class="ctools-jump-menu-button form-submit" type="submit" id="edit-go" name="op" value="Go" /></div><input type="hidden" name="form_build_id" value="form-8O09TJbcQ27Ev2T3QG1-rMVLWB3EoUB3h-3YaosVyi0" />
<input type="hidden" name="form_id" value="ctools_jump_menu" />
</div></form>    </div>
  
  
  
  
      <div class="view-footer">
      <p> &nbsp; </p>    </div>
  
  
</div>      </div>
      </div>
	<div class="ag6 omega">
    	    <div class="block events">
      <h3><a href="/news">Upcoming Events &amp; Activities</a></h3>
      <div class="view view-event-list view-id-event_list view-display-id-block_1 view-dom-id-08c94162c87168bc15f7528612d2aac5">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first clearfix">
      
  <div class="left">        <time datetime="2020-01-06 09:00:00 to 2020-02-14 17:00:00" class="icon">
  <em>Monday</em>
  <strong>January</strong>
  <span>6</span>
</time>  </div>  
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/events/foreign-language-film-review-competition">Foreign Language Film Review Competition</a></h3>  </div>  
  <span class="views-field views-field-field-location">    <strong class="views-label views-label-field-location">Location: </strong>    <span class="field-content">National - Open to all Secondary Schools in England</span>  </span>  
  <div class="views-field views-field-field-event-description">        <div class="field-content">Do you love watching the latest films? Have you ever fancied trying your hand at being a film...</div>  </div>  </div>
  <div class="views-row views-row-2 views-row-even clearfix">
      
  <div class="left">        <time datetime="2020-01-08 13:00:00 to 2020-01-08 16:00:00" class="icon">
  <em>Wednesday</em>
  <strong>January</strong>
  <span>8</span>
</time>  </div>  
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/events/essay-writing-film-pans-labyrinth">Essay Writing for Film: Pan’s Labyrinth </a></h3>  </div>  
  <span class="views-field views-field-field-location">    <strong class="views-label views-label-field-location">Location: </strong>    <span class="field-content">Manchester Metropolitan University</span>  </span>  
  <div class="views-field views-field-field-event-description">        <div class="field-content">These half-day events are offered in French, German and Spanish, and focus on a popular film...</div>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd clearfix">
      
  <div class="left">        <time datetime="2020-01-15 13:00:00 to 2020-01-15 16:00:00" class="icon">
  <em>Wednesday</em>
  <strong>January</strong>
  <span>15</span>
</time>  </div>  
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/events/essay-writing-film-la-haine">Essay Writing for Film: La Haine</a></h3>  </div>  
  <span class="views-field views-field-field-location">    <strong class="views-label views-label-field-location">Location: </strong>    <span class="field-content">Manchester Metropolitan University</span>  </span>  
  <div class="views-field views-field-field-event-description">        <div class="field-content">These half-day events are offered in French, German and Spanish, and focus on a popular film...</div>  </div>  </div>
  <div class="views-row views-row-4 views-row-even clearfix">
      
  <div class="left">        <time datetime="2020-01-21 10:30:00 to 2020-01-21 15:00:00" class="icon">
  <em>Tuesday</em>
  <strong>January</strong>
  <span>21</span>
</time>  </div>  
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/events/spanish-level-study-day-year-12-el-futuro-perfecto">Spanish A Level Study Day (Year 12): El Futuro Perfecto</a></h3>  </div>  
  <span class="views-field views-field-field-location">    <strong class="views-label views-label-field-location">Location: </strong>    <span class="field-content">HOME, Manchester</span>  </span>  
  <div class="views-field views-field-field-event-description">        <div class="field-content">This study morning will include a screening of the film El Futuro Perfecto. The film will be...</div>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last clearfix">
      
  <div class="left">        <time datetime="2020-01-22 10:30:00 to 2020-01-22 15:00:00" class="icon">
  <em>Wednesday</em>
  <strong>January</strong>
  <span>22</span>
</time>  </div>  
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/events/french-level-study-day-year-12-une-saison-en-france">French A Level Study Day (Year 12):  Une Saison en France</a></h3>  </div>  
  <span class="views-field views-field-field-location">    <strong class="views-label views-label-field-location">Location: </strong>    <span class="field-content">HOME, Manchester</span>  </span>  
  <div class="views-field views-field-field-event-description">        <div class="field-content">This study day examines the themes of the film (family, migration, social exclusion and solidarity...</div>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/events">
    View all events  </a>
</div>
  
  
  
</div>    </div>
      </div>
  


      </div>
  <!-- /Region: Content -->

  <!-- Region: Right Sidebar -->
  <div class="right-sidebar">
        <div id="block-custom-twitter-timeline-custom-twitter-timeline" class="block block--custom-twitter-timeline block--custom-twitter-timeline-custom-twitter-timeline">
        <h2 class="block__title">Twitter feed by Routesintolangs</h2>
      <div class="block__content">
    <ul><li><br><a href="https://twitter.com//status/" target="_blank">more..</a></li>
</ul>  </div>
</div>
<div id="block-block-5" class="block block--block block--block-5">
        <h2 class="block__title">Latest YouTube Videos</h2>
      <div class="block__content">
    <p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/?list=UU4qvPJYJCj2TDnCT3IQ34wg" width="100%"></iframe></p>  </div>
</div>
  </div>
  <!-- /Region: Right Sidebar -->

  <!-- Dummy div, needed for sticky footer -->
  <div class="container-footer-overflow"></div>

</div>
<!-- /Main Grid Container -->

<!-- Create a second container with the same scss just to include the footer.
     Needed for sticky footer -->
<!-- Footer Grid Container -->
<div class="footer-container">

  <!-- Region: Footer -->
  <div class="footer">
    <div class="footer-logo"></div>
	<div class="top-footer_container clearfix">
		<div class="top-footer clearfix">
			<div class="top-footer-inner">
				    <nav id="block-menu-menu-footer-menu" role="navigation" class="block block--menu block--menu-menu-footer-menu">
      
  <ul class="menu"><li class="first leaf"><a href="/cookies">Cookies</a></li>
<li class="leaf"><a href="/accessibility-statement">Accessibility</a></li>
<li class="last leaf"><a href="/contact">Contact Us</a></li>
</ul></nav>
			</div>
		</div>
		<div class="top-footer clearfix">
			<div class="top-footer-inner">
							</div>	
		</div>
		<div class="top-footer clearfix">
			<div class="top-footer-inner">
							</div>	
		</div>
		<div class="top-footer clearfix">
			<div class="top-footer-inner">	
				    <div id="block-block-4" class="block block--block block--block-4">
        <div class="block__content">
    <p><a href="http://www.twiter.com/routesintolangs" target="_blank">Twitter</a> - <a href="http://www.facebook.com/routes.into.lang" target="_blank">Facebook</a> - <a href="https://www.youtube.com/channel/UC4qvPJYJCj2TDnCT3IQ34wg" target="_blank">YouTube</a></p>
  </div>
</div>
			</div>	
		</div>	
	</div>
    <div class="footer-copyright clearfix">    <div id="block-block-6" class="block block--block block--block-6">
        <div class="block__content">
    <span class="small">© Copyright 2019 Routes into Languages. All Rights Reserved.</span>  </div>
</div>
<div id="block-block-7" class="block block--block block--block-7">
        <div class="block__content">
    <p><img alt="" src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/hosted-by-the-university-of-southampton.png" /></p>
  </div>
</div>
</div>
  </div>
  <!-- /Region: Footer -->

</div>
<!-- /Footer Grid Container -->
  <script src="https://www.routesintolanguages.ac.uk/sites/routesintolanguages.ac.uk/files/js/js_uTpGZRbRZm_lrt5640lI88hN-6jGIe3E3hxZcagIuss.js"></script>
</body>
</html>
