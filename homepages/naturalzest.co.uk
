<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.naturalzest.co.uk/sites/default/files/target-icon.png" type="image/png" />
<meta name="description" content="Our beginner Tai Chi &amp; Qi Gong holidays are a combination of all the best Greece has to offer. Our retreat is set in 5000sqm of nature, with the smell of the orange and lemon groves set in peace and tranquility." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.naturalzest.co.uk/" />
<link rel="shortlink" href="https://www.naturalzest.co.uk/" />
<meta property="og:site_name" content="Natural Zest" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.naturalzest.co.uk/" />
<meta property="og:title" content="Natural Zest - Tai chi, Qigong Holidays in Greece" />
<meta property="og:description" content="Our beginner Tai Chi &amp; Qi Gong holidays are a combination of all the best Greece has to offer. Our retreat is set in 5000sqm of nature, with the smell of the orange and lemon groves set in peace and tranquility." />
<meta property="og:image" content="http://naturalzest.co.uk/sites/all/themes/natural/img/front_2200.jpg" />
  <title>Natural Zest | Tai chi, Qigong Holidays in Greece</title>
  <link type="text/css" rel="stylesheet" href="https://www.naturalzest.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.naturalzest.co.uk/sites/default/files/css/css_ln2CQuma-X9eX1FYIMK2zMwII0lRrTkClK8Alx6eTOU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.naturalzest.co.uk/sites/default/files/css/css_r87naKT-lBgrpyrwOIcdaNM3ryCno-SU0yCd5Gj7KHo.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@3.3.7/flatly/bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.naturalzest.co.uk/sites/default/files/css/css_04BDiztCVCjSfoB0gwaoIR6FLnYc0Dn8WkgStQ21ZuE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.naturalzest.co.uk/sites/default/files/css/css_lEZ9P2PxxqssBcRLOp01ei5SNu1WwrxO7og8Izw-i_U.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower:regular|Pacifico:regular|Quicksand:300&amp;subset=latin" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script>window.google_analytics_uacct = "UA-125445344-1";</script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_-GWntAMdxMbIut2W0XKAyJeNlGpRcXxtej8zRZRPqkY.js"></script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_qE4KfYgJty9x7gyqzjMp9mcDiG6PQCqqJbfsBu7bK8w.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_YD8cGCqGnsATGVDieq-Wb8b5xjhxtnhcdh_Tkf11tP8.js"></script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_60wLRlEpkH-cPQHj2tMr67An82MBJmtiCXPJAqzi_4U.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-125445344-1", {"cookieDomain":"auto"});ga("require", "displayfeatures");ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_QSChhbrNmHvdE5_xs8yfNJb8A7KdPZvh9lzITFLjOsk.js"></script>
<script src="https://www.naturalzest.co.uk/sites/default/files/js/js_OfBZabJoVJs4qyLwe9JPe2ioNR4GEZ5RVhokfovDDwg.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"natural","theme_token":"0VXryLn9fRDeQOHeGoBvmWoxfaZ01mR23cX1oi2c6QE","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"0":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/fitvids\/jquery.fitvids.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.effect.min.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.7\/dist\/js\/bootstrap.min.js":1,"sites\/all\/modules\/fitvids\/fitvids.js":1,"sites\/all\/modules\/back_to_top\/js\/back_to_top.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/stickynav\/js\/stickynav.js":1,"sites\/all\/modules\/views_bootstrap\/js\/views-bootstrap-carousel.js":1,"sites\/all\/libraries\/wow\/dist\/wow.min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/modules\/blockanimate\/js\/blockanimate.js":1,"sites\/all\/themes\/natural\/js\/natural.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/back_to_top\/css\/back_to_top.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/libraries\/animate\/animate.min.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/stickynav\/css\/stickynav.css":1,"sites\/all\/libraries\/fontawesome\/css\/font-awesome.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootswatch@3.3.7\/flatly\/bootstrap.min.css":1,"sites\/all\/themes\/bootstrap\/css\/3.3.7\/overrides-flatly.min.css":1,"sites\/all\/themes\/natural\/css\/style.css":1,"sites\/default\/files\/fontyourface\/font.css":1,"https:\/\/fonts.googleapis.com\/css?family=Indie+Flower:regular|Pacifico:regular|Quicksand:300\u0026subset=latin":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"stickynav":{"selector":"#navbar"},"viewsBootstrap":{"carousel":{"1":{"id":1,"name":"testimonials","attributes":{"interval":9000,"pause":"hover"}}}},"back_to_top":{"back_to_top_button_trigger":"300","back_to_top_button_text":"Back to top","#attached":{"library":[["system","ui"]]}},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":0,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":0,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-home">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
	<div class="cover text-center">
              <a class="name navbar-brand-front" href="/" title="Home">Natural Zest</a>
      	  <div id="arrow">
	  <p style="display:inline;float:left;">Tai Chi</p>
	  <a href="#mymenu"<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
	  <p style="float:right;">Qigong</p>
	  </div>
</div><!-- cover -->

<header id="navbar" role="banner" class="header-front navbar container-fluid navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      
             <!-- <a class="name navbar-brand" href="/" title="Home">Natural Zest</a>-->
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

	<div id="mymenu">
		              <a class="name navbar-brand" href="/" title="Home">Natural Zest</a>
      	  
          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first leaf"><a href="/tai-chi">Tai Chi</a></li>
<li class="leaf"><a href="/qigong">Qigong</a></li>
<li class="leaf"><a href="/messinia" title="Zest Holidays in Messinia">Messinia</a></li>
<li class="leaf"><a href="/zante" title="Tai Chi &amp; Qigong Holidays">Zante</a></li>
<li class="leaf"><a href="/testimonials" title="">Testimonials</a></li>
<li class="leaf"><a href="/about-me">About me</a></li>
<li class="leaf"><a href="/blog" title="">Blog</a></li>
<li class="last leaf"><a href="/contact-us">Contact us</a></li>
</ul>                                                                                  </nav>
      </div>
    	</div>
  </div>
</header>


<div class="main-container container-fluid">

  <header role="banner" id="page-header">
    
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-12">
                  <a id="main-content"></a>
                    <!--<h1 class="page-header">--><!--<h1 class="lead text-center">Οργάνωση Εθελοντών</h1>-->
                                                          <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div class="view view-no-front-page view-id-no_front_page view-display-id-page view-dom-id-a08b7a729b7f56e840029f44e6a8f36f">
        
  
  
  
  
  
  
  
  
</div>
</section>
<section id="block-block-2" class="block block-block animated wow fadeIn clearfix" data-wow-duration="4s">

      
  <!-- NEW ONE --><h2 class="title"><span>What we offer</span></h2>
<div class="frontblocks">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="hovereffect"><img alt="" class="img-responsive" src="/sites/all/themes/natural/img/front_tai_chi_585x390.JPG" /><div class="overlay">
<h2>Tai-Chi</h2>
<blockquote style="color:#fff;"><p>a journey of a thousand miles begins with one step.<br />
― Lao Tzu
</p>
</blockquote>
<p><a class="info" href="/tai-chi"><img alt="link icon" src="/sites/all/themes/natural/img/link_icon_50x50.png" /></a></p></div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="hovereffect"><img alt="" class="img-responsive" src="/sites/all/themes/natural/img/qigong/IMG_5214_585x390.jpg" /><div class="overlay">
<h2>Qigong</h2>
<blockquote style="color:#fff;"><p>To a mind that is still, the entire universe surrenders.<br /><br />
― Zhuangzi</p>
</blockquote>
<p><a class="info" href="/qigong"><img alt="link icon" src="/sites/all/themes/natural/img/link_icon_50x50.png" /></a></p></div>
</div>
</div>
</div>

</section>
<section id="block-block-3" class="block block-block animated wow zoomIn clearfix" data-wow-duration="2s">

      
  <h2 class="title"><span>Beginners Tai Chi Holidays in Greece</span></h2>
<div class="frontblocks">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><img alt="" class="img-responsive" src="/sites/all/themes/natural/img/voidokoilia.jpg" /></div>
<blockquote><p>Mother Nature operates forever.<br />
― Huang Tsu-hou
</p>
</blockquote>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<!--<p>Voidokilia beach peloponnese</p>--><p>Our beginner Tai Chi &amp; Qi Gong holidays are a combination of all the best Greece has to offer. Our retreat is set in 5000sqm of nature, with the smell of the orange and lemon groves set in peace and tranquility.  Local grown food, Ancient Greek traditional healthy recipes, breath-taking scenery and crystal clear swimming beaches that nourish your mind, body and soul. </p>
<p><a style="display: inline-block;" href="/messinia" class="btn btn-primary">Messinia</a><br /><a style="display:inlline-block;" href="/zante" class="btn btn-primary">Zante</a>
</p></div>
</div>

</section>
<section id="quote-carousel" class="block block-views animated wow fadeIn container clearfix" data-wow-duration="4s">

      
  <div class="view view-testimonials view-id-testimonials view-display-id-block view-dom-id-c5622f9b7bbbd2c37e35e3767e65d1c9">
            <div class="view-header">
      <h2 class="title"><span>What you say</span></h2>
    </div>
  
  
  
      <div class="view-content">
      <div id="views-bootstrap-carousel-1" class="views-bootstrap-carousel-plugin-style carousel slide"  data-ride="carousel">
  
  <!-- Carousel items -->
  <div class="carousel-inner">
          <div class="item active">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>I have just returned home from a 10 day stay at Natural Zest, Zante and it has been a life changing and totally spiritual journey for me.  Nas is the most beautiful person I have ever met and I feel so honoured and privileged to have been taught by, I feel, a true Master.  He emanates love from when you first meet him and it continues to grow throughout your stay.  He is kind, gentle, thoughtful and extremely patient and guides you through your Tai Chi experience step by step.  When arriving at the Retreat, I was totally blown away by the location; it is perfect, paradise, in fact.  The view of the beautiful sea and the sound of the waves are with you all the way, throughout the day and night, and I did not miss a sunrise!  Add all this together it gave me so much joy and strength for each day.  The meals, are stupendous and also became a ritual in itself, my favourite was breakfast!  I have returned home to appreciate healthy food more than ever and have already made changes to my diet.  Even though I did not choose the total detox, Nas did provide me with some juices to taste and they were phenomenal!  I can highly recommend a total detox!   Each day, I began with a walk, followed by Tai Chi.  After lunch you have free time to do whatever you wish, I swam, walked, read and then we would return for a lesson to enhance on what we had learned earlier in the day and to finish with QiGong, which bonded all of us even more.  The programme really worked for me and confirmed why I had chosen Natural Zest in the first place.  With Nas’s guidance, I managed to achieve the first 14 Tai Chi moves to start me off, for the rest of my life and I thank you, Nas, so much!  Lastly, I am planning my diary now, to return next year!  Love and peace to Nas and Julie, forever in my heart!</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Victoria, South West France</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>The week that brought me back to life!!! When I first arrived at NaturalZest, I truly had forgotten what the word ‘zest’ meant. I was very low – in fact, I was depressed. My expectations, despite having read the reviews, were next to none. My anxiety, however, was huge. Nas picked us up from the airport – straight away, the bond started. He was friendly, warm, funny and down to earth. I slowly became more at ease. When we arrived at the property, I was utterly blown away by the beauty of what was around me – the place was nothing short of stunning and paradise-like. That night, the sound of the sea – so close – and the songs of the cicadas told me this would be an experience I would forever remember. I have been suffering from chronic fatigue and fibromyalgia for many years now. I know that too much effort can throw me into a very painful and depleted state that can last for weeks. But every day I swam, did Tai-Chi, Chi-Kung and went for walks. I also went on adventures that I would never had said my body/mind could endure – like walking up a mountain to see the sunrise or swimming in a blue cave. All the way, I had ample time to rest and recover. I always had a choice to hear my body and be gentle. Nas and the people I was with were always kind, patient and encouraging, as I legged behind in our walks, as I struggled to keep up, as I doubted myself – and I was respected, supported and accepted just as I was. Day by day I became clearer, stronger, open and happier. My dreams – and certainly my zest for life – were reignited by Nas &amp; Julie, by that nurturing place, by the people I met there and I will be forever grateful to them. Nas &amp; Julie live in my heart now and their teachings and their presence keep me focused on my way ahead towards my goals. This was a transformative journey, one that is a benchmark in my life. I will return – of that I am sure – to renew the vows I made with myself of maintaining the connection to what is important to me and also to meet Nas and Julie again. If like me, when you read the reviews you thought ‘this is too good to be true’, I can tell you – it is true and it is wonderful. If anything, our appraisals fall short of what it is to be there. Nas &amp; Julie and the beautiful people I shared that week with – thank you for bringing me back to life.</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Patricia (Portugal/England)</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>  I wanted to begin a practice in Tai Chi, but as I have a very intensive work life where I am posted in remote locations, I have no access to learn. I was looking for a teacher to show me the very basics in an intensive one-week course, so I can take something back with me to add to my daily life. Finding Nas was an amazing blessing! Nas is a great teacher – he is patient, never gets frustrated and has the most serene ability to review things over and over again to help one understand the subtle and specific points of the form. More than anything, Nas is a kind and generous person, whose energy I deeply appreciated bringing into my life. He and his wife Julie took great care of me, cooking delicious Greek meals, blending up fantastic juices and showing me stunning beaches. If you are looking for lots of stimulation, parties or running around, look elsewhere – but if you want to spend a few weeks with beautiful people, in a beautiful location, learning a beautiful art form – than please visit Nas and Julie!</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Deborah</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>Nas is a Tai Chi expert and has trained in China with well know professors in the Yang tradition. He is very knowledgeable and able to transmit the finer points of The Form. He really cares that his students get what they want from their retreat and is an enthusiastic and patient teacher. Continuous repetition can be tedious but Nas’s gentle humour transforms it into an enjoyable experience. By the end of one week I was able to complete the first part of The Form well enough for me to practice on my own at home without having to stop and think about which movement comes next. Filming Nas doing the first part of the Form was a useful aide memoir when I got home and I forgot some of the details.Nas’s dream is to run his own Tai Chi and Qigong school and I wish him every success with this endeavour. He truly deserves it!!! </p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Carol Richardson, Hert</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>We spend a week in NaturalZest and we were lucky enough to experience Tai Chi for the first time. Nasos found the perfect spot to introduce us to it. The beach at sunset was peaceful and relaxing and we were really surprised at how much we enjoyed Tai Chi itself. We learnt how to become aware of incorrect body posture and how to breathe deeply for relaxation. We have done Tai Chi a few more times with Nas who really is a wonderful teacher, and the peace that it brings to the mind and body is really amazing!!!</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Adele &amp; Jacky, Stoke on Trent</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>The two weeks that I spent at NaturalZest with Nas and Julie were beyond peaceful and transformational. Nas is an incredibly gifted teacher, who took all the time and effort in the world to make sure that my first experience of Tai Chi and Chi Gong were the best I could have. I arrived very exhausted with little sleep and a very poor diet, and over the 2 weeks Nas and Julie helped transform my view on food and diet alongside physical and spiritual movements. Every tiny detail was taken care of, and the entire time was tailored to suit my exact needs. I was new to Tai Chi and Chi Gong and through the amazing teaching of Nas, I now couldn’t imagine it not being a part of my every day - the effects on my daily life are beyond words!! The entire experience was one of a kind and one that I sincerely hope to repeat. Nas and Julie are at the centre of this amazing programme, and it’s due to their dedication and experience that the teaching comes through so well, I absolutely cannot recommend enough!!</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Lottie, London</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>We spent a fortnight with Nas and Julie last April. We stayed in a beautiful villa right by the sea surrounded by wild spring flowers, by the scent of jasmine and by birdsong with the Mountains of kefalonia on the horizon: an idyllic setting. We learned a routine of Qi Gong moves that we practiced each morning, discovering how breath and movement bring forth calmness and vitality. We learned Tai Chi walking, a slow and sustained physical mediation that grounds and connects body and mind. We learned the first 14 postures of the long Tai Chi form, a sequence of flowing movement practice that offers ongoing inspiration and physical discipline. Amidst this wealth of nourishment, both physical and spiritual, it was Nas and Julie’s attentiveness and humanity that transformed this holiday from a pleasure into a revelation. We learned a way of living that has proved a turning point for me and my partner. Five weeks after leaving we continue to practice Qi Gong and Tai Chi in our local park each morning. Our two weeks with NaturalZest has been an inspiring and enduring experience. If you are seeking health, happiness and well being, Nas and Julie will lead you well on your way.</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Sara, London</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>My partner and I recently visited NaturalZest for two weeks and the experience has genuinely improved our lives.  Nas is friendly,open and With great patience, skill and clarity introduced us to Tai Chi and Qi Gong, something neither of us had experienced before. This physical work has given us an exercise program which is natural, safe, healthy and life enhancing. Now, every morning we get up, go to the park and begin the day by doing our Qi Gong warm up and our Tai Chi routine to the sun, enjoying the bird song and the beautiful trees in the park!! If you despair of going to the gym and suffering injuries then the NaturalZest experience is for you. We cannot recommend them too highly.</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Lawrence, Bath</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>Dear NaturalZest<br />
I want to thank you for a Wonderful Week. Nas, I really going to miss our Tai Chi / Qigong sessions. I immediately felt something 'clicking' in me when going through the exercises. You are a great teacher and your Energy is amazing and yet so calm! Wonderful!!!This was exactly what i had hoped for,so thanks again for making everything possible. I am going to miss you all - I feel like I may have friends for life, and I would love to stay in touch. If any of you should ever come to Denmark do let me know! I hope to see you again. Xx Kathrina Martinsen Denmark<br />
Doing Tai Chi with Nasos was a fantastic experience. Natural Zest has a lovely atmosphere and Nasos is a very professional and calm teacher. His caring way of passing on his knowledge makes you feel wonderful. I left each lesson with a clear mind feeling totally destressed. Grateful greetings.</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Vibeke, Norway</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>It's the best health and relaxation holiday that I have been on!!  The Tai Chi linked with meditation was inspirational!! Being on your balcony, feeling the warmth of the sun and having my own very patient 1:1 teacher encouraging me and telling me 'very nice' when I nearly get something right, was fantastic!!  Nas is a great teacher xx </p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Helen, Suffolk</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>Couple of month ago I experienced a little bit (just one week) of Tai Chi with Nasos. And today I´m feeling sensations I had, when we practiced!!! How light and happy I felt, how relaxed and at ease, how simple and natural, I really can hear the sounds around me, the air aorund and I can see that peaceful smile on my teacher! Yes I´m feeling it all today, when lots of days have passed... Thank You Nasos for guiding me there! !! When I think of Tai Chi, then I don´t think of exercises, what first comes into my mind is Nature, in all means: majestic mountains, the sound of birds (and frogs <i class="fa fa-smile-o"></i>, the beautiful sunset, the smell of a fresh moist of the evening, and a peaceful loving being in all that when moving softly through the air. This is Tai Chi for me!!! I thought I would write something short, but hey, I could go on and on. I guess I miss it...</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Katri Kaasik, Esthonia</div>
</div>
</div>

</blockquote></div>  </div>      </div>
          <div class="item ">
          
  <div class="views-field views-field-body">        <div class="field-content"><blockquote>
<div class="col-md-12">
<div class="row quote">
<div data-wow-duration="2s" class="wow fadeInLeftBig"><p>I spent a week with Nasos learning Tai Chi from scratch, 2 sessions a day and it's difficult to know where to start but I will say Nasos is the most patient and subtle teacher I have had for anything and the calm, energy and general well being (a relaxed, confident and greater awareness way) that tai chi can give you just has to be experienced to understand. I had a week and would like 6 months of it, I was looked after brilliantly! Nasos is surely a great teacher of Tai Chi and I absolutely loved it. Great location, great relaxed teaching from a great teacher who I now feel fortunate enough to call a friend, 11/10 for the whole experience, and something that can stay with you forever, my advice, just do it, you won't regret it, tai chi can only ever have positive impact I believe and I for one can't wait to go again and learn more!!!</p>
</div>
</div>
<div data-wow-duration="2s" class="wow fadeInRightBig">
<div class="row byline">-- Nick, Bristol</div>
</div>
</div>

</blockquote></div>  </div>      </div>
      </div>

      <!-- Carousel navigation -->
    <a class="carousel-control left" href="#views-bootstrap-carousel-1" data-slide="prev">
      <span class="icon-prev"></span>
    </a>
    <a class="carousel-control right" href="#views-bootstrap-carousel-1" data-slide="next">
      <span class="icon-next"></span>
    </a>
  </div>
    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
    </section>

    
  </div>
</div>

  <footer class="footer container-fluid">
      <div class="region region-footer well well-lg">
    <section id="block-block-1" class="block block-block clearfix">

      
  <div class="col-md-6 col-sm-12 sec-menu">
<ul class="footer-menu"><li><a href="/terms-conditions">Terms &amp; Conditions</a></li>
<!--<li><a href="#">Disclaimer</a></li>--><li><a href="/contact-us">Contact us</a></li>
</ul></div>
<div class="col-md-6 col-sm-12 footer-copyright">
<p>© Copyright 2019, Naturalzest.co.uk</p>
</div>

</section>
  </div>
  </footer>
  <script src="https://www.naturalzest.co.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
