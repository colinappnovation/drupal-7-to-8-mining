<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="en" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="en" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="en" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->

<head>
  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/themes/coachingclimate/favicon.png" type="image/png" />
<meta name="description" content="Sarah Gornall provides Executive Coaching and Coaching Supervision; helping make good decisions, achieve goals and improve work-life balance." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.coachingclimate.co.uk/" />
<link rel="shortlink" href="https://www.coachingclimate.co.uk/" />
<meta property="og:site_name" content="Coaching Climate" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.coachingclimate.co.uk/" />
<meta property="og:title" content="Coaching Climate" />
  <title>Coaching Climate | Executive Coaching and Supervision in Bristol</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <!--[if IEMobile]>
    <meta http-equiv="cleartype" content="on">
  <![endif]-->

  <link type="text/css" rel="stylesheet" href="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/css/css_f_bpoAzEk2t6A9CRRN6ZTSmbxBFY8VgWF29Mr5DntLM.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/css/css_Wu8npAzy16WmnnnWKxpexfgsAryolGGaX6yO3GWA5bU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/css/css_r6Ga_OqvcUXGkQ5cyxVqmViMNzszC2JPmm6AUSu8TjM.css" media="all" />
  <script src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/js/js_qikmINIYTWe4jcTUn8cKiMr8bmSDiZB9LQqvceZ6wlM.js"></script>
<script src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/js/js_8fJKAA2mETRfFI8QKtNqk6y1OftAOtzXE6XZ4o3gqZo.js"></script>
<script src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/js/js_gPqjYq7fqdMzw8-29XWQIVoDSWTmZCGy9OqaHppNxuQ.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-45839429-1", {"cookieDomain":"auto"});ga("send", "pageview");</script>
<script src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/js/js_QtXmu3XhowfeK2mB1uZKxVQHCRSRTYYmDOHxxICXmxc.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"coachingclimate","theme_token":"LMwuUWoy2_ZB6oTylAIuvyHpKDryDNrnpB8rM-iqzIM","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/coachingclimate.co.uk\/modules\/fd_responsivemenu\/fd_responsivemenu.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/js\/script.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/coachingclimate.co.uk\/modules\/fd_responsivemenu\/fd_responsivemenu.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/system.menus.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/system.messages.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/system.theme.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/styles.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/layouts\/responsive.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/components\/misc.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/fonts.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/print.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/menus.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/typography.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/ckeditor.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/forms.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/regions.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/blocks.css":1,"sites\/coachingclimate.co.uk\/themes\/coachingclimate\/css\/views.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});</script>
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5-respond.js"></script>
    <![endif]-->
  </head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-2 node-type-page colours-coaching-climate" >
      <p id="skip-link">
      <a href="#main-menu" class="element-invisible element-focusable" accesskey="S">Jump to navigation</a>
    </p>
      
<div id="page">

  <header class="header" id="header" role="banner">

          <a href="/" title="Home" rel="home" class="header__logo" id="logo"><img src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/themes/coachingclimate/logo.png" alt="Home" class="header__logo-image" /></a>
    
    
    
      <div class="header__region region region-header">
    <div id="block-block-3" class="block block-block contact first odd">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <p>0117 330 2017</p>
      </div>
    </div>
  </div>
</div>
<div id="block-block-2" class="block block-block smartphone-hide slogan even">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <p>Clarity, Integrity, Growth</p>
      </div>
    </div>
  </div>
</div>
<div id="block-block-4" class="block block-block desktop-hide toggle-menu last odd">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <p><a class="menu-toggle" href="#navigation">Menu</a></p>
      </div>
    </div>
  </div>
</div>
  </div>

  </header>

  <div id="navigation">

    
      <div class="region region-navigation">
    <div id="block-system-main-menu" class="block block-system block-menu first last odd" role="navigation">
  <div class="gutter">
    <div class="inner">
                    <h2 class="block__title block-title">Main menu</h2>
                  <div class="content">
        <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/" accesskey="1" class="menu__link active">Home</a></li>
<li class="menu__item is-expanded expanded"><a href="/why-have-coaching" class="menu__link">Coaching</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/why-have-coaching" class="menu__link">Why?</a></li>
<li class="menu__item is-leaf leaf"><a href="/how-does-coaching-work" class="menu__link">How?</a></li>
<li class="menu__item is-leaf last leaf"><a href="/coaching-testimonials" class="menu__link">Testimonials</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/why-have-supervision" class="menu__link">Supervision</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/why-have-supervision" class="menu__link">Why?</a></li>
<li class="menu__item is-leaf leaf"><a href="/how-does-supervision-work" class="menu__link">How?</a></li>
<li class="menu__item is-leaf last leaf"><a href="/supervision-testimonials" class="menu__link">Testimonials</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/sarah-gornall-pcc" class="menu__link">Support</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/sarah-gornall-pcc" class="menu__link">Sarah</a></li>
<li class="menu__item is-leaf leaf"><a href="/books" class="menu__link">Books</a></li>
<li class="menu__item is-leaf leaf"><a href="/resources" class="menu__link">Resources</a></li>
<li class="menu__item is-leaf last leaf"><a href="/blog" accesskey="2" class="menu__link">Blog</a></li>
</ul></li>
<li class="menu__item is-leaf last leaf"><a href="/contact-us" accesskey="9" class="menu__link">Contact us</a></li>
</ul>      </div>
    </div>
  </div>
</div>
  </div>

  </div>

    <div class="banner" id="banner" role="banner">
      <div class="region region-banner">
    <div id="block-menu-menu-social-media-links" class="block block-menu social-media first odd" role="navigation">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="http://uk.linkedin.com/in/sarahgornall" class="menu__link linkedin">LinkedIn</a></li>
<li class="menu__item is-leaf leaf"><a href="http://www.sarahgornall.com/blog/" class="menu__link blog">Blog</a></li>
<li class="menu__item is-leaf last leaf"><a href="mailto:info@coachingclimate.co.uk" class="menu__link email">Email</a></li>
</ul>      </div>
    </div>
  </div>
</div>
<div id="block-views-banners-block-1" class="block block-views smartphone-hide last even">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <div class="view view-banners view-id-banners view-display-id-block_1 view-dom-id-d3fe73f89b5357b3aa868591337c5dc6 views-row-count-1">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 Left">
    <div class="gutter">
  <div class="inner">
          
      <div class="views-field views-field-field-image">                <div class="field-content"><img typeof="foaf:Image" src="https://www.coachingclimate.co.uk/sites/coachingclimate.co.uk/files/styles/banner/public/banner-images/balloons.jpg?itok=4JIaJDsq" width="917" height="156" alt="" /></div>      </div>          
      <div class="views-field views-field-body">                <div class="field-content"></div>      </div>      </div>
</div>
  </div>
    </div>
  
  
  
  
  
  
</div>      </div>
    </div>
  </div>
</div>
  </div>
  </div>
    
  
  <div id="main">

    <div id="content" class="column" role="main">
      <ul class="accreditations">
        <li class="pcc">PCC</li>
        <li class="aocs">AOCS</li>
      </ul>
                  <a id="main-content"></a>
                    <h1 class="page__title title" id="page-title">Home</h1>
                                          


<article class="node-2 node node-page view-mode-full clearfix" about="/home" typeof="foaf:Document">      <header>                  <span property="dc:title" content="Home" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>          </header>    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p><span class="large">Welcome</span> to Coaching Climate</p>
</div></div></div>    </article>            
            <div class="postscript" id="postscript">
          <div class="region region-postscript">
    <div id="block-views-postscript-blocks-block-1" class="block block-views first last odd">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <div class="view view-postscript-blocks view-id-postscript_blocks view-display-id-block_1 view-dom-id-9c708c1389dd8524c023e56aed443d3e views-row-count-3">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 colours-1">
    <div class="gutter">
  <div class="inner">
          
      <h2>                <span>Coaching</span>      </h2>          
      <div class="views-field views-field-body">                <div class="field-content"><ul><li>
		<a href="/why-have-coaching">Why?</a></li>
	<li>
		<a href="/how-does-coaching-work">How?</a></li>
	<li>
		<a href="/coaching-testimonials">Testimonials</a></li>
</ul><p>
	Coaching helps people make good decisions, achieve their goals and improve their work-life balance.</p>

<p>
	"We have managers who are 100% more capable than they were last year"</p></div>      </div>          
      <div class="views-field views-field-field-link-destination-reference">                <div class="field-content"><a href="/why-have-coaching">&gt;Read more</a></div>      </div>      </div>
</div>
  </div>
  <div class="views-row views-row-2 colours-2">
    <div class="gutter">
  <div class="inner">
          
      <h2>                <span>Supervision</span>      </h2>          
      <div class="views-field views-field-body">                <div class="field-content"><ul><li>
		<a href="/why-have-supervision">Why?</a></li>
	<li>
		<a href="/how-does-supervision-work">How?</a></li>
	<li>
		<a href="/supervision-testimonials">Testimonials</a></li>
</ul><p>
	Supervision is for coaches who want to devleop their business, gain clarity and improve their skills.</p>

<p>
	"She straightens my head out which helps me move my own business forward."</p></div>      </div>          
      <div class="views-field views-field-field-link-destination-reference">                <div class="field-content"><a href="/why-have-supervision">&gt;Read more</a></div>      </div>      </div>
</div>
  </div>
  <div class="views-row views-row-3 colours-10">
    <div class="gutter">
  <div class="inner">
          
      <h2>                <span>Support</span>      </h2>          
      <div class="views-field views-field-body">                <div class="field-content"><ul><li>
		<a href="/sarah-gornall-pcc">Sarah Gornall</a></li>
	<li>
		<a href="/books">Books</a></li>
	<li>
		<a href="/resources">Resources</a></li>
</ul><p>
	Creating spaces of calm reflection where clients can explore new thoughts and grow.</p>

<p>
	Author of 4 books, including "How to Work with People... and Enjoy It!" and President (2018-19) of UK ICF</p></div>      </div>          
      <div class="views-field views-field-field-link-destination-reference">                <div class="field-content"><a href="/sarah-gornall-pcc">&gt;Read more</a></div>      </div>      </div>
</div>
  </div>
    </div>
  
  
  
  
  
  
</div>      </div>
    </div>
  </div>
</div>
  </div>
      </div>
          </div>

    
    
  </div>

    <footer id="footer" class="region region-footer">
    <div id="block-block-5" class="block block-block contact first odd">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <p>©2014 Coaching Climate Ltd <span class="white">T</span> 0117 330 2017 <span class="white">E</span> <a href="mailto:info@coachingclimate.co.uk">info@coachingclimate.co.uk</a></p>
      </div>
    </div>
  </div>
</div>
<div id="block-menu-menu-footer-links" class="block block-menu last even" role="navigation">
  <div class="gutter">
    <div class="inner">
                        <div class="content">
        <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/accessibility" accesskey="0" class="menu__link">Accessibility</a></li>
<li class="menu__item is-leaf last leaf"><a href="/cookies-our-website" class="menu__link">Cookies on our website</a></li>
</ul>      </div>
    </div>
  </div>
</div>
  </footer>

</div>

  </body>
</html>
