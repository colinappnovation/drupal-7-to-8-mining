
<!doctype html>
<html>
<head>
<meta name="google-site-verification" content="gh7aQaaNnbH-AXRFDlNVqxM-R-PJsqK83HjAEnXqL90" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<title>We Hear You Cancer Counselling</title>
<meta name="description" content="We Hear You (WHY) provides free, professional counselling to anyone affected or bereaved by cancer or other life threatening conditions." />
<meta property="og:title" content="We Hear You Cancer Counselling" />
<meta property="og:type" content="website" />
<meta property="og:description" content="We Hear You (WHY) provides free, professional counselling to anyone affected or bereaved by cancer or other life threatening conditions." />
<meta property="og:url" content="https://www.wehearyou.org.uk" />
<meta property="og:locale" content="en_GB" />
<meta property="og:site_name" content="We Hear You" />
<link rel="stylesheet" type="text/css" href="/_webfonts/LL_Brown_Regular_Web/LL Brown-Regular Web/css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="/_webfonts/LL_Brown_Bold_Web/LL Brown-Bold Web/css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="/_css/reset.css"/>
<link rel="stylesheet" type="text/css" href="/_css/fonts.css"/>
<script type="text/javascript" src="/_js/respond.min.js"></script>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="/_js/jquery.js"></script>
<script type="text/javascript" src="/_js/easing.js"></script>
<script type="text/javascript" src="/_js/jquery.mobile.js"></script>
<script type="text/javascript" src="/_js/jquery.inputfit.js"></script>
<script type="text/javascript" src="/_js/fancyBox/source/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="/_js/fancyBox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="/_js/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" type="text/css" href="/_css/styles.css" />
<link rel="stylesheet" type="text/css" href="/_js/swiper/swiper.css"/>
<script type="text/javascript" src="/_js/swiper/swiper.min.js"></script>
</head>
<body>
<!-- start of mobile navigation -->
<div id="mobileNav" class="mobileOnly">
<div class="content">
<nav>
<ul class="nav">
<li><a href="/">Home</a></li>
<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">About WHY</a>
<ul>
<li class="first"><a href="/professional-cancer-counselling/" >What We Do</a></li>
<li><a href="/the-why-team/" >The WHY Team</a></li>
<li><a href="/trustees/" >Trustees</a></li>
<li><a href="/founder/" >The Founder</a></li>
<li><a href="/recruitment/" >Recruitment</a></li>
<li class="last"><a href="/reports/" >Reports</a></li>
</ul>
</li>
<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">Get Support</a>
<ul class="group">
    <li class="first"><a href="/cancer-support/" >Counselling</a></li>
    <li><a href="/cancer-counselling-for-adults/" >For Adults</a></li>
    <li><a href="/cancer-counselling-for-young-people/" >For Children &amp; Young People</a></li>
    <li><a href="/cancer-counselling-group-therapy/" >Group Therapy</a></li>
    <li><a href="/cancer-counselling-online/" >Online Counselling</a></li>
    <li><a href="/art-and-drama-therapy/" >Art and Drama Therapy</a></li>
    <li><a href="/other-therapies/" >Other Therapies</a></li>
    <li class="last"><a href="/cancer-counselling-venues/" >Counselling Venues</a></li>
</ul>
</li>

<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">Get Involved</a>
<ul>
<li class="first"><a href="/cancer-charity-fundraising/" >Fundraising</a></li>
<li><a href="/25th-anniversary/" >25th Anniversary</a></li>
<li><a href="/cancer-charity-volunteering/" >Volunteering</a></li>
<li><a href="/donate/" >Make a Donation</a></li>
<li><a href="/regular-giving/" >Regular Giving</a></li>
<li><a href="/corporate-giving/" >Corporate Giving</a></li>
<li><a href="/remember-someone/" >Remember Someone</a></li>
<li class="last"><a href="/_login/">Login / Register</a></li>
</ul>
<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">Real Life Stories</a>
<ul>
<li class="first"><a href="/experiences/" >Service User Stories</a></li>
<li><a href="/fundraiser-stories/" >Fundraiser Stories</a></li>
<li class="last"><a href="/testimonials/" >Testimonials</a></li>
</ul>
</li>
<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">Events</a>
<ul>
<li class="first"><a href="/auction/" >Charity Auction</a></li>
<li><a href="/events/" >Challenge Events</a></li>
<li class="last"><a href="/fundraising/" >Community Events</a></li>
</ul>
</li>
<li><a href="/news/" >News</a></li>
<li class="parentnav"><a href="javascript:void(0)" class="subnavopener ">Shop</a>
<ul>
<li class="first"><a href="/search/" >Products</a></li>
<li><a href="/_basket/" >View Basket</a></li>
<li><a href="/_checkout/" >Checkout</a></li>
<li class="last"><a href="/_account/">My Account</a></li>
</ul>
</li>
<li><a href="/contact/" >Contact us</a></li>
</ul>
<ul class="nav2">
<li class="help"><a href="/cancer-support/">Get Support</a></li>
<li class="donate"><a href="/donate/">Donate Online</a></li>
</ul>
<ul class="nav2">
<li class="account"><a href="/_login/">Login / Register</a></li>
</ul>
<ul class="nav2">
<li class="twitter"><a href="https://twitter.com/WHYwehearyou" target="_blank">Follow us on Twitter</a></li>
<li class="facebook"><a href="http://www.facebook.com/pages/Positive-Action-on-Cancer/122946604460009" target="_blank">Find us on Facebook</a></li>
<li class="instagram"><a href="https://www.instagram.com/why_wehearyou/" target="_blank">Follow us on Instagram</a></li>
</ul>
<div class="mbnavfooter">
<p>We Hear You<br/>
First Floor<br>Frome Town Hall<br>Christchurch Street West<br>Frome<br>Somerset<br>BA11 1EB</p>
<p>Tel: 01373 455 255<br/>
Email: <a href="mailto:info@wehearyou.org.uk">info@wehearyou.org.uk</a></p>
<p>Charitable Incorporated Organisation<br/>
Registered charity no: 1156001</p>
</div>
</div>
</div>
<!-- end of mobile navigation -->

<!-- start of mobile header -->
<div id="mobileheader" class="group mobileOnly">
    <p class="mobileh1"><a href="/"><img src="/_images/mobilelogo.png" width="112" height="60" /></a><span class="titletext">PAC</span></p>
    <div id="menubuttons" class="stopshrinking">
        <ul class="group">
            <li class="menu"><a href="#mobileNav" class="impatient strong">Menu</a></li>
            <li class="rightbuttons">
            <ul class="group">
            <li><a href="/donate/"><img src="/_images/donate.png" width="70" height="70" alt="Donate" /></a></li>
            <li><a href="/contact/"><img src="/_images/call.png" width="70" height="70" alt="Contact Us" /></a></li>
            </ul>
            </li>
        </ul>
	</div>
</div>
<div class="mobileheaderpad mobileOnly"></div>
<!-- end of mobile header -->

<div class="pagewrap">
<div class="dtonly">
<div class="headerpad"></div>
</div>
<div class="headerwrap dtonly">
	<div class="r1wrap">
    	<div class="contain_1220 group">
        	<h1><a href="/">Why Cancer Counselling</a></h1>
            <ul class="bold group">
            <li><span class="drop">e:&nbsp;</span><a href="mailto:info@wehearyou.org.uk">info@wehearyou.org.uk</a></li>
            <li>t: 01373 455 255</li>
            <li class="facebook"><a href="http://www.facebook.com/pages/Positive-Action-on-Cancer/122946604460009" title="Find us on Facebook" target="_blank">Find us on Facebook</a></li>
            <li class="twitter"><a href="https://twitter.com/WHYwehearyou" title="Follow us on Twitter" target="_blank">Follow us on Twitter</a></li>
            <li class="instagram"><a href="https://www.instagram.com/why_wehearyou/" target="_blank">Follow us on Instagram</a></li>
             <li class="olinks"><a href="/_basket/">View Basket <span id="btqty">(0)</span></a><a href="/_checkout/">Checkout</a><a href="/_account/">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="r2wrap">
    	<div class="contain_1220 group">
        <ul>
        <li class="lb">Cancer Counselling Somerset</li>
        <li class="opt opt1"><a href="/">Home</a></li>
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener " data-subnav="subnav4">About<span class="opt opt2">&nbsp;WHY</span><span class="ar"></span></a></li>
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener " data-subnav="subnav5">Get Support<span class="ar"></span></a></li>
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener " data-subnav="subnav1">Get Involved<span class="ar"></span></a></li>
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener " data-subnav="subnav2"><span class="opt opt3">Real&nbsp;</span>Life Stories<span class="ar"></span></a></li>        
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener " data-subnav="subnav6">Events<span class="ar"></span></a></li> 
        <li><a href="/news/" >News</a></li>
        <li class="parentnav"><a href="javascript:void(0)" class="subnavopener  " data-subnav="subnav3">Shop<span class="ar"></span></a></li>
        <li class="last"><a href="/contact/" >Contact</a></li>
        </ul>
        </div>
    </div>
    <div class="subnav" id="subnav1">
    	<div class="contain_1220">
        	<ul class="group">
            <li class="first"><a href="/cancer-charity-fundraising/" >Fundraising</a></li>
            <li><a href="/25th-anniversary/" >25th&nbsp;Anniversary</a></li>
            <li><a href="/cancer-charity-volunteering/" >Volunteering</a></li>
            <li><a href="/donate/" >Donations</a></li>
            <li><a href="/regular-giving/" >Regular Giving</a></li>
            <li><a href="/corporate-giving/" >Corporate&nbsp;Giving</a></li>
            <li class="last"><a href="/remember-someone/" >Remember&nbsp;Someone</a></li>
            </ul>
        </div>
    </div>
    <div class="subnav" id="subnav2">
    	<div class="contain_1220">
        	<ul class="group">
            <li class="first"><a href="/experiences/" >Service User Stories</a></li>
            <li><a href="/fundraiser-stories/" >Fundraiser Stories</a></li>
            <li><a href="/testimonials/" >Testimonials</a></li>
            </ul>
        </div>
    </div>
    <div class="subnav" id="subnav3">
    	<div class="contain_1220">
        	<ul class="group">
            <li class="first"><a href="/search/" >Products</a></li>
            <li><a href="/_basket/" >View Basket</a></li>
            <li><a href="/_checkout/" >Checkout</a></li>
            <li class="last"><a href="/_account/">My Account</a></li>
            </ul>
        </div>
    </div>
    <div class="subnav" id="subnav4">
    	<div class="contain_1220">
            <ul class="group">
                <li class="first"><a href="/professional-cancer-counselling/" >What We Do</a></li>
                <li><a href="/the-why-team/" >The WHY Team</a></li>
                <li><a href="/trustees/" >Trustees</a></li>
                <li><a href="/founder/" >The Founder</a></li>
                <li><a href="/recruitment/" >Recruitment</a></li>
                <li class="last"><a href="/reports/" >Reports</a></li>
            </ul>
    	</div>
    </div>
    
    <div class="subnav" id="subnav5">
    	<div class="contain_1220">
            <ul class="group">
                <li class="first"><a href="/cancer-support/" >Counselling</a></li>
                <li><a href="/cancer-counselling-for-adults/" >For Adults</a></li>
                <li><a href="/cancer-counselling-for-young-people/" >For Children &amp; Young People</a></li>
                <li><a href="/cancer-counselling-group-therapy/" >Group Therapy</a></li>
                <li><a href="/cancer-counselling-online/" >Online Counselling</a></li>
                <li><a href="/art-and-drama-therapy/" >Art and Drama Therapy</a></li>
                <li><a href="/other-therapies/" >Other Therapies</a></li>
                <li class="last"><a href="/cancer-counselling-venues/" >Counselling Venues</a></li>
            </ul>
    	</div>
    </div>
    
    <div class="subnav" id="subnav6">
    	<div class="contain_1220">
            <ul class="group">
            <li class="first"><a href="/events/" >Challenge Events</a></li>
            <li class="last"><a href="/fundraising/" >Community Events</a></li>
            </ul>
    	</div>
    </div>
    
   
    
    <div class="tagswrap contain_1220">
    <div class="tags">
        <ul class="group">
        <li class="tag1"><a href="/cancer-support/">Need Help?</a></li>
        <li class="tag2"><a href="/donate/">Donate Online</a></li>
        </ul>
     </div>
     </div>
    
</div>
<!-- full width slideshow -->
<div class="contain_1440" id="slideshowwrap5">
<div class="slideshowloader"><img src="/_images/loader.png" width="64" height="64" alt="loading" /></div>
<div class="swiper-container containerpad" id="slideshow5">

<ul class="slideshownav group" id="slideshownav5">
    <li class="left"><a href="javascript:void(0)">Prev</a></li>
    <li class="right"><a href="javascript:void(0)">Next</a></li>
</ul>

<div class="swiper-wrapper">

<div class="swiper-slide"><img src="/_pageAssets/5/31/1440.png" alt="We Hear You Cancer Counselling" width="100%" /></div>

<div class="swiper-slide"><img src="/_pageAssets/5/33/1440.png" alt="We Hear You Cancer Counselling" width="100%" /></div>

<div class="swiper-slide"><img src="/_pageAssets/5/45/1440.png" alt="We Hear You Cancer Counselling" width="100%" /></div>

<div class="swiper-slide"><img src="/_pageAssets/5/46/1440.png" alt="We Hear You Cancer Counselling" width="100%" /></div>

</div>

<div class="bullets"></div>

</div>
</div>
<script>
$("#slideshow5").hide();
$("#slideshowwrap5 .slideshowloader").show();
$(document).ready(function() {
	$("#slideshowwrap5 .slideshowloader").hide();
	$("#slideshow5").show();
	$(function(){
	var mySwiper5 = new Swiper('#slideshow5', {
	mode:'horizontal',
	loop: true,
	autoplay: 7000,
	
	pagination: '#slideshow5 .bullets',
	
	paginationClickable: true,
	createPagination: true,
	calculateHeight: true,
	eventTarget: 'container'
	});
	$('#slideshownav5 .left a').click(function(){mySwiper5.swipePrev()})
	$('#slideshownav5 .right a').click(function(){mySwiper5.swipeNext()})
	})
});
</script>
<!-- end -->

<!-- home special -->
<div class="twocolhome textblockwrap containerpad">
	<div class="contain_1220">
    	<div class="colwrap">
        	
            <div class="col col1">
            
                <div class="text stdtext">
                    <h1 class="title"><span class="purple">We Hear You &ndash; Cancer Counselling</span></h1>

<p class="intro">We Hear You works across Bath and North East Somerset, Somerset and Wiltshire providing emotional support to patients, families, friends and carers who have been affected&nbsp;by cancer or any other life threatening condition.</p>

<p>At We Hear you we understand that cancer and life threatening conditions can be overwhelming. People who&nbsp;use our service often report feeling numb, isolated and terrified and are struggling to cope. We provide a safe space where people can say the unsayable and ask the unanswerable. When you ask WHY&hellip; we hear you.</p>

<p>If you are affected or have been bereaved by cancer or another life threatening condition&nbsp;and would like to know more about our counselling, <a href="http://www.wehearyou.org.uk/cancer-support/">please see our &#39;Get Support&#39; pages</a>.</p>

                    <h2 class="purpletitle">How You Can Help Us</h2>
                    <ul class="helpbullets">
                        <li class="b1"><span class="subtitle">Fundraising</span><br/>There are many ways that you can support and fundraise for We Hear You. We can help to support your fundraising activities by writing or distributing a press release for your event, working with you to create a poster and social media graphics and offering other forms of practical help. Our Fundraising Pack will give you ideas and practical help with planning your event. We can also promote your activity on our website and through our social media channels. Remember to follow us on Facebook, Twitter and Instagram and share your photos with us.</li>
                        <li class="b2"><span class="subtitle">Volunteering</span><br/>Volunteering can be a lot of fun and it’s a good way of making new friends, gaining new skills and supporting your local community. There are a number of ways in which you can volunteer to help support our work.</li>
                        <li class="b3"><span class="subtitle">Make a Donation</span><br/>We receive no central government funding and can only continue to provide a lifeline to those who need it because of the generous support of people like you. Making a donation, however big or small, is a great way of supporting the vital work we do and allowing us to continue.</li>
                    </ul>
                </div>
            
        	</div>
            <div class="col col2">
                <div class="eventsbox group">
                    <h2><a href="/events/">Events<span class="right">view all</span></a></h2>
                    
                    <div class="mfloat">
                        <ul class="event first equalheight">
                        	
                            <li class="image"><a href="/events/hadrians-wall-2020/" style="background-image:url('/_pageimages/3272/900.jpg')">Hadrians Wall June 2020</a></li>
                            
                            <li class="eTitle">Hadrians Wall June 2020</li>
                            <li class="eLoc"></li>
                            <li class="eDate"> on 26 June 2020</li>
                            <li class="eButton"><a href="/events/hadrians-wall-2020/" class="button buttongrey">More Information</a></li>
                        </ul>
                    </div>
                    
                    <div class="mfloat">
                        <ul class="event last equalheight">
                        	
                            <li class="image"><a href="/events/london-to-paris-bike-ride-july-2019/" style="background-image:url('/_pageimages/128/900.jpg')">London to Paris Bike Ride July 2020 - Tour De France Edition</a></li>
                            
                            <li class="eTitle">London to Paris Bike Ride July 2020 - Tour De France Edition</li>
                            <li class="eLoc">London to Paris </li>
                            <li class="eDate"> on 15 July 2020</li>
                            <li class="eButton"><a href="/events/london-to-paris-bike-ride-july-2019/" class="button buttongrey">More Information</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- end -->

<!-- latest news -->
<div class="containerpad">
<div class="threecolwrap greybg newswrap">
	<div class="contain_1240">
    
        <div class="stdtext">
        <h2 class="title">Latest News</h2><p>Read the latest news from We Hear You.&nbsp;</p>

        </div>
    
    
	<div class="col news">
	
    	<ul class="equalheight">
        
		<li class="image"><a href="/news/why-celebrate-the-life-of-founder-jill-miller/" style="background-image:url('/_pageimages/3280/900.jpg')">WHY celebrate the life of founder Jill Miller</a></li>
		
        <li class="nTitle">WHY celebrate the life of founder Jill Miller</li>
        <li class="nDate">30 October 2019</li>
        <li class="nSummary">We''re so very sad to hear of the death of our founder Jill Miller - our director and chair of trustees talk about her legacy and the huge difference she has made in our community</li>
        <li class="nButton"><a href="/news/why-celebrate-the-life-of-founder-jill-miller/" class="button">Read Article</a></li>
        </ul>
    
    </div>
    
    <div class="col news">
    
    	<ul class="equalheight">
        
		<li class="image"><a href="/news/we-hear-you-look-forward-to-a-busy-festive-season/" style="background-image:url('/_pageimages/3279/900.jpg')">We Hear You look forward to a busy festive season</a></li>
		
        <li class="nTitle">We Hear You look forward to a busy festive season</li>
        <li class="nDate">13 November 2019</li>
        <li class="nSummary">Join us for some festive fun at We Hear You and help us make it a bumper end to our 25th anniversary year.</li>
        <li class="nButton"><a href="/news/we-hear-you-look-forward-to-a-busy-festive-season/" class="button">Read Article</a></li>
        </ul>
    
    </div>
    
    <div class="col news">
    
    	<ul class="equalheight">
        
		<li class="image"><a href="/news/please-help-why-win-20-of-festive-profits-at-my-favourite-voucher-codes/" style="background-image:url('/_pageimages/3284/900.jpg')">Please help WHY win 20% of festive profits at My Favourite Voucher Codes</a></li>
		
        <li class="nTitle">Please help WHY win 20% of festive profits at My Favourite Voucher Codes</li>
        <li class="nDate">25 November 2019</li>
        <li class="nSummary">Please vote WHY and help us win 20% of profits from My Favourite Voucher Codes this Christmas</li>
        <li class="nButton"><a href="/news/please-help-why-win-20-of-festive-profits-at-my-favourite-voucher-codes/" class="button">Read Article</a></li>
        </ul>
   
    </div>
    
    
    </div>
</div>
</div>
<!-- end -->

<!-- text block -->
<div class="textblockwrap containerpad20">
	<div class="contain_1220">
        <div class="text stdtext">
        <h1 class="title">Life Stories</h1>

<p class="defaulttext">We are enormously grateful to people who have used our service and felt able to share their experiences of what this has meant for them and how it has helped. Thanks to Ben, Rachel, Sally and Pamela for sharing their stories. You can <a href="http://www.wehearyou.org.uk/experiences/">read more experiences of how our service has helped people<strong>&nbsp;</strong>here.</a><strong>&nbsp;</strong></p>

        </div>
        
        
    </div>
</div>
<!-- end -->

<!-- video -->
<div class="containerpad">
<div class="contain_1220">
<div class="videowrap">
	<div class="video">
    	<div class="image col" style="background-image:url('/_pageAssets/6491/900.jpg')"><a href="https://vimeo.com/345455912" class="fancybox-media"></a></div>
        <div class="text col">
        	<div class="stdtext">
            	<div class="pad">
                <h2 class="title">Ben and Rachel - We Heard You</h2><p>Here Ben and Rachel tell us about the important role counselling played for them when they were affected by cancer.</p>

<p>&nbsp;</p>

				<p><a href="https://vimeo.com/345455912" class="button fancybox-media">Watch Video</a></p>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end -->

<!-- video -->
<div class="containerpad">
<div class="contain_1220">
<div class="videowrap">
	<div class="video">
    	<div class="image col" style="background-image:url('/_pageAssets/268/900.jpg')"><a href="https://vimeo.com/200236792" class="fancybox-media"></a></div>
        <div class="text col">
        	<div class="stdtext">
            	<div class="pad">
                <h2 class="title">Sally's story</h2><p>We Hear You is a free professional one-to-one counselling service for anyone affected by a diagnosis of cancer or another life threatening condition. Here, Sally&nbsp;tells how WHY&#39;s&nbsp;service supported her some time after her cancer treatment ended.&nbsp;</p>

				<p><a href="https://vimeo.com/200236792" class="button fancybox-media">Watch Video</a></p>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end -->

<!-- video -->
<div class="containerpad">
<div class="contain_1220">
<div class="videowrap">
	<div class="video">
    	<div class="image col" style="background-image:url('/_pageAssets/78/900.jpg')"><a href="https://vimeo.com/154080083" class="fancybox-media"></a></div>
        <div class="text col">
        	<div class="stdtext">
            	<div class="pad">
                <h2 class="title">Pamela's Story</h2><p>At We Hear You we provide emotional support to anyone affected by cancer &ndash; a patient, family member or friend. Here you can view a story from Pamela, whose husband had cancer, who explains what having counselling meant to her and how she wished she had known about it earlier in her experiences.</p>

				<p><a href="https://vimeo.com/154080083" class="button fancybox-media">Watch Video</a></p>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end -->

<!-- testimonial -->
<div class="containerpad">
<div class="testimonialwrap">
	<div class="contain_1220">
	<div class="testmonial ">
    	<div class="text col">
        	<p>&ldquo;It’s not a cliché that your life changes after a cancer diagnosis. I found that my relationships with my body, my friends and family and the wider world all changed. I was anxious and fearful that the cancer would come back. My sessions with We Hear You counsellor Jo helped me to move forward with my life in a positive way, to deal with stress and to help me see that it’s ok to feel scared, angry and confused. There is light at the end of the tunnel, and We Hear You helped me find it.&rdquo;</p>
        </div>
        
        <div class="image col"><div class="border"><img src="/_pageAssets/3/900.png" width="100%" alt="Cancer Charity" /></div></div>
		
    </div>
    </div>
</div>
</div>
<!-- end -->
<div class="subscribewrap">
	<div class="contain_940 stdtext">
    	<h2 class="title">Subscribe to our newsletter</h2>
        <p class="intro">Enter your email address below to receive news and events directly to your inbox.</p>
        <div class="subsmsg"></div>
        <div class="form">
        	<div class="col col1"><label for="semail">Email Address</label></div>
            <div class="col col2"><input type="text" id="semail" class="semail"></div>
            <div class="col col3"><a href="javascript:void(0)" class="subsbutton">Submit</a></div>
        </div>
        <p class="small">This information is being collected by We Hear You. It will only be used to contact you via email communication. To find out more please see our <a href='privacy' target='_blank'>privacy policy.</a></p>
    </div>
</div>

<div class="footerwrap">
	<div class="contain_1220">
    	<div class="footer">
        	<div class="col col1">
            	<ul>
                <li><strong>We Hear You</strong><br/>First Floor<br>Frome Town Hall<br>Christchurch Street West<br>Frome<br>Somerset<br>BA11 1EB</li>
                <li>t: 01373 455 255<br/>
                  e: <a href="mailto:info@wehearyou.org.uk">info@wehearyou.org.uk</a></li>
                <li class="smaller">Charitable Incorporated Organisation<br/>Registered charity no: 1156001</li>
                <li class="bla"><img src="/_images/footerlogos/logo1/bath-lifeproudfinalist.jpg" alt="Bath Life Awards Finalists" /></li><li class="bla"><img src="/_images/footerlogos/logo2/frreglogohr.jpg" alt="Registered with Fundraising Regulator" /></li>
                </ul>
            </div>
            <div class="col col2">
            <ul>
            <li class="footertitle">Site Links</li>
            </ul>
            <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/professional-cancer-counselling/">What We Do</a></li>
            <li><a href="/the-why-team/">The WHY Team</a></li>
            <li><a href="/founder/">The Founder</a></li>
            <li><a href="/recruitment/">Recruitment</a></li>
            <li><a href="/reports/">Reports</a></li>
            <li><a href="/cancer-support/">Cancer Counselling</a></li>
            <li><a href="/cancer-counselling-for-adults/">Cancer Counselling For Adults</a></li>
            <li><a href="/cancer-counselling-for-young-people/">Cancer Counselling For Children &amp; Young People</a></li>
            <li><a href="/cancer-counselling-online/">Online Cancer Counselling</a></li>
            <li><a href="/cancer-counselling-venues/">Cancer Counselling Venues</a></li>
            <li><a href="/cancer-charity-fundraising/">Fundraising</a></li>
            <li><a href="/25th-anniversary/">25th Anniversary</a></li>
            <li><a href="/cancer-charity-volunteering/">Volunteering</a></li>
            <li><a href="/donate/">Make a Donation</a></li>
            <li><a href="/regular-giving/">Regular Giving</a></li>
            <li><a href="/corporate-giving/">Corporate Giving</a></li>
            <li><a href="/remember-someone/">Remember Someone</a></li>
            <li><a href="/experiences/">Real Life Stories</a></li>
            <li><a href="/testimonials/">Testimonials</a></li>
            <li><a href="/tributes/">Tributes</a></li>
            <li><a href="/events/">Challenge Events</a></li>
			<li><a href="/fundraising/">Community Events</a></li>
            <li><a href="/news/">News</a></li>
            <li><a href="/search/">Products</a></li>
            <li><a href="/contact/">Contact us</a></li>
            </ul>
            <ul>
            <li><a href="https://twitter.com/WHYwehearyou" target="_blank">Follow us on Twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Positive-Action-on-Cancer/122946604460009" target="_blank">Find us on Facebook</a></li>
            <li><a href="https://www.instagram.com/why_wehearyou/" target="_blank">Follow us on Instagram</a></li>
            </ul>
            </div>
            <div class="col col3">
            <ul>
            <li class="footertitle">Legal</li>
            </ul>
            <ul>
            <li><a href="/policies-and-practice/">Policies and Practice</a></li>
            <li><a href="/terms/">Terms of Use</a></li>
			<li><a href="/privacy/">Privacy Policy</a></li>
			</ul>
            <ul>
            <li class="copyright">&copy;2019 We Hear You, all rights reserved.</li>
			</ul>
            </div>
        </div>
    </div>
</div>
</div>
<div class="loader"></div><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-73388241-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript" src="/_js/pageslide2.js"></script>
<script type="text/javascript" src="/_pagejs/sitewide.js"></script>
</body>

</html>
