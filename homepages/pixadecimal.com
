<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-GB">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-GB">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en-GB">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="pixadecimal.com">

    <title>Pixadecimal :: Better Applications By Design</title>

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="https://www.pixadecimal.com/xmlrpc.php">

    <link rel="shortcut icon" href="https://www.pixadecimal.com/wp-content/themes/uhura/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="https://www.pixadecimal.com/wp-content/themes/uhura/favicon.ico" type="image/x-icon" />

    <link href="https://www.pixadecimal.com/wp-content/themes/uhura/assets/css/bootstrap.css" rel="stylesheet">
    <link href="https://www.pixadecimal.com/wp-content/themes/uhura/style.css" rel="stylesheet">

    <link rel='dns-prefetch' href='//s.w.org' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.pixadecimal.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.12"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='bootstrap.min-css'  href='https://www.pixadecimal.com/wp-content/themes/uhura/assets/css/bootstrap.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='https://www.pixadecimal.com/wp-content/themes/uhura/style.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-bootstrap-style-css'  href='https://www.pixadecimal.com/wp-content/plugins/bootstrap-for-contact-form-7/assets/dist/css/style.min.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.pixadecimal.com/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.9.12' type='text/css' media='all' />
<link rel='stylesheet' id='social_comments-css'  href='https://www.pixadecimal.com/wp-content/plugins/social/assets/comments.css?ver=3.1.1' type='text/css' media='screen' />
<link rel='stylesheet' id='wp-syntax-css-css'  href='https://www.pixadecimal.com/wp-content/plugins/wp-syntax/css/wp-syntax.css?ver=1.1' type='text/css' media='all' />
<script type='text/javascript' src='https://www.pixadecimal.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/google-analyticator/external-tracking.min.js?ver=6.5.4'></script>
<link rel='https://api.w.org/' href='https://www.pixadecimal.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.pixadecimal.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.pixadecimal.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.12" />
<meta name="description" content="Better Applications by Design" />
		<style type="text/css">
			div.wpcf7 .ajax-loader {
				background-image: url('https://www.pixadecimal.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif');
			}
		</style>
		<!-- Google Analytics Tracking by Google Analyticator 6.5.4: http://www.videousermanuals.com/google-analyticator/ -->
<script type="text/javascript">
    var analyticsFileTypes = [''];
    var analyticsSnippet = 'disabled';
    var analyticsEventTracking = 'enabled';
</script>
<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-39366574-1', 'auto');
	ga('require', 'linkid', 'linkid.js');
 
	ga('send', 'pageview');
</script>
</head>

<body>

<div class="container">


    <!-- Main Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Company Masthead -->
            <img class="masthead" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/masthead.png" alt=" ">
        </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="menu-primary" class="nav navbar-nav"><li id="menu-item-18" class="divider-right divider-color-nav menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-18 active"><a title="overview" href="http://www.pixadecimal.com">overview</a></li>
<li id="menu-item-19" class="divider-right divider-left divider-color-nav menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a title="latest news" href="http://www.pixadecimal.com/news/">latest news</a></li>
<li id="menu-item-57" class="divider-right divider-left divider-color-nav menu-item menu-item-type-custom menu-item-object-custom menu-item-57"><a title="our products" href="http://www.pixadecimal.com/applications">our products</a></li>
<li id="menu-item-20" class="divider-left divider-color-nav menu-item menu-item-type-custom menu-item-object-custom menu-item-20"><a title="about us" href="http://www.pixadecimal.com/about-us/">about us</a></li>
</ul>            </div>
    </nav>
    <!-- Main Navigation -->
    <!-- Header Content End-->
<!-- Homepage Content -->

<!-- Image Carousel-->
<div id="productCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
        <ol class="carousel-indicators">
	            <li data-target="#productCarousel" class='active' data-slide-to="0"></li>
	            <li data-target="#productCarousel" class='active' data-slide-to="1"></li>
	            <li data-target="#productCarousel" class='active' data-slide-to="2"></li>
	            <li data-target="#productCarousel" class='active' data-slide-to="3"></li>
	        </ol>

    <!-- Carousel Content -->
    <div class="carousel-inner" role="listbox">
                <div class="item active">
            <div class="container">
                <img class="img-responsive" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/carousel/morse.jpg" alt="Morse! The morse code app">
                <div class="carousel-caption">
                    <h2>Morse! The morse code app</h2>
                    <p>Have fun flashing at your friends and neighbours…</p>
	                <p><a class="btn btn-keyline" href="https://www.pixadecimal.com/applications/morse/" role="button">Learn more</a></p>

                </div>
            </div>
        </div>
                <div class="item ">
            <div class="container">
                <img class="img-responsive" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/carousel/raincheck.png" alt="Should I take my umbrella?">
                <div class="carousel-caption">
                    <h2>Should I take my umbrella?</h2>
                    <p>Don't get caught short in the rain again with Rain Check!</p>
	                <p><a class="btn btn-keyline" href="https://www.pixadecimal.com/news/im-singing-in-the-rain/" role="button">Learn more</a></p>

                </div>
            </div>
        </div>
                <div class="item ">
            <div class="container">
                <img class="img-responsive" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/carousel/site_launch.png" alt="It's ALIVE!!!">
                <div class="carousel-caption">
                    <h2>It's ALIVE!!!</h2>
                    <p>Welcome to the all new Pixadecimal.com. We hope you like what we've done with the place…</p>
	                <p><a class="btn btn-keyline" href="https://www.pixadecimal.com/news/were-live/" role="button">Learn more</a></p>

                </div>
            </div>
        </div>
                <div class="item ">
            <div class="container">
                <img class="img-responsive" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/carousel/lifeforce.png" alt="LifeForce 1.0">
                <div class="carousel-caption">
                    <h2>LifeForce 1.0</h2>
                    <p>If you love Magic the Gathering, check out our nifty game life utility for iOS</p>
	                <p><a class="btn btn-keyline" href="https://www.pixadecimal.com/applications/lifeforce/" role="button">Learn more</a></p>

                </div>
            </div>
        </div>
            </div>

    <!-- Carousel Navigation -->
    <a class="left carousel-control" href="#productCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#productCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- Image Carousel -->

<!-- News Content -->
<div class="row news">

    <div class="col-md-12">
        <!-- Featured News Article-->
                <h2>We&#8217;re live!</h2>
                    <p class="slim-image"><a href="https://www.pixadecimal.com/news/were-live/" rel="bookmark" title="We&#8217;re live!">
                    <img class="img-responsive" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/featured/site_launch.png" alt="We&#8217;re live!"/></a></p>
                <p>When you're a boutique agency time is precious. So it has taken a while for us to schedule in a site redesign. Fortunately we managed to tie it all down in a matter of weeks in-between projects. We like the new design and hopefully so will you, and while we still have a lot planned for the site, we also need to make sure client work is finished too. So this site is designed to scale and grow as we get time to add and update each new section.</p>
        <p class="clearfix news-divider"><a class="btn btn-default btn-pixdark pull-right" href="https://www.pixadecimal.com/news/were-live/" rel="bookmark" role="button">Continue Reading…</a></p>
    </div>
    
    <!-- Secondary News Articles -->
    
        <div class="col-sm-4 news-highlight">
            <h3>Dot dot dot, dash dash dash…</h3>
            <p>We&#8217;ve got another app out folks. this time it&#8217;s a simple fun, (but educational) app for iOS using Morse code &#8211; imaginatively named Morse! Taking advantage of your phone&#8217;s torch you can type a message, the same way you would an SMS. Morse! even converts it to &#8216;dots&#8217; and &#8216;dashes&#8217; and then uses your torch [&hellip;]</p>
            <p class="clearfix news-divider"><a class="btn btn-default btn-pixdark pull-right" href="https://www.pixadecimal.com/news/dot-dot-dot-dash-dash-dash/" rel="bookmark"  role="button">Continue Reading…</a></p>
        </div>
    
        <div class="col-sm-4 news-highlight">
            <h3>Can We Help?</h3>
            <p>While we are a small agency (2&#189; if you count Jpeg), we have a wealth of experience working on large scale projects as part of an integrated team, collaboration or just by ourselves. So if you want to know more or would like us to come and meet with you to discuss your requirements, then [&hellip;]</p>
            <p class="clearfix news-divider"><a class="btn btn-default btn-pixdark pull-right" href="https://www.pixadecimal.com/news/can-we-help/" rel="bookmark"  role="button">Continue Reading…</a></p>
        </div>
    
        <div class="col-sm-4 news-highlight">
            <h3>To The Mana Burn</h3>
            <p>When not working on finely crafted products and solutions here at Pixadecimal Towers, we like to play games. Lots of games. Board games, video games &amp; card games. One of our long standing favourites is Magic The Gathering and a few years ago Mike knocked together a quick prototype of a life counter app to [&hellip;]</p>
            <p class="clearfix news-divider"><a class="btn btn-default btn-pixdark pull-right" href="https://www.pixadecimal.com/news/to-the-mana-born/" rel="bookmark"  role="button">Continue Reading…</a></p>
        </div>
    </div>

    <!-- News End-->
    <!-- End of Content -->

    <script type='text/javascript' src='https://www.pixadecimal.com/wp-content/themes/uhura/assets/js/bootstrap.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.pixadecimal.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.5'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-includes/js/jquery/jquery.form.min.js?ver=4.2.1'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/bootstrap-for-contact-form-7/assets/dist/js/scripts.min.js?ver=1.4.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"https:\/\/www.pixadecimal.com\/wp-admin\/admin-ajax.php","hideEffect":"slide","onScroll":"yes","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"true","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":"1","refuse":"no","revoke_cookies":"0","revoke_cookies_opt":"automatic","secure":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.44'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var Sociali18n = {"commentReplyTitle":"Post a Reply"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/social/assets/social.js?ver=3.1.1'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-content/plugins/wp-syntax/js/wp-syntax.js?ver=1.1'></script>
<script type='text/javascript' src='https://www.pixadecimal.com/wp-includes/js/wp-embed.min.js?ver=4.9.12'></script>

			<div id="cookie-notice" role="banner" class="cn-bottom bootstrap" style="color: #000000; background-color: #ffffff;"><div class="cookie-notice-container"><span id="cn-notice-text">We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.</span><a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie cn-button bootstrap button">Ok</a>
				</div>
				
			</div><!-- Footer Content -->
<!-- Back to top banner -->
<div id="up" class="back-to-top">
    <h4>
        <a href="#top">⇪ back to top</a>
    </h4>
</div>

<!-- Footer -->
<footer class="footer footer-inverse">

    <div class="row">
        <!-- Explore -->
        <section class="col-md-12 explore-connect">
            <h4>Explore</h4>
            <ul id="menu-footer_explore" class="menu"><li id="menu-item-21" class="divider-right menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-21 active"><a title="home" href="http://www.pixadecimal.com/">home</a></li>
<li id="menu-item-22" class="divider-right menu-item menu-item-type-custom menu-item-object-custom menu-item-22"><a title="about us" href="http://www.pixadecimal.com/about-us/">about us</a></li>
<li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a title="privacy" href="https://www.pixadecimal.com/privacy/">privacy</a></li>
</ul>        </section>

        <!-- Connect -->
        <section class="col-md-12 explore-connect">
            <h4>Connect</h4>
            <ul id="menu-footer_connect" class="menu"><li id="menu-item-26" class="divider-right divider-color-nav menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a title="contact us" href="https://www.pixadecimal.com/contact/">contact us</a></li>
<li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27"><a title="rss" href="http://www.pixadecimal.com/feed/">rss</a></li>
</ul>            <ul id="menu-footer_social" class="menu"><li id="menu-item-28" class="divider-right menu-item menu-item-type-custom menu-item-object-custom menu-item-28"><a title="twitter" target="_blank" href="https://twitter.com/pixadecimal">twitter</a></li>
<li id="menu-item-29" class="divider-right menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a title="linkedin" target="_blank" href="https://www.linkedin.com/company/pixadecimal">linkedin</a></li>
<li id="menu-item-30" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a title="facebook" target="_blank" href="https://www.facebook.com/Pixadecimal-Ltd-209180139174235">facebook</a></li>
</ul>        </section>
    </div>

    <div class="row">

        <!-- Branding -->
        <div class="col-xs-12 col-sm-1 branding">
            <img class="logo" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/logo_mobile.png"/>
        </div>
        <div class="col-xs-12 col-sm-5 branding">
            <img class="company-name" src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/images/logo_full.png"/>
        </div>

        <!-- Social Media-->
        <section class="col-xs-12 col-sm-6 social">
            <ul class="social-links">
                <li><a id="twitter" href="https://twitter.com/pixadecimal" target="_blank"></a></li>
                <li><a id="facebook" href="https://www.facebook.com/Pixadecimal-Ltd-209180139174235" target="_blank"></a></li>
                <li><a id="linkedin" href="https://www.linkedin.com/company/pixadecimal" target="_blank"></a></li>
<!--                <li><a id="googleplus" href="#"></a></li>-->
                <li><a id="rss" href="/feeds"></a></li>
            </ul>

            <!-- Copyright Notice -->
            <div class="copyright">copyright &copy; 2007 - 2019 pixadecimal ltd.</div>
        </section>
    </div>
</footer>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://www.pixadecimal.com/wp-content/themes/uhura/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
<!-- Footer End -->
<!-- Dynamic page generated in 10.341 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-12-02 10:52:46 -->

<!-- Compression = gzip -->
<!-- super cache -->