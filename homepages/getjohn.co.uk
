<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="https://www.getjohn.co.uk/xmlrpc.php" />
<!--[if lt IE 9]><script src="https://www.getjohn.co.uk/wp-content/themes/fruitful/js/html5.js" type="text/javascript"></script><![endif]-->
<title>GetJohn&amp;Co &#8211; WE take on the hard stuff, linking computer systems together, automating e-commerce and selling online. Sometimes we blog about running a small business and personal development.</title>
<link rel='dns-prefetch' href='//www.getjohn.co.uk' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="GetJohn&amp;Co &raquo; Feed" href="https://www.getjohn.co.uk/feed/" />
<link rel="alternate" type="application/rss+xml" title="GetJohn&amp;Co &raquo; Comments Feed" href="https://www.getjohn.co.uk/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="GetJohn&amp;Co &raquo; Home Comments Feed" href="https://www.getjohn.co.uk/home/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.getjohn.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='ff-google-fonts-css'  href='//fonts.googleapis.com/css?family=Raleway%3A400%2C500%2C700&#038;ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-shortcodes-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/css/front.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_alert-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_alert/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_btn-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_btn/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_dbox-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_dbox/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_pbar-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_pbar/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-shortcodes-grid-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/libs/bootstrap/bootstrap-grid.min.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_recent_posts-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_recent_posts/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='slick-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/libs/slick/slick.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_recent_posts_slider-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_recent_posts_slider/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ff-fruitful_tabs-css'  href='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_tabs/css/styles.css?ver=071120180241' type='text/css' media='all' />
<link rel='stylesheet' id='ie-style-css'  href='https://www.getjohn.co.uk/wp-content/themes/fruitful/woocommerce/ie.css?ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='fn-box-style-css'  href='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/fnBox/jquery.fancybox.css?ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='fancy-select-css'  href='https://www.getjohn.co.uk/wp-content/themes/fruitful/css/fancySelect.css?ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='custom_fonts_5de4de1c77ae3-css'  href='https://fonts.googleapis.com/css?family=PT+Sans%3A400%2C400italic%2C700%2C700italic&#038;subset=latin%2Ccyrillic&#038;ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='custom_fonts_5de4de1c77aed-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C700italic%2C800italic%2C400%2C800%2C700%2C600%2C300&#038;subset=latin%2Clatin-ext&#038;ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='fontawesome-style-css'  href='https://www.getjohn.co.uk/wp-content/themes/fruitful/css/font-awesome.min.css?ver=5.3' type='text/css' media='all' />
<link rel='stylesheet' id='main-style-css'  href='https://www.getjohn.co.uk/wp-content/themes/fruitful/style.css?ver=5.3' type='text/css' media='all' />
<style id='main-style-inline-css' type='text/css'>
h1 {font-size : 27px; }h2 {font-size : 23px; }h3 {font-size : 20px; }h4 {font-size : 17px; }h5 {font-size : 14px; }h6 {font-size : 12px; }h1, h2, h3, h4, h5, h6 {font-family : PT Sans, sans-serif; } .main-navigation a {font-family : Open Sans, sans-serif; color : #333333; } .main-navigation ul:not(.sub-menu) > li > a, .main-navigation ul:not(.sub-menu) > li:hover > a { font-size : 14px;} .main-navigation {background-color : #ffffff; }#header_language_select a {font-family : Open Sans, sans-serif;} body {font-size : 14px; font-family : Open Sans, sans-serif; }body { background-color : #ffffff; }.page-container .container {background-color : #ffffff; } .head-container, .head-container.fixed{background-color : #ffffff; }.head-container {min-height : 20px; }.head-container {position : fixed; }.site-header img {max-height : 60px; }.main-navigation ul li.current_page_item a, .main-navigation ul li.current-menu-ancestor a, .main-navigation ul li.current-menu-item a, .main-navigation ul li.current-menu-parent a, .main-navigation ul li.current_page_parent a {background-color : #F15A23; }.main-navigation ul li.current_page_item a, .main-navigation ul li.current-menu-ancestor a, .main-navigation ul li.current-menu-item a, .main-navigation ul li.current-menu-parent a, .main-navigation ul li.current_page_parent a {color : #ffffff; } .main-navigation ul > li:hover>a {background-color : #F15A23; color : #ffffff; } #masthead .main-navigation ul > li > ul > li > a {background-color : #ffffff; color : #333333; } #masthead .main-navigation ul > li > ul > li:hover > a {background-color : #F15A23; color : #ffffff; } #masthead .main-navigation ul > li ul > li.current-menu-item > a {background-color : #F15A23; color : #ffffff; } #masthead div .main-navigation ul > li > ul > li > ul a {background-color : #ffffff; color : #333333; } #masthead div .main-navigation ul > li > ul > liul li:hover a {background-color : #F15A23; color : #ffffff; } #lang-select-block li ul li a{background-color : #ffffff; color : #333333;}#lang-select-block li ul li a:hover{background-color : #F15A23; color : #ffffff;}#lang-select-block li ul li.active a{background-color : #F15A23; color : #ffffff;}#header_language_select ul li.current > a { color : #333333; } #header_language_select { background-color : #ffffff; } #header_language_select ul li.current:hover > a { background-color : #F15A23;color : #ffffff;} body {color : #333333; } #page .container #secondary .widget h3.widget-title, #page .container #secondary .widget h1.widget-title, header.post-header .post-title{border-color : #F15A23; } body.single-product #page .related.products h2{border-bottom-color : #F15A23; } a {color : #333333; }#page .container #secondary>.widget_nav_menu>div>ul>li ul>li>a:before {color : #333333; }#page .container #secondary .widget ul li.cat-item a:before {color : #333333; }html[dir="rtl"] #page .container #secondary>.widget_nav_menu>div>ul>li ul>li>a:after {color : #333333; }html[dir="rtl"] #page .container #secondary .widget ul li.cat-item a:after {color : #333333; }a:hover {color : #FF5D2A; } #page .container #secondary>.widget_nav_menu li.current-menu-item>a {color : #FF5D2A; } #page .container #secondary>.widget_nav_menu>div>ul>li ul>li>a:hover:before,#page .container #secondary>.widget_nav_menu>div>ul>li ul>li.current-menu-item>a:before,#page .container #secondary>.widget_nav_menu>div>ul>li ul>li.current-menu-item>a:hover:before{color : #FF5D2A; }#page .container #secondary .widget ul li.current-cat>a,#page .container #secondary .widget ul li.cat-item ul li.current-cat a:before,#page .container #secondary .widget ul li.cat-item a:hover:before{color : #FF5D2A; }html[dir="rtl"] #page .container #secondary>.widget_nav_menu>div>ul>li ul>li>a:hover:after,html[dir="rtl"] #page .container #secondary>.widget_nav_menu>div>ul>li ul>li.current-menu-item>a:after,html[dir="rtl"] #page .container #secondary>.widget_nav_menu>div>ul>li ul>li.current-menu-item>a:hover:after{color : #FF5D2A; } html[dir="rtl"] #page .container #secondary .widget ul li.current-cat>a,html[dir="rtl"] #page .container #secondary .widget ul li.current-cat>a:after,html[dir="rtl"] #page .container #secondary .widget ul li.cat-item a:hover:after{color : #FF5D2A; } a:focus {color : #FF5D2A; } a:active{color : #FF5D2A; } .blog_post .date_of_post{background : none repeat scroll 0 0 #F15A23; } .blog_post .date_of_post{color : #ffffff; } button, input[type="button"], input[type="submit"], input[type="reset"], .wc-proceed-to-checkout a{background-color : #333333 !important; } body a.btn.btn-primary, body button.btn.btn-primary, body input[type="button"].btn.btn-primary , body input[type="submit"].btn.btn-primary {background-color : #333333 !important; }.nav-links.shop .pages-links .page-numbers, .nav-links.shop .nav-next a, .nav-links.shop .nav-previous a, .woocommerce .return-to-shop .button {background-color : #333333 !important; }button:hover, button:active, button:focus{background-color : #F15A23 !important; }input[type="button"]:hover, input[type="button"]:active, input[type="button"]:focus{background-color : #F15A23 !important; }input[type="submit"]:hover, input[type="submit"]:active, input[type="submit"]:focus, .wc-proceed-to-checkout a:focus, .wc-proceed-to-checkout a:hover, .wc-proceed-to-checkout a:active{background-color : #F15A23 !important; }input[type="reset"]:hover, input[type="reset"]:active, input[type="reset"]:focus{background-color : #F15A23 !important; }body a.btn.btn-primary:hover, body button.btn.btn-primary:hover, body input[type="button"].btn.btn-primary:hover , body input[type="submit"].btn.btn-primary:hover {background-color : #F15A23 !important; }.nav-links.shop .pages-links .page-numbers:hover, .nav-links.shop .nav-next a:hover, .nav-links.shop .nav-previous a:hover, .nav-links.shop .pages-links .page-numbers.current, .woocommerce .return-to-shop .button:hover {background-color : #F15A23 !important; }.social-icon>a>i{background:#333333}.social-icon>a>i{color:#ffffff}
</style>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/fnBox/jquery.fancybox.pack.js?ver=20140525'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/fancySelect.js?ver=20140525'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/mobile-dropdown.min.js?ver=20130930'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ThGlobal = {"ajaxurl":"https:\/\/www.getjohn.co.uk\/wp-admin\/admin-ajax.php","is_fixed_header":"1","mobile_menu_default_text":"Navigate to..."};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/init.min.js?ver=20130930'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/themes/fruitful/js/small-menu-select.js?ver=20130930'></script>
<link rel='https://api.w.org/' href='https://www.getjohn.co.uk/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.getjohn.co.uk/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.getjohn.co.uk/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.3" />
<link rel="canonical" href="https://www.getjohn.co.uk/" />
<link rel='shortlink' href='https://www.getjohn.co.uk/' />
<link rel="alternate" type="application/json+oembed" href="https://www.getjohn.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.getjohn.co.uk%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.getjohn.co.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.getjohn.co.uk%2F&#038;format=xml" />
<meta name="viewport" content="width=device-width" /><script type="text/javascript">jQuery(document).ready(function($) { });</script> 
</head> 
<body class="home page-template-default page page-id-253 responsive">
	<div id="page-header" class="hfeed site">
				
					<div class="head-container">
						<div class="container">
							<div class="sixteen columns">
								<header id="masthead" class="site-header" role="banner">
																		<div data-originalstyle="left-pos" class="header-hgroup left-pos">  
										<a class="link-logo" href="https://www.getjohn.co.uk/" title="WE take on the hard stuff, linking computer systems together, automating e-commerce and selling online. Sometimes we blog about running a small business and personal development." rel="home"><img class="logo" src="http://www.getjohn.co.uk/wp-content/uploads/2019/01/Screen-Shot-2019-01-17-at-21.13.30.png" width="" height="60" alt="WE take on the hard stuff, linking computer systems together, automating e-commerce and selling online. Sometimes we blog about running a small business and personal development."/></a><a class="link-logo-retina" href="https://www.getjohn.co.uk/" title="WE take on the hard stuff, linking computer systems together, automating e-commerce and selling online. Sometimes we blog about running a small business and personal development." rel="home"><img class="logo retina" src="http://www.getjohn.co.uk/wp-content/uploads/2019/01/Screen-Shot-2019-01-17-at-21.13.30.png" width="" height="60" alt="WE take on the hard stuff, linking computer systems together, automating e-commerce and selling online. Sometimes we blog about running a small business and personal development."/></a>									</div>	
										
									<div data-originalstyle="right-pos" class="menu-wrapper right-pos">
																															
										<nav role="navigation" class="site-navigation main-navigation">
											<div class="menu-main-nav-container"><ul id="menu-main-nav" class="menu"><li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="https://www.getjohn.co.uk/work/">Current Work</a></li>
<li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50"><a href="https://www.getjohn.co.uk/contact-john-orourke/">Contact Us</a></li>
<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51"><a href="https://www.getjohn.co.uk/about/">Our Availability</a></li>
<li id="menu-item-261" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-261"><a href="https://www.getjohn.co.uk/blog/">Thoughts</a></li>
</ul></div>										</nav><!-- .site-navigation .main-navigation -->
									</div>
								</header><!-- #masthead .site-header -->
							</div>
						</div>
					</div>
					
				</div><!-- .header-container -->
	
		
	<div id="page" class="page-container">		
		<div class="container">		
					
				<div class="sixteen columns">
								<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">	
			
<article id="post-253" class="post-253 page type-page status-publish hentry">
			<div class="entry-content">
		<h2>Digital, Sales and Marketing agencies &#8211; we&#8217;ve got your back!</h2>
<p>Does technology feel like it gets in the way more than it helps you get the job done? We know that you aren&#8217;t alone in feeling like that, many of our clients came to us feeling that they were being hindered rather than helped.</p>
<p>We love the challenges that others may shy away from.</p>
<ul>
<li>Do you have a system that you need to alter but no longer exists?</li>
<li>Or want to make sure your data is processed safely?</li>
<li>Maybe you have two different systems that you just can&#8217;t get to talk to each other?</li>
</ul>
<p>Then we are the people to help you!</p>
<p>Your problems are our problems, we work alongside you to fully understand what is causing you a headache and then build systems that do what you want them to do and do it when you need them to!</p>
					</div><!-- .entry-content -->
</article><!-- #post-253 -->
				</div>
			</div>	
			
				
				</div>
			</div>
		</div><!-- .page-container-->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="sixteen columns">
					<div class="site-info">
						© Copyright 2018 Get John Limited.					</div><!-- .site-info -->
					<div class="ff-social-icon social-icon"><a class="twitter" 	 title="twitter"	href="http://twitter.com/johnorourke" target="_blank"><i class="fa"></i></a><a class="linkedin" 	 title="linkedin"	href="http://www.linkedin.com/in/getjohnorourke" target="_blank"><i class="fa"></i></a><a class="email" 	 title="email"		href="mailto:info@getjohn.co.uk"><i class="fa"></i></a></div>				</div>
			</div>
			<div id="back-top">
				<a rel="nofollow" href="#top" title="Back to top">&uarr;</a>
			</div>
		</footer><!-- #colophon .site-footer -->
	<!--WordPress Development by Fruitful Code-->
			<!-- Fruitful Shortcodes Custom Styles -->
			<style>
						</style>
			<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/js/front.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_alert/js/scripts.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/libs/wow/wow.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_pbar/js/scripts.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0/assets/libs/slick/slick.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_recent_posts_slider/js/scripts.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-content/plugins/fruitful-shortcodes/fruitful-shortcodes-2.0.0//assets/shared/fruitful_tabs/js/scripts.min.js?ver=071120180241'></script>
<script type='text/javascript' src='https://www.getjohn.co.uk/wp-includes/js/wp-embed.min.js?ver=5.3'></script>
</body>
</html>