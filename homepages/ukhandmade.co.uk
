<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="canonical" href="/magazine" />
<link rel="shortlink" href="/node/52" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>UK Handmade Magazine | UK Handmade</title>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_aA8pdKhox0GUNjuBSZx9PaRRt9T_fOYrZtiGsWKJKtw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_5hvPkqcYTw-iaqL5zHqtzD341IdGo0klhhONrYxmV8Q.css" media="all" />
<style>.flexible-slideshow,.flexible-slideshow .article-inner,.flexible-slideshow .article-content,.flexslider{max-width:px;}
</style>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_amxEilfLWpVf1MnrSWhRFyxccrR5YJ21dVxIkJJT4Yc.css" media="all" />
<style>#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#959595;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,.eu-cookie-compliance-more-button,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#ffffff !important;}.eu-cookie-withdraw-tab{border-color:#ffffff;}
</style>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_WkIQlS2V8h4Vh16HARhGTGX8u2nA_FzvuTZ5-DXLwaQ.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_VeBSowNA1ZIm2dKljah3AppRJliB5wuU8odLet773wQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_fb_UEdtQ9GVUQEYfZD0CwddSxtGiOYkA0_b7-stCKV8.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_VLb2WSorLiC9QIzad3jdIEwiChLeWhWWrH8GgZ8J1_0.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_mvjJ6Fnrs3nddE7QqcvKxBeMuEBRHx9jfrdv7KRaFT4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_BQYOIPzbbkaSK5Ze-JIfD6zsfMAl139oZlBbxJai9hQ.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Muli|Open+Sans|Roboto+Condensed|Roboto:400,700" media="all" />

<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_IgLFwZxfwjPzuQpwmRtWfjC3lqKSCu6Xm2zQuovCry8.css" media="screen" />
<![endif]-->

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_AbpHGcgLb-kRsJGnwFEktk7uzpZOCcBY74-YBdrKVGs.css" media="screen" />
<![endif]-->

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_F5dDLDDznkN461Wcixy86a2AxU-sZUFzvmM3dKEfGMM.css" media="screen" />
<![endif]-->

<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_yNGa1BdQKp2VqshF_KAN7MUZ4pGvnVgIT1gkuR4s1Ms.css" media="screen" />
<![endif]-->

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.ukhandmade.co.uk/sites/default/files/css/css_AbpHGcgLb-kRsJGnwFEktk7uzpZOCcBY74-YBdrKVGs.css" media="screen" />
<![endif]-->
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_0RyHJ63yYLuaWsodCPCgSD8dcTIA0dqcDf8-7c2XdBw.js"></script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_TVTqjz8JHRb2KK9hlzuk0YsjzD013dKyYX_OTz-2VXU.js"></script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_Tik8PIaz_eQ5I4FMzmjkWoPEs9jKBgTSauo1jgsNa6g.js"></script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_UQSKnMfL0HAEyT2ClAsaFqgO3AaIjL-M2bN8PeJSm9o.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-16731576-3", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_L4HK3EpP8u_bUPRSIqQGYjiGtNru4cehVuq7NundeWU.js"></script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_OmTzEw2YkeSGlpopn2YDusFX8T2KRyiakU3dUjMQ7ck.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_commerce","theme_token":"fXqISm-gD9qrLuJOSwefPamQfqWAP1N4Yoi8QwrKq-k","js":{"0":1,"1":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/modules\/extlink\/extlink.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"2":1,"sites\/all\/themes\/at-commerce\/scripts\/jquery.flexslider-min.js":1,"sites\/all\/themes\/at-commerce\/scripts\/slider.options.js":1,"sites\/all\/themes\/at-commerce\/scripts\/draw.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/outside-events.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/menu-toggle.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/extlink\/extlink.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"1":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/white.css":1,"0":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/at-commerce\/css\/styles.base.css":1,"sites\/all\/themes\/at-commerce\/css\/styles.modules.css":1,"sites\/all\/themes\/at-commerce\/css\/styles.settings.css":1,"sites\/all\/themes\/at-commerce\/color\/colors.css":1,"public:\/\/adaptivetheme\/at_commerce_files\/at_commerce.responsive.layout.css":1,"public:\/\/adaptivetheme\/at_commerce_files\/at_commerce.fonts.css":1,"sites\/all\/themes\/at-commerce\/css\/styles.slideshow.css":1,"public:\/\/adaptivetheme\/at_commerce_files\/at_commerce.menutoggle.css":1,"public:\/\/adaptivetheme\/at_commerce_files\/at_commerce.responsive.styles.css":1,"\/\/fonts.googleapis.com\/css?family=Muli|Open+Sans|Roboto+Condensed|Roboto:400,700":1,"public:\/\/adaptivetheme\/at_commerce_files\/at_commerce.lt-ie9.layout.css":1,"sites\/all\/themes\/at-commerce\/css\/ie-6.css":1,"sites\/all\/themes\/at-commerce\/css\/ie-lte-7.css":1,"sites\/all\/themes\/at-commerce\/css\/ie-8.css":1,"sites\/all\/themes\/at-commerce\/css\/ie-lte-9.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"lightbox2":{"rtl":"0","file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":true,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":false,"disable_resize":false,"disable_zoom":false,"force_show_nav":false,"show_caption":true,"loop_items":false,"node_link_text":"View Image Details","node_link_target":false,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":false,"useragent":"Python-urllib\/3.8"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":1,"popup_scrolling_confirmation":0,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWe use cookies on this site to enhance your user experienceBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EThank you for accepting cookiesYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-cookies","popup_link_new_window":1,"popup_position":null,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"default","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false},"extlink":{"extTarget":"_blank","extClass":"ext","extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":"","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":"mailto","mailtoLabel":"(link sends e-mail)"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/search\/node":true,"\/":true},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":false,"dropShadows":false,"disableHI":false},"plugins":{"touchscreen":{"mode":"window_width"},"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"Main menu"},"supposition":true,"bgiframe":false}}},"adaptivetheme":{"at_commerce":{"layout_settings":{"bigscreen":"two-sidebars-right","tablet_landscape":"two-sidebars-right","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-stack","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"},"menu_toggle_settings":{"menu_toggle_tablet_portrait":"false","menu_toggle_tablet_landscape":"false"}}}});</script>
<!--[if lt IE 9]>
<script src="https://www.ukhandmade.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?pr8t5d"></script>
<![endif]-->
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-52 node-type-page site-name-hidden atr-7.x-3.x atv-7.x-3.0 lang-en site-name-uk-handmade color-scheme-custom at-commerce  bb-n hl-c mb-dd mma-c itrc-3 isrc-3 hide-ss-nav hide-ss-dir">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page ssc-n ssw-n ssa-l sss-n btc-n btw-n bta-l bts-n ntc-n ntw-n nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-n pta-l pts-n at-mt">

    
    <div id="header-wrapper">
      
      <div class="container clearfix">
        <header class="clearfix">

                      <!-- start: Branding -->
            <div id="branding" class="branding-elements clearfix with-logo site-name-hidden no-slogan">

                              <div id="logo">
                  <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="https://www.ukhandmade.co.uk/sites/default/files/2014logo_0_0.jpg" alt="UK Handmade" /></a>                </div>
              
                              <!-- start: Site name and Slogan hgroup -->
                <div id="name-and-slogan" class="element-invisible h-group" id="name-and-slogan">

                                      <h1 id="site-name" class="element-invisible" id="site-name"><a href="/" title="Home page" class="active">UK Handmade</a></h1>
                  
                  
                </div><!-- /end #name-and-slogan -->
              

            </div><!-- /end #branding -->
          
          
                      <div id="menu-wrapper"><div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1" >  
      <h2 class="element-invisible block-title element-invisible">Main menu</h2>
  
  <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-white sf-total-items-4 sf-parent-items-0 sf-single-items-4"><li id="menu-1129-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" title="" class="sf-depth-1 active">Home</a></li><li id="menu-1130-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/about" title="" class="sf-depth-1">About</a></li><li id="menu-1131-1" class="active-trail middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/magazine" class="sf-depth-1 active">Magazine</a></li><li id="menu-1133-1" class="last even sf-item-4 sf-depth-1 sf-no-children"><a href="/contact" class="sf-depth-1">Contact</a></li></ul>
  </nav></div></div>
          
        </header>
      </div>
    </div>

    
    <div id="content-wrapper" class="no-secondary"><div class="container">

      
      
      
      <div id="columns">
        <div class="columns-inner clearfix">

          <div id="content-column">
            <div class="content-inner">

              <div class="region region-highlighted"><div class="region-inner clearfix"><section id="block-block-1" class="block block-block odd first last block-count-2 block-region-highlighted block-1" ><div class="block-inner clearfix">  
      <h2 class="block-title">UK Handmade Magazine Spring 2018</h2>
  
  <div class="block-content content"><div data-configid="7951910/60487801" style="width:100%; height:500px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script><p>Click image above to read or <a href="http://www.lulu.com/shop/ukhandmade/uk-handmade-magazine-spring-2018/ebook/product-23613877.html">Buy PDF Download Here</a></p><p> </p></div>
  </div></section></div></div>
              
              <section id="main-content">

                                                
                
                                  <div id="content">
                    <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-52" class="node node-page article odd node-full ia-n clearfix" about="/magazine" typeof="foaf:Document" role="article">
  <div class="node-inner clearfix">
    
    
          <header class="node-header">

        
                  <h1 class="node-title">
                          UK Handmade Magazine                      </h1>
        
        
      </header>
    
    <div class="node-content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p>UK Handmade Magazine is a quarterly digital magazine that celebrates the best designers and makers across the UK. Read our latest issue above. Below are all our past issues. Please click the covers to read them for free on your computer or device.</p><table border="0" cellpadding="1" cellspacing="1" style="width: 700px;"><tbody><tr><td><strong>2017</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_spring_2017"><img alt="" src="/sites/default/files/resize/SpringCoverSM-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_magazine_summer_2017"><img alt="" src="/sites/default/files/UK%20Handmade%20Magazine%20Summer%202017sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_magazine_autumn_2017"><img alt="" src="/sites/default/files/coversm_8.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_magazine_winter_2017"><img alt="" src="/sites/default/files/resize/UK%20Handmade%20Magazine%20Winter%202017sm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2016</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="https://issuu.com/ukhandmade/docs/ukh_spr16_draft_lores_final"><img alt="" src="/sites/default/files/resize/SpringCoverLRG-150x149.JPG" style="width: 150px; height: 149px;" width="150" height="149" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_magazine_summer_2016"><img alt="" src="/sites/default/files/resize/frontsm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_autumn_2016"><img alt="" src="/sites/default/files/resize/Coversm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/uk_handmade_winter_magazine_2016"><img alt="" src="/sites/default/files/resize/winter2016-150x150.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2015</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://issuu.com/ukhandmade/docs/ukhandmadespring2015" target="_blank"><img alt="" src="/sites/default/files/resize/large_coverlrg-150x150.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/uk_handmade_summer_2015" target="_blank"><img alt="" src="/sites/default/files/resize/Summer2015sm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhautumnmagazine2015" target="_blank"><img alt="" src="/sites/default/files/UKH%20Autumn%202015%20Mag%20Coversm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="https://issuu.com/ukhandmade/docs/ukhandmadewinter2015"><img alt="" src="/sites/default/files/resize/wintercovesm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2014</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://issuu.com/ukhandmade/docs/spring2014lr" target="_blank"><img alt="Spring 2014" class="image-large" src="/sites/default/files/resize/spring2014sm-150x150.jpg" style="width: 150px; height: 150px;" title="Spring 2014" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhandmadesummer2014" target="_blank"><img alt="Summer 2014" class="image-large" src="/sites/default/files/resize/coversm_4-150x150.jpg" style="width: 150px; height: 150px;" title="Summer 2014" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhandmademagazineautumn2014"><img alt="" class="image-large" src="/sites/default/files/resize/autumnsm-150x150.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhwinter2014" target="_blank"><img alt="" src="/sites/default/files/resize/wintercover2014med-150x150.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2013</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://issuu.com/ukhandmade/docs/springissue2013"><img alt="Spring 2013" class="imagecache-large" src="/sites/default/files/resize/spring2013small-150x150.jpg" style="width: 150px; height: 150px;" title="Spring 2013" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhandmadesummer2013" target="_blank"><img alt="Summer 2013" class="imagecache-large" src="/sites/default/files/resize/summer2013sm-150x150.jpg" style="width: 150px; height: 150px;" title="Summer 2013" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhandmadeautumn"><img alt="" class="image-large" src="/sites/default/files/resize/autumn2013lrg_1-150x150.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://issuu.com/ukhandmade/docs/ukhandmadewinter2013"><img alt="Winter 2013" class="image-large" src="/sites/default/files/resize/coversm_0-150x150.jpg" style="width: 150px; height: 150px;" title="Winter 2014" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2012</strong></td><td> </td><td> </td><td> </td></tr><tr><td><p><a href="http://content.yudu.com/Library/A1wmow/UKHandmadeSpringIssu/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F517471%2FUK-Handmade-Spring-Issue-2012"><img alt="Spring 2012" class="imagecache-large" src="/sites/default/files/resize/SPRING2012sm_0-150x150.jpg" style="width: 150px; height: 150px;" target="_blank" title="Spring 2012" width="150" height="150" /></a></p></td><td><a href="http://content.yudu.com/Library/A1xumv/UKHandmadeSummer2012/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F566440%2FUK-Handmade-Summer-2012"><img alt="" class="imagecache-large" src="/sites/default/files/resize/SUMMER2012small_1-150x150.jpg" style="width: 150px; height: 150px;" title="" width="150" height="150" /></a></td><td style="text-align: center;"><a href="http://content.yudu.com/Library/A1zlzo/UKHandmadeAutumn2012/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F640665%2FUK-Handmade-Autumn-2012%3Fedit_mode%3Don"><img alt="Autumn 2012" class="imagecache-large" src="/sites/default/files/resize/coversm-150x150.jpg" style="width: 150px; height: 150px;" title="Autumn 2012" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A21jid/Winter2012/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F719120%2FWinter-2012" target="_blank"><img alt="Winter 2012" class="imagecache-large" src="/sites/default/files/resize/winter2012coversm-150x150.jpg" style="width: 150px; height: 150px;" title="Winter 2012" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2011</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://content.yudu.com/Library/A1s1oz/UKHandmadeMagazineSp/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F326976%2FUK-Handmade-Magazine-Spring-2011%3Fedit_mode%3Don" target="_blank"><img alt="" src="/sites/default/files/resize/spring2011sm-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1tcv1/UKHandmadeSummer2011/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F380068%2FUK-Handmade-Summer-2011" target="_blank"><img alt="Summer 2011" class="imagecache-large" src="/sites/default/files/resize/summer%20fcover-150x150.jpg" style="width: 150px; height: 150px;" title="Summer 2011" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1uicz/UKHandmadeAutumn2011/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F429194%2FUK-Handmade-Autumn-2011" target="blank"><img alt="Autumn 2011" class="imagecache-large" src="/sites/default/files/resize/Autumn2011-150x150.jpg" style="margin-left: 5px; margin-right: 5px; float: left; width: 150px; height: 150px;" title="Autumn 2011" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1vufc/WinterIssue2011/resources/index.htm?referrerUrl=http%3A%2F%2Ffree.yudu.com%2Fitem%2Fdetails%2F484740%2FWinter-Issue-2011" target="_blank"><img alt="Winter 2011" class="imagecache-large" src="/sites/default/files/resize/Winter2011coversm_1-150x150.jpg" style="width: 150px; height: 150px;" title="Winter 2011" width="150" height="150" /></a></td></tr><tr><td> </td><td> </td><td> </td><td> </td></tr><tr><td><strong>2010</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://content.yudu.com/Library/A1n9xp/UKHandmadeSpring2010/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F144091%2FUK-Handmade-Spring-2010" target="_blank"><img alt="" src="/sites/default/files/spring2010sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1nxbr/UKHandmadeSummer2010/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F171211%2FUK-Handmade-Summer-2010" target="_blank"><img alt="" src="http://ukhandmade.co.uk/sites/default/files/summer2010sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1oybc/UKHandmadeAutumn2010/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F211536%2FUK-Handmade-Autumn-2010" target="_blank"><img alt="" src="/sites/default/files/autumncoversm.JPG" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1r9tg/UKHandmadeWinter2010/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F297193%2FUK-Handmade-Winter-2010%3Fedit_mode%3Don" target="_blank"><img alt="" src="/sites/default/files/resize/Winter2011coversm_1_0-150x150.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td><strong>2009</strong></td><td> </td><td> </td><td> </td></tr><tr><td><a href="http://content.yudu.com/Library/A168yq/UKHandmadeSpring2009/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F45671%2FUK-Handmade-Spring-2009" target="_blank"><img alt="" src="/sites/default/files/springsmall.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1cmwy/UKHandmadeSummer2009/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F73081%2FUK-Handmade-Summer-2009" target="_blank"><img alt="" src="/sites/default/files/summer2009sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1h5r4/UKHandmadeAutumn2009/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F92314%2FUK-Handmade-Autumn-2009" target="_blank"><img alt="" src="/sites/default/files/autumn2009sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td><td><a href="http://content.yudu.com/Library/A1jcj7/UKHandmadeWinter2009/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F108393%2FUK-Handmade-Winter-2009" target="_blank"><img alt="" src="/sites/default/files/winter2009sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></td></tr><tr><td><strong>2008</strong></td><td> </td><td> </td><td> </td></tr><tr><td><p><strong><a href="http://content.yudu.com/Library/A15lxi/UKHandmadeWinter2008/resources/index.htm?referrerUrl=http%3A%2F%2Fwww.yudu.com%2Fitem%2Fdetails%2F43194%2FUK-Handmade-Winter-2008" target="_blank"><img alt="" src="/sites/default/files/winter2008sm.jpg" style="width: 150px; height: 150px;" width="150" height="150" /></a></strong></p></td><td> </td><td> </td><td> </td></tr></tbody></table><p> </p></div></div></div>    </div>

    
    
    <span property="dc:title" content="UK Handmade Magazine" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>  </div>
</article>

  </div>                  </div>
                
              </section>

              
            </div>
          </div>

                    <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><div id="block-search-form" class="block block-search no-title odd first block-count-4 block-region-sidebar-second block-form"  role="search"><div class="block-inner clearfix">  
  
  <div class="block-content content no-title"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-mMI57DEwxFCJrKsitabffIUsEJw-xeYcs3tD34uVN1g" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div></div><div id="block-block-17" class="block block-block no-title even last block-count-5 block-region-sidebar-second block-17" ><div class="block-inner clearfix">  
  
  <div class="block-content content no-title"><p><a href="https://www.facebook.com/UKHandmade/"><img alt="" src="/sites/default/files/resize/pictures/facebook_online_social_media-128-35x35.png" style="width: 35px; height: 35px; margin-left: 5px; margin-right: 5px;" width="35" height="35" /></a><a href="https://www.instagram.com/ukhandmademagazine/"><img alt="" src="/sites/default/files/resize/pictures/instagram_online_social_media-128-35x35.png" style="width: 35px; height: 35px; margin-left: 5px; margin-right: 5px;" width="35" height="35" /></a><a href="https://twitter.com/ukhandmade"><img alt="" src="/sites/default/files/resize/pictures/online_social_media_twitter-128-35x35.png" style="width: 35px; height: 35px; margin-left: 5px; margin-right: 5px;" width="35" height="35" /></a><a href="https://uk.pinterest.com/ukhandmade/"><img alt="" src="/sites/default/files/resize/pictures/pinterest_online_social_media-128-35x35.png" style="width: 35px; height: 35px; margin-left: 5px; margin-right: 5px;" width="35" height="35" /></a></p></div>
  </div></div></div></div>
        </div>
      </div>

      
      </div>
    </div>

    
    <div id="page-footer">
      
              <div id="footer-wrapper">
          <div class="container clearfix">
            <footer class="clearfix">
              <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-3" class="block block-block no-title odd first last block-count-6 block-region-footer block-3" ><div class="block-inner clearfix">  
  
  <div class="block-content content no-title"><table border="0" cellpadding="1" cellspacing="1" width="900"><tbody><tr><td width="225"><a href="http://ukhandmade.co.uk/about"><span style="color: rgb(255, 255, 255);">About UK Handmade</span></a><br /><a href="http://ukhandmade.co.uk/terms"><span style="color: rgb(255, 255, 255);">Terms and Conditions</span></a><br /><a href="http://ukhandmade.co.uk/privacy-cookies"><span style="color: rgb(255, 255, 255);">Privacy &amp; Cookies</span></a><br /><a href="http://ukhandmade.co.uk/contact"><span style="color: rgb(255, 255, 255);">Contact</span></a></td><td width="225"> </td></tr></tbody></table><p> </p></div>
  </div></div></div></div>                          </footer>
         </div>
      </div>
          </div>

  </div>
</div>
  <script>function euCookieComplianceLoadScripts() {}</script>
<script>var eu_cookie_compliance_cookie_name = "";</script>
<script src="https://www.ukhandmade.co.uk/sites/default/files/js/js_4MRGjqSerJEQxet46pdKTifSYprPwyhoaSNKNoRsFw4.js"></script>
</body>
</html>
