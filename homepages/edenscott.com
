<!doctype html>
<html class="no-js" lang="en">
<head>
    <!-- Meta tags etc -->
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0 maximum-scale=1, user-scalable=0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>


  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">



  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.edenscott.com/sites/all/themes/edenscott/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="As Scotland&#039;s leading independent recruitment business, we provide permanent, contract, temporary and executive solutions across the UK and internationally." />
<meta name="robots" content="follow, index" />
<meta name="keywords" content="Eden Scott, jobs, recruitment, Scotland" />
<meta name="news_keywords" content="Eden Scott, recruitment, jobs, permanent, temporary, contract, Scotland, Edinburgh, Aberdeen, Glasgow" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.edenscott.com/" />
<link rel="shortlink" href="https://www.edenscott.com/" />
<meta property="og:site_name" content="Eden Scott" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.edenscott.com/" />
<meta property="og:title" content="Eden Scott" />
<meta property="og:description" content="As Scotland&#039;s leading independent recruitment business, we provide permanent, contract, temporary and executive solutions across the UK and internationally." />
<meta property="og:image" content="http://www.edenscott.com/sites/default/files/logo_0.jpg" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@EdenScottLtd" />
<meta name="twitter:site:id" content="308441281" />
<meta name="twitter:url" content="https://www.edenscott.com/home" />
<meta name="twitter:title" content="Home" />

    <title>Scotland Jobs and Recruitment | Eden Scott</title>

    <!-- Stylesheets -->
  <style type="text/css" media="all">
@import url("https://www.edenscott.com/sites/all/modules/escape_admin/css/escapeAdmin.admin_menu.css?pziyw7");
@import url("https://www.edenscott.com/modules/system/system.base.css?pziyw7");
@import url("https://www.edenscott.com/modules/system/system.menus.css?pziyw7");
@import url("https://www.edenscott.com/modules/system/system.messages.css?pziyw7");
@import url("https://www.edenscott.com/modules/system/system.theme.css?pziyw7");
</style>
<style type="text/css" media="all">
@import url("https://www.edenscott.com/sites/all/modules/date/date_api/date.css?pziyw7");
@import url("https://www.edenscott.com/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?pziyw7");
@import url("https://www.edenscott.com/sites/all/modules/fences/field.css?pziyw7");
@import url("https://www.edenscott.com/sites/all/modules/fitvids/fitvids.css?pziyw7");
@import url("https://www.edenscott.com/modules/node/node.css?pziyw7");
@import url("https://www.edenscott.com/modules/search/search.css?pziyw7");
@import url("https://www.edenscott.com/modules/user/user.css?pziyw7");
@import url("https://www.edenscott.com/sites/all/modules/views/css/views.css?pziyw7");
</style>
<style type="text/css" media="all">
@import url("https://www.edenscott.com/sites/all/modules/ctools/css/ctools.css?pziyw7");
</style>
<style type="text/css" media="all">
@import url("https://www.edenscott.com/sites/all/themes/edenscott/css/base.css?pziyw7");
@import url("https://www.edenscott.com/sites/all/themes/edenscott/css/style.css?pziyw7");
</style>
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
window.google_analytics_uacct = "UA-5051662-1";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.edenscott.com/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script type="text/javascript" src="https://www.edenscott.com/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.edenscott.com/misc/drupal.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/modules/escape_admin/js/escapeAdmin.modernizer.js?v=7.67"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/modules/escape_admin/js/escapeAdmin.js?v=7.67"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/libraries/fitvids/jquery.fitvids.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/modules/fitvids/fitvids.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/modules/google_analytics/googleanalytics.js?pziyw7"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-5051662-1", {"cookieDomain":"auto"});ga("require", "displayfeatures");ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://use.typekit.net/iix0gsz.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
try { Typekit.load({async: true}); } catch (e) { }
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"edenscott","theme_token":"dU2Q9jZqee2juOIl19CeySasFn7fmRNb3nX1Wq4RtkM","js":{"sites\/all\/themes\/edenscott\/js\/retina.min.js":1,"sites\/all\/themes\/edenscott\/js\/matchHeight.min.js":1,"sites\/all\/themes\/edenscott\/js\/modernizr.custom.js":1,"sites\/all\/themes\/edenscott\/js\/basescripts.min.js":1,"sites\/all\/themes\/edenscott\/js\/jquery.query-object.js":1,"sites\/all\/themes\/edenscott\/js\/site.js":1,"sites\/all\/themes\/edenscott\/js\/cookies.js":1,"0":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/escape_admin\/js\/escapeAdmin.modernizer.js":1,"sites\/all\/modules\/escape_admin\/js\/escapeAdmin.js":1,"sites\/all\/libraries\/fitvids\/jquery.fitvids.js":1,"sites\/all\/modules\/fitvids\/fitvids.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"https:\/\/use.typekit.net\/iix0gsz.js":1,"2":1},"css":{"sites\/all\/modules\/escape_admin\/css\/escapeAdmin.admin_menu.css":1,"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/edenscott\/css\/base.css":1,"sites\/all\/themes\/edenscott\/css\/style.css":1}},"better_exposed_filters":{"views":{"homepage_banner":{"displays":{"block":{"filters":[]}}},"campaigns":{"displays":{"homepage_block":{"filters":[]}}},"testimonials":{"displays":{"block":{"filters":[]}}},"logos":{"displays":{"block":{"filters":[]}}}}},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"currentPath":"node\/9","currentPathIsAdmin":false,"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
          'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-WXTCVZ7');</script>
    <!-- End Google Tag Manager -->
</head>

<body class="html front not-logged-in no-sidebars page-node page-node- page-node-9 node-type-page home page-home" >
<!-- Lead Forensics -->
<script type="text/javascript" src="https://secure.leadforensics.com/js/80506.js"></script>
<noscript><img src="https://secure.leadforensics.com/80506.png" alt="" style="display:none;" /></noscript>

<div id="skip-link">
    <a href="#main-content"
       class="element-invisible element-focusable">Skip to main content</a>
</div>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WXTCVZ7"
            height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<nav id="mobile-nav">  <div class="region region-navigation">
    <div id="block-menu-block-1" class="block block-menu-block">

    
  <div class="content">
    <div class="menu-block-wrapper menu-block-1 menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="main-menu" id="main-menu"><li class="first leaf active-trail active menu-mlid-1179"><a href="/home" class="active-trail active">Home</a></li>
<li class="leaf has-children menu-mlid-595"><a href="/job-search">Job Search</a></li>
<li class="leaf has-children menu-mlid-596"><a href="/client-campaigns">Client Campaigns</a></li>
<li class="leaf has-children menu-mlid-598"><a href="/about-us">About Us</a></li>
<li class="leaf has-children menu-mlid-613"><a href="/our-services">Our Services</a></li>
<li class="leaf has-children menu-mlid-597"><a href="/markets">Markets</a></li>
<li class="leaf has-children menu-mlid-1627"><a href="/careers">Work For Eden Scott</a></li>
<li class="leaf has-children menu-mlid-618"><a href="/blog" title="">Blog/News</a></li>
<li class="leaf menu-mlid-600"><a href="/send-cv">Send CV</a></li>
<li class="last leaf menu-mlid-602"><a href="/lets-talk">Contact Us</a></li>
<a href="#" class="btn btn-secondary hidden">For Employers</a>
            <a href="#" class="btn btn-primary hidden">Send Your CV</a></ul></div>
  </div>
</div>
  </div>
</nav>

<div class="pre-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-24">
                <div class="header-buttons">
                    <p>
                        <a href="/newsletter" class="btn btn-primary">Subscribe</a>
                        <a href="/markets" class="btn btn-primary">For Employers</a>
                        <a href="/send-cv" class="btn btn-primary">Send Your CV</a>
                        <a  href="https://edenscott.vincere.io/careers/user/login" target="_blank" class="btn btn-secondary"><span class="icon icon-user-2"></span><span class="wording">Members Area</span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header">
    <div class="container">
        <a href="/" class="logo">
<!--            <img src="/--><!--/images/logo.png"/>-->
            <img src="/sites/all/themes/edenscott/images/logo15yr-new.png"/>
        </a>
        <div class="mobile-menu-link-container">
            <a class="menu-link" href="#mobile-nav"><span
                    class="word-menu">Menu</span>
                <button class="c-hamburger c-hamburger--htx">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
    </div>
</header>

<nav class="main-nav">
    <div class="container">
		  <div class="region region-navigation">
      <div class="region region-navigation">
    <div id="block-menu-block-1" class="block block-menu-block">

    
  <div class="content">
    <div class="menu-block-wrapper menu-block-1 menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="main-menu" id="main-menu"><li class="first leaf active-trail active menu-mlid-1179"><a href="/home" class="active-trail active">Home</a></li>
<li class="leaf has-children menu-mlid-595"><a href="/job-search">Job Search</a></li>
<li class="leaf has-children menu-mlid-596"><a href="/client-campaigns">Client Campaigns</a></li>
<li class="leaf has-children menu-mlid-598"><a href="/about-us">About Us</a></li>
<li class="leaf has-children menu-mlid-613"><a href="/our-services">Our Services</a></li>
<li class="leaf has-children menu-mlid-597"><a href="/markets">Markets</a></li>
<li class="leaf has-children menu-mlid-1627"><a href="/careers">Work For Eden Scott</a></li>
<li class="leaf has-children menu-mlid-618"><a href="/blog" title="">Blog/News</a></li>
<li class="leaf menu-mlid-600"><a href="/send-cv">Send CV</a></li>
<li class="last leaf menu-mlid-602"><a href="/lets-talk">Contact Us</a></li>
<a href="#" class="btn btn-secondary hidden">For Employers</a>
            <a href="#" class="btn btn-primary hidden">Send Your CV</a></ul></div>
  </div>
</div>
  </div>
  </div>
    </div>
</nav>

<div class="wrapper">
    <div class="overlay"></div>

	
    
    <div class="banner home">
          

      
          
  <div class="field-image">
    <img src="https://www.edenscott.com/sites/default/files/home/BEING%20PROMOTED.jpg" width="1500" height="844" alt="" />  </div>
          <div class="darkening-mask"></div>
        <div class="banner-overlay">
            <div class="inner">
                <h1>Looking for a job?</h1>
                <p>Browse over 276                    opportunities</p>
                <form action="/job-search/" method="post">
                    <input type="text" id="keywords" value="" name="keywords"
                           placeholder="Eg. Job Title, Keywords">
                    <button type="submit" class="btn btn-secondary">Search
                    </button>
                </form>
                <p class="advanced"><a href="/job-search">Advanced Search</a>
                </p>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-sm-14 col-sm-push-5 col-xs-24 col-xs-push-0">
                <div class="intro-home">
										
					                        <div class="tabs">
							                        </div>
					
					
					  <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-9" class="node node-page clearfix">

  
      
  
  <div class="content">
    
  <div class="field-body">
    <p> </p>
<table border="0"><tbody><tr><td>
<p style="text-align: center;">To deliver the very best recruitment experience for you we need to be faster and smarter than ever before. That's why we continually strive to lead the market by finding new and innovative recruitment solutions.</p>
<p style="text-align: center;">We work across over 20 markets and have offices in Edinburgh, Glasgow, Aberdeen and Belfast.</p>
<p style="text-align: center;">Our team of sector specialists are genuinely passionate about what they do and are committed to going above and beyond your expectations. Discover how our bespoke recruitment solutions can help you.</p>
<p style="text-align: center;">Let's talk.</p>
<p style="text-align: center;"><a href="/job-search" class="btn btn-primary">Current Opportunities</a></p>
<p style="text-align: center;"><a href="/send-cv" class="small">Send us your CV</a></p>
</td>
<td> </td>
</tr></tbody></table>  </div>
  </div>

  
  
</div>
  </div>
</div>
  </div>
                </div>
            </div>
        </div>
    </div>

	
    <div class="featured-employers employers-companies light-grey-bck clearfix">
		  <div class="region region-home-featured">
    <div id="block-views-campaigns-homepage-block" class="block block-views">

    
  <div class="content">
    <div class="view view-campaigns view-id-campaigns view-display-id-homepage_block view-dom-id-67cad3b919b6d2ed296c4d22ffe0dc1d">
        
  
  
      <div class="view-content">
      
<div class="container">
    <div class="col-xs-24">
        <h3 class="text-center">Featured companies</h3>
    </div>

    <div class="row">
        <div class="col-sm-20 col-sm-push-2 col-xs-24 col-xs-push-0">
							
	
	<div class="logo-company">		<a href="/client-campaigns/edinburgh-university-students-association"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/EUSA%20Logo%20375x225.png?itok=AS-uIrK7" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/eicc"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Logo.jpg?itok=f-J7T8ZQ" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/floors-castle"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Floors%20castle%20375x225.png?itok=Rdv2ON_w" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/genius-foods"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Genius%20Foods%20Logo%20375225.png?itok=OxoR_Jll" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/hanover-scotland-housing-association"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Hanover%20Scotland%20375x225.png?itok=f1c7bQ4N" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/hopetoun-house"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Hopetoun%20House%20375x225.png?itok=2dp-rl-P" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/ian-macleod-distillers"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Ian-Macleod-Distillers.jpg?itok=fJS_KK-k" alt="" /></a>	</div>							
	
	<div class="logo-company">		<a href="/client-campaigns/inveraray-castle"><img src="https://www.edenscott.com/sites/default/files/styles/grid_image_large/public/Inveraray%20Castle%20375x225.png?itok=F72OALmw" alt="" /></a>	</div>			        </div>
        <div class="col-xs-24">
            <p class="text-center"><a
                    href="/client-campaigns"
                    class="btn btn-primary">See all companies</a></p>
        </div>
    </div>
</div>
    </div>
  
  
  
  
  
  
</div>  </div>
</div>
  </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-24">
				  <div class="region region-home-testimonial">
    <div id="block-views-testimonials-block" class="block block-views">

    
  <div class="content">
    <div class="view view-testimonials view-id-testimonials view-display-id-block view-dom-id-b287c965138ea05cbe78af20d23c70ec">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
    
<blockquote class="blockquote-home">
    <p>I&#039;ve worked with Megan on three separate campaigns over the last few months and I have always been impressed by her attitude and approach. She has provided first class support and delivered each time. She takes care to understand the brief and the environment each role works within. She is careful to appreciate nuances and even recognises the politics that inevitably surround us all. I am delighted to recommend Megan and hope that I am able to continue to work with her in the future.</p>
    <footer>Rob Gibson, <cite title="Source Title"><div class="field-job-title"><div class="field-items"><div class="field-item even">Head of IS at Scotland’s Rural College (SRUC)</div></div></div></cite></footer>
</blockquote>
  </div>
    </div>
  
  
  
  
  
  
</div>  </div>
</div>
  </div>
            </div>
        </div>
    </div>

</div>

<a href="#0" class="cd-top">Top</a>

<footer>
    <div class="banner-bottom">
        <div class="banner-container">
            <img src="/sites/all/themes/edenscott/images/talentspark-logo.png"
                 alt="..."/>
            <p>We're passionate about Scotland's startup and SME community.
                That's why we launched TalentSpark, our recruitment service
                tailored specifically for early stage businesses.</p>

            <p>Our dedicated teams have proven track record of helping ambitious
                businesses flourish across professional technical and executive
                staffing markets.</p>

            <p>We'd love to help your business grow. Discover more about
                TalentSpark by visiting talentspark.scot</p>
            <a href="http://www.talentspark.scot/" target="_blank"
               class="btn btn-transparent">Access TalentSpark</a>
        </div>
    </div>

	  <div class="region region-footer-social">
    <div id="block-edenscott-social-links-social-links" class="block block-edenscott-social-links">

    
  <div class="content">
    <ul class='footer-social'><li><a href="https://facebook.com/EdenScottLtd" title="Follow us on Facebook" target="_blank"><div class="icon icon-facebook"></div></a></li><li><a href="https://twitter.com/EdenScottLtd" title="Follow us on Twitter" target="_blank"><div class="icon icon-twitter"></div></a></li><li><a href="http://www.linkedin.com/company/eden-scott" title="Follow us on LinkedIn" target="_blank"><div class="icon icon-linkedin"></div></a></li><li><a href="https://plus.google.com/u/0/+edenscott" title="Follow us on Google Plus" target="_blank"><div class="icon icon-google-plus"></div></a></li><li><a href="https://www.kiltr.com/eden-scott-ltd" title="Follow us on Kiltr" target="_blank"><div class="icon icon-kiltr"></div></a></li></ul>  </div>
</div>
  </div>

    <div class="footer-content">

        <div class="container">
            <div class="row">
				  <div class="region region-footer">
    <div id="block-block-2" class="block block-block">

    
  <div class="content">
    <div class="col-xs-6 col-xxs-24">
	<address>
		<h5>Edinburgh</h5>
		26 St Andrew Square<br>
		Edinburgh<br>
		EH2 1AF<br>
		Tel: 0131 550 1100
	</address>
</div>


<div class="col-xs-6 col-xxs-24">
	<address>
		<h5>Glasgow</h5>
		The Exchange,<br>
		142 St Vincent Street<br>
		Glasgow<br>
		G2 5LA<br>
		Tel: 0141 410 1000
	</address>
</div>

<div class="col-xs-6 col-xxs-24">
	<address>
		<h5>Aberdeen*</h5>
		Citypoint House,<br>
		11 Chapel Street<br>
		Aberdeen<br>
		AB10 1SQ<br>
		Tel: 01224 653 382
	</address>
</div>  </div>
</div>
<div id="block-views-logos-block" class="block block-views">
        <div class="footer-logos">
                            <h5>Accreditations</h5>
                
        <div class="content">
            <div class="view view-logos view-id-logos view-display-id-block view-dom-id-993cc6d8a674dede470401b80063061e">
        
  
  
      <div class="view-content">
               <div class="logo-footer logo-container">
        
  <div class="field-image">
    <img src="https://www.edenscott.com/sites/default/files/Cyber%20Essentials%20Badge.png" width="250" height="211" alt="" />  </div>
    </div>

          <div class="logo-footer logo-container">
        
  <div class="field-image">
    <img src="https://www.edenscott.com/sites/default/files/IASME%20GDPR%20selfcert%20badge_0.jpg" width="250" height="145" alt="" />  </div>
    </div>

      </div>
  
  
  
  
  
  
</div>        </div>
    </div>
</div>  </div>
            </div>

            <div class="row">
                <div class="col-md-14 col-sm-24">
					  <div class="region region-footer-menu">
    <div id="block-menu-block-5" class="block block-menu-block">

    
  <div class="content">
    <div class="menu-block-wrapper menu-block-5 menu-name-menu-footer-links parent-mlid-0 menu-level-1">
  <ul class="list-inline menu-footer"><li class="first leaf menu-mlid-2242"><a href="/help" title="">Help</a></li>
<li class="leaf menu-mlid-2243"><a href="/sitemap" title="">Sitemap</a></li>
<li class="leaf menu-mlid-2244"><a href="https://www.edenscott.com/privacy-policy" title="">Privacy Notice</a></li>
<li class="leaf menu-mlid-2360"><a href="https://www.edenscott.com/cookie-policy" title="">Cookie Policy</a></li>
<li class="leaf menu-mlid-2361"><a href="https://www.edenscott.com/candidate-terms-and-conditions" title="">Terms and Conditions</a></li>
<li class="leaf menu-mlid-2245"><a href="/rss" title="">RSS</a></li>
<li class="leaf menu-mlid-2246"><a href="https://www.timesheetz.net/EtzWeb/Login/Login.aspx" title="">Timesheets</a></li>
<li class="leaf menu-mlid-2247"><a href="/careers" title="">Eden Scott Careers</a></li>
<li class="leaf menu-mlid-2248"><a href="/feedback" title="">Feedback</a></li>
<li class="leaf menu-mlid-2372"><a href="https://www.edenscott.com/cyber-essentials-and-iasme" title="">* Cyber Essentials and IASME</a></li>
<li class="last leaf menu-mlid-2561"><a href="https://www.edenscott.com/anti-slavery-and-human-trafficking-policy-2018" title="">Anti-Slavery and Human Trafficking Policy 2018 </a></li>
</ul></div>
  </div>
</div>
  </div>
                </div>

                <div class="col-md-10 col-sm-24">
                    <div class="copyright"> 2019 &copy; Eden
                        Scott | Website Design &amp; Build by <a
                            href="https://www.wearetheweather.co.uk/"
                            target="_blank" title="The Weather Digital">The
                            Weather Digital</a>
                    </div>
                </div>

            </div>

        </div>
    </div>

</footer>

<div class="job-search-mobile">
    <a href="#" class="refine-button"><span class="icon icon-arrow-66"></span>
        Advanced Search</a>
    <div class="job-search-container green-bck">
        <div class="container">
            <div class="row">
                <div class="col-xs-24">
					  <div class="region region-job-search">
    <div id="block-jobs-job-search-form" class="block block-jobs">

    
  <div class="content">
    <form action='/job-search/' method='POST'>
				<div class='row'>
					<div class='col-xs-8 col-sm-10 col-xxs-24'>
						<label class='control-label'>Keywords</label>
						<input type='text' id='keywords' value='' name='keywords' class='form-control' placeholder='Job Title, Keywords' />
					</div>
					
					<div class='col-xs-8 col-sm-7 col-xxs-24'>
						<label class='control-label'>Location</label>
						<select class='form-control' name='location' id='location' >
							<option value=''>Select Location</option>
							  <option value="89" >Scotland</option> <option value="91" > - Edinburgh</option> <option value="98" > - Glasgow</option> <option value="109" > - Aberdeen</option> <option value="112" > - Angus</option> <option value="105" > - Central Scotland</option> <option value="111" > - Dundee</option> <option value="92" > - East Lothian</option> <option value="99" > - Dunbartonshire</option> <option value="90" > - East Central Scotland</option> <option value="101" > - Ayrshire</option> <option value="110" > - Elgin</option> <option value="106" > - Falkirk</option> <option value="95" > - Fife</option> <option value="114" > - Highlands & Islands</option> <option value="103" > - Inverclyde</option> <option value="100" > - Lanarkshire</option> <option value="115" > - Inverness</option> <option value="93" > - Midlothian</option> <option value="104" > - Stirlingshire</option> <option value="102" > - Renfrewshire</option> <option value="96" > - Perth & Kinross</option> <option value="108" > - North East</option> <option value="176" > - North West Scotland</option> <option value="113" > - Speyside</option> <option value="107" > - Stirling</option> <option value="117" > - Scottish Borders</option> <option value="116" > - South West Scotland</option> <option value="97" > - West Central Scotland</option> <option value="94" > - West Lothian</option> <option value="88" >UK</option> <option value="126" > - Northern Ireland</option> <option value="118" > - England</option> <option value="119" > -- London</option> <option value="121" > -- North West England</option> <option value="120" > -- North East England</option> <option value="124" > -- South East England</option> <option value="123" > -- South West England</option> <option value="122" > -- Midlands</option> <option value="125" > - Wales</option> <option value="134" >Ireland</option> <option value="167" >Australia & Oceania</option> <option value="128" >North & Western Europe</option> <option value="129" > - Austria</option> <option value="130" > - Belgium</option> <option value="132" > - France</option> <option value="131" > - Finland</option> <option value="133" > - Germany</option> <option value="135" > - Italy</option> <option value="136" > - Luxembourg</option> <option value="137" > - Netherlands</option> <option value="138" > - Norway</option> <option value="151" > - Poland</option> <option value="139" > - Portugal</option> <option value="140" > - Spain</option> <option value="141" > - Sweden</option> <option value="142" > - Switzerland</option> <option value="143" >Eastern Europe</option> <option value="144" > - Albania</option> <option value="145" > - Belarus</option> <option value="146" > - Bulgaria</option> <option value="147" > - Estonia</option> <option value="148" > - Hungary</option> <option value="149" > - Latvia</option> <option value="150" > - Luthiania</option> <option value="152" > - Russia</option> <option value="153" > - Ukraine</option> <option value="154" >North America</option> <option value="159" > - USA</option> <option value="157" > - Canada</option> <option value="158" > - Mexico</option> <option value="156" > - Carribbean</option> <option value="155" > - Bermuda</option> <option value="160" >South America</option> <option value="175" > - Chile</option> <option value="166" >Africa</option> <option value="174" > - Angola</option> <option value="161" >Asia</option> <option value="162" > - China</option> <option value="163" > - Singapore</option> <option value="164" >Middle East</option> <option value="165" > - United Arab Emirates</option> <option value="127" >International</option>
						</select>
					</div>
					
					<div class='col-xs-8 col-sm-7 col-xxs-24'>
						<label class='control-label'>Sector</label>
						<select class='form-control' name='sector[]' id='sector'>
							<option value=''>Select Sector</option>
							  <option value="18" >Accounting and Finance</option> <option value="20" >Business Support </option> <option value="4299" >Construction </option> <option value="12809" >Digital</option> <option value="21" >Executive and Senior Management</option> <option value="22" >Financial Services</option> <option value="1991" >Food and Drink</option> <option value="4760" >Hospitality and Tourism</option> <option value="23" >Human Resources</option> <option value="18322" >Industrial</option> <option value="24" >Information Technology</option> <option value="25" >Legal</option> <option value="11184" >Life Sciences</option> <option value="27" >Manufacturing and Technology</option> <option value="1992" >Nuclear Energy</option> <option value="28" >Oil and Gas</option> <option value="29" >Public Sector</option> <option value="30" >Renewable Energy</option> <option value="31" >Sales and Marketing</option> <option value="32" >Supply Chain Recruitment</option>
						</select>
					</div>
				</div>
			
				<div class='row'>
					<div class='col-sm-10 col-xs-12 col-xxs-24'>
						<div class='form-50 left'>
						<label class='control-label'>Job Type</label>
						<select class='form-control' name='jobtype' id='jobtype' >
							 <option value="Any" >Any</option> <option value="1" >Temporary/Contract</option> <option value="2" >Permanent</option>
						</select>
						</div>
			
						<div class='form-50 right'>
						<label class='control-label'>Date Added</label>
						<select class='form-control' id='dateadded' name='dateadded'>
							 <option value="90" >Last 3 months</option> <option value="30" >Last 30 days</option> <option value="7" >Last 7 days</option> <option value="1" >Last 24 hours</option>
						</select>
						</div>
					</div>
			
			
					<div class='col-sm-7 col-xs-12 col-xxs-24'>
						<div class='form-50 left'>
						<label class='control-label'>Salary From</label>
						<select class='form-control' name='min' id='min'>
							<option value='0'>Any</option>
							 <option value="10000" >£10,000</option> <option value="20000" >£20,000</option> <option value="30000" >£30,000</option> <option value="40000" >£40,000</option> <option value="50000" >£50,000</option> <option value="60000" >£60,000</option> <option value="70000" >£70,000</option> <option value="80000" >£80,000</option> <option value="90000" >£90,000</option> <option value="100000" >£100,000</option>
						</select>
						</div>
			
						<div class='form-50 right'>
						<label class='control-label'>Salary To</label>
						<select class='form-control' name='max' id='max'>
							<option value=''>Any</option>
							 <option value="10000" >£10,000</option> <option value="20000" >£20,000</option> <option value="30000" >£30,000</option> <option value="40000" >£40,000</option> <option value="50000" >£50,000</option> <option value="60000" >£60,000</option> <option value="70000" >£70,000</option> <option value="80000" >£80,000</option> <option value="90000" >£90,000</option> <option value="100000" >£100,000</option>
						</select>
						</div>
					</div>
			
					<div class='col-sm-7 col-xs-24'>
						<button type='submit' class='btn btn-secondary'>Update results</button>
					</div>
			
				</div>
			</form>  </div>
</div>
  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="cookie-message" data-cookie-expiry="60"><div id="block-block-9" class="block block-block">

    
  <div class="content">
    <p>We use cookies on this website. By continuing to use our website without changing the settings, you are agreeing to our use of cookies. Find out <a href="/cookie-policy" target="_blank" class="btn btn-white btn-sm">how to manage cookies</a> or <a href="#" class="btn btn-white btn-sm" id="cookie-agree">accept &amp; hide message</a></p>
  </div>
</div>
</div><script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/retina.min.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/matchHeight.min.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/modernizr.custom.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/basescripts.min.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/jquery.query-object.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/site.js?pziyw7"></script>
<script type="text/javascript" src="https://www.edenscott.com/sites/all/themes/edenscott/js/cookies.js?pziyw7"></script>

</body>
</html>
