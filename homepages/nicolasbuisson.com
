<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>Nicolas Buisson Photography</title>

<!-- This site is optimized with the Yoast SEO plugin v9.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Food Photographer &amp; Creative Director. Lives in London. Born and raised in Paris, France. Italian from the heart."/>
<link rel="canonical" href="https://www.nicolasbuisson.com/" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Nicolas Buisson Photography" />
<meta property="og:description" content="Food Photographer &amp; Creative Director. Lives in London. Born and raised in Paris, France. Italian from the heart." />
<meta property="og:url" content="https://www.nicolasbuisson.com/" />
<meta property="og:site_name" content="Nicolas Buisson Photography" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Food Photographer &amp; Creative Director. Lives in London. Born and raised in Paris, France. Italian from the heart." />
<meta name="twitter:title" content="Nicolas Buisson Photography" />
<script type='application/ld+json'>{"@context":"https://schema.org","@type":"WebSite","@id":"https://www.nicolasbuisson.com/#website","url":"https://www.nicolasbuisson.com/","name":"Nicolas Buisson","potentialAction":{"@type":"SearchAction","target":"https://www.nicolasbuisson.com/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"https://schema.org","@type":"Person","url":"https://www.nicolasbuisson.com/","sameAs":["https://www.facebook.com/NICOLASBUISSONphotography","https://www.instagram.com/nicobuisson/","http://www.linkedin.com/in/NicolasBuisson"],"@id":"#person","name":"Nicolas Buisson"}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Nicolas Buisson Photography &raquo; Flux" href="https://www.nicolasbuisson.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Nicolas Buisson Photography &raquo; Flux des commentaires" href="https://www.nicolasbuisson.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.nicolasbuisson.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.7"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='wp-block-library-css'  href='https://www.nicolasbuisson.com/wp-includes/css/dist/block-library/style.min.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='parent-style-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/style.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='child-style-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe-child/style.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='thebe-fonts-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/css/fonts.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='thebe-font-awesome-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/css/font-awesome/font-awesome.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='thebe-pe-icon-7-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/css/pe-icon-7-stroke.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='thebe-other-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/css/other.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='thebe-style-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe-child/style.css?ver=5.0.7' type='text/css' media='all' />
<style id='thebe-style-inline-css' type='text/css'>
@font-face {
				  font-family: "HelveticaNeue MD";
				  src: url("https://www.nicolasbuisson.com/wp-content/uploads/thebe_custom_font/HelveticaNeueLTStd-Md.woff2") format("woff2"),url("https://www.nicolasbuisson.com/wp-content/uploads/thebe_custom_font/HelveticaNeueLTStd-Md.woff") format("woff");
				  font-weight: normal;
				  font-style: normal;
				}
@font-face {
				  font-family: "HelveticaNeue TH";
				  src: url("https://www.nicolasbuisson.com/wp-content/uploads/thebe_custom_font/HelveticaNeueLTStd-Th.woff2") format("woff2"),url("https://www.nicolasbuisson.com/wp-content/uploads/thebe_custom_font/HelveticaNeueLTStd-Th.woff") format("woff");
				  font-weight: normal;
				  font-style: normal;
				}
body,textarea,input,button,select,.single-meta a,.h .intro{font-family:HelveticaNeue TH,sans-serif;}
h1,h2,h3,h4,h5,h6,strong,b,mark,legend,blockquote,.price-amount,.title,.h a,.widget-title,.category-nav li,.filter{font-family:HelveticaNeue TH,sans-serif;}
.main-menu > ul > li > a,.addition-menu p,.popup-sub-menu a{font-family:HelveticaNeue MD,sans-serif;}
.pt-popup .h,h1,.section-title .h,.sc-slider.small-dots .h, .main-menu-outside a{font-family:HelveticaNeue TH,sans-serif;font-weight:800;}
.list-category a, .widget-title{font-family:HelveticaNeue MD,sans-serif;font-weight:700;}
input.search-field:focus,.wpcf7 input:not(.wpcf7-submit):focus,.wpcf7 textarea:focus,.single-meta a:not(.btn):hover,a.text-link:hover,.list-meta a:hover,.fn a:hover,.pic-loader:after,.single-tags a:hover,.single-related .category a:hover,.comment-root .reply a:hover,.comment-root textarea:focus,.comment-root p:not(.form-submit) input:focus,.body-category-wrap a:hover,.blog-list a.btn:hover,.pages li .current.page-numbers:after,.intro .h a:not(.btn),.grid-list .list-category a:hover,.blog-list.style-01 a.btn:hover,.blog-list.style-02 .item .h a:hover,.site-bg-code.style-01 i.highlight,.tagcloud a:hover{
		border-color: #ff3a3a;
	}
.comment-awaiting-moderation:before,.blog-list.style-01 a.btn,.comment-notes:before,.call-popup:hover:before,.sub-menu li:hover a,.pt-social a:hover,.recentcomments span,.go-top:hover:before,#cancel-comment-reply-link:hover:before,.widget a:hover,.widget_archive li,.widget_rss cite,.sub-menu li.current_page_item a:before,.blog-list:not(.style-02) .category a:hover,.blog-list:not(.style-02) .item .h a:hover,.cate-item a:hover,#music-player:hover:before,.comment-root .title span,.single-extend .text h2 i,i.close-bar-single:hover:before,.sub-menu .current-menu-ancestor>a:before,.item:not(.text-post) .category:before,.item.text-post:not([data-bg-color]) .category:before,.title-group .h i,.sc-mixbox .title:after,.single-extend h2:before,.comment-respond .h.title:before{
		color:#ff3a3a;
	}
.main-menu a:after,.main-menu-outside a:after,.title-group:after,.v-ctrl.v-pause:hover,.pic-list .h span:after,.category-nav .active:after,.single-main-intro .category a,.filter li:after,em.close-category:hover,.category-nav li.active:after,i.call-filter,.pages li .current,a.btn:hover:after,.item.sticky .img:before,.item.sticky.text-post:before,.cate-nav-ctrl:hover:before,#nprogress .bar,.reply a:hover,.comment-root input[type="submit"]:hover,.text-area input[type="submit"]:hover,.default-template-page input[type="submit"]:hover.blog-list.style-02 .item:not(.text-post) .category a:hover,.blog-list:not(.style-02) em.show-all-category:hover,.single-related .category a:hover,.tagcloud a:hover,.widget-title:before,.wpcf7-submit,.single-related h5:after,.title-group h1:after,.title-group .intro:after,.title-group .intro:before,.small-btn a:hover,.owl-nav>div:hover,.grid-list.caption-02 .list-category a:hover,.loader-icon i,.sc-text-carousel .owl-dot.active span,.pc-mode .call-popup:hover:before,.call-search.btn:hover,.single-tags a:hover{
		background-color: #ff3a3a;
	}
::selection{
		color:#fff;
		background:#ff3a3a;
		text-shadow:none;
	}
html{font-size:14px;}

</style>
<link rel='stylesheet' id='thebe-widget-css'  href='https://www.nicolasbuisson.com/wp-content/themes/thebe/css/widget.css?ver=5.0.7' type='text/css' media='all' />
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://www.nicolasbuisson.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.nicolasbuisson.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.nicolasbuisson.com/wp-includes/wlwmanifest.xml" /> 
<link rel='shortlink' href='https://www.nicolasbuisson.com/' />
<link rel="alternate" type="application/json+oembed" href="https://www.nicolasbuisson.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nicolasbuisson.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.nicolasbuisson.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nicolasbuisson.com%2F&#038;format=xml" />
<link rel="icon" href="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/faviconnb-150x150.png" sizes="32x32" />
<link rel="icon" href="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/faviconnb-300x300.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/faviconnb-300x300.png" />
<meta name="msapplication-TileImage" content="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/faviconnb-300x300.png" />

<!-- BEGIN ExactMetrics v5.3.7 Universal Analytics - https://exactmetrics.com/ -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-134073351-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- END ExactMetrics Universal Analytics -->
</head>
<body class="home page-template page-template-page-portfolio page-template-page-portfolio-php page page-id-73 page-portfolio site-dark m-nav-fixed" data-color="#ff3a3a">
	<div class="site-loader">
				<div class="loader-icon"><i></i><i></i><i></i></div>
		<div class="loader-bg"></div>
	</div>
					<div class="site-bg">
										<div class="bg-full" data-src="https://player.vimeo.com/video/185626875" data-type="3" data-volume="0"></div>
										</div>
				
	<header class="pt-header menu-style-01 menu-open-mode-02 outside-menu no-addition-menu" data-text-color="white" data-bg-color="#000000">
		<div class="wrap">

					<div class="logo">
			<a href="https://www.nicolasbuisson.com/">
				<img alt="Logo" src="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/Logo2018-3-3-1000x177.png" data-retina="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/Logo2018.png" width="1000" height="177">
				<img alt="Addition logo" class="addition" src="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/Logo2018-3-3-360x63.png" data-retina="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/Logo2018.png" width="360" height="63">
			</a>
		</div>
					
			<div class="header-right">
							<i class="call-menu btn"></i>
			</div>

			<div class="menu-wrap">
				<i class="close-menu btn"></i>

				<nav class="main-menu"><ul id="main-menu-list" class="menu-list"><li id="menu-item-1655" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-73 current_page_item menu-item-1655"><a href="https://www.nicolasbuisson.com/">HOME</a></li>
<li id="menu-item-933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-933"><a href="https://www.nicolasbuisson.com/art-prints/">ART PRINTS</a></li>
<li id="menu-item-895" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-895"><a href="https://www.nicolasbuisson.com/film/">FILM</a></li>
<li id="menu-item-883" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-883"><a href="https://www.nicolasbuisson.com/image-bank/">IMAGE BANK</a></li>
<li id="menu-item-898" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-898"><a href="https://www.nicolasbuisson.com/contact/">CONTACT</a></li>
</ul></nav>				
			</div>
		</div>
	</header>
	
	<div class="main-content">
	<div class="pic-list grid-list ajax type-auto col-auto caption-01">
		<div class="wrap">
					<div class="item filter-cat-70 filter-cat-54 filter-cat-60 filter-cat-69" data-w="640" data-h="826" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/02/Hand-36-3-3-640x826.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/porn-hands/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/beauty/">Beauty</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/food/">Food</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/sweets/">Sweets</a>
																											</div>
																<h2>
										<span>Porn Hands</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/porn-hands/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-64 filter-cat-60 filter-cat-65" data-w="640" data-h="811" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/01/ANGEL-3-3-640x811.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/977/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/fragrances/">Fragrances</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/popsicle/">Popsicle</a>
																											</div>
																<h2>
										<span>Popsicle</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/977/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-70 filter-cat-60 filter-cat-77" data-w="640" data-h="452" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/01/LASH-MAGAZINE-222-3-3-640x452.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/lash-magazine/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/beauty/">Beauty</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/jewellery/">Jewellery</a>
																											</div>
																<h2>
										<span>Venom</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/lash-magazine/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-70 filter-cat-54 filter-cat-60 filter-cat-59 filter-cat-69" data-w="640" data-h="853" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/pornFood_0284-1-3-3-640x853.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/porn-food/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/beauty/">Beauty</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/food/">Food</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/prints/">Prints</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/sweets/">Sweets</a>
																											</div>
																<h2>
										<span>Porn Food</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/porn-food/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-60 filter-cat-73" data-w="640" data-h="781" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/02/watches_0048-rvb-def1-3-3-640x781.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/seizure/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/watches/">watches</a>
																											</div>
																<h2>
										<span>The seizure</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/seizure/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-54 filter-cat-60 filter-cat-77" data-w="640" data-h="811" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/02/AJ_0037-3-3-640x811.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/haute-food/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/food/">Food</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/jewellery/">Jewellery</a>
																											</div>
																<h2>
										<span>Haute Food</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/haute-food/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-67 filter-cat-60" data-w="640" data-h="640" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/01/BooM_0023-3-3-640x640.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/explosions/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/explosions/">Explosions</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																											</div>
																<h2>
										<span>Explosions</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/explosions/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-70 filter-cat-60 filter-cat-77 filter-cat-59" data-w="640" data-h="767" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/JdeeD2_0282_1-3-3-640x767.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/jdeed/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/beauty/">Beauty</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/jewellery/">Jewellery</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/prints/">Prints</a>
																											</div>
																<h2>
										<span>Strange Fruit</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/jdeed/"></a>
																					</h2>
															</div>

					</div>
				</div>
								<div class="item filter-cat-70 filter-cat-60 filter-cat-77 filter-cat-59" data-w="640" data-h="640" data-bg-color="" data-text-color="white" data-header-color="white" data-title-color="">
					<div class="inner-wrap">
						<div class="img">							<div class="img-main">
							<div class="bg-full" data-bg="https://www.nicolasbuisson.com/wp-content/uploads/2019/02/Felicie_0031-3-3-640x640.jpg"></div>									<a class="full" data-href="https://www.nicolasbuisson.com/project_list/felicie/"></a>
																</div>
													</div>
						<div class="text">
							<div class="list-category">
																												<a href="https://www.nicolasbuisson.com/project_cat/beauty/">Beauty</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/homepage/">Home</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/jewellery/">Jewellery</a>
																																									<a href="https://www.nicolasbuisson.com/project_cat/prints/">Prints</a>
																											</div>
																<h2>
										<span>Félicie</span>
																						<a class="full" data-href="https://www.nicolasbuisson.com/project_list/felicie/"></a>
																					</h2>
															</div>

					</div>
				</div>
				<div class="pages"><ul><li><span aria-current='page' class='page-numbers current'>1</span></li><li><a class='page-numbers' href='https://www.nicolasbuisson.com/page/2/'>2</a></li><li><a class="next page-numbers" href="https://www.nicolasbuisson.com/page/2/">&gt;</a></li></ul></div>	</div>
</div><p style="text-align: center;">all images © Nicolas Buisson 2019</p>		</div>
	<footer class="pt-footer">
		<div class="footer-left">
			<div class="pt-social">
				<ul>
					<li><a href="https://www.facebook.com/NICOLASBUISSONphotography" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="https://www.instagram.com/nicobuisson" target="_blank"><i class="fa fa-instagram"></i></a></li><li><a href="http://www.linkedin.com/in/NicolasBuisson" target="_blank"><i class="fa fa-linkedin"></i></a></li><li><a href="https://vimeo.com/nicobuisson" target="_blank"><i class="fa fa-vimeo"></i></a></li>				</ul>
			</div>
					</div>
		<div class="footer-right">
			<p class="copyright"></p>
					</div>
	</footer>

								<div class="footer-widgets wide" data-header-color="white">
					<div class="wrap">
					<div id="text-2" class="widget widget_text"><div class="widget-title">Nicolas Buisson</div>			<div class="textwidget"><p><img class="wp-image-880 size-thumbnail alignleft" src="https://www.nicolasbuisson.com/wp-content/uploads/2018/09/nicolas-buisson-rounded-150x150.png" alt="" width="150" height="150" /><strong>Food Photographer, Director &amp; Creative Director.</strong></p>
<p><strong>I was born and raised in Paris but l live in London.</strong></p>
<p>&nbsp;</p>
<p>Ask Mel for more : <strong><a href="mailto:melanie@nicolasbuisson.com">melanie@nicolasbuisson.com</a></strong></p>
</div>
		</div><div id="text-4" class="widget widget_text"><div class="widget-title">Me, me, me, me</div>			<div class="textwidget"><p>My Grand Mother was born in Syria, my Mother was born in Morocco and my family is from Naples : I&rsquo;m a traveller</p>
<p>Meeting photographer Herb Ritts in San Francisco at the age 15 drove me to be a photographer.</p>
<p>After a year of Architecture studies (where I spent most of my nights in the photolab), I finally graduated from ICART school in 1994.</p>
<p>Since then I decided to persue photography as a visual art.</p>
<p>2008 : Meeting Michelin Chefs Alain Ducasse and Yannick Alléno raised my vision to the eclectic, yet disruptive highest level of luxury.</p>
</div>
		</div><div id="text-3" class="widget widget_text"><div class="widget-title">They trusted me</div>			<div class="textwidget"><p>EDITORIAL : AD, A Nous Paris, Air France Madame, Capital, Code, Elle, FOUR, Fudge, Glamour, Grazia, Harpers Bazaar, Interni, Idéat, L’express, Madame Figaro, Le Monde, Numéro, l’Officiel, Tatler, YAM</p>
<p>&nbsp;</p>
<p>AGENCIES : Air Paris, Auditoire, Australie, BETC, Buzzman, CLM/BBDO, Colorado, Euro RSCG  C&amp;O, Fred &amp; Farid, Heresie, Jesus &amp; Gabriel, JWT , La Chose, Léo Burnett, Ogilvy,  Publicis Conseil, Publicis et Nous, Rosapark, TBWA,  Saachi, Y &amp; R, Future Brand, Mullen Lowe</p>
<p>&nbsp;</p>
<p>BRANDS : Air France,  Armani,  Boucheron,  Cacharel, Citroën, Dior Parfums, Dior Joaillerie, EDF, Fauchon,  Fendi,  Giorgio Armani, Givenchy, Groupe Alain Ducasse ,  Groupe Yannick Alléno, Ladurée,  Laurent-Perrier, L’Oréal,  Marc Jacobs,  Mercedes Benz , Monoprix, Movenpick,  Nespresso, Nestlé,  Pierre Marcolini, Pouchkine, Yves Saint Laurent</p>
</div>
		</div><p class="copyright"></p>					</div>
									</div>
							
	
				<div class="ajax-content project">
				<div class="wrap">
					<i class="m-close-single"></i>
					<div class="close-single"></div>
					<div class="ajax-target"></div>
				</div>
			</div>
				
	<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe-child/js/main.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/jquery.easing.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/isotope.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/jquery.fleximages.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/tweenjs-0.6.2.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/nprogress.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/owl.carousel.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/plugins/howler.min.js?ver=5.0.7'></script>
<script type='text/javascript'>
!(function(win){
			"use strict";
			win.__pt_theme_root_url = "https://www.nicolasbuisson.com/wp-content/themes/thebe";
			win.__pt_upload_url = "https://www.nicolasbuisson.com/wp-content/uploads";
		})(window);
</script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-content/themes/thebe/js/theme/pt-plugins.min.js?ver=5.0.7'></script>
<script type='text/javascript' src='https://www.nicolasbuisson.com/wp-includes/js/wp-embed.min.js?ver=5.0.7'></script>
</body>
</html>