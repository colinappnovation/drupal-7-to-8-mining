<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-GB">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-GB">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-GB">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="https://www.autotrain.org/xmlrpc.php" />
<!--[if lt IE 9]>
<script src="https://www.autotrain.org/wp-content/themes/iconic-one/js/html5.js" type="text/javascript"></script>
<![endif]-->
<title>AutoTrain &#8211; Web, Video and E-Learning Technology Developers</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="AutoTrain &raquo; Feed" href="https://www.autotrain.org/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.autotrain.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.7"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='wp-block-library-css'  href='https://www.autotrain.org/wp-includes/css/dist/block-library/style.min.css?ver=5.0.7' type='text/css' media='all' />
<link rel='stylesheet' id='themonic-fonts-css'  href='https://fonts.googleapis.com/css?family=Ubuntu%3A400%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='themonic-style-css'  href='https://www.autotrain.org/wp-content/themes/iconic-one/style.css?ver=1.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='custom-style-css'  href='https://www.autotrain.org/wp-content/themes/iconic-one/custom.css?ver=5.0.7' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='themonic-ie-css'  href='https://www.autotrain.org/wp-content/themes/iconic-one/css/ie.css?ver=20130305' type='text/css' media='all' />
<![endif]-->
<script type='text/javascript' src='https://www.autotrain.org/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.autotrain.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://www.autotrain.org/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.autotrain.org/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.autotrain.org/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.0.7" />
<link rel="canonical" href="https://www.autotrain.org/" />
<link rel='shortlink' href='https://www.autotrain.org/' />
<link rel="alternate" type="application/json+oembed" href="https://www.autotrain.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.autotrain.org%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.autotrain.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.autotrain.org%2F&#038;format=xml" />
<style type='text/css'></style>
<style type="text/css" id="custom-background-css">
body.custom-background { background-color: #f8ffea; }
</style>
<link rel="icon" href="https://www.autotrain.org/wp-content/uploads/2019/02/AutoTrain_logo400-150x150.png" sizes="32x32" />
<link rel="icon" href="https://www.autotrain.org/wp-content/uploads/2019/02/AutoTrain_logo400-300x300.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.autotrain.org/wp-content/uploads/2019/02/AutoTrain_logo400-300x300.png" />
<meta name="msapplication-TileImage" content="https://www.autotrain.org/wp-content/uploads/2019/02/AutoTrain_logo400-300x300.png" />
		<style type="text/css" id="wp-custom-css">
			.themonic-nav li a {
	background-color:#f8ffea;
}

.themonic-nav ul.nav-menu, .themonic-nav div.nav-menu > ul {
		background-color:#f8ffea;
}
.themonic-nav .current-menu-item > a, .themonic-nav .current-menu-ancestor > a, .themonic-nav .current_page_item > a, .themonic-nav .current_page_ancestor > a
{
	background-color:#efed95;
	color:black;
	font-weight:bold;
}

#searchsubmit
{
	background-color:#efed95;
	color:black;
	font-weight:bold;
}

.themonic-nav ul.nav-menu, .themonic-nav div.nav-menu > ul {
		border-bottom-color:#efed95;
}

footer[role="contentinfo"]{
		background-color:#f8ffea;
	color:black;
}

.themonic-nav li a:hover
{
  color: black;
}		</style>
	</head>
<body class="home page-template-default page page-id-21 custom-background custom-font-enabled single-author">
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
					
		<div class="themonic-logo">
        <a href="https://www.autotrain.org/" title="AutoTrain" rel="home"><img src="https://www.autotrain.org/wp-content/uploads/2019/02/logo-tagline-2019.png" alt="AutoTrain"></a>
		</div>
		

		
		<nav id="site-navigation" class="themonic-nav" role="navigation">
			<a class="assistive-text" href="#main" title="Skip to content">Skip to content</a>
			<ul id="menu-top" class="nav-menu"><li id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-72"><a href="https://www.autotrain.org">Home</a></li>
<li id="menu-item-171" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-171"><a href="https://www.autotrain.org/category/news/">News</a></li>
<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-74"><a href="https://www.autotrain.org/about-us/">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="https://www.autotrain.org/about-us/collaborators/">Collaborators</a></li>
	<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="https://www.autotrain.org/about-us/autotrain-project/">AutoTrain Project</a></li>
	<li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="https://www.autotrain.org/about-us/euromotor-project/">EuroMotor Project</a></li>
	<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="https://www.autotrain.org/about-us/accessibility-privacy-and-cookies/">Accessibility, Privacy and Cookies</a></li>
	<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="https://www.autotrain.org/about-us/privacy-policy-2/">Privacy Policy</a></li>
</ul>
</li>
<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="https://www.autotrain.org/contact/">Contact Us</a></li>
<li id="menu-item-75" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-75"><a href="https://www.autotrain.org/services/">Services</a>
<ul class="sub-menu">
	<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="https://www.autotrain.org/services/autoview-moodle-plugin/">AutoView Moodle plugin</a></li>
	<li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="https://www.autotrain.org/services/elearning/">e-Learning Hosting and Development</a></li>
	<li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84"><a href="https://www.autotrain.org/services/euromotor-virtual-college/">EuroMotor Virtual College</a></li>
	<li id="menu-item-85" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85"><a href="https://www.autotrain.org/services/internet-video-consultancy/">Internet Video Consultancy</a></li>
	<li id="menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86"><a href="https://www.autotrain.org/services/cms/">Content Management Systems</a></li>
</ul>
</li>
</ul>		</nav><!-- #site-navigation -->
		<div class="clear"></div>
	</header><!-- #masthead -->

	<div id="main" class="wrapper">
	<div id="primary" class="site-content">
		<div id="content" role="main">

							
	<article id="post-21" class="post-21 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title"></h1>
		</header>

		<div class="entry-content">
			<p><img class="aligncenter wp-image-67 size-full" src="https://www.autotrain.org/wp-content/uploads/2019/02/strip.jpg" alt="" width="650" height="30" srcset="https://www.autotrain.org/wp-content/uploads/2019/02/strip.jpg 650w, https://www.autotrain.org/wp-content/uploads/2019/02/strip-300x14.jpg 300w" sizes="(max-width: 650px) 100vw, 650px" /></p>
<p>AutoTrain is a Web Technology Development company, specialising in e-Learning Systems (including Virtual Learning Environments), Content Management Systems and Internet Video for all sectors of business, industry and education. We provide development services to our customers, both on and off site.</p>
<p>AutoTrain was founded in 2006 as a University of Birmingham spin-off company. We have been working on e-Learning technology projects at the University since 1998.</p>
<p>For more information please use the links on this page to browser our services. You can email us on <a href="mailto:info@autotrain.org">info@autotrain.org</a>, or speak to us in person by phone on +44 (0)844 487 4117.</p>
					</div><!-- .entry-content -->
		<footer class="entry-meta">
					</footer><!-- .entry-meta -->
	</article><!-- #post -->
							
		</div><!-- #content -->
	</div><!-- #primary -->


			<div id="secondary" class="widget-area" role="complementary">
			<aside id="custom_html-5" class="widget_text widget widget_custom_html"><p class="widget-title">EPMA Courses</p><div class="textwidget custom-html-widget"><div style="text-align:center">
	<a href="http://epma.autotrain.org">
		<img src="/wp-content/uploads/2019/02/epma.jpg" width="140" height="41" alt="EPMA"/></a></div>
<a href="http://epma.autotrain.org">
	Free Powder Metallurgy Courses Hosted by AutoTrain</a></div></aside><aside id="custom_html-3" class="widget_text widget widget_custom_html"><p class="widget-title">EuroMotor Virtual College</p><div class="textwidget custom-html-widget"><div style="text-align:center">
	<a href="http://www.euromotor.org">
	<img src="/wp-content/uploads/2019/02/euromotor-44.jpg" width="140" height="44" /></a>
</div></div></aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<p class="widget-title">Recent Posts</p>		<ul>
											<li>
					<a href="https://www.autotrain.org/2019/01/18/autotrain-at-moodlemoot-uk-2019/">AutoTrain at MoodleMoot UK 2019</a>
											<span class="post-date">18/01/2019</span>
									</li>
											<li>
					<a href="https://www.autotrain.org/2018/08/30/autotrain-at-pycon-uk-2018/">AutoTrain at PyCon UK 2018</a>
											<span class="post-date">30/08/2018</span>
									</li>
											<li>
					<a href="https://www.autotrain.org/2018/08/16/autoview-repository-file-manager-and-course-file-area-updated/">AutoView, Repository File Manager and Course File Area updated</a>
											<span class="post-date">16/08/2018</span>
									</li>
											<li>
					<a href="https://www.autotrain.org/2018/01/15/autotrain-goes-to-moodlemoot-uk-2018/">AutoTrain goes to MoodleMoot UK 2018</a>
											<span class="post-date">15/01/2018</span>
									</li>
											<li>
					<a href="https://www.autotrain.org/2017/08/08/autotrain-at-pyconuk-2017/">AutoTrain at PyconUK 2017</a>
											<span class="post-date">08/08/2017</span>
									</li>
											<li>
					<a href="https://www.autotrain.org/2017/01/04/autotrain-at-moodlemoot-uk-2017/">AutoTrain at MoodleMoot UK 2017</a>
											<span class="post-date">04/01/2017</span>
									</li>
					</ul>
		</aside><aside id="search-2" class="widget widget_search"><form role="search" method="get" id="searchform" class="searchform" action="https://www.autotrain.org/">
				<div>
					<label class="screen-reader-text" for="s">Search for:</label>
					<input type="text" value="" name="s" id="s" />
					<input type="submit" id="searchsubmit" value="Search" />
				</div>
			</form></aside>		</div><!-- #secondary -->
		</div><!-- #main .wrapper -->
	<footer id="colophon" role="contentinfo">
		<div class="site-info">
		<div class="footercopy">EuroMotor-AutoTrain LLP<br />
Email : <a href="info@autotrain.org">info@autotrain.org</a><br />
Phone : +44 (0) 844 487 4117
</div>
		<div class="footercredit">UK Registration number: OC317070
VAT number: 865 0351 29</div>
		<div class="clear"></div>
		</div><!-- .site-info -->
		</footer><!-- #colophon -->
		<div class="site-wordpress">
				<a href="http://themonic.com/iconic-one/">Iconic One</a> Theme | Powered by <a href="http://wordpress.org">Wordpress</a>
				</div><!-- .site-info -->
				<div class="clear"></div>
</div><!-- #page -->

<script type='text/javascript' src='https://www.autotrain.org/wp-content/themes/iconic-one/js/selectnav.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.autotrain.org/wp-includes/js/wp-embed.min.js?ver=5.0.7'></script>
</body>
</html>