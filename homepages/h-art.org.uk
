<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.h-art.org.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="5-13th September 2020 Herefordshire Art Week is a nine day art trail, for artists, craftmakers and creative businesses to open studios, galleries and group exhibitions to raise their profile and sell artwork with the majority of venues opening each day between 11am and 5pm. But, please check the information on the venues you plan to visit, as some open earlier and close later," />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.h-art.org.uk/home" />
<link rel="shortlink" href="https://www.h-art.org.uk/node/126" />
  <title>Home | Herefordshire Art Week</title>
  <!-- Start of favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="/sites/all/themes/hart/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/sites/all/themes/hart/images/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/sites/all/themes/hart/images/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/sites/all/themes/hart/images/favicons/manifest.json">
  <link rel="mask-icon" href="/sites/all/themes/hart/images/favicons/safari-pinned-tab.svg" color="#e3147c">
  <link rel="shortcut icon" href="/sites/all/themes/hart/images/favicons/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="h.Art">
  <meta name="application-name" content="h.Art">
  <meta name="msapplication-config" content="/sites/all/themes/hart/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- End of favicons -->
  <link type="text/css" rel="stylesheet" href="https://www.h-art.org.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.h-art.org.uk/sites/default/files/css/css_4QodHjYryEaXYrFICqsNfWXh_sHUecUAyaQ_bu4Vy0c.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.h-art.org.uk/sites/default/files/css/css_b7vv72THm3TlKRYQIOssGfSG3HpO_LjGV8adtGyC0Y0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.h-art.org.uk/sites/default/files/css/css_vk6Ub9sNRzURPH7uXRlIXZliD6RbAMxfsSsF7QAY1kQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.h-art.org.uk/sites/default/files/css/css_34UpNww6u2GbQcJGUgL0NW96x5qkUDxmbuUWqpmRYww.css" media="print" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Asap:400,500,700" rel="stylesheet">
  <script type="text/javascript" src="https://www.h-art.org.uk/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script type="text/javascript" src="https://www.h-art.org.uk/sites/default/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-98406446-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.h-art.org.uk/sites/default/files/js/js_qe6QiZRCZNHXp3ia7DxOOOX69TXJWnTVwaN9n7UDogQ.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"hart","theme_token":"B68hCd9RnXpv2_juyyKG00T0Xfv0eMHJFmsfptc7AHQ","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/hart\/js\/build\/modernizr.js":1,"sites\/all\/themes\/hart\/js\/build\/macy.js":1,"sites\/all\/themes\/hart\/js\/build\/scripts.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/themes\/hart\/css\/tabs.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/hart\/css\/style.css":1,"sites\/all\/themes\/hart\/css\/print.css":1}},"better_exposed_filters":{"views":{"featured_artists":{"displays":{"block_1":{"filters":[]}}},"parners":{"displays":{"block":{"filters":[]}}},"banners":{"displays":{"embed_1":{"filters":[]}}}}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>
  <script type="text/javascript">
  /* gulpicon Stylesheet Loader | https://github.com/filamentgroup/gulpicon | (c) 2012 Scott Jehl, Filament Group, Inc. | MIT license. */
  window.gulpicon=function(e){if(e&&3===e.length){var t=window,n=!(!t.document.createElementNS||!t.document.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect||!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")||window.opera&&-1===navigator.userAgent.indexOf("Chrome")||-1!==navigator.userAgent.indexOf("Series40")),o=function(o){var a=t.document.createElement("link"),r=t.document.getElementsByTagName("script")[0];a.rel="stylesheet",a.href=e[o&&n?0:o?1:2],a.media="only x",r.parentNode.insertBefore(a,r),setTimeout(function(){a.media="all"})},a=new t.Image;a.onerror=function(){o(!1)},a.onload=function(){o(1===a.width&&1===a.height)},a.src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="}};
  gulpicon(["/sites/all/themes/hart/images/icons/css/icons.png.css", "/sites/all/themes/hart/images/icons/css/icons.fallback.css"]);
  </script>
  <noscript><link href="/sites/all/themes/hart/images/icons/css/icons.fallback.css" rel="stylesheet"></noscript>  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-126 node-type-page role-anonymous-user" >
  <div id="skip">
    <a href="#content">Jump to Navigation</a>
  </div>
    <div id="page" class="page with-navigation">

  <!-- ______________________ HEADER _______________________ -->

  <header id="header">
    <div class="container page-padding">
      <div class="header-logo">
                  <a href="/" title="Herefordshire Art Week" rel="home" id="logo">
            <img src="https://www.h-art.org.uk/sites/all/themes/hart/svgs/logo.svg" alt="Herefordshire Art Week" />
          </a>
              </div>
      <div class="header-right">
        <a class="icon icon-menu" href="javascript:void(0)"></a>
                  <div id="header-region">
            <div class="region region-header">
  <div class="block block-search block-odd first last block" data-bid="49">
        <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." placeholder="Search" type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="icon icon-search form-submit" type="submit" id="edit-submit" name="op" value="Search" /></div><input type="hidden" name="form_build_id" value="form-g-KsbDpmoMo1sM0FASSdDaTIlk_mzdzqjMq2HTlu-iA" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div><!-- /block -->
</div>          </div>
                          <nav id="navigation" class="menu with-primary">
            <a class="home-icon" title="Home" href="/"><span class="icon icon-home"></span></a><ul id="primary" class="main-menu"><li class="menu-219 first"><a href="/about-hart">About</a></li>
<li class="menu-632"><a href="/explore/location" title="">Explore</a></li>
<li class="menu-624"><a href="/take-part">Take Part</a></li>
<li class="menu-739"><a href="/featured-artists">Featured Artists</a></li>
<li class="menu-58100"><a href="/news">News</a></li>
<li class="menu-340 last"><a href="/contact" title="">Contact</a></li>
</ul>          </nav><!-- /navigation -->
              </div>
    </div>
    <div class="breadcrumb-wrapper">
      <div class="container">
              </div>
    </div>
  </header><!-- /header -->

    <nav class="fixed-navigation"><div class="container"></div></nav>

      <div class="view view-banners view-id-banners view-display-id-embed_1 view-dom-id-cc3a640a66e46160f887c7c167d501ed">
        
  
  
      <div class="view-content">
        <div data-slide="0"  class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-banner-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/banner_large/public/banners/Shelly%20Perkins_Roe%20Rhythms.jpg?itok=doWhBijx" width="1400" height="440" alt="illustration" title="Roe Rhythms" /></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Shelly Perkins</span>  </div>  </div>
  <div data-slide="1"  class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-banner-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/banner_large/public/banners/Damian%20Keefe%20_I%20love%20to%20make%20drinking%20vessels.jpg?itok=jq-1__gb" width="1400" height="440" alt="ceramic mugs" title="drinking vessels" /></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Damian Keefe</span>  </div>  </div>
  <div data-slide="2"  class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-banner-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/banner_large/public/banners/Phil%20Clark_Towards%20Newquay.jpg?itok=0xQsUhYL" width="1400" height="440" alt="printmaking" title="Towards Newquay" /></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Phil Clark </span>  </div>  </div>
  <div data-slide="3"  class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-banner-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/banner_large/public/banners/Neil%20Lossock%20_small%20fern%20group%20%282%29.jpg?itok=ktSUU7Gh" width="1400" height="440" alt="Artist blacksmith" title="Small fern group" /></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Neil Lossock </span>  </div>  </div>
  <div data-slide="4"  class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-banner-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/banner_large/public/banners/Eve%20and%20the%20birds.jpg?itok=L6BgnnPn" width="1400" height="440" alt="mono print" title="Eve and the Birds" /></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Adrienne Craddock </span>  </div>  </div>

<div class="pagination">
  <div class="container">
    <ul>
              <li><a href="#" data-slide="0"></a></li>
              <li><a href="#" data-slide="1"></a></li>
              <li><a href="#" data-slide="2"></a></li>
              <li><a href="#" data-slide="3"></a></li>
              <li><a href="#" data-slide="4"></a></li>
          </ul>
  </div>
</div>
    </div>
  
  
  
  
  
  
</div>  
  <!-- ______________________ MAIN _______________________ -->

  <div id="main">

    <div class="region-padding">
      <div class="page-padding">
        <section id="content">

                      <!-- <div id="content-header"> -->


                            
              
              
              
              
            <!-- </div> /#content-header -->
          
          <div id="content-area">
            
<div class="region region-content">

<div class="container">
  <article class="node node-page node-odd node-full node-page-full" data-nid="126" >

          <header>
                        <span property="dc:title" content="Home" class="rdf-meta element-hidden"></span>
        
              </header>
    
    <div class="content">
      <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><h1>5-13th September 2020</h1>
<p>Herefordshire Art Week is a nine day art trail, for artists, craftmakers and creative businesses to open studios, galleries and group exhibitions to raise their profile and sell artwork with the majority of venues opening each day between 11am and 5pm. But, please check the information on the venues you plan to visit, as some open earlier and close later, giving you more time to visit.  There are also a few who close on an occasional day or a little earlier than 5pm.</p>
</div></div></div>    </div><!-- /content -->

    <div class="paragraphs-content">
          </div>

    
          <div class="links">
              </div><!-- /links -->
    
      </article><!-- /node -->
</div></div>          </div>

          
        </section><!-- /content -->

      </div>
    </div>
  </div><!-- /main -->

      <!-- ___________________ NOTICEBOARD _____________________ -->
    
<div class="region region-notice_board">
  <div class="region-padding">
    <div class="block block-hart-blocks block-odd first last block" data-bid="78">
        <div id="hart_notice_board" class="hart-notice-board">
  <div class="container">
          <h2><span class="pink-line">Artist Notice Board</span></h2>
      
    <div class="notice-board-boxes content">
  
              <div class="notice-board-box">
          <div>
                          <h3 class="box-title">h.Art 2020 will be 5 - 13 September</h3>
              <div class="box-text"><p><strong>h.Art 2020, Saturday 5</strong><sup>th</sup><strong> - Sunday 13th September. The printed guide will be out mid July, but all details will be available on the wesbite by the end of May.</strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<ul>
</ul>

<p>&nbsp;</p>
</div>
                      </div>
        </div>
              <div class="notice-board-box">
          <div>
                          <h3 class="box-title">To take part in h.Art 2020, please complete the online form - OPEN Mon 9 March</h3>
              <div class="box-text"><p><strong>Registration opens online - Mon 9 Mar &amp; closes on Weds 15 April</strong><strong> -&nbsp;</strong><a href="http://www.h-art.org.uk/take-part" style="color: rgb(7, 130, 193); font-family: sans-serif,Arial,Verdana,&amp;quot;trebuchet ms&amp;quot;; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 700; letter-spacing: normal; orphans: 2; text-align: left; text-decoration: underline; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;">http://www.h-art.org.uk/take-part</a> This is an online form which is only available from 9 March. You can start the form and go back in to complete it anytime during the registration period. Once the form is completed, you will be asked for payment online. Come along to the Artist Planning Meeting if you have any queries and want to learn more, see 3rd Notice Board for details.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
</div>
                      </div>
        </div>
              <div class="notice-board-box">
          <div>
                          <h3 class="box-title">h.Art 2020 - Important DATES for Artist & Venues </h3>
              <div class="box-text"><p><strong>If anyone is planning for next year, whether you're new to h.Art or an old hand, it would be great to see you at the Artists Planning Meeting - everyone is welcome, it's informal and you're not committing yourself by turning up, just come along to find out more about what's involved...</strong></p>

<p><span style="color:#000080"><strong>Artists</strong> <strong>Planning Meeting Tues 11 Feb time, 4.45 - 6.45pm, Main Hall, Hereford College of Arts,</strong></span></p>

<p><span style="color:#000080"><strong>COLLEGE ROAD</strong>, </span><strong><span style="color:#000080">HR1 1EB</span> </strong>parking on site, come though the main entrance and go straight on to the Main Hall. Please don't go to the Folly Lane site.</p>

<p>To take part in h.Art you will need to fill in the online form, this will go live on Monday 9 April, there is no acccess to the form before then.</p>

<p><strong><span style="color:#000080">Registration opens online - Mon 9 Mar &amp; closes on Weds 15 April</span> -&nbsp;<a href="http://www.h-art.org.uk/take-part">http://www.h-art.org.uk/take-part</a></strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>
</div>
                      </div>
        </div>
      
    </div>

  </div>
</div>
</div><!-- /block -->
  </div>
</div>  
      <!-- _______________________ PLAN ________________________ -->
    <div class="region region-plan">
  <div class="region-padding">
    <div class="block block-hart-blocks block-odd first last block" data-bid="68">
        <div id="hart_plan_your_week" class="hart-plan-your-week">
  <div class="container">
          <h2><span class="pink-line">Plan your week</span></h2>
      
    <div class="content">

      <div class="plan-box text text-1">
        <div class="box-content">
                      <a href="/explore/venues"><span class="large-text">111</span> Venues across the county</a>
                  </div>
      </div>
      <div class="plan-box image image-1">
        <div class="box-content">
          <img src="https://www.h-art.org.uk/sites/all/themes/hart/images/template/village-church.jpg" alt="" />
        </div>
      </div>
      <div class="plan-box text text-2">
        <div class="box-content">
          <a href="/explore/artforms"><span>h.Art is dedicated to exhibiting a wide variety of artists and media!</span></a>
        </div>
      </div>
      <div class="plan-box image image-2">
        <div class="box-content">
          <img src="https://www.h-art.org.uk/sites/all/themes/hart/images/template/sign.jpg" alt="" />
        </div>
      </div>
      <div class="plan-box text text-3">
        <div class="box-content">
                      <a href="/explore/artists"><span class="large-text">403</span> Artists exhibiting</a>
                  </div>
      </div>
      <div class="plan-box image image-3">
        <div class="box-content">
          <img src="https://www.h-art.org.uk/sites/all/themes/hart/images/template/hidden-gems.jpg" alt="" />
        </div>
      </div>

    </div>

    <a href="/explore/location" class="button">Explore</a>

  </div>
</div>
</div><!-- /block -->
  </div>
</div>  

      <!-- _____________________ FEATURED ______________________ -->
    
<div class="region region-featured">
  <div class="region-padding">
    <div class="container">
  <div class="block block-views block-odd first last block" data-bid="71">
              <h2 class="title"><span class="pink-line">Featured Artists</span></h2>
            <div class="view view-featured-artists view-id-featured_artists view-display-id-block_1 view-dom-id-9d2277d47e8fbbcd45f1929119ead1cd">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/3_of_12/public/rose-3_0.jpg?itok=6KkXoT0u" width="327" height="327" title="Japanese Rose" /></div>  </div>  
  <div class="views-field views-field-nothing">        <span class="field-content"><a href="/explore/artist/lizzie-harper"><span class="merged-field">
<span class="artist-name">Lizzie Harper</span>
<span class="venue-name">Lizzie Harper &amp; The Woodee</span>
</span></a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/3_of_12/public/1pen-y-fan-brian-griffiths_0.jpg?itok=O1dy7YKQ" width="327" height="327" title="Pen-y-Fan" /></div>  </div>  
  <div class="views-field views-field-nothing">        <span class="field-content"><a href="/explore/artist/brian-griffiths"><span class="merged-field">
<span class="artist-name">Brian Griffiths</span>
<span class="venue-name">h.Art @ Llangarron</span>
</span></a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.h-art.org.uk/sites/default/files/styles/3_of_12/public/1-7.jpg?itok=kSloPuXR" width="327" height="327" title="Various sculpture, painting &amp; drawings" /></div>  </div>  
  <div class="views-field views-field-nothing">        <span class="field-content"><a href="/explore/artist/toose-morton"><span class="merged-field">
<span class="artist-name">Toose Morton</span>
<span class="venue-name">Toose Morton</span>
</span></a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div><!-- /block -->
</div>  </div>
</div>  
      <!-- ____________________ INSTRAGRAM _____________________ -->
    
<div class="region region-instagram">
  <div class="region-padding">
    <div class="block block-hart-instagram block-odd first last block" data-bid="84">
        <div class="container page-padding">
  <h2><span class="pink-line">Instagram</span></h2>
  <div class="container images">
        </div>
</div>
</div><!-- /block -->
  </div>
</div>  
  

  <!-- _______________________ FOOTER ________________________ -->

  <footer id="footer">
    <div class="container page-padding">

      <div class="get-in-touch">
        <div class="title">Get in Touch</div>
        <div class="contact-method">T: 07983 495966</div>
        <div class="contact-method">E: info@h-art.org.uk</div>
      </div>

      <ul class="social">
        <li><a href="https://www.facebook.com/HerefordshireArtWeek/" class="icon icon-facebook"></a></li>
        <li><a href="https://twitter.com/hArtWeek" class="icon icon-twitter"></a></li>
        <li><a href="https://www.instagram.com/HerefordshireArtWeek/" class="icon icon-instagram"></a></li>
      </ul>

          </div>
  </footer><!-- /footer -->

  <div class="legal">
    <p>Website by <a href="https://orphans.co.uk/websites" rel="nofollow">Orphans</a></p>
  </div>

</div><!-- /page -->
  <div class="region region-page_bottom">
  </div></body>
</html>
