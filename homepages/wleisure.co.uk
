<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="en" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="en" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="en" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->

<head>
  <meta charset="utf-8" />
<meta name="description" content="W Leisure is a sales and marketing communication company specialising in the travel, tourism and holiday sector." />
<meta name="keywords" content="marketing, communications, travel, holiday, tourism, UK, sales, overseas, pr" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.wleisure.co.uk/" />
<link rel="shortlink" href="https://www.wleisure.co.uk/" />
<meta property="og:site_name" content="wleisure" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.wleisure.co.uk/" />
<meta property="og:title" content="wleisure" />
  <title>Marketing Communications for Travel and Tourism</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="cleartype" content="on">

  <style>
@import url("https://www.wleisure.co.uk/modules/system/system.base.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/system/system.messages.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/system/system.theme.css?pqketf");
</style>
<style>
@import url("https://www.wleisure.co.uk/sites/all/modules/simplenews/simplenews.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/comment/comment.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/field/theme/field.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/node/node.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/search/search.css?pqketf");
@import url("https://www.wleisure.co.uk/modules/user/user.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/views/css/views.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?pqketf");
</style>
<style>
@import url("https://www.wleisure.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/ctools/css/ctools.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/panels/css/panels.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/nice_menus/css/nice_menus.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/nice_menus/css/nice_menus_default.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/modules/eu_cookie_compliance/css/eu_cookie_compliance.css?pqketf");
</style>
<style>#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#b2a06b;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}
</style>
<style>
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/normalize.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/custom.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/wireframes.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/layouts/fixed-width.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/tabs.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/pages.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/blocks.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/navigation.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/views-styles.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/nodes.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/comments.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/forms.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/fields.css?pqketf");
@import url("https://www.wleisure.co.uk/sites/all/themes/wleisure/css/print.css?pqketf");
</style>
<link rel="stylesheet" href="https://use.typekit.net/cpp8aih.css">
  <script src="https://www.wleisure.co.uk/misc/jquery.js?v=1.4.4"></script>
<script src="https://www.wleisure.co.uk/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script src="https://www.wleisure.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.wleisure.co.uk/misc/drupal.js?pqketf"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/nice_menus/js/jquery.bgiframe.js?v=2.1"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/nice_menus/js/jquery.hoverIntent.js?v=0.5"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/nice_menus/js/superfish.js?v=1.4.8"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/nice_menus/js/nice_menus.js?v=1.0"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/eu_cookie_compliance/js/jquery.cookie-1.4.1.min.js?v=1.4.1"></script>
<script src="https://www.wleisure.co.uk/sites/all/libraries/colorbox/jquery.colorbox-min.js?pqketf"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/colorbox/js/colorbox.js?pqketf"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.js?pqketf"></script>
<script src="https://www.wleisure.co.uk/sites/all/modules/google_analytics/googleanalytics.js?pqketf"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-51925818-1", {"cookieDomain":"auto"});ga("send", "pageview");</script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"wleisure","theme_token":"Azk94g7C4LHz5zpz8GnGJgMhu6U4luAONLGDDgvLtn8","js":{"0":1,"1":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.bgiframe.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.hoverIntent.js":1,"sites\/all\/modules\/nice_menus\/js\/superfish.js":1,"sites\/all\/modules\/nice_menus\/js\/nice_menus.js":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/jquery.cookie-1.4.1.min.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"2":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus_default.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/all\/themes\/wleisure\/system.menus.css":1,"sites\/all\/themes\/wleisure\/css\/normalize.css":1,"sites\/all\/themes\/wleisure\/css\/custom.css":1,"sites\/all\/themes\/wleisure\/css\/wireframes.css":1,"sites\/all\/themes\/wleisure\/css\/layouts\/fixed-width.css":1,"sites\/all\/themes\/wleisure\/css\/page-backgrounds.css":1,"sites\/all\/themes\/wleisure\/css\/tabs.css":1,"sites\/all\/themes\/wleisure\/css\/pages.css":1,"sites\/all\/themes\/wleisure\/css\/blocks.css":1,"sites\/all\/themes\/wleisure\/css\/navigation.css":1,"sites\/all\/themes\/wleisure\/css\/views-styles.css":1,"sites\/all\/themes\/wleisure\/css\/nodes.css":1,"sites\/all\/themes\/wleisure\/css\/comments.css":1,"sites\/all\/themes\/wleisure\/css\/forms.css":1,"sites\/all\/themes\/wleisure\/css\/fields.css":1,"sites\/all\/themes\/wleisure\/css\/print.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"nice_menus_options":{"delay":800,"speed":"slow"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":1,"popup_scrolling_confirmation":0,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWe use cookies on this site to enhance your user experienceBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, I agree\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EThank you for accepting cookiesYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy","popup_link_new_window":1,"popup_position":null,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"default","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EWe use cookies on this site to enhance your user experienceYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1}});</script>
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5-respond.js"></script>
    <![endif]-->
  </head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page" >
      <p id="skip-link">
      <a href="#main-menu" class="element-invisible element-focusable">Jump to navigation</a>
    </p>
      
<div id="page-wrapper"><div id="page">

  <header id="header" role="banner">

          <a href="/" title="Home" rel="home" id="logo"><img src="https://www.wleisure.co.uk/sites/wleisure/files/wleisure-logo.png" alt="Home" /></a>
    
    
    
      <div class="header__region region region-header">
    <div id="block-nice-menus-1" class="block block-nice-menus first last odd">

      
  <ul class="nice-menu nice-menu-down nice-menu-main-menu" id="nice-menu-1"><li class="menu__item menu-218 menu-path-front first odd "><a href="/" title="" class="menu__link active">HOME</a></li>
<li class="menu__item menu-341 menuparent  menu-path-node-2  even "><a href="/services" class="menu__link">SERVICES</a><ul><li class="menu__item menu-412 menu-path-node-12 first odd "><a href="/business-analysis" class="menu__link">Business Analysis Profiling</a></li>
<li class="menu__item menu-413 menu-path-node-13  even "><a href="/marketing-communications" class="menu__link">Marketing Communications</a></li>
<li class="menu__item menu-414 menu-path-node-14  odd "><a href="/business-development" class="menu__link">New Business Development</a></li>
<li class="menu__item menu-415 menu-path-node-15  even last"><a href="/representation-services" class="menu__link">Representation Services</a></li>
</ul></li>
<li class="menu__item menu-342 menu-path-node-3  odd "><a href="/clients" class="menu__link">CLIENTS</a></li>
<li class="menu__item menu-388 menu-path-node-8  even "><a href="/press" class="menu__link">PRESS</a></li>
<li class="menu__item menu-387 menu-path-node-7  odd last"><a href="/contacts" class="menu__link">CONTACTS</a></li>
</ul>

</div>
  </div>

  </header>
  
  	
	

  <div id="main">
  
  <div id="mainimage">
	  <div class="region region-image-holder">
    <div id="block-block-1" class="block block-block first last odd">

      
  <p><img alt="" src="/sites/wleisure/files/resize/images/main-image-holder-980x301.jpg" style="height:301px; width:980px" width="980" height="301" /></p>

</div>
  </div>
	</div><!-- /#mainimage -->

    <div id="content" class="column" role="main">
                  <a id="main-content"></a>
                    <h1 class="title" id="page-title">Welcome to W Leisure</h1>
                                          


<article class="node-1 node node-page view-mode-full clearfix" about="/index" typeof="foaf:Document">

      <header>
                  <span property="dc:title" content="Welcome to W Leisure" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      
          </header>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><table border="0" cellpadding="0" cellspacing="0" style="width:905px">
<tbody>
<tr>
<td style="text-align:left; vertical-align:top; width:435px">
<p>W Leisure is a sales and marketing communication company specialising in the travel, tourism and holiday sector. We specialise in enabling overseas properties or brands to expand their client base within UK and other English speaking countries, as well as assisting UK specialist holiday companies to expand their offering overseas. Our flexible service is tailored to meet the individual needs of our clients.</p>
</td>
<td>&nbsp;</td>
<td style="text-align:left; vertical-align:top; width:435px">
<p>Since incorporation in 2010, we have developed strong relationships with both our clients and the media as well as establishing strong partnerships with specialists in social and digital marketing disciplines. Our skills lie in working with existing teams to help, advise and assist them in expanding their offering into English&nbsp;speaking markets. We can provide either a full marketing service or work with&nbsp;established in-house marketing teams.</p>
</td>
</tr>
</tbody>
</table>
</div></div></div>
  
  
</article>
          </div><!-- /#content -->


	
	

	

    
    
  </div><!-- /#main -->

    <footer id="footer" class="region region-footer">
    <div id="block-block-2" class="block block-block first last odd">

      
  <p><span style="color:#FFFFFF"><span style="font-size:20px"><span style="font-family:arial,helvetica,sans-serif">Tel: +44 (0) 7738 415 894</span></span></span><br />
<span style="font-size:12px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:#FFFFFF">W Leisure, 23 Martins Drive, Ferndown, Dorset. BH22 9SG. UK</span><br />
Email: <a href="mailto:enquiries@wleisure.co.uk">enquiries@wleisure.co.uk</a> | <a href="terms">Terms &amp; Conditions</a> | <a href="privacy">Privacy Statement</a> | <a href="cookies">Cookies</a> | <a href="http://www.wleisure.co.uk/wleisure-data-protection-policy.pdf" target="_blank">Data Protection Policy</a><br />
&nbsp;</span></span></p>

</div>
  </footer>

</div></div><!-- /#page, /#page-wrapper -->

  <script>function euCookieComplianceLoadScripts() {Drupal.attachBehaviors();}</script>
<script>var eu_cookie_compliance_cookie_name = "";</script>
<script src="https://www.wleisure.co.uk/sites/all/modules/eu_cookie_compliance/js/eu_cookie_compliance.js?pqketf"></script>
</body>
</html>
