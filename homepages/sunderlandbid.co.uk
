<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" />
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-60527786-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<meta name="title" content="Home | Sunderland BID" />
<link rel="shortlink" href="https://www.sunderlandbid.co.uk/" />
<link rel="canonical" href="https://www.sunderlandbid.co.uk/" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script>function euCookieComplianceLoadScripts() {}</script>
<style>div#sliding-popup, div#sliding-popup .eu-cookie-withdraw-banner, .eu-cookie-withdraw-tab {background: #0779bf} div#sliding-popup.eu-cookie-withdraw-wrapper { background: transparent; } #sliding-popup h1, #sliding-popup h2, #sliding-popup h3, #sliding-popup p, .eu-cookie-compliance-more-button, .eu-cookie-compliance-secondary-button, .eu-cookie-withdraw-tab { color: #ffffff;} .eu-cookie-withdraw-tab { border-color: #ffffff;}</style>
<link rel="shortcut icon" href="/themes/sunderland_bid/favicon.png" type="image/png" />
<link rel="revision" href="https://www.sunderlandbid.co.uk/home" />

    <title>Home | Sunderland BID</title>
    <link rel="stylesheet" media="all" href="/sites/default/files/css/css_mZ3io7bYzFQVusJU11qq8c6gQwGvI3llH0iIGj7KYIE.css?q1mepa" />
<link rel="stylesheet" media="all" href="/sites/default/files/css/css_GGpwGSQNYpC0sEO3G8O0c5ezkwV6pW5jj3keq1CduoE.css?q1mepa" />

    
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

  </head>
  <body class="path-frontpage page-node-type-page no-sidebars">
        <a href="#main-content" class="visually-hidden focusable skip-link">
      Skip to main content
    </a>
    
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    
<header class="l-header" role="banner">
  <div class="l-search">
  <div class="l-search__overlay">
    <div class="container">
      <div class="row">
          <div class="region region-search col-12">
    <div class="search-block-form settings-tray-editable block block-search container-inline" data-drupal-selector="search-block-form" id="block-searchform" role="search" data-drupal-settingstray="editable">
  
      <h2>Search Sunderland BID</h2>
    
      <form action="/search/node" method="get" id="search-block-form" accept-charset="UTF-8">
  <div class="js-form-item form-item js-form-type-search form-type-search js-form-item-keys form-item-keys form-no-label">
      <label for="edit-keys" class="visually-hidden">Search</label>
        <input title="Enter the terms you wish to search for." data-drupal-selector="edit-keys" type="search" id="edit-keys" name="keys" value="" size="15" maxlength="128" class="form-search" />

        </div>
<div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions"><input data-drupal-selector="edit-submit" type="submit" id="edit-submit" value="Search" class="button js-form-submit form-submit" />
</div>

</form>

  </div>

    <a id="search-close" href="#">Close</a>
  </div>

      </div>
    </div>
  </div>
</div>
  <div class="container">
    <div class="row">
        <div class="region region-branding col-6 col-md-3">
    <div id="block-sitebranding" class="settings-tray-editable block block-system block-system-branding-block" data-drupal-settingstray="editable">
  
    
        <a href="/" title="Home" rel="home" class="site-logo">
      <img src="/themes/sunderland_bid/logo.svg" alt="Home" />
    </a>
      </div>

  </div>

        <div class="region region-navigation col-6 col-md-9">
    <div class="navigation-top">
      <div class="social-media-links">
  <a class="social-icon social-icon--facebook" href="https://www.facebook.com/sunderlandexperience/">Facebook</a>
  <a class="social-icon social-icon--twitter" href="https://twitter.com/SunderlandBID">Twitter</a>
  <a class="social-icon social-icon--linkedin" href="https://www.linkedin.com/company/sunderlandbid/">LinkedIn</a>
  <a class="social-icon social-icon--instagram" href="https://www.instagram.com/sunderland_experience/">Instagram</a>
</div>
      <p class="telephone-number">Tel: 0191 562 3130</p>
    </div>
    <div class="navigation-bottom">
      <a id="search-open" href="#">Search</a>
      <div id="block-horizontalmenu" class="settings-tray-editable block block-responsive-menu block-responsive-menu-horizontal-menu" data-drupal-settingstray="editable">
  
    
      <nav class="responsive-menu-block-wrapper">
  
              <ul id="horizontal-menu" class="horizontal-menu">
                    <li class="menu-item menu-item--collapsed">
        <a href="/about" data-drupal-link-system-path="node/2">About</a>
              </li>
                <li class="menu-item">
        <a href="/events" data-drupal-link-system-path="events">Events</a>
              </li>
                <li class="menu-item">
        <a href="/christmas-sunderland" data-drupal-link-system-path="node/4657">Christmas</a>
              </li>
                <li class="menu-item">
        <a href="/news" data-drupal-link-system-path="news">News</a>
              </li>
                <li class="menu-item">
        <a href="/vibe-magazine" data-drupal-link-system-path="node/4">Vibe Magazine</a>
              </li>
                <li class="menu-item">
        <a href="/contact" data-drupal-link-system-path="node/1835">Contact</a>
              </li>
        </ul>
  


</nav>
  </div>
<div id="block-responsivemenumobileicon" class="responsive-menu-toggle-wrapper responsive-menu-toggle settings-tray-editable block block-responsive-menu block-responsive-menu-toggle" data-drupal-settingstray="editable">
  
    
      <a id="toggle-icon" class="toggle responsive-menu-toggle-icon" title="Menu" href="#off-canvas">
  <span class="icon"></span><span class="label">Menu</span>
</a>
  </div>

    </div>
  </div>

    </div>
  </div>
</header>

<main role="main">
  <a id="main-content" tabindex="-1"></a>
  <div class="l-content">
    
    
      <div class="region region-content">
    <div id="block-messages" class="settings-tray-editable block block-system block-system-messages-block container" data-drupal-settingstray="editable">
  
    
      <div data-drupal-messages-fallback class="hidden"></div>
  </div>
<div id="block-mainpagecontent" class="block block-system block-system-main-block">
  
    
      
<article data-history-node-id="1" role="article" about="/home" typeof="schema:WebPage" class="node node--type-page node--view-mode-full">

  
      <span property="schema:name" content="Home" class="rdf-meta hidden"></span>


  
  <div class="node__content">
    <div class="section-no-margin layout layout--onecol-full">
  <div class="container-fluid">

    
    <div class="row">

              <div  class="layout__region layout__region--content col-12">
          

<div data-layout-content-preview-placeholder-label="&quot;Hero image block&quot; block" class="block block-layout-builder block-inline-blockhero-image-block block-hero-image" style="background-image: url(/sites/default/files/2019-08/Food%20and%20drink%20crowd.png );">
  <div class="container">
    <div class="row">
      
              <h1 class="col-lg-8" >
            <div class="field field--name-field-title field--type-string field--label-hidden field__item">Sunderland&#039;s Business Improvement District</div>
      </h1>
            
    </div>
  </div>
    </div>

        </div>
      
    </div>
  </div>
</div>
  <div class="section-large-margin layout layout--twocol">
    <div class="container">

      
      <div class="row">

      
              <div  class="layout__region layout__region--first col-lg-6">
          <div data-layout-content-preview-placeholder-label="&quot;Image block&quot; block" class="block block- block-inline-blockimage-block block-image">
  
  
      
  </div>
<div data-layout-content-preview-placeholder-label="&quot;Image block&quot; block" class="block block- block-inline-blockimage-block block-image">
  
  
      
  </div>
<div data-layout-content-preview-placeholder-label="&quot;Image block&quot; block" class="block block- block-inline-blockimage-block block-image">
  
  
      
  </div>
<div data-layout-content-preview-placeholder-label="&quot;Image block&quot; block" class="block block-layout-builder block-inline-blockimage-block block-image">
  
  
      
            <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item"><article class="media media--type-image media--view-mode-image-block-component">
  
      
  <div class="field field--name-field-media-image field--type-image field--label-visually_hidden">
    <div class="field__label visually-hidden">Image</div>
              <div class="field__item">  <img src="/sites/default/files/styles/image_block/public/2019-08/C3.jpg?itok=NbIxSH_Q" width="1050" height="680" alt="Wearmouth Bridge" typeof="foaf:Image" class="image-style-image-block" />


</div>
          </div>

  </article>
</div>
      
  </div>

        </div>
      
              <div  class="layout__region layout__region--second col-lg-6">
          <div data-layout-content-preview-placeholder-label="&quot;Text block&quot; block" class="block block-layout-builder block-inline-blockbasic block-basic">
  
      <h2 >
            <div class="field field--name-field-title field--type-string field--label-hidden field__item">Welcome to Sunderland</div>
      </h2>
    
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><p>Welcome to Sunderland, our beautiful city by the sea. Find out what Sunderland BID are doing to create a vibrant city centre and opportunities for everyone.</p></div>
      
    
  </div>

        </div>
      
      
      </div>
    </div>
  </div>
<div class="layout layout--onecol-full">
  <div class="container-fluid">

    
    <div class="row">

              <div  class="layout__region layout__region--content col-12">
          

<div data-layout-content-preview-placeholder-label="&quot;Image feature block&quot; block" class="block block-layout-builder block-inline-blockimage-feature-block block-image-feature" style="background-image: url(/sites/default/files/2019-11/DW_26-11-15_SUNDERLAND%20FROST%20VILLAGE_07701.JPG );">
  <div class="container">
    <div class="row">
            <div class="col-md-9 col-lg-5">
        
                  <h2 >
            <div class="field field--name-field-title field--type-string field--label-hidden field__item">CHRISTMAS IN SUNDERLAND</div>
      </h2>
                
        
            <div class="field field--name-field-date field--type-daterange field--label-hidden field__item"><time datetime="2019-11-21T00:00:00Z" class="datetime">
  <span class="date-day">21</span>
  <span class="date-month">Nov</span>
  <span class="date-year">2019</span>
</time> - <time datetime="2020-01-05T00:00:00Z" class="datetime">
  <span class="date-day">5</span>
  <span class="date-month">Jan</span>
  <span class="date-year">2020</span>
</time></div>
      
        
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><p>Join us this winter and get ready for a jam-packed programme of activity as the festive season launches on Thursday 21 November!</p></div>
      
        
            <div class="field field--name-field-link field--type-link field--label-hidden field__item"><a href="https://www.sunderlandbid.co.uk/christmas-sunderland" class="btn btn--white">Christmas in Sunderland</a></div>
      
      </div>
          </div>
  </div>
</div>

        </div>
      
    </div>
  </div>
</div>
  <div class="section-large-margin layout layout--twocol">
    <div class="container">

      
      <div class="row">

      
              <div  class="layout__region layout__region--first col-lg-6">
          <div data-layout-content-preview-placeholder-label="&quot;Video block&quot; block" class="block block-layout-builder block-inline-blockvideo-block block-video">
  
  
      
            <div class="field field--name-field-video field--type-video-embed-field field--label-hidden field__item"><div class="video-embed-field-provider-vimeo video-embed-field-responsive-video"><iframe width="854" height="480" frameborder="0" allowfullscreen="allowfullscreen" src="https://player.vimeo.com/video/352921360?autoplay=1"></iframe>
</div>
</div>
      
  </div>

        </div>
      
              <div  class="layout__region layout__region--second col-lg-6">
          <div data-layout-content-preview-placeholder-label="&quot;Text block&quot; block" class="block block-layout-builder block-inline-blockbasic block-basic">
  
      <h2 >
            <div class="field field--name-field-title field--type-string field--label-hidden field__item">About the BID</div>
      </h2>
    
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><p>Sunderland Business Improvement District is a business led, working partnership with one clear vision – to create a vibrant city centre and opportunities for everyone. Since 2014, the BID has invested more than £3m into the city centre and with the support of our business owners, stakeholders and key partners, the BID has developed a relevant and achievable plan for the future.</p></div>
      
    
            <div class="field field--name-field-link field--type-link field--label-hidden field__item"><a href="/about" class="btn btn--red">About The BID</a></div>
      
  </div>

        </div>
      
      
      </div>
    </div>
  </div>
<div class="layout layout--onecol-full">
  <div class="container-fluid">

    
    <div class="row">

              <div  class="layout__region layout__region--content col-12">
          <div data-layout-content-preview-placeholder-label="&quot;Featured events block&quot; block" class="block block-layout-builder block-inline-blockfeatured-events-block block-featured-events">
  
  
  <div class="container-fluid">
    <div class="row">
      <div class="featured-events col-12">
                  
      <div class="field field--name-field-event field--type-entity-reference field--label-hidden row field__items">
              <div class="field__item col-12 col-lg-6">


<article data-history-node-id="4687" role="article" about="/events/festival-light-mowbray-park" class="node node--type-event node--view-mode-featured-event-component">

  <div class="node__content">
    <a href="/events/festival-light-mowbray-park">
            <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item"><article class="media media--type-image media--view-mode-featured-event-component">
  
      
  <div class="field field--name-field-media-image field--type-image field--label-visually_hidden">
    <div class="field__label visually-hidden">Image</div>
              <div class="field__item">  <img src="/sites/default/files/styles/featured_event/public/2019-10/LIGHTS.png?itok=KxVO4Dfj" width="1920" height="1904" alt="Festival of Lights" typeof="foaf:Image" class="image-style-featured-event" />


</div>
          </div>

  </article>
</div>
      </a>
    <div class="featured-event-info">
      <span class="node-label">Featured Event</span>
      
        <h2>
          <a href="/events/festival-light-mowbray-park" rel="bookmark"><span class="field field--name-title field--type-string field--label-hidden">FESTIVAL OF LIGHT Mowbray Park</span>
</a>
        </h2>
      
      
            <div class="field field--name-field-date field--type-daterange field--label-hidden field__item"><time datetime="2019-11-21T00:00:00Z" class="datetime">
  <span class="date-day">21</span>
  <span class="date-month">Nov</span>
  <span class="date-year">2019</span>
</time>  to  <time datetime="2019-12-22T00:00:00Z" class="datetime">
  <span class="date-day">22</span>
  <span class="date-month">Dec</span>
  <span class="date-year">2019</span>
</time></div>
      
      
            Open every week Thursday - Sunday, 21 November – 22 December

New location, Mowbray Park, Sunderland City Centre
      

    </div>
  </div>

</article>
</div>
          </div>
  
              </div>
    </div>
  </div>
</div>

        </div>
      
    </div>
  </div>
</div>
<div class="layout layout--onecol">
  <div class="container">

    
    <div class="row">

              <div  class="layout__region layout__region--content col-12">
          <div data-layout-content-preview-placeholder-label="&quot;News&quot; views block" class="block block-views block-views-blocknews-block-1 block-latest-news">
  
      <h2 class="block__title" >Latest News</h2>
    
      <div class="views-element-container"><div class="view view-news view-id-news view-display-id-block_1 js-view-dom-id-b05be48bd0029da7af11663cbdab01da005752adab1dff293b3e2c34e49c5f49 view-grid">
  
    
      
      <div class="view-content row">
          <div class="col-md-6 col-lg-4 views-row">



<article data-history-node-id="4811" role="article" about="/blog/bid-independents" class="node node--type-blog-post node--view-mode-component">

  <div class="node__content">
    <a href="/blog/bid-independents">
            <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item"><article class="media media--type-image media--view-mode-card-component">
  
      
  <div class="field field--name-field-media-image field--type-image field--label-visually_hidden">
    <div class="field__label visually-hidden">Image</div>
              <div class="field__item">  <img src="/sites/default/files/styles/card/public/2019-11/Donna%20Arkley%2C%20owner%20of%20Bou-chique.jpg?itok=VdobT8b-" width="660" height="440" alt="Bou-Chique" typeof="foaf:Image" class="image-style-card" />


</div>
          </div>

  </article>
</div>
      </a>
    
          <h2>
        <a href="/blog/bid-independents" rel="bookmark"><span class="field field--name-title field--type-string field--label-hidden">A BID FOR INDEPENDENTS  </span>
</a>
      </h2>
        
    <div class="field field--name-created">29 November 2019</div>
    <div class="field field--name-body">
            A campaign has been launched to shine a spotlight on independent shops, restaurants and businesses in Sunderland.
      
</div>
  </div>

</article>
</div>
    <div class="col-md-6 col-lg-4 views-row">



<article data-history-node-id="4809" role="article" about="/blog/worldwide-smash-hit-musical-mamma-mia-will-return-sunderland-empire" class="node node--type-blog-post node--view-mode-component">

  <div class="node__content">
    <a href="/blog/worldwide-smash-hit-musical-mamma-mia-will-return-sunderland-empire">
            <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item"><article class="media media--type-image media--view-mode-card-component">
  
      
  <div class="field field--name-field-media-image field--type-image field--label-visually_hidden">
    <div class="field__label visually-hidden">Image</div>
              <div class="field__item">  <img src="/sites/default/files/styles/card/public/2019-11/Mamma%20Mia.png?itok=vE9xlNUN" width="660" height="440" alt="Mamma Mia " typeof="foaf:Image" class="image-style-card" />


</div>
          </div>

  </article>
</div>
      </a>
    
          <h2>
        <a href="/blog/worldwide-smash-hit-musical-mamma-mia-will-return-sunderland-empire" rel="bookmark"><span class="field field--name-title field--type-string field--label-hidden">THE WORLDWIDE SMASH HIT MUSICAL MAMMA MIA! WILL RETURN TO SUNDERLAND EMPIRE </span>
</a>
      </h2>
        
    <div class="field field--name-created">27 November 2019</div>
    <div class="field field--name-body">
            The sensational feel-good musical MAMMA MIA!
      
</div>
  </div>

</article>
</div>
    <div class="col-md-6 col-lg-4 views-row">



<article data-history-node-id="4806" role="article" about="/blog/veterans-children-receive-advent-treat" class="node node--type-blog-post node--view-mode-component">

  <div class="node__content">
    <a href="/blog/veterans-children-receive-advent-treat">
            <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item"><article class="media media--type-image media--view-mode-card-component">
  
      
  <div class="field field--name-field-media-image field--type-image field--label-visually_hidden">
    <div class="field__label visually-hidden">Image</div>
              <div class="field__item">  <img src="/sites/default/files/styles/card/public/2019-11/L-R%20Phil%20Moir%20%28Director%20of%20RRS%29%2C%20Sam%20Neil%20%28Veterans%20in%20Crisis%29%2C%20Sarah%20Reid%20%28MD%20of%20RRS%29%2C%20Barry%20Wood%20%28Director%20of%20RRS%29%20and%20Kirsty%20Currie%20%28Sunderland%20BI.jpg?itok=JFJbtAAY" width="660" height="440" alt="Veterans in Crisis Advent Calendars" typeof="foaf:Image" class="image-style-card" />


</div>
          </div>

  </article>
</div>
      </a>
    
          <h2>
        <a href="/blog/veterans-children-receive-advent-treat" rel="bookmark"><span class="field field--name-title field--type-string field--label-hidden">VETERANS’ CHILDREN TO RECEIVE AN ADVENT TREAT </span>
</a>
      </h2>
        
    <div class="field field--name-created">25 November 2019</div>
    <div class="field field--name-body">
            The children of military veterans in Sunderland will enjoy an early Christmas treat thanks to one of the city’s leading law firms.
      
</div>
  </div>

</article>
</div>

    </div>
  
            <div class="view-footer">
      <a class="btn btn--red" href="/news">Read more news</a>
    </div>
    </div>
</div>

  </div>

        </div>
      
    </div>
  </div>
</div>

  </div>

</article>

  </div>

  </div>

  </div>
  <div class="l-newsletter">
    <div class="container">
      <div class="row">
          <div class="region region-newsletter col-12">
    <div class="mailchimp-signup-subscribe-form settings-tray-editable block block-mailchimp-signup block-mailchimp-signup-subscribe-blocksunderland-bid-newsletter row" data-drupal-selector="mailchimp-signup-subscribe-block-sunderland-bid-newsletter-form" id="block-mailchimpsubscriptionformsunderlandbidnewsletter" data-drupal-settingstray="editable">
  <div class="block-mailchimp-signup__text col-lg-4">
    
          <h4 class="block__title" >Sunderland BID Newsletter</h4>
        
    <p>Sign up to receive news and event information from Sunderland BID</p>
  </div>
  <div class="col-md-8">
          <form action="/" method="post" id="mailchimp-signup-subscribe-block-sunderland-bid-newsletter-form" accept-charset="UTF-8">
  <div id="mailchimp-newsletter-4acc33c144-mergefields" class="mailchimp-newsletter-mergefields"><div class="js-form-item form-item js-form-type-email form-type-email js-form-item-mergevars-email form-item-mergevars-email">
      <label for="edit-mergevars-email" class="js-form-required form-required">Email Address</label>
        <input data-drupal-selector="edit-mergevars-email" type="email" id="edit-mergevars-email" name="mergevars[EMAIL]" value="" size="25" maxlength="254" class="form-email required" required="required" aria-required="true" />

        </div>
<div class="js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-mergevars-fname form-item-mergevars-fname">
      <label for="edit-mergevars-fname">First Name</label>
        <input data-drupal-selector="edit-mergevars-fname" type="text" id="edit-mergevars-fname" name="mergevars[FNAME]" value="" size="25" maxlength="128" class="form-text" />

        </div>
<div class="js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-mergevars-lname form-item-mergevars-lname">
      <label for="edit-mergevars-lname">Last Name</label>
        <input data-drupal-selector="edit-mergevars-lname" type="text" id="edit-mergevars-lname" name="mergevars[LNAME]" value="" size="25" maxlength="128" class="form-text" />

        </div>
</div><input autocomplete="off" data-drupal-selector="form-3w0a54jqziyq-kv4ffxdxvj7frnugqvsvrazhiz0ruq" type="hidden" name="form_build_id" value="form-3W0A54JqzIyQ-kV4FFXDxvj7FRNUgQVsvrazHiz0ruQ" />
<input data-drupal-selector="edit-mailchimp-signup-subscribe-block-sunderland-bid-newsletter-form" type="hidden" name="form_id" value="mailchimp_signup_subscribe_block_sunderland_bid_newsletter_form" />
<div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions--2"><input data-drupal-selector="edit-submit" type="submit" id="edit-submit--2" name="op" value="Sign up" class="button js-form-submit form-submit" />
</div>

</form>

      </div>
</div>

  </div>

      </div>
    </div>
  </div>

</main>

<footer class="l-footer" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <img src="/themes/sunderland_bid/images/logo-footer.svg" alt="Home" />
      </div>
        <div class="region region-footer-first col-md-3">
    <nav role="navigation" aria-labelledby="block-mainnavigation-menu" id="block-mainnavigation" class="settings-tray-editable block block-menu navigation menu--main" data-drupal-settingstray="editable">
            
  <h2 class="visually-hidden" id="block-mainnavigation-menu">Main navigation</h2>
  

        
              <ul class="menu">
                    <li class="menu-item menu-item--collapsed">
        <a href="/about" data-drupal-link-system-path="node/2">About</a>
              </li>
                <li class="menu-item">
        <a href="/events" data-drupal-link-system-path="events">Events</a>
              </li>
                <li class="menu-item">
        <a href="/christmas-sunderland" data-drupal-link-system-path="node/4657">Christmas</a>
              </li>
                <li class="menu-item">
        <a href="/news" data-drupal-link-system-path="news">News</a>
              </li>
                <li class="menu-item">
        <a href="/vibe-magazine" data-drupal-link-system-path="node/4">Vibe Magazine</a>
              </li>
                <li class="menu-item">
        <a href="/contact" data-drupal-link-system-path="node/1835">Contact</a>
              </li>
        </ul>
  


  </nav>

  </div>

        <div class="region region-footer-second col-md-3">
    <nav role="navigation" aria-labelledby="block-footer-menu" id="block-footer" class="settings-tray-editable block block-menu navigation menu--footer" data-drupal-settingstray="editable">
            
  <h2 class="visually-hidden" id="block-footer-menu">Footer</h2>
  

        
              <ul class="menu">
                    <li class="menu-item">
        <a href="/privacy-policy" data-drupal-link-system-path="node/6">Privacy Policy</a>
              </li>
                <li class="menu-item">
        <a href="/governance" data-drupal-link-system-path="node/5">Governance</a>
              </li>
                <li class="menu-item">
        <a href="/website-usage-terms-and-conditions" data-drupal-link-system-path="node/7">Terms and Conditions</a>
              </li>
        </ul>
  


  </nav>

  </div>

        <div class="region region-footer-third col-md-3">
    <div id="block-contactdetails" class="settings-tray-editable block block-block-content block-block-content140aa24f-2b76-42b3-8e2c-2100206258bb block-basic" data-drupal-settingstray="editable">
  
    
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><p>Tel: 0191 562 3130</p>

<p>info@sunderlandbid.co.uk</p></div>
      
    
  </div>

  </div>

    </div>
  </div>
</footer>

  </div>

    <div class="off-canvas-wrapper"><div id="off-canvas">
              <ul class="menu">
                    <li class="menu-item menu-item--expanded">
        <a href="/about" data-drupal-link-system-path="node/2">About</a>
                                <ul class="menu">
                    <li class="menu-item">
        <a href="/city-pride" data-drupal-link-system-path="node/14">City Pride</a>
              </li>
                <li class="menu-item">
        <a href="/city-promotion" data-drupal-link-system-path="node/15">City Promotion</a>
              </li>
                <li class="menu-item">
        <a href="/city-voice" data-drupal-link-system-path="node/16">City Voice</a>
              </li>
        </ul>
  
              </li>
                <li class="menu-item">
        <a href="/events" data-drupal-link-system-path="events">Events</a>
              </li>
                <li class="menu-item">
        <a href="/christmas-sunderland" data-drupal-link-system-path="node/4657">Christmas</a>
              </li>
                <li class="menu-item">
        <a href="/news" data-drupal-link-system-path="news">News</a>
              </li>
                <li class="menu-item">
        <a href="/vibe-magazine" data-drupal-link-system-path="node/4">Vibe Magazine</a>
              </li>
                <li class="menu-item">
        <a href="/contact" data-drupal-link-system-path="node/1835">Contact</a>
              </li>
        </ul>
  

</div></div>
    <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/1","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","responsive_menu":{"position":"right","theme":"theme-dark","pagedim":"pagedim","breakpoint":"all and (min-width: 992px)","extension_keyboard":true,"superfish":{"active":false,"delay":300,"speed":100,"speedOut":100},"mediaQuery":"all and (min-width: 992px)"},"google_analytics":{"trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"eu_cookie_compliance":{"popup_enabled":true,"popup_agreed_enabled":false,"popup_hide_agreed":false,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info eu-cookie-compliance-content\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022 class=\u0022eu-cookie-compliance-message\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info eu-cookie-compliance-content\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022 class=\u0022eu-cookie-compliance-message\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy tapping any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003ENo, thanks\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","mobile_breakpoint":768,"popup_html_agreed":false,"popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/","popup_link_new_window":true,"popup_position":false,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":false,"cookie_name":"","reload_page":false,"domain":"","popup_eu_only_js":false,"cookie_lifetime":100,"cookie_session":null,"disagree_do_not_show_popup":false,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info eu-cookie-compliance-content\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022 class=\u0022eu-cookie-compliance-message\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","withdraw_enabled":false},"ajaxTrustedUrl":{"form_action_p_pvdeGsVG5zNF_XLGPTvYSKCf43t8qZYSwcfZl2uzM":true,"\/search\/node":true},"user":{"uid":0,"permissionsHash":"922a4f1560765407edd86a35ecf0671e03b2a70f7055e4cb31aaf677ff476953"}}</script>
<script src="/sites/default/files/js/js_-FNss7ao84tcakktHyd-IiL-0z8R3dmsSNQLareMcbk.js"></script>

  </body>
</html>
