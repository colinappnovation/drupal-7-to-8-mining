<!DOCTYPE html>
<html  lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="/themes/custom/tce/favicon.ico" type="image/vnd.microsoft.icon" />

    <title>Home | The Carney Effect</title>
    <link rel="stylesheet" href="/sites/default/files/css/css_5L7E_C9ZzBeixMkLEcWL2Hxx-AsrVSagGPy8Y6jSxHQ.css?pnqr0j" media="all" />
<link rel="stylesheet" href="/sites/default/files/css/css_tIcCvIQw_BKQNMUgAmKE7t2wc4Q2eYZJaFudwuTs_HE.css?pnqr0j" media="all" />
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Ramabhadra|Roboto" media="all" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all" />
<link rel="stylesheet" href="/sites/default/files/css/css_eBSbTWSkKV_cKUWd0JLUzuccM1yAsLulKGeVJNoRIYU.css?pnqr0j" media="all" />

    
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

  </head>
  <body class="path-frontpage has-glyphicons">
    <a href="#main-content" class="visually-hidden focusable skip-link">
      Skip to main content
    </a>
    
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    <div class="container-fluid header upper">
  <div class="container">
                <a href="/" title="Home" rel="home" class="site-branding__logo">
      <img src="/themes/custom/tce/logo.png" alt="Home" />
    </a>
    <div class="site-branding__name">
      <a href="/" title="Home" rel="home">The Carney Effect</a>
    </div>
  </div>
</div>
<div class="container-fluid header lower">
  <div class="container">
    <div class="site-branding__slogan">ramblings of a wannabe Drupal developerer</div>
  </div>
</div>
    
  <div role="main" class="main-container container js-quickedit-main-content">
    <div class="row">

            
                  <section class="col-sm-12">

                                      <div class="highlighted">  <div class="region region-highlighted">
    

  </div>
</div>
                  
                
                
                
                          <a id="main-content"></a>
            <div class="region region-content">
    <div class="views-element-container form-group"><div class="view view-frontpage view-id-frontpage view-display-id-page_1 js-view-dom-id-a9703a7992f961658ed4a0eefa7f569e3e4df41c6c702465f2b4296224d02bae">
  
    
      
      <div class="view-content">
          <div class="views-row"><article role="article" about="/getting-started-drupal-8" class="post is-promoted is-sticky teaser clearfix">

  
      <h2>
      <a href="/getting-started-drupal-8" rel="bookmark"><span>Getting started with Drupal 8</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/getting-started-drupal-8" rel="tag" title="Getting started with Drupal 8" hreflang="en">Read more<span class="visually-hidden"> about Getting started with Drupal 8</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item">
  <p>While taking a look at Drupal 8 for the first time, I thought it would be a good idea to jot down a few things as I learn them&nbsp;so I can refresh my memory when needed:</p>

<p><img alt="drupal8.gif" class="pull-right" data-entity-type="" data-entity-uuid="" height="107" src="/sites/default/files/assets/images/drupal8.gif" style="width: 350px;height: 234px;right: 200px;position: relative;top: 50px;" width="150" /></p>

<ul>
	<li><a href="/whats-new-drupal-8">What's new in Drupal 8</a></li>
	<li><a href="/installing-drupal-8-local-environment">Installing Drupal 8 on a Local Environment</a></li>
	<li><a href="/adding-modules">Adding Modules</a></li>
	<li><a href="/configuration-management">Configuration Management</a></li>
	<li><a href="/creating-installation-profile">Creating an Installation Profile</a>
	<ul>
	</ul>
	</li>
	<li><a href="/module-development">Module Development</a>
	<ul>
		<li><a href="/drupal-8-development-tools">Development Tools</a></li>
		<li><a href="/thin-controllers">Thin Controllers</a></li>
		<li><a href="/services-and-dependency-injection">Services and&nbsp;Dependency Injection</a></li>
		<li><a href="/creating-service">Creating a service</a></li>
		<li><a href="/drupal-8-ajax">Ajax and Commands</a></li>
		<li><a href="/wildcard-loader-arguments-menu-upcasting">Wildcard Loader Arguments </a></li>
		<li><a href="/alter-existing-routes">Alter existing Routes</a></li>
		<li><a href="/custom-access-checking-routes-custom-access-callback">Custom Access Checking on Routes</a></li>
		<li><a href="/quick-module-tips">Quick Module Tips</a></li>
	</ul>
	</li>
	<li><a href="/theme-development">Theme Development</a>
	<ul>
		<li><a href="/bootstrap-theme-and-sass">Bootstrap Theme and SASS</a></li>
		<li><a href="/sass-compiler-phpstorm-using-file-watcher">SASS compiler in PhpStorm using File Watcher</a></li>
		<li><a href="/render-array-types">Render Array Types</a></li>
		<li><a href="/quick-theming-tips">Quick Theming Tips</a></li>
	</ul>
	</li>
	<li><a href="/drupal-8-gotchas">Gotchas</a></li>
	<!-- <li>Production Site</li> -->
	<li><a href="/snippets">Snippets</a></li>
</ul>
    </div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/drupal-friendly-varexport-drupal-8" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/drupal-friendly-varexport-drupal-8" rel="bookmark"><span>Drupal-friendly var_export() in Drupal 8</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/drupal-friendly-varexport-drupal-8" rel="tag" title="Drupal-friendly var_export() in Drupal 8" hreflang="en">Read more<span class="visually-hidden"> about Drupal-friendly var_export() in Drupal 8</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p>When developing in Drupal 8, I used the&nbsp;<a href="https://api.drupal.org/api/drupal/includes%21utility.inc/function/drupal_var_export/7.x">drupal_var_export()</a> function a lot to print arrays to the screen so I could then copy and paste into code to create the same array. This saved me a lot of time. In Drupal 8 the drupal_var_export function no longer exists. Instead do the following:</p>

<pre>
<code class="language-php">dsm(\Drupal\Component\Utility\Variable::export($array_to_output));</code></pre>

<p>Obviously you need the Devel module enabled.</p>
</div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/bye-bye-ctools-object-cache-hello-private-tempstore" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/bye-bye-ctools-object-cache-hello-private-tempstore" rel="bookmark"><span>Bye bye CTools object cache, hello Private TempStore</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/bye-bye-ctools-object-cache-hello-private-tempstore" rel="tag" title="Bye bye CTools object cache, hello Private TempStore" hreflang="en">Read more<span class="visually-hidden"> about Bye bye CTools object cache, hello Private TempStore</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/alter-existing-routes" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/alter-existing-routes" rel="bookmark"><span>Alter existing routes</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/alter-existing-routes" rel="tag" title="Alter existing routes" hreflang="en">Read more<span class="visually-hidden"> about Alter existing routes</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/custom-access-checking-routes-custom-access-callback" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/custom-access-checking-routes-custom-access-callback" rel="bookmark"><span>Custom Access Checking on Routes (Custom Access Callback)</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/custom-access-checking-routes-custom-access-callback" rel="tag" title="Custom Access Checking on Routes (Custom Access Callback)" hreflang="en">Read more<span class="visually-hidden"> about Custom Access Checking on Routes (Custom Access Callback)</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/wildcard-loader-arguments-menu-upcasting" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/wildcard-loader-arguments-menu-upcasting" rel="bookmark"><span>Wildcard Loader Arguments (Menu upcasting)</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/wildcard-loader-arguments-menu-upcasting" rel="tag" title="Wildcard Loader Arguments (Menu upcasting)" hreflang="en">Read more<span class="visually-hidden"> about Wildcard Loader Arguments (Menu upcasting)</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/render-array-types" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/render-array-types" rel="bookmark"><span>Render Array Types</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/render-array-types" rel="tag" title="Render Array Types" hreflang="en">Read more<span class="visually-hidden"> about Render Array Types</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/drupal-8-ajax" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/drupal-8-ajax" rel="bookmark"><span>Drupal 8 AJAX</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/drupal-8-ajax" rel="tag" title="Drupal 8 AJAX" hreflang="en">Read more<span class="visually-hidden"> about Drupal 8 AJAX</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/add-extra-large-breakpoint-drupal-boostrap-theme-sass-starterkit" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/add-extra-large-breakpoint-drupal-boostrap-theme-sass-starterkit" rel="bookmark"><span>Add extra large breakpoint in Drupal Boostrap Theme (SASS starterkit)</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/add-extra-large-breakpoint-drupal-boostrap-theme-sass-starterkit" rel="tag" title="Add extra large breakpoint in Drupal Boostrap Theme (SASS starterkit)" hreflang="en">Read more<span class="visually-hidden"> about Add extra large breakpoint in Drupal Boostrap Theme (SASS starterkit)</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>
    <div class="views-row"><article role="article" about="/sass-compiler-phpstorm-using-file-watcher" class="post is-promoted teaser clearfix">

  
      <h2>
      <a href="/sass-compiler-phpstorm-using-file-watcher" rel="bookmark"><span>SASS compiler in PhpStorm using File Watcher</span>
</a>
    </h2>
    

  
  <div class="content">
    <ul class="links inline list-inline"><li class="node-readmore"><a href="/sass-compiler-phpstorm-using-file-watcher" rel="tag" title="SASS compiler in PhpStorm using File Watcher" hreflang="en">Read more<span class="visually-hidden"> about SASS compiler in PhpStorm using File Watcher</span></a></li></ul>
            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"></div>
      
  </div>

</article>
</div>

    </div>
  
        <nav class="pager-nav text-center" role="navigation" aria-labelledby="pagination-heading">
    <h4 id="pagination-heading" class="visually-hidden">Pagination</h4>
    <ul class="pagination js-pager__items">

            
            
                    <li class="pager__item is-active active">
                                          <a href="?page=0" title="Current page">
            <span class="visually-hidden">
              Current page
            </span>1</a>
        </li>
              <li class="pager__item">
                                          <a href="?page=1" title="Go to page 2">
            <span class="visually-hidden">
              Page
            </span>2</a>
        </li>
              <li class="pager__item">
                                          <a href="?page=2" title="Go to page 3">
            <span class="visually-hidden">
              Page
            </span>3</a>
        </li>
      
                    <li class="pager__item pager__item--next">
          <a href="?page=1" title="Go to next page" rel="next">
            <span class="visually-hidden">Next page</span>
            <span aria-hidden="true">Next ›</span>
          </a>
        </li>
      
                  <li class="pager__item pager__item--last">
        <a href="?page=2" title="Go to last page" rel="last">
          <span class="visually-hidden">Last page</span>
          <span aria-hidden="true">Last »</span>
        </a>
      </li>
      
    </ul>
  </nav>

          </div>
</div>

  </div>

              </section>

                </div>
  </div>

      <footer class="footer container" role="contentinfo">
      <div class="copyright">&copy; 2019 The Carney Effect</div><div class="powered">Powered by <em class="placeholder"><a href="http://www.web-assistant.co.uk/">Web Assistant</a></em></div>
    </footer>
  
  </div>

    
    <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","bootstrap":{"forms_has_error_value_toggle":1,"modal_animation":1,"modal_backdrop":"true","modal_focus_input":1,"modal_keyboard":1,"modal_select_text":1,"modal_show":1,"modal_size":"","popover_enabled":1,"popover_animation":1,"popover_auto_close":1,"popover_container":"body","popover_content":"","popover_delay":"0","popover_html":0,"popover_placement":"right","popover_selector":"","popover_title":"","popover_trigger":"click","popover_trigger_autoclose":1,"tooltip_enabled":1,"tooltip_animation":1,"tooltip_container":"body","tooltip_delay":"0","tooltip_html":0,"tooltip_placement":"auto left","tooltip_selector":"","tooltip_trigger":"hover"},"user":{"uid":0,"permissionsHash":"cbb822d052930633ba9fe993c5ea334928575c70e6c307a83bff0b0130ab3c79"}}</script>
<script src="/sites/default/files/js/js_Hz3kqbpLMNequAjQ_Zf8E3Krn36EwjY1w69xODsf5z4.js"></script>

  </body>
</html>
