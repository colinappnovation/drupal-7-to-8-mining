<!DOCTYPE html>
<!--  Last Published: Mon Jul 08 2019 09:34:21 GMT+0000 (UTC)  -->
<html data-wf-page="5d076e486a9360818898ae09" data-wf-site="5d076e486a93605f0898ae08">
<head>
  <meta charset="utf-8">
  <title>Nikita Mazepin :: 2019 Formula 2 Driver</title>
  <meta content="In 2019 Nikita Mazepin is racing in the FIA Formula 2 Championship with ART Grand Prix." name="description">
  <meta content="Nikita Mazepin :: 2019 Formula 2 Driver" property="og:title">
  <meta content="In 2019 Nikita Mazepin is racing in the FIA Formula 2 Championship with ART Grand Prix." property="og:description">
  <meta content="https://uploads-ssl.webflow.com/5d076e486a93605f0898ae08/5d08b3e9277f0945d4591ad1_Nikita-About.jpg" property="og:image">
  <meta content="summary" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/components.css" rel="stylesheet" type="text/css">
  <link href="css/nikita-mazepin.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/Favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div data-collapse="none" data-animation="default" data-duration="400" class="navbar w-nav"><a href="#" class="nikita-mazepin w-clearfix w-nav-brand"><img src="images/NMSmallFull.svg" height="22" alt="" class="brand-nm"><h2 class="hero-heading">NIKITA MAZEPIN</h2></a>
    <nav role="navigation" class="nav-menu w-nav-menu"><a href="https://www.facebook.com/nmazepin" target="_blank" class="social-link w-inline-block"><img src="images/facebook-f.svg" height="15" alt=""></a><a href="https://twitter.com/NMazepin" target="_blank" class="social-link w-inline-block"><img src="images/twitter.svg" height="15" alt=""></a><a href="https://www.instagram.com/nikita_mazepin/" target="_blank" class="social-link w-inline-block"><img src="images/instagram.svg" height="17" alt=""></a></nav>
    <div class="w-nav-button">
      <div class="w-icon-nav-menu"></div>
    </div>
  </div>
  <section class="hero-section">
    <div class="main-inside red"><img src="images/NMLogoLeft.svg" width="300" alt=""></div>
    <aside class="main-inside"><img src="images/Untitled-1niktared.svg" width="300" data-w-id="177db88e-abdd-6a78-6dab-ea656781ec6f" style="-webkit-transform:translate3d(-150%, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-150%, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-150%, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-150%, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" alt=""></aside>
  </section>
  <section class="section">
    <main class="section-container"><img src="images/NMSmallFull.svg" width="50" alt="">
      <h1>NIKITA<br>MAZEPIN</h1>
      <p class="large">Since first taking to the wheel aged 7, Nikita Mazepin has risen through the ranks of motorsport to become Russia’s most exciting young racing prospect.</p>
      <p>Learning his craft in grassroots racing, under the guidance of former World Karting Champion, Oliver Oakes, Nikita claimed an impressive catalogue of victories and podiums across international karting including 2014 CIK-FIA World Karting Vice-Champion.<br><br>Making his single-seater debut in the MRF Challenge, his acute ability to adapt saw him take to the podium in only his second race. The young Russian continued to show his adaptability in 2015 earning his first Formula Renault podium at the Red Bull Ring. These efforts did not go unnoticed, as he earned himself a place on Force India Development programme in 2016. Returning to the grid in 2017 with Hitech GP he ran a consistent point-scoring campaign, earning himself 3 podiums along the way.<br><br>2018 was the coming-of-age for the young Russian star, teaming up with ART GP for GP3 Series. Under the eye of the Formula 1 paddock, Nikita made a glittering start, with victory in the opening race in Barcelona. His season went from from strength to strength, making an appearance on the podium in 8 out of the 9 events to finish the year vice-champion.<br><br>In 2019 Nikita is racing in the <a href="http://www.fiaformula2.com/" target="_blank" class="link">Formula 2 Championship</a> with ART GP.</p>
    </main>
    <aside class="image-container"></aside>
  </section>
  <section class="section normal includingfooter">
    <aside class="image-container contact"></aside>
    <div class="section-container center">
      <address><img src="images/NMSmallFull.svg" width="50" alt="">
        <h1>CONTACT<br></h1>
        <div class="contact-detail-wrapper">
          <h4>NIKITA MAZEPIN</h4>
          <p><a href="mailto:nm@nikitamazepin.com" class="link">nm@nikitamazepin.com</a></p>
        </div>
        <h4>ENQUIRIES</h4>
        <p><a href="mailto:office@nikitamazepin.com" class="link">office@nikitamazepin.com</a></p>
      </address>
    </div>
    <a href="https://wearegrip.com/" target="_blank" class="footer-link w-inline-block">
      <h6 class="wearegrip">SITE BUILT WITH ♥ BY</h6><img src="images/WAGblack.svg" height="11" alt="" class="wearegrip-logo"></a>
    <div class="footer-link ontheright">
      <h6 class="wearegrip">Copyright © 2019 NIKITA MAZEPIN</h6>
    </div>
  </section>
  <script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="js/nikita-mazepin.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>