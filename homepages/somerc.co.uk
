<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="Somerset Environmental Records Centre RSS" href="https://www.somerc.co.uk/?q=rss.xml" />
<link rel="shortcut icon" href="https://www.somerc.co.uk/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>Somerset Environmental Records Centre | Better informed is better prepared</title>
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css__LeQxW73LSYscb1O__H6f-j_jdAzhZBaesGL19KEB6U.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_dWBHPbSQWh-19e3Bc29cTHIsefj--RoEVftmVdOAMn4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_WEeF7Pa9esxs-an407Q56un314vA-YiqGGCCeMvUZnU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_rP5iJ-BWw4FVpOe_fVdjUyvLTPm1N_h8l6xOGKgf73Y.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_9jBgLmzPSPKX8crbiiglrk0dUBlAxZScyhDZ7SSZKYE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_rOczKc55hIKR6_IbLJwrHLekWB_xmd14GXmUuHu3BJs.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_JA9oGomeVPC1Y--jGCQF5WdsPqzrcN9dDzAUAxIj_ko.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_AbpHGcgLb-kRsJGnwFEktk7uzpZOCcBY74-YBdrKVGs.css" media="screen" />
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans" media="all" />

<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.somerc.co.uk/sites/default/files/css/css_1Dw9oa79xYXSWvFP-bOVoUEU0kgoiM4mftJJdZz-gAI.css" media="screen" />
<![endif]-->
<script type="text/javascript" src="https://www.somerc.co.uk/sites/default/files/js/js_v7z_ueNi9EIrZXPI8So7AkuzyXuKekr2jbdDptsM5Oc.js"></script>
<script type="text/javascript" src="https://www.somerc.co.uk/sites/default/files/js/js_Oq987EJUa2pHuqxKT9CMx86MuTeUig_ee_a4gS2or1M.js"></script>
<script type="text/javascript" src="https://www.somerc.co.uk/sites/default/files/js/js_cAmVn4EwTDrCFDY20mPcAIIeloxOoXCsCWaEYkaU4-4.js"></script>
<script type="text/javascript" src="https://www.somerc.co.uk/sites/default/files/js/js_AEpCt5Wf_vTOhuTXFqATeOyZTgsVuxiElzVfppUou1w.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"indiciatheme","theme_token":"8NmdKDEm0VdyRFKJoRN0YcoLoD_JCOKlWjJ7X3Cf3i0","jquery_version":"1.7","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/form.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/modal_forms\/js\/modal_forms_login.js":1,"misc\/progress.js":1,"sites\/all\/modules\/ctools\/js\/modal.js":1,"sites\/all\/modules\/modal_forms\/js\/modal_forms_popup.js":1,"misc\/collapse.js":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/ctools\/css\/modal.css":1,"sites\/all\/modules\/modal_forms\/css\/modal_forms_popup.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/indiciatheme\/color\/colors.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.settings.style.css":1,"sites\/all\/themes\/indiciatheme\/css\/indiciatheme.css":1,"public:\/\/adaptivetheme\/indiciatheme_files\/indiciatheme.responsive.layout.css":1,"public:\/\/adaptivetheme\/indiciatheme_files\/indiciatheme.fonts.css":1,"public:\/\/adaptivetheme\/indiciatheme_files\/indiciatheme.responsive.styles.css":1,"public:\/\/adaptivetheme\/indiciatheme_files\/indiciatheme.custom.css":1,"\/\/fonts.googleapis.com\/css?family=Open+Sans":1,"public:\/\/adaptivetheme\/indiciatheme_files\/indiciatheme.lt-ie9.layout.css":1}},"CToolsModal":{"loadingText":"Loading...","closeText":"Close Window","closeImage":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022image-style-none\u0022 src=\u0022https:\/\/www.somerc.co.uk\/sites\/all\/modules\/ctools\/images\/icon-close-window.png\u0022 alt=\u0022Close window\u0022 title=\u0022Close window\u0022 \/\u003E","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022image-style-none\u0022 src=\u0022https:\/\/www.somerc.co.uk\/sites\/all\/modules\/ctools\/images\/throbber.gif\u0022 alt=\u0022Loading\u0022 title=\u0022Loading...\u0022 \/\u003E"},"modal-popup-small":{"modalSize":{"type":"fixed","width":300,"height":300},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022image-style-none\u0022 src=\u0022https:\/\/www.somerc.co.uk\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"modal-popup-medium":{"modalSize":{"type":"fixed","width":550,"height":450},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022image-style-none\u0022 src=\u0022https:\/\/www.somerc.co.uk\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"modal-popup-large":{"modalSize":{"type":"scale","width":0.8,"height":0.8},"modalOptions":{"opacity":0.85,"background":"#000"},"animation":"fadeIn","modalTheme":"ModalFormsPopup","throbber":"\u003Cimg typeof=\u0022foaf:Image\u0022 class=\u0022image-style-none\u0022 src=\u0022https:\/\/www.somerc.co.uk\/sites\/all\/modules\/modal_forms\/images\/loading_animation.gif\u0022 alt=\u0022Loading...\u0022 title=\u0022Loading\u0022 \/\u003E","closeText":"Close"},"urlIsAjaxTrusted":{"\/?q=search\/node":true},"superfish":[{"id":"2","sf":{"animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":true,"dropShadows":true,"disableHI":false},"plugins":{"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"User menu"},"supposition":true,"bgiframe":false,"supersubs":{"minWidth":"12","maxWidth":"27","extraWidth":1}}},{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"\u0027fast\u0027","autoArrows":true,"dropShadows":true,"disableHI":false},"plugins":{"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"Main menu"},"supposition":true,"bgiframe":false,"supersubs":{"minWidth":"12","maxWidth":"27","extraWidth":1}}}],"adaptivetheme":{"indiciatheme":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});
//--><!]]>
</script>
<!--[if lt IE 9]>
<script src="https://www.somerc.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?pu6gwh"></script>
<![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node atr-7.x-3.x atv-7.x-3.0-rc1 site-name-somerset-environmental-records-centre indiciatheme bs-n bb-n mb-dd mbp-l rc-0">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="texture-overlay">
  <div id="page" class="container page snc-n snw-n sna-l sns-n ssc-n ssw-n ssa-l sss-n btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-n">

    <header  id="header" class="clearfix" role="banner">
      <div class="header-inner clearfix">

                  <!-- start: Branding -->
          <div  id="branding" class="branding-elements clearfix">

                          <div id="logo">
                <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="https://www.somerc.co.uk/sites/default/files/SERCSiteBanner2_1.png" alt="Somerset Environmental Records Centre" /></a>              </div>
            
                          <!-- start: Site name and Slogan hgroup -->
              <div  class="h-group" id="name-and-slogan">

                                  <h1 id="site-name"><a href="/" title="Home page" class="active">Somerset Environmental Records Centre</a></h1>
                
                
              </div><!-- /end #name-and-slogan -->
            
          </div><!-- /end #branding -->
        
        <div class="region region-header"><div class="region-inner clearfix"><nav id="block-superfish-2" class="block block-superfish no-title odd first last block-count-1 block-region-header block-2" ><div class="block-inner clearfix">  
  
  <ul id="superfish-2" class="menu sf-menu sf-user-menu sf-horizontal sf-style-none sf-total-items-1 sf-parent-items-0 sf-single-items-1"><li id="menu-385-2" class="firstandlast odd sf-item-1 sf-depth-1 sf-no-children"><a href="/?q=user/login" class="sf-depth-1">Login</a></li></ul>
  </div></nav></div></div>
      </div>

    </header> <!-- /header -->

    <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish no-title menu-wrapper menu-bar-wrapper clearfix odd first last block-count-2 block-region-menu-bar block-1" >  
  
  <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-1 sf-parent-items-1 sf-single-items-0"><li id="menu-401-1" class="firstandlast odd sf-item-1 sf-depth-1 sf-total-children-8 sf-parent-children-0 sf-single-children-8 menuparent"><a title="Record your wildlife sightings" class="sf-depth-1 menuparent nolink">Wildlife recording</a><ul><li id="menu-402-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/?q=record/introduction" title="An introduction to recording your wildlife sightings" class="sf-depth-2">Introduction to recording</a></li><li id="menu-1537-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/?q=content/submit-sighting" class="sf-depth-2">Submit a sighting</a></li><li id="menu-1257-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://www.somerc.co.uk/maps/dbl/doublemap.html" title="" class="sf-depth-2">Grab a Grid</a></li><li id="menu-1535-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/?q=content/identification-guides" title="Links to various species identification guides for UK species." class="sf-depth-2">Identification Guides</a></li><li id="menu-419-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/?q=record/sightings-list" class="sf-depth-2">Submit a list of sightings</a></li><li id="menu-423-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="/?q=explore/my-records" class="sf-depth-2">My records</a></li><li id="menu-387-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="/?q=groups/list" title="A list of the local recording groups known to us." class="sf-depth-2">Local recording groups</a></li><li id="menu-388-1" class="last even sf-item-8 sf-depth-2 sf-no-children"><a href="/?q=groups/my-groups" title="Lists the local recording groups you are a member of." class="sf-depth-2">My local recording groups</a></li></ul></li></ul>
  </nav></div>
    <!-- Messages and Help -->
        
    <!-- Breadcrumbs -->
    
    
    <!-- Three column 3x33 Gpanel -->
    
    <div id="columns">
      <div class="columns-inner clearfix">

        <div id="content-column">
          <div class="content-inner">

            
            <div id="main-content" role="main">

                                          
                              <div id="content">
                  <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-34" class="node node-page node-promoted node-teaser article odd iat-n clearfix" about="/?q=node/34" typeof="foaf:Document" role="article">
  
      <header class="node-header">
              <h1 class="node-title">
          <a href="/?q=node/34" rel="bookmark">Welcome</a>
        </h1>
          </header>
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even" property="content:encoded"><p><strong>Somerset Environmental Records Centre</strong> (<strong>SERC</strong>) is the centre of reference for information relating to biodiversity and geology in the county. The data we hold on species, habitats and sites across Somerset is provided by recorders, professional ecologists and the public. We make this information available to a wide-range of individuals and organisations for use in nature conservation, development planning, and research.<br />
 <br />
No doubt you have come here to enter a species sighting from your local patch or even your garden. If that is the case then click <a href="http://drupal.somerc.co.uk/?q=record/sighting#overlay-context=record/sighting%3Fq%3Drecord/sighting">here</a> for a form to enter all the bits and pieces that make that record of your sighting; who, what, where &amp; when.<br />
 <br /><strong>RoAM</strong> users will need to log in. When logged in you will see extra menus for your recording.<br />
 <br /><strong>Our Area</strong><br />
We cover the current administrative County of Somerset, which doesn't include the unitary authorities of North Somerset or Bath and North East Somerset. For recording purposes our area lies within both vice-county 5 South Somerset and vice-county 6 North Somerset.</p>
</div></div></div>  </div>

      <nav class="clearfix"><ul class="links inline"><li class="node-readmore first last"><a href="/?q=node/34" rel="tag" title="Welcome">Read more<span class="element-invisible"> about Welcome</span></a></li></ul></nav>
  
  
  <span property="dc:title" content="Welcome" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span></article>

  </div>                </div>
              
              <!-- Feed icons (RSS, Atom icons etc -->
              <a href="/?q=rss.xml" class="feed-icon" title="Subscribe to Somerset Environmental Records Centre RSS"><img typeof="foaf:Image" class="image-style-none" src="https://www.somerc.co.uk/misc/feed.png" width="16" height="16" alt="Subscribe to Somerset Environmental Records Centre RSS" /></a>
            </div> <!-- /main-content -->

            
          </div>
        </div> <!-- /content-column -->

                
      </div>
    </div> <!-- /columns -->

    
    <!-- four-4x25 Gpanel -->
    
    
  </div> <!-- /page -->
</div> <!-- /texture overlay -->
  </body>
</html>
