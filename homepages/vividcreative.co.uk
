

<!DOCTYPE html>
<!--[if IE 9]>     <html class="ie-9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en">             <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
        <meta name="google-site-verification" content="N2hONe1f2pTeNsA8YMwjtWzUz9jSh8ATYdU4o4x_dkc" />
		
		<link rel="shortcut icon" href="https://www.vividcreative.com/wp-content/themes/vivid/images/favicons/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://www.vividcreative.com/wp-content/themes/vivid/images/favicons/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="https://www.vividcreative.com/wp-content/themes/vivid/images/favicons/apple-touch-icon-152x152.png" />
        <link href="/wp-content/themes/vivid/style.css" rel="stylesheet">
				<link href="/wp-content/themes/vivid/update.css" rel="stylesheet">

        		
<!-- This site is optimized with the Yoast SEO plugin v11.8 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Creative Marketing Agency in Sheffield | Vivid Creative</title>
<meta name="description" content="We are a leading agency delivering brand strategy, creative design, digital marketing, SEO and web design in Sheffield for UK and global brands"/>
<link rel="canonical" href="https://www.vividcreative.com/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Creative Marketing Agency in Sheffield | Vivid Creative" />
<meta property="og:description" content="We are a leading agency delivering brand strategy, creative design, digital marketing, SEO and web design in Sheffield for UK and global brands" />
<meta property="og:url" content="https://www.vividcreative.com/" />
<meta property="og:site_name" content="Vivid Creative" />
<meta property="og:image" content="http://www.vividcreative.com/wp-content/uploads/2015/07/vivid.jpg" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.vividcreative.com/#website","url":"https://www.vividcreative.com/","name":"Vivid Creative","potentialAction":{"@type":"SearchAction","target":"https://www.vividcreative.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://www.vividcreative.com/#webpage","url":"https://www.vividcreative.com/","inLanguage":"en-US","name":"Creative Marketing Agency in Sheffield | Vivid Creative","isPartOf":{"@id":"https://www.vividcreative.com/#website"},"datePublished":"2015-03-04T15:14:52+00:00","dateModified":"2019-10-30T12:23:03+00:00","description":"We are a leading agency delivering brand strategy, creative design, digital marketing, SEO and web design in Sheffield for UK and global brands"}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Vivid Creative &raquo; Vivid Creative &#8211; Brand, Design &#038; Digital Agency Comments Feed" href="https://www.vividcreative.com/vivid-creative-brand-design-digital-agency/feed/" />
<link rel='https://api.w.org/' href='https://www.vividcreative.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.vividcreative.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.vividcreative.com/wp-includes/wlwmanifest.xml" /> 
<link rel='shortlink' href='https://www.vividcreative.com/' />
<link rel="alternate" type="application/json+oembed" href="https://www.vividcreative.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.vividcreative.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.vividcreative.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.vividcreative.com%2F&#038;format=xml" />
<script defer>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-2590927-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
	</head>
	<body class="home page-template page-template-page-homepage page-template-page-homepage-php page page-id-68">
		
		<header id="bn__header" class="">
			<!-- Logo -->
			<a href="/" class="bn__header--logo" aria-label="Vivid Creative Home Page">
				<svg class="bn__header--logo__desktop" viewBox="0 0 129 50" height="50" width="129"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#vivid-logo"></use></svg>
			</a>

			<!-- Navigation -->
			<nav id="bn__nav">
				<div class="menu-primary-menu-container"><ul id="menu-primary-menu" class="main-nav"><li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-226"><a title="Creative Portfolio" href="https://www.vividcreative.com/our-work/">Our Work</a></li>
<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a title="Brand, Web and Design Services" href="https://www.vividcreative.com/our-services/">Our Services</a><div class="services-mega-menu"><div class="mega-menu-col">
			            <h2><a href="/our-services/brand-strategy/">Brand strategy</a></h2><div class="menu-brand-strategy-container"><ul id="menu-brand-strategy" class="mega-menu-nav"><li id="menu-item-386" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-386"><a title="Brand Review" href="https://www.vividcreative.com/our-services/brand-strategy/brand-review/">Brand Review</a></li>
<li id="menu-item-385" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-385"><a title="Brand Direction" href="https://www.vividcreative.com/our-services/brand-strategy/brand-direction/">Brand Direction</a></li>
<li id="menu-item-384" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-384"><a title="Brand Communication Strategy" href="https://www.vividcreative.com/our-services/brand-strategy/brand-communication-strategy/">Brand Communication Strategy</a></li>
<li id="menu-item-388" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-388"><a title="Product Launch Agency" href="https://www.vividcreative.com/our-services/brand-strategy/product-launches/">Product Launches</a></li>
<li id="menu-item-387" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-387"><a title="Internal Communications Consultants" href="https://www.vividcreative.com/our-services/brand-strategy/internal-communications/">Internal Communications</a></li>
</ul></div></div><div class="mega-menu-col">
		                <h2><a href="/our-services/brand-creation/">Brand creation</a></h2><div class="menu-brand-creation-container"><ul id="menu-brand-creation" class="mega-menu-nav"><li id="menu-item-391" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-391"><a title="Brand Logo Design" href="https://www.vividcreative.com/our-services/brand-creation/brand-identity/">Brand Identity</a></li>
<li id="menu-item-390" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-390"><a title="Brand Guideline Designers" href="https://www.vividcreative.com/our-services/brand-creation/brand-guidelines/">Brand Guidelines</a></li>
<li id="menu-item-573" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-573"><a title="Brand Experience Consultants" href="https://www.vividcreative.com/our-services/brand-creation/branded-experience/">Branded Experience</a></li>
</ul></div></div><div class="mega-menu-col">
		                <h2><a href="/our-services/web-digital/">Web &amp; Digital</a></h2><div class="menu-web-digital-container"><ul id="menu-web-digital" class="mega-menu-nav"><li id="menu-item-406" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-406"><a title="Website design Sheffield" href="https://www.vividcreative.com/our-services/web-digital/website-design-development/">Website design &#038; development</a></li>
<li id="menu-item-405" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-405"><a title="Social Media Agency" href="https://www.vividcreative.com/our-services/web-digital/social-media-campaigns/">Social Media Campaigns</a></li>
<li id="menu-item-399" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-399"><a title="Conversion Optimisation" href="https://www.vividcreative.com/our-services/web-digital/cro/">Conversion Rate Optimisation</a></li>
<li id="menu-item-400" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-400"><a title="Ecommerce website design" href="https://www.vividcreative.com/our-services/web-digital/ecommerce-websites/">Ecommerce Websites</a></li>
<li id="menu-item-401" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-401"><a title="Email marketing agency" href="https://www.vividcreative.com/our-services/web-digital/email-marketing/">Email Marketing</a></li>
<li id="menu-item-403" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-403"><a title="PPC Agency Sheffield" href="https://www.vividcreative.com/our-services/web-digital/ppc-display-advertising/">PPC &#038; Display Advertising</a></li>
<li id="menu-item-404" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-404"><a title="SEO Sheffield" href="https://www.vividcreative.com/our-services/web-digital/seo/">Search &#038; SEO</a></li>
</ul></div></div><div class="mega-menu-col">
		                <h2><a href="/our-services/creative-design/">Creative design</a></h2><div class="menu-creative-design-container"><ul id="menu-creative-design" class="mega-menu-nav"><li id="menu-item-393" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-393"><a title="Brochure Design Sheffield" href="https://www.vividcreative.com/our-services/creative-design/brochure-design/">Brochures and Literature Design</a></li>
<li id="menu-item-397" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-397"><a title="Catalogue Design Sheffield" href="https://www.vividcreative.com/our-services/creative-design/product-catalogues/">Product Catalogues</a></li>
<li id="menu-item-392" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-392"><a title="Advertising Agency Sheffield" href="https://www.vividcreative.com/our-services/creative-design/advertising-campaigns/">Advertising Campaigns</a></li>
<li id="menu-item-394" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-394"><a title="Direct Mail Agency" href="https://www.vividcreative.com/our-services/creative-design/direct-mail/">Direct Mail</a></li>
<li id="menu-item-395" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-395"><a title="Packaging Design Agency" href="https://www.vividcreative.com/our-services/creative-design/packaging-design/">Packaging Design</a></li>
</ul></div></div></div></li>
<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a title="Latest News &#038; Posts" href="https://www.vividcreative.com/blog/">Blog</a></li>
<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a title="About Vivid Creative" href="https://www.vividcreative.com/about-vivid/">About Vivid</a></li>
<li id="menu-item-3009" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3009"><a title="Contact Vivid Creative Sheffield" href="#get-in-touch">Get In Touch</a></li>
</ul></div>			</nav>

			<button class="bn__toggle" aria-label="Main Menu"><!-- Silence is Golden --></button>
		</header>


		
<div class="hp-video--outer">
	<div class="hp-video">
		<iframe id="hp-video-vimeo" title="Vivid Creative Video Showreel" src="https://player.vimeo.com/video/254705277?autoplay=1&quality=720p" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		<img src="https://www.vividcreative.com/wp-content/themes/vivid/images/volume-muted.svg" alt="Volume Control" class="volume-button" style="display: none;"/>
		<div class="play-pause-btn"></div>
	</div>
</div>
<div class="hp-video-padder"></div>

<div class="outer grey">
	<header class="bn__contact">
	    <h1>Brands Burn Brighter</h1>
			<div class="bn__contact--details">
				<div class="bn-phone-number">
					<i><svg style="width: 16px; height: 16px;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" xml:space="preserve" width="16" height="16"><g class="nc-icon-wrapper" fill="#e62f1c"><path fill="#e62f1c" d="M15.285,12.305l-2.578-2.594c-0.39-0.393-1.025-0.393-1.416-0.002L9,12L4,7l2.294-2.294 c0.39-0.39,0.391-1.023,0.001-1.414l-2.58-2.584C3.324,0.317,2.691,0.317,2.3,0.708L0.004,3.003L0,3c0,7.18,5.82,13,13,13 l2.283-2.283C15.673,13.327,15.674,12.696,15.285,12.305z"></path></g></svg></i>
					<a href="tel:01142212121">01142 212121</a>
				</div>
				<div class="bn-phone-email">
					<i><svg style="width: 16px; height: 16px;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" xml:space="preserve" width="16" height="16"><g class="nc-icon-wrapper" fill="#e62f1c "><path data-color="color-2" fill="#e62f1c " d="M15,1H1C0.4,1,0,1.4,0,2v1.4l8,4.5l8-4.4V2C16,1.4,15.6,1,15,1z"></path> <path fill="#e62f1c " d="M7.5,9.9L0,5.7V14c0,0.6,0.4,1,1,1h14c0.6,0,1-0.4,1-1V5.7L8.5,9.9C8.22,10.04,7.78,10.04,7.5,9.9z"></path></g></svg></i>
					<a href="mailto:studio@vividcreative.co.uk">studio@vividcreative.co.uk</a>
				</div>
			</div>
	</header>

	
<!-- Section Start -->
<section id="bn__cs--wrapper">

            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/providore/" class="home_post_providore bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2018/01/providore.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Providoré</h3>
                <!-- Excerpt -->
                <p>For over a decade, Providoré have brought you the finest quality hampers and business gifts, to help you make a positive lasting impression on your most important business clients.</p>
            </div>
        </a>
            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/symphony-group/" class="home_post_symphony-group bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2018/02/symphony.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Symphony Group</h3>
                <!-- Excerpt -->
                <p>Symphony Group PLC is one of the UK’s largest suppliers of fitted kitchen, bedroom and bathroom furniture.</p>
            </div>
        </a>
            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/club-gt-events/" class="home_post_club-gt-events bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2018/02/Club-GT-case-study-1.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Club GT Events</h3>
                <!-- Excerpt -->
                <p>Club GT Events take small groups of Supercar owners through some of Europe’s most breath-taking scenery on exclusive and extraordinary European driving tours.</p>
            </div>
        </a>
            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/conrad-blandford/" class="home_post_conrad-blandford bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2016/06/v10180_Conrad_Blandford_3-1.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Conrad Blandford</h3>
                <!-- Excerpt -->
                <p>When we won the Digital Direction funded project work for leading Sheffield Salon, Conrad Blandford earlier this year, we knew it just needed a little love to boost its image on the digital waves.
</p>
            </div>
        </a>
            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/wild-country/" class="home_post_wild-country bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2016/06/wildcountry.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Wild Country</h3>
                <!-- Excerpt -->
                <p>From our very humble beginnings, Vivid and have partnered with Wild Country to continually forge their brand into the hearts and minds of climbers all around the world. #pureclimbing</p>
            </div>
        </a>
            <!-- Portfolio Item Wrapper -->
        <a href="https://www.vividcreative.com/our-work/carrs-silverware/" class="home_post_carrs-silverware bn__cs--wrapper__item" style="background-image:url('https://www.vividcreative.com/wp-content/uploads/2016/06/carrs.jpg');">
            <!-- Animated Content -->
            <div class="bn__cs--wrapper__item--content">
                <!-- Client Name -->
                <h3>Carrs Silverware</h3>
                <!-- Excerpt -->
                <p>We’ve been working with Carrs Silverware and their portfolio of luxury brands for almost a decade. On their brand journey we have collaborated with Harrods, Selfridges and Mappin & Webb to champion their retail presence.</p>
            </div>
        </a>
    
</section>
</div>
<div class="outer">
	<a class="pre-footer-cta-outer" href="https://www.vividcreative.com/our-work/">
	    <div class="pre-footer-cta">
	        <h4>Explore our work</h4>
		        <i class="right">
			        <svg class="icon-arrow" viewBox="0 0 30 30">
			            <use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-arrow"></use>
			        </svg>
		    	</i>
	    </div>
	</a>
</div>	<div class="vivid-intro-text">
    <div class="vivid-intro">
       <p><p>With over 20 years' experience and literally hundreds of success stories on the way, Vivid are one of the most effective branding and digital agencies in the UK. We make Brands Burn Brighter than ever before. Ranked Best Brand Strategy Agency in the UK 2018/19 by RAR+</p>
</p>
    </div>
    <div class="vivid-latest-news">
    		    <a href="https://www.vividcreative.com/weve-created-a-new-home/" class="news-item bn__post--wrapper" style="background-image: url('https://www.vividcreative.com/wp-content/uploads/2019/06/vivid-interior.jpg');">
			<div class="bn__post">
	    			<p>Latest News</p>
	    			<h2>We've created a new home</h2>
			</div>
	    </a>
    </div>
</div>    <footer class="pg-footer">
        <div class="inner-padded">
            <div class="pg-footer-header">
                <h3>Get in touch</h3>
            </div>
            <div class="quick-contact cf7">
                <div class="quick-contact-form">
                    <div role="form" class="wpcf7" id="wpcf7-f187-p68-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f187-p68-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="187" />
<input type="hidden" name="_wpcf7_version" value="5.0.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f187-p68-o1" />
<input type="hidden" name="_wpcf7_container_post" value="68" />
</div>
<p><span id="hp5de4da15cc87c" class="wpcf7-form-control-wrap username-wrap" style="display:none !important; visibility:hidden !important;"><label  class="hp-message">Please leave this field empty.</label><input class="wpcf7-form-control wpcf7-text"  type="text" name="username" value="" size="40" tabindex="-1" autocomplete="nope" /></span></p>
<div class="quick-contact-fields">
<div class="quick-contact-column">
<p><span class="wpcf7-form-control-wrap full-name"><input type="text" name="full-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Full name (required)" /></span></p>
<p><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email (required)" /></span></p>
</div>
<div class="quick-contact-column">
<p><span class="wpcf7-form-control-wrap organisation"><input type="text" name="organisation" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Organisation (required)" /></span></p>
<p><span class="wpcf7-form-control-wrap telephone"><input type="text" name="telephone" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Telephone" /></span></p>
</div>
</div>
<div class="quick-contact-enquiry">
<p><span class="wpcf7-form-control-wrap enquiry"><textarea name="enquiry" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Your enquiry"></textarea></span></p>
</div>
<div class="quick-contact-submit">
<p><input type="submit" value="Send enquiry" class="wpcf7-form-control wpcf7-submit" /></p>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>                </div>
            </div>
            <div class="vivid-address">
                <div class="address-col">
                    <div class="address-block">
                        <h4>Sheffield</h4>
                        <ul>
<li>Wharncliffe Works</li>
<li>Green Lane</li>
<li>Sheffield S3 8SE</li>
</ul>
                        <a class="map-btn" href="https://goo.gl/maps/JFF1j" target="_blank" rel="noreferrer">Map<svg class="icon-arrow right" viewBox="0 0 30 30"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-arrow"></use></svg></a>
                    </div>
                    <div class="address-block">
                        <ul>
                            <li class="pg-footer-tel">01142 21 21 21</li>
                            <li><a href="mailto:studio@vividcreative.com">studio@vividcreative.com</a></li>
                        </ul>
                    </div>
                </div>
                <nav class="social-nav">
                    <ul class="social-nav-list">
                        <li class="social-facebook"><a href="http://www.facebook.com/vividcreativeuk" target=_"blank" aria-label="Find us on Facebook"><svg viewBox="-20 -10 50 50"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-facebook"></use></svg></a></li>
                        <li class="social-twitter"><a href="http://twitter.com/vivid_creative" target=_"blank" aria-label="Follow us on Twitter"><svg viewBox="0 0 50 50"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-twitter"></use></svg></a></li>
                        <li class="social-linkedin"><a href="http://www.linkedin.com/company/vivid-creative" target=_"blank" aria-label="Find us on LinkedIn"><svg viewBox="-9.728 -9.764 50 50"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-linkedin"></use></svg></a></li>
                        <li class="social-instagram"><a href="http://www.instagram.com/vivid_creative" target=_"blank" aria-label="Find us on Instagram"><svg viewBox="0 0 50 50"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#icon-instagram"></use></svg></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>

<div class="post-footer ">
    <div class="inner">

		<div class="awardslogos">
			<img src="https://www.vividcreative.com/wp-content/themes/vivid/images/googlepartner.png" alt="Certified Google Partner" alt class="gogolepartner"/>
			<img src="https://www.vividcreative.com/wp-content/themes/vivid/images/thedrumtop100.png" alt="DRUM RAR Top 100 Agency" class="drum"/>
			<img src="https://www.vividcreative.com/wp-content/themes/vivid/images/rar.png" alt="Recommended Agency Register" class="rar"/>
		</div>

        <div class="footer-logo">
           <svg class="vivid-logo-sml" viewBox="0 0 16 16"><use xlink:href="https://www.vividcreative.com/wp-content/themes/vivid/images/icons.svg#vivid-icon"></use></svg>
        </div>
        <ul class="footer-legal-links">
            <li>&copy; 2019 Vivid Creative Ltd </li>
            <li id="menu-item-173" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-173"><a href="mailto:studio@vividcreative.com">studio@vividcreative.com</a></li>
<li id="menu-item-175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175"><a title="Marketing Jobs Sheffield" href="https://www.vividcreative.com/current-vacancies/">Join The Team</a></li>
<li id="menu-item-1028" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1028"><a title="Privacy Policy &#038; Cookies" href="https://www.vividcreative.com/privacy-and-cookies/">Privacy and cookies</a></li>
        </ul>
    </div>
</div>
<script type='text/javascript' defer src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.vividcreative.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' defer src='https://www.vividcreative.com/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript'>
var cf7FormIDs = {"ID_1563":"Digital Marketing Consultation","ID_563":"Apply Now","ID_187":"Quick Contact"}
</script>
<script type='text/javascript' defer src='https://www.vividcreative.com/wp-content/plugins/cf7-google-analytics/js/cf7-google-analytics.min.js'></script>
<script type='text/javascript' defer src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
<script type='text/javascript' defer src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js'></script>
<script type='text/javascript' defer src='https://cdnjs.cloudflare.com/ajax/libs/mixitup/2.1.11/jquery.mixitup.min.js'></script>
<script type='text/javascript' defer src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js'></script>
<script type='text/javascript' defer src='https://www.vividcreative.com/wp-content/themes/vivid/scripts/main.min.js'></script>
<script type='text/javascript' defer src='https://www.vividcreative.com/wp-includes/js/wp-embed.min.js'></script>
<script defer type="text/javascript" src="/wp-content/themes/vivid/jquery.vimeo.api.js"></script>
<script defer type="text/javascript" src="/wp-content/themes/vivid/update.js"></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/svg4everybody/2.1.9/svg4everybody.min.js'></script>
<script>svg4everybody(); // run it now or whenever you are ready</script>
</body>
</html>

<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Object Caching 137/141 objects using disk
Page Caching using disk: enhanced 
Database Caching 11/30 queries in 0.006 seconds using disk

Served from: www.vividcreative.com @ 2019-12-02 09:32:05 by W3 Total Cache
-->