<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="en" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="en" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="en" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->

<head>
  <meta charset="utf-8" />
<link rel="shortlink" href="/en/node/1" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/en/welcome" />
  <title>Welcome | Sepp Schwarz</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="cleartype" content="on">

  <link type="text/css" rel="stylesheet" href="https://www.seppschwarz.com/sites/default/files/css/css_kShW4RPmRstZ3SpIC-ZvVGNFVAi0WEMuCnI0ZkYIaFw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.seppschwarz.com/sites/default/files/css/css_kxjycaqb9gaNUq-4IU-nFk7diakSG2YK6YYZ6m8VR8s.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.seppschwarz.com/sites/default/files/css/css_e9i1o2m_GsLXCsIe3pJwpeNkUjTw8sddNBrCOrjat7I.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.seppschwarz.com/sites/default/files/css/css_-9x9EiOeupdqk9Eegx8nn-DP4uoL_3kNjbjhMk8B3yM.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:regular&amp;subset=latin" media="all" />
  <script src="https://www.seppschwarz.com/sites/default/files/js/js_MpKfe1sTh5JIVGCZ17DsAuT1rqAC38MLLlkjqjQ1X_k.js"></script>
<script src="https://www.seppschwarz.com/sites/default/files/js/js_U2z3D6BkMbInlZMboBflNgZZpDBRj9LN0MYyyHFud_I.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"en\/","ajaxPageState":{"theme":"sepp","theme_token":"ZWnPVT6XLXinsLTV-krnrmTPBld_A8EalYeRYulNaGw","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/sepp\/js\/custom.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"modules\/locale\/locale.css":1,"sites\/all\/themes\/zen\/system.menus.css":1,"sites\/all\/themes\/sepp\/css\/style.css":1,"https:\/\/fonts.googleapis.com\/css?family=Droid+Serif:regular\u0026subset=latin":1}}});</script>
      <!--[if lt IE 9]>
    <script src="/sites/all/themes/zen/js/html5-respond.js"></script>
    <![endif]-->
  </head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-1 node-type-page i18n-en" >
      <p id="skip-link">
      <a href="#main-menu" class="element-invisible element-focusable">Jump to navigation</a>
    </p>
      <div id="wrapper">
    <section class="region region-sidebar-first column sidebar">
    <div id="block-block-1" class="block block-block first odd">

      
  <p><a href="/"><img alt="Sepp Schwarz" src="/sites/all/themes/sepp/i/logo.jpg" /></a></p>

</div>
<div id="block-bean-intro-text" class="block block-bean even">

      
  <div class="entity entity-bean bean-basic-text clearfix" about="/en/block/intro-text" typeof="">

  <div class="content">
    
  <p>The late Prof. Josef Schwarz (17/02/1917 - 25/12/2013) counts as one of the most influential Tyrolean artists of all time.  He was a dedicated, private man who, from 1938, earned his living as a teacher and later as a Headmaster in Telfs, Tirol.  His prolific output ranges from oils through watercolour and graphic works to wood blocks and carvings.  He lived and worked in Telfs from 1946 until his recent passing on Christmas Day 2013.</p>
  </div>
</div>

</div>
<div id="block-views-taxonomy-links-block" class="block block-views odd">

        <h2 class="block__title block-title">Categories</h2>
    
  <div class="view view-taxonomy-links view-id-taxonomy_links view-display-id-block view-dom-id-748f63065cf68c38d0028a0c220bf825">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/graphic-pen-ink">Graphic (Pen &amp; Ink)</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/mixed-media">Mixed media</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/religion">Religion</a></span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/watercolour">Watercolour</a></span>  </div></li>
          <li class="views-row views-row-5 views-row-odd">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/wood-prints">Wood Prints</a></span>  </div></li>
          <li class="views-row views-row-6 views-row-even views-row-last">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/categories/wooden-sculptures">Wooden Sculptures</a></span>  </div></li>
      </ul></div>    </div>
  
  
  
  
  
  
</div>
</div>
<div id="block-views-taxonomy-links-block-1" class="block block-views last even">

        <h2 class="block__title block-title">Decades</h2>
    
  <div class="view view-taxonomy-links view-id-taxonomy_links view-display-id-block_1 view-dom-id-95e6013d39370de07822c161db5bd500">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1930s">1930s</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1940s">1940s</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1950s">1950s</a></span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1960s">1960s</a></span>  </div></li>
          <li class="views-row views-row-5 views-row-odd">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1970s">1970s</a></span>  </div></li>
          <li class="views-row views-row-6 views-row-even">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1980s">1980s</a></span>  </div></li>
          <li class="views-row views-row-7 views-row-odd">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/1990s">1990s</a></span>  </div></li>
          <li class="views-row views-row-8 views-row-even views-row-last">  
  <div class="views-field views-field-name-i18n">        <span class="field-content"><a href="/en/year/2000s">2000s</a></span>  </div></li>
      </ul></div>    </div>
  
  
  
  
  
  
</div>
</div>
  </section>
    <div class="region region-above-content">
    <div id="block-locale-language" class="block block-locale first odd" role="complementary">

      
  <ul class="language-switcher-locale-url"><li class="de first"><a href="/de" class="language-link" xml:lang="de">Deutsch</a></li>
<li class="en last active"><a href="/en" class="language-link active" xml:lang="en">English</a></li>
</ul>
</div>
<div id="block-menu-menu-top-nav" class="block block-menu last even" role="navigation">

      
  <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/en" title="" class="menu__link active">Home</a></li>
<li class="menu__item is-leaf leaf"><a href="/en/gallery" title="" class="menu__link">Gallery</a></li>
<li class="menu__item is-leaf last leaf"><a href="/en/museum-opening-times" title="" class="menu__link">Museum Opening Times</a></li>
</ul>
</div>
  </div>
  <section id="main-content">
  


<article class="node-1 node node-page view-mode-full clearfix" about="/en/welcome" typeof="foaf:Document">

      <header>
                  <span property="dc:title" content="Welcome" class="rdf-meta element-hidden"></span>
      
          </header>
  
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden">
    <p><img alt="" src="/sites/default/files/Image-6169-HomeCrop4.jpg" style="width: 705px; height: 341px;" /></p>
<p> </p>
<p><img alt="" src="/sites/default/files/Image-6169-HomeCrop5.jpg" style="width: 460px; height: 330px; float: right; margin-left: 15px; margin-right: 5px; margin-bottom: 18px;" /></p>
<p><strong>As well as exhibiting in his own right Sepp was also invited to exhibit at Group exhibits in Antwerp, Bucharest, Cologne, Zurich, Vienna and Salzburg together with Pablo Picasso, Salvadore Dali and Henry Moore recognizing Josef Schwarz’s as an important artist.</strong></p>
<p>A self taught artist, he has specialised since the mid 50’s in astounding pen and ink works drawing from his imagination.  From 1968, he turned his talents evermore to woodblocks, developing his own unique style. Throughout his life he has also produced watercolours inspired by his travels through Israel, Corsica, Italy, England and France.</p>
  </div>

  
  
</article>
<div id="block-bean-home-page-three-image-block" class="block block-bean last even">

      
  <div class="entity entity-bean bean-basic-text clearfix" about="/en/block/home-page---three-image-block" typeof="">

  <div class="content">
    
  <p><img alt="" src="/sites/default/files/Image-6169-HomeCrop6.jpg" style="width: 213px; height: 160px;" /><img alt="" src="/sites/default/files/Image-6169-HomeCrop7.jpg" style="width: 213px; height: 160px; margin-left: 32px; margin-right: 33px;" /><img alt="" src="/sites/default/files/Image-6169-HomeCrop8.jpg" style="width: 213px; height: 160px;" /></p>
  </div>
</div>

</div>
  </section>
    <footer>
      <footer id="footer" class="region region-footer">
    <div id="block-block-4" class="block block-block first last odd">

      
  <p>© Josef Schwarz 2013</p>

</div>
  </footer>
  </footer>
</div>

  </body>
</html>
