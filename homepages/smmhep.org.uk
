<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" lang="en" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.smmhep.org.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<meta content="Introduction" about="/introduction" property="dc:title" />
<meta about="/introduction" property="sioc:num_replies" content="0" datatype="xsd:integer" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="/introduction" />
<link rel="shortlink" href="/node/101" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>Introduction | Scotland Malawi Mental Health Education Project</title>
<style>@import url("https://www.smmhep.org.uk/modules/system/system.base.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/system/system.menus.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/system/system.messages.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/system/system.theme.css?q152hw");</style>
<style>@import url("https://www.smmhep.org.uk/modules/comment/comment.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/field/theme/field.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/modules/logintoboggan/logintoboggan.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/node/node.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/search/search.css?q152hw");
@import url("https://www.smmhep.org.uk/modules/user/user.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/modules/views/css/views.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/modules/ckeditor_image2/ckeditor_image2.theme.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/modules/ckeditor_image2/ckeditor_image2.responsive.css?q152hw");</style>
<style>@import url("https://www.smmhep.org.uk/sites/all/modules/ctools/css/ctools.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/modules/tagclouds/tagclouds.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/libraries/superfish/css/superfish.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/libraries/superfish/style/space-orange.css?q152hw");</style>
<style media="screen">@import url("https://www.smmhep.org.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.image.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?q152hw");</style>
<style>@import url("https://www.smmhep.org.uk/sites/default/files/color/pixture_reloaded-d48275a4/colors.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/pixture_reloaded.css?q152hw");
@import url("https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/pixture_reloaded.settings.style.css?q152hw");</style>
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/default/files/adaptivetheme/pixture_reloaded_files/pixture_reloaded.responsive.layout.css?q152hw" media="only screen" />
<style media="screen">@import url("https://www.smmhep.org.uk/sites/default/files/adaptivetheme/pixture_reloaded_files/pixture_reloaded.fonts.css?q152hw");</style>
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/responsive.smartphone.portrait.css?q152hw" media="only screen and (max-width:120px)" />
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/responsive.smartphone.landscape.css?q152hw" media="only screen and (min-width:121px) and (max-width:280px)" />
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/responsive.tablet.portrait.css?q152hw" media="only screen and (min-width:281px) and (max-width:668px)" />
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/responsive.tablet.landscape.css?q152hw" media="only screen and (min-width:669px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/css/responsive.desktop.css?q152hw" media="only screen and (min-width:1025px)" />

<!--[if (lt IE 9)&(!IEMobile 7)]>
<style media="screen">@import url("https://www.smmhep.org.uk/sites/default/files/adaptivetheme/pixture_reloaded_files/pixture_reloaded.lt-ie9.layout.css?q152hw");</style>
<![endif]-->
<style>#header .header-inner{background:url('https://www.smmhep.org.uk/sites/all/themes/pixture_reloaded/header-images/background.png') center top no-repeat #ffffff  !important;height:auto;-o-background-size:auto auto;-webkit-background-size:auto auto;-khtml-background-size:auto auto;-moz-background-size:auto auto;background-size:auto auto;background-origin:border-box;}
</style>
<script src="https://www.smmhep.org.uk/sites/all/modules/jquery_update/replace/jquery/1.7/jquery.min.js?v=1.7.2"></script>
<script src="https://www.smmhep.org.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.smmhep.org.uk/misc/drupal.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/modules/behavior_weights/behavior_weights.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/modules/google_analytics/googleanalytics.js?q152hw"></script>
<script>var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-2835464-2"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();</script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/sftouchscreen.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/supposition.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/jquery.bgiframe.min.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/superfish.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/libraries/superfish/supersubs.js?q152hw"></script>
<script src="https://www.smmhep.org.uk/sites/all/modules/superfish/superfish.js?q152hw"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"pixture_reloaded","theme_token":"DKgC635psYzGcV95lleM5Q2Sf5yejDn4imbJ0wpe8I8","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/behavior_weights\/behavior_weights.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/jquery.bgiframe.min.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor_image2\/ckeditor_image2.theme.css":1,"sites\/all\/modules\/ckeditor_image2\/ckeditor_image2.responsive.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/tagclouds\/tagclouds.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/style\/space-orange.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/pixture_reloaded\/color\/colors.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.settings.style.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.responsive.layout.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.fonts.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/responsive.smartphone.portrait.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/responsive.smartphone.landscape.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/responsive.desktop.css":1,"public:\/\/adaptivetheme\/pixture_reloaded_files\/pixture_reloaded.lt-ie9.layout.css":1,"0":1}},"rs":{"global":{"url":"https:\/\/www.smmhep.org.uk\/sites\/default\/files\/","dir":"\/sites\/all\/modules\/revslider"}},"carbide_builder":{"DisallowContainers":["101|field_carbide"]},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show"},"speed":"fast","autoArrows":false},"plugins":{"touchscreen":{"mode":"useragent_predefined"},"supposition":true,"bgiframe":true,"supersubs":{"minWidth":"6","maxWidth":"20"}}}},"glazed":{"glazedPath":"sites\/all\/themes\/glazed\/"}});</script>
<!--[if lt IE 9]>
<script src="https://www.smmhep.org.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?q152hw"></script>
<![endif]-->
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-101 node-type-page site-name-scotland-malawi-mental-health-education-project color-scheme-custom pixture-reloaded bs-l bb-n mb-dd mbp-l rc-0">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="texture-overlay">
  <div id="page" class="container page snc-n snw-b sna-c sns-l ssc-n ssw-n ssa-l sss-n btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-n">

    <header id="header" class="clearfix" role="banner">
      <div class="header-inner clearfix">

                  <!-- start: Branding -->
          <div id="branding" class="branding-elements clearfix">

                          <div id="logo">
                <a href="/" title="Home page" class="active"><img class="site-logo image-style-none" typeof="foaf:Image" src="https://www.smmhep.org.uk/sites/default/files/SMMHEP.png" alt="Scotland Malawi Mental Health Education Project" /></a>              </div>
            
                          <!-- start: Site name and Slogan hgroup -->
              <hgroup id="name-and-slogan" id="name-and-slogan">

                                  <h1 id="site-name" id="site-name"><a href="/" title="Home page" class="active">Scotland Malawi Mental Health Education Project</a></h1>
                
                
              </hgroup><!-- /end #name-and-slogan -->
            
          </div><!-- /end #branding -->
        
        
      </div>

    </header> <!-- /header -->

    <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish no-title menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1" >  
  
  <ul  id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-space-orange sf-total-items-6 sf-parent-items-1 sf-single-items-5"><li id="menu-218-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" title="" class="sf-depth-1 active">Home</a></li><li id="menu-522-1" class="middle even sf-item-2 sf-depth-1 sf-total-children-6 sf-parent-children-0 sf-single-children-6 menuparent"><a href="/project" class="sf-depth-1 menuparent">The Project</a><ul><li id="menu-962-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/project/what-we-do" class="sf-depth-2">What we do</a></li><li id="menu-526-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/project/how-help" class="sf-depth-2">How to Help</a></li><li id="menu-525-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/funding-information" class="sf-depth-2">Funding Information</a></li><li id="menu-528-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/project/video" class="sf-depth-2">Video</a></li><li id="menu-1513-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="/latestnews" title="" class="sf-depth-2">News</a></li><li id="menu-721-1" class="last even sf-item-6 sf-depth-2 sf-no-children"><a href="/project/useful-links" class="sf-depth-2">Useful Links</a></li></ul></li><li id="menu-524-1" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/scotland" class="sf-depth-1">Scotland</a></li><li id="menu-523-1" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="/malawi" class="sf-depth-1">Malawi</a></li><li id="menu-527-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/contact" class="sf-depth-1">Contact</a></li><li id="menu-718-1" class="last even sf-item-6 sf-depth-1 sf-no-children"><a href="https://www.smmhep.org.uk/user/login?destination=node/101" title="" class="sf-depth-1">Login/Register</a></li></ul>
  </nav></div>
    <!-- Messages and Help -->
        
    <!-- Breadcrumbs -->
    
    <div class="region region-secondary-content"><div class="region-inner clearfix"><div id="block-views-latest-news-block-1" class="block block-views no-title odd first last block-count-2 block-region-secondary-content block-latest-news-block-1" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><div class="view view-latest-news view-id-latest_news view-display-id-block_1 view-dom-id-7bc912b251efe8b76a6e24a634d6bb32">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <h1 class="field-content page-title"><a href="/content/currently-recruiting-201920">Currently recruiting for 2019/20</a></h1>  </div>  
  <div class="views-field views-field-body">        <div class="field-content"><p><strong>Currently recruiting for 2019/20 undergraduate teaching in Blantyre and Zomba Mental Hospital. If you're a higher trainee or consultant psychiatrist and are interested and available to to work for between three and eight weeks in Malawi there may be an exciting opportunity for you in 2019/20. Please contact <a href="mailto:mail@SMMHEP.org.uk">mail@SMMHEP.org.uk</a> to find out more.</strong></p>
</div>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/latestnews">
    more  </a>
</div>
  
  
  
</div></div>
  </div></div></div></div>
    <!-- Three column 3x33 Gpanel -->
    
    <div id="columns">
      <div class="columns-inner clearfix">

        <div id="content-column">
          <div class="content-inner">

            
            <section id="main-content" role="main">

                                            <header id="main-content-header" class="clearfix">

                                      <h1 id="page-title">Introduction</h1>
                  
                  
                </header>
                            
                              <div id="content">
                  <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-101" class="node node-page article odd node-full ia-n clearfix" about="/introduction" typeof="foaf:Document" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p>The Republic of Malawi is a beautiful and peaceful country in southeast Africa, with a population of about 17 million, three times that of Scotland. It is bordered by Zambia to the northwest, Tanzania to the northeast, and Mozambique on the east, south and west. It is separated from Tanzania and Mozambique by Lake Malawi.</p>
<p>Though beautiful, Malawi is extremely poor; the World Bank estimates that it is one of the poorest in the world. The average life expectancy of 55-60 years reflects some of the world's highest rates for infant and maternal mortality, malnutrition and infectious diseases. Despite the its serious health problems, the country has one of the lowest numbers of doctors per capita (1 per 50,000). International epidemiological studies would suggest that the rates of mental illness in Malawi are also high - at least as high as those in western and other developing countries. In spite of this mental health services are very underdeveloped. Two doctors have recently qualified as the only trained Malawian psychiatrists working in the country: a ratio of one psychiatrist to treat more than 8 million people. </p>
<div id="feature_box_right">
<h4>View the SMMHEP video</h4>
<p><a href="/project/video" target="_self" title="Video"><img alt="project video" height="318" src="/sites/default/files/resize/images/smmhepvid-601x318.jpg" width="601" /></a></p></div>
<p>The Scotland Malawi Mental Health Education Project (SMMHEP) aims to help develop sustainable mental health services in Malawi, by supporting psychiatric teaching and training for student doctors and other health care professionals. The project has enjoyed close links with individuals and institutions in Malawi since 2006 and became a registered Scottish Charity in 2008, converting to a Scottish Charitable Incorporated Organisation (SCIO) in September 2014.</p>
<p>Currently we deliver psychiatry teaching to medical students at the Malawi College of Medidcine and support postgraduate psychiatry trainees doing the Master in Medicine course.  We have supported training of psychiatry nurses and clinical officers based in Zomba Mental Hospital and Queen Elizabeth Central Hospital, Blantyre. Between 2011 and 2016, we also funded and helped organise an annual Mental Health Conference in Malawi that was attended by delegates from Malawi, other countries in sub Saharan Africa, the UK, Europe and the USA.</p>
<p>In addition, we have also developed a package of e-learning materials in mental health and completed a project to train primary care health workers in mental health.</p>
<p>The project has received support from the Royal College of Psychiatrists (London and Scotland), the Scottish Government, National Health Service Education Scotland (NES), the Tropical Health Education Trust (THET) and local postgraduate Deaneries and tutors in Scotland.</p>
</div></div></div>  </div>

  
  
  </article>

  </div>                </div>
              
              <!-- Feed icons (RSS, Atom icons etc -->
              
            </section> <!-- /main-content -->

            
          </div>
        </div> <!-- /content-column -->

                <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section id="block-views-latest-news-block" class="block block-views odd first block-count-4 block-region-sidebar-second block-latest-news-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Latest News</h2>
  
  <div class="block-content content"><div class="view view-latest-news view-id-latest_news view-display-id-block view-dom-id-28a2ed73c421d6418f25a89690f98596">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/smmhep-supports-launch-african-alliance-maternal-mental-health">SMMHEP supports launch of African Alliance for Maternal Mental Health</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/exciting-lecturersenior-lecturer-opportunity-college-medicine">Exciting Lecturer/Senior Lecturer opportunity at College of Medicine</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd views-row-last">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/smmhep-featured-rcpsych-insight-magazine">SMMHEP featured in RCPsych Insight Magazine</a></span>  </div></li>
      </ul></div>    </div>
  
      <div class="item-list"><ul class="pager"><li class="pager-previous even first">&nbsp;</li><li class="pager-current odd">1 of 6</li><li class="pager-next even last"><a title="Go to next page" href="/introduction?page=1">next ›</a></li></ul></div>  
  
      
<div class="more-link">
  <a href="/latestnews">
    more  </a>
</div>
  
  
  
</div></div>
  </div></section><section id="block-block-2" class="block block-block even block-count-5 block-region-sidebar-second block-2" ><div class="block-inner clearfix">  
      <h2 class="block-title">Donate</h2>
  
  <div class="block-content content"><p class="c5"><a href="http://www.justgiving.com/smmhep/donate" target="_blank"><img alt="Donation Online button" border="0" height="53" hspace="0" src="/sites/default/files/pictures/justgiving.jpg" width="150" /></a></p>
</div>
  </div></section><div id="block-search-form" class="block block-search no-title odd last block-count-6 block-region-sidebar-second block-form"  role="search"><div class="block-inner clearfix">  
  
  <div class="block-content content"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-coI2rFwf67TnJun_n4nxVUpJsuz4SYIBSZlrLdlYivg" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div></div></div></div>
      </div>
    </div> <!-- /columns -->

    
    <!-- four-4x25 Gpanel -->
    
          <footer id="footer" role="contentinfo">
        <div id="footer-inner" class="clearfix">
          <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-3" class="block block-block no-title odd first last block-count-7 block-region-footer block-3" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p style="text-align: center;">© 2019 Scotland Malawi Mental Health Education Project<br /><br />Scottish Charitable Incorporated Organisation SC039523</p>
</div>
  </div></div></div></div>        </div>
      </footer>
    
  </div> <!-- /page -->
</div> <!-- /texture overlay -->
  <div style="display:none">sfy39587stp17</div></body>
</html>
