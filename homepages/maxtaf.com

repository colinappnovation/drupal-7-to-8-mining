<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en" dir="ltr">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en" dir="ltr">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en" dir="ltr">  <!--<![endif]-->
<head>
  <!--[if IE]><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.maxtaf.com/sites/default/files/logo%20no%20text.png" type="image/png" />
<meta name="description" content="Automated testing for Maximo Asset Management - Flawless Record &amp; Play - IBM Certified - Covers all layers: UI, MBO, MIF, DB... and more." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.maxtaf.com/" />
<link rel="shortlink" href="https://www.maxtaf.com/" />

  <title>MaxTAF | Maximo Test Automation Framework</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Specific Metas
	================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link type="text/css" rel="stylesheet" href="https://www.maxtaf.com/sites/default/files/advagg_css/css__Q_Qquwk5yIq0WDXuIZUQKU1y4YgXRZ9OCeplxIM_IGg__40l8U6s13WW7RmM3Bd_THmF6_exYws88VVnUytjGcRw__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.maxtaf.com/sites/default/files/advagg_css/css__PmwwwMmAi1SDGjvjorA5_gSpDoYqqAqptySDBmfLbMY__zc-4Ps0sB9aAbb9xlJGc8iKGYWiJpsiNzBYG4j0ktD4__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.maxtaf.com/sites/default/files/advagg_css/css__CtnTPy_7x_yql4wBtghJ1kjSDxPUdqLrMq_wWUvPLkw__JUETukPmzO5LwHipjZ919i6wR6XUN4u8C6e3-lQVXbE__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.css" media="all" />
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Anton|Muli:300,400,400italic,300italic|Goudy+Bookletter+1911|Oswald&amp;subset=latin,latin-ext" media="all" />
<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.maxtaf.com/sites/default/files/advagg_css/css__81-y7pqUQbMKAVOIHehLR0In_sfL0gm7YnY1PRxoFf0__9HXhSRF8zS3rORTuDyTmhok0ejITs9pyIodAuiTeg_k__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.css" media="all" />

	<!-- Head Libs -->
	<script src="/sites/all/themes/stability/vendor/modernizr.js"></script>

	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="/sites/all/themes/stability/vendor/respond.min.js"></script>
	<![endif]-->

	<!--[if IE]>
		<link rel="stylesheet" href="/sites/all/themes/stability/css/ie.css">
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="apple-touch-icon" href="/sites/all/themes/stability/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/sites/all/themes/stability/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/sites/all/themes/stability/images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/sites/all/themes/stability/images/apple-touch-icon-144x144.png">
  </head>
<body class="html front not-logged-in no-sidebars page-home">

    
<div class="site-wrapper">
  
  <header class="header header-default">
  <div class="header-top">
    <div class="container">

      <div class="header-top-left">
		<ul class="social-links social-links__dark">
			<li>
				<a target="_blank" href="//www.twitter.com/CodeDevLtd">
					<i class="fa fa-twitter"></i>
				</a>
			</li>
			<li>
				<a target="_blank" href="//www.linkedin.com/company/code-development-ltd-">
					<i class="fa fa-linkedin"></i>
				</a>
			</li>
			<li>
				<a target="_blank" href="//www.youtube.com/channel/UCDo5oVErAsFmcXxhXUb3xLg">
					<i class="fa fa-youtube"></i>
				</a>
			</li>
		</ul>
              </div>
      <div class="header-top-right">
		        
      <div class="header-top-right">
        <span class="login">
        <i class="fa fa-lock"></i> <a href="/user">Login</a>
      </span><span class="register"><i class="fa fa-pencil-square-o"></i>Not a Member? <a href="/user/register">Register</a></span><span class="cart">
        <i class="fa fa-shopping-cart"></i> <a href="/cart">Shopping Cart</a>
      </span>
      </div>      </div>

    </div>
  </div>

  <div class="header-main">
    <div class="container">

      <!-- Logo -->
      <div class="logo">
                  <a href="/"><img src="https://www.maxtaf.com/sites/default/files/logo.svg" alt="MaxTAF"></a>
                <p class="tagline"></p>
      </div>
      <!-- Logo / End -->

      <button type="button" class="navbar-toggle">
        <i class="fa fa-bars"></i>
      </button>

        <!-- Navigation -->
      <nav class="nav-main">
        <ul data-breakpoint="992" class="flexnav">
         <li  data-id="295" data-level="1" data-type="menu_item" data-class="hide-text" data-xicon="fa-home" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-1 mega hide-text active active-trail">
  <a href="/"  title="Home">
          <i class="fa fa-home"></i>
        
    Home      </a>
  </li>

<li  data-id="3206" data-level="1" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-1 mega dropdown">
  <a href="/products"  class="dropdown-toggle" title="Products">
        
    Products      </a>
  <ul  data-class="" data-width="" class="row tb-megamenu-submenu dropdown-menu mega-dropdown-menu nav-child">
  <div  data-class="" data-width="12" data-hidewcol="0" id="tb-megamenu-column-1" class="tb-megamenu-column   mega-col-nav">
  <li  data-id="4097" data-level="2" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-2 mega">
  <a href="/maxtaf-cloud"  title="MaxTAF Cloud">
        
    <img style="width:100%" src="/sites/default/files/images/MaxTAF Cloud Logo.svg">      </a>
  </li>

<li  data-id="4098" data-level="2" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-2 mega">
  <a href="/maxtaf-total"  title="MaxTAF Total">
        
    <img style="width:100%" src="/sites/default/files/images/MaxTAF Total Logo.svg">      </a>
  </li>

<li  data-id="4099" data-level="2" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-2 mega">
  <a href="/performance-testing"  title="Performance Testing">
        
    <img style="width:100%" src="/sites/default/files/images/MaxTAF Performance Logo.svg">      </a>
  </li>

<li  data-id="4100" data-level="2" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-2 mega">
  <a href="/services"  title="Services">
        
    <img style="width:100%" src="/sites/default/files/images/MaxTAF Services.svg">      </a>
  </li>
</div>
</ul>
</li>

<li  data-id="2512" data-level="1" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-1 mega">
  <a href="/documentation/guides"  title="Guides">
        
    Guides      </a>
  </li>

<li  data-id="300" data-level="1" data-type="menu_item" data-class="" data-xicon="" data-caption="" data-alignsub="" data-group="0" data-hidewcol="0" data-hidesub="0" class="tb-megamenu-item level-1 mega">
  <a href="/contact"  title="Contact">
        
    Contact      </a>
  </li>
		  <li class="tb-megamenu-item level-1 mega try">
			<div class="btn-container">
				<a href="/demo" class="btn btn-success">ARRANGE DEMO</a>
			</div>
		  </li>
        </ul>
      </nav>
      <!-- Navigation / End -->
      
    </div>
  </div>
</header>
  <div class="main" role="main">

    
    
      <section  style="padding-bottom: -80px; " class="nd-region">
  
    <div class = "container">   

      <div class = "container-fluid">

        <div  class="row" id="Top-Section">     

          
                                          <div  class="col-md-12 " id="top">
                              
                                  <div id="block-block-69" class="widget block block-block" >
  
      
  <div  class = 'boxed container'>
<div  class = 'row'>
<div  class = 'col-md-push-2 col-md-8'>
<p class="rtecenter"><img style="margin: 10px; width: 50%;" alt="" src="/sites/default/files/images/MaxTAF Colours 2 -reduced.svg"><h1 class="rtecenter">	<span style="font-size:36px;">Strengthen &amp; Accelarate your Maximo Testing&nbsp;</span></h1>
<div  style = 'padding-bottom: 10px; margin-bottom: 20px;'><p class="rtecenter">MaxTAF is the world's leading IBM Maximo specialised testing solution.</p><p class="rtecenter">Built with the world's best automation technology, MaxTAF's innovative scripting and recording facilties allows teams to dramatically lower the total cost of testing compared with other Maximo testing methodologies.</p></div>
</div>
</div>
<div  class = 'row'>
<div  class = 'col-md-4'>
<div  class = 'bordered icon-box  centered'>
    <div class="icon">
      <i class="fa fa-comment-o"></i>
    </div>
    <div class="icon-box-body">
      
<h2>	Arrange a Demo</h2>
<p>Arrange a free demo with one of our CDL Qualified Experts and see:</p><p>How to record a test</p><p>How to run a test</p><p>How to report test results</p>
<a  class = 'btn  btn-success btn-sm' href = '/demo'><i class = "fa fa-comments-o"></i>&nbsp; 
Arrange Demo</a>

    </div>
  </div>
</div>
<div  class = 'col-md-4'>
<div  class = 'bordered icon-box  centered'>
    <div class="icon">
      <i class="fa fa-bell-o"></i>
    </div>
    <div class="icon-box-body">
      
<h2>	Learn More</h2><p>Automating your testing guarantees:</p><p>Greater test coverage</p><p>Quicker releases</p><p>Project and operational cost reductions.</p>
<a  class = 'btn  btn-success btn-sm' href = '/sites/default/files/pdfs/MaxTAF%20Information%20Pack.pdf'><i class = "fa fa-file-code-o"></i>&nbsp; 
Info PDF</a>

    </div>
  </div>
</div>
<div  class = 'hidden-xs hidden-sm col-md-4'>
<div  class = 'bordered icon-box  centered' style = ' text-align:left;'>
    <div class="icon">
      <i class="fa fa-exclamation"></i>
    </div>
    <div class="icon-box-body">
      
<h2 class="rtecenter">	What's New with MaxTAF?</h2><p class="rtecenter">August 2019</p>
<p class="rtecenter"><img style="width:70%" alt="" src="/sites/default/files/images/Banner 3.0.png">

    </div>
  </div>
</div>
</div>
</div>
 
  </div> <!-- /.block -->
                
                              </div>
                                    
          
                      
          
                      
          
                      
          
                      
          
                      
          
        </div>

      </div>

    </div>
      </section>
    


      <section  class="nd-region">
  
       

      <div class = "container">

        <div  class="row" id="Top">     

          
                      
          
                      
          
                                          <div  class="col-md-12 " id="block-50">
                              
                                  <div id="block-block-50" class="widget block block-block " >
  
      
  <h1>	The No.1 Testing Solution for Maximo</h1>
<div  class = 'row'>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-play"></i><i class="fa fa-play  bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div>	<h3>Streamlined Record and Play</h3>	<p class="rtejustify">Fully optimized for Maximo user interface. No additional tuning required.</p></div>

    </div>
  </div>
</div>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-code"></i><i class="fa fa-code fa-fw bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div>	<h3>		No programming necessary</h3></div><p class="rtejustify">Friendly XML based scripting that can be mastered in a matter of days.</p>

    </div>
  </div>
</div>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-thumbs-o-up"></i><i class="fa fa-thumbs-o-up fa-fw bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div><h3>	Resilient UI tests</h3></div><p class="rtejustify">Immune to screen content changes and element re-arrangements.</p>

    </div>
  </div>
</div>
<div class="clearfix"></div>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-sort-amount-asc"></i><i class="fa fa-sort-amount-asc fa-fw bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div><h3>	Maximo back-end testing</h3></div><p class="rtejustify">Out of the box command library covering MBO, MIF, DB, workflows and more.</p>

    </div>
  </div>
</div>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-plug"></i><i class="fa fa-plug fa-fw bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div><h3>	Extensible framework</h3></div><p class="rtejustify">Further adapt to your Maximo with in-house built custom commands.</p>

    </div>
  </div>
</div>
<div  class = 'col-sm-6 col-md-4'>
<div  class = 'bordered icon-box  circled'>
    <div class="icon">
      <i class="fa fa-plus fa-fw"></i><i class="fa fa-plus fa-fw bg-icon"></i>
    </div>
    <div class="icon-box-body">
      
<div><h3>	Additional Benefits</h3></div><p class="rtejustify">Performance testing using real browsers. Sophisticated data loading.</p>

    </div>
  </div>
</div>
</div>
<div  class = 'row'>
</div>
<div  class = 'btn-container container'>
<a  class = 'btn  btn-success' href = '/product'><i class = "fa fa-lightbulb-o"></i>&nbsp; 
Learn More</a>
</div>
 
  </div> <!-- /.block -->
                
                              </div>
                                    
          
                      
          
                      
          
                      
          
        </div>

      </div>

    
      </section>
    


      <section  style="padding-top: 50px; " class="nd-region full-width-box">
  
    <div class="fwb-bg fwb-parallax fwb-fixed" style = "background-image: url('https://www.maxtaf.com/sites/default/files/partners_background.jpg');" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="200">
        
        </div>   

      <div class = "container-fluid">

        <div  class="row" id="Partners---Customers">     

          
                      
          
                      
          
                      
          
                                          <div  class="col-md-12 " id="views-33be84e4eb05bcc206c0a2ca1efbb0b3" data-animation="flipInX">
                              
                                  <div id="block-views-33be84e4eb05bcc206c0a2ca1efbb0b3" class="widget block block-views" >
  
      
  <div class="view view-customers-partners-carousel view-id-customers_partners_carousel view-display-id-block view-dom-id-d5bb792ae66066d3b64384f933e44449">
        
  
  
      <div class="owl-carousel-block44"><div class="item-0 item-odd">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="IBM" src="/sites/default/files/IBM-logo.svg" style="height: 40px;"></p>
 </div></div></div></div><div class="item-1 item-even">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="Vetasi" src="/sites/default/files/vetasi-logo.png" style="height: 75px;"></p>
 </div></div></div></div><div class="item-2 item-odd">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="Deloitte" src="/sites/default/files/Deloitte-logo.svg" style="height: 40px;"></p>
 </div></div></div></div><div class="item-3 item-even">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="ABBE" src="/sites/default/files/ABBE-logo.png" style="height: 75px;"></p>
 </div></div></div></div><div class="item-4 item-odd">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="MACS" src="/sites/default/files/macs_logo.png" style="height: 45px;"></p>
 </div></div></div></div><div class="item-5 item-even">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="Affinity Water" src="/sites/default/files/AffinityWater-logo.svg" style="height: 55px;"></p>
 </div></div></div></div><div class="item-6 item-odd">  
    <div class="field-content"><div class="owlcarousel-fields-186 disabled"><div class="item-0 item-odd"><p><img alt="JFC" src="/sites/default/files/jfc_logo.png" style="height: 55px;"></p>
 </div></div></div></div></div>  
  
  
  
  
  
</div>
  </div> <!-- /.block -->
                
                              </div>
                                    
          
                      
          
                      
          
        </div>

      </div>

    
      </section>
    


      <section  class="nd-region">
  
       

      <div class = "container">

        <div  class="row" id="Online-Trial">     

          
                      
          
                      
          
                      
          
                      
          
                                          <section  class="col-md-12 " id="block-51">
                              
                                  <div id="block-block-51" class="widget block block-block " >
  
      
  <div  class = 'row'>
<div  class = 'col-md-7' style = ' text-align:center;'>
<figure   style = 'height:300px' class = 'alignnone youtube'>
    <iframe src="//www.youtube.com/embed/fTrP1lW4Zck?showinfo=0&amp;wmode=opaque" frameborder="0" width="100%" height ="100%" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
  </figure>
</div>
<div  class = 'col-md-offset-1 col-lg-offset-1 col-md-4'>
<h2>Try It Out Now</h2><p>In just 3 easy steps run your first automated tests in front of you. Online trial requires an active website account. Registering an account also provides access to:</p><ul class="fancy-list">	<li>Basic and advance training courses</li><li>Test case library</li><li>Limited support</li></ul>
<a  class = 'btn  btn-success' href = '/user/register'><i class = "fa fa-key"></i>&nbsp; 
Create Account</a>
</div>
</div>
 
  </div> <!-- /.block -->
                
                              </section>
                                    
          
                      
          
        </div>

      </div>

    
      </section>
    


      <section  class="nd-region section-grey">
  
       

      <div class = "container-fluid">

        <div  class="row row-setting-open" id="Latest-Posts">     

          
                      
          
                      
          
                      
          
                      
          
                      
          
                      
          
        </div>

      </div>

    
      </section>
    


      <footer  class="nd-region">
  
       

      <div class = "container">

        <div  id="Footer" class="row">     

          
                      
          
                      
          
                      
          
                      
          
                      
          
                                          <div  id="footer" class="col-md-12 ">
                              
                                  <div id="block-block-60" class="widget block block-block col-md-12" >
  
      
  <div  id = 'element-wrap1991' style = 'position:relative; top:-150px;'></div>
<div  id = 'footer-links' class = 'row'>
<div  class = 'col-md-offset-3 col-md-2' style = ' text-align:center;'>
<div  style = ' text-align:center;'><div>	<h3>		<a href="/contact" style="color:white;">Contact Us</a></h3></div></div>
</div>
<div  class = 'col-md-2' style = ' text-align:center;'>
<div>	<h3>		<a href="/demo" style="color:white;">Arrange Demo</a></h3></div>
</div>
<div  style = 'color:black; text-align:center;' class = 'col-md-2'>
<div>	<h3>		<a href="/quote-request" style="color:white;">Quote Request</a></h3></div>
</div>
</div>
<hr  class = 'no-top-margin no-bottom-margin lg'/>
 
  </div> <!-- /.block -->
  <div id="block-block-61" class="widget block block-block col-md-8" >
  
      
  <div><h3>Subscribe To Newsletter</h3></div><!-- SendinBlue Signup Form HTML Code --><div id="sib_embed_signup">	<div class="forms-builder-wrapper">		<form class="description" id="theform" name="theform" action="https://my.sendinblue.com/users/subscribeembed/js_id/2qeok/id/1" method="POST">			<input type="hidden" name="js_id" id="js_id" value="2qeok">			<input type="hidden" name="listid" id="listid" value="10">			<input type="hidden" name="from_url" id="from_url" value="yes">			<input type="hidden" name="hdn_email_txt" id="hdn_email_txt" value="">			<input type="hidden" name="sib_simple" value="simple">						<input type="hidden" name="sib_forward_url" value="://www.maxtaf.com/newsletter/thank-you" id="sib_forward_url">			<div class="sib-container rounded">				<input type="hidden" name="req_hid" id="req_hid" value="">				<div class="view-messages"></div>				<!-- an email as primary -->				<div class="primary-group form-group email-group forms-builder-group ui-sortable">					<div class="mandatory-email">						<div class="lbl-tinyltr"><label for="email">EMail *</label></div>						<input required="" type="email" name="email" id="email" value="" class="form-control form-text required">					</div>				</div>				<div class="captcha forms-builder-group" style="display: none;">					<div style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(52, 52, 52); font-size: 17px; padding: 10px 20px;">						<div id="gcaptcha"></div>					</div>				</div>				<!-- end of primary -->				<div class="byline">					<button class="btn btn-primary" type="submit" data-editfield="subscribe">Subscribe</button>				</div>			</div>		</form>	</div></div> <!-- End : SendinBlue Signup Form HTML Code -->
<div  class = 'spacer'/></div>
<div>	<h3>		Connect With Us</h3></div><ul class="social-links social-links__dark"> <li><a target="_blank" href="//www.twitter.com/CodeDevLtd"><i class="fa fa-twitter"></i></a></li> <li><a target="_blank" href="//www.linkedin.com/company/code-development-ltd-"><i class="fa fa-linkedin"></i></a></li> 				 <li><a target="_blank" href="//www.youtube.com/channel/UCDo5oVErAsFmcXxhXUb3xLg"><i class="fa fa-youtube"></i></a></li> </ul>
 
  </div> <!-- /.block -->
  <div id="block-block-43" class="widget block block-block col-md-3 col-md-offset-1" >
  
      
  <div>	<h3>IBM Certified</h3></div>
<p>MaxTAF is certified for IBM Cloud &amp; Smarter Infrastructure</p><div><a href="http://www-304.ibm.com/partnerworld/gsd/solutiondetails.do?solution=48229&amp;expand=true&amp;lc=en" target="_blank"><img alt="IBM Business Partner" src="/sites/default/files/ibm_partner_csi_ready.svg" style="width: 179px; height: 150px;"></a></div>
 
  </div> <!-- /.block -->
                
                              </div>
                                    
          
        </div>

      </div>

    
      </footer>
    


    <div class="footer-copyright">
		Copyright &copy; 2019  <a href="http://www.codedevelopment.com">Code Development Ltd</a>&nbsp;|&nbsp;All Rights Reserved    </div>

  </div>
</div>  <script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__0xltv0qTC2femUE7Q2JlO9ww8ZyD7SoxJdSR-0Ju6wE__fCdEjuWjL922i0VBsFg_-utKWUpkKROMjL1rrWJP8bM__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-26242941-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__33K1GzFvIaUyJucCL0psOMFAVZLXnJsMQvBDW_QBwHc__HCm7YwX-mVzdo2WbRH2zhSGi-5R0n7Nv1_Kti6s471M__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__T1m5tTNyldmypBQqe9-wr1exv098b8wwRCxPItVWFnQ__6w-aswHMYuPB5rkDGtF03lSLQougKeuet9AgegcX7kw__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__jodKFQM46gwz-n-aVIeyw2buvfTQGlX9AOPNcZ2CMnA__dzwwBlvj80A6An4u1bwbNflX-7mwGUG8vKvaMNR6AAY__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__u0M2yYpUq-d1U36xDjS7fyFRN8z9h4DmMXfZL15zbaY__ZKo90oLEz6kFSExMuXWF1laPzlPO0o01XCSxjJmbLSs__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"stability_sub","theme_token":"02GhpF7PELGHysDLL-gmADdzBpFuwyAujqFfJKIh-oM","jquery_version":"1.8","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.messages.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"misc\/ui\/jquery.ui.button.css":1,"misc\/ui\/jquery.ui.resizable.css":1,"misc\/ui\/jquery.ui.dialog.css":1,"sites\/all\/libraries\/owl-carousel\/owl.carousel.css":1,"sites\/all\/libraries\/owl-carousel\/owl.theme.css":1,"sites\/all\/libraries\/owl-carousel\/owl.transitions.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/ubercart\/uc_file\/uc_file.css":1,"sites\/all\/modules\/ubercart\/uc_order\/uc_order.css":1,"sites\/all\/modules\/ubercart\/uc_product\/uc_product.css":1,"sites\/all\/modules\/ubercart\/uc_store\/uc_store.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/styles\/shCore.css":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/styles\/shThemeEclipse.css":1,"sites\/all\/modules\/nikadevs_cms\/css\/jquery.stellar.css":1,"sites\/all\/modules\/addtoany\/addtoany.css":1,"\/\/fonts.googleapis.com\/css?family=Anton|Muli:300,400,400italic,300italic|Goudy+Bookletter+1911|Oswald\u0026subset=latin,latin-ext":1,"\/\/maxcdn.bootstrapcdn.com\/font-awesome\/4.2.0\/css\/font-awesome.min.css":1,"sites\/all\/themes\/stability\/css\/bootstrap.min.css":1,"sites\/all\/themes\/stability\/vendor\/owl-carousel\/owl.carousel.css":1,"sites\/all\/themes\/stability\/vendor\/owl-carousel\/owl.theme.css":1,"sites\/all\/themes\/stability\/vendor\/magnific-popup\/magnific-popup.css":1,"sites\/all\/themes\/stability\/vendor\/mediaelement\/mediaelementplayer.css":1,"sites\/all\/themes\/stability\/vendor\/circliful\/css\/jquery.circliful.css":1,"sites\/all\/themes\/stability\/css\/theme.css":1,"sites\/all\/themes\/stability\/css\/theme-elements.css":1,"sites\/all\/themes\/stability\/css\/animate.min.css":1,"sites\/all\/themes\/stability\/css\/skins\/red.css":1,"sites\/all\/themes\/stability\/css\/skins\/green.css":1,"sites\/all\/themes\/stability\/stability_sub\/css\/custom.css":1},"js":{"sites\/all\/libraries\/owl-carousel\/owl.carousel.min.js":1,"sites\/all\/modules\/syntaxhighlighter\/syntaxhighlighter.min.js":1,"sites\/all\/modules\/owlcarousel\/includes\/js\/owlcarousel.settings.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.widget.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.button.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.mouse.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.draggable.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.position.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.resizable.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.dialog.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shCore.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shBrushBash.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shBrushJava.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shBrushPhp.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shBrushPlain.js":1,"sites\/all\/libraries\/syntaxhighlighter_3.0.83\/scripts\/shBrushXml.js":1,"sites\/all\/modules\/ubercart\/uc_file\/uc_file.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"misc\/progress.js":1,"sites\/all\/modules\/autodialog\/autodialog.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.retina.js":1,"sites\/all\/modules\/nikadevs_cms\/js\/jquery.stellar.min.js":1,"sites\/all\/themes\/stability\/vendor\/jquery-migrate-1.2.1.min.js":1,"sites\/all\/themes\/stability\/vendor\/bootstrap.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.flexnav.min.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.hoverIntent.minified.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.flickrfeed.js":1,"sites\/all\/themes\/stability\/vendor\/isotope\/jquery.isotope.min.js":1,"sites\/all\/themes\/stability\/vendor\/isotope\/jquery.isotope.sloppy-masonry.min.js":1,"sites\/all\/themes\/stability\/vendor\/isotope\/jquery.imagesloaded.min.js":1,"sites\/all\/themes\/stability\/vendor\/magnific-popup\/jquery.magnific-popup.js":1,"sites\/all\/themes\/stability\/vendor\/owl-carousel\/owl.carousel.min.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.fitvids.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.appear.js":1,"sites\/all\/themes\/stability\/vendor\/jquery.stellar.min.js":1,"sites\/all\/themes\/stability\/vendor\/snap.svg-min.js":1,"sites\/all\/themes\/stability\/vendor\/mediaelement\/mediaelement-and-player.min.js":1,"sites\/all\/themes\/stability\/vendor\/circliful\/js\/jquery.circliful.min.js":1,"sites\/all\/themes\/stability\/js\/custom.js":1}},"inEffects":["bounceIn","bounceInDown","bounceInUp","bounceInLeft","bounceInRight","fadeIn","fadeInUp","fadeInDown","fadeInLeft","fadeInRight","fadeInUpBig","fadeInDownBig","fadeInLeftBig","fadeInRightBig","flipInX","flipInY","foolishIn","lightSpeedIn","puffIn","rollIn","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","twisterInDown","twisterInUp","swap","swashIn","tinRightIn","tinLeftIn","tinUpIn","tinDownIn","vanishIn"],"outEffects":["bombRightOut","bombLeftOut","bounceOut","bounceOutDown","bounceOutUp","bounceOutLeft","bounceOutRight","fadeOut","fadeOutUp","fadeOutDown","fadeOutLeft","fadeOutRight","fadeOutUpBig","fadeOutDownBig","fadeOutLeftBig","fadeOutRightBig","flipOutX","flipOutY","foolishOut","hinge","holeOut","lightSpeedOut","puffOut","rollOut","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","rotateDown","rotateUp","rotateLeft","rotateRight","swashOut","tinRightOut","tinLeftOut","tinUpOut","tinDownOut","vanishOut"],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"stability":{"flickr_id":"","logo_sticky":"50"},"owlcarousel":{"owlcarousel-fields-186":{"settings":{"items":5,"itemsDesktop":["1199",4,"1199",4,"1199",4,"1199",4,"1199",4,"1199",4,"1199",4],"itemsDesktopSmall":["979",3,"979",3,"979",3,"979",3,"979",3,"979",3,"979",3],"itemsTablet":["768",2,"768",2,"768",2,"768",2,"768",2,"768",2,"768",2],"itemsTabletSmall":["0",0,"0",0,"0",0,"0",0,"0",0,"0",0,"0",0],"itemsMobile":["479",1,"479",1,"479",1,"479",1,"479",1,"479",1,"479",1],"singleItem":false,"itemsScaleUp":false,"slideSpeed":200,"paginationSpeed":800,"rewindSpeed":1000,"autoPlay":"5000","stopOnHover":false,"navigation":true,"navigationText":["\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E"],"rewindNav":true,"scrollPerPage":false,"pagination":true,"paginationNumbers":false,"responsive":true,"responsiveRefreshRate":200,"baseClass":"owl-carousel","theme":"owl-theme","lazyLoad":false,"lazyFollow":true,"lazyEffect":"fadeIn","autoHeight":false,"jsonPath":false,"jsonSuccess":false,"dragBeforeAnimFinish":true,"mouseDrag":true,"touchDrag":true,"addClassActive":false,"transitionStyle":false},"views":{"ajax_pagination":0}},"owl-carousel-block44":{"settings":{"items":5,"itemsDesktop":["1199",4],"itemsDesktopSmall":["979",3],"itemsTablet":["768",2],"itemsTabletSmall":["0",0],"itemsMobile":["479",1],"singleItem":false,"itemsScaleUp":false,"slideSpeed":200,"paginationSpeed":800,"rewindSpeed":1000,"autoPlay":"5000","stopOnHover":false,"navigation":true,"navigationText":["\u003Ci class=\u0022fa fa-arrow-left\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E","\u003Ci class=\u0022fa fa-arrow-right\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E"],"rewindNav":true,"scrollPerPage":false,"pagination":true,"paginationNumbers":false,"responsive":true,"responsiveRefreshRate":200,"baseClass":"owl-carousel","theme":"owl-theme","lazyLoad":false,"lazyFollow":true,"lazyEffect":"fadeIn","autoHeight":false,"jsonPath":false,"jsonSuccess":false,"dragBeforeAnimFinish":true,"mouseDrag":true,"touchDrag":true,"addClassActive":false,"transitionStyle":false},"views":{"ajax_pagination":0}}},"better_exposed_filters":{"views":{"customers_partners_carousel":{"displays":{"block":{"filters":[]}}}}}});
//--><!]]>
</script>
  <!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3359870.js"></script>
<!-- End of HubSpot Embed Code --><script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__Cc00cuDocLsDXk33-LQX1_cmb7v04nqWu_AoT76aJOc__sYgBjTrSNpXAPbfhfzy0MBJ0DKcJC-5j_PAKUzCGgb4__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__udYswusRJfxeT25ypO5REGt6o4udW-I7WIBuyMxYD-k__U5MPDvIHq8ll8jrN14s4TYt9oxpoLbDJ9B7FpOQ3Rhw__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript" src="https://www.maxtaf.com/sites/default/files/advagg_js/js__GS3PIS5MQhE5a8ywHZkZG5u4zuWhCm7uzQmfBFtp26U__nv9a2oHudyNYMZvU4EJJ3uYXj9tL5iJgP9KtRXtaSYE__KFRWmW62r6zZeaIA8y6mxWPAyF3uOH4G0SPoah0Y-w0.js"></script>
<script type="text/javascript">
_linkedin_partner_id = "60610";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=60610&fmt=gif" />
</noscript>


  
</body>
</html>