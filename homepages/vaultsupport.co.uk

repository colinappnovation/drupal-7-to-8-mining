<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.vaultsupport.co.uk/sites/default/files/vault-support-favicon.png" type="image/png" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.vaultsupport.co.uk/" />
<link rel="shortlink" href="https://www.vaultsupport.co.uk/" />
  <title>Vault Support |</title>
  <style>
@import url("https://www.vaultsupport.co.uk/modules/system/system.base.css?p7v5f2");
</style>
<style>
@import url("https://www.vaultsupport.co.uk/modules/field/theme/field.css?p7v5f2");
@import url("https://www.vaultsupport.co.uk/modules/node/node.css?p7v5f2");
@import url("https://www.vaultsupport.co.uk/sites/all/modules/views/css/views.css?p7v5f2");
</style>
<style>
@import url("https://www.vaultsupport.co.uk/sites/all/modules/ctools/css/ctools.css?p7v5f2");
@import url("https://www.vaultsupport.co.uk/sites/all/modules/panels/css/panels.css?p7v5f2");
</style>
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.css" media="all" />
<style>
@import url("https://www.vaultsupport.co.uk/sites/all/themes/bootstrap/css/3.3.5/overrides.min.css?p7v5f2");
@import url("https://www.vaultsupport.co.uk/sites/all/themes/bootstrap_vaultsupport/css/style.css?p7v5f2");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="https://www.vaultsupport.co.uk/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script src="https://www.vaultsupport.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.vaultsupport.co.uk/misc/drupal.js?p7v5f2"></script>
<script src="//cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.js"></script>
<script src="https://www.vaultsupport.co.uk/sites/all/modules/admin_menu/admin_devel/admin_devel.js?p7v5f2"></script>
<script src="https://www.vaultsupport.co.uk/misc/tableheader.js?p7v5f2"></script>
<script src="https://www.vaultsupport.co.uk/sites/all/modules/server_monitor/js/server_monitor.js?p7v5f2"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap_vaultsupport","theme_token":"sJZThgeD5l7niRvR_GfnCHXmxXcf5iOMiuNqajQj_po","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.5\/js\/bootstrap.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"misc\/tableheader.js":1,"sites\/all\/modules\/server_monitor\/js\/server_monitor.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.5\/css\/bootstrap.css":1,"sites\/all\/themes\/bootstrap\/css\/3.3.5\/overrides.min.css":1,"sites\/all\/themes\/bootstrap_vaultsupport\/css\/style.css":1}},"urlIsAjaxTrusted":{"\/":true},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-faq-new-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <header id="navbar" role="banner" class="navbar container navbar-default">
  <div class="container">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="https://www.vaultsupport.co.uk/sites/default/files/vault-support-logo.png" alt="Home" />
        </a>
      
              <a class="name navbar-brand" href="/" title="Home">Vault Support</a>
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first last leaf active"><a href="/" class="active">Home</a></li>
</ul>                                      </nav>
      </div>
      </div>
</header>

<div class="main-container container">

  <header role="banner" id="page-header">
    
      <div class="region region-header">
    <section id="block-block-2" class="block block-block clearfix">

        <h2 class="block-title">Welcome to the Vault Support Website</h2>
    
  <p>The Vault Support website provides you with the information you need to use the vault.Each category contains specific frequently asked questions to help you perform common tasks on the Vault. The questions are in multiple categories to help you find exactly what you need quickly.</p>

</section>
  </div>
  </header> <!-- /#page-header -->

  <div class="row">

          <aside class="col-sm-3" role="complementary">
          <div class="region region-sidebar-first well">
    <section id="block-server-monitor-server-monitor-status" class="block block-server-monitor clearfix">

      
  <div class="table-responsive">
<table id="server-monitor-status-table" class="table table-hover table-striped sticky-enabled">
 <thead><tr><th>Service</th><th>Status</th> </tr></thead>
<tbody>
 <tr><td colspan="2">Loading, please wait...</td> </tr>
</tbody>
</table>
</div>

</section>
<section id="block-search-form" class="block block-search clearfix">

      
  <form class="form-search content-search" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div>
      <h2 class="element-invisible">Search form</h2>
    <div class="input-group"><input title="Enter the terms you wish to search for." placeholder="Search" class="form-control form-text" type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" /><span class="input-group-btn"><button type="submit" class="btn btn-primary"><span class="icon glyphicon glyphicon-search" aria-hidden="true"></span></button></span></div><div class="form-actions form-wrapper form-group" id="edit-actions"><button class="element-invisible btn btn-primary form-submit" type="submit" id="edit-submit" name="op" value="Search">Search</button>
</div><input type="hidden" name="form_build_id" value="form-5LyU8S2JrkLv4Z1b-Bmu994PvKqfB7UHM9FVpmPxo9Y" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>
</section>
<section id="block-faq-faq-categories" class="block block-faq clearfix">

        <h2 class="block-title">FAQ Categories</h2>
    
  <ul><li><a href="/faq-page/4">Reports</a></li>
<li><a href="/faq-page/7">Report Categories</a></li>
<li><a href="/faq-page/6">Client Management</a></li>
<li><a href="/faq-page/5">User Manager</a></li>
</ul>
</section>
<section id="block-views-faq-faq-recent" class="block block-views clearfix">

        <h2 class="block-title">Recent FAQs</h2>
    
  <div class="view view-faq view-id-faq view-display-id-faq_recent view-dom-id-3d2ce092d537f234219d2940cd62ad64">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/deleting-user">Deleting a User</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/adding-user">Adding a User</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/user-basics">User Basics</a></span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/deleting-category">Deleting a Category</a></span>  </div></li>
          <li class="views-row views-row-5 views-row-odd views-row-last">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/editing-category">Editing a Category</a></span>  </div></li>
      </ul></div>    </div>
  
  
  
      
<div class="more-link">
  <a href="/faq-questions-inline">
    View all FAQs  </a>
</div>
  
  
  
</div>
</section>
  </div>
      </aside>  <!-- /#sidebar-first -->
    
    <section class="col-sm-9">
                  <a id="main-content"></a>
                    <h1 class="page-header">FAQ</h1>
                                                          <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div class="view view-faq view-id-faq view-display-id-page_2 view-dom-id-2c8000f0a2afaccb19ae2f10dc83f351">
        
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/deleting-user">Deleting a User</a></span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/adding-user">Adding a User</a></span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/user-basics">User Basics</a></span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/deleting-category">Deleting a Category</a></span>  </div></li>
          <li class="views-row views-row-5 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/editing-category">Editing a Category</a></span>  </div></li>
          <li class="views-row views-row-6 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/adding-category">Adding a Category</a></span>  </div></li>
          <li class="views-row views-row-7 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/category-basics">Category Basics</a></span>  </div></li>
          <li class="views-row views-row-8 views-row-even">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/deleting-client">Deleting a Client</a></span>  </div></li>
          <li class="views-row views-row-9 views-row-odd">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/adding-client">Adding a Client</a></span>  </div></li>
          <li class="views-row views-row-10 views-row-even views-row-last">  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/content/client-basics">Client Basics</a></span>  </div></li>
      </ul></div>    </div>
  
      <div class="text-center"><ul class="pagination"><li class="active"><span>1</span></li>
<li><a title="Go to page 2" href="/faq-new-page?page=1">2</a></li>
<li class="next"><a title="Go to next page" href="/faq-new-page?page=1">next ›</a></li>
<li class="pager-last"><a title="Go to last page" href="/faq-new-page?page=1">last »</a></li>
</ul></div>  
  
      
<div class="more-link">
  <a href="/faq-new-page">
    View all FAQs  </a>
</div>
  
  
  
</div>
</section>
  </div>
    </section>

    
  </div>
</div>

  <footer class="footer container">
      <div class="region region-footer">
    <section id="block-block-1" class="block block-block clearfix">

      
  <p>Vault Support last updated 10-01-2017</p>

</section>
  </div>
  </footer>
  <script src="https://www.vaultsupport.co.uk/sites/all/themes/bootstrap/js/bootstrap.js?p7v5f2"></script>
</body>
</html>
