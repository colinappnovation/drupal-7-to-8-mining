<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.middleton-bespoke.co.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, user-scalable=no" />
<link rel="icon" href="/favicon.png" type="image/x-icon" />
<meta name="description" content="Middleton bespoke designs and creates english kitchens and furniture, handmade in the Sussex countryside, by a family of craftsmen." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.middleton-bespoke.co.uk/" />
<link rel="shortlink" href="https://www.middleton-bespoke.co.uk/" />
  <title>Middleton Bespoke | Handcrafted Bespoke Kitchens</title>
  <link type="text/css" rel="stylesheet" href="https://www.middleton-bespoke.co.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.middleton-bespoke.co.uk/sites/default/files/css/css_uzfgJ6Ztr3jIC8GM4US5tJtU8tqni68wyDuJ7xMtod0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.middleton-bespoke.co.uk/sites/default/files/css/css_9w7vzcilUJdJK9gXlf9JZ2k4YrwLVgshULLiUAM-iTw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.middleton-bespoke.co.uk/sites/default/files/css/css_Baxdq5YZj-QSD0TdrjIC0GZ3tn8JqYreaZPAJ4Pzadk.css" media="all" />
  <script type="text/javascript" src="https://www.middleton-bespoke.co.uk/sites/default/files/js/js_-HJHFkJrdW5OFPmTAfaBzz6a9W6vZN0ve8N60lpZEWs.js"></script>
<script type="text/javascript" src="https://www.middleton-bespoke.co.uk/sites/default/files/js/js_4nhl8AvkZMcGDJR36vWPmvdZI0wz6VphaW_-5NM1FHQ.js"></script>
<script type="text/javascript" src="https://www.middleton-bespoke.co.uk/sites/default/files/js/js_o7BSE6sskbTBWp8zXx_VIDqeo8WNuewSnZwefWs60w8.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"middleton","theme_token":"ilTrl1G0M1yBK49BMIMEbRIub1n6A8mjspaROp8tE9g","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/textarea.js":1,"sites\/all\/modules\/webform\/js\/webform.js":1,"sites\/all\/themes\/middleton\/scripts\/jquery.js":1,"sites\/all\/themes\/middleton\/scripts\/vendor.js":1,"sites\/all\/themes\/middleton\/scripts\/global.js":1,"sites\/all\/themes\/middleton\/scripts\/google-analytics.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/asset\/css\/assets.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/wysiwyg_ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/field_collection\/field_collection.theme.css":1,"sites\/all\/modules\/webform\/css\/webform.css":1,"sites\/all\/themes\/middleton\/styles\/main.css":1,"sites\/all\/themes\/middleton\/styles\/media.css":1,"sites\/all\/themes\/middleton\/styles\/vendor.css":1}},"ckeditor":{"modulePath":"sites\/all\/modules\/wysiwyg_ckeditor"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-home page-home" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
<script type='application/ld+json'>
{
    "@context": "http://www.schema.org",
    "@type": "LocalBusiness",
    "name": "Middleton Bespoke",
    "url": "https://www.middleton-bespoke.co.uk",
    "image":"https://www.middleton-bespoke.co.uk/sites/default/files/mbr002_2.jpg",
    "logo": "https://www.middleton-bespoke.co.uk/sites/all/themes/middleton/images/middleton-bespoke-logo.png",
    "description": "Middleton bespoke designs and creates english kitchens and furniture, handmade in the Sussex countryside, by a family of craftsmen.",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "Tidey's Barn, Three Cornered Platt, Cowfold Road",
        "addressLocality": "West Grinstead",
        "addressRegion": "West Sussex",
        "postalCode": "RH13 8LY",
        "addressCountry": "United Kingdom"
    },
    "hasMap": "https://goo.gl/maps/VTKY4DJ1H7A2",
    "openingHours": "Mo, Tu, We, Th, Fr 09:00-17:00 Sa 10:00-14:00",
    "contactPoint": {
        "@type": "ContactPoint",
        "telephone": "+441403 864446",
        "contactType":"customer service"
    }
}
</script>

<div class="masthead">
	<div class="top-bar">
		<div class="container">
			<div class="site-logo">
				<a href="/" title=""><img src="/sites/all/themes/middleton/images/logo-middleton-bespoke.png" /></a>
			</div>
			<ul class="contact-info-list">
				<li class="contact-info contact-phone"><a href="tel:+441403864446" title="Contact us">01403 864446</a></li>
				<li class="contact-info contact-email"><a href="/contact" title="Contact us">Contact us</a></li>
			</ul>
		</div>
	</div>
	<div class="main-nav">
		<div class="bg-split-nav"></div>
		<div class="container">
			<ul class="user-request-buttons">
				<li class="user-request-brochure"><a href="https://www.middleton-bespoke.co.uk/request-a-brochure" title="">Request a<br />brochure</a></li>
				<li class="user-request-consultation"><a href="https://www.middleton-bespoke.co.uk/design-consultation" title="">Free design<br />consultation</a></li>
			</ul>
	
	        <button id="trigger-nav" type="button" role="button" aria-label="Toggle Navigation" class="lines-button minus">
	          <span class="lines"></span>
	          <span class="trigger-menu"></span>
	        </button>
	        
			<nav>
				<ul class="main-nav-items"><li><a href='/about/our-story'>About Us</a></li>
<li><a class='has-sub ' href='/our-work'>Our Kitchens<img src='https://www.middleton-bespoke.co.uk/sites/all/themes/middleton/images/icon-arrow-nav.png' width='16' height='10' class='active' /></a><ul class="main-nav-items"><li><a href='/category/classic'>Classic</a></li>
<li><a href='/category/country'>Country</a></li>
<li><a href='/category/modern'>Modern</a></li>
<li><a href='/category/aga'>Aga</a></li>
</ul></li>
<li><a href='/about/our-craft'>Our Craft</a></li>
<li><a href='/showroom'>Showroom</a></li>
<li><a href='/praise'>Testimonials</a></li>
<li><a href='/be-inspired'>Blog</a></li>
</ul>			</nav>
		</div>
	</div>
</div>








<div class="header-shadow"></div>
<!-- END => UNIVERSAL HEADER / NAV -->

<script>
$(document).ready(function() {
  $('.expanded').click();
});
</script>

  <div class="region region-content">
    
<div class="field-collection-container clearfix">
<div class="banner-main slider">
  <ul class="bxslider">
        <li id="0"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/middleton-bespoke-white-kitchen.jpg'  /></li>
        <li id="1"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/middleton-bespoke-dark-blue-kitchen.jpg'  /></li>
        <li id="2"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/jasper-gemma-max-sitting-2800x1200.jpg'  /></li>
        <li id="3"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/middleton-bespoke-green-kitchen.jpg'  /></li>
        <li id="4"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/max-handcrafting-workshop-black-and-white-2800x1200.jpg'  /></li>
      </ul>
      <div class="logo-banner"><img src='https://www.middleton-bespoke.co.uk/sites/all/themes/middleton/images/middleton-bespoke-logo.png' alt="Middleton - Handmade Bespoke English Kitchens & Furniture" width="396" height="160" /></div>
    <span class="scroll-down">
    	Scroll down
    	<img src="/sites/all/themes/middleton/images/arrow-gold.png" alt="" />
    </span>
  </div>

<script>
$(document).ready(function(){
	$('.bxslider').bxSlider({	
		auto: true,
                                 mode:'fade',
		infiniteLoop: ($(".bxslider li").length > 1) ? true: false,
		touchEnabled: ($(".bxslider li").length > 1) ? true: false,
		controls: false,
                            pause:6000,
                            speed:800,
		responsive: true,
		pager: true
	});
});
</script>
</div>

<div id="marquee" class="main texture-brown">
	<div class="edge edge-brown-top"></div>  
	<div class="edge edge-brown-bottom"></div>  
	<ul class="marquee-slider">
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-the-english-home_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-beautiful-kitchens-new.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-country-homes_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-essential-kitchens_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/thesundaytimes.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-grand-designs-white.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-living-etc.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-homes-and-gardens.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/elle-logo-white.png' alt="" /></li>
			</ul>
</div>

<div class="field-collection-container clearfix">

<div class="main texture-grey">
	<div class="edge edge-grey-top"></div>
	<div class="inner intro page-title">
          <h1>BESPOKE KITCHENS HANDCRAFTED IN GREAT BRITAIN</h1>
        
              	<p>At Middleton our aim is simple, to create spaces to cook, live, enjoy and make
memories in, that will last a lifetime. Kitchens are where we retreat to, where good food is made and
eaten, where wine and conversation flows, where delicious smells fill the air, where laughter echoes, where our children create masterpieces, where we have our first coffee of the day watching the sunrise, the hearts of our homes.</p>
              	<p>With a 2017 New Year Honour from 'The English Home' for our work and a Design Award shortlisting for 'The Kitchen, Bedroom and Bathroom Awards, 2017' get in touch and find out what’s so special about a Middleton kitchen. </p>
              	<p>We create unique, handmade Great British kitchens for homes across London, Surrey, Sussex and all over the United Kingdom. Our beautiful, handmade, bespoke kitchens start from £25,000



</p>
                  
        	</div>
	<div class="edge edge-grey-bottom"></div>
</div>







</div>
<div class="main texture-dark-grey">
	<div class="block-title">
		<h1>The collection</h1>
	</div>
</div>

<ul class="category-blocks">

    
    <li>
        <a href="/category/classic">
            <img src='https://www.middleton-bespoke.co.uk/sites/default/files/classic-kitchen-650x650-new.jpg'  />            <div class="category-block-name"><span>Classic</span></div>
        </a>
    </li>


    
    <li>
        <a href="/category/country">
            <img src='https://www.middleton-bespoke.co.uk/sites/default/files/country-kitchen-thumbnail-650x650.jpg'  />            <div class="category-block-name"><span>Country</span></div>
        </a>
    </li>


    
    <li>
        <a href="/category/modern">
            <img src='https://www.middleton-bespoke.co.uk/sites/default/files/modern.jpg'  />            <div class="category-block-name"><span>Modern</span></div>
        </a>
    </li>


    
    <li>
        <a href="/category/aga">
            <img src='https://www.middleton-bespoke.co.uk/sites/default/files/aga-kitchen-thmbnail-650x650.jpg'  />            <div class="category-block-name"><span>Aga</span></div>
        </a>
    </li>

<ul>
<div class="clearfix"></div>

<div class="field-collection-container clearfix">
<div class="main">	
	<div class="inner intro">
            
                    <h1>Visit our Showroom</h1>

<p>Within easy reach of London, Surrey and Brighton why not take a trip to our inspiring showroom barn in the beautiful Sussex countryside where you can touch, feel and see our kitchens in action. Our <a href="http://www.middleton-bespoke.co.uk/showroom">showroom barn</a> is a must see for anyone looking to commission a truly made to measure kitchen.&nbsp;</p>

<p>Our tailored approach to design begins with understanding your dream.&nbsp;Let us craft your bespoke kitchen from concept to reality. With our bespoke kitchen design service we can create your vision.</p>

<p>We can take care of all aspects of kitchen design, from high-end stone and wood flooring to luxury appliances. We even advise on all the little extras such as colours, lighting and soft furnishings.&nbsp;With a comprehensive unique service that&rsquo;s so much more than just a kitchen, <a href="http://www.middleton-bespoke.co.uk/contact">contact us</a>&nbsp;today to begin the conversation.</p>

<p>&nbsp;</p>
        
            	</div>
</div>
</div>  </div>


<!--
/*

<div id="marquee" class="main texture-brown">
	<div class="edge edge-brown-top"></div>  
	<ul class="marquee-slider">
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-the-english-home_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-beautiful-kitchens-new.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-country-homes_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-essential-kitchens_0.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/thesundaytimes.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-grand-designs-white.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-living-etc.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/logo-homes-and-gardens.png' alt="" /></li>
				<li><img src='https://www.middleton-bespoke.co.uk/sites/default/files/elle-logo-white.png' alt="" /></li>
			</ul>
</div>
*/
-->

<!-- CTA -->
<div class="cta-strip" style="margin-bottom:0px; ">
<a class="consulation-check" href="https://www.middleton-bespoke.co.uk/contact" title="Free design consultation">
<span>

<p><img src="https://www.middleton-bespoke.co.uk/sites/all/themes/middleton/images/Icon@3x_phone.svg">Speak to a friendly team member on 01403 864 446 or Contact us online <span style="padding-left:5px;"><img src="https://www.middleton-bespoke.co.uk/sites/all/themes/middleton/images/Icon@3x_email.svg" alt=""></span></p>
</span>
</a>
</div>



<!-- REQUEST FORM -->
<!--
<div class="request-block">
<div class="edge edge-green-top"></div>  
			
	<img class="brochure-preview" src="/sites/all/themes/middleton/images/brochure.png">
		
	<div class="inner" id="request-form">
	
		<form id="brochure-consultation-form">
		
			<div class="form-block">
				<h2>Request a brochure</h2>
				
				<p>Would you like to request your <strong>free brochure?</strong></p>
				<ul class="radio-list">	
					<li>	
						<input type="radio" name="request_brochure" id="request_brochure_no" class="radio" value="No"/>
						<label for="request_brochure_no">No, thank you</label>
					</li>
					<li>	
						<input type="radio" name="request_brochure" id="request_brochure_yes" class="radio" value="Yes"/>
						<label for="request_brochure_yes">Yes, please</label>
					</li>
				</ul>
			</div>
			
			<div class="form-block">
				<h2>Free design consultation</h2>
				
				<p>Would you like to arrange a <strong>free design consultation?</strong></p>
				<ul class="radio-list">	
					<li>	
						<input type="radio" name="request_consultation" id="request_consultation_no" class="radio" value="No"/>
						<label for="request_consultation_no">No, thank you</label>
					</li>
					<li>	
						<input type="radio" name="request_consultation" id="request_consultation_yes" class="radio" value="Yes"/>
						<label for="request_consultation_yes">Yes, please</label>
					</li>
				</ul>
				
                                <div style="display: none;" id="date-required">
                                    <p class="date-select" style=""><span>Your preferred date:</span> <input type="text" id="datepicker" name="consultation_date"> 				<img class="date-pick-cal" src="/sites/all/themes/middleton/images/icon-calendar.png" width="15" height="13" class=""></p>
                                    
                                    <p class="form-note">Please select your preferred date and we'll be in touch to confirm</p>
                                </div>
			</div>
			
			<div class="form-block">
				<ul>
					<li><input type="text" name="contact_fullname" placeholder="*Full name" /></li>
					<li><input type="text" name="contact_postcode" placeholder="*Postcode" /><button id="postcode-enter" type="button" class="btn-postcode" onclick="return false">Find address</button></li>
                    <li id="lookup-field"></li>
                    <li class="enter-add-manual">Click here to enter address manually</li>                    
					<li class="add-extra"><input type="text" name="contact_address_one" placeholder="*House Name/Number" /></li>
					<li class="add-extra"><input type="text" name="contact_address_two" placeholder="Address Line One" /></li>
					<li class="add-extra"><input type="text" name="contact_address_three" placeholder="Address Line Two" /></li>
					<li class="add-extra"><input type="text" name="contact_town" placeholder="*Town/City" /></li>
					<li><input type="text" name="contact_number" placeholder="*Contact no." /></li>
					<li><input type="text" name="contact_email" placeholder="*Email address" /></li>
					<li><textarea rows="6" name="contact_message" placeholder="Leave a message (Optional)" /></textarea></li>
				</ul>	
				
				<p class="form-note">*Required</p>
			</div>
			
			<div class="form-block">
				<input class="btn btn-block btn-white" type="submit" value="Submit">
			</div>
			
		</form>
	
	</div>
</div>
-->


<!-- UNIVERSAL FOOTER -->
<footer>
	<div class="main texture-blue">
		<div class="inner clearfix">
			
			<div class="col-1 contact-info">
		        <p>Tideys Barn, Three Cornered Platt.</p>
		        <p class="footer-spacer">Cowfold Road, West Grinstead, West Sussex, RH13 8LY</p>
		        <a href="/contact" title="">Find us on the map</a>
			</div>
			
			<div class="col-1 contact-info">
				<ul>
					<li><span>T</span> 01403 864446</li>
					<li class="footer-spacer"><span>E</span> <a href="/contact" title="">mail@middleton-bespoke.co.uk</a></li>
					<li>Showroom opening hours:<br /> 
					Monday-Friday 9am-5pm<br />
					Saturday 10am-2pm (by appointment)</li>
				</ul>
			</div>
			
			<div class="col-3 newsreg">
		        <ul class="social-icons">
										<li><a href="https://www.facebook.com/middletonsussex/" title="Facebook" target="_blank"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/icon-facebook_0.png' alt="Facebook" height="20" width="20" /></a></li>
										<li><a href="https://twitter.com/MiddletonSussex" title="Twitter" target="_blank"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/icon-twitter_0.png' alt="Twitter" height="20" width="20" /></a></li>
										<li><a href="https://www.instagram.com/middletonbespoke/" title="Instagram" target="_blank"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/icon-instagram_0.png' alt="Instagram" height="20" width="20" /></a></li>
										<li><a href="https://www.pinterest.com/middbespoke/" title="Pinterest" target="_blank"><img src='https://www.middleton-bespoke.co.uk/sites/default/files/icon-pinterest_0.png' alt="Pinterest" height="20" width="20" /></a></li>
							        </ul>

				<div class="footnotes">
					<a href="/privacy" title="Privacy">Privacy</a>
					<a class="stereo" href="" title=""><img src="/sites/all/themes/middleton/images/stereo.png" alt="" width="61" height="16" /></a>
				</div>
				
			</div>
			
		</div>
	</div>
</footer>
<!-- END => UNIVERSAL FOOTER -->

<div style="display: none;">
<form class="webform-client-form webform-client-form-81" action="/" method="post" id="webform-client-form-81" accept-charset="UTF-8"><div><div class="webform-progressbar">
      <div class="webform-progressbar-outer">
      <div class="webform-progressbar-inner" style="width: 0%">&nbsp;</div>
              <span class="webform-progressbar-page current" style="left: 0%">
          <span class="webform-progressbar-page-number">1</span>
                    <span class="webform-progressbar-page-label">
            Start          </span>
                  </span>
              <span class="webform-progressbar-page" style="left: 100%">
          <span class="webform-progressbar-page-number">2</span>
                    <span class="webform-progressbar-page-label">
            Complete          </span>
                  </span>
          </div>
  

  
  </div>
<div  class="form-item webform-component webform-component-select webform-component--request-brochure">
  <label for="edit-submitted-request-brochure">Request Brochure <span class="form-required" title="This field is required.">*</span></label>
 <select required="required" id="edit-submitted-request-brochure" name="submitted[request_brochure]" class="form-select required"><option value="" selected="selected">- Select -</option><option value="No">No</option><option value="Yes">Yes</option></select>
</div>
<div  class="form-item webform-component webform-component-select webform-component--request-consultation">
  <label for="edit-submitted-request-consultation">Request Consultation <span class="form-required" title="This field is required.">*</span></label>
 <select required="required" id="edit-submitted-request-consultation" name="submitted[request_consultation]" class="form-select required"><option value="" selected="selected">- Select -</option><option value="No">No</option><option value="Yes">Yes</option></select>
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--preferred-date">
  <label for="edit-submitted-preferred-date">Preferred Date </label>
 <input type="text" id="edit-submitted-preferred-date" name="submitted[preferred_date]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--full-name">
  <label for="edit-submitted-full-name">Full Name </label>
 <input type="text" id="edit-submitted-full-name" name="submitted[full_name]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--address-line-1">
  <label for="edit-submitted-address-line-1">Address Line 1 </label>
 <input type="text" id="edit-submitted-address-line-1" name="submitted[address_line_1]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--address-line-2">
  <label for="edit-submitted-address-line-2">Address Line 2 </label>
 <input type="text" id="edit-submitted-address-line-2" name="submitted[address_line_2]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--address-line-3">
  <label for="edit-submitted-address-line-3">Address Line 3 </label>
 <input type="text" id="edit-submitted-address-line-3" name="submitted[address_line_3]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--town-city">
  <label for="edit-submitted-town-city">Town / City </label>
 <input type="text" id="edit-submitted-town-city" name="submitted[town_city]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--postcode">
  <label for="edit-submitted-postcode">Postcode </label>
 <input type="text" id="edit-submitted-postcode" name="submitted[postcode]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-textfield webform-component--contact-number">
  <label for="edit-submitted-contact-number">Contact Number </label>
 <input type="text" id="edit-submitted-contact-number" name="submitted[contact_number]" value="" size="60" maxlength="128" class="form-text" />
</div>
<div  class="form-item webform-component webform-component-email webform-component--email-address">
  <label for="edit-submitted-email-address">Email Address </label>
 <input class="email form-text form-email" type="email" id="edit-submitted-email-address" name="submitted[email_address]" size="60" />
</div>
<div  class="form-item webform-component webform-component-textarea webform-component--message">
  <label for="edit-submitted-message">Message </label>
 <div class="form-textarea-wrapper resizable"><textarea id="edit-submitted-message" name="submitted[message]" cols="60" rows="5" class="form-textarea"></textarea></div>
</div>
<input type="hidden" name="details[sid]" />
<input type="hidden" name="details[page_num]" value="1" />
<input type="hidden" name="details[page_count]" value="1" />
<input type="hidden" name="details[finished]" value="0" />
<input type="hidden" name="form_build_id" value="form-0de3-cRugdqcND48I7cV1bQ7TuFZ2MpQ2VdxXmHLCJE" />
<input type="hidden" name="form_id" value="webform_client_form_81" />
<div class="form-actions"><input class="webform-submit button-primary form-submit" type="submit" name="op" value="Submit" /></div></div></form></div>

<script>
    // jQuery UI Datepicker
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>

<script>
    // validation
    $("#brochure-consultation-form").validate({
        rules: {
            request_brochure: "required",
            request_consultation: "required",
            consultation_date: {
                required: {
                    depends: function(element) {
                        return $('#request_consultation_yes:checked').length;
                    }
                }
            },
            contact_fullname: "required",
            contact_postcode: "required",
            contact_address_one: "required",
            contact_town: "required",
            contact_number: {
		      	required: true,
			  	digits: true	            
            },
		    contact_email: {
		      	required: true,
			  	email: true
		    }
        },
        messages: {
            contact_fullname: "Please enter your full name",
            contact_postcode: "Please enter your postcode",
            contact_address_one: "Please enter the first line of your address",
            contact_town: "Please enter your town",
            contact_number: {
		      	required: "Please enter a contact number",
			  	digits: "Please enter digits only"	            
            },
		    contact_email: {
		      	required: "Please enter your email address",
			  	email: "Please enter a valid email address"
		    }
        },
        submitHandler: function(form) {

            var realForm = $("#webform-client-form-81");

            var fullname = $('input[name=contact_fullname]').val();
            var addr1 = $('input[name=contact_address_one]').val()
            var addr2 = $('input[name=contact_address_two]').val()
            var addr3 = $('input[name=contact_address_three]').val()
            var towncity = $('input[name=contact_town]').val()
            var postcode = $('input[name=contact_postcode]').val()
            var number = $('input[name=contact_number]').val()
            var email = $('input[name=contact_email]').val()

            var payload = {
                'EMAIL': email,
                'NAME': fullname,
                'PHONE': number,
                'ADDR1': addr1,
                'ADDR2': addr2,
                'ADDR3': addr3,
                'TOWNCITY': towncity,
                'POSTCODE': postcode,
            };

            // submit to mailchimp
            $.ajax({
                url: 'https://middleton-bespoke.us9.list-manage.com/subscribe/post-json?u=4adfc172744ba385edae7492c&id=d7f7927f5c',
                dataType: 'jsonp',
                jsonp: 'c',
                data: payload,
                method: 'POST',
                complete: function(xhr, status) {

                    //console.log(payload, xhr, status);

                    // mirror values onto real form
                    var rb = $('input[name=request_brochure]:checked');
                    realForm.find("#edit-submitted-request-brochure").val(rb.val());

                    var rc = $('input[name=request_consultation]:checked');
                    realForm.find("#edit-submitted-request-consultation").val(rc.val());

                    realForm.find("#edit-submitted-preferred-date").val($('#datepicker').val());

                    realForm.find("#edit-submitted-full-name").val(fullname);

                    realForm.find("#edit-submitted-postcode").val(postcode);

                    realForm.find("#edit-submitted-address-line-1").val(addr1);
                    realForm.find("#edit-submitted-address-line-2").val(addr2);
                    realForm.find("#edit-submitted-address-line-3").val(addr3);
                    realForm.find("#edit-submitted-town-city").val(towncity);

                    realForm.find("#edit-submitted-contact-number").val(number);

                    realForm.find("#edit-submitted-email-address").val(email);

                    realForm.find("#edit-submitted-message").val($('textarea[name=contact_message]').val());

                    realForm.submit();

                }
            });

        },
        errorElement: 'span'
    });
    
	$(".enter-add-manual, #postcode-enter").click(function() {
		$(".add-extra").slideDown();
		$(".enter-add-manual").slideUp();
	});

	$(".user-request-brochure").click(function() {
		$("#request_brochure_yes").click();
	    $('html, body').animate({
	        scrollTop: $("#request-form").offset().top
	    }, 1200);
	    return false;
	});
	$(".user-request-consultation, .consulation-check").click(function() {
		$("#request_consultation_yes").click();
	    $('html, body').animate({
	        scrollTop: $("#request-form").offset().top
	    }, 1200);
	    return false;
	});

	
</script>

<script>
$('#lookup-field').setupPostcodeLookup({
    // Set your API key
    api_key: 'ak_imz1mybr4JxBWtmpw4S5z2KHFbks7',
    // Pass in CSS selectors pointing to your input fields to pipe the results
    output_fields: {
        line_1: 'input[name=contact_address_one]',
        line_2: 'input[name=contact_address_two]',
        line_3: 'input[name=contact_address_three]',
        post_town: 'input[name=contact_town]',
        postcode: 'input[name=contact_postcode]'
    },
    input: 'input[name=contact_postcode]',
    button: '#postcode-enter'
});
</script>

<script>
$('#request_consultation_yes, #request_consultation_no').on('change', function() {
    if($('#request_consultation_yes:checked').length) {
        $('#date-required').slideDown();
    } else {
        $('#date-required').slideUp();
    }
});
</script>

  </body>
</html>
