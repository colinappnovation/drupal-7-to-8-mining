<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="Assault Reduction International RSS" href="https://www.assaultreduction.com/rss.xml" />
<link rel="shortcut icon" href="https://www.assaultreduction.com/sites/default/files/ARI.ico" type="image/vnd.microsoft.icon" />
  <title>Assault Reduction International | Dedicated to reducing the risk of assault in the workplace.</title>
  <link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/sites/default/files/css/css_sEV1DGBBLnNJdK0SeDEtHkfLAZSU47RivwyXJyl-ngQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/sites/default/files/css/css_DOTcrx1_XXA2PA4UwhGHqrpHInhFCxWkhRRrvRRh5b0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/sites/default/files/css/css_Q8LZ1yKp8qTde0E22pD4B5KFYRtXSuJ5Mq9XCZwb9B8.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/sites/default/files/css/css_2THG1eGiBIizsWFeexsNe1iDifJ00QRS9uSd03rY9co.css" media="print" />

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/themes/bartik/css/ie.css?p8ctuk" media="all" />
<![endif]-->

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.assaultreduction.com/themes/bartik/css/ie6.css?p8ctuk" media="all" />
<![endif]-->
  <script type="text/javascript" src="https://www.assaultreduction.com/sites/default/files/js/js_vDrW3Ry_4gtSYaLsh77lWhWjIC6ml2QNkcfvfP5CVFs.js"></script>
<script type="text/javascript" src="https://www.assaultreduction.com/sites/default/files/js/js_lgAAOlplEun7p_8Pb-8dM079wtvnfwLZ0hAK0mH7Dto.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-20946764-1"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bartik_ari","theme_token":"EJuM1hHbRtAK_PFZ1wAwQPJcTFv0haF1cqXCgkouJG8","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/ckeditor\/ckeditor.css":1,"themes\/bartik\/css\/layout.css":1,"themes\/bartik\/css\/style.css":1,"sites\/all\/themes\/bartik_ari\/css\/colors.css":1,"sites\/all\/themes\/bartik_ari\/css\/bartik_ari.css":1,"themes\/bartik\/css\/print.css":1,"themes\/bartik\/css\/ie.css":1,"themes\/bartik\/css\/ie6.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper"><div id="page">

  <div id="header" class="without-secondary-menu"><div class="section clearfix">

          <a href="/" title="Home" rel="home" id="logo">
        <img src="https://www.assaultreduction.com/sites/default/files/ARI_Logo.png" alt="Home" />
      </a>
    
          <div id="name-and-slogan">

                              <h1 id="site-name" class="element-invisible">
              <a href="/" title="Home" rel="home"><span>Assault Reduction International</span></a>
            </h1>
                  
                  <div id="site-slogan">
            Dedicated to reducing the risk of assault in the workplace.          </div>
        
      </div> <!-- /#name-and-slogan -->
    
      <div class="region region-header">
    <div id="block-block-3" class="block block-block">

    
  <div class="content">
    <p><img id="phBtn" alt="Phone: +(44) 0113 2796912" src="/sites/default/files/resources/icons/phone_32.png" title="Phone: +(44) 0113 2796912" /> <a href="/contact/Enquiry"><img alt="Contact us" src="/sites/default/files/resources/icons/SocNetIcons/email_32.png" title="Contact us" /></a> <a href="http://www.facebook.com/pages/Assault-Reduction-International/124615927604415" target="_blank"><img alt="Our facebook" src="/sites/default/files/resources/icons/SocNetIcons/facebook_32.png" title="Our facebook" /></a> <a href="http://twitter.com/ReduceAssaults" target="_blank"><img alt="Follow us" src="/sites/default/files/resources/icons/SocNetIcons/twitter_32.png" title="Follow us" /></a> <a href="http://uk.linkedin.com/pub/mark-jenner/15/894/a51" target="_blank"><img alt="Connect to us" src="/sites/default/files/resources/icons/SocNetIcons/linkedin_32.png" title="Connect to us" /></a> <a href="http://assaultreduction.com/rss.xml" target="_blank"><img alt="RSS" src="/sites/default/files/resources/icons/SocNetIcons/rss_32.png" title="RSS" /></a><br /><span id="phNum" style="display:none;">+(44) 0113 2796912</span></p>
<script>
<!--//--><![CDATA[// ><!--

jQuery(document).ready(function($) {

$("#phBtn").click(function() {
    $("#phNum").toggle("slow");
});      

});

//--><!]]>
</script>  </div>
</div>
  </div>

          <div id="main-menu" class="navigation">
        <h2 class="element-invisible">Main menu</h2><ul id="main-menu-links" class="links clearfix"><li class="menu-198 first active"><a href="/" class="active">Home</a></li>
<li class="menu-358"><a href="/about-us">About Us</a></li>
<li class="menu-359"><a href="/conflict-management">Conflict Management</a></li>
<li class="menu-360"><a href="/physical-intervention">Physical Intervention</a></li>
<li class="menu-508 last"><a href="/nhs-alsms-services">NHS ALSMS Services</a></li>
</ul>      </div> <!-- /#main-menu -->
    
    
  </div></div> <!-- /.section, /#header -->

  
  
  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    
    
    <div id="content" class="column"><div class="section">
            <a id="main-content"></a>
                                <div class="tabs">
                  </div>
                          <div class="region region-content">
    <div id="block-block-5" class="block block-block">

    
  <div class="content">
    <p>
	<strong>Assault Reduction International (ARI)</strong> is dedicated to reducing the risk of assaults in the workplace through policy, communication and practical skills.</p>
<p>
	<em>Mark Jenner</em>, long synonymous with Maybo’s physical skills programme, will establish a regional Maybo Business Centre in the North of England. Establishing this centre of excellence for Maybo’s physical skills, Mark will continue to work closely with Maybo as a Physical Interventions Specialist, Lead Trainer and Quality Reviewer whilst exploring new market opportunities specific to his expertise in physical intervention training.</p>
<p>
	<strong>ARI</strong> can provide a range of training programmes to give staff the confidence to deal with increasing levels of ‘violence’ risk through its partnership with the UK’s leading brand in workplace conflict along with sector specific qualifications in sports coaching, care and security.</p>
  </div>
</div>
<div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-14" class="node node-blog node-promoted node-teaser clearfix" about="/node/14" typeof="sioc:Post sioct:BlogPost">

        <h2>
      <a href="/node/14">Can we ever manage violence and aggression?</a>
    </h2>
    <span property="dc:title" content="Can we ever manage violence and aggression?" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      <div class="meta submitted">
        <div class="user-picture">
    <img typeof="foaf:Image" src="https://www.assaultreduction.com/sites/default/files/styles/thumbnail/public/pictures/picture-7-1295206129.jpg?itok=uLQcoc40" alt="Mark Jenner&#039;s picture" title="Mark Jenner&#039;s picture" />  </div>
      <span property="dc:date dc:created" content="2012-01-16T17:25:12+00:00" datatype="xsd:dateTime" rel="sioc:has_creator">Submitted by <span class="username" xml:lang="" about="/user/7" typeof="sioc:UserAccount" property="foaf:name" datatype="">Mark Jenner</span> on Mon, 16/01/2012 - 17:25</span>    </div>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Well so much for us hoping 2012 would see a reduction in workplace violence!</p>
<p>So far staff and employees continue to deal with violence and aggression from the very people they are employed to serve. Across all sectors hard working employees are being hurt, sometimes seriously, as they try to meet the ever increasing expectations of customers and patients against a backdrop of budget and resource cuts.</p></div></div></div>  </div>

      <div class="link-wrapper">
      <ul class="links inline"><li class="node-readmore first"><a href="/node/14" rel="tag" title="Can we ever manage violence and aggression?">Read more<span class="element-invisible"> about Can we ever manage violence and aggression?</span></a></li>
<li class="blog_usernames_blog"><a href="/blog/7" title="Read Mark Jenner&#039;s latest blog entries.">Mark Jenner&#039;s blog</a></li>
<li class="comment_forbidden last"><span><a href="/user/login?destination=node/14%23comment-form">Log in</a> to post comments</span></li>
</ul>    </div>
  
  
</div>
<div id="node-13" class="node node-blog node-promoted node-teaser clearfix" about="/node/13" typeof="sioc:Post sioct:BlogPost">

        <h2>
      <a href="/node/13">Arts staff learn how to deal with aggression</a>
    </h2>
    <span property="dc:title" content="Arts staff learn how to deal with aggression" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="2" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      <div class="meta submitted">
        <div class="user-picture">
    <img typeof="foaf:Image" src="https://www.assaultreduction.com/sites/default/files/styles/thumbnail/public/pictures/picture-7-1295206129.jpg?itok=uLQcoc40" alt="Mark Jenner&#039;s picture" title="Mark Jenner&#039;s picture" />  </div>
      <span property="dc:date dc:created" content="2011-04-16T21:39:30+01:00" datatype="xsd:dateTime" rel="sioc:has_creator">Submitted by <span class="username" xml:lang="" about="/user/7" typeof="sioc:UserAccount" property="foaf:name" datatype="">Mark Jenner</span> on Sat, 16/04/2011 - 21:39</span>    </div>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>
	 </p>
<p>
	Hugh Dougherty</p>
<p>
	The performance space at Aberdeen’s Lemon Tree arts venue is lit up and some 15 figures look as if they’re rehearsing moves for a play’s fight sequence. Some are putting their palms up in front of their faces to ward off attack. Others are pulling aggressors away from colleagues and there’s action aplenty. </p>
<p>
	But this is not a part of the centre’s theatre programme. The clue is in the fit figure, fitting between the bodies, offering encouragement here, showing where the hands should be there. </p></div></div></div>  </div>

      <div class="link-wrapper">
      <ul class="links inline"><li class="node-readmore first"><a href="/node/13" rel="tag" title="Arts staff learn how to deal with aggression">Read more<span class="element-invisible"> about Arts staff learn how to deal with aggression</span></a></li>
<li class="blog_usernames_blog"><a href="/blog/7" title="Read Mark Jenner&#039;s latest blog entries.">Mark Jenner&#039;s blog</a></li>
<li class="comment-comments"><a href="/node/13#comments" title="Jump to the first comment of this posting.">2 comments</a></li>
<li class="comment_forbidden last"><span><a href="/user/login?destination=node/13%23comment-form">Log in</a> to post comments</span></li>
</ul>    </div>
  
  
</div>
<div id="node-12" class="node node-blog node-promoted node-teaser clearfix" about="/node/12" typeof="sioc:Post sioct:BlogPost">

        <h2>
      <a href="/node/12">Budget cuts increase risk of workplace violence?</a>
    </h2>
    <span property="dc:title" content="Budget cuts increase risk of workplace violence?" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      <div class="meta submitted">
        <div class="user-picture">
    <img typeof="foaf:Image" src="https://www.assaultreduction.com/sites/default/files/styles/thumbnail/public/pictures/picture-7-1295206129.jpg?itok=uLQcoc40" alt="Mark Jenner&#039;s picture" title="Mark Jenner&#039;s picture" />  </div>
      <span property="dc:date dc:created" content="2011-02-01T22:07:09+00:00" datatype="xsd:dateTime" rel="sioc:has_creator">Submitted by <span class="username" xml:lang="" about="/user/7" typeof="sioc:UserAccount" property="foaf:name" datatype="">Mark Jenner</span> on Tue, 01/02/2011 - 22:07</span>    </div>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>
	 </p>
<p>
	In the last two weeks, across the world, ordinary people working in roles providing support and help to others have been exposed to the risk of violence and aggression. On a daily basis reports come in, from every corner, of staff being attacked and injured whilst at work.</p></div></div></div>  </div>

      <div class="link-wrapper">
      <ul class="links inline"><li class="node-readmore first"><a href="/node/12" rel="tag" title="Budget cuts increase risk of workplace violence?">Read more<span class="element-invisible"> about Budget cuts increase risk of workplace violence?</span></a></li>
<li class="blog_usernames_blog"><a href="/blog/7" title="Read Mark Jenner&#039;s latest blog entries.">Mark Jenner&#039;s blog</a></li>
<li class="comment_forbidden last"><span><a href="/user/login?destination=node/12%23comment-form">Log in</a> to post comments</span></li>
</ul>    </div>
  
  
</div>
<div id="node-8" class="node node-blog node-promoted node-teaser clearfix" about="/node/8" typeof="sioc:Post sioct:BlogPost">

        <h2>
      <a href="/node/8">The HSE (UK) ‘Violence at work: Findings from the 2009/10 British Crime Survey’.</a>
    </h2>
    <span property="dc:title" content="The HSE (UK) ‘Violence at work: Findings from the 2009/10 British Crime Survey’." class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      <div class="meta submitted">
        <div class="user-picture">
    <img typeof="foaf:Image" src="https://www.assaultreduction.com/sites/default/files/styles/thumbnail/public/pictures/picture-7-1295206129.jpg?itok=uLQcoc40" alt="Mark Jenner&#039;s picture" title="Mark Jenner&#039;s picture" />  </div>
      <span property="dc:date dc:created" content="2011-01-23T18:02:47+00:00" datatype="xsd:dateTime" rel="sioc:has_creator">Submitted by <span class="username" xml:lang="" about="/user/7" typeof="sioc:UserAccount" property="foaf:name" datatype="">Mark Jenner</span> on Sun, 23/01/2011 - 18:02</span>    </div>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Reporting on the 2009/2010 British Crime Survey (BCS) the report shows that the risk of being a victim of violence at work in the UK is low, down by 9,000 form last year figures with victims of one or more workplace violence incidents accounting for 1.4% of all working adults. Undoubtedly critics will suggest these figures are low and that anecdotally they are much higher. This highlights a systemic under reporting across many work sectors, not wanting to ‘rock the boat’ or ‘nothing ever gets done about it’ underpinning the general malaise in reporting violence at work incidents.</p></div></div></div>  </div>

      <div class="link-wrapper">
      <ul class="links inline"><li class="node-readmore first"><a href="/node/8" rel="tag" title="The HSE (UK) ‘Violence at work: Findings from the 2009/10 British Crime Survey’.">Read more<span class="element-invisible"> about The HSE (UK) ‘Violence at work: Findings from the 2009/10 British Crime Survey’.</span></a></li>
<li class="blog_usernames_blog"><a href="/blog/7" title="Read Mark Jenner&#039;s latest blog entries.">Mark Jenner&#039;s blog</a></li>
<li class="comment_forbidden last"><span><a href="/user/login?destination=node/8%23comment-form">Log in</a> to post comments</span></li>
</ul>    </div>
  
  
</div>
<div id="node-7" class="node node-blog node-promoted node-teaser clearfix" about="/node/7" typeof="sioc:Post sioct:BlogPost">

        <h2>
      <a href="/node/7">Higher Education and Industry</a>
    </h2>
    <span property="dc:title" content="Higher Education and Industry" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
      <div class="meta submitted">
        <div class="user-picture">
    <img typeof="foaf:Image" src="https://www.assaultreduction.com/sites/default/files/styles/thumbnail/public/pictures/picture-7-1295206129.jpg?itok=uLQcoc40" alt="Mark Jenner&#039;s picture" title="Mark Jenner&#039;s picture" />  </div>
      <span property="dc:date dc:created" content="2011-01-17T21:09:02+00:00" datatype="xsd:dateTime" rel="sioc:has_creator">Submitted by <span class="username" xml:lang="" about="/user/7" typeof="sioc:UserAccount" property="foaf:name" datatype="">Mark Jenner</span> on Mon, 17/01/2011 - 21:09</span>    </div>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>As KPMG UK and Durham University steal the headlines by announcing a long term partnership, aimed at broadening access to the accountancy profession, an equally important, but less heralded, partnership begins in the licensed retail sector.<br />
The British Institute of Inn-keeping, the BII, have worked closely with Leeds Metropolitan University to develop a package which has been designed to suit the needs of the university and of their hospitality students.<br /></p></div></div></div>  </div>

      <div class="link-wrapper">
      <ul class="links inline"><li class="node-readmore first"><a href="/node/7" rel="tag" title="Higher Education and Industry">Read more<span class="element-invisible"> about Higher Education and Industry</span></a></li>
<li class="blog_usernames_blog"><a href="/blog/7" title="Read Mark Jenner&#039;s latest blog entries.">Mark Jenner&#039;s blog</a></li>
<li class="comment_forbidden last"><span><a href="/user/login?destination=node/7%23comment-form">Log in</a> to post comments</span></li>
</ul>    </div>
  
  
</div>
  </div>
</div>
  </div>
      <a href="/rss.xml" class="feed-icon" title="Subscribe to Assault Reduction International RSS"><img typeof="foaf:Image" src="https://www.assaultreduction.com/misc/feed.png" width="16" height="16" alt="Subscribe to Assault Reduction International RSS" /></a>
    </div></div> <!-- /.section, /#content -->

          <div id="sidebar-second" class="column sidebar"><div class="section">
          <div class="region region-sidebar-second">
    <div id="block-block-2" class="block block-block">

    
  <div class="content">
    <p><a class="twitter-timeline" href="https://twitter.com/ReduceAssaults/industry-news" data-widget-id="394883230895058944" data-chrome="transparent" data-tweet-limit="5">News from Twitter</a></p>
<script>
<!--//--><![CDATA[// ><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
//--><!]]>
</script>  </div>
</div>
  </div>
      </div></div> <!-- /.section, /#sidebar-second -->
    
  </div></div> <!-- /#main, /#main-wrapper -->

  
  <div id="footer-wrapper"><div class="section">

    
          <div id="footer" class="clearfix">
          <div class="region region-footer">
    <div id="block-block-4" class="block block-block">

    
  <div class="content">
    <p>
	Website © Assault Reduction International</p>
  </div>
</div>
  </div>
      </div> <!-- /#footer -->
    
  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
  </body>
</html>
