<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">    
    <head profile="http://www.w3.org/1999/xhtml/vocab">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.verisys.org.uk/sites/all/themes/slc/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="We provide specialist testing, commissioning, validation and verification services to clients, contractors and developers" />
<meta name="keywords" content="Commissioning Management, HVAC Commissioning &amp; Testing, Soft Landings, Seasonal Commissioning, Building Services Surveys, Validation Surveys, Thermographics" />
<meta name="robots" content="follow, index" />
<meta name="rating" content="general" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.verisys.org.uk/home" />
<link rel="shortlink" href="https://www.verisys.org.uk/node/4" />
    
        <!-- For third-generation iPad with high-resolution Retina display: -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="sites/all/themes/slc/images/touch-icon/apple-touch-icon-144x144-precomposed.png">
        
        <!-- For iPhone with high-resolution Retina display: -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="sites/all/themes/slc/images/touch-icon/apple-touch-icon-114x114-precomposed.png">
        
        <!-- For first- and second-generation iPad: -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="sites/all/themes/slc/images/touch-icon/apple-touch-icon-72x72-precomposed.png">
        
        <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
        <link rel="apple-touch-icon-precomposed" href="sites/all/themes/slc/images/touch-icon/apple-touch-icon-precomposed.png">


        <title>Verisys - Specialist Services to the Building Services Industry</title>
    
        <link href='http://fonts.googleapis.com/css?family=Actor|Open+Sans:300italic,600italic,400italic,300,400,600,700' rel='stylesheet' type='text/css'>
    
        <style type="text/css" media="all">
@import url("https://www.verisys.org.uk/modules/system/system.base.css?piij2u");
@import url("https://www.verisys.org.uk/modules/system/system.menus.css?piij2u");
@import url("https://www.verisys.org.uk/modules/system/system.messages.css?piij2u");
</style>
<style type="text/css" media="all">
@import url("https://www.verisys.org.uk/sites/all/modules/date/date_api/date.css?piij2u");
@import url("https://www.verisys.org.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?piij2u");
</style>
<style type="text/css" media="all">
@import url("https://www.verisys.org.uk/sites/all/themes/slc/layout.css?piij2u");
@import url("https://www.verisys.org.uk/sites/all/themes/slc/editor.css?piij2u");
@import url("https://www.verisys.org.uk/sites/all/themes/slc/js/fancybox/jquery.fancybox-1.3.4.css?piij2u");
</style>
<style type="text/css" media="screen and (min-width: 768px) and (min-device-width: 768px)">
@import url("https://www.verisys.org.uk/sites/all/themes/slc/tablet.css?piij2u");
</style>
<style type="text/css" media="screen and (min-width: 1024px) and (min-device-width: 1024px)">
@import url("https://www.verisys.org.uk/sites/all/themes/slc/desktop.css?piij2u");
</style>
    
        <script type="text/javascript" src="https://www.verisys.org.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/misc/drupal.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/jquery.touchSwipe.min.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/slideShow.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/slideShow_home_init.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/slider.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/slider_home_init.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/twitterFetcher_v10_min.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/fancybox/jquery.fancybox-1.3.4.pack.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/menu.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/mobile.js?piij2u"></script>
<script type="text/javascript" src="https://www.verisys.org.uk/sites/all/themes/slc/js/general.js?piij2u"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"slc","theme_token":"Est7a4xRo7hU0hwYBqPBHYEM_qOkncO4n0BBDU7Z8Lo","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/slc\/js\/jquery.touchSwipe.min.js":1,"sites\/all\/themes\/slc\/js\/slideShow.js":1,"sites\/all\/themes\/slc\/js\/slideShow_home_init.js":1,"sites\/all\/themes\/slc\/js\/slider.js":1,"sites\/all\/themes\/slc\/js\/slider_home_init.js":1,"sites\/all\/themes\/slc\/js\/twitterFetcher_v10_min.js":1,"sites\/all\/themes\/slc\/js\/fancybox\/jquery.fancybox-1.3.4.pack.js":1,"sites\/all\/themes\/slc\/js\/menu.js":1,"sites\/all\/themes\/slc\/js\/mobile.js":1,"sites\/all\/themes\/slc\/js\/general.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/themes\/slc\/layout.css":1,"sites\/all\/themes\/slc\/editor.css":1,"sites\/all\/themes\/slc\/js\/fancybox\/jquery.fancybox-1.3.4.css":1,"sites\/all\/themes\/slc\/tablet.css":1,"sites\/all\/themes\/slc\/desktop.css":1}}});
//--><!]]>
</script>
    </head>
    
    <body class="html front not-logged-in no-sidebars page-node page-node- page-node-4 node-type-home-page" >

        <div id="fb-root"></div>
        <script async="async">
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=210915835784896&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <div id="skip-link">
            <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
        </div>

        
        

<div id="page">
    <div id="header">
        <div class="wrapper clearfix">

                            <a href="/" title="Home" rel="home" id="logo">
                    <img src="https://www.verisys.org.uk/sites/default/files/verisys%20blue%20logo-01.jpg" alt="Home" />
                </a>
            
                <div class="region region-header">
        <div id="block-block-2" class="block block-block block-header-contact-numbers">

    
  <div class="content">
    <p><a href="tel:0113 393 3326">0113 393 3326</a> <a href="info@verisys.org.uk">info@verisys.org.uk</a></p>
  </div>
</div>
<div id="block-block-1" class="block block-block block-header-social-icons">

    
  <div class="content">
    <ul><li><a class="email" href="mailto:info@verisys.org.uk">Email</a></li>
<li><a class="twitter" href="https://twitter.com/silcockleedham">Twitter</a></li>
<li><a class="linkedin" href="https://www.linkedin.com/company/verisys-ltd/">LinkedIn</a></li>
</ul>  </div>
</div>
    </div>
    

            <a class="click-to-call" href="tel:+44 (0) 113 393 3310">Click to Call</a>

            <div class="burger">
                <div class="item top"></div>
                <div class="item mid"></div>
                <div class="item bun"></div>
            </div>
        </div><!-- .wrapper -->
    </div><!-- #header -->

    <div id="menu">
        <div class="wrapper clearfix">
                                        <div id="main-menu" class="navigation">
                    <ul id="menu-main_menu" class="menu mm-menu main_menu clearfix"><li class="home even first"><a href="/" title="Home">Home</a></li><li class="about_us odd"><a href="/about-us" title="About Us">About Us</a></li><li class="services even dropdown"><a class="has-children" href="/services" title="Services">Services</a><ul class="sub-menu level-1 no-level-2"><li class="commissioning_management even first"><a href="/services/commissioning-management" title="Commissioning Management">Commissioning Management</a></li><li class="commissioning_verification odd"><a href="/services/commissioning-verification" title="Commissioning Verification">Commissioning Verification</a></li><li class="hvac_commissioning_testing even"><a href="/services/hvac-commissioning-testing" title="HVAC Commissioning & Testing">HVAC Commissioning & Testing</a></li><li class="surveys odd"><a href="/services/surveys" title="Surveys">Surveys</a></li><li class="water_treatment_building_leakage even"><a href="/services/water-treatment-building-leakage" title="Water Treatment & Building Leakage">Water Treatment & Building Leakage</a></li><li class="validation odd"><a href="/services/validation" title="Validation">Validation</a></li><li class="soft_landings even last"><a href="/services/soft-landings" title="Soft Landings">Soft Landings</a></li></ul></li><li class="case_studies odd"><a href="/case-studies" title="Case Studies">Case Studies</a></li><li class="group even"><a href="/group-information" title="Group">Group</a></li><li class="key_staff odd"><a href="/key-staff" title="Key Staff">Key Staff</a></li><li class="contact_us even last"><a href="/contact-us" title="Contact Us">Contact Us</a></li></ul>                </div> <!-- /#main-menu -->
                    </div>
    </div>

        
    <div id="main">
        <div id="content" class="column">
            
            
                <div class="region region-content">
        <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div id="node-4" class="node node-home-page clearfix" about="/home" typeof="sioc:Item foaf:Document">

            <div class="field field-name-field-slides field-type-entityreference field-label-hidden"><ul class="slides"><li class="slide slide-1 even"><div id="node-173" class="node node-slider clearfix" about="/banner-1" typeof="sioc:Item foaf:Document">
    
            <span class="slide-img" style="background-image:url(https://www.verisys.org.uk/sites/default/files/Title%20banners-01.jpg);"></span>
    
            <div class="field field-name-field-slide-image field-type-image field-label-hidden"><img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/Title%20banners-01.jpg" width="5000" height="1584" alt="" /></div>    
    </div>
</li><li class="slide slide-2 odd"><div id="node-174" class="node node-slider clearfix" about="/banner-2" typeof="sioc:Item foaf:Document">
    
            <span class="slide-img" style="background-image:url(https://www.verisys.org.uk/sites/default/files/Title%20banners-02.jpg);"></span>
    
            <div class="field field-name-field-slide-image field-type-image field-label-hidden"><img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/Title%20banners-02.jpg" width="5000" height="1584" alt="" /></div>    
    </div>
</li><li class="slide slide-3 even"><div id="node-178" class="node node-slider clearfix" about="/banner-3" typeof="sioc:Item foaf:Document">
    
            <span class="slide-img" style="background-image:url(https://www.verisys.org.uk/sites/default/files/Title%20banners-03.jpg);"></span>
    
            <div class="field field-name-field-slide-image field-type-image field-label-hidden"><img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/Title%20banners-03.jpg" width="5000" height="1584" alt="" /></div>    
    </div>
</li><li class="slide slide-4 odd"><div id="node-189" class="node node-slider clearfix" about="/banner-4" typeof="sioc:Item foaf:Document">
    
            <span class="slide-img" style="background-image:url(https://www.verisys.org.uk/sites/default/files/Title%20banners%205-04-04.jpg);"></span>
    
            <div class="field field-name-field-slide-image field-type-image field-label-hidden"><img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/Title%20banners%205-04-04.jpg" width="5000" height="1584" alt="" /></div>    
    </div>
</li></ul></div>        
    <div class="wrapper clearfix">
        
                            <div class="view view-services view-id-services view-display-id-block_1 view-dom-id-3a2255f9feda4c2031b56fbe93e74584">
            <h2 class="title">Services</h2>
    <div class="views-row views-row-1 views-row-odd views-row-first">
        <div id="node-19" class="node node-service node-teaser clearfix" about="/services/hvac-commissioning-testing" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/hvac-commissioning-testing" title="HVAC Commissioning &amp; Testing">HVAC Commissioning &amp; Testing</a></h3>
            </div>
    </div>
    <div class="views-row views-row-2 views-row-even">
        <div id="node-20" class="node node-service node-teaser clearfix" about="/services/commissioning-management" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/commissioning-management" title="Commissioning Management">Commissioning Management</a></h3>
            </div>
    </div>
    <div class="views-row views-row-3 views-row-odd">
        <div id="node-21" class="node node-service node-teaser clearfix" about="/services/commissioning-verification" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/commissioning-verification" title="Commissioning Verification">Commissioning Verification</a></h3>
            </div>
    </div>
    <div class="views-row views-row-4 views-row-even">
        <div id="node-170" class="node node-service node-teaser clearfix" about="/services/surveys" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/surveys" title="Surveys">Surveys</a></h3>
            </div>
    </div>
    <div class="views-row views-row-5 views-row-odd">
        <div id="node-171" class="node node-service node-teaser clearfix" about="/services/water-treatment-building-leakage" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/water-treatment-building-leakage" title="Water Treatment &amp; Building Leakage">Water Treatment &amp; Building Leakage</a></h3>
            </div>
    </div>
    <div class="views-row views-row-6 views-row-even views-row-last">
        <div id="node-172" class="node node-service node-teaser clearfix" about="/services/validation" typeof="sioc:Item foaf:Document">
                        <h3 class="title"><a href="/services/validation" title="Validation">Validation</a></h3>
            </div>
    </div>
    </div>        
                    </div>

    <div class="wrapper pod-wrapper clearfix">
                    <div class="field field-name-field-pods field-type-entityreference field-label-hidden"><div class="pod pod-1 even"><div id="node-175" class="node node-pod clearfix" about="/about-us-0" typeof="sioc:Item foaf:Document">
            <img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/styles/home_page_pod_style/public/pod-example-1.jpg?itok=UYXiGqhK" width="300" height="265" alt="" />    
    <div class="overlay clearfix">
        <h2 class="title">About Us</h2>

        <div class="field field-name-body field-type-text-with-summary field-label-hidden"><p>We are committed to our profession and excellence in the future of the built environment.</p>
</div>
                    <a class="read-more" href="/about-us">View Now</a>
            </div>
</div>
</div><div class="pod pod-2 odd"><div id="node-176" class="node node-pod clearfix" about="/contact-us-0" typeof="sioc:Item foaf:Document">
    
    <div class="overlay clearfix">
        <h2 class="title">Contact Us</h2>

        <div class="field field-name-body field-type-text-with-summary field-label-hidden"><p>Get in touch today!</p>
</div>
                    <a class="read-more" href="/contact-us">View Now</a>
            </div>
</div>
</div><div class="pod pod-3 even"><div id="node-177" class="node node-pod clearfix" about="/commissioning-management" typeof="sioc:Item foaf:Document">
            <img typeof="foaf:Image" src="https://www.verisys.org.uk/sites/default/files/styles/home_page_pod_style/public/SL%20Web%20Images%20117.jpg?itok=mIl1DYcl" width="300" height="265" alt="" />    
    <div class="overlay clearfix">
        <h2 class="title">Commissioning Management</h2>

        <div class="field field-name-body field-type-text-with-summary field-label-hidden"><p>Verisys provides a truly effective addition to your own Project Team by influencing the activities of everyone involved in order to make the building operate successfull.</p>
</div>
                    <a class="read-more" href="/services/commissioning-management">View Now</a>
            </div>
</div>
</div></div>            </div>
</div>
  </div>
</div>
    </div>
        </div><!-- #content -->
    </div> <!-- #main -->

            <div id="top-footer">
            <div class="wrapper clearfix">
                    <div class="region region-top-footer clearfix">
        <div id="block-block-3" class="block block-block block-certifications">

    
  <div class="content">
    <p><img alt="" src="/sites/default/files/ace-logo.png" style="width: 169px; height: 70px;" /><img alt="" src="/sites/default/files/bsria-logo.png" style="width: 248px; height: 70px;" /><img alt="" src="/sites/default/files/construction-line-logo.png" style="width: 73px; height: 70px;" /><img alt="" src="/sites/default/files/csa-logo.png" style="width: 70px; height: 70px;" /></p>
  </div>
</div>
<div id="block-block-10" class="block block-block block-footer-social-icons">

    
  <div class="content">
    <ul><li><a class="twitter" href="https://twitter.com/silcockleedham">Twitter</a></li>
<li><a class="linkedin" href="https://www.linkedin.com/company/verisys-ltd/">LinkedIn</a></li>
</ul>  </div>
</div>
    </div>
            </div>
        </div>
        
            <div id="footer">
            <div class="wrapper clearfix">
                    <div class="region region-footer">
        <div id="block-menu-menu-footer-menu" class="block block-menu">

    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/cookie-policy">Cookie Policy</a></li>
<li class="leaf"><a href="/terms-use-0">Terms of Use</a></li>
<li class="last leaf"><a href="/privacy-policy-1">Privacy Policy</a></li>
</ul>  </div>
</div>
<div id="block-block-4" class="block block-block block-legal-notice">

    
  <div class="content">
    <p>© 2019 Verisys Limited</p>
  </div>
</div>
    </div>
            </div>
        </div> <!-- /#footer -->
    </div> <!-- #page -->


                <script src="https://use.fontawesome.com/650957856b.js"></script>
    </body>
</html>
