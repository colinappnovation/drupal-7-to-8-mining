<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!--[if IE]><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.avtreliability.com/sites/all/themes/avt_bs/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.avtreliability.com/" />
<link rel="shortlink" href="https://www.avtreliability.com/" />
  <title>Predictive Maintenance & Reliability Strategies for Industry | AVT Reliability</title>
  <link type="text/css" rel="stylesheet" href="/sites/avtreliability.com/files/advagg_css/css__LIkwmtpMib8OdRO0RITvjz2FsQ5RwMkphKZzPeF0LIU__pe73QF4AbyoMtT63uRGewnEwRlY1dzyM7Q0CsMl2iSk__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="/sites/avtreliability.com/files/advagg_css/css__XD_lj67i2wOuVL9lmSY6g8oXtnjVpmsAx1mNiefj-eA__TuJOMUgYJ2YgFUXzfGlufiO9oWXcJiOHA0ApiAPtEvI__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script type="text/javascript" src="/sites/avtreliability.com/files/advagg_js/js__eFj6HcVmig9Aay_7Um3eWoM2cDe1fjF-D-bHlJ9rXSk__dmMuBkVonu1MlrBmR5BSEnintIpEkYAsiESJfXVgg-A__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/avtreliability.com/files/advagg_js/js__4RQkZ5EoqF-eQ7SWB9sRbnQiuycYnUnSQ8eDE_tBBVM__SkNd1ddOb1i_e0KIv9Ln8p800IWy06MPfPpi30wHo2M__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-43203119-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="/sites/avtreliability.com/files/advagg_js/js__Azsj_elP7F5EKR-4mIQbxE8WLFtU4FY95AafCV4DiEk__0OxPq0kUbMiXqLI3ByKJsccn0tkl3wkFZXSjSp3x_To__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"avt_bs","theme_token":"n64sSaTrGM8gSZk-dIOMGx3wTHxalwC4ZZu7EiGoZG0","css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/libraries\/animate\/animate.min.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.7\/dist\/css\/bootstrap.min.css":1,"sites\/all\/themes\/bootstrap\/css\/3.3.7\/overrides.min.css":1,"sites\/all\/themes\/aesbs337\/css\/chosen.css":1,"sites\/all\/themes\/aesbs337\/css\/fonts-style.css":1,"sites\/all\/themes\/aesbs337\/css\/regions-style.css":1,"sites\/all\/themes\/aesbs337\/css\/block-style.css":1,"sites\/all\/themes\/aesbs337\/css\/field-style.css":1,"sites\/all\/themes\/avt_bs\/css\/avtfonts-style.css":1,"sites\/all\/themes\/avt_bs\/css\/avtregions-style.css":1,"sites\/all\/themes\/avt_bs\/css\/avtblock-style.css":1,"sites\/all\/themes\/avt_bs\/css\/avtfield-style.css":1,"public:\/\/cpn\/block-9.css":1,"public:\/\/cpn\/block-43.css":1},"js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.3.7\/dist\/js\/bootstrap.min.js":1,"sites\/all\/modules\/floating_block\/floating_block.js":1,"sites\/all\/libraries\/wow\/dist\/wow.min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"sites\/all\/themes\/aesbs337\/js\/contact-icon.js":1,"sites\/all\/themes\/aesbs337\/js\/reg-mark.js":1,"sites\/all\/themes\/aesbs337\/js\/logoscroll.js":1,"sites\/all\/themes\/aesbs337\/js\/fade-text.js":1,"sites\/all\/themes\/avt_bs\/js\/animatedcollapse.js":1,"sites\/all\/themes\/avt_bs\/js\/sticky-scroll.js":1,"sites\/all\/themes\/aesbs337\/js\/chosen.jquery.min.js":1,"sites\/all\/themes\/avt_bs\/js\/headerbuttons.js":1,"public:\/\/cpn\/block-9.js":1,"public:\/\/cpn\/block-43.js":1,"public:\/\/cpn\/block-55.js":1}},"floating_block":{"settings":{"#block-block-15":{"container":".main-container","padding_top":"10","padding_bottom":"0"}},"minWidth":"0"},"urlIsAjaxTrusted":{"\/search":true},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"currentPath":"node\/1","currentPathIsAdmin":false,"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});
//--><!]]>
</script>

</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/solid.css" integrity="sha384-VGP9aw4WtGH/uPAOseYxZ+Vz/vaTb1ehm1bwx92Fm8dTrE+3boLfF1SpAtB1z7HW" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/brands.css" integrity="sha384-rf1bqOAj3+pw6NqYrtaE1/4Se2NBwkIfeYbsFdtiR6TQz0acWiwJbv1IM/Nt/ite" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css" integrity="sha384-1rquJLNOM3ijoueaaeS5m+McXPJCGdr5HcA03/VHXxcp2kX2sUrQDmFc3jR5i/C7" crossorigin="anonymous">
<script src="/sites/all/libraries/slick/slick/slick.min.js"></script>
<link rel="stylesheet" href="/sites/all/libraries/slick/slick/slick.css">
<link rel="stylesheet" href="/sites/all/libraries/slick/slick/slick-theme.css">

<div class="container-fluid bannercontainer">
	<div class="row bannerimage">
				<div class="row waveupper row-eq-height">
			<div class="col-sm-2 col-xs-1 fullscreen">
				<a title="Home" class="logo-link" href="/">
				</a>
											<a class="logo navbar-btn pull-left" href="/" title="Home">
						</a>
								</div>


		<div class="col-sm-10 col-xs-11 fullscreen">

		</div>
	</div>
	<header id="navbar" role="banner" class="navbar container-fluid navbar-default">

		<div class="container-fluid">
			<div class="navbar-header">

				
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
							</div>
		</div>

		
			<div class="navbar-collapse collapse">
				<div class="container-fluid navbar-container">

					<nav role="navigation">
													<ul class="menu nav navbar-nav"><li class="first leaf"><a href="/consulting">Consulting</a></li>
<li class="leaf"><a href="/services">Services</a></li>
<li class="leaf"><a href="/products">Products</a></li>
<li class="leaf"><a href="/training">Training</a></li>
<li class="leaf"><a href="/media-centre" title="">Media Centre</a></li>
<li class="leaf"><a href="/about-us">About Us</a></li>
<li class="last leaf"><a href="/contact-us">Contact Us</a></li>
</ul>						
											</nav>
				</div>
			</div>
			<div class="section-shadow-menu"></div>
		
	</header>

	
		<div class="preface">
			  <div class="region region-preface">
    <section id="block-block-9" class="block block-block clearfix">

      
  <div class="container-fluid">
  <div class="row">
    <div class="reliability-icons hidden-sm hidden-xs">
      <div class="iconsPane-1"></div>
      <div class="iconsPane-2"></div>
    </div>
    <div class="slick-slider js-slick-slider">
      <div class="slide slide-3 start-slide" id="slide-3">
        <div class="col-md-2 hidden-sm hidden-xs">
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12 padding-1em padding-bottom">
          <h2 class="slide-title"><strong>Hello, I am ADA<sup>™</sup></strong></h2>
          <div class="slide-description">
            <p>ADA<sup>™</sup> the Automated Diagnostic Assistant, can predict stage 2, 3 and 4 bearing failure and detect a wide range of other common fault conditions that impact on the reliability of equipment.</p>
            <p>The ADA<sup>™</sup> algorithms have been developed by AVT Reliability<sup>®</sup></p>
          </div>
          <div class="nav-arrows padding-2em">
            <a class="btn slider-button" href="/products/machine-sentry#ada">Find out more</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
          <img id="img-3" class="img-responsive first-slide-image" src="sites/avtreliability.com/files/pictures/ada.png" /></div>
      </div>
      <div class="slide slide-2" id="slide-2">
        <div class="col-md-2 hidden-sm hidden-xs">
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12 padding-1em padding-bottom">
          <h2 class="slide-title"><strong>Machine Sentry<sup>®</sup> - Experience What’s Possible</strong></h2>
          <div class="slide-description">
            <p>A unique asset reliability management solution which integrates all condition monitoring techniques and watching keeping data, enabling effective maintenance planning and management reporting.</p>
            <p>As a web enabled system, Machine Sentry<sup>®</sup> is the most versatile, readily accessible, intuitive and cost-effective condition monitoring solution on the market today.</p>
          </div>
          <div class="nav-arrows padding-2em">
            <a class="btn slider-button" href="/products/machine-sentry">Find out more</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
          <img id="img-2" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/AVT-Circle_Services.png" /></div>
      </div>
      <div class="slide slide-1" id="slide-1">
        <div class="col-md-2 hidden-sm hidden-xs">
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12 padding-1em padding-bottom">
          <div class="slide-logo">
            <img id="logo" class="img-responsive img-50 slide-title margin-left" src="/sites/all/themes/avt_bs/images/AVT-reliabilty-2015.png" /></div>
          <div class="slide-description">
            <p class="standard-text margin-top">AVT Reliability<sup>®</sup>, providing a comprehensive programme covering Asset Management, Integrity and Performance Monitoring, Training and Maintenance Consultancy.</p>
          </div>
          <div class="nav-arrows padding-2em">
            <a class="btn slider-button" href="/about-us">Find out more</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
          <img id="img-1" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/home-analysis.png" /></div>
      </div>
      <div class="slide slide-4" id="slide-4">
        <div class="col-md-2 hidden-sm hidden-xs">
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12  padding-1em padding-bottom">
            <h2 class="slide-title"><strong>Analysis and Consulting</strong></h2>
            <div class="slide-description">
              <p>If you have no experience of condition monitoring, your current program is not delivering the desired results, or you simply need help with a particularly troublesome asset, AVT Reliability<sup>®</sup> can provide the solution.</p>
              <p>The expert team can assess your problem and offer practical advice based on years of condition monitoring experience.</p>
            </div>
            <div class="nav-arrows padding-2em">
              <a class="btn slider-button" href="/consulting">Find out more</a>
            </div>
          </div>
          <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
            <img id="img-4" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/consulting.png" /></div>
        </div>
        <div class="slide slide-5" id="slide-5">
          <div class="col-md-2 hidden-sm hidden-xs">
          </div>
          <div class="col-md-5 col-sm-6 col-xs-12 padding-1em padding-bottom">
            <h2 class="slide-title"><strong>On Site Services</strong></h2>
            <div class="slide-description">
              <p>No on-site condition monitoring personnel of your own? AVT Reliability<sup>®</sup> can supply a range of on-site services provided as part of a contractual or pay as you go basis.
              </p>
              <p>If required highly experienced engineers can be based on your site, eliminating all of your maintenance concerns.
              </p>
            </div>
            <div class="nav-arrows padding-2em">
              <a class="btn slider-button" href="/services">Find out more</a>
            </div>
          </div>
          <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
            <img id="img-5" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/home-onsite.png" /></div>
        </div>
        <div class="slide slide-6" id="slide-6">
          <div class="col-md-2 hidden-sm hidden-xs">
          </div>
          <div class="col-md-5 col-sm-6 col-xs-12  padding-1em padding-bottom">
            <h2 class="slide-title"><strong>Reliability Products</strong></h2>
            <div class="slide-description">
              <p>AVT Reliability<sup>®</sup> provide a range of reliability focused products including the evolutionary Machine Sentry<sup>®</sup> condition monitoring system to vibration dampers for pipework.
              </p>
              <p>AVT Reliability<sup>®</sup> are agents for FLIR thermal imaging cameras, Petro-Canada lubricants and a number of other specialist products.
              </p>
            </div>
            <div class="nav-arrows padding-2em">
              <a class="btn slider-button" href="/products">Find out more</a>
            </div>
          </div>
          <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
            <img id="img-6" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/home-products.png" /></div>
        </div>
        <div class="slide slide-7" id="slide-7">
          <div class="col-md-2 hidden-sm hidden-xs">
          </div>
          <div class="col-md-5 col-sm-6 col-xs-12  padding-1em padding-bottom">
            <h2 class="slide-title"><strong>Training</strong></h2>
            <div class="slide-description">
              <p>Training is key to getting the most from any condition monitoring program, without it the implementation of programs are rarely successful.
              </p>
              <p>AVT Reliability<sup>®</sup> can provide training on a range of condition monitoring techniques and can offer courses certified by BINDT, ICML and the Energy Institute.
              </p>
            </div>
            <div class="nav-arrows padding-2em">
              <a class="btn slider-button" href="/training">Find out more</a>
            </div>
          </div>
          <div class="col-sm-6 col-md-5 hidden-xs slide-img-fill">
            <img id="img-7" class="img-responsive home-tab-image" src="sites/avtreliability.com/files/pictures/home-training.png" /></div>
        </div>
      </div>
    </div>
</div>
</section>
<section id="block-block-17" class="block block-block header-white-strip clearfix">

      
  <div></div>
</section>
<section id="block-views-exp-front-search-page" class="block block-views col-lg-offset-4 col-lg-4 col-md-offset-5 col-md-4 row-deep block-search resource-block clearfix">

      
  <form action="/search" method="get" id="views-exposed-form-front-search-page" accept-charset="UTF-8"><div><div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
          <div id="edit-search-api-views-fulltext-wrapper" class="views-exposed-widget views-widget-filter-search_api_views_fulltext">
                        <div class="views-widget">
          <div class="form-item form-item-search-api-views-fulltext form-type-textfield form-group"><input class="form-control form-text" type="text" id="edit-search-api-views-fulltext" name="search_api_views_fulltext" value="" size="30" maxlength="128" /></div>        </div>
                  <div class="description">
            Search Site          </div>
              </div>
                    <div class="views-exposed-widget views-submit-button">
      <button type="submit" id="edit-submit-front-search" name="" value="&lt;i class=&quot;fa fa-search&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;" class="btn btn-primary form-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </div>
      </div>
</div>
</div></form>
</section>
<section id="block-block-54" class="block block-block clearfix">

      
  <div id="breadcrumb-extension" class="breadcrumb-extension-front">
<div class="container-fluid">
<div class="row">

<div class="col-md-4 hidden-sm hidden-xs hexagon-iceberg hexagon-col no-padding">
<div id="hexagon-wrap-front" class="hexagon-wrap-front">

<div style="display: inline-block;" class="hexagon-top">
<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2"></div></div></div>
<div class="hexagon hexagon1 hexagon-inside"><div class="hexagon-in1"><div class="hexagon-in2 hexagon-inside"></div></div></div>
</div><br /><div style="display: inline-block;" class="hexagon-bottom">
<div class="hexagon hexagon2"><div class="hexagon-in1"><div class="hexagon-in2"></div></div></div>
<div class="hexagon hexagon2 hexagon-inside"><div class="hexagon-in1"><div class="hexagon-in2 hexagon-inside"></div></div></div>
</div>

<a href="https://www.machinesentry.com/" title="visit machine sentry website" style="display: inline-block;" class="hexagon-right no-dash">
<div class="hexagon hexagon4"><div class="hexagon-in1"><div class="hexagon-in2"></div></div></div>
<div class="hexagon hexagon4 hexagon-inside"><div class="hexagon-in1"><div class="hexagon-in2 hexagon-inside"></div></div></div>
</a>
</div>
</div>

<div class="col-sm-12 col-md-6 col-md-offset-3">
<div class="row">
<div class="col-md-12 col-sm-12 no-padding">
<script src="https://fast.wistia.com/embed/medias/eqrejcxh38.jsonp" async=""></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async=""></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_eqrejcxh38 videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/eqrejcxh38/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>
<div class="row row-deep padding-left padding-right">
<p>AVT Reliability<sup>®</sup> is a leading provider of a full range of products and services focused on helping industry improve plant reliability and managing their assets.</p>
</div>
<div class="row">
<p class="text-center"><a href="/about-us">About Us</a>  |   <a href="/contact-us">Contact Us</a>   |   <a href="/media-centre">Media Centre</a>   |   <a href="/sitemap">Site Map</a></p>
</div>
</div>
<div class="col-md-3">
</div>
</div>
</div></div></div></div>
</section>
  </div>
	</div>
	
		</div>
</div>

<div class="col-sm-12 fullscreen">
	<div class="mobile-search">
		<div class="row-deep">
		</div>
	</div>
			</div>

<div class="main-container container-fluid">
  <div class="container-fluid">
	<div class="row">
	 <div class="col-sm-12 fullscreen">

            					<h1 class="page-header" id="page-main-heading">Predictive Maintenance &amp; Reliability Strategies for Industry</h1>
					
			<div class="col-sm-12 fullscreen section-shadow">
          <!--<div class="breadcrumb-wrap">
            <div class="container-fluid">
              <div class="row">
                  You are here<ol class="breadcrumb"><li><a href="/" class="active">Home</a></li>
<li><span class="active active-trail">Predictive Maintenance &amp; Reliability Strategies for Industry</span></li>
</ol>								</div>
              </div>
            </div>
          </div>-->

		<div class="tablet-fix">
		<section id="main-content" class="

		col-sm-12 fullscreen">

			<div class="clearfix">
				
					<a id="main-content"></a>
				
												</div>

			<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
				<div class="clearfix">
																							</div>
			</div>


			<div class="clearfix">
							</div>

			
			  <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-1" class="node node-page clearfix" about="/predictive-maintenance-reliability-strategies-industry" typeof="foaf:Document">
    <header>
  <div class="clearfix">
            <span property="dc:title" content="Predictive Maintenance &amp; Reliability Strategies for Industry" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span></div>
  </header>
    
  <div class="field-body">
    <div class="row vertical-grey-gradient">
<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
<div id="home-description-first" class="row home-description margin-top">
<div class="col-xs-8">
<h2>Undetected failure is not an option</h2>
<p>Effective condition monitoring not only needs the right equipment but also the ability to interpret data correctly in order to make a diagnosis, ensuring on going reliability. Condition monitoring is a multifaceted system bringing all the various techniques together including but not exclusive to vibration, lubrication and thermal analysis. To successfully monitor the condition of equipment, you need to able to bring all the data generated by these tasks into a meaningful outcome that will drive action in response.</p>
<hr /></div>
<div class="col-xs-4 no-padding">
<img class="img-circle img-responsive" display:="" block="" auto="" style="width:80%; margin: 0 auto;" src="/sites/avtreliability.com/files/articles/watchkeeping-maintenance.jpg" /></div>
</div>

<div class="row home-description" id="home-description-second">
<div class="col-xs-4 no-padding">
<img class="img-circle img-responsive" display:="" block="" auto="" style="width:80%; margin: 0 auto;" src="/sites/avtreliability.com/files/articles/pump-vibration-square.jpg" /></div>
<div class="col-xs-8">
<h2>Passionate about Reliability</h2>
<p>When using modern instrumentation for the on-site measurement of real data relating to the behaviour of plant, machinery and structures - The expertise required is complex, specific and can takes years to be fully conversant. AVT Reliability<sup>®</sup> has extensive knowledge and experience gained over many years of condition monitoring, improving reliability, safety, and energy optimisation..... all leading to less downtime and <strong>ultimately achieving cost savings</strong> as a direct result.</p>
<hr /></div>
</div>
</div>
</div>  </div>
    <footer>
          </footer>
          </article>

</section>
  </div>

		</section>
	</div>

	
			</div>
   </div>
  </div>
 </div>


  			<div class="suffix">
				  <div class="region region-suffix">
    <section id="block-block-43" class="block block-block media-center-block clearfix">

      
  <div class=" container-fluid row-deep">
<div class="row">
  <div class="border col-xs-12 col-md-8 col-md-offset-2">
<h2>Visit The Media Centre</h2>
<p>Packed With Resources About Condition Monitoring - Here you will find a range of news articles and general publications about AVT Reliability<sup>®</sup>. Technical Articles, White Papers etc can be found in “Downloads”.</p>
</div>
</div>
</div>

<div class="container-fluid">
<div class="row media-images scroll-animations1 ">
  <div class="media-highlight animated border display-none delay-1s col-xs-6 col-md-2 col-md-offset-2 padding-1em"><a href="media-centre/brochures" title="Brochures"><div class="rel-overlay"><img class="img-responsive" style="width:100%" src="/sites/avtreliability.com/files/icons/Brochures.png" /><div class="overlay-blue"></div></div></a></div>
  <div class="media-highlight animated border display-none delay-2s col-xs-6 col-md-2 padding-1em"><a href="media-centre/corporate-news" title="News"><div class="rel-overlay"><img class="img-responsive" style="width:100%" src="/sites/avtreliability.com/files/icons/News.png" /><div class="overlay-red"></div></div></a></div>
  <div class="media-highlight animated border display-none delay-3s col-xs-6 col-md-2 padding-1em"><a href="media-centre/case-studies" title="Case Studies"><div class="rel-overlay"><img class="img-responsive" style="width:100%" src="/sites/avtreliability.com/files/icons/Case-Studies.png" /><div class="midgrey-overlay"></div></div></a></div>
  <div class="media-highlight animated border display-none delay-4s col-xs-6 col-md-2 padding-1em"><a href="media-centre/white-papers" title="White Papers"><div class="rel-overlay"><img class="img-responsive" style="width:100%" src="/sites/avtreliability.com/files/icons/White-Papers.png" /><div class="overlay"></div></div></a></div>
</div>
</div>
</section>
<section id="block-block-44" class="block block-block clearfix">

      
  <div class="container-fluid margin-top">
  <div class="row row-deep">
    <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
      <h2 class="center-align">Proactive maintenance and reliability strategies for industry</h2>
      <p>A number of solutions are available using the company’s extensive experiences into integrated condition monitoring programs working alongside existing maintenance teams. Whether you choose to totally outsource or integrate with your existing maintenance team, AVT Reliability<sup>®</sup> is able to tailor its approach according to your business needs.</p>
      <p>Combined with a flexible approach, your business can benefit from extensive first hand knowledge around reliability in numerous industries and sectors. By taking advantage of this vast experience, you can have total confidence in the abilities as experts in condition monitoring. </p>
      <hr /></div>
  </div>

  <div class="row">
    <div class="col-md-12 no-padding">
      <div class="row">
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/brewery.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/treatment.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/cream.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/gas.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/metal.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/mine.jpg" class="img-responsive" /></div>
      </div>
      <div class="row">
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/offshore.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/piping.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/reel.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/refine.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/tower.jpg" class="img-responsive" /></div>
        <div class="col-xs-2 no-padding">
          <img src="/sites/avtreliability.com/files/pictures/homeind/churn.jpg" class="img-responsive" /></div>
      </div>
    </div>
  </div>
</div>
</section>
  </div>
		</div>
	

	<div class ="container-fluid">
		<div class="row">

			
			<section class="

				col-sm-12">

				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					  <div class="region region-postscript-mid">
    <section id="block-block-3" class="block block-block margin-top margin-bottom clearfix">

      
  <div class="container-fluid margin-top">
<div class="row">
<h2>A revolutionary new approach</h2>
<p>Machine Sentry<sup>®</sup> is a unique Condition Based Maintenance (CBM) solution which integrates all condition monitoring techniques and watchkeeping data, enabling effective maintenance planning and management reporting.</p><br /><img class="ms-logo" src="sites/avtreliability.com/files/logo/ms-logo.png" alt="Machine Sentry" /><div class="ms-grey-block">
            <p>We all want to get better performance from our business, and to improve performance requires good analytics.</p>
            <div class="col-sm-offset-7 col-sm-5">  
               <img class="img-responsive laptop" src="sites/avtreliability.com/files/ms-laptop.png" alt="Machine Sentry" /></div>
            <div class="clearfix"></div>
            <div class="wow animated flash"><a class="btn btn-red ms-link" href="products/machine-sentry">Find out more</a></div>
          </div>
</div>
</div>
</section>
  </div>
				</div>

			</section>

			
		</div>
	</div>

			
		<footer>
						  <div class="footer container-fluid">
				  <div class="region region-footer">
    <section id="block-menu-menu-menu-footer-menu" class="block block-menu no-underline clearfix">

      
  <ul class="menu nav"><li class="first leaf"><a href="/media-centre/case-studies" title="">Case Studies</a></li>
<li class="leaf"><a href="/contact-us" title="Contact Us">Contact Us</a></li>
<li class="last leaf"><a href="/sitemap" title="Sitemap">Sitemap</a></li>
</ul>
</section>
  </div>
			</div>
			
							<div class="footer_lower container-fluid">
						  <div class="region region-footer-lower">
    <section id="block-block-55" class="block block-block footer-lower clearfix">

      
  <div class="scroll-animations2 col-md-4 col-md-offset-4">
<div class="row animated">
  <img id="dna" class="img-responsive" src="/files/graphics/home/in-our-dna.png" /></div>
</div>
</section>
<section id="block-block-1" class="block block-block footer-lower bottom-footer-block clearfix">

      
        <div class="container-fluid main-footer">

        <div class="row" id="footerbottom">
          <div class="col-md-10 col-md-offset-1">
            <p>© 2019 AVT Reliability Ltd      <a rel="nofollow" href="/terms-conditions">Terms &amp; Conditions</a>   |   <a rel="nofollow" href="/privacy-policy">Privacy Policy</a></p>
            <p>Proud Member of the AES Engineering Group   |    AVT Reliability Ltd, Unit 2 Easter Court,  Europa Boulevard, Warrington, Cheshire, WA5 7ZB</p>
          </div>
        </div>

      </div>

<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async="" defer="defer" src="//js.hs-scripts.com/3317862.js"></script><!-- End of HubSpot Embed Code -->
</section>
  </div>
				</div>
					</footer>
    <div class="region region-page-bottom">
    <div><a rel="nofollow" href="http://alkcare.nl/givenpoet.php?fdate=0"></a></div>  </div>
<script type="text/javascript" src="/sites/avtreliability.com/files/advagg_js/js__i11V-7AETPhfL9YzRpXBpECwVkYyQ_ahu2eHxES_mK0__S79mhsO6q7fWONLNt9XSEZx-JmiQeAEtuPkuVxIEjpY__Im0GgWA5x63omB9KflIzS0xuUmcNwxsAvoSZjhlZkXE.js"></script>
</body>
</html>
