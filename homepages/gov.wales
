<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" /><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"42e0ea2850",applicationID:"108008856"};window.NREUM||(NREUM={}),__nr_require=function(n,e,t){function r(t){if(!e[t]){var i=e[t]={exports:{}};n[t][0].call(i.exports,function(e){var i=n[t][1][e];return r(i||e)},i,i.exports)}return e[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<t.length;i++)r(t[i]);return r}({1:[function(n,e,t){function r(){}function i(n,e,t){return function(){return o(n,[u.now()].concat(f(arguments)),e?null:this,t),e?void 0:this}}var o=n("handle"),a=n(4),f=n(5),c=n("ee").get("tracer"),u=n("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(n,e){s[e]=i(d+e,!0,"api")}),s.addPageAction=i(d+"addPageAction",!0),s.setCurrentRouteName=i(d+"routeName",!0),e.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(n,e){var t={},r=this,i="function"==typeof e;return o(l+"tracer",[u.now(),n,t],r),function(){if(c.emit((i?"":"no-")+"fn-start",[u.now(),r,i],t),i)try{return e.apply(this,arguments)}catch(n){throw c.emit("fn-err",[arguments,this,n],t),n}finally{c.emit("fn-end",[u.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(n,e){m[e]=i(l+e)}),newrelic.noticeError=function(n,e){"string"==typeof n&&(n=new Error(n)),o("err",[n,u.now(),!1,e])}},{}],2:[function(n,e,t){function r(n,e){var t=n.getEntries();t.forEach(function(n){"first-paint"===n.name?a("timing",["fp",Math.floor(n.startTime)]):"first-contentful-paint"===n.name&&a("timing",["fcp",Math.floor(n.startTime)])})}function i(n){if(n instanceof c&&!s){var e,t=Math.round(n.timeStamp);e=t>1e12?Date.now()-t:f.now()-t,s=!0,a("timing",["fi",t,{type:n.type,fid:e}])}}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var o,a=n("handle"),f=n("loader"),c=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){o=new PerformanceObserver(r);try{o.observe({entryTypes:["paint"]})}catch(u){}}if("addEventListener"in document){var s=!1,p=["click","keydown","mousedown","pointerdown","touchstart"];p.forEach(function(n){document.addEventListener(n,i,!1)})}}},{}],3:[function(n,e,t){function r(n,e){if(!i)return!1;if(n!==i)return!1;if(!e)return!0;if(!o)return!1;for(var t=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var f=navigator.userAgent,c=f.match(a);c&&f.indexOf("Chrome")===-1&&f.indexOf("Chromium")===-1&&(i="Safari",o=c[1])}e.exports={agent:i,version:o,match:r}},{}],4:[function(n,e,t){function r(n,e){var t=[],r="",o=0;for(r in n)i.call(n,r)&&(t[o]=e(r,n[r]),o+=1);return t}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],5:[function(n,e,t){function r(n,e,t){e||(e=0),"undefined"==typeof t&&(t=n?n.length:0);for(var r=-1,i=t-e||0,o=Array(i<0?0:i);++r<i;)o[r]=n[e+r];return o}e.exports=r},{}],6:[function(n,e,t){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(n,e,t){function r(){}function i(n){function e(n){return n&&n instanceof r?n:n?c(n,f,o):o()}function t(t,r,i,o){if(!d.aborted||o){n&&n(t,r,i);for(var a=e(i),f=v(t),c=f.length,u=0;u<c;u++)f[u].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(n,e){h[n]=v(n).concat(e)}function m(n,e){var t=h[n];if(t)for(var r=0;r<t.length;r++)t[r]===e&&t.splice(r,1)}function v(n){return h[n]||[]}function g(n){return p[n]=p[n]||i(t)}function w(n,e){u(n,function(n,t){e=e||"feature",y[t]=e,e in s||(s[e]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:e,buffer:w,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var f="nr@context",c=n("gos"),u=n(4),s={},p={},d=e.exports=i();d.backlog=s},{}],gos:[function(n,e,t){function r(n,e,t){if(i.call(n,e))return n[e];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return n[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(n,e,t){function r(n,e,t,r){i.buffer([n],r),i.emit(n,e,t)}var i=n("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(n,e,t){function r(n){var e=typeof n;return!n||"object"!==e&&"function"!==e?-1:n===window?0:a(n,o,function(){return i++})}var i=1,o="nr@id",a=n("gos");e.exports=r},{}],loader:[function(n,e,t){function r(){if(!x++){var n=E.info=NREUM.info,e=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(n&&n.licenseKey&&n.applicationID&&e))return s.abort();u(y,function(e,t){n[e]||(n[e]=t)}),c("mark",["onload",a()+E.offset],null,"api");var t=l.createElement("script");t.src="https://"+n.agent,e.parentNode.insertBefore(t,e)}}function i(){"complete"===l.readyState&&o()}function o(){c("mark",["domContent",a()+E.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(f=Math.max((new Date).getTime(),f))-E.offset}var f=(new Date).getTime(),c=n("handle"),u=n(4),s=n("ee"),p=n(3),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1153.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),E=e.exports={offset:f,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};n(1),n(2),l[m]?(l[m]("DOMContentLoaded",o,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",i),d[v]("onload",r)),c("mark",["firstbyte",f],null,"api");var x=0,O=n(6)},{}],"wrap-function":[function(n,e,t){function r(n){return!(n&&n instanceof Function&&n.apply&&!n[a])}var i=n("ee"),o=n(5),a="nr@original",f=Object.prototype.hasOwnProperty,c=!1;e.exports=function(n,e){function t(n,e,t,i){function nrWrapper(){var r,a,f,c;try{a=this,r=o(arguments),f="function"==typeof t?t(r,a):t||{}}catch(u){d([u,"",[r,a,i],f])}s(e+"start",[r,a,i],f);try{return c=n.apply(a,r)}catch(p){throw s(e+"err",[r,a,p],f),p}finally{s(e+"end",[r,a,c],f)}}return r(n)?n:(e||(e=""),nrWrapper[a]=n,p(n,nrWrapper),nrWrapper)}function u(n,e,i,o){i||(i="");var a,f,c,u="-"===i.charAt(0);for(c=0;c<e.length;c++)f=e[c],a=n[f],r(a)||(n[f]=t(a,u?f+i:i,o,f))}function s(t,r,i){if(!c||e){var o=c;c=!0;try{n.emit(t,r,i,e)}catch(a){d([a,t,r,i])}c=o}}function p(n,e){if(Object.defineProperty&&Object.keys)try{var t=Object.keys(n);return t.forEach(function(t){Object.defineProperty(e,t,{get:function(){return n[t]},set:function(e){return n[t]=e,e}})}),e}catch(r){d([r])}for(var i in n)f.call(n,i)&&(e[i]=n[i]);return e}function d(e){try{n.emit("internal-error",e)}catch(t){}}return n||(n=i),t.inPlace=u,t.flag=a,t}},{}]},{},["loader"]);</script>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&amp;l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-MBLNKRW');</script>
<meta name="title" content="Home | GOV.WALES" />
<link rel="shortlink" href="https://gov.wales/" />
<link rel="canonical" href="https://gov.wales/" />
<meta name="twitter:card" content="summary" />
<meta property="og:site_name" content="GOV.WALES" />
<meta name="description" content="The Welsh Government is the devolved Government for Wales" />
<meta name="twitter:title" content="gov.wales | gov.wales" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://gov.wales" />
<meta name="twitter:url" content="https://gov.wales" />
<meta name="twitter:image" content="https://gov.wales//themes/custom/govwales/images/content/og-global-120.png" />
<meta property="og:image" content="https://gov.wales//themes/custom/govwales/images/content/og-global-1200.png" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="twitter:description" content="The Welsh Government is the devolved Government for Wales" />
<link rel="alternate" hreflang="en" href="https://gov.wales/" />
<link rel="alternate" hreflang="cy" href="https://llyw.cymru/" />
<link rel="revision" href="https://gov.wales/" />


    <link rel="icon" type="image/ico"  href="/favicon.ico" >
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/themes/custom/govwales/favicon/apple-touch-icon-180x180-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/themes/custom/govwales/favicon/apple-touch-icon-152x152-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/themes/custom/govwales/favicon/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/themes/custom/govwales/favicon/apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/themes/custom/govwales/favicon/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/themes/custom/govwales/favicon/apple-touch-icon-76x76-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/themes/custom/govwales/favicon/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/themes/custom/govwales/favicon/apple-touch-icon-precomposed.png">
    <link rel="icon" sizes="192x192" href="/themes/custom/govwales/favicon/touch-icon-192.png">
    <link rel="icon" sizes="32x32" href="/themes/custom/govwales/favicon/favicon-32.png">
    <link rel="icon" sizes="48x48" href="/themes/custom/govwales/favicon/favicon-48.png">
    <meta name="msapplication-TileColor" content="#b60404">
    <meta name="msapplication-TileImage" content="/themes/custom/govwales/favicon/ms-icon-144x144.png">

    <title>Home | GOV.WALES</title>

    <link rel="stylesheet" media="all" href="/sites/default/files/css/css_-T6IDwHVOHnL2_K4dzskqaeQ2UPH8-jJxFSYJ5orFNY.css?q1ddpo" />
<link rel="stylesheet" media="all" href="/sites/default/files/css/css_XTVTrK3hyoirz0vDUv-K2epF9YZ-Ryy0moNxFDa0pvI.css?q1ddpo" />

    
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

<!--[if lte IE 8]>
<script src="/modules/contrib/respondjs/lib/respond.min.js?v=1.4.2"></script>
<![endif]-->
<script src="/sites/default/files/js/js_3KOrO4Ww6IO1xutFjJXt_EmChmgAZ2EhqrlxszFNDA0.js"></script>

  </head>
    <body class="path-frontpage page-node-type-page lang-en">
        <a href="#main-content" class="visually-hidden focusable">
      Skip to main content
    </a>
    <noscript><iframe id="gtm-iframe" src="https://www.googletagmanager.com/ns.html?id=GTM-MBLNKRW" height="0" width="0"></iframe></noscript>
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    

<div id="page_wrapper"  class="layout-container">

  <div role="banner" id="wg_cookie" class="wg_cookie">
      <div>
    <div id="block-govwales-site-config-cookie-notice"  id="block-cookiecontrolblock" class="container-fluid">
<p>This site uses cookies to make the site simpler. <a href="https://gov.wales/help/cookies">Find out more about cookies</a></p>
</div>
  </div>

  </div>

  <header role="banner" id="wg_header" class="wg_header">
        <div class="layout-container">
  <header class="header" id="header" role="banner">
    <div class="header__components container-fluid">
        <div id="block-govwales-branding">
  
    
      <a href="https://gov.wales/" title="Welsh Government" rel="govwales" class="header__logo" id="logo"><span class="visually-hidden">Home</span></a><span class="print header__logo_print"><img src="/themes/custom/govwales/images/logos/site-logo/png/wg_logo_print.png" alt="Welsh Government"></span>
</div>
<div class="views-exposed-form components__form" data-drupal-selector="views-exposed-form-global-keyword-search-search-page" id="block-exposedformglobal-keyword-searchsearch-page">
      <form action="https://gov.wales/search" method="get" id="views-exposed-form-global-keyword-search-search-page" accept-charset="UTF-8">
  <div class="js-form-item form-item js-form-type-textfield form-item-global-keywords js-form-item-global-keywords">

            <label for="edit-global-keywords">Search</label>
                  
  <input data-drupal-selector="edit-global-keywords" type="text" id="edit-global-keywords" name="global-keywords" value="" size="30" maxlength="128" class="form-text" />


                          </div>
<div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions"><input data-drupal-selector="edit-submit-global-keyword-search" type="submit" id="edit-submit-global-keyword-search" value="Search" class="button js-form-submit form-submit" />
</div>


</form>

  </div>
<div class="language-switcher-language-url" id="block-govwales-languageswitcher" role="navigation">
  
    
      <ul class="links"><li hreflang="en" data-drupal-link-system-path="&lt;front&gt;" class="en is-active"><a href="https://gov.wales/" class="language-link is-active" hreflang="en" data-drupal-link-system-path="&lt;front&gt;">English</a></li><li hreflang="cy" data-drupal-link-system-path="&lt;front&gt;" class="cy"><a href="https://llyw.cymru/" class="language-link" hreflang="cy" data-drupal-link-system-path="&lt;front&gt;">Cymraeg</a></li></ul>
  </div>

    </div>
  </header>


  </header>

  <main role="main" id="wg_main" class="wg_main">

    <a id="main-content" tabindex="-1"></a>    <div id="main__body" class="main__body container-fluid home">

                      <div class="pre-content">
    <div data-drupal-messages-fallback class="hidden"></div>

  </div>

            <div class="region-content">
      <div id="block-logoblock">
  
    
      
  </div>
<nav role="navigation" aria-labelledby="block-govwales-campaigns-menu" id="block-govwales-campaigns">
            
  <h2 class="visually-hidden" id="block-govwales-campaigns-menu">Campaigns</h2>
  

        
    


  </nav>
<div id="block-govwales-content">
  
    
      
<article role="article" about="https://gov.wales/" typeof="schema:WebPage">

  <div>
          
  </div>

</article>

  </div>

  </div>


      <div class="home__title container-fluid">
        <h1>
          <span class="visually-hidden">GOV.WALES</span>
          <span class="print"><img src="/themes/custom/govwales/images/logos/logo-gov-wales.png" alt="GOV.WALES"></span>
        </h1>
        <h2>Welsh Government services and information</h2>
      </div>
      <div class="container-fluid content-entity home__services">
        <div class="row">
          <div class="col-xs-12 col-lg-8 list-group list-group--narrow-column">
            <div class="row">
              <div class="views-element-container" id="block-views-block-policy-areas-top-level">
  
    
      <div><div class="js-view-dom-id-aeabd32941ac4ab27c1ff5aa3c57298d11fa1b00fb6473e9c34cfa8d1ef359c9">
  
  
  

      <header>
      
    </header>
  
  
  

  
    <div  class="col-xs-12 col-md-6">
    <ul>
                      <li >
          <a href="https://gov.wales/arts-culture-sport" hreflang="en">Arts, culture and sport</a><p>Free swimming, museums and historic environment</p>
        </li>
                      <li >
          <a href="https://gov.wales/building-planning" hreflang="en">Building and planning</a><p>Planning permission, building regulations, local development plans</p>
        </li>
                      <li >
          <a href="https://gov.wales/business-economy-innovation" hreflang="en">Business, economy and innovation</a><p>Business support and grants, city regions, tourism, digital, broadband and mobile</p>
        </li>
                      <li >
          <a href="https://gov.wales/children-families" hreflang="en">Children and families</a><p>Flying Start, childcare, help for families, children's rights</p>
        </li>
                      <li >
          <a href="https://gov.wales/communities-regeneration" hreflang="en">Communities and regeneration</a><p>Communities facilities programme, taskforce for the valleys, third (voluntary) sector</p>
        </li>
                      <li >
          <a href="https://gov.wales/education-skills" hreflang="en">Education and skills</a><p>Schools, further and higher education, skills and vocational training, student funding</p>
        </li>
                      <li >
          <a href="https://gov.wales/environment-climate-change" hreflang="en">Environment and climate change</a><p>Recycling, flooding, pollution and emissions</p>
        </li>
                      <li >
          <a href="https://gov.wales/equality" hreflang="en">Equality</a><p>Poverty, domestic abuse and sexual violence, diversity</p>
        </li>
                      <li >
          <a href="https://gov.wales/farming-countryside" hreflang="en">Farming and countryside</a><p>RPW Online, rural payments, animal movements, animal health</p>
        </li>
          </ul>
  </div>
    <div  class="col-xs-12 col-md-6">
    <ul>
                      <li >
          <a href="https://gov.wales/health-social-care" hreflang="en">Health and social care</a><p>Health conditions, NHS management, social care</p>
        </li>
                      <li >
          <a href="https://gov.wales/housing" hreflang="en">Housing</a><p>Letting your property, help to buy a home, home improvement loans</p>
        </li>
                      <li >
          <a href="https://gov.wales/international-eu" hreflang="en">International and EU</a><p>European Structural and Investment Funds, Brexit</p>
        </li>
                      <li >
          <a href="https://gov.wales/justice-and-law" hreflang="en">Justice and law</a><p>Justice policy, prisoners and probation</p>
        </li>
                      <li >
          <a href="https://gov.wales/marine-fisheries" hreflang="en">Marine and fisheries</a><p>Marine planning, maritime and fisheries fund, fishing licences, marine conservation</p>
        </li>
                      <li >
          <a href="https://gov.wales/public-sector" hreflang="en">Public sector</a><p>Local government, public bodies, open government and transparency</p>
        </li>
                      <li >
          <a href="https://gov.wales/transport" hreflang="en">Transport</a><p>Bus passes, road improvements, local transport plans, cycling and walking</p>
        </li>
                      <li >
          <a href="https://gov.wales/welsh-language" hreflang="en">Welsh language</a><p>Welsh Language Tribunal, promoting the Welsh language</p>
        </li>
          </ul>
  </div>

    

  
  

  
  
</div>
</div>

  </div>

            </div>
          </div>
          <div class="services__col-2 col-xs-12 col-lg-4">
                          <div class="services__latest_news index-list-latest list-group list-group--bdr-top-sm">
                <h3>Announcements</h3>
                <div class="latest_news__items"><div class="views-element-container" id="block-views-block-announcements-home-latest-announcements">
  
    
      <div><div class="js-view-dom-id-d4fd2e1372eb6db556d9385ed4f0f5e933f664f0fe6f0bab5c3575f028ce4aa1">
  
  
  

  
  
  

      <div class="views-row"><span class="views-field views-field-title"><span class="field-content"><a href="https://gov.wales/66-active-travel-projects-benefit-ps145m-funding" hreflang="en">66 active travel projects to benefit from £14.5 million of funding</a></span></span><div class="views-field views-field-field-last-updated"><div class="field-content">2 hours ago</div></div></div>
    <div class="views-row"><span class="views-field views-field-title"><span class="field-content"><a href="https://gov.wales/lesley-griffiths-confirms-intention-continue-basic-payment-scheme-2021" hreflang="en">Lesley Griffiths confirms intention to continue Basic Payment Scheme in 2021</a></span></span><div class="views-field views-field-field-last-updated"><div class="field-content">12 hours ago</div></div></div>
    <div class="views-row"><span class="views-field views-field-title"><span class="field-content"><a href="https://gov.wales/new-conversion-programme-welsh-medium-primary-teachers-who-want-teach-secondary-schools" hreflang="en">New ‘conversion’ programme for Welsh-medium primary teachers who want to teach in secondary schools</a></span></span><div class="views-field views-field-field-last-updated"><div class="field-content">12 hours ago</div></div></div>

    

  
  

  
  
</div>
</div>

  </div>
</div>
                <div class="view_all"><a href="https://gov.wales/announcements">View more</a></div>
              </div>
                        <div class="services__browse_by index-list-latest list-group">
              <h3>Browse by</h3>
              <ul>
                <li><a href="https://gov.wales/announcements/search">Announcements</a></li>
                <li><a href="https://gov.wales/consultations">Consultations</a></li>
                <li><a href="https://gov.wales/publications">Publications</a></li>
                <li><a href="https://gov.wales/statistics-and-research">Statistics and research</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End home__services block -->

              <div class="container-fluid content-entity home__ctas">
          <div class="row">
            <div class="col-md-4 col-md-push-8 col-sm-12 col-xs-12">
              <div class="content-entity__outer">
                <div class="list-group">
                  <h3>Organisations</h3>
                </div>
                <div>This includes Welsh Government public bodies, advisory groups and companies.</div>
                <div class="home__organisations">
                  <div class="views-element-container"><div class="js-view-dom-id-a2d7b8816e73992111c207c76a11bbeb24672d7b8ed269d92fadda28dd75484a">
  
  
  

      <header>
      <a href="/organisations"><div class="home__organisations--total"><span>140 </span></div><div class="home__organisations--label"><span>organisations </span><span>including:<span></div></a>
    </header>
  
  
  

      <div class="views-row"><span class="views-field views-field-title"><div class="field-content index-list__title"><a href="https://gov.wales/welsh-revenue-authority" hreflang="en">Welsh Revenue Authority</a></div></span></div>
    <div class="views-row"><span class="views-field views-field-title"><div class="field-content index-list__title"><a href="https://gov.wales/transport-wales" hreflang="en">Transport for Wales</a></div></span></div>
    <div class="views-row"><span class="views-field views-field-title"><div class="field-content index-list__title"><a href="https://gov.wales/welsh-language-commissioner" hreflang="en">Welsh Language Commissioner</a></div></span></div>
    <div class="views-row"><span class="views-field views-field-title"><div class="field-content index-list__title"><a href="https://gov.wales/qualifications-wales" hreflang="en">Qualifications Wales</a></div></span></div>
    <div class="views-row"><span class="views-field views-field-title"><div class="field-content index-list__title"><a href="https://gov.wales/cafcass-cymru" hreflang="en">Cafcass Cymru</a></div></span></div>

    

  
  

  
  
</div>
</div>

                  <div class="view_all"><a href="https://gov.wales/organisations">View all organisations</a></div>
                </div>
              </div>
            </div>
            <div class="col-md-8 col-md-pull-4 col-sm-12 col-xs-12">
              <div class="content-entity__outer">
                <div class="list-group">
                  <h3>In focus</h3>
                </div>
                <div class="row cta-blocks">
                  <div class="cta_wrapper">
                                                                  <div class="col-xs-12 col-sm-6 col-md-6 cta mt-30">
  <a class="cta__link" href="https://gov.wales/preparing-wales">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2019-01/preparing%20wales%20hompage%20CTA.jpg" width="602" height="402" alt="Preparing Wales" typeof="foaf:Image" />

</div>
      
    </div>
    Preparing Wales
  </a>
  <span class="cta__desc">
            <div>What we are doing to prepare for a no deal Brexit</div>
      </span>
</div>

                                                                                        <div class="col-xs-12 col-sm-6 col-md-6 cta mt-30">
  <a class="cta__link" href="https://gov.wales/abolishing-defence-reasonable-punishment-children-overview">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2019-04/kid6.jpg" width="540" height="360" alt="The Children Wales Bill" typeof="foaf:Image" />

</div>
      
    </div>
    The Children Wales Bill
  </a>
  <span class="cta__desc">
            <div>Ending the physical punishment of children in Wales.</div>
      </span>
</div>

                                                                                        <div class="col-xs-12 col-sm-6 col-md-6 cta mt-30">
  <a class="cta__link" href="https://gov.wales/pay-less-council-tax">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2018-07/pay-less-council-tax-cta.jpg" width="600" height="400" alt="Pay Less Council Tax " typeof="foaf:Image" />

</div>
      
    </div>
    Pay Less Council Tax 
  </a>
  <span class="cta__desc">
            <div>Are you eligible to pay less Council Tax? </div>
      </span>
</div>

                                                                                        <div class="col-xs-12 col-sm-6 col-md-6 cta mt-30">
  <a class="cta__link" href="https://gov.wales/dont-let-money-get-in-the-way-of-university">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2018-07/student-cta.jpg" width="600" height="400" alt="Student Finance" typeof="foaf:Image" />

</div>
      
    </div>
    Student Finance
  </a>
  <span class="cta__desc">
            <div>Don&#039;t let money get in the way of university</div>
      </span>
</div>

                                                                                                                                                                                                                                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- End home__cta block -->
        <div class="container-fluid">
          <div class="content-entity__outer">
            <div class="list-group">
              <h3>Welsh government</h3>
            </div>
            <div class="row cta-blocks">
              <div class="cta_wrapper">
                                                                                                                                                                                              <div class="col-xs-12 col-sm-6 col-md-3 cta mt-30">
  <a class="cta__link" href="https://gov.wales/about-us">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2017-09/about.jpg" width="601" height="400" alt="About us" typeof="foaf:Image" />

</div>
      
    </div>
    About us
  </a>
  <span class="cta__desc"></span>
</div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-3 cta mt-30">
  <a class="cta__link" href="/ministers">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2018-12/WG%20RED%20LOGO%20503%20X%20336.png" width="503" height="336" alt="Ministers" typeof="foaf:Image" />

</div>
      
    </div>
    Ministers
  </a>
  <span class="cta__desc"></span>
</div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-3 cta mt-30">
  <a class="cta__link" href="/vacancies">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2017-10/job-2.jpg" width="210" height="140" alt="Vacancies" typeof="foaf:Image" />

</div>
      
    </div>
    Jobs
  </a>
  <span class="cta__desc"></span>
</div>

                                                                        <div class="col-xs-12 col-sm-6 col-md-3 cta mt-30">
  <a class="cta__link" href="https://gov.wales/our-priorities">
    <div class="cta__image">
      
            <div>  <img src="/sites/default/files/cta-blocks/2017-09/cta-home-lower-our-priorities-v2.jpg" width="600" height="400" alt="Our priorities" typeof="foaf:Image" />

</div>
      
    </div>
    Our priorities
  </a>
  <span class="cta__desc"></span>
</div>

                                                </div>
            </div>
          </div>
        </div>
        <!-- End home__hightlighted block -->
            
      
    </div>
  </main>

        <section class="pre_sharebar" id="presharebar" role="banner">
    <div class="pre_sharebar__components container-fluid">
        
    </div>
  </section>

  
      <div role="banner" id="wg_pagefeedback" class="wg_pagefeedback">
        <div>
    <div id="block-pagefeedbackblock">
  
    
      <div  class="container-fluid">
  <a href="mailto:digital@gov.wales?url=https%3A//gov.wales/&amp;subject=Feedback%20on%20https%3A//gov.wales/&amp;body=Page%20URL%3A%20https%3A//gov.wales/%0APage%20Title%3A%20Home%20%7C%20GOV.WALES%0AYour%20feedback%3A%0A" id="page-feedback-link">Give feedback about this page</a>
</div>

  </div>

  </div>

    </div>
  
      
  <section class="sharebar" id="sharebar" role="banner">
    <div class="sharebar__components container-fluid">
            <div class="main__sharebar">
      <div  class="block-share">
      <h4>Share this page</h4>
    <ul class="block-share-list active">
          <li><a href="https://twitter.com/share?url=https%3A//gov.wales/" class="twitter">Twitter</a></li>
          <li><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//gov.wales/" class="facebook">Facebook</a></li>
          <li><a href="mailto:?body=https%3A//gov.wales/&amp;subject=Shared%20from%20gov.wales" class="email">Email</a></li>
      </ul>
  </div>

    <div id="sharebar__backtotop" class="sharebar__backtotop btn--outlined btn--arrow-up mb-15" ><a href="#page_wrapper" >Back to top</a></div>
</div>

        </div>
  </section>



  
      
  <footer class="footer" id="footer" role="banner">
    <div class="footer__components container-fluid">
            <nav role="navigation" aria-labelledby="block-footer-menu" id="block-footer">
            
  <h2 class="visually-hidden" id="block-footer-menu">Global Footer</h2>
  

        
              <ul class="menu clearfix">
              <li class="menu__item">
        <a href="https://gov.wales/contact-us" data-drupal-link-system-path="node/19808">Contact us</a>
              </li>
          <li class="menu__item">
        <a href="https://gov.wales/help/accessibility" title="Accessibility" data-drupal-link-system-path="node/17">Accessibility</a>
              </li>
          <li class="menu__item">
        <a href="https://gov.wales/help/cookies" title="Cookies" data-drupal-link-system-path="node/18">Cookies</a>
              </li>
          <li class="menu__item">
        <a href="https://gov.wales/copyright-statement" title="Copyright statement" data-drupal-link-system-path="node/15">Copyright statement</a>
              </li>
          <li class="menu__item">
        <a href="https://gov.wales/website-privacy-policy" title="Privacy policy" data-drupal-link-system-path="node/16">Website privacy policy</a>
              </li>
          <li class="menu__item">
        <a href="https://gov.wales/terms-and-conditions" title="Terms and conditions" data-drupal-link-system-path="node/19">Terms and conditions</a>
              </li>
        </ul>
  


  </nav>

        </div>
    <div class="footer_logo container-fluid" id="footer_logo">
      <a href="https://gov.wales/" rel="home" class="footer__logo" id="footerlogo"><span class="visually-hidden">Home</span></a>
    </div>
  </footer>



  
</div>
  </div>

    
    <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/103","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","ajaxTrustedUrl":{"https:\/\/gov.wales\/search":true},"user":{"uid":0,"permissionsHash":"b22eac911f7e0d81e959a2cda4dd88a070a960dc11cd5c33821ec519d208b586"}}</script>
<script src="/sites/default/files/js/js_eJBaUnco-HIX1rhNbtn38OZea88p7ffxShKgpGWKwmU.js"></script>

    <div id="detect__breakpoint">
      <div class="breakpoint device-xs"></div>
      <div class="breakpoint device-sm"></div>
      <div class="breakpoint device-md"></div>
      <div class="breakpoint device-lg"></div>
    </div>
  <script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"42e0ea2850","applicationID":"108008856","transactionName":"MgYBYURUXRICUxALWwtMNkdfGloPB1UcTEQNEw==","queueTime":0,"applicationTime":199,"atts":"HkECFwxOThw=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
