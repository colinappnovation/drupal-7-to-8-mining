<!doctype html>

<!--[if IEMobile 7 ]> <html lang="en-GB"class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html lang="en-GB" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en-GB" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-GB" class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-GB" class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>We Welcome You to YMCA Derbyshire</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<link rel="pingback" href="https://ymcaderbyshire.org.uk/xmlrpc.php">
		<!-- wordpress head functions -->
		
<!-- This site is optimized with the Yoast SEO plugin v12.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="We welcome you to YMCA Derbyshire. We have been at the heart of the community in Derbyshire since 1847, supporting young people in a variety of ways."/>
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
<link rel="canonical" href="https://ymcaderbyshire.org.uk/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="We Welcome You to YMCA Derbyshire" />
<meta property="og:description" content="We welcome you to YMCA Derbyshire. We have been at the heart of the community in Derbyshire since 1847, supporting young people in a variety of ways." />
<meta property="og:url" content="https://ymcaderbyshire.org.uk/" />
<meta property="og:site_name" content="YMCA Derbyshire" />
<meta property="og:image" content="https://ymcaderbyshire.org.uk/wp-content/uploads/JJ-social-banner-1024x379.jpg" />
<meta property="og:image:secure_url" content="https://ymcaderbyshire.org.uk/wp-content/uploads/JJ-social-banner-1024x379.jpg" />
<meta property="og:image:width" content="1024" />
<meta property="og:image:height" content="379" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="We welcome you to YMCA Derbyshire. We have been at the heart of the community in Derbyshire since 1847, supporting young people in a variety of ways." />
<meta name="twitter:title" content="We Welcome You to YMCA Derbyshire" />
<meta name="twitter:image" content="https://ymcaderbyshire.org.uk/wp-content/uploads/JJ-social-banner.jpg" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://ymcaderbyshire.org.uk/#website","url":"https://ymcaderbyshire.org.uk/","name":"YMCA Derbyshire","potentialAction":{"@type":"SearchAction","target":"https://ymcaderbyshire.org.uk/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://ymcaderbyshire.org.uk/#primaryimage","url":"https://ymcaderbyshire.org.uk/wp-content/uploads/JJ-social-banner.jpg","width":3546,"height":1313},{"@type":"WebPage","@id":"https://ymcaderbyshire.org.uk/#webpage","url":"https://ymcaderbyshire.org.uk/","inLanguage":"en-GB","name":"We Welcome You to YMCA Derbyshire","isPartOf":{"@id":"https://ymcaderbyshire.org.uk/#website"},"primaryImageOfPage":{"@id":"https://ymcaderbyshire.org.uk/#primaryimage"},"datePublished":"2016-11-21T20:26:13+00:00","dateModified":"2019-11-20T14:10:56+00:00","description":"We welcome you to YMCA Derbyshire. We have been at the heart of the community in Derbyshire since 1847, supporting young people in a variety of ways."}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://ymcaderbyshire.org.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.2.4' type='text/css' media='all' />
<link rel='stylesheet' id='wpbs-css'  href='https://ymcaderbyshire.org.uk/wp-content/themes/ymca/library/dist/css/styles.f6413c85.min.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='wpbs-style-css'  href='https://ymcaderbyshire.org.uk/wp-content/themes/ymca/style.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='https://ymcaderbyshire.org.uk/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.7' type='text/css' media='all' />
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-content/themes/ymca/bower_components/bootstrap/dist/js/bootstrap.js?ver=1.2'></script>
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-content/themes/ymca/library/dist/js/scripts.d1e3d952.min.js?ver=1.2'></script>
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-content/themes/ymca/bower_components/modernizer/modernizr.js?ver=1.2'></script>
<link rel='https://api.w.org/' href='https://ymcaderbyshire.org.uk/wp-json/' />
<link rel='shortlink' href='https://ymcaderbyshire.org.uk/' />
<link rel="alternate" type="application/json+oembed" href="https://ymcaderbyshire.org.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fymcaderbyshire.org.uk%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://ymcaderbyshire.org.uk/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fymcaderbyshire.org.uk%2F&#038;format=xml" />
<link rel="stylesheet" href="/wp-content/themes/ymca/library/font-awesome-4.7.0/css/font-awesome.min.css"><script src="/wp-content/themes/ymca/library/js/ymca_scripts.js"></script>
<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/ymca/images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/ymca/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/ymca/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/ymca/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/ymca/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/ymca/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/ymca/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/ymca/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/ymca/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/ymca/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/ymca/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/ymca/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/ymca/images/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/wp-content/themes/ymca/images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">      <script type='text/javascript' src='/wp-content/themes/ymca/vc-elements/assets/social_icons.js'></script>
      <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://ymcaderbyshire.org.uk/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1484776171746{margin-bottom: 35px !important;}.vc_custom_1488288111929{margin-bottom: 35px !important;}.vc_custom_1480437256246{margin-top: 10px !important;background-image: url(https://www.ymca.org.uk/wp-content/uploads/2016/04/Donate-background.jpg?id=14849) !important;}.vc_custom_1467798951155{padding-top: 10px !important;padding-bottom: 20px !important;}.vc_custom_1571832539575{margin-bottom: 15px !important;}.vc_custom_1468593457083{padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1468593466587{padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1468593473519{padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1571832396684{margin-bottom: 20px !important;padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1571832496703{padding-right: 10px !important;padding-left: 10px !important;}.vc_custom_1531238282222{margin-bottom: 20px !important;padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1531238116575{padding-right: 10px !important;padding-left: 10px !important;}.vc_custom_1573552021477{margin-bottom: 20px !important;padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1536589205035{padding-right: 10px !important;padding-left: 10px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>		<!-- end of wordpress head -->
		<!-- IE8 fallback moved below head to work properly. Added respond as well. Tested to work. -->
			<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

			<!-- respond.js -->
		<!--[if lt IE 9]>
		          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
		<![endif]-->
	</head>

	<body class="home page-template-default page page-id-5 wpb-js-composer js-comp-ver-5.7 vc_responsive">
		<header role="banner">

			<div class="navbar navbar-default navbar-static-top">
				<div class="top_nav_wrapper">
					<div class="container">
						<div class="col-sm-6 left">
							<a href="/" class="title">YMCA Derbyshire</a>
						</div>
						<div class="col-sm-6 right">
							<div class="top_button_nav clearfix"><ul id="menu-top-button-links" class="menu"><li id="menu-item-2183" class="btn_style_b menu-item menu-item-type-post_type menu-item-object-page first last menu-item-2183"><a href="https://ymcaderbyshire.org.uk/support-us/donate/">Donate</a></li>
</ul></div>							<div class="top_social_nav clearfix"><ul id="menu-top-social-links" class="menu"><li id="qwertymenu-item-18" class="menu-item menu-item-type-custom menu-item-object-custom first"><a href="https://www.facebook.com/YMCADerbyshire" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://twitter.com/YMCADerbyshire" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.youtube.com/user/ymcaengland" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-4532" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.linkedin.com/company/ymca-derbyshire/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-5513" class="menu-item menu-item-type-custom menu-item-object-custom last"><a href="https://www.instagram.com/ymca_derbyshire/?hl=en" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
</ul></div>						</div>
					</div>
				</div>
				<div class="container">

					<div class="navbar-header">


						<a class="navbar-brand" title="At the heart of the community in Derbyshire since 1847" href="https://ymcaderbyshire.org.uk">
							<img class=" preload-me" src="/wp-content/themes/ymca/images/ymca-logo.png" srcset="/wp-content/themes/ymca/images/ymca-logo.png 150w, /wp-content/themes/ymca/images/ymca-logo-retina.png 299w" width="150" height="44" sizes="150px" alt="YMCA England">
						</a>
					</div>
					<div class="btn_search_nav">
					<div class="top_button_nav clearfix"><ul id="menu-top-button-links-1" class="menu"><li class="btn_style_b menu-item menu-item-type-post_type menu-item-object-page first last menu-item-2183"><a href="https://ymcaderbyshire.org.uk/support-us/donate/">Donate</a></li>
</ul></div>					<a href="#" class="search_btn"><i class="fa fa-search" aria-hidden="true"></i></a>
					<div class="search_wrapper">
				<form class="navbar-form navbar-right search" role="search" method="get" id="searchform" action="https://ymcaderbyshire.org.uk/">
					<div class="form-group">
						<input name="s" id="s" type="text" class="search-query form-control" autocomplete="off" placeholder="Search...">
					</div>
				</form>
			</div>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

					<div class="collapse-main navbar-collapse-main navbar-responsive-collapse-main">
							<i class="fa fa-times" aria-hidden="true"></i>
							<div class="nav_wrapper_main">
								<ul id="menu-main-navigation" class="nav navbar-nav"><li id="menu-item-2167" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children first"><a href="https://ymcaderbyshire.org.uk/about/" class="dropdown-toggle disabled" data-toggle="dropdown">About</a>
<ul class="dropdown-menu">
	<li id="menu-item-2169" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://ymcaderbyshire.org.uk/about/impact-of-ymca-derbyshire/" class="dropdown-toggle disabled" data-toggle="dropdown">Our Impact</a>
	<ul class="dropdown-menu">
		<li id="menu-item-4737" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/about/impact-of-ymca-derbyshire/meet-drew/">Meet Drew</a></li>
		<li id="menu-item-4759" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/meet-nadia/">Meet Nadia</a></li>
		<li id="menu-item-4753" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/meet-declan/">Meet Declan</a></li>
		<li id="menu-item-4774" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/meet-kat/">Meet Kat</a></li>
		<li id="menu-item-4788" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/meet-nathan/">Meet Nathan</a></li>
		<li id="menu-item-4787" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/meet-sam/">Meet Sam</a></li>
	</ul>
</li>
	<li id="menu-item-2172" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/about/vision-missions-values/">Vision, Mission and Values</a></li>
	<li id="menu-item-2173" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/about/history-and-heritage-of-ymca-derbyshire/">History and Heritage</a></li>
	<li id="menu-item-2180" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/news/">Our News</a></li>
	<li id="menu-item-2734" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/contact-us/">Contact Us</a></li>
</ul>
</li>
<li id="menu-item-2174" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://ymcaderbyshire.org.uk/what-we-do/" class="dropdown-toggle disabled" data-toggle="dropdown">What We Do</a>
<ul class="dropdown-menu">
	<li id="menu-item-2250" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/what-we-do/accommodation/">Accommodation</a></li>
	<li id="menu-item-2253" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/what-we-do/support-advice/">Support and Advice</a></li>
	<li id="menu-item-2252" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/what-we-do/health-and-wellbeing/">Health and Wellbeing</a></li>
	<li id="menu-item-2526" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/what-we-do/communityhub/">Community Campus</a></li>
	<li id="menu-item-4538" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.keycollege.co.uk">Key College</a></li>
	<li id="menu-item-4872" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/what-we-do/communityhub/claudes-cafe-marble-hall/">Claude’s Cafe</a></li>
	<li id="menu-item-2251" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/y-kidz/">Y-Kidz</a></li>
</ul>
</li>
<li id="menu-item-4222" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.keycollege.co.uk">Key College</a></li>
<li id="menu-item-3702" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://ymcaderbyshire.org.uk/y-kidz/" class="dropdown-toggle disabled" data-toggle="dropdown">Y-Kidz</a>
<ul class="dropdown-menu">
	<li id="menu-item-3974" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/y-kidz/markeaton-primary-school/activity-club/">Holiday Activity Club at Markeaton</a></li>
	<li id="menu-item-3971" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/y-kidz/markeaton-primary-school/">Wraparound Care at Markeaton</a></li>
	<li id="menu-item-3976" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/y-kidz/st-josephs-catholic-primary-school/">St Joseph&#8217;s Catholic Primary School</a></li>
	<li id="menu-item-3975" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/y-kidz/school-services/">School Services</a></li>
</ul>
</li>
<li id="menu-item-2376" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://ymcaderbyshire.org.uk/support-us/" class="dropdown-toggle disabled" data-toggle="dropdown">Support Us</a>
<ul class="dropdown-menu">
	<li id="menu-item-2407" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/events/">Sleep Easy</a></li>
	<li id="menu-item-2177" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/become-a-friend-of-ymca/">Become a Friend of YMCA</a></li>
	<li id="menu-item-2179" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/corporate-partnerships/">Corporate Partnerships</a></li>
	<li id="menu-item-2406" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/donate/">Donate</a></li>
	<li id="menu-item-3793" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/join-our-community-meal/">Join our Community Meal</a></li>
	<li id="menu-item-2408" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/support-us/leave-a-gift-in-your-will/">Leave a Gift in Your Will</a></li>
</ul>
</li>
<li id="menu-item-2413" class="dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://ymcaderbyshire.org.uk/join-our-team/" class="dropdown-toggle disabled" data-toggle="dropdown">Join Our Team</a>
<ul class="dropdown-menu">
	<li id="menu-item-2409" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://ymcaderbyshire.org.uk/join-our-team/volunteer/">Become an iVolunteer</a></li>
	<li id="menu-item-5544" class="menu-item menu-item-type-post_type menu-item-object-page last"><a href="https://ymcaderbyshire.org.uk/join-our-team/current-vacancies/">Job Vacancies</a></li>
</ul>
</li>
</ul>							</div>

											</div>

				</div> <!-- end .container -->
			</div> <!-- end .navbar -->

		</header> <!-- end header -->



		<a href="https://ymcaderbyshire.org.uk/support-ymca-derbyshire-by-wearing-a-jolly-jumper-this-christmas/" >		<div class="landing_bg">
						<img src="https://ymcaderbyshire.org.uk/wp-content/uploads/JJ-social-banner.jpg" />
		</div>
		</a>
		<div class="container main">
			
			<p class="intro">We welcome you to YMCA Derbyshire. 
We have been at the heart of the community in Derbyshire since 1847. While the scope of our work has changed considerably over the years, our vision has't. Our holistic development in body, mind and spirit and our mission to break down barriers to achievement and help local people to fulfil their potential has remained our constant purpose.</p>			<div class="landing_bg_tri">
				<img src="/wp-content/themes/ymca/images/bg_arrow_landing.png">
			</div>
			
			<div id="content" class="clearfix row">

				<div id="main" class="col-sm-12 clearfix" role="main">

					
					<article id="post-5" class="clearfix post-5 page type-page status-publish has-post-thumbnail hentry first last" role="article">

						<header>

							


						</header>

						<section class="row post_content">

							<div class="col-sm-12">

								<div class="vc_row wpb_row vc_row-fluid vc_custom_1484776171746"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_video_widget wpb_content_element vc_clearfix   vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-center" >
		<div class="wpb_wrapper">
			
			<div class="wpb_video_wrapper"><iframe width="580" height="326" src="https://www.youtube.com/embed/seHwirFfo4Q?wmode=transparent&amp;rel=0&amp;feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
        <div class="ymca_wwd_home_wrapper ">
          <div class="ymca_wwd_home_img" style="background:url(https://ymcaderbyshire.org.uk/wp-content/uploads/wwd_home.jpg) no-repeat center center;">
          <img src="https://ymcaderbyshire.org.uk/wp-content/uploads/wwd_home.jpg" style="visibility:hidden;">
          </div>
          <div class="ymca_wwd_home_content">
            <h3 class="ymca_wwd_home_title">What We Do</h3>
            <a href="http://ymcaderbyshire.org.uk/about/what-we-do/accommodation/" class="blue">Accommodation</a>
            <a href="http://ymcaderbyshire.org.uk/about/what-we-do/support-advice/" class="yellow">Support and Advice</a>
            <a href="http://ymcaderbyshire.org.uk/about/what-we-do/health-and-wellbeing/" class="green">Health &amp; Wellbeing</a>
            <a href="http://ymcaderbyshire.org.uk/about/what-we-do/training-and-education/" class="purple">Training &amp; Education</a>
            <a href="http://ymcaderbyshire.org.uk/about/what-we-do/family-work/" class="magenta">Family Work</a>
          </div>
        </div>
        </div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1488288111929"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8 vc_hidden-xs"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h2 style="text-align: center;">What we are all about</h2>
<p>Today YMCA Derbyshire is an independent Christian charity committed to helping people at times of need, regardless of their gender, race, ability or faith. We provide accommodation at our Campus and in the community. Our supported housing for people facing homelessness and life crises is highly regarded and often praised.</p>
<p>Other core offerings include <a href="http://www.keycollege.co.uk/">Key College</a>, which provides a host of innovative Training &amp; Education courses for young people aged 16 -19 (and up to 24 in some instances).</p>
<p>Our recent ‘GOOD’ Ofsted report drew attention to: “good quality teaching” “staff provide a caring supportive environment” “learners make good progress and a large majority go on to further education and training or jobs after their study programme”.</p>
<p>YMCA Derbyshire also offer a variety of schemes for children and their families, projects aimed at achieving sustainable community development and schemes aimed at improving individual’s employment skills leading to a paid job.</p>
<p>We are affiliated to the national and international YMCA movement, and our values to protect, trust, hope and persevere underpin all of our services across the county.</p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1480437256246 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h2 style="color: #ffffff;text-align: center" class="vc_custom_heading" ><a href="/support-us/donate/">Make a donation</a></h2></div></div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1467798951155"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h2 style="text-align: center" class="vc_custom_heading vc_custom_1571832539575" >Find what you're looking for?</h2><div class="vc_row wpb_row vc_inner vc_row-fluid vc_column-gap-20 vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner vc_custom_1468593457083"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1571832396684">
		
		<figure class="wpb_wrapper vc_figure">
			<a href="https://ymcaderbyshire.org.uk/dancing-the-night-away-at-our-annual-ceilidh/" target="_self" class="vc_single_image-wrapper   vc_box_border_grey"><img width="372" height="248" src="https://ymcaderbyshire.org.uk/wp-content/uploads/TE-image-for-banner-1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://ymcaderbyshire.org.uk/wp-content/uploads/TE-image-for-banner-1.jpg 372w, https://ymcaderbyshire.org.uk/wp-content/uploads/TE-image-for-banner-1-65x43.jpg 65w, https://ymcaderbyshire.org.uk/wp-content/uploads/TE-image-for-banner-1-300x200.jpg 300w, https://ymcaderbyshire.org.uk/wp-content/uploads/TE-image-for-banner-1-343x229.jpg 343w" sizes="(max-width: 372px) 100vw, 372px" /></a>
		</figure>
	</div>
<h3 style="color: #4d4f53;text-align: center" class="vc_custom_heading" ><a href="https://ymcaderbyshire.org.uk/support-us/become-a-friend-of-ymca/">Become a friend of YMCA Derbyshire!</a></h3>
	<div class="wpb_text_column wpb_content_element  vc_custom_1571832496703" >
		<div class="wpb_wrapper">
			<p style="text-align: center;">Make a small monthly donation to help change the lives of young and vulnerable people in Derby.</p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner vc_custom_1468593466587"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1531238282222">
		
		<figure class="wpb_wrapper vc_figure">
			<a href="https://www.keycollege.co.uk/" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey"><img width="6016" height="4000" src="https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409.jpg 6016w, https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409-65x43.jpg 65w, https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409-300x199.jpg 300w, https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409-768x511.jpg 768w, https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409-1024x681.jpg 1024w, https://ymcaderbyshire.org.uk/wp-content/uploads/DSC_0409-343x229.jpg 343w" sizes="(max-width: 6016px) 100vw, 6016px" /></a>
		</figure>
	</div>
<h3 style="color: #4d4f53;text-align: center" class="vc_custom_heading" ><a href="http://www.keycollege.co.uk/" target=" _blank">Key College</a></h3>
	<div class="wpb_text_column wpb_content_element  vc_custom_1531238116575" >
		<div class="wpb_wrapper">
			<p style="text-align: center;"><strong>&#8220;Unlocking the door to your future&#8221;</strong></p>
<p style="text-align: center;">Aged 16-19 and want to gain accredited qualifications that will lead to employment? Then Key College is the place for you.</p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner vc_custom_1468593473519"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1573552021477">
		
		<figure class="wpb_wrapper vc_figure">
			<a href="https://ymcaderbyshire.org.uk/y-kidz/markeaton-primary-school/" target="_self" class="vc_single_image-wrapper   vc_box_border_grey"><img width="580" height="387" src="https://ymcaderbyshire.org.uk/wp-content/uploads/IMG_1374.jpg" class="vc_single_image-img attachment-large" alt="Children enjoying time outdoors at YMCA Y-Kidz" srcset="https://ymcaderbyshire.org.uk/wp-content/uploads/IMG_1374.jpg 640w, https://ymcaderbyshire.org.uk/wp-content/uploads/IMG_1374-65x43.jpg 65w, https://ymcaderbyshire.org.uk/wp-content/uploads/IMG_1374-300x200.jpg 300w, https://ymcaderbyshire.org.uk/wp-content/uploads/IMG_1374-343x229.jpg 343w" sizes="(max-width: 580px) 100vw, 580px" /></a>
		</figure>
	</div>
<h3 style="color: #4d4f53;text-align: center" class="vc_custom_heading" ><a href="http://ymcaderbyshire.org.uk/about/what-we-do/family-work/markeaton-primary-school/activity-club/">Y-Kidz Wraparound Care</a></h3>
	<div class="wpb_text_column wpb_content_element  vc_custom_1536589205035" >
		<div class="wpb_wrapper">
			<p style="text-align: center;"><strong>Book now</strong> for our Y-Kidz Wraparound Care at Markeaton Primary School</p>

		</div>
	</div>
</div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>

							</div>

						</section> <!-- end article header -->

					</article> <!-- end article -->
					</div>

					
					
				</div> <!-- end #main -->

			</div> <!-- end #content -->

</div> <!-- end #container -->
	<footer role="contentinfo">

		<div id="inner-footer" class="container clearfix">
					<div id="widget-footer" class="clearfix row">
						<div class="col-sm-3">
							<div class="footer-links clearfix"><ul><h4>About</h4><li id="menu-item-2165" class="menu-item menu-item-type-post_type menu-item-object-page first menu-item-2165"><a href="https://ymcaderbyshire.org.uk/about/">About</a></li>
<li id="menu-item-2137" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2137"><a href="https://ymcaderbyshire.org.uk/terms-and-conditions/">Terms and Conditions</a></li>
<li id="menu-item-2136" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2136"><a href="https://ymcaderbyshire.org.uk/privacy-and-cookies/">Privacy and Cookies</a></li>
<li id="menu-item-2135" class="menu-item menu-item-type-post_type menu-item-object-page last menu-item-2135"><a href="https://ymcaderbyshire.org.uk/accessibility/">Accessibility</a></li>
</ul></div>						</div>
						<div class="col-sm-3">
							<div class="footer-links clearfix"><ul><h4>Information</h4><li id="menu-item-2420" class="menu-item menu-item-type-post_type menu-item-object-page first last menu-item-2420"><a href="https://ymcaderbyshire.org.uk/policies-procedures/">Policies &#038; Procedures</a></li>
</ul></div>						</div>
						<div class="col-sm-3">
							<h4>Stay In Touch</h4>
							<div class="bottom_social_nav clearfix"><ul id="menu-bottom-social-links" class="menu"><li id="qwertymenu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom first"><a href="https://www.facebook.com/YMCADerbyshire" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-25" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://twitter.com/YMCADerbyshire" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.youtube.com/user/ymcaengland" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-5514" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://www.linkedin.com/company/ymca-derbyshire/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
<li id="qwertymenu-item-5512" class="menu-item menu-item-type-custom menu-item-object-custom last"><a href="https://www.instagram.com/ymca_derbyshire/?hl=en" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
</ul></div>						</div>
						<div class="col-sm-3">
							<div id="text-2" class="widget col-sm-3widget_text"><h4 class="widgettitle">YMCA Derbyshire</h4>			<div class="textwidget"><p>770 London Road<br />
Wilmorton, Derby<br />
DE24 8UT</p>
<p>Registered Charity No: 1049904<br />
Housing Association No: H4085<br />
VAT No: 143 7783 89</p>
</div>
		</div>						</div>
					</div>
			<p class="pull-right"></p>

			<p class="attribution"></p>
			<div class="wf-td bottom-text-block">
				<div class="vc_col-sm-12 bottom_footer">
					<div class="vc_col-sm-2 logo">
						<img class=" preload-me" src="/wp-content/themes/ymca/images/ymca-logo-white.png" srcset="/wp-content/themes/ymca/images/ymca-logo-white.png 130w, /wp-content/themes/ymca/images/ymca-logo-white-retina.png 259w" width="130" height="38" sizes="130px" alt="YMCA England">
					</div>
					<div class="vc_col-sm-10 content">
						YMCA enables people to develop their full potential in mind, body and spirit. Inspired by, and faithful to, our Christian values, we create supportive, inclusive and energising communities, where young people can truly belong, contribute and thrive.
					</div>
				</div>
					<div class="footer_col_wrapper">
						<span class="footer_col sa">SUPPORT &amp; ADVICE</span>
						<span class="footer_col acc">ACCOMMODATION</span>
						<span class="footer_col fw">FAMILY WORK</span>
						<span class="footer_col hw">HEALTH &amp; WELLBEING</span>
						<span class="footer_col te">TRAINING &amp; EDUCATION</span>
					</div>
				<div class="bottom_social">
					<div class="Social_Footer"></div>
				</div>
			</div>
		</div> <!-- end #inner-footer -->

	</footer> <!-- end footer -->
<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.7'></script>
<script type='text/javascript' src='https://ymcaderbyshire.org.uk/wp-includes/js/wp-embed.min.js?ver=5.2.4'></script>
<script src="/wp-content/themes/ymca/library/retina/dist/retina.min.js"></script>
</body>

</html>
