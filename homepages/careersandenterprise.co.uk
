<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en" version="XHTML+RDFa 1.0"
      dir="ltr"
  xmlns:og="http://ogp.me/ns#"
  xmlns:article="http://ogp.me/ns/article#"
  xmlns:book="http://ogp.me/ns/book#"
  xmlns:profile="http://ogp.me/ns/profile#"
  xmlns:video="http://ogp.me/ns/video#"
  xmlns:product="http://ogp.me/ns/product#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-dns-prefetch-control" content="on" />
<link rel="dns-prefetch" href="//cdn.careersandenterprise.co.uk" />
<!--[if IE 9]>
<link rel="prefetch" href="//cdn.careersandenterprise.co.uk" />
<![endif]-->
<meta name="description" content="Our mission is to prepare and inspire young people for the fast-changing world of work." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.careersandenterprise.co.uk/" />
<link rel="shortlink" href="https://www.careersandenterprise.co.uk/" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>The Careers & Enterprise Company</title>
  <link type="text/css" rel="stylesheet" href="//cdn.careersandenterprise.co.uk/sites/default/files/cdn/css/https/css_-lAV3Ju2F9wfhAMeZ0GLsB1b-8_6GPczj-jO-P1TwkQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="//cdn.careersandenterprise.co.uk/sites/default/files/cdn/css/https/css_3g3Gl0JJkZdqluRU9MDtH7sFGmuE-4GWc6bCj55Qswk.css" media="all" />
<link type="text/css" rel="stylesheet" href="//cdn.careersandenterprise.co.uk/sites/default/files/cdn/css/https/css_7hv-_CmEJ6LkvHtbQDmlEMZl8eBzlqqCAfDhamv-HcE.css" media="all" />
<link type="text/css" rel="stylesheet" href="//cdn.careersandenterprise.co.uk/sites/default/files/cdn/css/https/css_YmO5u45GY5fVcrsr9a1qQpaBBG_njs3wKfVma5KvPYM.css" media="all" />
<link type="text/css" rel="stylesheet" href="//cdn.careersandenterprise.co.uk/sites/default/files/cdn/css/https/css_47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU.css" media="print" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900"
          rel="stylesheet">
    <link rel="shortcut icon"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/favicon.ico"/>

    <link rel="apple-touch-icon-precomposed" sizes="57x57"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/apple-touch-icon-152x152.png"/>
    <link rel="icon" type="image/png"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/favicon-32x32.png"
          sizes="32x32"/>
    <link rel="icon" type="image/png"
          href="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/favicon-16x16.png"
          sizes="16x16"/>


    <meta name="application-name" content="Careers and Enterprise"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage"
          content="https://www.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/mstile-144x144.png"/>

    <meta name="format-detection" content="telephone=no"/>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start':
              new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-WQLD7ZS');</script>
    <!-- End Google Tag Manager -->


</head>
<body class="html front not-logged-in no-sidebars page-node node"  itemscope
      itemtype="http://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQLD7ZS"
            height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="skip-link">
    <a href="#main-content"
       class="element-invisible element-focusable">Skip to main content</a>
</div>
<!-- Template: page--front.tpl.php -->

<header class="main-head" itemscope itemtype="http://schema.org/WPHeader">

    <div class="container">
        <div class="col2 m-full s-full">
            <div class="main-logo">
                                <a href="/"
                     title="Home"></a>
                          </div>
        </div>

        <div class="col4 m-full colr">
            <div class="search-bar">
                <div class="main-search">
                  <form action="/search" method="get" id="views-exposed-form-search-block" accept-charset="UTF-8">
  <input class="search-input form-text" placeholder="Search our site..." type="text" id="edit-key" name="key" value="" size="30" maxlength="128" />  <input class="btn form-submit" type="submit" id="edit-submit-search" name="" value="Search" /></form>                </div>
            </div>
        </div>

      
        <div class="menu-mobile">
            <a href="#" class="btn-menu">Menu                <i></i></a>
        </div>
    </div>


    <section class="login-area">
        <div class="login-area-content">
            <div class="container">

                                              <div class="col6">

          <h2>User login</h2>
    
    <div class="content">
      <form action="/node?destination=node" method="post" id="user-login-form" accept-charset="UTF-8"><p></p><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Username or e-mail <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list"><ul><li class="first last"><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li>
</ul></div><input type="hidden" name="form_build_id" value="form-2VXq-U8Cu3p8J2XlCnz3EKEI-Amkj9idsjOOGjDE4yI" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions"><input class="btn form-submit" type="submit" id="edit-submit" name="op" value="Log in" /></div></form>    </div>
</div>
              
            </div>
        </div>
    </section>
      <nav class="main-nav"><div class="container"><div class="col12"><ul class="main-menu"><li class="user-data user-data-mobile"><a href="/user/login">Log in</a></li><li class="has-child"><a href="/schools-and-colleges">Schools &amp; Colleges</a><ul class="sub-main-menu"><li class="sub-main-menu-back"><a href="#" >Back</a></li><li class="parent-link desktop-hide"><a href="/schools-and-colleges">Schools &amp; Colleges</a></li><li ><a href="/schools-colleges/join-our-network">Join our Network</a></li>
<li ><a href="/schools-colleges/understand-gatsby-benchmarks">Understand the Gatsby Benchmarks</a></li>
<li ><a href="/schools-colleges/careers-leaders">Careers Leaders support</a></li>
<li ><a href="https://tools.careersandenterprise.co.uk/">Evaluation and planning tools</a></li>
<li ><a href="/schools-colleges/compass-plus">Compass+ (NEW)</a></li>
<li ><a href="/find-activity-provider">Find an activity provider</a></li>
<li ><a href="/schools-colleges/future-skills">Future Skills</a></li>
<li ><a href="/schools-colleges/primary-school-toolkit">Primary School Toolkit</a></li>
</ul></li>
<li class="has-child"><a href="https://www.careersandenterprise.co.uk/employers-volunteers">Employers &amp; Volunteers</a><ul class="sub-main-menu"><li class="sub-main-menu-back"><a href="#" >Back</a></li><li class="parent-link desktop-hide"><a href="https://www.careersandenterprise.co.uk/employers-volunteers">Employers &amp; Volunteers</a></li><li ><a href="https://www.careersandenterprise.co.uk/give-an-hour">Give an hour </a></li>
<li ><a href="https://www.careersandenterprise.co.uk/employers-volunteers/join-enterprise-adviser-network">Become an Enterprise Adviser</a></li>
<li ><a href="https://www.careersandenterprise.co.uk/employers-volunteers/cornerstone-employers">Become a Cornerstone Employer</a></li>
</ul></li>
<li class="has-child"><a href="/investment">Investment</a><ul class="sub-main-menu"><li class="sub-main-menu-back"><a href="#" >Back</a></li><li class="parent-link desktop-hide"><a href="/investment">Investment</a></li><li ><a href="/investment/active-funds">Active funds</a></li>
<li ><a href="/investment/previous-funds">Previous funds</a></li>
<li ><a href="/investment/investment-resources">Investment resources</a></li>
</ul></li>
<li ><a href="/our-research">Our Research</a></li>
<li ><a href="/news">News</a></li>
<li class="has-child"><a href="/about-us">About us</a><ul class="sub-main-menu"><li class="sub-main-menu-back"><a href="#" >Back</a></li><li class="parent-link desktop-hide"><a href="/about-us">About us</a></li><li ><a href="/about-us/our-governance">Our board and governance</a></li>
<li ><a href="/about/our-team">Our team</a></li>
<li ><a href="/about-us/join-the-team">Join us</a></li>
<li ><a href="https://www.careersandenterprise.co.uk/tender-opportunities">Tender Opportunities</a></li>
<li class="has-child"><a href="/about-us/our-network">Our network</a><ul class="sub-main-menu"><li class="sub-main-menu-back"><a href="#" >Back</a></li><li class="parent-link desktop-hide"><a href="/about-us/our-network">Our network</a></li><li ><a href="/about-us/our-network/opportunity-areas">Opportunity Areas</a></li>
<li ><a href="/about-us/our-network/careers-hubs">Our Careers Hubs</a></li>
</ul></li>
<li ><a href="/about-us/contact-us">Contact us</a></li>
</ul></li>
</ul></div></div></nav>
  
</header>


    <section class="banner contextual-links-region entity entity-bean bean-full-page-hero"
             style="background-image:url(  //cdn.careersandenterprise.co.uk/sites/default/files/styles/crop_banner/public/hero-images/homepage.jpg?itok=cSKyeb4d)">
              <div class="container">
            <div class="col10 center">
                              <div class="cont">
                    <h1>  Inspiring and preparing young people for the fast-changing world of work</h1>
                                  </div>
            </div>
        </div>
    </section>
<section class="banner banner-home">
  <div class="container contextual-links-region">
        <div class="col6 pre1 suf1">
      <div class="cont">
        <h2>
                        Youth Social Action Toolkit                  </h2>
        <p class="hero">  <p>Our brand new toolkit helps schools and colleges use youth social action to support careers education. Youth social action can support your careers programme and help you meet the Gatsby Benchmarks.</p>
<style type="text/css">
<!--/*--><![CDATA[/* ><!--*/
.banner-home .contextual-links-region .block { 
background-position: center center; 
/* background-size:contain; */
background-repeat:no-repeat; 
}

/*--><!]]>*/
</style><p><a class="btn btn-secondary" href="https://www.youth-social-action.careersandenterprise.co.uk/" target="_blank">Learn more</a></p>
</p>
      </div>
    </div>
    <div class="col4">
      <div class="block" style="background-image:url(  //cdn.careersandenterprise.co.uk/sites/default/files/styles/crop_banner_home/public/block-homepage-banner/cec_southend_250418_003.jpg?itok=iBkErP_W)">
        <h2></h2>
        <p></p>
              </div>
    </div>
  </div>
</section>
<section class="section contextual-links-region ">
            <div class="container">
        <div class="col12">
            <div class="section-title">
                <h2></h2>
            </div>
        </div>
    </div>
    <div class="container">
          <div class="col4 m-1_3">
     <a href="  https://www.careersandenterprise.co.uk/schools-and-colleges" class="block-link">    <div class="txtc">
        <div class="icon">
              <img src="//cdn.careersandenterprise.co.uk/sites/default/files/block-3-4-column-block/schools_and_colleges_icon.png" alt="Schools and colleges icon" />        </div>
    </div>
     </a>     <a href="  https://www.careersandenterprise.co.uk/schools-and-colleges" class="block-link"><h3 class="txtc"><span class="link">  Schools and colleges</span></h3></a>    <div class="body">  <div class="body" style="width: 270px; margin: 0px auto; padding-bottom: 10px; color: rgb(58, 58, 58); font-family: Lato, sans-serif; font-size: 16px;">
<p class="rtecenter">Support with high-impact careers activity and connections to business.</p>
<p class="rtecenter"><a href="http://www.careersandenterprise.co.uk/schools-colleges"><u>Find out more &gt;</u></a></p>
</div>
</div>
    </div>  <div class="col4 m-1_3">
     <a href="  https://www.careersandenterprise.co.uk/employers" class="block-link">    <div class="txtc">
        <div class="icon">
              <img src="//cdn.careersandenterprise.co.uk/sites/default/files/block-3-4-column-block/employers_icon.png" alt="Employers icon" />        </div>
    </div>
     </a>     <a href="  https://www.careersandenterprise.co.uk/employers" class="block-link"><h3 class="txtc"><span class="link">  Employers</span></h3></a>    <div class="body">  <div class="body" style="width: 270px; margin: 0px auto; padding-bottom: 10px; color: rgb(58, 58, 58); font-family: Lato, sans-serif; font-size: 16px;">
<p class="rtecenter">Help young people as a company or individual volunteer.</p>
<p class="rtecenter"><a href="http://www.careersandenterprise.co.uk/employers"><u>Find out more &gt;</u></a></p>
</div>
</div>
    </div>  <div class="col4 m-1_3">
     <a href="  https://www.careersandenterprise.co.uk/investment" class="block-link">    <div class="txtc">
        <div class="icon">
              <img src="//cdn.careersandenterprise.co.uk/sites/default/files/block-3-4-column-block/funding_icon.png" alt="Funding icon" />        </div>
    </div>
     </a>     <a href="  https://www.careersandenterprise.co.uk/investment" class="block-link"><h3 class="txtc"><span class="link">  Investment</span></h3></a>    <div class="body">  <div class="body" style="width: 270px; margin: 0px auto; padding-bottom: 10px; color: rgb(58, 58, 58); font-family: Lato, sans-serif; font-size: 16px;">
<p class="rtecenter">Investing to scale up and support proven careers activities.</p>
<p class="rtecenter"><a href="http://Investing to scale up and support proven careers activities.  Find out more &gt;"><u>Find out more &gt;</u></a></p>
</div>
</div>
    </div>    </div>
</section>




  <section class="section section-grey homepage-news">
  <div class="container">
    <div class="col12">
      <h3><a href="/news" class="link">Latest news</a></h3>
    </div>
  </div>
  <div class="container">
          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-blogs" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/youth-social-action-putting-young-peoples-talents-work" data-sticky="0" data-nid="1223" >
          <div class="img">
          <a href="/news/youth-social-action-putting-young-peoples-talents-work"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/imageresize_4.jpg?itok=vBRHQliA" width="370" height="220" /></a>      </div>
              <div class="article-type">Blogs</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/youth-social-action-putting-young-peoples-talents-work" class="name">Youth Social Action - putting young people’s talents to work</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">19/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          To celebrate the launch of our Youth Social Action Toolkit, Steve Holliday discusses the importance of putting young people’s talents to work.                       </p>
          <p><a href="/news/youth-social-action-putting-young-peoples-talents-work">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-blogs" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/youth-social-action-equity" data-sticky="0" data-nid="1222" >
          <div class="img">
          <a href="/news/youth-social-action-equity"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/cecwagamamaswilliamellis140919090resize.jpg?itok=6avV6fh6" width="370" height="220" /></a>      </div>
              <div class="article-type">Blogs</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/youth-social-action-equity" class="name">Youth Social Action for equity</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">18/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          Young people from low-income backgrounds are less likely to take part in social action activities than their more well-off peers.                      </p>
          <p><a href="/news/youth-social-action-equity">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-blogs" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/youth-social-action-and-careers" data-sticky="0" data-nid="1221" >
          <div class="img">
          <a href="/news/youth-social-action-and-careers"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/cecwagamamaswilliamellis140919077resizee.jpg?itok=xGWXXuSb" width="370" height="220" /></a>      </div>
              <div class="article-type">Blogs</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/youth-social-action-and-careers" class="name">Youth Social Action and Careers</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">18/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          I remember as a Secondary school teacher working with rural community in Buxton, the sense of satisfaction that pupils took from taking part in activities such as volunteering in care homes – beyond t                      </p>
          <p><a href="/news/youth-social-action-and-careers">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-blogs" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/how-can-youth-social-action-benefit-employers-well-young-people" data-sticky="0" data-nid="1220" >
          <div class="img">
          <a href="/news/how-can-youth-social-action-benefit-employers-well-young-people"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/generalcareersenterprisefund2015grantrecipients_3.jpg?itok=2tog_uC9" width="370" height="220" /></a>      </div>
              <div class="article-type">Blogs</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/how-can-youth-social-action-benefit-employers-well-young-people" class="name">How can youth social action benefit employers, as well as young people?</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">18/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          There is no shortage of young people wanting to gain experience of the workplace, and no shortage of employers wanting to offer it.                      </p>
          <p><a href="/news/how-can-youth-social-action-benefit-employers-well-young-people">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-blogs" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/promoting-engineering-construction-careers-next-generation" data-sticky="0" data-nid="1213" >
          <div class="img">
          <a href="/news/promoting-engineering-construction-careers-next-generation"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/itec-yeovilcollegefeb19copy.jpg?itok=rT8LpFCL" width="370" height="220" /></a>      </div>
              <div class="article-type">Blogs</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/promoting-engineering-construction-careers-next-generation" class="name">Promoting engineering construction careers to the next generation</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">05/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          Chris Claydon, CEO of the Engineering Construction Industry Training Board (ECITB) shares his thoughts on the importance of promoting engineering construction careers to the next generation.                      </p>
          <p><a href="/news/promoting-engineering-construction-careers-next-generation">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>          
<div class="col4 m-1_3 s-1_1">
  <article class="article-grid node node-blog article article-news" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting" data-url="/news/careers-leader-year-philippa-hartley" data-sticky="0" data-nid="1212" >
          <div class="img">
          <a href="/news/careers-leader-year-philippa-hartley"><img src="//cdn.careersandenterprise.co.uk/sites/default/files/styles/grid_news__370x220_/public/hero-images/careersexcellence-awards-270919-1245.jpg?itok=kRqCeods" width="370" height="220" /></a>      </div>
              <div class="article-type">News</div>
    
      <div class="content">
        <h3 itemprop="name headline"> <a href="/news/careers-leader-year-philippa-hartley" class="name">Careers Leader of the Year - Philippa Hartley</a></h3>
        <div class="publish-date">
                      <span class="published" itemprop="datePublished">04/11/2019</span>
                  </div>
        <div class="body-content">
          <p class="body-text" itemprop="articleBody">
                          Philippa has taken a transformative approach to the Huntcliff School’s careers programme.                      </p>
          <p><a href="/news/careers-leader-year-philippa-hartley">Read more&gt;</a></p>
        </div>
      </div>
      </article>
</div>      </div>
</section>



<footer itemscope itemtype="http://schema.org/WPFooter">
  <div class="footer-top">
    <div class="container">
        <div class="col12">
            <div class="foot-col contextual-links-region">
                <a href="https://twitter.com/CareerEnt" target="_blank">Follow us<img alt="Follow Us" height="28" src="//cdn.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/icon-twitter-circle.svg" width="28"></a>
            </div>
        </div>
    </div>
</div>
	<div class="footer-main">
		<div class="container">
			<div class="col3 suf1 m-1_4">
        <div class="foot-col contextual-links-region">
              <div itemscope="" itemtype="http://schema.org/Organization">
<address itemscope="" itemtype="http://schema.org/PostalAddress">
<div class="img"><img alt="The Careers &amp; Enterprise Company" src="//cdn.careersandenterprise.co.uk/sites/all/themes/cecmer/static/img/cec-logo.svg" /></div>

<p><span itemprop="streetAddress">Second floor,</span><br />
<span itemprop="streetAddress">2-7 Clerkenwell Green, </span><br />
<span itemprop="addressRegion">London,</span> <span itemprop="postalCode">EC1R 0DE</span><br />
<strong><a class="telephone" href="tel:02075663400" itemprop="telephone">0207 566 3400</a></strong>
</address>
</div>
</div>			</div>
			<div class="col3 m-1_4">
        <div class="foot-col contextual-links-region">
              <h4><u><a href="/contact-us">Contact us</a></u></h4>
<h5>Enquiries</h5>
<p><u><a class="email" href="mailto:info@careersandenterprise.co.uk">info@careersandenterprise.co.uk</a></u></p>
<h5>Press Enquiries</h5>
<p><u><a class="email" href="mailto:press@careersandenterprise.co.uk">press@careersandenterprise.co.uk</a></u></p>
</div>			</div>
			<div class="col3 m-1_4">
        <div class="foot-col contextual-links-region">
              <h4><u><a href="http://www.careersandenterprise.co.uk/schools-colleges">Schools and colleges</a></u></h4>
<p><a href="https://tools.careersandenterprise.co.uk/"><u>Evaluation and planning tools</u></a></p>
<h4><u><a href="https://www.careersandenterprise.co.uk/employers-volunteers">Employers</a></u></h4>
<p><u><a href="https://www.careersandenterprise.co.uk/employers-volunteers/join-enterprise-adviser-network">Become an Enterprise Adviser</a></u></p>
<h4><u><a href="https://www.careersandenterprise.co.uk/investment">Investment</a></u></h4>
<p><a href="https://www.careersandenterprise.co.uk/investment/active-funds"><u>Active Funds</u></a></p>
</div>			</div>
			<div class="col2 m-1_4">
        <div class="foot-col contextual-links-region">
              <h4><u><a href="https://www.careersandenterprise.co.uk/about-us">About us</a></u></h4>
<p><u><a href="https://www.careersandenterprise.co.uk/about-us/our-network">Our network</a><br /><a href="https://www.careersandenterprise.co.uk/about-us/join-the-team">Join the team</a></u></p>
<p><a href="https://www.careersandenterprise.co.uk/news/careers-excellence-awards-2019-winners-and-runners"><u>Careers Excellence Awards 2019</u></a></p>
<p><u><a href="https://www.careersandenterprise.co.uk/tender-opportunities">Tender Opportunities</a></u></p>
<p><a href="https://www.careersandenterprise.co.uk/enterprise-coordinator-resources"><u>Resources for Enterprise Coordinators</u></a></p>
<p><a href="https://www.careersandenterprise.co.uk/about-us/our-network/enterprise-adviser-resources"><u>Resources for Enterprise Advisers</u></a></p>
</div>			</div>
		</div>
	</div>
  <div class="footer-bottom">
    <div class="container">
      <div class="col12">
        <div class="txtc contextual-links-region">
      <p>© The Careers and Enterprise Company 2019. All rights reserved. <a href="/privacy-policy">Privacy Policy</a></p>
</div>
      </div>
    </div>
  </div>
</footer>


<script type="text/javascript" src="//cdn.careersandenterprise.co.uk/sites/default/files/js/js_xvYJgU6LChHqbcSh4y1AvdXfD5QBIwT3GVGVUeuksbM.js"></script>
<script type="text/javascript" src="//cdn.careersandenterprise.co.uk/sites/default/files/js/js_BLxotNs2yt7YGlf9QRI9L9AMfdnkQfnN-_ADBTW3SiE.js"></script>
<script type="text/javascript" src="//cdn.careersandenterprise.co.uk/sites/default/files/js/js_G-gd6eAg9LV0rJ36PyFVAcnIrxfuN44B6r4jxPGbKfs.js"></script>
<script type="text/javascript" src="//cdn.careersandenterprise.co.uk/sites/default/files/js/js_fl8bliXxUrucVniRDtrMVzEOPp2aEz03bv6nvybT8rg.js"></script>
<script type="text/javascript" src="//cdn.careersandenterprise.co.uk/sites/default/files/js/js_UG2AbIkB4GD7Qquog-N25GFOF_SQEGlh-OU9rcOBGGU.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"cecmer","theme_token":"fUCdkVkxHNx82vN1UexiV3-G6Z_-Og8ZWNF_vwmDMJw","jquery_version":"1.10","js":{"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/contrib\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/contrib\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/custom\/cec_provider\/js\/provider.js":1,"sites\/all\/modules\/custom\/cec_provider_new\/js\/provider.js":1,"sites\/all\/modules\/features\/cec_provider_new\/js\/script.js":1,"sites\/all\/modules\/contrib\/views_load_more\/views_load_more.js":1,"sites\/all\/modules\/contrib\/views\/js\/base.js":1,"misc\/progress.js":1,"sites\/all\/modules\/contrib\/views\/js\/ajax_view.js":1,"sites\/all\/themes\/cecmer\/static\/js\/isotope.min.js":1,"sites\/all\/themes\/cecmer\/static\/js\/scripts.js":1,"sites\/all\/themes\/cecmer\/static\/js\/src\/jquery.lazyload.js":1,"sites\/all\/themes\/cecmer\/static\/js\/src\/accordion.js":1,"sites\/all\/themes\/cecmer\/static\/js\/src\/icons.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/contrib\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/cecmer\/static\/css\/style.css":1,"sites\/all\/themes\/cecmer\/static\/css\/fontawesome-all.css":1,"sites\/all\/themes\/cecmer\/static\/css\/print.min.css":1}},"urlIsAjaxTrusted":{"\/search":true,"\/views\/ajax":true,"\/node?destination=node":true},"views":{"ajax_path":"\/views\/ajax","ajaxViews":{"views_dom_id:afa04c0a32cd7d44c4b958967aa186ee":{"view_name":"news","view_display_id":"homepage","view_args":"","view_path":"node","view_base_path":"news","view_dom_id":"afa04c0a32cd7d44c4b958967aa186ee","pager_element":0}}},"better_exposed_filters":{"views":{"news":{"displays":{"homepage":{"filters":[]}}}}}});
//--><!]]>
</script>
<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "Organization",
			"name" : "The Careers & Enterprise Company",
			"url" : "https://www.careersandenterprise.co.uk/",
			"logo" : "https://www.careersandenterprise.co.uk/sites/all/themes/cecmerstatic//img/cec-logo.svg",
			"sameAs" : [
			],
			"contactPoint" : [
			{
				"@type" : "ContactPoint",
				"telephone" : "02075663400",
				"contactType" : "enquiries"
			}
			]
		}

</script>

</body>
</html>
