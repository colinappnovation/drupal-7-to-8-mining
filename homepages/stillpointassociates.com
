<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <!-- The title tag we show the title of our site and the title of the current page -->
  <title>Stillpoint Associates | Welcome to stillpoint – transforming business through behavioural change</title>

  <!-- Stylesheets can be included using the `css()` helper. Kirby also provides the `js()` helper to include script file.
        More Kirby helpers: https://getkirby.com/docs/reference/templates/helpers -->
	<link href="https://www.stillpointassociates.com/assets/bulma-0.7.5/fa-css/all.css" rel="stylesheet">
	<link href="https://www.stillpointassociates.com/assets/bulma-0.7.5/css/bulma.css" rel="stylesheet">
	<script src="https://www.stillpointassociates.com/assets/js/nav.js"></script>
</head>
<body>
	<div class="grid-wrapper">
		<div class="header-wrapper">
			<header class="header">
	  			<div id="desktop-logo">
	  				<a href="https://www.stillpointassociates.com">
	  					<img src="../assets/bulma-0.7.5/images/logo-1200.jpg" alt="Stillpoint Associates" />
	  				</a>
	  			</div>
	  			<div class="container">
					<nav class="navbar" role="navigation" aria-label="main navigation">
						<div class="navbar-brand">
							<a class="navbar-item" href="https://www.stillpointassociates.com">Stillpoint Associates</a>
							<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
							</a>
						</div>
						<div class="navbar-menu" id="navMenu">
							<a class="navbar-item is-active" href="https://www.stillpointassociates.com">
								Home							</a>
															<a class="navbar-item" href="https://www.stillpointassociates.com/coaching">
									Coaching								</a>
															<a class="navbar-item" href="https://www.stillpointassociates.com/training">
									Training								</a>
															<a class="navbar-item" href="https://www.stillpointassociates.com/about-us">
									About us								</a>
															<a class="navbar-item" href="https://www.stillpointassociates.com/testimonials">
									Testimonials								</a>
															<a class="navbar-item" href="https://www.stillpointassociates.com/contact-us">
									Contact us								</a>
													</div>
					</nav>
	  			</div>
			</header>
		</div>
<!-- Start the main content (but don't close it as other templates may add content -->
<main>
	<div class="main-wrapper">
		<section class="section">
			<div class="container">
				<h1 class="title is-size-1">Welcome to stillpoint – transforming business through behavioural change</h1>
				<h2 class="subtitle is-3"></h2>
				<div class="content">
					<p>Stillpoint is a solution-provider in the field of organisational development, with expertise in building organisational capability and capacity – the vital ingredient for any 21st century organisation. Using a blended fusion of consultancy, training, facilitation and coaching, we design to provide the results you are seeking.</p>
<p>Change is the new normal. There is no time to get people fit TO change. Fitness FOR change has to be a constant state of readiness.</p>
<h2>Is your 21st century organisation fit for change?</h2>
<p>Our <strong>Fit for Change Initiative</strong> is the road to strategic advantage by increasing organisational capability and capacity and creating a <a href="https://www.stillpointassociates.com/pages/the-21st-century-organisation">21st century organisational culture</a>. We focus on 4 key areas for change:</p>
<ul>
<li><a href="https://www.stillpointassociates.com/training/employee-engagement">employee engagement</a></li>
<li><a href="https://www.stillpointassociates.com/training/internal-coaching-culture">internal coaching culture</a></li>
<li><a href="https://www.stillpointassociates.com/training/immunity-to-change">immunity to change</a></li>
<li><a href="https://www.stillpointassociates.com/training/leadership-capability">leadership capability</a></li>
</ul>
<p>We uniquely combine a robust developmental and outcome-based process with a non-formulaic and adaptive approach covering both individual and collective developmental needs. The success rate of strategic changes identified by the business can then increase dramatically.</p>
<p>As a boutique solution-provider, we co-design with you to provide a bespoke blend of consultancy, training, facilitation and coaching.</p>				</div>
			</div>
			<!-- Other templates may add stuff here -->
		</section>
	</div>
</main>
<aside class="aside-wrapper">
	<section class="section">
		<div class="container">
			<!-- Display one random client quote -->
						<div class="content aside-block">
				<h3 class="title is-size-5">What our clients say about us</h3>
				<p><span class="fas fa-quote-left has-padding-right"></span>Your session last night put me back into neutral which meant it was never going to be anything other than successful. A million thanks for helping me to get back to the place of calm serenity, where I believe in myself and my abilities instead of the dark depressing world of doubt, judgement and body armour… thank you for your beautiful way and expert guidance.<span class="fas fa-quote-right has-padding-left"></span></p>
			</div>
		</div>
	</section>
</aside>
<div class="footer-wrapper">
	<footer class="footer">
		<div class="content">
			<a href="https://www.stillpointassociates.com">Copyright &copy; 2019 Stillpoint Associates. All rights reserved.</a>
		</div>
		<div class="content">
			<a class="" href="https://www.stillpointassociates.com/legal">Legal</a>
		</div>
	</footer>
</div>
</body>
</html>