<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.super-talenti.com/sites/default/files/super-talentix.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="Steve Jobs, le créateur d’Apple, est encore une célébrité en innovation, en technologie et en marketing même après sa mort. John Maynard Keynes est l’un des économistes les plus influents de l’Histoire. Son travail est encore fréquemment analysé aujourd’hui. Ici, nous rendons hommage aux génies les plus créatifs de l&#039;ère moderne" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="keywords" content="économie, histoire, finance, Keynes, emploi, politique, monnaie, bourse" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.super-talenti.com/fr" />
<link rel="shortlink" href="https://www.super-talenti.com/node/7" />
<title>John Maynard Keynes, l’économiste du siècle</title>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_vZ7OMldNxT0kN_1nW7_5iIquAxAdcU-aJ-ucVab5t40.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_r9PJyqe_MAsr1ak5sF_5S0q6uou4ov4ztJL834S-WLY.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_lU8iLwYwjJJhkcfRmYbLWxohrpNBn07o02vhTHVbcgw.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_oBwJlUqk50VJF8d0qKX08oRiZi_A-6m3e4q3aMLlu7Y.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_5SPC4RGAy3DC9-d3AIRaamiAJVTghP1WhECz5UBECYU.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_ZKrMUWhuaFc7Vexxs5GRFWAh__LGVptVAUg6S9Hb7YM.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_MMwz_DHoJPLpLGM4vZSEsI6xp8E9asuG1BwOBN-sVcY.css" media="only screen" />

<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_OQw0u--G3ey7_fkC3ZVWT4kDUo9w1wDU4AS2vhzTE4k.css" media="screen" />
<![endif]-->

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_cF04dxFMEWKnot0kje6eKpyQrAp2H_f67HAewwO8OUU.css" media="screen" />
<![endif]-->

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_5eIb9N0D2_Pfc9l6FBt2VtSiiZCn8heHM44BeRyqpaE.css" media="screen" />
<![endif]-->

<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_kCqKQHv_NTAzs4Ko0FFzeU3NWDVEYy-W2r7saGkBYZg.css" media="screen" />
<![endif]-->

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.super-talenti.com/sites/default/files/css/css_siq-t5G3eRJLCxYbR8Nv6oPDyUl36hjIw_F4Jv7-BcE.css" media="screen" />
<![endif]-->
<script src="https://www.super-talenti.com/sites/default/files/js/js_vDrW3Ry_4gtSYaLsh77lWhWjIC6ml2QNkcfvfP5CVFs.js"></script>
<script src="https://www.super-talenti.com/sites/default/files/js/js_45JniWrXrY8q8AEVIV6GFupI5scizolw-FrujytMgiY.js"></script>
<script src="https://www.super-talenti.com/sites/default/files/js/js_ZdKxSuA1fkezsVufHEcECTJtAPQUeXSaKnIW9V_kdiM.js"></script>
<script src="https://www.super-talenti.com/sites/default/files/js/js_mQmpGzvQcLAM2vhycGevdHKAKmDRGBoiYpTaSnasoLg.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_biz","theme_token":"hLIBvcSRjbdG60lny8CD6wzgUYUPsBWPjYoPXNKdr9U","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.bgiframe.js":1,"sites\/all\/modules\/nice_menus\/js\/jquery.hoverIntent.js":1,"sites\/all\/modules\/nice_menus\/js\/superfish.js":1,"sites\/all\/modules\/nice_menus\/js\/nice_menus.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/themes\/at_biz\/js\/eq.js":1,"sites\/all\/themes\/at_biz\/js\/eq-gp.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus.css":1,"sites\/all\/modules\/nice_menus\/css\/nice_menus_default.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.settings.style.floatblocks.css":1,"sites\/all\/themes\/adaptivetheme\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/at_biz\/css\/styles.base.css":1,"sites\/all\/themes\/at_biz\/css\/styles.settings.css":1,"sites\/all\/themes\/at_biz\/css\/styles.modules.css":1,"sites\/all\/themes\/at_biz\/css\/styles.custom.css":1,"sites\/all\/themes\/at_biz\/color\/colors.css":1,"public:\/\/adaptivetheme\/at_biz_files\/at_biz.responsive.layout.css":1,"public:\/\/adaptivetheme\/at_biz_files\/at_biz.fonts.css":1,"public:\/\/adaptivetheme\/at_biz_files\/at_biz.responsive.styles.css":1,"public:\/\/adaptivetheme\/at_biz_files\/at_biz.lt-ie9.layout.css":1,"sites\/all\/themes\/at_biz\/css\/ie-6.css":1,"sites\/all\/themes\/at_biz\/css\/ie-lte-7.css":1,"sites\/all\/themes\/at_biz\/css\/ie-8.css":1,"sites\/all\/themes\/at_biz\/css\/ie-lte-9.css":1}},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":true,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":false,"disable_resize":false,"disable_zoom":false,"force_show_nav":false,"show_caption":true,"loop_items":false,"node_link_text":"View Image Details","node_link_target":false,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":false,"useragent":"Python-urllib\/3.8"},"nice_menus_options":{"delay":"800","speed":"slow"},"urlIsAjaxTrusted":{"\/search\/node":true,"\/":true},"adaptivetheme":{"at_biz":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)"}}}});</script>
<!--[if lt IE 9]>
<script src="https://www.super-talenti.com/sites/all/themes/adaptivetheme/adaptivetheme/at_core/scripts/html5.js?ppa9b2"></script>
<![endif]-->
</head>
<body class="html front not-logged-in two-sidebars page-node page-node- page-node-7 node-type-french-page site-name-hidden atr-7.x-3.x atv-7.x-3.0 site-name-super-talenti color-scheme-custom bb-dlt  mb-n rc-4 bs-n">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page ssc-n ssw-n ssa-l sss-n btc-n btw-b bta-l bts-d ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-w">

          <div id="menu-top-wrapper">
        <div class="container clearfix">
          <div class="region region-menu-bar-top"><div class="region-inner clearfix"><div id="block-search-form" class="block block-search no-title odd first block-count-1 block-region-menu-bar-top block-form"  role="search"><div class="block-inner clearfix">  
  
  <div class="block-content content no-title clearfix"><div class="block-content-inner clearfix"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-GX7xKuUyMl6JCAVZRC_s4eJcU3uZsXvLeHzq1TCKjuU" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div></div>
  </div></div><div id="block-block-3" class="block block-block no-title even last block-count-2 block-region-menu-bar-top block-3" ><div class="block-inner clearfix">  
  
  <div class="block-content content no-title clearfix"><div class="block-content-inner clearfix"><div class="block-content content no-title rteright">
<ul class="language-switcher-locale-url" style="list-style:none;list-style-type:none;margin-left:0;padding-left:0;"><li class="fr first" style="display:inline;margin-left:0;padding-left:0">
			<a class="language-link" href="/fr"><img alt="Français" class="language-icon" height="30" src="/sites/default/files/fr.png" title="Français" typeof="foaf:Image" width="30" /></a></li>
<li class="en last" style="display:inline;padding-left:20px">
			<a class="language-link" href="/en"><img alt="English" class="language-icon" height="30" src="/sites/default/files/en.png" title="English" typeof="foaf:Image" width="30" /></a></li>
</ul></div>
</div></div>
  </div></div></div></div>        </div>
      </div>
    
    <div id="header-wrapper">
      <div class="container clearfix">
        <header id="header" class="clearfix without-region-header" role="banner">

                      <!-- start: Branding -->
            <div id="branding" class="branding-elements clearfix">

                              <div id="logo">
                  <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="https://www.super-talenti.com/sites/default/files/super-talentix.jpg" alt="Super Talenti" /></a>                </div>
                          </div><!-- /end #branding -->
          
          <!-- region: Header -->
          
        </header>
      </div>
    </div>

    
    <div id="secondary-content-wrapper">
      <div class="image-overlay">
        <div class="container clearfix">

          
          <!-- Three column 3x33 Gpanel -->
          
        </div>
      </div>
    </div>

    
    
    <div id="content-wrapper">
      <div class="container">

        
        <div id="columns">
          <div class="columns-inner clearfix">
            <div id="content-column">
              <div class="content-inner">
  
                  
                <section id="main-content">
  
                                      <header id="main-content-header" class="clearfix">
                                                                    <h1 id="page-title">John Maynard Keynes, l’économiste du siècle</h1>
                                              
                                          </header>
                    
                  <!-- region: Main Content -->
                                      <div id="content">
                      <div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main" >  
  
  <article id="node-7" class="node node-french-page node-promoted article odd node-full ia-r clearfix" about="/fr" typeof="sioc:Item foaf:Document" role="article">
  <div class="article-inner">
    
    
    <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p>John Maynard Keynes (1883-1946) est souvent considéré comme l’économiste le plus influent et le plus important du XXe siècle… et son travail est encore repris aujourd’hui pour analyser l’actualité financière, économique et politique avec la perspective née de son courant de pensée, le keynésianisme.</p>
<p>Keynes était loin d’être un rat de bibliothèque. Pragmatique et engagé dans la vie politique et économique, il a notamment pris part à la mise en place du Fonds Monétaire International (FMI) et de la Banque Mondiale dès 1944, dans le cadre de Bretton Woods. Spéculateur boursier, il a également fortement inspiré la politique du New Deal.</p>
<p>En 1930, Keynes a publié <em>Traité sur la monnaie, </em>un ouvrage en 2 volumes qui revient sur l’histoire de la monnaie, ses origines, son rôle et sa vision du « cycle du crédit », ce qui lui permet ainsi de développer les grands traits de la politique monétaire, les causes du sous-emploi et la manière de stabiliser l’économie (via la stabilité des prix).</p>
<p>Quelles sont ses idées les plus célèbres ? Elles sont nombreuses, mais on peut citer parmi elles la réfutation, dans son ouvrage <em>Théorie générale de l’emploi, de l’intérêt et de la monnaie</em> (1936), de la loi de Jean-Baptiste Say qui affirme que le marché est naturellement équilibré entre l’offre et la demande. Or Keynes a mis en avant le caractère incertain de l’économie, les erreurs dans les choix de ceux qui y prennent part, et le fait que la monnaie ne soit pas toujours utilisée comme elle le devrait en étant stockée par les particuliers. Une théorie révolutionnaire qui rappelle directement que le chômage, le déséquilibre des productions et les crises économiques peuvent régulièrement survenir, et que les pouvoirs publics doivent donc s’y atteler pour réguler l’ensemble.</p>
<p>Ce qui nous amène à sa vision des pouvoirs publics : ces derniers ne doivent pas intervenir en toutes circonstances (contrairement à la lecture répandue de Keynes), mais que selon les événements, les Etats doivent réagir pour rétablir l’équilibre, sans menacer pour autant les différentes sociétés privées.</p>
<p>Laissé de côté dans les années 1980, Keynes (sa pensée, en tout cas) a fait un grand retour depuis la dernière crise économique mondiale qui a frappé l’économie en 2008. On évoque régulièrement son travail sur des questions d’interventionnisme politique dans l’économie, de politique de plein emploi et de régulation (ou non) des marchés.</p>
</div></div></div>    </div>

    
  </div> <!-- inner -->

  
  
</article>

  </div>                    </div>
                    
                    
                </section>
  
                
              </div>
            </div>
  
            <!-- regions: Sidebar first and Sidebar second -->
            <div class="region region-sidebar-first sidebar"><div class="region-inner clearfix"><nav id="block-nice-menus-3" class="block block-nice-menus odd first block-count-4 block-region-sidebar-first block-3" ><div class="block-inner clearfix">  
      <h2 class="block-title">Super Talented</h2>
  
  <div class="block-content content clearfix"><div class="block-content-inner clearfix"><ul class="nice-menu nice-menu-right nice-menu-main-menu" id="nice-menu-3"><li class="menu-477 menu-path-node-7 active-trail first odd  menu-depth-1 menu-item-477"><a href="/fr" class="active">John Maynard Keynes</a></li><li class="menu-1839 menu-path-node-19  even  menu-depth-1 menu-item-1839"><a href="/technologie-design-entreprise">Steve Jobs</a></li><li class="menu-1840 menu-path-node-20  odd  menu-depth-1 menu-item-1840"><a href="/art-culture-design">Pablo Picasso</a></li><li class="menu-1841 menu-path-node-21  even  menu-depth-1 menu-item-1841"><a href="/marketing-entreprise">David Ogilvy</a></li><li class="menu-1777 menu-path-node-18  odd last menu-depth-1 menu-item-1777"><a href="/contact">Contact</a></li></ul>
</div></div>
  </div></nav><nav id="block-system-user-menu" class="block block-system block-menu even last block-count-5 block-region-sidebar-first block-user-menu"  role="navigation"><div class="block-inner clearfix">  
      <h2 class="block-title">User menu</h2>
  
  <div class="block-content content clearfix"><div class="block-content-inner clearfix"><ul class="menu clearfix"><li class="first last leaf menu-depth-1 menu-item-789"><a href="/user/login" title="">Log in</a></li></ul></div></div>
  </div></nav></div></div>            <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><div id="block-views-left-hand-column-block" class="block block-views no-title odd first last block-count-6 block-region-sidebar-second block-left-hand-column-block" ><div class="block-inner clearfix">  
  
  <div class="block-content content no-title clearfix"><div class="block-content-inner clearfix"><div class="view view-left-hand-column view-id-left_hand_column view-display-id-block view-dom-id-e73f9b4d466e2632cbb6f74eaaabe65f">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-left-hand-column">        <div class="field-content"><p class="rteright"><img alt="" height="329" src="/sites/default/files/images/Keynes.jpg" width="368" /></p>
<p class="rtecenter"><em><strong>John Maynard Keynes en 1913</strong></em></p>
<p class="rteright"><img alt="" height="435" src="/sites/default/files/images/John_Maynard_Keynes.jpg" width="364" /></p>
<p class="rtecenter"><em><strong>et 1946</strong></em></p>
</div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div></div>
  </div></div></div></div>  
          </div>
        </div>
      </div>
    </div>

    <!-- Five column Gpanel -->
    
    
          <div id="footer-wrapper">
        <div class="container clearfix">
          <footer id="footer" class="clearfix" role="contentinfo">
            <!-- Four column Gpanel (Quad) -->
                        <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-7" class="block block-block no-title odd first last block-count-7 block-region-footer block-7" ><div class="block-inner clearfix">  
  
  <div class="block-content content no-title clearfix"><div class="block-content-inner clearfix"><p class="rtecenter"><a href="http://super-talenti.com/">super-talenti.com</a>    <a href="http://www.indigoextra.com/fr/conception-de-sites-web">Conception de sites web - Indigoextra</a></p>
</div></div>
  </div></div></div></div>          </footer>
        </div>
      </div>
    
  </div>
</div>
  </body>
</html>
