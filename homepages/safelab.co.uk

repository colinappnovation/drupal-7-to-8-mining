<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.safelab.co.uk/sites/all/themes/gwstrap/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Fume Cupboards for Laboratories and Schools. Safelab are a market leader in the design, manufacture and servicing of fume cupboards in the UK. Every fume cupboard is built in our factory in North Somerset for installation worldwide." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.safelab.co.uk/" />
<link rel="shortlink" href="https://www.safelab.co.uk/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.safelab.co.uk/" />
<meta name="twitter:title" content="Safelab" />
<meta name="twitter:description" content="Fume cupboard innovation since 1984" />
<meta itemprop="name" content="Safelab" />
<meta itemprop="description" content="Fume cupboard innovation since 1984" />
  <title>Fume Cupboard | Fume Cabinet | Fume Hood | Class II Safety Cabinet</title>
  <link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css_6zemUaNACzZ5sPLowbJJP0jVAcgeofg1dmXJdb1dfGY.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css_jmIQt-G63wvSSQDGlJsGr75BcyQS3EAhDlFLaIXMFv0.css" media="all" />

<!--[if lt IE 10]>
<link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css_glu_xvtd5CMYGe4s1e-I_Fqp_vX8TDmMN5qKebbObE0.css" media="all" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css__T2yu26iNiuxQ-3JiFzl7ZXfp17VduwOmiDBskSoVho.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.safelab.co.uk/sites/default/files/css/css_6hUQQuzmiXVwS2uuHQdNWINsN8hhNFJv9bW7WqdWylQ.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.11/jquery.min.js'>\x3C/script>")</script>
<script>jQuery.migrateMute=true;jQuery.migrateTrace=false;</script>
<script src="https://www.safelab.co.uk/sites/default/files/js/js_uqNWfakHPL8LsoHNp4LDW3ClUu3R40Lf7p1gv6u7l8o.js"></script>
<script src="https://www.safelab.co.uk/sites/default/files/js/js_6j1BH4GWvSDefvLGg4KrU2DNQdshrszlXjeV3d2QXJE.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-2036475-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.safelab.co.uk/sites/default/files/js/js_6q0EIivVyWUwT_MoMm7XLfBEoMwG08iVYXccPjNWdFY.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"gwstrap","theme_token":"y7Ddc4ALjhbK1GcFKCGpR7Wb1GSpXEZW_9zdSQstFY4","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.11.2\/jquery.min.js":1,"0":1,"1":1,"sites\/all\/modules\/jquery_update\/replace\/jquery-migrate\/1.2.1\/jquery-migrate.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"2":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/affix.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/alert.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/button.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/carousel.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/collapse.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/dropdown.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/modal.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/tooltip.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/popover.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/scrollspy.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/tab.js":1,"sites\/all\/themes\/gwstrap\/bootstrap\/js\/transition.js":1,"sites\/all\/themes\/gwstrap\/js\/bootstrap-hover-dropdown.min.js":1,"sites\/all\/themes\/gwstrap\/js\/imagelightbox.min.js":1,"sites\/all\/themes\/gwstrap\/js\/gwstrap.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"public:\/\/ctools\/css\/9531db8f2c83e2605567a681bbb8dfe9.css":1,"sites\/all\/libraries\/fontawesome\/css\/font-awesome.css":1,"sites\/all\/themes\/gwstrap\/js\/imagelightbox.min.css":1,"sites\/all\/themes\/gwstrap\/less\/style.less":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
  <script src="https://use.typekit.net/cas8xly.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body class="html front not-logged-in no-sidebars page-node">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
<header id="navbar" role="banner" class="container-fluid  gwstrap-navbar-container">
  <div class="container navbar navbar-default gwstrap-navbar">


    <div class="row">
        <div class="region region-header">
    <section id="block-gwsblocks-sub-header" class="block block-gwsblocks clearfix">

      
  <div class="col-md-4 col-sm-5 col-xs-8 sub-header-slogan">
  <span>Fume cupboard innovation since 1984</span>
</div>
<div class="col-md-4 col-sm-3 col-xs-4 sub-header-phone">
  <a href="tel: +44 (0) 1934 421 340">
    <i class="fa fa-phone"></i>
    <span class="phone"><a href="tel: +44  1934 421 340">+44 (0) 1934 421 340</a></span>
  </a>
</div>
<div class="col-lg-4 col-lg-offset-4 col-md-4 col-sm-4 hidden-xs sub-header-text2">
  <div class="text-container">
    <span class="image-wrapper">
      <img class="img-responsive" src="https://www.safelab.co.uk/sites/default/files/fumecupboard_0.png" alt="Fame Cup Logo."/>
    </span>
    <span class="text-wrapper">Manufactured in the UK</span>
  </div>
</div>
</section>
  </div>
    <div>

    <div class="row gwstrap-header">
      <div class="navbar-header col-lg-3 col-md-2 col-sm-12 col-xs-12">
   
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                          <div class="gwstrap-logo" style="background-image:url(https://www.safelab.co.uk/sites/default/files/logo_05n.png);">
            <a class="logo navbar-btn pull-left" href="/" title="Home">
            </a>
          </div>
        
        
      </div>

      <div class="primary-nav col-lg-9 col-md-10 col-sm-12 col-xs-12">
                  <div class="navbar-collapse collapse navbar-right">
            <nav role="navigation">
                              <ul class="menu nav navbar-nav"><li class="first leaf active menu-219"><a href="/" class="active">Home</a></li>
<li class="expanded menu-922 dropdown"><a href="/fume-cupboards-products-sectors" class="dropdown-toggle disabled" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false" data-target="#">Products and Sectors<i class="fa fa-angle-down"></i></a><ul class="dropdown-menu"><li class="first leaf menu-923 tax-sector term-fume-cupboards-schools-colleges"><a href="/fume-cupboards-schools-colleges">Schools and Colleges</a></li>
<li class="leaf menu-924 tax-sector term-fume-cupboards-industry-higher-education"><a href="/fume-cupboards-industrial-higher-education">Industrial and Higher Education</a></li>
<li class="leaf menu-925 tax-sector term-laminar-flow-class-ii-cabinets"><a href="/laminar-flow-class-ii-cabinets">Laminar Flow &amp; Class II Cabinets</a></li>
<li class="last leaf menu-926 tax-sector term-forensic-chemical-storage"><a href="/forensic-chemical-storage">Forensic and Chemical Storage</a></li>
</ul></li>
<li class="expanded menu-927 dropdown"><a href="/fume-cupboards-service-maintenance" class="dropdown-toggle disabled" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false" data-target="#">Service & Maintenance<i class="fa fa-angle-down"></i></a><ul class="dropdown-menu"><li class="first leaf menu-612 ctype-service"><a href="/fume-cupboards-service-maintenance/schools-colleges">Schools and Colleges</a></li>
<li class="leaf menu-613 ctype-service"><a href="/fume-cupboards-service-maintenance/industrial-higher-education">Industrial and Higher Education</a></li>
<li class="last leaf menu-614 ctype-service"><a href="/fume-cupboards-service-maintenance/filter-replacement">Filter Replacement</a></li>
</ul></li>
<li class="expanded menu-562 ctype-page dropdown"><a href="/about-us" class="dropdown-toggle disabled" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false" data-target="#">About Us<i class="fa fa-angle-down"></i></a><ul class="dropdown-menu"><li class="first leaf menu-647 ctype-page"><a href="/news">News</a></li>
<li class="leaf menu-646 ctype-page"><a href="/fume-cupboards-design-manufacturing">Design &amp; Manufacturing</a></li>
<li class="leaf menu-1103 ctype-page"><a href="/fume-cupboard-bim">Fume Cupboard BIM</a></li>
<li class="leaf menu-643 ctype-page"><a href="/our-clients">Our Clients</a></li>
<li class="leaf menu-791"><a href="/case-studies">Case Studies</a></li>
<li class="last leaf menu-660 ctype-page"><a href="/terms-conditions">Terms and Conditions</a></li>
</ul></li>
<li class="last leaf menu-563 ctype-page"><a href="/contact-us">Contact Us</a></li>
</ul>                          </nav>
          </div>
              </div>
    </div>
  </div>
</header>
 

    <section class="container">
          </section>
    <section id="high-top" class="container-fluid">
              <div class="highlighted">  <div class="region region-highlighted">
    <section id="block-views-slide-home-block" class="block block-views clearfix">

      
  <div class="view view-slide view-id-slide view-display-id-home_block view-dom-id-db4aa33b8dc2fb42759c502edd9f5f6a">
        
  
  
      <div class="view-content">
      
<script type="text/javascript" charset="utf-8">
  jQuery(document).ready(function ($) {

    $('.home-slide-caption').flexslider({
      animation: "fade",
            directionNav: false,
      controlNav: false,
      namespace:  "flex-",
      slideshow: true,
      slideshowSpeed: 7000,
      animationSpeed: 600,
      initDelay: 1,      
      sync: "#home-slide-img",
      start: function(){
           $('.flex-images').show(); 
      },
    });


    $('#home-slide-img').flexslider({
      animation: "slide",
            direction: "horizontal",
                  directionNav: false,
      controlNav: false,
      slideshowSpeed: 7000,
      animationSpeed: 600,
      start: function(){
           $('#home-slide-img-wrapper').removeClass('loading'); 
      },
    });
  });
</script>



<div id="home-slide-img-wrapper" class="container-fluid loading">
  <div id="home-slide-img">
    <ul class="slides">
              <li>
          <a href="schools-colleges" title="Read More">
            <div class="home-slide-img" style="background-image:url(/sites/default/files/styles/slider/public/slides/school-fume-cupboards.jpg?itok=rtzFV0aR);"></div>
          </a>
        </li>
              <li>
          <a href="industrial-higher-education" title="Read More">
            <div class="home-slide-img" style="background-image:url(/sites/default/files/styles/slider/public/slides/fume-cupboards-in-laboratory.jpg?itok=21e4ziy7);"></div>
          </a>
        </li>
              <li>
          <a href="service-maintenance" title="Read More">
            <div class="home-slide-img" style="background-image:url(/sites/default/files/styles/slider/public/slides/fume-cupboard-service-vehicles.jpg?itok=lfvABhML);"></div>
          </a>
        </li>
          </ul>
  </div>
</div>


<div class="home-slide-caption-container">
  <div class="container">
    <div id="home-slide-caption-wrapper" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="home-slide-caption">
        <ul class="slides">
                      <li>
              <div class="home-slide-color" style="background-color: rgba(225, 125, 51, 0.84);">
                <div class="home-slide-title">
                  <a href="schools-colleges" title="Read More">
                    <h2>Ducted and filtered fume cupboards tested by CLEAPSS</h2>
                  </a>
                </div>
                <div class="home-slide-readmore">
                  <a href="schools-colleges" title="Read More">
                    <span>Read More</span>
                    <i class="fa fa-angle-right"></i>
                  </a>
                </div>
              </div>
            </li>
                      <li>
              <div class="home-slide-color" style="background-color: rgba(134, 62, 128, 0.84);">
                <div class="home-slide-title">
                  <a href="industrial-higher-education" title="Read More">
                    <h2>Fume cupboards for industry and higher education</h2>
                  </a>
                </div>
                <div class="home-slide-readmore">
                  <a href="industrial-higher-education" title="Read More">
                    <span>Read More</span>
                    <i class="fa fa-angle-right"></i>
                  </a>
                </div>
              </div>
            </li>
                      <li>
              <div class="home-slide-color" style="background-color: rgba(71, 126, 183, 0.84);">
                <div class="home-slide-title">
                  <a href="service-maintenance" title="Read More">
                    <h2>Comprehensive LEV service and maintenance solutions</h2>
                  </a>
                </div>
                <div class="home-slide-readmore">
                  <a href="service-maintenance" title="Read More">
                    <span>Read More</span>
                    <i class="fa fa-angle-right"></i>
                  </a>
                </div>
              </div>
            </li>
                  </ul>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
          </div>
  </div>
</div>

    </div>
  
  
  
  
  
  
</div>
</section>
  </div>
</div>
          
    </section>  
    <section id="main-home" class="container">
      <a id="main-content"></a>
                                      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="region region-home-sidebar">
    <div id="block-menu-menu-homepage" class="block block-menu">

<div class="navbar-header">  
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".left-navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="menu-info">Inner Nav</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>
<div class="left-navbar-collapse collapse">
	<nav role="navigation">
    <ul class="menu nav"><li class="first leaf menu-652"><a href="/contact-us">Contact Us<i class="fa fa-angle-right"></i></a></li>
<li class="leaf menu-649"><a href="/fume-cupboards-schools-colleges">School Fume Cupboard<i class="fa fa-angle-right"></i></a></li>
<li class="leaf menu-653"><a href="/fume-cupboards-industrial-higher-education">Industrial Fume Cupboard<i class="fa fa-angle-right"></i></a></li>
<li class="leaf menu-654"><a href="/fume-cupboards-industrial-higher-education">University Fume Cupboard<i class="fa fa-angle-right"></i></a></li>
<li class="leaf menu-657"><a href="/laminar-flow-class-ii-cabinets">Laminar Flow & Class II Cabinets<i class="fa fa-angle-right"></i></a></li>
<li class="leaf menu-655"><a href="/forensic-chemical-storage">Forensic & Chemical Storage<i class="fa fa-angle-right"></i></a></li>
<li class="last leaf menu-930"><a href="https://www.safelab.co.uk/case-studies">Case Studies<i class="fa fa-angle-right"></i></a></li>
</ul>	</nav>
</div>

</div>
  </div>
                  </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="region region-home-content">
    <section id="block-gwsblocks-home-tabs" class="block block-gwsblocks clearfix">

      
  <style>
.home-tab1 {
  background-color: #DD6E1C; 
}
.home-tab1:hover {
  background-color: #e17d33; 
}
.home-tab2 {
  background-color: #782871; 
}
.home-tab2:hover {
  background-color: #863e80; 
}
.home-tab3 {
  background-color: #477EB7; 
}
.home-tab3:hover {
  background-color: #5a8bbf; 
}
</style>
<div class="row">
  <div class="home-tab1-wrap col-md-4">
    <div class="home-tab home-tab1">
      <div class="col-xs-3 home-tab-icon-wrapper">
        <div class="home-tab-icon" style="background-image: url(https://www.safelab.co.uk/sites/default/files/icon1.png);"></div>
      </div>
      <div class="col-xs-9 home-tab-link">
        <a href="schools-colleges">
          <h3>Schools and Colleges</h3>
        </a>
      </div>
    </div>
  </div>

  <div class="home-tab2-wrap col-md-4">
    <div class="home-tab home-tab2">
      <div class="col-xs-3 home-tab-icon-wrapper">
        <div class="home-tab-icon" style="background-image: url(https://www.safelab.co.uk/sites/default/files/icon2.png);"></div>
      </div>
      <div class="col-xs-9 home-tab-link">
        <a href="industrial-higher-education">
          <h3>Industrial and Higher Education</h3>
        </a>
      </div>
    </div>
  </div>

  <div class="home-tab3-wrap col-md-4">
    <div class="home-tab home-tab3">
      <div class="col-xs-3 home-tab-icon-wrapper">
        <div class="home-tab-icon" style="background-image: url(https://www.safelab.co.uk/sites/default/files/icon3_0.png);"></div>
      </div>
      <div class="col-xs-9 home-tab-link">
        <a href="service-maintenance">
          <h3>Service and Maintenance</h3>
        </a>
      </div>
    </div>
  </div>
</div>
</section>
<section id="block-gwsblocks-welcome" class="block block-gwsblocks clearfix">

      
  <div class="welcome-message"><h1>Fume Cupboard Innovation</h1>

<h3>Safelab is&nbsp;a leading&nbsp;UK designer and manufacturer&nbsp;of high quality <strong>fume cupboards</strong> and <strong>safety cabinets</strong> with a rich heritage that&nbsp;stretches back over 30 years.</h3>

<p>We install our products for a wide range of clients, from&nbsp;schools and small laboratories&nbsp;to&nbsp;entire university departments and&nbsp;industrial R&amp;D&nbsp;facilities.&nbsp;We&nbsp;test and maintain any fume cupboard, whether&nbsp;Safelab or another manufacturers' to support our customers ongoing legal compliance requirements under COSHH Regulation 9. We work directly with end users and&nbsp;laboratory furniture companies to provide the perfect solution and in many cases have become&nbsp;the preferred supplier of fume cupboards for some of the largest construction companies in the UK.&nbsp;Safelab stands for:-</p>

<ul>
	<li><strong>High quality manufacturing</strong>.&nbsp;A Safelab fume cupboard&nbsp;is&nbsp;designed and manufactured by us in our purpose-built facility near Bristol.&nbsp;We have full control over the quality of our products, which we can&nbsp;customise for specific requirements.</li>
	<li><strong>Pre-sale consultation</strong>.&nbsp;We listen&nbsp;to your needs and use&nbsp;our expertise to provide solutions that work for your application.</li>
	<li><strong>Outstanding customer support</strong> from&nbsp;our&nbsp;technical and customer service team at our UK head office and our&nbsp;nationwide team of highly trained engineers.</li>
	<li><strong>Innovation</strong>.&nbsp;We continually strive to enhance our products for the benefit of our customers.</li>
</ul>

<p>Our core values, drive to innovate and dedication to quality means&nbsp;every fume cupboard we manufacture&nbsp;is at the top end of the market and has made us one of the leading&nbsp;manufacturers in the UK. Whether we're supplying filtration cupboards into a local school, a mixture of ducted and filtered cupboards into the largest private school installation in the UK or installing over 100 cupboards into Loughborough University we treat every order equally&nbsp;from enquiry to after sales care. We're here to offer the best service and product of any fume cupboard manufacturer in the country.</p>
</div>
</section>
  </div>
                              <div>
      </div>
    </section>

  <footer class="footer-top container">
      <div class="region region-footer-top">
    <section id="block-menu-menu-quick-links" class="block block-menu col-lg-3 col-md-3 col-sm-12 col-xs-12 clearfix">

        <h2 class="block-title">Fume Cupboard Quick Links</h2>
    
  <ul class="menu nav"><li class="first leaf menu-570"><a href="/fume-cupboards-schools-colleges">Schools &amp; Colleges</a></li>
<li class="leaf menu-573"><a href="/fume-cupboards-industrial-higher-education">Industrial &amp; Higher Education</a></li>
<li class="leaf menu-572"><a href="/laminar-flow-class-ii-cabinets">Laminar Flow Cabinets</a></li>
<li class="leaf menu-571"><a href="/downloads">Downloads</a></li>
<li class="leaf menu-611"><a href="/case-studies">Case Studies</a></li>
<li class="leaf menu-928"><a href="http://www.safelab.co.uk/news">News</a></li>
<li class="last leaf menu-929"><a href="http://www.safelab.co.uk/our-clients">Our Clients</a></li>
</ul>
</section>
<section id="block-gwsblocks-footer-credits" class="block block-gwsblocks col-lg-4 col-md-4 col-sm-6 col-xs-12 clearfix">

      
  <div class="col-xs-6 footer-credits-img-wrapper">
  <div class="footer-credits-img" style="background-image: url(https://www.safelab.co.uk/sites/default/files/new_iso_logo.ai_.png);"></div>
</div>
<div class="col-xs-6 footer-credits-img-wrapper">
  <div class="footer-credits-img" style="background-image: url(https://www.safelab.co.uk/sites/default/files/safecontr.jpg);"></div>
</div>
</section>
<section id="block-block-2" class="block block-block clearfix">

        <h2 class="block-title">Contact Us for all your Fume Cupboard needs</h2>
    
  <ul>
<li>Tel: +44(0) 1934 421340</li>
<li>Email: <a href="mailto:enquiries@safelab.co.uk">enquiries@safelab.co.uk</a></li>
</ul>

</section>
  </div>
  </footer>
  <footer class="footer-bottom container-fluid">
      <div class="region region-footer-bottom">
    <section id="block-gwsblocks-footer-info" class="block block-gwsblocks container clearfix">

      
  <span class="footer-info-item copy"><span>&copy;2019</span><span>Safelab Limited</span></span>
<span class="footer-info-item registration">Registered in England and Wales No.05336826</span>
<span class="footer-info-item vat">VAT Registration Number <span class="vat-number">860 2131 63</span></span>
</section>
<section id="block-menu-menu-footer-links" class="block block-menu container clearfix">

      
  <ul class="menu nav"><li class="first leaf menu-659"><a href="/terms-conditions">Terms and Conditions</a></li>
<li class="leaf menu-585"><a href="/privacy-policy">Privacy Policy</a></li>
<li class="leaf menu-586"><a href="/accessibility">Accessibility</a></li>
<li class="last leaf menu-584"><a href="/website-terms-use">Website Terms of Use</a></li>
</ul>
</section>
  </div>
  </footer>
   <script src="https://www.safelab.co.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
