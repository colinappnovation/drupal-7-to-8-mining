<!DOCTYPE html>
<html lang="en" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="Cole Associates and based in Manchester and are specialists in Corporate Finance, Management Buy Outs, Management Buy Ins and other financial services" />
<meta name="keywords" content="Cole Associates, Corporate Finance, Management Buy Outs, Management Buy ins, Acquisitions, Disposals, Raising Development Capital, Business Plans and Financial Projections, Valuation, Strategic Business Advice, Rebanking, Restructure and Rescue, Due Diligence" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.cole-group.co.uk/" />
<link rel="shortlink" href="https://www.cole-group.co.uk/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.cole-group.co.uk/" />
<meta name="twitter:title" content="Cole Associates" />
<link rel="shortcut icon" href="https://www.cole-group.co.uk/sites/default/files/favicon_0.png" type="image/png" />
<title>Cole Associates | Corporate Finance, Chartered Accountants | Manchester</title>
<style type="text/css" media="all">
@import url("https://www.cole-group.co.uk/modules/system/system.base.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/system/system.menus.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/system/system.messages.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/system/system.theme.css?q19gw5");
</style>
<style type="text/css" media="all">
@import url("https://www.cole-group.co.uk/sites/all/libraries/slick/slick/slick.css?q19gw5");
</style>
<style type="text/css" media="all">
@import url("https://www.cole-group.co.uk/modules/comment/comment.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/date/date_api/date.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/field/theme/field.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/node/node.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/search/search.css?q19gw5");
@import url("https://www.cole-group.co.uk/modules/user/user.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/nodeorder/css/nodeorder.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/views/css/views.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?q19gw5");
</style>
<style type="text/css" media="all">
@import url("https://www.cole-group.co.uk/sites/all/modules/ctools/css/ctools.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/eu_cookie_compliance/css/eu_cookie_compliance.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/libraries/sidr/stylesheets/jquery.sidr.dark.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/modules/addtoany/addtoany.css?q19gw5");
</style>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#222222;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}

/*]]>*/-->
</style>
<style type="text/css" media="all">
@import url("https://www.cole-group.co.uk/sites/all/themes/coleassociates/css/bootstrap.min.css?q19gw5");
@import url("https://www.cole-group.co.uk/sites/all/themes/coleassociates/style.css?q19gw5");
</style>
<script type="text/javascript" src="https://www.cole-group.co.uk/sites/default/files/js/js_0RyHJ63yYLuaWsodCPCgSD8dcTIA0dqcDf8-7c2XdBw.js"></script>
<script type="text/javascript" src="https://www.cole-group.co.uk/sites/default/files/js/js_3wnGm9aUv5jQLmDAkDC7cfdVshfX3iE-Ul_hK2-PqLA.js"></script>
<script type="text/javascript" src="https://www.cole-group.co.uk/sites/default/files/js/js_yVN-NDqAFxB8KA3G3T0UJ0L8sjuWhMgbY471XZajyPk.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-34329920-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cole-group.co.uk/sites/default/files/js/js_38dYfT5tBcNRDpGLvmZst3EvwIKHRrNMAUPl_CmZHoU.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"coleassociates","theme_token":"wjgrSutH99JdoLlh_pZT69t5OJhn1aLK7wBINv4VlVs","js":{"0":1,"sites\/all\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/slick\/slick\/slick.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/themes\/coleassociates\/js\/jquery.flexslider.js":1,"sites\/all\/themes\/coleassociates\/js\/slide.js":1,"misc\/collapse.js":1,"misc\/form.js":1,"sites\/all\/modules\/slick\/js\/slick.load.min.js":1,"sites\/all\/modules\/responsive_menus\/styles\/sidr\/js\/responsive_menus_sidr.js":1,"sites\/all\/libraries\/sidr\/jquery.sidr.min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/themes\/coleassociates\/js\/bootstrap.min.js":1,"sites\/all\/themes\/coleassociates\/js\/superfish.js":1,"sites\/all\/themes\/coleassociates\/js\/mobilemenu.js":1,"sites\/all\/themes\/coleassociates\/js\/custom.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/libraries\/slick\/slick\/slick.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/nodeorder\/css\/nodeorder.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"sites\/all\/libraries\/sidr\/stylesheets\/jquery.sidr.dark.css":1,"sites\/all\/modules\/addtoany\/addtoany.css":1,"0":1,"sites\/all\/themes\/coleassociates\/css\/bootstrap.min.css":1,"sites\/all\/themes\/coleassociates\/style.css":1}},"slick":{"accessibility":true,"adaptiveHeight":false,"autoplay":false,"autoplaySpeed":3000,"pauseOnHover":true,"pauseOnDotsHover":false,"arrows":true,"centerMode":false,"centerPadding":"50px","dots":false,"dotsClass":"slick-dots","draggable":true,"fade":false,"focusOnSelect":false,"infinite":true,"initialSlide":0,"lazyLoad":"ondemand","mousewheel":false,"randomize":false,"rtl":false,"rows":1,"slidesPerRow":1,"slide":"","slidesToShow":1,"slidesToScroll":1,"speed":500,"swipe":true,"swipeToSlide":false,"edgeFriction":0.35,"touchMove":true,"touchThreshold":5,"useCSS":true,"cssEase":"ease","useTransform":true,"easing":"linear","variableWidth":false,"vertical":false,"verticalSwiping":false,"waitForAnimate":true},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EDecline\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--opt-in\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-secondary-button\u0022\u003EOK, I agree\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022decline-button eu-cookie-compliance-default-button\u0022 \u003EDecline\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EHide\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EMore info\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/privacy-policy","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EYou have given your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":false,"cookie_categories":[],"enable_save_preferences_button":true,"fix_first_cookie_category":true,"select_all_categories_by_default":false},"responsive_menus":[{"selectors":["#main-menu"],"trigger_txt":"\u2630 Menu","side":"left","speed":"200","media_size":"3000","displace":"1","renaming":"1","onOpen":"","onClose":"","responsive_menus_style":"sidr"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="/sites/all/themes/coleassociates/js/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
    <div id="page">
<div id="outer-masthead">
  <header id="masthead" class="site-header" role="banner">
    <div class="row">

	<div class="col-sm-2 col-xs-2 mainmenu">
        <nav id="navigation" role="navigation">
          <div id="main-menu">
            <ul class="menu"><li class="first leaf active-trail"><a href="/home" class="active-trail active">Home</a></li>
<li class="leaf"><a href="/about">About</a></li>
<li class="collapsed"><a href="/services" title="">Services</a></li>
<li class="leaf"><a href="/our-partners">Our Partners</a></li>
<li class="leaf"><a href="/transactions-and-news" title="">Transactions and News</a></li>
<li class="leaf"><a href="/investment-and-acquisition-opportunities" title="">Investment and Acquisition &amp; Opportunities</a></li>
<li class="leaf"><a href="/sectors">Sectors</a></li>
<li class="leaf"><a href="/corporate-finance-guides" title="">Corporate Finance Guides</a></li>
<li class="leaf"><a href="/cole-capital">Cole Capital</a></li>
<li class="leaf"><a href="/working-us">Working With Us</a></li>
<li class="last leaf"><a href="/contact">Contact</a></li>
</ul>          </div>
        </nav>
      </div>
	  <div class="top-contact col-sm-3 col-xs-3">
      	<p><strong>0161 832 9945</strong><br />
		<a href="mailto:enquiries@cole-group.co.uk" class="email">enquiries@cole-group.co.uk</a></p>
      </div>
      <div id="logo" class="site-branding col-sm-2 col-xs-2">
        <div id="site-logo"><a href="/" title="Home">
          <img src="https://www.cole-group.co.uk/sites/default/files/logo_0.png" alt="Home" />
        </a></div>      </div>
	  <div class="slogan col-sm-3 col-xs-3">
        <p><strong>Corporate Finance</strong><br />
		Chartered Accountants</p>
      </div>
	  <div class="top-get-in-touch col-sm-2 col-xs-2">
       <a href="/contact">Contact Us</a>
      </div>


      


    </div>
  </header>
</div>


      
  
      <div id="header-block">
  
        
            <div class="region region-header">
  <div id="block-views-home-banner-slide-block" class="block block-views">

      
  <div class="content">
    <div class="view view-home-banner-slide view-id-home_banner_slide view-display-id-block view-dom-id-aae90e520e0e7af9699729b29ab1db51">
        
  
  
      <div class="view-content">
            <div class="slick slick--view--home-banner-slide slick--view--home-banner-slide--block slick--optionset--clone-of-default" id="slick-views-home-banner-slide-1">
      <div class="slick__slider" id="slick-views-home-banner-slide-1-slider" data-slick="{&quot;adaptiveHeight&quot;:true,&quot;autoplay&quot;:true,&quot;autoplaySpeed&quot;:5000,&quot;speed&quot;:800,&quot;useTransform&quot;:false}">
  
          <div class="slick__slide slide slide--0">      <div class="slide__content">              
          <div class="flex-caption views-fieldset" data-module="views_fieldsets">
            <div class="views-field views-field-body"><div class="field-content"><p>Our <strong>unique approach to corporate finance</strong> advice is characterised by our relationship with clients. We treat each transaction as unique, and provide an exceptionally <strong>high standard of client service and quality of advice</strong>.</p>
</div></div>                <div class="views-field views-field-field-link"><div class="field-content"><a href="https://www.cole-group.co.uk/about">Find out more</a></div></div>      </div>

    
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/banner_slide/public/banner-4.jpg?itok=fCbJOcy9" width="1500" height="650" alt="" /></div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--1">      <div class="slide__content">              
          <div class="flex-caption views-fieldset" data-module="views_fieldsets">
            <div class="views-field views-field-body"><div class="field-content"><p>Our mission is to be one of the most respected and successful <strong>specialist corporate finance </strong> firms in the North West by <strong>helping ambitious businesses</strong> and management teams <strong>achieve and exceed their goals</strong>.</p>
</div></div>                <div class="views-field views-field-field-link"><div class="field-content"><a href="https://www.cole-group.co.uk/about">Find out more</a></div></div>      </div>

    
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/banner_slide/public/manchester.jpg?itok=_hWamCPI" width="1500" height="650" alt="" /></div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--2">      <div class="slide__content">              
          <div class="flex-caption views-fieldset" data-module="views_fieldsets">
            <div class="views-field views-field-body"><div class="field-content"><p>We provide <strong>straight-talking advice</strong>, <strong>lateral thinking</strong> to unlock issues on transactions in <strong>innovative ways</strong>, and <strong>practical solutions</strong> to complex problems.</p>
</div></div>                <div class="views-field views-field-field-link"><div class="field-content"><a href="https://www.cole-group.co.uk/about">Find out more</a></div></div>      </div>

    
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/banner_slide/public/salford-quays-2.jpg?itok=gln4TG7c" width="1500" height="650" alt="" /></div>  </div>            
      
    </div>  </div>    
      </div>
    <nav class="slick__arrow">
      <button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>            <button type="button" data-role="none" class="slick-next" aria-label="next">next</button>    </nav>
  </div>
      </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
        
      
    </div>
  
    <div id="main-content">
    <div class="container"> 
      <div class="row">
                <div id="primary" class="content-area col-sm-12">
          <section id="content" role="main" class="clearfix">
                                                <div id="content-wrap">
                                                        <div class="region region-content">
  <div id="block-system-main" class="block block-system">

      
  <div class="content">
    <div  about="/home" typeof="foaf:Document" class="ds-2col-stacked node node-page node-full view-mode-full  clearfix">

  
  <div class="group-header col-lg-12 col-md-12 col-sm-12">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><h1>Cole Associates recognise that each client's transaction is probably the most important business event in their lives and we treat it as such.</h1>
</div></div></div>  </div>

  <div class="group-left col-lg-6 col-md-6 col-sm-12">
    <div class="field field-name-field-left-column field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p>We provide straight-talking advice, lateral thinking to unlock issues on transactions in innovative ways, and practical solutions to complex problems - delivered with enthusiasm and energy. Our unique approach to corporate finance advice is characterised by our relationship with clients. We provide bespoke, exclusive advice to a select number of high quality clients, taking time to understand their needs and aspirations.</p>
<p>We treat each client's transaction as unique, and provide an exceptionally high standard of client service and quality of advice.</p>
</div></div></div>  </div>

  <div class="group-right col-lg-6 col-md-6 col-sm-12">
    <div class="field field-name-field-right-column field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p>A further distinguishing feature of our approach is our focus on the project management of each client's transaction, facilitating a prompt completion, by 'driving' the timetable. Without exception, a transaction that is managed proactively in this way is more likely to reach a successful completion than one that is allowed to 'drift'.</p>
</div></div></div>  </div>

  <div class="group-footer">
      </div>

</div>

  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
            </div>
          </section>
        </div>
              </div>
    </div>
  </div>

     <div id="pre-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="region region-pre-footer">
  <div id="block-views-home-page-services-block" class="block block-views">

        <h2 >Our Services</h2>
    
  <div class="content">
    <div class="view view-home-page-services view-id-home_page_services view-display-id-block view-dom-id-fde37b5ada98143cbf8792ada69f3666">
            <div class="view-header">
      <p>Cole Associates' team has many years of experience in advising a wide variety of clients, across many different sectors, in the UK and overseas. Our services include:</p>
    </div>
  
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/management-buy-outs-mbos"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/london.jpg?itok=JRHxxxFZ" width="500" height="320" alt="Management Buy-outs (MBOs)" title="Management Buy-outs (MBOs)" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/management-buy-outs-mbos">Management Buy-outs (MBOs)</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/management-buy-ins-mbis"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/san-francisco.jpg?itok=c8gLfSN-" width="500" height="320" alt="Management Buy-ins (MBIs)" title="Management Buy-ins (MBIs)" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/management-buy-ins-mbis">Management Buy-ins (MBIs)</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/acquisitions"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/new-york.jpg?itok=XtrI5YEe" width="500" height="320" alt="Acquisitions" title="Acquisitions" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/acquisitions">Acquisitions</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/business-sales"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/tokyo.jpg?itok=3zC-0Z4p" width="500" height="320" alt="Business Sales" title="Business Sales" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/business-sales">Business Sales</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/raising-finance"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/tokyo_0.jpg?itok=FofAxYRv" width="500" height="320" alt="Raising Finance" title="Raising Finance" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/raising-finance">Raising Finance</a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/valuation"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/rio-de-janeiro.jpg?itok=O11SVmmU" width="500" height="320" alt="Valuation" title="Valuation" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/valuation">Valuation</a></span>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/strategic-business-advice"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/moscow.jpg?itok=a-htgSan" width="500" height="320" alt="Strategic Business Advice" title="Strategic Business Advice" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/strategic-business-advice">Strategic Business Advice</a></span>  </div>  </div>
  <div class="views-row views-row-8 views-row-even views-row-last col-lg-3 col-md-4 col-sm-6 col-xs-12">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><a href="/due-diligence"><img typeof="foaf:Image" src="https://www.cole-group.co.uk/sites/default/files/styles/thumbnail/public/rome.jpg?itok=83xIS57g" width="500" height="320" alt="Due Diligence" title="Due Diligence" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/due-diligence">Due Diligence</a></span>  </div>  </div>
    </div>
  
  
  
  
      <div class="view-footer">
      <p><a href="/services">View all services</a></p>
    </div>
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>
        </div>
      </div>
    </div>
  
      <div id="footer-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="region region-footer">
  <div id="block-views-testimonial-block-block" class="block block-views">

      
  <div class="content">
    <div class="view view-testimonial-block view-id-testimonial_block view-display-id-block view-dom-id-623135863e5632d0374ee8f93936a273">
        
  
  
      <div class="view-content">
            <div class="slick slick--view--testimonial-block slick--view--testimonial-block--block slick--optionset--testimonials" id="slick-views-testimonial-block-2">
      <div class="slick__slider" id="slick-views-testimonial-block-2-slider" data-slick="{&quot;useTransform&quot;:false}">
  
          <div class="slick__slide slide slide--0">      <div class="slide__content">              
  <div class="views-field views-field-body">        <div class="field-content"><p>" I’m grateful to Cole Associates for guiding the four shareholders through the sale process. Their corporate finance skills, tenacity and professionalism have ensured an excellent outcome for the shareholders, with close communication and clear explanations at the heart of their approach"</p>
</div>  </div>  
  <div class="views-field views-field-field-client-name-and-company">        <div class="field-content">John Martin, Managing Director, TerraConsult Ltd, April 2019</div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--1">      <div class="slide__content">              
  <div class="views-field views-field-body">        <div class="field-content"><p>"Jeremy and David guided us through the complex MBO process with great skill, energy and enthusiasm; recognising it was all new to us. Their knowledge of the funding market and their connections with the right types of funders for our deal were fundamental to the successful outcome - they negotiated great terms for us. They also helped project-manage the whole process and deliver completion on time. We would definitely recommend Cole Associates to all prospective MBO teams and companies looking to grow by acquisition."</p>
</div>  </div>  
  <div class="views-field views-field-field-client-name-and-company">        <div class="field-content">James Hickman, CEO, Online Home Retail Ltd (www.plumbworld.co.uk), January 2019</div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--2">      <div class="slide__content">              
  <div class="views-field views-field-body">        <div class="field-content"><p>"Cole Associates' advice and guidance was crucial to the successful outcome of the sale of the company. They ran the sale process in a thorough and professional way, and handled all the issues that came up with tact and skill”</p>
</div>  </div>  
  <div class="views-field views-field-field-client-name-and-company">        <div class="field-content">Crispin Gilson | Owner | B.T.S. Haulage Ltd</div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--3">      <div class="slide__content">              
  <div class="views-field views-field-body">        <div class="field-content"><p>“Jeremy and David at Cole Associates guided me expertly through the whole MBO process. From the day we appointed them to the day of completion they were ‘on it’ 100%. Their knowledge of the funding market and experience in handling negotiations with Grafton Group on my behalf were vital ingredients to achieving such a great result, and allowed me to focus on running the business during the MBO process.”</p>
</div>  </div>  
  <div class="views-field views-field-field-client-name-and-company">        <div class="field-content">Terry Seville | Managing Director | Boole’s Tools and Pipe Fittings Ltd</div>  </div>            
      
    </div>  </div>          <div class="slick__slide slide slide--4">      <div class="slide__content">              
  <div class="views-field views-field-body">        <div class="field-content"><p>Cole Associates’ advice and guidance was crucial to the successful outcome. They ran the sale process in a thorough and professional way, and handled all the issues that came up with tact and skill.</p>
</div>  </div>  
  <div class="views-field views-field-field-client-name-and-company">        <div class="field-content">Crispin Gilson | BTS</div>  </div>            
      
    </div>  </div>    
      </div>
    <nav class="slick__arrow">
      <button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>            <button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>    </nav>
  </div>
      </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>
        </div>
      </div>
    </div>
  
	<div id="call-to-action">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-xs-12">
		<p>Looking for straight-talking advice to help your company?</p>
		</div>

		<div class="col-lg-3 col-md-3 col-xs-12">
		<a href="/contact" class="button">Get in touch</a>
		</div>
      </div>
    </div>
  </div>

          <div id="bottom">
      <div class="container">
        <div class="row">
          <div class="footer-block col-sm-6 col-xs-6">
            <div class="region region-footer-first">
  <div id="block-block-1" class="block block-block">

      
  <div class="content">
    <p><strong>0161 832 9945</strong><br />
<a href="mailto:enquiries@cole-group.co.uk">enquiries@cole-group.co.uk</a></p>
<p>Cole Associates Corporate Finance<br />
19 Spring Gardens<br />
Manchester<br />
M2 1FB</p>
<p><a class="footer-link" href="/privacy-policy">Privacy Policy</a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>          <div class="footer-block col-sm-6 col-xs-6">
            <div class="region region-footer-second">
  <div id="block-block-2" class="block block-block">

      
  <div class="content">
    <p><a href="/"><img src="/images/footer-logo.png" alt="Cole Associates" /></a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
          </div>                            </div>
      </div>
    </div>
  
  <footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="fcred col-sm-12">

        </div>
      </div>
    </div>
  </div>
</div>  <script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cole-group.co.uk/sites/default/files/js/js_Llgek5Zasqh0wiimoKH-uIdmSIEO0i9Cbi7UdXEdRgw.js"></script>
</body>
</html>