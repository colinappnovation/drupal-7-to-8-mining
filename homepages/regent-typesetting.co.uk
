<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.regent-typesetting.co.uk/sites/default/files/favicon_1.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="1" />
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.regent-typesetting.co.uk/" />
<link rel="shortlink" href="https://www.regent-typesetting.co.uk/" />
<meta property="og:site_name" content="Regent Typesetting" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.regent-typesetting.co.uk/" />
<meta property="og:title" content="Regent Typesetting" />
<meta property="og:description" content="providing professional typesetting since 1991" />
<meta name="dcterms.title" content="Regent Typesetting" />
<meta name="dcterms.description" content="providing professional typesetting since 1991" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.format" content="text/html" />
<meta name="dcterms.identifier" content="https://www.regent-typesetting.co.uk/" />
<title>Regent Typesetting | providing professional typesetting since 1991</title>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/modules/system/system.base.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/system/system.menus.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/system/system.messages.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/system/system.theme.css?pr2xxi");
</style>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/views_slideshow/views_slideshow.css?pr2xxi");
</style>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/date/date_api/date.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/field/theme/field.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/node/node.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/pm/pm.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/modules/user/user.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/views/css/views.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/ckeditor/css/ckeditor.css?pr2xxi");
</style>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/ctools/css/ctools.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/panels/css/panels.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/flexslider/assets/css/flexslider_img.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/libraries/flexslider/flexslider.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/modules/contributed/views_ticker/views.ticker.scroller.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/css/superfish.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/css/superfish-smallscreen.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/style/simple/simple.css?pr2xxi");
</style>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.headings.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.settings.style.image.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/adaptivetheme/at_core/css/at.layout.css?pr2xxi");
</style>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/styles.base.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/styles.modules.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/styles.settings.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/styles.custom.css?pr2xxi");
@import url("https://www.regent-typesetting.co.uk/sites/default/files/color/at_memento-5725129b/colors.css?pr2xxi");
</style>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/default/files/adaptivetheme/at_memento_files/at_memento.default.layout.css?pr2xxi");
</style>
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/default/files/adaptivetheme/at_memento_files/at_memento.responsive.layout.css?pr2xxi" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.custom.css?pr2xxi" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.smalltouch.portrait.css?pr2xxi" media="only screen and (max-width:320px)" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.smalltouch.landscape.css?pr2xxi" media="only screen and (min-width:321px) and (max-width:480px)" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.tablet.portrait.css?pr2xxi" media="only screen and (min-width:581px) and (max-width:768px)" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.tablet.landscape.css?pr2xxi" media="only screen and (min-width:769px) and (max-width:1024px)" />
<link type="text/css" rel="stylesheet" href="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/responsive.desktop.css?pr2xxi" media="only screen and (min-width:1025px)" />
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/default/files/adaptivetheme/at_memento_files/at_memento.custom.css?pr2xxi");
</style>
<style type="text/css" media="all">
@import url("https://www.regent-typesetting.co.uk/sites/default/files/fontyourface/font.css?pr2xxi");
</style>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Gudea:regular&amp;subset=latin-ext" media="all" />

<!--[if lt IE 8]>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/default/files/adaptivetheme/at_memento_files/at_memento.lt-ie8.layout.css?pr2xxi");
</style>
<![endif]-->

<!--[if IE 6]>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/ie-6.css?pr2xxi");
</style>
<![endif]-->

<!--[if lte IE 7]>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/ie-lte-7.css?pr2xxi");
</style>
<![endif]-->

<!--[if IE 8]>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/ie-8.css?pr2xxi");
</style>
<![endif]-->

<!--[if lte IE 9]>
<style type="text/css" media="screen">
@import url("https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/css/ie-lte-9.css?pr2xxi");
</style>
<![endif]-->
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/jquery_update/replace/jquery/1.5/jquery.min.js?v=1.5.2"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/misc/jquery-extend-3.4.0.js?v=1.5.2"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/misc/drupal.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/views_slideshow/js/views_slideshow.js?v=1.0"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/flexslider/jquery.flexslider-min.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/flexslider/flexslider_views_slideshow/js/flexslider_views_slideshow.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/views_ticker/js/jquery.vticker-min.js?pr2xxi"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function($) { Drupal.behaviors.views_ticker_vTicker = { attach: function (context) { $("#views-ticker-vTicker-nodequeue_2").vTicker({mousePause: false , speed:1000 , pause:5000 , showItems:1 , direction: "up"}); } };})(jQuery);
//--><!]]>
</script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/jquery.hoverIntent.minified.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/sfsmallscreen.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/supposition.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/superfish.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/libraries/superfish/supersubs.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/superfish/superfish.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/themes/at_memento/js/memento.js?pr2xxi"></script>
<script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/scalefix.js?pr2xxi"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"at_memento","theme_token":"KvDPBuZlp9XwNOTFAqcbgdIm71MTiqqbJia5TBDuw7Y","js":{"sites\/all\/modules\/contributed\/flexslider\/assets\/js\/flexslider.load.js":1,"sites\/all\/modules\/contributed\/jquery_update\/replace\/jquery\/1.5\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contributed\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/modules\/contributed\/flexslider\/flexslider_views_slideshow\/js\/flexslider_views_slideshow.js":1,"sites\/all\/modules\/contributed\/views_ticker\/js\/jquery.vticker-min.js":1,"0":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/contributed\/superfish\/superfish.js":1,"sites\/all\/themes\/at_memento\/js\/memento.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/scalefix.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contributed\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/contributed\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contributed\/pm\/pm.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contributed\/views\/css\/views.css":1,"sites\/all\/modules\/contributed\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contributed\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contributed\/panels\/css\/panels.css":1,"sites\/all\/modules\/contributed\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/modules\/contributed\/views_ticker\/views.ticker.scroller.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/libraries\/superfish\/css\/superfish-smallscreen.css":1,"sites\/all\/libraries\/superfish\/style\/simple\/simple.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/at_memento\/css\/styles.base.css":1,"sites\/all\/themes\/at_memento\/css\/styles.modules.css":1,"sites\/all\/themes\/at_memento\/css\/styles.commerce.css":1,"sites\/all\/themes\/at_memento\/css\/styles.settings.css":1,"sites\/all\/themes\/at_memento\/css\/styles.custom.css":1,"sites\/all\/themes\/at_memento\/color\/colors.css":1,"public:\/\/adaptivetheme\/at_memento_files\/at_memento.default.layout.css":1,"public:\/\/adaptivetheme\/at_memento_files\/at_memento.responsive.layout.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.custom.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.smalltouch.portrait.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.smalltouch.landscape.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.tablet.portrait.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.tablet.landscape.css":1,"sites\/all\/themes\/at_memento\/css\/responsive.desktop.css":1,"public:\/\/adaptivetheme\/at_memento_files\/at_memento.custom.css":1,"sites\/default\/files\/fontyourface\/font.css":1,"https:\/\/fonts.googleapis.com\/css?family=Gudea:regular\u0026subset=latin-ext":1,"public:\/\/adaptivetheme\/at_memento_files\/at_memento.lt-ie8.layout.css":1,"sites\/all\/themes\/at_memento\/css\/ie-6.css":1,"sites\/all\/themes\/at_memento\/css\/ie-lte-7.css":1,"sites\/all\/themes\/at_memento\/css\/ie-8.css":1,"sites\/all\/themes\/at_memento\/css\/ie-lte-9.css":1}},"viewsSlideshow":{"nodequeue_1-block_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","flexsliderViewsSlideshow","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","flexsliderViewsSlideshow","viewsSlideshowCycle"],"play":["viewsSlideshowControls","flexsliderViewsSlideshow","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","flexsliderViewsSlideshow","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"flexslider_views_slideshow":{"#flexslider_views_slideshow_main_nodequeue_1-block_1":{"num_divs":4,"id_prefix":"#flexslider_views_slideshow_main_","vss_id":"nodequeue_1-block_1","animation":"fade","slideDirection":"horizontal","slideshow":true,"slideshowSpeed":7000,"animationLoop":true,"randomize":false,"slideToStart":0,"directionNav":false,"controlNav":false,"keyboardNav":1,"mousewheel":1,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","pauseOnAction":true,"manualControls":"","animationSpeed":600,"direction":"","keyboard":false,"startAt":0,"start":null,"before":null,"after":null,"end":null,"namespace":"flex-","selector":".slides \u003E li","easing":"swing","reverse":false,"smoothHeight":false,"initDelay":0,"useCSS":true,"touch":true,"video":false,"multipleKeyboard":false,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"thumbCaptions":false,"thumbCaptionsBoth":false,"pauseOnHover":false}},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"fast"},"plugins":{"smallscreen":{"mode":"window_width","breakpointUnit":"px","title":"Main menu"},"supposition":true,"supersubs":true}}},"adaptivetheme":{"at_memento":{"layout_settings":{"bigscreen":"two-sidebars-right","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:581px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"}}}});
//--><!]]>
</script>
<!--[if lt IE 9]>
<script src="https://www.regent-typesetting.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?pr2xxi"></script>
<![endif]-->
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-21 node-type-page pm-use-storm-icons atv-7.x-3.1 lang-en site-name-regent-typesetting at-memento bgo-n fpo-h1 pfo-h2 hl-l mb-ah  itrc-0 isrc-6">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper">
  <div id="page" class="page snc-uc snw-n sna-l sns-n ssc-n ssw-n ssa-l sss-n btc-uc btw-n bta-l bts-n ntc-n ntw-n nta-l nts-n ctc-n ctw-n cta-l cts-n ptc-n ptw-n pta-l pts-n">

    <div id="menu-top-wrapper">
      <div class="container clearfix">
              </div>
    </div>

    <div id="header-wrapper" class="no-header-region">
      <div class="container clearfix">

        <header id="header" class="clearfix" role="banner">

                      <!-- !Branding -->
            <div id="branding" class="branding-elements clearfix">

                              <div id="logo">
                  <a href="/" class="active"><img class="site-logo" src="https://www.regent-typesetting.co.uk/sites/default/files/logo_magenta_web_3.jpg" alt="Regent Typesetting" /></a>                </div>
              
                              <!-- !Site name and Slogan -->
                <div class="h-group" id="name-and-slogan">

                                      <h1 id="site-name"><a href="/" title="Home page" class="active">Regent Typesetting</a></h1>
                  
                                      <h2 id="site-slogan">providing professional typesetting since 1991</h2>
                  
                </div>
              
            </div>
          
                      <div id="menu-wrapper">
              <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix odd first last block-count-1 block-region-menu-bar block-1">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
              <h2 class="element-invisible block-title element-invisible">Main menu</h2>
      
      
      <div class="block-content content clearfix">
        <ul  id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-simple sf-total-items-2 sf-parent-items-1 sf-single-items-1"><li id="menu-219-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-905-1" class="last even sf-item-2 sf-depth-1 sf-total-children-2 sf-parent-children-0 sf-single-children-2 menuparent"><a href="/about" class="sf-depth-1 menuparent">About</a><ul><li id="menu-907-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/our-customers" class="sf-depth-2">Our customers</a></li><li id="menu-906-1" class="last even sf-item-2 sf-depth-2 sf-no-children"><a href="/our-services" class="sf-depth-2">Our services</a></li></ul></li></ul>      </div>

    </div>
  </div>

</nav>
</div>            </div>
          
          
        </header>
      </div>
    </div>

    
          <div id="secondary-content-wrapper">
        <div class="container clearfix">
          <div class="region region-secondary-content"><div class="region-inner clearfix"><div id="block-views-nodequeue-1-block" class="block block-views no-title odd first last block-count-2 block-region-secondary-content block-nodequeue-1-block">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
      
      
      <div class="block-content content no-title">
        <div class="view view-nodequeue-1 view-id-nodequeue_1 view-display-id-block view-dom-id-6ad70793b97bcbf3bb1b27ebb0ac896a">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
    <div id="flexslider_views_slideshow_main_nodequeue_1-block_1" class="flexslider_views_slideshow_main views_slideshow_main"><div class="flex-nav-container">
  <div class="flexslider">
    <ul id="flexslider_views_slideshow_nodequeue_1-block_1" class="flexslider-views-slideshow-main-frame slides">
        <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-1 views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img class="image-style-flexslider-full" src="https://www.regent-typesetting.co.uk/sites/default/files/styles/flexslider_full/public/Books.png?itok=MVMzFG1s" width="1140" height="320" alt="" />
<p class="flex-caption">Fiction and non-fiction books for print and epub distribution </p></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img class="image-style-flexslider-full" src="https://www.regent-typesetting.co.uk/sites/default/files/styles/flexslider_full/public/nspcc.png?itok=nFtXVTz9" width="1140" height="320" alt="" />
<p class="flex-caption">Research and policy reports for major UK charities </p></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img class="image-style-flexslider-full" src="https://www.regent-typesetting.co.uk/sites/default/files/styles/flexslider_full/public/IIED_Spanish.png?itok=njEP9VRd" width="1140" height="320" alt="" />
<p class="flex-caption">Working with international policy and research institutes </p></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img class="image-style-flexslider-full" src="https://www.regent-typesetting.co.uk/sites/default/files/styles/flexslider_full/public/legend.png?itok=JQg6X-VP" width="1140" height="320" alt="" />
<p class="flex-caption">Newsletters, project and research bulletins </p></div>  </div>  </li>
    </ul>
  </div>
</div></div>
      </div>
    </div>
  
  
  
  
  
  
</div>      </div>

    </div>
  </div>

</div>
</div></div>        </div>
      </div>
    
    
    
    <div id="content-wrapper">
      <div class="container">

        
        <div id="columns">
          <div class="columns-inner clearfix">
            <div id="content-column">
              <div class="content-inner">

                
                <section id="main-content">

                                                      
                  
                  <div id="content" class="clearfix"><div id="block-system-main" class="block block-system no-title odd first last block-count-3 block-region-content block-main">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
      
      
      <div class="block-content content no-title">
        <article id="article-21" class="node node-page article odd node-full 1 ia-c clearfix" role="article">
  <div class="article-inner clearfix">
  
    
    
              <header class="clearfix">

        
                  <h1 class="node-title">
                          Welcome to Regent Typesetting                      </h1>
        
        
      </header>
        
    <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even"><p>Regent Typesetting provide fast turnaround, high quality typesetting for publishers, large and small, and for individuals interested in self-publishing. <a href="mailto:info@regent-typesetting.co.uk">Contact us</a> to find out how we can help you.</p>
</div></div></div>    </div>

    
    
  </div>
</article>
      </div>

    </div>
  </div>

</div>
</div>

                </section>

                <div class="region region-content-aside"><div class="region-inner clearfix"><div id="block-views-nodequeue-3-block" class="block block-views no-title odd first last block-count-4 block-region-content-aside block-nodequeue-3-block">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
      
      
      <div class="block-content content no-title">
        <div class="view view-nodequeue-3 view-id-nodequeue_3 view-display-id-block view-dom-id-f460deb1463b8ab199737210b2495b6b">
        
  
  
      <div class="view-content">
      <div class="nodequeue">    <ul class="nodequeue">          <li class="views-row views-row-1 views-row-odd views-row-first">  
          <h2><a href="/about">About</a></h2>
<p>An established, professional typesetting service providing high-quality typesetting for publishers large and small.</p>
  </li>
          <li class="views-row views-row-2 views-row-even">  
          <h2><a href="/our-services">Our services</a></h2>
<p>We provide typesetting for books, reports and other material for print and online distribution.</p>
  </li>
          <li class="views-row views-row-3 views-row-odd views-row-last">  
          <h2><a href="/our-customers">Our customers</a></h2>
<p>We work with national and international charities, publishers and printers.</p>
  </li>
      </ul></div>    </div>
  
  
  
  
  
  
</div>      </div>

    </div>
  </div>

</div>
</div></div>
              </div>
            </div>

                        <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section id="block-views-nodequeue-2-block" class="block block-views odd first last block-count-5 block-region-sidebar-second block-nodequeue-2-block">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
              <h2 class="block-title">Testimonials</h2>
      
      
      <div class="block-content content">
        <div class="view view-nodequeue-2 view-id-nodequeue_2 view-display-id-block view-dom-id-24058a9d3ca306597f2e31902b94d3cc">
        
  
  
      <div class="view-content">
      <!-- start scroll -->
<div class='view view-nodequeue_2'><div class='view-content view-content-nodequeue_2'><div id='views-ticker-vTicker-nodequeue_2'><ul id='views-ticker-vTicker-list-nodequeue_2'><li class='views-vTicker-item views-vTicker-item-nodequeue_2'><span class='views-vTicker-tick-field'>  
  <div>        <div><p>Regent have carried out both design and typesetting services for IIED and have done so to an excellent standard. They've always been highly professional, adaptable and can deliver to meet tight deadlines. I wouldn't hesitate to recommend them to anyone!</p>
<span class="bold">IIED</span></div>  </div></span></li><li class='views-vTicker-item views-vTicker-item-nodequeue_2'><span class='views-vTicker-tick-field'>  
  <div>        <div><p>I have worked with Regent Typesetting twice now, and both times have been delighted with their work. I recommend them for their technical expertise, their service-oriented can-do approach and for their helpful way of doing business.</p>
<span class="bold">Aryanne Oade, Author</span></div>  </div></span></li><li class='views-vTicker-item views-vTicker-item-nodequeue_2'><span class='views-vTicker-tick-field'>  
  <div>        <div><p>For a reliable, professional and cost effective typesetting service, look no further than Regent.</p><span class="bold">Production Manager, SCM Press</span></div>  </div></span></li><li class='views-vTicker-item views-vTicker-item-nodequeue_2'><span class='views-vTicker-tick-field'>  
  <div>        <div><p>I can’t thank you enough for your amazing work on this project. It was unbelievable the speed and quality at which you were able to work,&nbsp;the launch event would not have been possible without you.</p><span class="bold">International Rescue Committee</span></div>  </div></span></li></ul></div></div></div>
<!-- end scroll -->
    </div>
  
  
  
  
  
  
</div>      </div>

    </div>
  </div>

</section>
</div></div>
          </div>
        </div>
      </div>
    </div>

    
    
    <div id="page-footer">
      <div class="texture-overlay">

        
                  <div id="footer-wrapper">
            <div class="container clearfix">
              <footer class="clearfix">
                <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-4" class="block block-block no-title odd first block-count-6 block-region-footer block-4">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
      
      
      <div class="block-content content no-title">
        <p style="margin-top:12px;float:right;font-size:1.2em;"><a href="mailto:info@regent-typesetting.co.uk">info@regent-typesetting.co.uk</a><br /><span style="color:#ffffff;">Tel. 07816 873157</span></p>
      </div>

    </div>
  </div>

</div>
<div id="block-block-2" class="block block-block no-title even last block-count-7 block-region-footer block-2">

  <div class="block-inner">
    <div class="block-inner-2 clearfix">

      
      
      
      <div class="block-content content no-title">
        <p>Regent Typesetting is the trading name of Regent Typesetting Limited, Company No. 10854439 | VAT No. 272 9880 58<br />
Registered office: 16 Groombridge Road, London, E9 7DP</p>
      </div>

    </div>
  </div>

</div>
</div></div>              </footer>
             </div>
           </div>
        
      </div>
    </div>

    
  </div>
</div>
  <script type="text/javascript" src="https://www.regent-typesetting.co.uk/sites/all/modules/contributed/flexslider/assets/js/flexslider.load.js?pr2xxi"></script>
</body>
</html>
