<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.visiteastside.co.uk/sites/default/files/favicon.png" type="image/png" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<meta name="description" content="Your guide to what&#039;s on in EastSide with information on things to see and do, places to eat, drink and stay in east Belfast. EastSide: Inspiring Belfast." />
<meta name="abstract" content="Things to do in east Belfast. Places to eat in east Belfast. Places to stay in east Belfast. Events east Belfast." />
<meta name="keywords" content="East Belfast, EastSide, Belfast, Northern Ireland, Events" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.visiteastside.co.uk/" />
<link rel="shortlink" href="https://www.visiteastside.co.uk/" />
  <title>Visit EastSide |</title>
  <style type="text/css" media="all">
@import url("https://www.visiteastside.co.uk/modules/system/system.base.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/system/system.menus.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/system/system.messages.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/system/system.theme.css?q19xm3");
</style>
<style type="text/css" media="all">
@import url("https://www.visiteastside.co.uk/sites/all/modules/calendar/css/calendar_multiday.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/date/date_api/date.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/field/theme/field.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/fitvids/fitvids.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/node/node.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/search/search.css?q19xm3");
@import url("https://www.visiteastside.co.uk/modules/user/user.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/extlink/extlink.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/views/css/views.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/back_to_top/css/back_to_top.css?q19xm3");
</style>
<style type="text/css" media="all">
@import url("https://www.visiteastside.co.uk/sites/all/modules/colorbox/styles/default/colorbox_style.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/modules/ctools/css/ctools.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/libraries/superfish/css/superfish.css?q19xm3");
</style>
<style type="text/css" media="all">
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/alpha/css/alpha-reset.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/alpha/css/alpha-mobile.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/alpha/css/alpha-alpha.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/formalize.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/omega-text.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/omega-branding.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/omega-menu.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/omega-forms.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/omega/omega/css/omega-visuals.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/global.css?q19xm3");
</style>

<!--[if (lt IE 9)&(!IEMobile)]>
<style type="text/css" media="all">
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default-normal.css?q19xm3");
</style>
<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)">
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default-narrow.css?q19xm3");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)">
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default-normal.css?q19xm3");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 1220px)">
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default-wide.css?q19xm3");
</style>
<!--<![endif]-->

<!--[if gte IE 9]><!-->
<style type="text/css" media="all and (min-width: 1500px)">
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default.css?q19xm3");
@import url("https://www.visiteastside.co.uk/sites/all/themes/principal/css/principal-alpha-default-extra.css?q19xm3");
</style>
<!--<![endif]-->
  <script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js__Jx470Qgvc_5PkL9DzE9QINnwlDGj9zxHQ--CCDExQs.js"></script>
<script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_q5yGQspBQ_CdBUOFqOXgbCrvmOoB70iGIdjjY11av30.js"></script>
<script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_LcpX8NGXoxI4vMIuSj5dwy7i9-g4ceK3qmyBctbRfR0.js"></script>
<script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_6rtP2w_6eLv1n5yaYyU0sDK41pDBeLWi7RX3whycNe4.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-57681140-26", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_aJ8UZq7_NemZmQ_f87cnkvKzTt-BcUnPooQUT0si5GU.js"></script>
<script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_43n5FBy8pZxQHxPXkf-sQF7ZiacVZke14b0VlvSA554.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"principal","theme_token":"KTk3CCft3pPOp9fN10e7vPxhMkkhegnqzlGIsnLNRGI","jquery_version":"1.7","js":{"sites\/all\/modules\/views_infinite_scroll\/views-infinite-scroll.js":1,"sites\/all\/modules\/custom\/common\/js\/pager_custom.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/fitvids\/jquery.fitvids.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.effect.min.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/jquery_update\/replace\/misc\/jquery.form.min.js":1,"misc\/ajax.js":1,"sites\/all\/modules\/jquery_update\/js\/jquery_update.js":1,"sites\/all\/modules\/fitvids\/fitvids.js":1,"sites\/all\/modules\/back_to_top\/js\/back_to_top.js":1,"sites\/all\/modules\/extlink\/extlink.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/better_exposed_filters\/better_exposed_filters.js":1,"sites\/all\/modules\/ctools\/js\/auto-submit.js":1,"sites\/all\/modules\/views\/js\/base.js":1,"misc\/progress.js":1,"sites\/all\/modules\/views\/js\/ajax_view.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/all\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/extlink\/extlink.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/back_to_top\/css\/back_to_top.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/all\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/all\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/all\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/all\/themes\/principal\/css\/global.css":1,"ie::normal::sites\/all\/themes\/principal\/css\/principal-alpha-default.css":1,"ie::normal::sites\/all\/themes\/principal\/css\/principal-alpha-default-normal.css":1,"ie::normal::sites\/all\/themes\/principal\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/all\/themes\/principal\/css\/principal-alpha-default.css":1,"narrow::sites\/all\/themes\/principal\/css\/principal-alpha-default-narrow.css":1,"sites\/all\/themes\/principal\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/all\/themes\/principal\/css\/principal-alpha-default.css":1,"normal::sites\/all\/themes\/principal\/css\/principal-alpha-default-normal.css":1,"sites\/all\/themes\/principal\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/all\/themes\/principal\/css\/principal-alpha-default.css":1,"wide::sites\/all\/themes\/principal\/css\/principal-alpha-default-wide.css":1,"sites\/all\/themes\/principal\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1,"extra::sites\/all\/themes\/principal\/css\/principal-alpha-default.css":1,"extra::sites\/all\/themes\/principal\/css\/principal-alpha-default-extra.css":1,"sites\/all\/themes\/principal\/css\/grid\/alpha_default\/extra\/alpha-default-extra-12.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"better_exposed_filters":{"datepicker":false,"slider":false,"settings":[],"autosubmit":false,"views":{"home":{"displays":{"page":{"filters":{"term_node_tid_depth":{"required":false}}}}},"banner":{"displays":{"block":{"filters":[]}}},"icons_menu":{"displays":{"block":{"filters":[]}}}}},"urlIsAjaxTrusted":{"\/home":true,"\/views\/ajax":true},"views_infinite_scroll":{"img_path":"https:\/\/www.visiteastside.co.uk\/sites\/all\/modules\/views_infinite_scroll\/images\/ajax-loader.gif","scroll_threshold":200},"views":{"ajax_path":"\/views\/ajax","ajaxViews":{"views_dom_id:ba3d352aa9b2ab762fce7548fc505655":{"view_name":"home","view_display_id":"page","view_args":"","view_path":"home","view_base_path":"home","view_dom_id":"ba3d352aa9b2ab762fce7548fc505655","pager_element":0}}},"back_to_top":{"back_to_top_button_trigger":"100","back_to_top_button_text":"Back to top","#attached":{"library":[["system","ui"]]}},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"extlink":{"extTarget":"_blank","extClass":0,"extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":"pdf","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":0,"mailtoLabel":"(link sends e-mail)"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"superfish":{"1":{"id":"1","sf":{"animation":{"opacity":"show","height":"show"},"speed":"fast"},"plugins":{"supposition":true,"supersubs":true}}},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide","extra"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)","extra":"all and (min-width: 1500px)"}}}});
//--><!]]>
</script>
  <link rel="stylesheet" href="https://use.typekit.net/ybd5ijk.css">
</head>
<body class="html front not-logged-in page-home">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
  <div class="region region-page-top" id="region-page-top">
  <div class="region-inner region-page-top-inner">
      </div>
</div>  <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">  
  <div id="zone-menu" class="zone zone-menu clearfix container-12">
    <div class="grid-12 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
        <div class="block block-superfish block-1 block-superfish-1 odd block-without-title" id="block-superfish-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-4 sf-parent-items-0 sf-single-items-4"><li id="menu-1886-1" class="first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/node/51/#main-content" title="Meet up with a Local" class="sf-depth-1">Meet an EastSider</a></li><li id="menu-1620-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="/node/1/#main-content" title="About EastSide Tourism" class="sf-depth-1">About Us</a></li><li id="menu-218-1" class="active-trail middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/" title="Homepage of VisitEastSide.com" class="sf-depth-1 active">Home</a></li><li id="menu-2061-1" class="last even sf-item-4 sf-depth-1 sf-no-children"><a href="/shop" class="sf-depth-1">Shop</a></li></ul>    </div>
  </div>
</div>  </div>
</div>
  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-header-wrapper" class="zone-wrapper zone-header-wrapper clearfix">  
  <div id="zone-header" class="zone zone-header clearfix container-12">
    <div class="grid-8 region region-user-first" id="region-user-first">
  <div class="region-inner region-user-first-inner">
    <div class="block block-block block-14 block-block-14 odd block-without-title" id="block-block-14">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p style="text-align: right;"><a href="/meet-eastsider#main-content"><img alt="Meet an EastSider" src="/sites/default/files/MeetanEastSider.png" /></a></p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-12 region region-header-first" id="region-header-first">
  <div class="region-inner region-header-first-inner">
    <div class="block block-views block-banner-block block-views-banner-block odd block-without-title" id="block-views-banner-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-banner view-id-banner view-display-id-block view-dom-id-4c3c18ec24e910acb50a54990fc0cff6">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/banner/public/banners/cranes.jpg?itok=ksb7m-Pa" width="2400" height="900" alt="" /></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div>
</div>  </div>
</div><div class="grid-12 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="block block-block block-2 block-block-2 odd block-without-title" id="block-block-2">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="headersocial"><a href="https://www.facebook.com/EastSideVisitorCentre/"><img alt="Facebook" src="/sites/default/files/facebook.png" onmouseover="this.src='/sites/default/files/facebook_yellow.png'" onmouseout="this.src='/sites/default/files/facebook.png'" style="width: 60px; height: 61px;"></a><a href="https://twitter.com/EastSideVC"><img alt="Twitter" src="/sites/default/files/twitter.png" onmouseover="this.src='/sites/default/files/twitter_yellow.png'" onmouseout="this.src='/sites/default/files/twitter.png'" style="width: 60px; height: 61px;"></a></div>
<div class="headerlogo"><a href="/"><img alt="EastSide - Inspiring Belfast" src="/sites/default/files/eastside.png" style="width: 500px; height: 328px;"></a></div>    </div>
  </div>
</div>  </div>
</div><div class="grid-12 region region-header-second" id="region-header-second">
  <div class="region-inner region-header-second-inner">
    <div class="block block-views block-icons-menu-block block-views-icons-menu-block odd block-without-title" id="block-views-icons-menu-block">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-icons-menu view-id-icons_menu view-display-id-block view-dom-id-57b59f69a78dd42e480c78092daa8eae">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-icon">        <div class="field-content"><a href="https://www.visiteastside.co.uk/category/eat-drink"><img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/icon/public/icons/eatdrink.png?itok=YSKRrSVw" width="160" height="160" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="https://www.visiteastside.co.uk/category/eat-drink">Eat &amp; Drink</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-icon">        <div class="field-content"><a href="https://www.visiteastside.co.uk/category/see-do"><img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/icon/public/icons/seedo.png?itok=KyRV8x-S" width="160" height="160" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="https://www.visiteastside.co.uk/category/see-do">See &amp; Do</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-icon">        <div class="field-content"><a href="https://www.visiteastside.co.uk/category/stay"><img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/icon/public/icons/stay.png?itok=y6-aD5Fe" width="160" height="160" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="https://www.visiteastside.co.uk/category/stay">Stay</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <div class="views-field views-field-field-icon">        <div class="field-content"><a href="https://www.visiteastside.co.uk/events"><img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/icon/public/icons/events.png?itok=9WOHkGxK" width="160" height="160" alt="" /></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="https://www.visiteastside.co.uk/events">Events</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-preface-wrapper" class="zone-wrapper zone-preface-wrapper clearfix">  
  <div id="zone-preface" class="zone zone-preface clearfix container-12">
    <div class="grid-4 region region-preface-first" id="region-preface-first">
  <div class="region-inner region-preface-first-inner">
    <div class="block block-block block-10 block-block-10 odd block-without-title" id="block-block-10">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="inner-categories" id="inner-categories"><li><a href="/category/restaurant">Restaurant</a></li>
<li><a href="/category/cafe">Cafe</a></li>
<li><a href="/category/pubbar">Pub/Bar</a></li>
</ul>    </div>
  </div>
</div><div class="block block-block block-11 block-block-11 even block-without-title" id="block-block-11">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="inner-categories" id="inner-categories"><li><a href="/category/attraction">Attraction</a></li>
<li><a href="/category/tourtrail">Tour/Trail</a></li>
<li><a href="/category/outdoors">Outdoors</a></li>
</ul>    </div>
  </div>
</div><div class="block block-block block-12 block-block-12 odd block-without-title" id="block-block-12">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="inner-categories" id="inner-categories"><li><a href="/category/self-catering">Self-Catering</a></li>
<li><a href="/category/hotel">Hotel</a></li>
<li><a href="/category/guesthouse">Guesthouse</a></li>
</ul>    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-12">    
        
        <div class="grid-12 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                <h1 class="title" id="page-title">Home</h1>
                        <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="view view-home view-id-home view-display-id-page view-dom-id-ba3d352aa9b2ab762fce7548fc505655">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/lazy-claire-patisserie#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/33892847_441103332981218_2202960121359761408_o.jpg?itok=wxLbqbWX" width="1200" height="800" alt="" />
<div class="itemtitle"><span class="title">Lazy Claire Patisserie</span><br>
<span class="itemtype">Eat &amp; Drink, Cafe</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/robins-hobby-cafe#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/Robin%27s%20Web.jpg?itok=JBfS0U6G" width="1200" height="800" alt="Robins&#039; Hobby Cafe East Belfast" />
<div class="itemtitle"><span class="title">Robins&#039; Hobby Cafe</span><br>
<span class="itemtype">Eat &amp; Drink, Cafe</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/freight-belfast#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/freight.jpg?itok=eHY66uzG" width="1200" height="800" alt="" />
<div class="itemtitle"><span class="title">Freight Belfast</span><br>
<span class="itemtype">Eat &amp; Drink, Restaurant</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/jack-coffee-bar#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/JACK%20Web.jpg?itok=cNji7zyI" width="1200" height="800" alt="JACK Coffee Bar Belfast Cafe" />
<div class="itemtitle"><span class="title">JACK Coffee Bar</span><br>
<span class="itemtype">Eat &amp; Drink, Cafe</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/eastside-gallery#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/ESide%20Arts%20FunDay%2017-0880%20%281%29.jpg?itok=APQWMh43" width="1200" height="800" alt="" />
<div class="itemtitle"><span class="title">EastSide Gallery</span><br>
<span class="itemtype">See &amp; Do, Attraction</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/loyalist-conflict-museum#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/Andy%20Tyrie.jpg?itok=ov9z2iQQ" width="1200" height="800" alt="Andy Tyrie Interpretative Centre" />
<div class="itemtitle"><span class="title">Loyalist Conflict Museum </span><br>
<span class="itemtype">See &amp; Do, Attraction</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/refresh-cafe#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/Refresh%20Cafe.jpg?itok=t_hPRDIS" width="1200" height="800" alt="Refresh Cafe" />
<div class="itemtitle"><span class="title">Refresh Cafe</span><br>
<span class="itemtype">Eat &amp; Drink, Cafe</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-8 views-row-even">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/aunt-sandras-candy-factory#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/Sandras.jpg?itok=nANkJVKV" width="1200" height="800" alt="Aunt Sandra&#039;s" />
<div class="itemtitle"><span class="title">Aunt Sandra&#039;s Candy Factory</span><br>
<span class="itemtype">See &amp; Do, Attraction</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-9 views-row-odd">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/our-george-half-day-itinerary#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/GB%20HW%20297x210%20HS%20FB%20%283%20of%2011%29.jpg?itok=xXKBtRqx" width="1200" height="800" alt="George Best" />
<div class="itemtitle"><span class="title">Our George - Half Day Itinerary</span><br>
<span class="itemtype">See &amp; Do, Tour/Trail</span></div>
</a>
</div></div>  </div>  </div>
  <div class="views-row views-row-10 views-row-even views-row-last">
      
  <div class="views-field views-field-field-image">        <div class="field-content"><div class="tileditem">
<a href="/listing/artisann-gallery#contentanchor">
<img typeof="foaf:Image" src="https://www.visiteastside.co.uk/sites/default/files/styles/tiled/public/eat/artisann3.jpg?itok=VMiosBSr" width="1200" height="800" alt="ArtisAnn Gallery" />
<div class="itemtitle"><span class="title">ArtisAnn Gallery</span><br>
<span class="itemtype">See &amp; Do, Attraction</span></div>
</a>
</div></div>  </div>  </div>
    </div>
  
      <ul class="pager pager--infinite-scroll ">
  <li class="pager__item">
    <a href="/home?page=1">Show More</a>  </li>
</ul>
  
  
  
  
  
</div>    </div>
  </div>
</div>      </div>
</div>  </div>
</div><div id="zone-postscript-wrapper" class="zone-wrapper zone-postscript-wrapper clearfix">  
  <div id="zone-postscript" class="zone zone-postscript clearfix container-12">
    <div class="grid-12 region region-postscript-first" id="region-postscript-first">
  <div class="region-inner region-postscript-first-inner">
    <div class="block block-block block-4 block-block-4 odd block-without-title" id="block-block-4">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <h2 style="text-align: center;">Visit Our Other Sites</h2>
    </div>
  </div>
</div>  </div>
</div><div class="grid-4 region region-postscript-second" id="region-postscript-second">
  <div class="region-inner region-postscript-second-inner">
    <div class="block block-block block-7 block-block-7 odd block-without-title" id="block-block-7">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p style="text-align: center;"><a href="https://www.eastsidearts.net/"><img alt="EastSide Arts" src="/sites/default/files/eastsidearts_logo.png" style="width: 400px; height: 200px;" /></a></p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-4 region region-postscript-third" id="region-postscript-third">
  <div class="region-inner region-postscript-third-inner">
    <div class="block block-block block-5 block-block-5 odd block-without-title" id="block-block-5">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p style="text-align: center;"><a href="http://www.connswatergreenway.co.uk/"><img alt="Connswater Community Greenway" src="/sites/default/files/ccglogo.png" style="width: 400px; height: 200px;" /></a></p>
    </div>
  </div>
</div>  </div>
</div><div class="grid-4 region region-postscript-fourth" id="region-postscript-fourth">
  <div class="region-inner region-postscript-fourth-inner">
    <div class="block block-block block-6 block-block-6 odd block-without-title" id="block-block-6">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p style="text-align: center;"><a href="https://www.eastsidelearning.co.uk/"><img alt="EastSide Learning" src="/sites/default/files/Logo%202.jpg" style="width: 1200px; height: 680px;" /></a></p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-12 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <div class="block block-block block-1 block-block-1 odd block-without-title" id="block-block-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div style="float: left; width: 13%; text-align: center;"><a href="http://www.visitbelfast.com" target="_blank"><img alt="Belfast" src="/sites/default/files/Belfast_Yellow.png"></a></div>
<div style="float: left; width: 74%; text-align: center; padding-top: 25px;"><p>© 2018 <a href="https://www.eastsidepartnership.com">EastSide Partnership</a> | <a href="https://www.eastsidepartnership.com/eastside-partnership-privacy-cookies-policy">Privacy &amp; Cookies Policy</a> | Site developed by <a href="https://www.avecsolutions.net">Avec Solutions</a></p><p>EastSide Tourism, EastSide Visitor Centre, 402 Newtownards Road, Belfast, BT4 1HH</p><p><a href="mailto:visitorcentre@eastsidepartnership.com">visitorcentre@eastsidepartnership.com</a></p></div>
<div style="float: left; width: 13%; text-align: center;">&nbsp;</div>    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <script type="text/javascript" src="https://www.visiteastside.co.uk/sites/default/files/js/js_bTaO6WQf1bHEg0PrxOYiesa1IgItVUET2cdZXMcLev4.js"></script>
</body>
</html>