<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#">
  <head>
    <meta charset="utf-8" />
    <title>Webarchitects Shared Hosting | Webarchitects Shared Hosting</title>
    
    <meta name="description" content="Webarchitects shared web hosting service." />
    <meta name="keywords" content="web hosting, green hosting, secure hosting" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Webarchitects Shared Hosting" />
    <meta property="og:description" content="Webarchitects shared web hosting service." />
    <meta property="og:site_name" content="Webarchitects Shared Hosting" />
    <meta property="og:image" content="/wsh/webarch_500.png" />
    <meta property="og:locale" content="en_GB" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@webarchcoop" />
    <meta name="twitter:image" content="https://www.webarch1.co.uk/wsh/webarch_500.png" />
    <link rel="shortcut icon" href="/wsh/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="apple-touch-icon" sizes="80x80" href="/wsh/apple-touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/wsh/apple-touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="167x167" href="/wsh/apple-touch-icon-ipad-retina.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/wsh/apple-touch-icon-iphone-retina.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/pure-min.css" media="screen" />
    <!--[if lte IE 8]>
      <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/grids-responsive-old-ie-min.css" media="screen" />
    <![endif]-->
    <!--[if gt IE 8]><!-->
      <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/grids-responsive-min.css" media="screen" />
    <!--<![endif]-->
    <link type="text/css" rel="stylesheet" href="/wsh/webarch.css?d=2017-12-20" media="screen" />
    <link type="text/css" rel="stylesheet" href="/wsh/webarch-print.css" media="print" />
    <script>
      // https://github.com/pure-css/pure/blob/master/LICENSE.md
      /*
      @licstart  The following is the entire license notice for the JavaScript code in this page.
      
      Software License Agreement (BSD License)
      ========================================
      
      Copyright 2013 Yahoo! Inc.
      
      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions are met:
      
          * Redistributions of source code must retain the above copyright
            notice, this list of conditions and the following disclaimer.
      
          * Redistributions in binary form must reproduce the above copyright
            notice, this list of conditions and the following disclaimer in the
            documentation and/or other materials provided with the distribution.
      
          * Neither the name of the Yahoo! Inc. nor the
            names of its contributors may be used to endorse or promote products
            derived from this software without specific prior written permission.
      
      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL YAHOO! INC. BE LIABLE FOR ANY
      DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
      ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
      SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
      @licend  The above is the entire license notice for the JavaScript code in this page.
      */
    </script>
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPLv3 or later
      document.getElementById('wa-noscript-head').innerHTML = "";
      // @license-end
    </script>
    <link type="text/css" rel="stylesheet" href="/wsh/lib/highlight/9.14.2/styles/agate.css" media="all" />
    <script src="/wsh/lib/highlight/9.14.2/highlight.pack.js"></script>
    <script>
      // @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD 3-Clause License 
      hljs.initHighlightingOnLoad();
      // @license-end
    </script>
    <noscript id="wa-noscript-head">
      <style>
        @media (max-width:47.99em) { 
        .custom-wrapper { height: 12.2em; }
        div#menu div.pure-menu a#toggle { display: none; }
        }
      </style>
    </noscript>
  </head>
  <body> 
    <div class="pure-g"><div class="custom-wrapper" id="menu" itemscope itemtype="https://schema.org/WPHeader"><div class="pure-u-1 pure-u-md-1-3">
          <header>
            <div class="pure-menu">
              <a class="pure-menu-heading custom-brand" href="https://webarch.net/"><img class="wa-logo" itemprop="thumbnailUrl" src="/wsh/logo.png" alt="Webarchitects" title="Webarchitects" height="120" width="137" /><img class="wa-logo" src="/wsh/coop.png" alt="coop" title="Co-operative" height="120" width="209" /></a>
              <a href="#" class="custom-toggle" id="toggle"><span class="bar"></span><span class="bar"></span></a>
            </div>
          </header>
        </div><div class="pure-u-1 pure-u-md-2-3">
          <p class="wa-strapline">Sustainable, co-operative hosting you can depend on</p>
          <div class="pure-menu pure-menu-horizontal custom-can-transform">
            <nav itemscope itemtype="https://schema.org/SiteNavigationElement">
              <ul class="pure-menu-list">
                <li class="pure-menu-item wa-hosting"><a class="pure-menu-link" href="https://webarch.net/hosting" itemprop="url" title="Website &amp; email hosting, servers, virtual servers, colocation">Hosting</a></li>
                 <li class="pure-menu-item wa-support"><a class="pure-menu-link" href="https://webarch.net/support" itemprop="url" title="Webarchitects system adminstration &amp; development operations services.">Support</a></li>
                 <li class="pure-menu-item wa-about"><a class="pure-menu-link" href="https://webarch.net/about" itemprop="url" title="About Webarchitects co-op &amp; our ethics, principals &amp; organisational structure.">About</a></li>
                 <li class="pure-menu-item wa-help"><a class="pure-menu-link" href="https://webarch.net/help"  itemprop="url" title="Webarchitects help documentation.">Help</a></li>
              </ul>
            </nav>
          </div>
        </div><script>
          // http://purecss.io/layouts/tucked-menu-vertical/
          (function (window, document) {
            var menu = document.getElementById('menu'),
                WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange':'resize';
            function toggleHorizontal() {
              [].forEach.call(
                document.getElementById('menu').querySelectorAll('.custom-can-transform'),
                function(el){
                    el.classList.toggle('pure-menu-horizontal');
                }
              );
            };
            function toggleMenu() {
              // set timeout so that the panel has a chance to roll up
              // before the menu switches states
              if (menu.classList.contains('open')) {
                setTimeout(toggleHorizontal, 500);
              }
              else {
                toggleHorizontal();
              }
              menu.classList.toggle('open');
              document.getElementById('toggle').classList.toggle('x');
            };
            function closeMenu() {
              if (menu.classList.contains('open')) {
                toggleMenu();
              }
            }
            document.getElementById('toggle').addEventListener('click', function (e) {
              toggleMenu();
            });
            window.addEventListener(WINDOW_CHANGE_EVENT, closeMenu);
          })(this, this.document);
        </script></div>
    </div><div class="pure-g">
      <div class="pure-u-1">
        <div class="wa-content wa-title">
          <h1 id="title">Webarchitects Shared Hosting <a href="#title" class="a">#</a></h1>
        </div>
        <div class="wa-content wa-description" itemscope itemtype="https://schema.org/Thing">
          <p id="description" itemprop="description">Webarchitects shared web hosting service.</p>
        </div>
        <div itemscope itemtype="http://schema.org/WebPage">
          <div class="wa-content" itemprop="mainContentOfPage">


     
        <!-- PHPMYADMIN_PATH set -->
        <h2 id="www.webarch1.co.uk">www.webarch1.co.uk</h2>
        <p>Welcome to the www.webarch1.co.uk server, for information about the services available on this server please see the 
           <a href="https://www.webarchitects.coop/shared-hosting">Webarchitects Co-operative Shared Hosting</a> packages.</p> 
        <h2 id="mysql">MySQL Access</h2>
        
          <p>
            Login to <a href="https://webarch1.co.uk/phpmyadmin" rel="nofollow">phpMyAdmin</a> to change your password and access your database(s). 
          </p>
        
     

      <!--if expr='$PIWIK_SITE_DOMAIN' -->
      <h2 id="stats">Web Stats</h2>
      <p>
        Login to <a href="https://stats.webarch1.co.uk/">Piwik</a> to access your web stats.
      </p> 
      

      <h2 id="docs">Documentation</h2>
      <p>
        Please see the <a href="https://docs.webarch.net/wiki/Category:Shared_Hosting">shared hosting pages</a> 
        on the <a href="https://docs.webarch.net/wiki/Main_Page">Webarchitects wiki</a> for help. 
      </p>

      <p>
        If you have any problems please email:
        <a href="mailto:support@webarchitects.coop">support@webarchitects.coop</a>
      </p>

          </div></div>
        <div class="wa-content wa-updated">
            <p id="updated" itemscope itemtype="http://schema.org/CreativeWork">Last updated: <time itemprop="dateModified" datetime="2019-10-24T18:07">Thursday, 24 October 2019, 06:07:23 PM</time></p>
        </div>
      </div></div>
    <div class="pure-g wa-footer"><div class="pure-u-1 pure-u-md-1-4 wa-footer">
        <div class="wa-footer" id="footer">
          <nav>
            <div class="pure-menu">
              <ul class="pure-menu-list">
		<li class="pure-menu-item"><a class="pure-menu-link" href="https://www.blog.webarchitects.coop/">Blog</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://webarch.net/about">About</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://webarch.net/contact">Contact Us</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.info/">Service Status</a></li>
		<li class="pure-menu-item"><a class="pure-menu-link" href="https://members.webarchitects.coop/">Members Forum</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://webarch.net/sitemap">Sitemap</a></li>
              </ul>
            </div>
          </nav>
        </div>
      </div><div class="pure-u-1 pure-u-md-3-4 wa-footer">
          <div class="wa-footer" id="footer-address" itemscope itemtype="http://schema.org/Organization">
            <div class="pure-menu">
              <ul class="pure-menu-list">
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled"><span itemprop="name">Webarchitects</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled"><span itemprop="telephone">+44 114 276 9709</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="mailto:support@webarchitects.coop" itemprop="email">support@webarchitects.coop</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">68a John Street</span>, <span itemprop="addressRegion">Sheffield</span>, <span itemprop="postalCode">S2 4QU</span>, <span itemprop="addressCountry">UK</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled">Registration no. 31305R</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://webarch.net/privacy">Privacy Policy</a></li>
              </ul>
            </div>
          </div>
      </div><div class="pure-u-1 pure-u-md-5-6 wa-footer">
          <div class="wa-footer" id="footer-membership">
            <a href="https://webarch.coops.tech/" title="Co-operative Technologists Member"><img id="cotech" class="desaturate" src="/wsh/cotech_colour.png" alt="CoTech" width="100" height="100" /></a>
            <a href="http://www.radicalroutes.org.uk/" title="Radical Routes Associate Member"><img id="radicalroutes" class="desaturate" src="/wsh/rr_colour.png" alt="Radical Routes" width="185" height="100" /></a>
            <a href="https://www.openrightsgroup.org/about/sponsors" title="Open Rights Group Corporate Sponsor"><img id="org" class="desaturate" src="/wsh/org_colour.png" alt="Open Rights Group" width="100" height="100" /></a>
            <a href="https://www.uk.coop/directory/location/webarchitects-r006394" title="Co-operatives UK Member"><img id="coopuk" class="desaturate" src="/wsh/coopuk_colour.png" alt="Co-operatives UK" width="184" height="100" /></a>
            <a href="https://www.nominet.uk/" title="Nominet Registrar Member"><img id="nominet" class="desaturate" src="/wsh/nominet_colour.png" alt="Nominet" width="100" height="100" /></a>
            <a href="https://community.jisc.ac.uk/library/janet-services-documentation/registrar-membership" title="Jisc Registrar Member"><img id="jisc" class="desaturate" src="/wsh/jisc_colour.png" alt="Jisc" width="172" height="100" /></a>
          </div>
      </div><div class="pure-u-1 pure-u-md-1-6 wa-footer">
          <div class="wa-footer" id="footer-socialmedia">
            <a href="https://www.linkedin.com/company/webarchcoop/" title="Webarchitects on LinkedIn"><img id="linkedin" class="desaturate" src="/wsh/linkedin_colour.png" alt="LinkedIn" width="25" height="25" /></a>
            <a href="https://twitter.com/webarchcoop" title="Webarchitects on Twitter"><img id="twitter" class="desaturate" src="/wsh/twitter_colour.png" alt="Twitter" width="25" height="25" /></a>
            <a href="https://github.com/webarch-coop" title="Webarchitects on GitHub"><img id="github" class="desaturate" src="/wsh/github_colour.png" alt="GitHub" width="25" height="25" /></a>
            <a href="https://gitlab.com/webarch" title="Webarchitects on GitLab"><img id="gitlab" class="desaturate" src="/wsh/gitlab_colour.png" alt="GitLab" width="25" height="25" /></a>
          </div>
      </div><div class="pure-u-1 wa-footer">
          <div class="wa-footer" id="footer-copyright" itemscope itemtype="http://schema.org/Organization">
            <p>&copy; 2019 <span itemprop="legalName">Webarch Co-operative Limited</span></p> 
	  </div>
          <p id="wa-footer-javascript"><small><a class="pure-menu-link" href="https://webarch.net/js" rel="jslicense">JavaScript license information</a></small></p>
      </div>
    <script>
      // @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD-3-Clause
      var _paq = _paq || [];
      _paq.push(['disableCookies']);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//stats.webarch1.co.uk/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', 1]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
      // @license-end
    </script>
    </div>
  </body>
</html>



