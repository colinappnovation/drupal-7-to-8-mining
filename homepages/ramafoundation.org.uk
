<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"

  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

	<head profile="http://www.w3.org/1999/xhtml/vocab"><!--start head section-->
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="alternate" type="application/rss+xml" title="Rama Foundation RSS" href="https://www.ramafoundation.org.uk/rss.xml" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
	  <title>Rama Foundation | ...learning from each other, sharing skills and working together to make a difference</title>
	  <style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/reset/reset.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/modules/system/system.base.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/system/system.menus.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/system/system.messages.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/system/system.theme.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/modules/comment/comment.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/field/theme/field.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/search/search.css?prxff8");
@import url("https://www.ramafoundation.org.uk/modules/user/user.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/grid/grid_960.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/common.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/links.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/typography.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/forms.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/drupal.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/layout.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/primary-links.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/slideshow.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/secondary-links.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/blocks.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/node.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/comments.css?prxff8");
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/pages/maintenance-page.css?prxff8");
</style>
<style type="text/css" media="print">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/print/print.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/css3/css3.css?prxff8");
</style>
<style type="text/css" media="all">
@import url("https://www.ramafoundation.org.uk/sites/all/themes/marinelli/css/css3/css3_graphics.css?prxff8");
</style>
	  <script type="text/javascript" src="https://www.ramafoundation.org.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/misc/drupal.js?prxff8"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/sites/all/themes/marinelli/js/cycle/cycle.js?prxff8"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/sites/all/themes/marinelli/js/banner/marinelli_configure_cycle.js?prxff8"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/sites/all/themes/marinelli/js/modernizer/modernizr.js?prxff8"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/sites/all/themes/marinelli/js/marinelli_marinelli.js?prxff8"></script>
<script type="text/javascript" src="https://www.ramafoundation.org.uk/sites/all/themes/marinelli/js/topregion/marinelli_topregion.js?prxff8"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"marinelli","theme_token":"KPvYdeYXqzPVBFEvfYWpWqjAmCzL_ohwZ_LHEimolm0","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/marinelli\/js\/cycle\/cycle.js":1,"sites\/all\/themes\/marinelli\/js\/banner\/marinelli_configure_cycle.js":1,"sites\/all\/themes\/marinelli\/js\/modernizer\/modernizr.js":1,"sites\/all\/themes\/marinelli\/js\/marinelli_marinelli.js":1,"sites\/all\/themes\/marinelli\/js\/topregion\/marinelli_topregion.js":1},"css":{"sites\/all\/themes\/marinelli\/css\/reset\/reset.css":1,"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/themes\/marinelli\/css\/grid\/grid_960.css":1,"sites\/all\/themes\/marinelli\/css\/common.css":1,"sites\/all\/themes\/marinelli\/css\/links.css":1,"sites\/all\/themes\/marinelli\/css\/typography.css":1,"sites\/all\/themes\/marinelli\/css\/forms.css":1,"sites\/all\/themes\/marinelli\/css\/drupal.css":1,"sites\/all\/themes\/marinelli\/css\/layout.css":1,"sites\/all\/themes\/marinelli\/css\/primary-links.css":1,"sites\/all\/themes\/marinelli\/css\/slideshow.css":1,"sites\/all\/themes\/marinelli\/css\/secondary-links.css":1,"sites\/all\/themes\/marinelli\/css\/blocks.css":1,"sites\/all\/themes\/marinelli\/css\/node.css":1,"sites\/all\/themes\/marinelli\/css\/comments.css":1,"sites\/all\/themes\/marinelli\/css\/pages\/maintenance-page.css":1,"sites\/all\/themes\/marinelli\/css\/print\/print.css":1,"sites\/all\/themes\/marinelli\/css\/css3\/css3.css":1,"sites\/all\/themes\/marinelli\/css\/css3\/css3_graphics.css":1}},"marinelli":{"bartext":"Slide Down","bartext2":"Slide Up","banner_effect":"fade","banner_speed":"4000","banner_delay":"4000","banner_pause":1},"urlIsAjaxTrusted":{"\/":true,"\/node?destination=node":true}});
//--><!]]>
</script>
	</head>
	<!--[if lt IE 7 ]> <body class="marinelli ie6 html front not-logged-in one-sidebar sidebar-first page-node"> <![endif]-->
    <!--[if IE 7 ]>    <body class="marinelli ie7 html front not-logged-in one-sidebar sidebar-first page-node"> <![endif]-->
    <!--[if IE 8 ]>    <body class="marinelli ie8 html front not-logged-in one-sidebar sidebar-first page-node"> <![endif]-->
    <!--[if IE 9 ]>    <body class="marinelli ie9 html front not-logged-in one-sidebar sidebar-first page-node"> <![endif]-->
    <!--[if gt IE 9]>  <body class="marinelli html front not-logged-in one-sidebar sidebar-first page-node"> <![endif]-->
    <!--[if !IE]><!--> <body class="marinelli html front not-logged-in one-sidebar sidebar-first page-node"> <!--<![endif]-->
	  <div id="skip-link">
	    <a href="#content" title="Jump to the main content of this page" class="element-invisible">Jump to Content</a>
	  </div>
	  	  

<!--start framework container-->
<div class="container_12 width_2" id="totalContainer">
      <!--start top section-->
    <div id="top" class="outsidecontent">

              
      <!--start branding-->
      <div id="branding">

        
                  <!--start title and slogan-->
          <div id="title-slogan">
                          <h1 id="site-title"><a href="/" title="Back to homepage" class="active">Rama Foundation</a></h1>            
                          <h2 id="site-slogan"><a href="/" title="Back to homepage" class="active">...learning from each other, sharing skills and working together to make a difference</a></h2>                      </div>
          <!--end title and slogan-->
        
      </div>
      <!--end branding-->

      
    </div>
    <!--end top section-->
  
      <!--start main menu-->
    <div id="navigation-primary" class="sitemenu">
      <ul id="primary" class="links clearfix main-menu"><li class="menu-198 first active"><a href="/" class="active">Home</a></li>
<li class="menu-464"><a href="/about" title="About the Rama Foundation">About</a></li>
<li class="menu-1049"><a href="/node/23">News</a></li>
<li class="menu-742"><a href="/education">Education</a></li>
<li class="menu-743"><a href="/health">Health</a></li>
<li class="menu-741"><a href="/nutrition">Nutrition</a></li>
<li class="menu-466"><a href="/projects">Partner Charities</a></li>
<li class="menu-547"><a href="/policies">Policies</a></li>
<li class="menu-1623"><a href="/accounts">Accounts</a></li>
<li class="menu-1624"><a href="/reports">Reports</a></li>
<li class="menu-744 last"><a href="/node/20">Experiences</a></li>
</ul>    </div>
    <!--end main menu-->
  
  <!--border start-->
  <div id="pageBorder" >
          <!--start advertise section-->
      <div id="header-images" >
                  <div id="header-image-text" class="marinelli-hide-no-js"><div id="header-image-text-data"><h2 id="header-image-title"><a href="#" class="bannerlink" title="See this content">title</a></h2><p id="header-image-description"><a href="#" class="bannerlink" title="See this content">description</a></p></div></div>          <div id="header-image-navigation" class="marinelli-hide-no-js"><a href="#" id="header-image-prev" title="Previous Ad">&lsaquo;</a><a href="#" id="header-image-next" title="Next Ad">&rsaquo;</a></div>          <a href="/" class="active"><img class="slide" id="slide-number-0" longdesc="Children in Mother Miracle" typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/banner/DSC_0059.jpg" alt="Children in Mother Miracle" title="Children in Mother Miracle" /></a><a href="/" class="active"><img class="slide marinelli-hide-no-js" id="slide-number-1" longdesc="These boys were given an eye examination and bought spectacles by the Rama Foundation." typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/banner/GlassesNew.jpg" alt="These boys were given an eye examination and bought spectacles by the Rama Foundation." title="Contributing to well-being" /></a><a href="/" class="active"><img class="slide marinelli-hide-no-js" id="slide-number-2" longdesc="The new learning environment, and a training session for local teachers by experienced volunteer teachers from the UK" typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/banner/LearningBanner2.jpg" alt="The new learning environment, and a training session for local teachers by experienced volunteer teachers from the UK" title="Sharing good practice" /></a><a href="/" class="active"><img class="slide marinelli-hide-no-js" id="slide-number-3" longdesc="A classroom at Divya Prem Sewa mission before and after improvements by the Rama Foundation." typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/banner/Classrooms.jpeg" alt="A classroom at Divya Prem Sewa mission before and after improvements by the Rama Foundation." title="Improving the learning environment" /></a><a href="/" class="active"><img class="slide marinelli-hide-no-js" id="slide-number-4" longdesc="Children in Rishikesh" typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/banner/DSC_0091.jpg" alt="Children in Rishikesh" title="Children in Rishikesh" /></a>              </div>
      <!--end advertise-->
    		
		
    <!-- start contentWrapper-->
    <div id="contentWrapper">
      <!--start breadcrumb -->
            <!-- end breadcrumb -->
		
			
      <!--start innercontent-->
			<div id="innerContent">

        <!--start main content-->
				<div class="grid_8" id="siteContent">
						   				
	   					           	
		      
          
                    
                      <div class="tab-container">
                          </div>
          
          
          
          <!--start drupal content-->
          <div id="content">
            <!-- start region -->
<div class="region region region-content">
  <div id="block-system-main" class="block block-system">
        <div class="content">
    
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/36" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/36">Written in retrospect </a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					May				</div>
				<div class="teaser-created-day">
					29				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/tanzil-blog.jpg?itok=SydIlsrt"><a href="/node/36"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/tanzil-blog.jpg?itok=SydIlsrt" width="220" height="165" alt="Tanzil with children" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Tanzil Miah, Specialist Dietitian</p>
<p>Above all else I wanted for my experience of volunteering with Rama Foundation to be a rewarding one not just for those we were supporting, but for myself too.  I promised myself that I would maintain a positive mind-set from the offset - as cliché as it may sound I felt this was the driving force behind our accomplishments. </p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/36" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/35" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/35">Something I will remember forever</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					May				</div>
				<div class="teaser-created-day">
					20				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image001.jpg?itok=7Um0Bzia"><a href="/node/35"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image001.jpg?itok=7Um0Bzia" width="220" height="165" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Claire Holgate, Specialist Dietitian</p>
<p>It is difficult to put into words my reflection on our trip to India, it is something I will remember forever;</p>
<p>We spent the first few days of our trip as tourists, getting to know India. We wanted to immerse ourselves in India’s rich culture, colour, food and most importantly acclimatise ourselves to the weather! I feel this was such an important part of our journey as it introduced us to the wonderful way of life, kindness and optimism of the Indian people.</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/35" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/34" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/34">A restless dissatisfaction!</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					May				</div>
				<div class="teaser-created-day">
					08				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/artika-blog.jpg?itok=3SUeVxoY"><a href="/node/34"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/artika-blog.jpg?itok=3SUeVxoY" width="220" height="165" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Artika Datta, Advanced Specialist Dietitian</p>
<p>I have returned from India after spending a week in Rishikesh volunteering in Ganga Prem Hospice and Government School number 8 with restless dissatisfaction. I am pleased with what I have achieved with the help of my colleagues Claire, Helaina and Tanzil and my supportive family but impatient that there is much more we can still to do.</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/34" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/33" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/33">Reflections on my volunteering in India with Rama Foundation in April 2019</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					May				</div>
				<div class="teaser-created-day">
					02				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/Helaina-GS8.png?itok=PQ-tBf2j"><a href="/node/33"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/Helaina-GS8.png?itok=PQ-tBf2j" width="220" height="165" alt="Helaina (far right) distributing books and pencils to children from disadvantaged backgrounds attending government school number 8. These materials were kindly donated by her family and friends in the UK. " /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Helaina Jackson, Specialist Dietitian, Cambridge University Hopsitals NHS Foundation Trust </p>
<p>It has been two weeks since I returned from India, yet only now am I beginning to process and reflect on what we achieved and the work that still lies ahead. Looking back is almost surreal. It was a challenging, fun, life-changing and hugely eye-opening adventure. In some ways, I am frustrated and want to return and continue the important work that we started.</p>
</div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/33" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/32" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/32">Artika Datta&#039;s blog, India April 2019</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					Apr				</div>
				<div class="teaser-created-day">
					01				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_2.png?itok=m10-6gqL"><a href="/node/32"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_2.png?itok=m10-6gqL" width="165" height="220" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Artika Datta, Advanced Specialist Dietitian</p>
<p>A lifelong passion for learning and the desire to enhance my knowledge have helped me to remain self- motivated. After completing BSc. Honours in Nutrition and Msc. in Dietetics from Kings College London, I was very fortunate to get my first job as a dietitian in Addenbrooke’s Hospital.</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/32" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/31" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/31">Claire Holgate&#039;s blog, India April 2019</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					Apr				</div>
				<div class="teaser-created-day">
					01				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_1.png?itok=pZeHfy8g"><a href="/node/31"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_1.png?itok=pZeHfy8g" width="220" height="218" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Claire Holgate, Specialist Dietitian</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/31" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/30" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/30">Helaina Jackson&#039;s blog, India April 2019</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					Mar				</div>
				<div class="teaser-created-day">
					31				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_0.png?itok=d8S0Blb0"><a href="/node/30"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1_0.png?itok=d8S0Blb0" width="123" height="220" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Helaina Jackson, Specialist Dietitian<br />
I have been working at Addenbrookes Hospital in Cambridge for over 3.5 years, initially as a newly-qualified dietitian before deciding to specialist in head and neck cancer.<br /></p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/30" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/node/29" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/node/29">Tanzil Miah&#039;s blog, India April 2019</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					Mar				</div>
				<div class="teaser-created-day">
					31				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1.png?itok=Np0VPBmk"><a href="/node/29"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/image1.png?itok=Np0VPBmk" width="181" height="220" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>By Tanzil Miah, Specialist Dietitian<br />
At school I was never the kid who knew that they wanted to be a Doctor, a lawyer or a dancer when they grew up, so after finishing my A-Levels I took a year out to explore my options. </p>
<p>I spent a lot of time researching Dietetics, before shadowing an Oncology Dietitian at Addenbrooke’s Hospital - I knew instantly that it was the career for me!  I completed my undergraduate training at Coventry University and have worked as a Dietitian since, moving around the country for various job roles.</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/node/29" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-article node-promoted node-teaser" about="/joe2018" typeof="sioc:Item foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/joe2018">My time at Divya Prem, by Joe Cottrell</a>				</h2>
					<div class="teaser-created">
				<div class="teaser-created-month">
					Dec				</div>
				<div class="teaser-created-day">
					16				</div>			
			</div>
					<div class="teaser-text">
			<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/DP.jpg?itok=OGGKts-L"><a href="/joe2018"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/sites/default/files/styles/medium/public/field/image/DP.jpg?itok=OGGKts-L" width="220" height="102" alt="" /></a></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>When I arrived at the gates of Divya Prem Sewa Mission (after a gruelling taxi journey in which I’d gotten lost after giving the driver the wrong address), I had no idea what to expect. The chance to come to this amazing place had been presented to me rather serendipitously by a friend of mine who happened to live next door to two people connected with it. I agreed to volunteer my services as an English teacher for a brief two weeks. Nothing could’ve prepared me for the depth and beauty of the experience I was to have.</p></div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <strong class="teaser-author">Posted By</strong> <span rel="sioc:has_creator"><span class="username" xml:lang="" about="/user/1" typeof="sioc:UserAccount" property="foaf:name" datatype="">admin</span></span>      <span class="teaser-readmore"><a href="/joe2018" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->
<div class="teaser-container node node-page node-promoted node-teaser" about="/about" typeof="foaf:Document">
	<div class="teaser-content">
		<h2 class="teaser-title">
		<a href="/about">About the Rama Foundation</a>				</h2>
					<div class="teaser-text">
			<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>The object of the Rama Foundation is to develop the capacities and the skills of members of socially and / or economically disadvantaged communities in all parts of the world (and particularly in India) in such a way as they are better able to identify, and help meet their needs and to participate more fully in society. </p>
</div></div></div>      		</div>
	</div>
  <div class="teaser-meta">
      <span class="teaser-readmore"><a href="/about" class="node-readmore-link">read more</a></span>
  </div>
</div><!--end teaser container-->  </div>
</div> <!-- /block --></div>
<!-- end region -->          </div>
          <!--end drupal content-->

          <a href="/rss.xml" class="feed-icon" title="Subscribe to Rama Foundation RSS"><img typeof="foaf:Image" src="https://www.ramafoundation.org.uk/misc/feed.png" width="16" height="16" alt="Subscribe to Rama Foundation RSS" /></a>
          
        </div>
        <!--end main content-->
	 			
          		    		
		              <!--start first sidebar-->
          <div class="grid_4 sidebar" id="sidebar-first">
						<!-- start region -->
<div class="region region region-sidebar-first">
  <div id="block-search-form" class="block block-search">
        <div class="content">
    <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-9ihOgWceA6EAXLO_4uav_Lwe4-K4Uw2JGJbPDnCSpvs" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>

</div></form>  </div>
</div> <!-- /block --><div id="block-user-login" class="block block-user">
        <div class="block-title">
      <h2 class="title">User login</h2>
    </div>
      <div class="content">
    <form action="/node?destination=node" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list"><ul><li class="first last"><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li>
</ul></div><input type="hidden" name="form_build_id" value="form-6n9wH8QJ0ha6WDLzhPaaxw5jkurhXru8WkiaSBCyW-k" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Log in" class="form-submit" /></div></div></form>  </div>
</div> <!-- /block --></div>
<!-- end region -->	
          </div>
          <!--end first sidebar-->
        		    
        						
						
				
      
      </div>
      <!--end innerContent-->


          </div>
    <!--end contentWrapper-->

	</div>
  <!--close page border Wrapper-->

      <!--start footer-->
    <div id="footer" class="outsidecontent">
      <!-- start region -->
<div class="region region region-footer">
  <div id="block-system-powered-by" class="blockhide block block-system">
        <div class="content">
    <span>Powered by <a href="https://www.drupal.org">Drupal</a></span>  </div>
</div> <!-- /block --></div>
<!-- end region -->
          </div>
    <!--end footer-->
  
</div>
<!--end framework container-->
	  	</body><!--end body-->
</html>