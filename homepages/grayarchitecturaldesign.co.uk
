<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="shortcut icon" href="https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Gray Architectural Design is a Northants based architect’s practice specialising in new build houses and residential &amp; commercial building refurbishments. If you are considering an architect for a potential project then do get in touch. We would be pleased to discuss your thoughts and requirements further." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="http://www.grayarchitecturaldesign.co.uk/" />
<link rel="shortlink" href="http://goo.gl/C3zaJG" />
<meta property="og:site_name" content="Gray Architectural Design" />
<meta property="og:type" content="company" />
<meta property="og:url" content="http://www.grayarchitecturaldesign.co.uk" />
<meta property="og:title" content="Gray Architectural Design" />
<meta property="og:description" content="Gray Architectural Design is a Northants based architect’s practice specialising in new build houses and residential &amp; commercial building refurbishments. If you are considering an architect for a potential project then do get in touch. We would be pleased to discuss your thoughts and requirements further." />
<meta property="og:updated_time" content="2014-05-01T10:16:44+01:00" />
<meta property="og:image" content="http://www.grayarchitecturaldesign.co.uk/gad.jpg" />
<meta property="article:published_time" content="2014-04-11T16:25:57+01:00" />
<meta property="article:modified_time" content="2014-05-01T10:16:44+01:00" />
<title>Gray Architectural Design | Northants Architects & London Architecture Designs</title>
<style type="text/css" media="all">
@import url("https://grayarchitecturaldesign.co.uk/modules/system/system.base.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/system/system.menus.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/system/system.messages.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/system/system.theme.css?pbywu7");
</style>
<style type="text/css" media="all">
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/views_slideshow/views_slideshow.css?pbywu7");
</style>
<style type="text/css" media="all">
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/adaptive_image/css/adaptive-image.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/field/theme/field.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/node/node.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/modules/user/user.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/views/css/views.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/ckeditor/css/ckeditor.css?pbywu7");
</style>
<style type="text/css" media="all">
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/ctools/css/ctools.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/modules/flexslider/assets/css/flexslider_img.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/libraries/flexslider/flexslider.css?pbywu7");
</style>
<style type="text/css" media="all">
@import url("https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/css/responsive-style.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/css/sidetogglemenu.css?pbywu7");
@import url("https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/style.css?pbywu7");
</style>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
document.cookie = 'adaptive_image=' + Math.max(screen.width, screen.height) + '; path=/';
//--><!]]>
</script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/misc/jquery.js?v=1.4.4"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/misc/drupal.js?pbywu7"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/modules/views_slideshow/js/views_slideshow.js?v=1.0"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/js/sidetogglemenu.js?pbywu7"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/js/mobile.js?pbywu7"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/libraries/flexslider/jquery.flexslider-min.js?pbywu7"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/modules/flexslider_views_slideshow/js/flexslider_views_slideshow.js?pbywu7"></script>
<script type="text/javascript" src="https://grayarchitecturaldesign.co.uk/sites/all/modules/google_analytics/googleanalytics.js?pbywu7"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-49986017-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"gad","theme_token":"voHL-qQz-fXeOHYhoKFunkbwcYJoiexGKDaMex0wtVY","js":{"0":1,"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/themes\/gad\/js\/sidetogglemenu.js":1,"sites\/all\/themes\/gad\/js\/mobile.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/modules\/flexslider_views_slideshow\/js\/flexslider_views_slideshow.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/adaptive_image\/css\/adaptive-image.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/themes\/gad\/css\/responsive-style.css":1,"sites\/all\/themes\/gad\/css\/sidetogglemenu.css":1,"sites\/all\/themes\/gad\/style.css":1}},"viewsSlideshow":{"front_slideshow-block_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","flexsliderViewsSlideshow","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","flexsliderViewsSlideshow","viewsSlideshowCycle"],"play":["viewsSlideshowControls","flexsliderViewsSlideshow","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","flexsliderViewsSlideshow","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"flexslider_views_slideshow":{"#flexslider_views_slideshow_main_front_slideshow-block_1":{"num_divs":4,"id_prefix":"#flexslider_views_slideshow_main_","vss_id":"front_slideshow-block_1","namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":false,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":7000,"directionNav":true,"controlNav":true,"prevText":"Previous","nextText":"Next","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script>
jQuery(document).ready(function(){
    jQuery("div#togglemenu1").hide().fadeIn(300);
});
</script>
<style>
img {display:none;}
.gmap img {display:block}
</style>
<script>
(function ($) { 
$(document).ready(function() {  
    $('img').load(function() {  
        $(this).fadeIn('slow');  
    })
.each(function() {
    if(this.complete) {
        jQuery(this).load();
    }
});
});
})(jQuery);
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-1 node-type-front-page">
    <div id="togglemenu1" class="sidetogglemenu">
        <ul class="menu"><li class="first leaf"><a href="/" title="" class="active">Home</a></li>
<li class="leaf"><a href="/projects">Projects</a></li>
<li class="last leaf"><a href="/contact">Contact</a></li>
</ul></div>
<div class="mastercontainer">
<div class="topbar">
	<header id="head" role="banner">
		<div class="five columns alpha">
		   <div id="logo">
			<a href="/" title="Home"><img src="https://grayarchitecturaldesign.co.uk/sites/all/themes/gad/logo.png" alt="Home"/></a>		   </div>
		</div>
		<div class="eleven columns omega" id="headright">
			<div class="toprightcontact">
				<a class="topphone" href="tel:07751216805">07751 216 805</a><br>
				<a class="topemail" href="mailto:info@grayarchitecturaldesign.co.uk">info@grayarchitecturaldesign.co.uk</a>
			</div>
		<nav id="navigation" role="navigation">
			<div id="main-menu">
					<ul class="menu"><li class="first leaf"><a href="/" title="" class="active">Home</a></li>
<li class="leaf"><a href="/projects">Projects</a></li>
<li class="last leaf"><a href="/contact">Contact</a></li>
</ul>			</div>
		</nav><!-- end main-menu -->
		  <a href="#" onClick="menu1.toggle();" tabindex="1" class="sideviewtoggle" id="mobilemenuicon">≡</a>
		 </div>
	</header>
</div>
	
<div class="container" id="content-contain">
 

    
  
 <div id="content" class="eleven columns">
  <div id="breadcrumbs"></div>
   <section id="post-content" role="main">
            <h1 class="page-title">Welcome</h1>                <div class="region region-content">
  <div id="block-system-main" class="block block-system">

      
  <div class="content">
                            
      
    
  <div class="content node-front-page">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even">Gray Architectural Design is a Northants based architect’s practice specialising in new build houses and residential & commercial building refurbishments. If you are considering an architect for a potential project then do get in touch. We would be pleased to discuss your thoughts and requirements further.

We use our architectural design skills to combine beautiful spaces, materials and natural light to create inspirational homes tailored to your requirements. We aim to maximise the potential of your property with a creative approach to design, and we recognise the importance of fine detailing and craftsmanship in the creation of inspiring architecture.</div></div></div>  </div>

      <footer>
          </footer>
  
    </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
  </section> <!-- /#main -->
 </div>

      <aside id="sidebar-first" role="complementary" class="sidebar five columns">
      <div class="region region-sidebar-first">
  <div id="block-views-front-slideshow-block" class="block block-views">

      
  <div class="content">
    <div class="view view-front-slideshow view-id-front_slideshow view-display-id-block view-dom-id-87c40ca068510cf6bb551bc3e80621d5">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
    <div id="flexslider_views_slideshow_main_front_slideshow-block_1" class="flexslider_views_slideshow_main views_slideshow_main"><div class="flex-nav-container">
  <div class="flexslider">
    <ul id="flexslider_views_slideshow_front_slideshow-block_1" class="flexslider-views-slideshow-main-frame slides">
        <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-1 views-row-odd">
      
  <div class="views-field views-field-field-slideshow-images">        <div class="field-content"><img class="adaptive-image" src="https://grayarchitecturaldesign.co.uk/sites/default/files/styles/slideshow/adaptive-image/public/GAD3.jpg?itok=Zq8JbIzj" alt="" /></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even">
      
  <div class="views-field views-field-field-slideshow-images">        <div class="field-content"><img class="adaptive-image" src="https://grayarchitecturaldesign.co.uk/sites/default/files/styles/slideshow/adaptive-image/public/GAD4.jpg?itok=H8VQkdC6" alt="" /></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd">
      
  <div class="views-field views-field-field-slideshow-images">        <div class="field-content"><img class="adaptive-image" src="https://grayarchitecturaldesign.co.uk/sites/default/files/styles/slideshow/adaptive-image/public/GAD1.jpg?itok=NzJ1mav5" alt="" /></div>  </div>  </li>
  <li class="flexslider-views-slideshow-main-frame-row flexslider_views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even">
      
  <div class="views-field views-field-field-slideshow-images">        <div class="field-content"><img class="adaptive-image" src="https://grayarchitecturaldesign.co.uk/sites/default/files/styles/slideshow/adaptive-image/public/GAD2.jpg?itok=Vr00LnFe" alt="" /></div>  </div>  </li>
    </ul>
  </div>
</div></div>
      </div>
    </div>
  
  
  
  
  
  
</div>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
    </aside>  <!-- /#sidebar-first -->
  
  <div class="clear"></div>
  
    
</div>
 
  
<div id="copyright" class="container">
 <div class="credit">Copyright &copy; 2019, Gray Architectural Design <br/> Site by <a href="http://www.webfroth.co.uk" target="_blank">Webfroth</a></div>
  <div class="clear"></div>
</div>
</div>  </body>
</html>