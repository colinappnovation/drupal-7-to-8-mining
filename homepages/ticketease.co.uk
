<!DOCTYPE html>
<html lang="en">
<head>
<!--[if IE]><![endif]-->
<meta charset="utf-8" />
<link rel="shortcut icon" href="https://ticketease.co.uk/sites/all/themes/ticketease_2018/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="We are an online ticketing solution built to change the UK event industry by providing a fairer ticketing solution for event organisers and event goers." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://ticketease.co.uk/sell-tickets-online-for-any-event-in-any-location" />
<link rel="shortlink" href="https://ticketease.co.uk/node/29" />
<meta property="og:site_name" content="TicketEase" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Sell Tickets to your Event for less with TicketEase" />
<meta property="og:url" content="https://ticketease.co.uk/sell-tickets-online-for-any-event-in-any-location" />
<meta property="og:description" content="We are an online ticketing solution built to change the UK event industry by providing a fairer ticketing solution for event organisers and event goers." />
<meta property="og:updated_time" content="2018-10-25T11:13:09+01:00" />
<meta property="og:image" content="https://ticketease.co.uk/sites/all/themes/ticketease_2018/img/confetti.png" />
<meta property="og:image:url" content="https://ticketease.co.uk/sites/all/themes/ticketease_2018/img/confetti.png" />
<meta property="og:image:secure_url" content="https://ticketease.co.uk/sites/all/themes/ticketease_2018/img/confetti.png" />
<meta property="og:image:type" content="image/png" />
<meta property="article:published_time" content="2018-01-23T14:46:20+00:00" />
<meta property="article:modified_time" content="2018-10-25T11:13:09+01:00" />
<title>Sell Tickets to your Event for less with TicketEase</title>
<style type="text/css" media="all">
@import url("/modules/system/system.base.css?q0idbk");
@import url("/modules/system/system.menus.css?q0idbk");
@import url("/modules/system/system.messages.css?q0idbk");
@import url("/modules/system/system.theme.css?q0idbk");
</style>
<style type="text/css" media="all">
@import url("/sites/all/modules/date/date_api/date.css?q0idbk");
@import url("/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?q0idbk");
@import url("/modules/field/theme/field.css?q0idbk");
@import url("/modules/node/node.css?q0idbk");
@import url("/modules/user/user.css?q0idbk");
@import url("/sites/all/modules/views/css/views.css?q0idbk");
@import url("/sites/all/modules/ckeditor/css/ckeditor.css?q0idbk");
</style>
<style type="text/css" media="all">
@import url("/sites/all/modules/ctools/css/ctools.css?q0idbk");
</style>
<style type="text/css" media="all">
@import url("/sites/all/themes/ticketease_2018/css/jquery.mmenu.all.css?q0idbk");
@import url("/sites/all/themes/ticketease_2018/css/flexslider.css?q0idbk");
@import url("/sites/all/themes/ticketease_2018/style.css?q0idbk");
@import url("/sites/all/themes/ticketease_2018/css/responsive.css?q0idbk");
@import url("/sites/all/themes/ticketease_2018/css/animate.css?q0idbk");
</style>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<script type="text/javascript" src="/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script type="text/javascript" src="/misc/jquery.once.js?v=1.2"></script>
<script type="text/javascript" src="/misc/drupal.js?q0idbk"></script>
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/jquery.mmenu.all.min.js?q0idbk"></script>
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/jquery.flexslider-min.js?q0idbk"></script>
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/slide.js?q0idbk"></script>
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/mobile.js?q0idbk"></script>
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/custom.js?q0idbk"></script>
<script type="text/javascript" src="https://js.stripe.com/v3"></script>
<script type="text/javascript" src="/sites/all/modules/google_analytics/googleanalytics.js?q0idbk"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-47442781-5", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"ticketease_2018","theme_token":"pDDqeKgYTQOuHLIeJ-Sm42Jt6xRhGsaXQUon-qlrXxE","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/ticketease_2018\/css\/jquery.mmenu.all.css":1,"sites\/all\/themes\/ticketease_2018\/css\/flexslider.css":1,"sites\/all\/themes\/ticketease_2018\/style.css":1,"sites\/all\/themes\/ticketease_2018\/css\/responsive.css":1,"sites\/all\/themes\/ticketease_2018\/css\/animate.css":1},"js":{"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.10.2\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/themes\/ticketease_2018\/js\/jquery.mmenu.all.min.js":1,"sites\/all\/themes\/ticketease_2018\/js\/jquery.flexslider-min.js":1,"sites\/all\/themes\/ticketease_2018\/js\/slide.js":1,"sites\/all\/themes\/ticketease_2018\/js\/mobile.js":1,"sites\/all\/themes\/ticketease_2018\/js\/custom.js":1,"https:\/\/js.stripe.com\/v3":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1}},"better_exposed_filters":{"views":{"page_headers":{"displays":{"block_2":{"filters":[]}}}}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"}});
//--><!]]>
</script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="/sites/all/themes/ticketease_2018/js/simple-lightbox.min.js"></script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "address": {
    "@type": "PostalAddress",
    "addressLocality": "Totnes",
    "addressRegion": "Devon",
    "postalCode":"TQ9 5DB",
    "streetAddress": "The Cider Warehouse, Bridge Court"
  },
  "description": "TicketEase is a free online ticket retailer that is fairer and cheaper for event organisers and event goers. Built by event organisers for event organisers.",
  "name": "TicketEase",
  "logo": "https://ticketease.co.uk/sites/all/themes/ticketease_2018/logo-square.jpg",
  "foundingDate": "2015",
   "url": "https://www.ticketease.co.uk",
  "sameAs" : [ "https://www.facebook.com/ticketease",
  "https://www.twitter.com/ticketease",
  "https://plus.google.com/ticketease","https://www.instagram.com/ticketease","https://www.linkedin.com/ticketease"]
}
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "https://www.ticketease.co.uk",
  "name": "TicketEase"
}
</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-29 node-type-basic-page p36 noscript">
  
  
<nav id="mm-menu">
<div>
		<ul class="menu"><li class="first leaf"><a href="/user" title="">Dashboard</a></li>
<li class="expanded"><a href="/user" title="">Selling</a><ul class="menu"><li class="first leaf"><a href="/user/login" title="">Sign Up</a></li>
<li class="leaf"><a href="/how-much-does-it-cost-to-sell-tickets-online">Prices</a></li>
<li class="leaf"><a href="/ticketease-pricing">Pricing Comparison</a></li>
<li class="last leaf"><a href="https://ticketease.freshdesk.com/support/home" title="">Support</a></li>
</ul></li>
<li class="expanded"><a href="/all-events" title="">Buying</a><ul class="menu"><li class="first leaf"><a href="/user#block-views-commerce-orders-block-1" title="">My Tickets</a></li>
<li class="leaf"><a href="/all-events">All Events</a></li>
<li class="last leaf"><a href="/support-categories/attending-an-event" title="">Support</a></li>
</ul></li>
<li class="expanded"><a href="/about-ticketease" title="">About TicketEase</a><ul class="menu"><li class="first leaf"><a href="/about-ticketease" title="">The TicketEase Story</a></li>
<li class="leaf"><a href="/the-ticketease-blog" title="">TicketEase Blog</a></li>
<li class="last leaf"><a href="/ticketease-features" title="">TicketEase Features</a></li>
</ul></li>
<li class="last leaf"><a href="/user/login" title="">Log In</a></li>
</ul>		</div>
</nav>

<div id="topbar">
	<div class="container">
			</div>
</div>

<div id="menubar">
	<div class="menu-container">
		<div id="logo">
		  <a href="/" title="Home"><img src="https://ticketease.co.uk/sites/all/themes/ticketease_2018/logo.png?2" alt="Home"/></a>		</div>
		<a href="#mm-menu" tabindex="1" class="sideviewtoggle" id="mobilemenuicon">≡</a>
				

		<nav id="navigation">
		  <div id="main-menu">
			<ul class="menu"><li class="first leaf"><a href="/user" title="">Dashboard</a></li>
<li class="expanded"><a href="/user" title="">Selling</a><ul class="menu"><li class="first leaf"><a href="/user/login" title="">Sign Up</a></li>
<li class="leaf"><a href="/how-much-does-it-cost-to-sell-tickets-online">Prices</a></li>
<li class="leaf"><a href="/ticketease-pricing">Pricing Comparison</a></li>
<li class="last leaf"><a href="https://ticketease.freshdesk.com/support/home" title="">Support</a></li>
</ul></li>
<li class="expanded"><a href="/all-events" title="">Buying</a><ul class="menu"><li class="first leaf"><a href="/user#block-views-commerce-orders-block-1" title="">My Tickets</a></li>
<li class="leaf"><a href="/all-events">All Events</a></li>
<li class="last leaf"><a href="/support-categories/attending-an-event" title="">Support</a></li>
</ul></li>
<li class="expanded"><a href="/about-ticketease" title="">About TicketEase</a><ul class="menu"><li class="first leaf"><a href="/about-ticketease" title="">The TicketEase Story</a></li>
<li class="leaf"><a href="/the-ticketease-blog" title="">TicketEase Blog</a></li>
<li class="last leaf"><a href="/ticketease-features" title="">TicketEase Features</a></li>
</ul></li>
<li class="last leaf"><a href="/user/login" title="">Log In</a></li>
</ul>		  </div>
		</nav>

	</div>
</div>







<div id="headerone" class="row">
	<img src="/sites/all/themes/ticketease_2018/img/confetti.png" alt="" class="video-alt"/>
	<div class="arrow bounce">
		<a class="smoothscroll" href="#features"><img src="/sites/all/themes/ticketease_2018/img/down-arrow.png" alt="scroll down"/></a>
	</div>
	<div class="hero-content">
		<div class="hero-text">
			<h1>Sell tickets to your event for less with TicketEase</h1>
			<p class="header-content">We are an online ticketing solution built to change the UK event industry by providing a fairer ticketing solution for event organisers and event goers.</p>
			<p><a href="/user/login">Start Selling</a>&nbsp;&nbsp;<a href="/all-events">Buy Tickets</a></p>
		</div>
	</div>
</div>

<div class="mastercontainer" id="features">

	<div class="full-width calculator">
		<div class="content-contain">
	    <h2>Keep more of your booking fees</h2>
	    <br>

	    <p>If you sold 1000 tickets at £30 each with a £3 booking fee, this is how much extra you could make compared to other ticketing solutions available.</p>
	    <div class="clearfix"></div>
		<div class="cont" data-pct="68" data-amnt="2,020">
	<svg id="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	  <circle id="bar" class="green" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	</svg>
	  <p>TicketEase</p>
	</div>
	<div class="cont" data-pct="35" data-amnt="1,040">
	<svg id="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	  <circle id="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	</svg>
	    <p>Eventbrite</p>
	</div>
	<div class="cont" data-pct="9" data-amnt="-250">
	<svg id="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	  <circle class="red" id="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	</svg>  <p>Skiddle</p>
	</div>
	<div class="cont" data-pct="0" data-amnt="0">
	<svg id="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	  <circle id="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	</svg>  <p>Fatsoma</p>
	</div>
	<div class="cont" data-pct="0" data-amnt="0">
	<svg id="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
	  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	  <circle id="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
	</svg>  <p>WeGotTickets</p>
	</div>
		</div>
	<div class="clearfix"></div>
	</div>
	<h3 class="page-title animated-type"> <span class="content-contain"> TicketEase gives you all the features you need<br/>to host your <span class="typewrite" data-period="2000" data-type='[ "conference", "workshop", "fundraiser", "festival", "art exhibition", "lecture", "triathlon" ]'>
	<span class="wrap"></span>
	</span></span></h3>
<div class="" id="content-contain">

 
 <div id="content" class="without-sidebar">
   <section id="post-content">
   <div class="without-inner-sidebar">
    	                <div class="region region-content">
  <div id="block-system-main" class="block block-system">

      
  <div class="content">
                            
      
    
  <div class="content node-basic-page">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><div class="features-intro">
<!-- <h3 class="features-heading">TicketEase gives you all the features you need to host a successful ticketed event</h3> -->
</div>


<div class="all-features">
<div class="feature">
<div><img alt="Design from TicketEase" class="adaptive-image" src="/sites/default/files/design_0.png" /></div>

<div class="feature-title">Branding and Design</div>

<div class="feature-description">
<p>Showcase your company's branding with your own brand page and display of all of your upcoming events.</p>
</div>
</div>

<div class="feature">
<div><img alt="Admin Dashboard with TicketEase" class="adaptive-image" src="/sites/default/files/admin_0.png" /></div>

<div class="feature-title">User Friendly Dashboard</div>

<div class="feature-description">
<p>Create unlimited events, set your own booking fee, amount of stock and multiple ticket prices per event.</p>
</div>
</div>

<div class="feature">
<div><img alt="Earn with TicketEase" class="adaptive-image" src="/sites/default/files/money_0.png" /></div>

<div class="feature-title">Earn as Tickets Sell</div>

<div class="feature-description">
<p>No need to wait for the event to finish, receive payments directly from customers as your tickets sell.</p>
</div>
</div>

<div class="feature">
<div><img alt="TicketEase Event Management" class="adaptive-image" src="/sites/default/files/list_0.png" /></div>

<div class="feature-title">Event Management</div>

<div class="feature-description">
<p>Automatically send e-tickets, view your guest lists and easily process refunds.</p>
</div>
</div>

<div class="feature">
<div><img alt="TicketEase App" class="adaptive-image" src="/sites/default/files/appicon1.png" /></div>

<div class="feature-title">Access to the TicketEase App</div>

<div class="feature-description">
<p>Scan QR codes on tickets using the TicketEase App, you will be alerted to any duplicate tickets.</p>
</div>
</div>

</div>
</div></div></div>  </div>

      <footer>
          </footer>
  
    </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
	</div>
  </section> <!-- /#main -->
 </div>

  <div class="clear"></div>


</div>

<div class="full-width types-of-events">
	<div class="content-contain">
<h2>All kinds of tickets,<br/>for all kinds of events</h2>
<p>From music festivals and art exhibitions, to marathons and charity events, with <b>TicketEase</b> you can confidently sell tickets to any kind of event. <br/> Not only are we the <b>cheapest online ticketing solution</b> but we actually help you make more money from each event. By offering low cost fees, TicketEase allows you to keep more of your booking fee increasing profits per ticket. </p>
<a href="/user/login" class="btn">Create an event</a>
</div>

</div>

<div class="full-width signupnow">
	<div class="content-contain">
<h2>Create your event today</h2>
<p>Sign up to create your event and start selling tickets now.</p>
<a href="/user/login" class="btn">Start Selling</a>
</div>
</div>

</div> <!-- End Master Container -->


  <div id="bottom">
	<div  class="container">
	  <div class="bottom-menus">
	  				<div class="bottom-four"><div class="region region-footer-first">
  <div id="block-menu-menu-footer-one" class="block block-menu">

        <h2 >Planning An Event?</h2>
    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/how-much-does-it-cost-to-sell-tickets-online" title="">Prices</a></li>
<li class="leaf"><a href="/ticketease-pricing" title="">Pricing Comparison</a></li>
<li class="leaf"><a href="/user" title="">My Account</a></li>
<li class="leaf"><a href="/support" title="">Support</a></li>
<li class="last leaf"><a href="/sell-tickets-on-squarespace-with-ticketease" title="">Selling tickets on SquareSpace</a></li>
</ul>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
						<div class="bottom-four"><div class="region region-footer-second">
  <div id="block-menu-menu-footer-two" class="block block-menu">

        <h2 >Going To An Event?</h2>
    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/all-events" title="">All Events</a></li>
<li class="leaf"><a href="/all-events" title="">Buy Tickets</a></li>
<li class="leaf"><a href="/user#block-views-commerce-orders-block-1" title="">My Tickets</a></li>
<li class="last leaf"><a href="/support-categories/attending-an-event" title="">Support</a></li>
</ul>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
						<div class="bottom-four"><div class="region region-footer-third">
  <div id="block-menu-menu-footer-three" class="block block-menu">

        <h2 >More Information</h2>
    
  <div class="content">
    <ul class="menu"><li class="first leaf"><a href="/eventbrite-alternative-ticketease-vs-eventbrite">TicketEase vs Eventbrite</a></li>
<li class="leaf"><a href="/fatsoma-alternative-ticketease-vs-fatsoma" title="">TicketEase vs Fatsoma</a></li>
<li class="leaf"><a href="/skiddle-alternative-ticketease-vs-skiddle" title="">TicketEase vs Skiddle</a></li>
<li class="leaf"><a href="/wegottickets-alternative-ticketease-vs-wegottickets">TicketEase vs WeGotTickets</a></li>
<li class="leaf"><a href="/terms-conditions">Terms &amp; Conditions</a></li>
<li class="last leaf"><a href="/privacy">Privacy Policy</a></li>
</ul>  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
						<div class="bottom-four"><div class="region region-footer-fourth">
  <div id="block-block-1" class="block block-block">

        <h2 >About Us</h2>
    
  <div class="content">
    <p><a class="facebook" rel="noopener" href="https://www.facebook.com/ticketease" target="_blank">Facebook</a> <a class="twitter rel="noopener"" href="https://twitter.com/ticketease" target="_blank">Twitter</a> <a class="googleplus" rel="noopener" href="https://plus.google.com/114416455440437841706" target="_blank">Googleplus</a> <a  rel="noopener" class="instagram" href="https://www.instagram.com/ticketease" target="_blank">Instagram</a> <a rel="noopener" class="linkedin" href="https://www.linkedin.com/company/ticketease" target="_blank">Linkedin</a></p>
  </div>
  
</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
			  </div>
	</div>
	<div id="copyright">
	  <div class="container">
	   <div class="credit">
				<div class="copyright-content">Copyright &copy; 2019, TicketEase</div>
	   </div>
      </div>
	</div>
  </div>
<script src="/sites/all/themes/ticketease_2018/js/home-typewriter.js"></script>

  
<script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
	FreshWidget.init("", {"queryString": "&widgetType=popup&submitTitle=Submit&submitThanks=Thank+you+for+your+message.+Our+offices+are+open+9am+-+5pm+Monday+to+Friday%2C+and+we+will+respond+to+your+query+as+soon+as+possible.", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Help & Support", "buttonColor": "black", "buttonBg": "#f2c927", "alignment": "2", "offset": "235px", "submitThanks": "Thank you for your message. Our offices are open 9am - 5pm Monday to Friday, and we will respond to your query as soon as possible.", "formHeight": "500px", "url": "https://ticketease.freshdesk.com"} );
</script>
</body>
</html>
