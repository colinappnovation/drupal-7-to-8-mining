<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.xlvets.co.uk/sites/default/themes/xlvetstrap/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="XLVets is a community of independently-owned, progressive veterinary practices that work together to achieve the highest standards of veterinary care. Members of XLVets work collaboratively sharing knowledge, experience, and skills, to achieve excellence in veterinary practice that ensures they deliver a high level of customer care and animal welfare." />
<meta name="abstract" content="Founded in the UK in 2005, XLVets originated from a group of dynamic predominantly farm animal biased veterinary practices, all of whom worked hard to create what they saw as a model of how individual practices can work successfully in partnership" />
<meta name="keywords" content="Small animal, Pets, Animal health, Independent practices, Excellent care, Cardiology, Heart disease, Dermatology, Small Talk magazine, Dental care, Educational Factsheets, How to videos, Lifetime Care Club, Preventative health care, Diabetes, Hyperthyroidism, Cushing&#039;s disease, Client booklets, Professional service" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.xlvets.co.uk/" />
<link rel="shortlink" href="https://www.xlvets.co.uk/" />
  <title>XLVets | By working together we can achieve so much more | www.xlvets.co.uk</title>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
  <link type="text/css" rel="stylesheet" href="https://www.xlvets.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.xlvets.co.uk/sites/default/files/css/css_cm_ZW5cC7MM09BHOdt255FtKrIH8cXrSYTO3XYDjLt4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.xlvets.co.uk/sites/default/files/css/css_Q64p1s4D5P4-iUzDTDE64onQU2p5E2UDmz2dLSvoPAI.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.xlvets.co.uk/sites/default/files/css/css_bdY7rOwn8tgyiIst6TDPb4XwRQ7SvRvhV4hdTJQe3zs.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.xlvets.co.uk/sites/default/files/css/css_tyMN0gpEABL8Hgg8ieIqboCu5vmhdNLgcuwF6M_Ul9U.css" media="print" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script src="https://www.xlvets.co.uk/sites/default/files/js/js_7gJarvouvmZwJzj-DyqVKSMAkUSuujbGOYoOydMy2P8.js"></script>
<script src="//code.jquery.com/jquery-1.8.3.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/1.8/jquery.min.js'>\x3C/script>")</script>
<script src="https://www.xlvets.co.uk/sites/default/files/js/js_dWhBODswdXXk1M5Z5nyqNfGljmqwxUwAK9i6D0YSDNs.js"></script>
<script src="https://www.xlvets.co.uk/sites/default/files/js/js_5CiuInwOmYenQAeXt5jJwJxBNgxSOrKpGUZIEjg7QDo.js"></script>
<script src="https://www.xlvets.co.uk/sites/default/files/js/js_wHpNqRG75XqI7D4CFsImMcSWMRuGv6_gcwrVQ6mbPHs.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-15996949-1", {"cookieDomain":"auto"});ga("set", "dimension1", "9db4634a-3acd-42af-b2d7-b637bef7c401");ga("set", "dimension2", "1");ga("set", "metric1", 9);ga("set", "metric2", 1);ga("send", "pageview");</script>
<script src="https://www.xlvets.co.uk/sites/default/files/js/js_BZAK7M4C5pzv85r3ouex4DU14kDJuMacmQG-xA3X-yc.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"xlvetstrap","theme_token":"59sv56yqz16EBbm_sGOmVk4hJsU_NQ8X-wlFRE7TJhw","js":{"sites\/default\/themes\/bootstrap\/js\/bootstrap.js":1,"public:\/\/google_tag\/google_tag.script.js":1,"\/\/code.jquery.com\/jquery-1.8.3.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/bef_bootstrap_fix\/includes\/bef_bootstrap_fix.js":1,"sites\/all\/modules\/xlvets_api\/bvd\/js\/bvd.js":1,"sites\/all\/modules\/xlvets\/xlvets_add_blocked_user\/js\/xlvets_add_blocked_user.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/modules\/quote\/quote.js":1,"sites\/default\/modules\/custom\/xlvets_mailinglist\/js\/xlvets_mailinglist.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/affix.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/alert.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/button.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/carousel.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/collapse.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/dropdown.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/modal.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/tooltip.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/popover.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/scrollspy.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/tab.js":1,"sites\/default\/themes\/xlvetstrap\/js\/bootstrap\/transition.js":1,"sites\/default\/themes\/xlvetstrap\/js\/misc\/_vertical-tabs.js":1,"sites\/default\/themes\/xlvetstrap\/js\/misc\/_xlvets.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/xlvets\/xlvets_add_blocked_user\/css\/styles.css":1,"sites\/all\/modules\/nodeorder\/css\/nodeorder.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/libraries\/colorbox\/example2\/colorbox.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/quote\/quote.css":1,"sites\/default\/modules\/custom\/xlvets_mailinglist\/css\/xlvets_mailinglist.css":1,"sites\/default\/themes\/xlvetstrap\/css\/style.css":1,"sites\/default\/themes\/xlvetstrap\/css\/print.css":1}},"colorbox":{"transition":"fade","speed":"350","opacity":"0.85","slideshow":false,"slideshowAuto":true,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","overlayClose":true,"returnFocus":true,"maxWidth":"98%","maxHeight":"98%","initialWidth":"300","initialHeight":"250","fixed":true,"scrolling":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"quote_nest":"2","better_exposed_filters":{"views":{"header_image":{"displays":{"block_3":{"filters":[]},"block_5":{"filters":[]}}},"accordian_blocks":{"displays":{"block":{"filters":[]}}}}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"urlIsAjaxTrusted":{"\/welcome-to-xlvets?destination=node\/43395":true},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-43395 node-type-page" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="region region-page-top">
    <noscript aria-hidden="true"><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KB8WW9T" height="0" width="0" style="display:none;visibility:hidden" title="Google Tag Manager">Google Tag Manager</iframe></noscript>  </div>
  
	<nav class="navbar navbar-default bottom-shadow">
		<div class="container pre-header">
			<div class="navbar-header">
				<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#pre-header-navbar">
					<span class="sr-only">Toggle navigation</span>
					<div class="button-bars">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</div>
					<span class="menu-text">Main menu</span>
				</button>

				<a class="logo xlvets-logo navbar-brand" href="/" title="Home"></a>
								  <a class="name sr-only navbar-brand" href="/" title="Home">XLVets</a>
							</div>

			<div class="collapse navbar-collapse" id="pre-header-navbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/" alt="Home" title="Home">Home</a></li>
					<li class="who-we-are dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Who we are <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/who-we-are" alt="Who we are" title="Who we are">Who we are</a></li>
							<li><a href="/member-practices/list" alt="Member practices" title="Member practices">Member practices</a></li>
							<li><a href="/meet-the-team" alt="Meet the team" title="Meet the team">Meet the team</a></li>
						</ul>
					</li>
					<li><a href="http://www.xlvets-farm.co.uk" alt="XLVets Farm" title="XLVets Farm">Farm</a></li>
					<li><a href="http://www.xlvets-pets.co.uk" alt="XLVets Pets" title="XLVets Pets">Pets</a></li>
					<li><a href="http://www.xlvets-equine.co.uk" alt="XLVets Equine" title="XLVets Equine">Equine</a></li>
					<li><a href="/vacancies" alt="Vacancies" title="Vacancies">Vacancies</a></li>
					<li><a href="/contact-us" alt="Contact" title="Contact">Contact</a></li>

											<li class="hidden-sm hidden-md hidden-lg"><a href="/user" alt="Member login" title="Member login">Member login</a></li>
						<li class="hidden-xs member-login dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Member login <span class="caret"></span></a>
							<ul class="dropdown-menu">
								  <div class="region region-login">
    <section id="block-user-login" class="block block-user clearfix">

      
  <form action="/welcome-to-xlvets?destination=node/43395" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-item-name form-type-textfield form-group"> <label class="control-label" for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
<input class="form-control form-text required" type="text" id="edit-name" name="name" value="" size="15" maxlength="60" /></div><div class="form-item form-item-pass form-type-password form-group"> <label class="control-label" for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
<input class="form-control form-text required" type="password" id="edit-pass" name="pass" size="15" maxlength="128" /></div><input type="hidden" name="form_build_id" value="form-LUZoN4E7dPCG3rlNkg3ycsP0IP4s9yHpIOWzeiUbTAA" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper form-group" id="edit-actions"><button type="submit" id="edit-submit" name="op" value="Log in" class="btn btn-primary form-submit icon-before"><span class="icon glyphicon glyphicon-log-in" aria-hidden="true"></span>
 Log in</button>
</div><ul><li><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li>
</ul></div></form>
</section> <!-- /.block -->
  </div>
							</ul>
						</li>
					
					
				</ul>
			</div>
		</div>
	</nav>

		
	
	
	<div class="main-container container">

	  <header role="banner" id="page-header">

			  </header> <!-- /#page-header -->

	  <div class="row">

		

		<section class="col-sm-12">
		  		  		  <a id="main-content"></a>
		  		  		  	<div class="page-header">
		  		<h1>Welcome to XLVets</h1>
			</div>
		  		  
		  		  					  		  		  		    <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div id="node-43395" class="node node-page clearfix" about="/welcome-to-xlvets" typeof="foaf:Document">

  
      <span property="dc:title" content="Welcome to XLVets" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
  
  <div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p class="h3">By working together we can achieve so much more</p>

<div class="front-boxes">

	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="small-animal box-border colour-box">
				<a href="https://www.xlvets-pets.co.uk"></a>
				<h2><a href="https://www.xlvets-pets.co.uk">Small Animal</a></h2>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="farm box-border colour-box">
				<a href="https://www.xlvets-farm.co.uk"></a>
				<h2><a href="https://www.xlvets-farm.co.uk">Farm</a></h2>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="equine box-border colour-box">
				<a href="https://www.xlvets-equine.co.uk"></a>
				<h2><a href="https://www.xlvets-equine.co.uk">Equine</a></h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="students box-border">
				
					<a href="/students"></a>
				
				<h2><a href="/students">Students</a></h2>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="working-with-us box-border">

					<a href="/working-together"></a>

				<h2><a href="/working-together">Working together</a></h2>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="becoming-a-member box-border">

					<a href="/the-boss"></a>

				<h2><a href="/the-boss">Join our community</a></h2>
			</div>
		</div>
	</div>
	
</div></div></div></div>  </div>

  
  
</div>

</section> <!-- /.block -->
  </div>
		</section>

		
	  </div>
	</div>

	
<div class="footer-wrapper">
	  <footer class="footer secondary-footer container-fluid dark-background">
	  <div class="container">
				<div class="row first-footer">
			<div class="col-xs-12 col-sm-6 col-md-3 right-vertical-line phone">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<p><strong>01228 711788</strong></p>
				<p>Mon - Fri (9am - 5pm)</p>

			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 right-vertical-line email">
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
				<p><strong>Email us at:</strong></p>
				<p><a href="mailto:admin@xlvets.co.uk">admin@xlvets.co.uk</a></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 right-vertical-line nearest-member">
				<i class="fa fa-map-o" aria-hidden="true"></i>
				<p><strong>Find a member practice</strong></p>
				<p><a href="http://www.xlvets.co.uk/member-practices/list">Locate your nearest member</a></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 social-media">
				<a href="https://www.facebook.com/XLVetsConnect" title="Facebook" alt="Facebook" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
				<a href="https://twitter.com/xlvets" title="Twitter" alt="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href="https://www.linkedin.com/company/xlvets" title="Linkedin" alt="Linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				<p><strong>Let's get social</strong></p>
				<p>Follow us</p>
			</div>
		</div>
	  </div>
	  </footer>

	<footer class="footer primary-footer container-fluid light-background">
	  <div class="container">
				<div class="row">
			<div class="col-xs-12 col-sm-9 col-lg-10">
				<p class="small"><a href="/content/terms-use">Terms of Use</a> | <a href="/content/privacy-policy">Privacy Policy</a></p>
				<p class="copyright small">This website and its content is copyright of XLVets - &copy; XLVets 2019.</p>
				<p class="small">Registered in Scotland: SC278724</p>
			</div>
			<div class="col-xs-12 col-sm-3 col-lg-2">
				<select name="xlvets-international" id="xlvets-international" class="pull-right form-control">
					<option value="">-- Change location --</option>
					<option value="http://www.xlvets.ie">Ireland</option>
					<option value="http://www.xlvets.co.nz">New Zealand</option>
					<option value="http://www.xlvets.co.uk">United Kingdom</option>
				</select>
			</div>
		</div>
	  </div>
	</footer>
</div>
	<script type="text/javascript">
		(function($){

		  $(document).ready(function(){
			$("#xlvets-international").change(function(){
			  var ext = $(this).find(':selected').val();
			  if (ext) {
				window.location.href = ext;
			  }
			});

		  });
		})(jQuery);
	</script>  <script src="https://www.xlvets.co.uk/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
