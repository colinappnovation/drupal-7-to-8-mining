<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="en" dir="ltr">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.carbonfreegroup.com/sites/all/themes/cfg/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="shortlink" href="/node/1" />
<link rel="canonical" href="/node/1" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
  <title>Connecting Innovation To Support A Movement For Change | Carbon Free Group</title>
  <style>
@import url("https://www.carbonfreegroup.com/modules/system/system.base.css?pwzhmj");
</style>
<style>
@import url("https://www.carbonfreegroup.com/sites/all/modules/date/date_api/date.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/sites/all/modules/date/date_repeat_field/date_repeat_field.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/modules/field/theme/field.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/modules/node/node.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/sites/all/modules/views/css/views.css?pwzhmj");
</style>
<style>
@import url("https://www.carbonfreegroup.com/sites/all/modules/ctools/css/ctools.css?pwzhmj");
</style>
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@unicorn-fail/drupal-bootstrap-styles@0.0.2/dist/3.3.1/7.x-3.x/drupal-bootstrap.css" media="all" />
<style>
@import url("https://www.carbonfreegroup.com/sites/all/themes/cfg/css/style.css?pwzhmj");
@import url("https://www.carbonfreegroup.com/sites/all/themes/cfg/css/lightbox.css?pwzhmj");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <script src="https://www.carbonfreegroup.com/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2"></script>
<script src="https://www.carbonfreegroup.com/misc/jquery-extend-3.4.0.js?v=1.10.2"></script>
<script src="https://www.carbonfreegroup.com/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.carbonfreegroup.com/misc/drupal.js?pwzhmj"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.js"></script>
<script src="https://www.carbonfreegroup.com/sites/all/modules/admin_menu/admin_devel/admin_devel.js?pwzhmj"></script>
<script src="https://www.carbonfreegroup.com/sites/all/themes/cfg/js/cfg.js?pwzhmj"></script>
<script src="https://www.carbonfreegroup.com/sites/all/themes/cfg/js/lightbox.js?pwzhmj"></script>
<script src="https://www.carbonfreegroup.com/sites/all/themes/cfg/js/slick.min.js?pwzhmj"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"cfg","theme_token":"MHXjywXh18mYxRY-SH-6WwnsmATotTCk6gNT8fk1d3c","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.4.1\/dist\/js\/bootstrap.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/themes\/cfg\/js\/cfg.js":1,"sites\/all\/themes\/cfg\/js\/lightbox.js":1,"sites\/all\/themes\/cfg\/js\/slick.min.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/bootstrap@3.4.1\/dist\/css\/bootstrap.css":1,"https:\/\/cdn.jsdelivr.net\/npm\/@unicorn-fail\/drupal-bootstrap-styles@0.0.2\/dist\/3.3.1\/7.x-3.x\/drupal-bootstrap.css":1,"sites\/all\/themes\/cfg\/css\/style.css":1,"sites\/all\/themes\/cfg\/css\/style.cssbootstrap5ths.css":1,"sites\/all\/themes\/cfg\/css\/lightbox.css":1}},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":"1","popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":"1","tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>

<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:400,700" rel="stylesheet" type='text/css'>
<link href="https://fonts.googleapis.com/css?family=DM+Serif+Text&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/041f56c86f.js" crossorigin="anonymous"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7734237-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
<body class="html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-1 node-type-page" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
<div id="lh">
    <a id="logo" href="/">Carbon Free Group</a>
</div>

<div id="topbars">
    <header id="navbar" role="banner" class="booomla navbar container navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

                    <div class="navbar-collapse collapse">
                <nav role="navigation" id="miut">
                                <ul class="menu nav navbar-nav"><li class="first expanded dropdown"><a href="/about" title="" class="dropdown-toggle" data-toggle="dropdown">About <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/cic">Our Community Interest Company</a></li>
<li class="leaf"><a href="/ecosystem">Company Background</a></li>
<li class="leaf"><a href="/history" title="">History</a></li>
<li class="leaf"><a href="/values" title="">Values</a></li>
<li class="last leaf"><a href="/people" title="">People</a></li>
</ul></li>
<li class="leaf"><a href="/clients" title="">Clients &amp; Partners</a></li>
<li class="expanded dropdown"><a href="/engage" title="" class="dropdown-toggle" data-toggle="dropdown">Work &amp; Services <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/projects">Our Work</a></li>
<li class="leaf"><a href="/services">Services</a></li>
<li class="last leaf"><a href="/member-testimonials" title="">Testimonials</a></li>
</ul></li>
<li class="last leaf"><a href="/contact">Contact</a></li>
</ul>                
                                </nav>
            </div>
                </div>
    </header>
</div>



	<div class="fpvid container-fluid">
		<div class="fpvoverlay"></div>
		<video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
			<source src="/sites/all/themes/cfg/images/pinesfly.mp4" type="video/mp4">
		</video>
		<div class="container fpvtexty">
			<div class="">
				<p>The Carbon Free Group is a Community Interest Company (CIC) focused on decarbonising the built environment by supporting innovative solutions into mainstream markets.</p>
				<p>As a group we oversee the acceleration of zero carbon and circular solutions in the following three areas:</p>
				<ul>
					<li><a href="#advisoryservices"><i class="fas fa-pencil-ruler"></i> &nbsp;Advisory Services</a>
					<li><a href="#energymicrogrid"><i class="fas fa-solar-panel"></i> &nbsp;Energy Microgrid Management</a>
					<li><a href="#zerocarbondevelopments"><i class="far fa-calendar-times"></i> &nbsp;Zero Carbon Developments</a></ul>

			</div>
		</div>
	</div>







<div id="herologo">
    <div id="tqi">
        <div id="topquote" class="noshadow">
            <a id="main-content"></a>
                        <h1 class="page-header h1s">Connecting Innovation To Support A Movement For Change</h1>
        </div>
    </div>
</div>










                                    
                          <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even">















	<div class="goodlife almostblack">
		<div class="container">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="animated" style="opacity: 1; display: block;">Our Services</h2>
				</div>
			</div>

			<div class="whychoose">

				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/management-support.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Management Support</h3>
				</a>


				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/grant.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Grant Applications</h3>
				</a>


				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/research.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Research and Development</h3>
				</a>


			</div>
			<div class="whychoose">

				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/introductions.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Game Changing Introductions</h3>
				</a>


				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/seed.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Investment Support</h3>
				</a>


				<a class="cloudytwo biggerbadderbetterdark" href="/services">
					<img src="/sites/all/themes/cfg/images/servicesfront/solar.jpg" alt="" class="img-responsive img-fluid" />
					<h3 class="schmancyh3">Start-Up Inception</h3>
				</a>





			</div>
		</div>
	</div>








	<div class="goodlife">
		<div class="container">
			<div class="row">

				<div class="col-sm-12">
					<h2 class="animated" style="opacity: 1; display: block;">Our Organisations</h2>
				</div>
			</div>

			<div class="whychoose">

				<a class="cloudyone biggerbadderbetter" href="http://wearecalyx.com/" id="advisoryservices">
			        <img src="/sites/all/themes/cfg/images/organisations/pinescalyx.jpg" alt="" class="img-responsive img-fluid" />
			        <h3 class="schmancyh3">Calyx</h3>
			        <p>Our advisory services arm, Calyx accelerates the development and delivery of the next generation of buildings</p>
				</a>


				<a class="cloudyone biggerbadderbetter" href="http://ptvolts.com/" id="energymicrogrid">
			        <img src="/sites/all/themes/cfg/images/organisations/powertransition.jpg" alt="" class="img-responsive img-fluid" />
			        <h3 class="schmancyh3">Power Transition</h3>
			        <p>Power Transition is a revolutionary Microgrid Management Platform designed to transform how energy is optimised, used and tracked</p>
				</a>


				<a class="cloudyone biggerbadderbetter" href="http://resilienthomes.co.uk/" id="zerocarbondevelopments">
			        <img src="/sites/all/themes/cfg/images/organisations/resilienthomes.jpg" alt="" class="img-responsive img-fluid" />
			        <h3 class="schmancyh3">Resilient Homes</h3>
			        <p>Resilient Homes builds small scale zero carbon developments demonstrating innovative products and technology</p>
				</a>
			</div>
		</div>
	</div>





































	<div class="sectory goodlife">
		<div class="container">
			<div class="row">
				<h2 class="lighth2">Find Out More</h2>
			</div>
			<div class="row fms">

					<a class="foricons energysectoricon" data-toggle="modal" data-target="#modalenergysector">Energy Sector</a>
					<a class="foricons developericon" data-toggle="modal" data-target="#modaldeveloper">Developer</a>
					<a class="foricons technologydevelopericon" data-toggle="modal" data-target="#modaltechnologydeveloper">Technology Developer</a>
					<a class="foricons architecticon" data-toggle="modal" data-target="#modalarchitect">Architect</a>
					<a class="foricons localauthorityicon" data-toggle="modal" data-target="#modallocalauthority">Local Authority</a>



					<!-- Modal Energy Sector -->
					<div class="modal modal-formodal" id="modalenergysector" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<div>
									<h2>Energy Sector</h2>
									<p>Are you concerned about the future of the National Grid and the impact of blackouts?</p><p>

										Are you looking for smart digital solutions that optimise and accelerate the transition to renewables?</p><p>

										Are you looking to track and account for energy in real time?</p><p>

										Are you generating energy that your network operator cannot use due to grid capacity issues?</p><p>

										Should you be achieving greater savings from generation through to storage and use? Are you losing revenue?</p><p>

										Do you require software that provides ultimate security, ultra-low carbon and unprecedented transaction speeds?</p><p>

										<a href="http://ptvolts.com">Power Transition</a> is a game-changing platform to help solve the future challenges of the energy sector.</p>
									</div>
								</div>
								<div class="modal-footer">
								<button type="button" class="omgalink jerry" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Modal-->





					<!-- Modal Developer Sector -->
					<div class="modal modal-formodal" id="modaldeveloper" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<div>
									<h2>Developer</h2>
									<p>You want to make a difference in the construction sector but cost, time and quality issues associated with new practices are preventing this?</p><p>Zero carbon building is your aspiration but you've not all the solutions to deliver this.</p><p>You want de-risked build systems and technologies fit for purpose.</p><p>You require solutions that carry the necessary building certificates, are BOPAS accredited, BS 7014, have building warrantees and are mortgageable.</p>
									</div>
								</div>
								<div class="modal-footer">
								<button type="button" class="omgalink jerry" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Modal-->






					<!-- Modal Technology Developer Sector -->
					<div class="modal modal-formodal" id="modaltechnologydeveloper" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<div>
									<h2>Technology Developer</h2>
									<p>You've an idea you know will have a positive impact.</p><p>You're struggling to finance and bring your solution to the market.</p><p>You face obstacles and barriers, resource constraints and uncertainty around whether your even ready to go to market?</p><p>You need technical due diligence and guidance on the next steps?</p><p>Our team has extensive experience in successful product development and bringing innovation to the mainstream.</p>
									</div>
								</div>
								<div class="modal-footer">
								<button type="button" class="omgalink jerry" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Modal-->





					<!-- Modal Architect Sector -->
					<div class="modal modal-formodal" id="modalarchitect" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<div>
									<h2>Architect</h2>
									<p>You've a community led scheme, a concept to build beyond zero carbon.</p><p>You require a team with the experience and knowledge of build systems and technology integration that can realise your design?</p><p>You want authentic design team integration and recognise the limits of working in silos.</p><p>You want to work with teams and consortiums that recognise the value of practical, awe inspiring, sustainable architecture.</p><p>Calyx has a unique set of skills that can set your project apart.</p>
									</div>
								</div>
								<div class="modal-footer">
								<button type="button" class="omgalink jerry" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Modal-->





					<!-- Modal Local Authority Sector -->
					<div class="modal modal-formodal" id="modallocalauthority" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<div>
									<h2>Local Authority</h2>
									<p>Social housing is in crisis and Council is desperately seeking solutions to house people each day?</p><p>You've infill sites that could provide a solution to social housing.</p><p>You require low cost, social housing that meets wider standards such as low to zero carbon and DDA.</p><p>You need build systems that can be assembled quickly and meet exemplar levels of quality and longevity.</p><p>You want solutions that empower communities, support cohesion, wellbeing and security.</p><p>You are looking for new business models that will drive revenue, assist the Council to meet its objectives and move beyond austerity?</p>
									</div>
								</div>
								<div class="modal-footer">
								<button type="button" class="omgalink jerry" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Modal-->



			</div>
		</div>
	</div>





</div>

</div></div></div>
</section>
  </div>

                                    

                        

            
            

<footer class="contented">
    <div class="container">
        <div class="row">

            <div class="col-sm-4">
				<ul class="list-unstyled">

					<li><a href="/events">Events</a></li>
					<li><a href="/news">News</a></li>
					<li><a href="/privacy">Privacy</a></li>
					<li><a href="/terms">Terms</a> </li>
					<li><span class="fab fa-facebook-square fw"></span> &nbsp;<a class="footer-facebook" href="https://facebook.com/carbonfreegroup">Facebook</a></li>
					<li><span class="fab fa-twitter-square fw"></span> &nbsp;<a class="footer-twitter" href="https://twitter.com/carbonfreegroup">Twitter</a></li>
					<li><span class="fab fa-linkedin fw"></span> &nbsp;<a class="footer-linkedin" href="https://linkedin.com/company/carbonfreegroup">LinkedIn</a></li>
				</ul>

            </div>

			<div class="col-sm-4 bottom-lft lower-logo">
				<div class="insfot">
					<img src="/sites/all/themes/cfg/images/logo-mono-small.png" alt="cfg logo in monochrome" />
					<p>The Carbon Free Group<br />Pines Calyx, Beach Road, St Margaret’s Bay, Dover, Kent, CT15 6DZ



				</div>
			</div>


            <div class="col-sm-4 bottom-right signupform">
                				<ul class="footer-social">

				</ul>
                <span class="fa fa-envelope fw fa-4x lowermailer"></span>

                <p><a href="https://carbonfreegroup.us16.list-manage.com/subscribe/post?u=f8d0da5a4627597427593184d&id=36a5728c6e">Sign up to our low-volume mailing list</a></p>
            </div>

        </div>
    </div>

</footer>
  <script src="https://www.carbonfreegroup.com/sites/all/themes/bootstrap/js/bootstrap.js?pwzhmj"></script>
</body>
  <script src="/sites/all/themes/cfg/js/stickyy.js"></script>
  <script src="/sites/all/themes/cfg/js/lightbox.js"></script>

</html>
