<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="CEoGB News" href="https://www.cycling-embassy.org.uk/news/feed" />
<link rel="alternate" type="application/rss+xml" title="CEoGB Blog" href="https://www.cycling-embassy.org.uk/blog/feed" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.cycling-embassy.org.uk/" />
<link rel="shortlink" href="https://www.cycling-embassy.org.uk/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.cycling-embassy.org.uk/" />
<meta name="twitter:title" content="Cycling Embassy of Great Britain" />
<meta name="twitter:description" content="Making riding a bike as easy as riding a bike." />
  <title>Cycling Embassy of Great Britain | Making riding a bike as easy as riding a bike.</title>
  <link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_ZC3N-9qOC2IAgnlJtOLbNjLsJEJ61Zb0wC6OGJHr-QU.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_O4pFH05DA3xG-_qbMg3ynaHUuPD3QLsxjkaZFrUTL-4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_mItWKfsDoArj3WtA0a8xYvGPqNW3C6Ktj1hEf2QuTp8.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_2THG1eGiBIizsWFeexsNe1iDifJ00QRS9uSd03rY9co.css" media="print" />

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/themes/bartik/css/ie.css?q1mhon" media="all" />
<![endif]-->

<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/themes/bartik/css/ie6.css?q1mhon" media="all" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_X9T9aC3VOw_tht6DFGq3Kx_5tXK81ZSe29v196JWwrE.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css_injector/css_injector_2.css?q1mhon" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/css/css_mKqN3hWcRva7S_6VD_7WAX2yQmnb5ghaF_O34DTjkjk.css" media="all" />
  <script type="text/javascript" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/js/js_GzwoZuzLVd86e2xqQXNH1K-9SxrDsm7BYW4w5lIo_qY.js"></script>
<script type="text/javascript" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/js/js_AjmHFqpXq-vGvMQ4vEKpWSIRR3vZGeSA_yDGgl_f6Kg.js"></script>
<script type="text/javascript" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/js/js_FH6Sq35ZRguJ-wPJN8tcglGkUDIsCVM2Ul79TNBqu_c.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-29341262-1", {"cookieDomain":"auto"});ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/js/js_i9-mZxF1wrIj_pHo8CcygMfG3cz35Mlm6odAjzL19fk.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _paq = _paq || [];(function(){var u=(("https:" == document.location.protocol) ? "https://piwik.fonant.com/" : "http://piwik.fonant.com/");_paq.push(["setSiteId", "34142"]);_paq.push(["setTrackerUrl", u+"piwik.php"]);_paq.push(["setDoNotTrack", 1]);_paq.push(["trackPageView"]);_paq.push(["setIgnoreClasses", ["no-tracking","colorbox"]]);_paq.push(["enableLinkTracking"]);var d=document,g=d.createElement("script"),s=d.getElementsByTagName("script")[0];g.type="text/javascript";g.defer=true;g.async=true;g.src=u+"piwik.js";s.parentNode.insertBefore(g,s);})();
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/js/js_wdoxK3Iz90bPPtXIP1AgwlNO0C6rDjZxyrgqpBEhKDM.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"ceogbbartik7","theme_token":"3jr3Ig3sIdlB4OLx7E2KXbUT-gLT8oxJgzU3obnThZs","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.7\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/extlink\/extlink.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/modules\/matomo\/matomo.js":1,"1":1,"sites\/all\/themes\/bartik_plus\/scripts\/superfish.js":1,"sites\/all\/themes\/bartik_plus\/scripts\/bartik_plus.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"modules\/aggregator\/aggregator.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/footnotes\/footnotes.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"modules\/node\/node.css":1,"modules\/poll\/poll.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/extlink\/extlink.css":1,"modules\/forum\/forum.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/rate\/rate.css":1,"sites\/cycling-embassy.org.uk\/files\/sl_sprites.css":1,"sites\/all\/modules\/tagclouds\/tagclouds.css":1,"sites\/all\/modules\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"public:\/\/ctools\/css\/6d729fa703647b63667c4ee55b61faa2.css":1,"themes\/bartik\/css\/layout.css":1,"themes\/bartik\/css\/style.css":1,"sites\/cycling-embassy.org.uk\/themes\/ceogbbartik7\/css\/colors.css":1,"sites\/all\/themes\/bartik_plus\/css\/menu_style.css":1,"sites\/cycling-embassy.org.uk\/themes\/ceogbbartik7\/css\/ceogb.css":1,"themes\/bartik\/css\/print.css":1,"themes\/bartik\/css\/ie.css":1,"themes\/bartik\/css\/ie6.css":1,"public:\/\/css_injector\/css_injector_1.css":1,"public:\/\/css_injector\/css_injector_2.css":1,"public:\/\/css_injector\/css_injector_3.css":1,"public:\/\/css_injector\/css_injector_4.css":1,"public:\/\/css_injector\/css_injector_5.css":1,"public:\/\/css_injector\/css_injector_6.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"extlink":{"extTarget":"_blank","extClass":"ext","extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":"","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site. We are not responsible for their content.","mailtoClass":"mailto","mailtoLabel":"(link sends e-mail)"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"matomo":{"trackMailto":1,"trackColorbox":1},"urlIsAjaxTrusted":{"\/":true,"\/home?destination=home":true}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-home" >
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div id="page-wrapper"><div id="page">

  <div id="header" class="with-secondary-menu"><div class="section clearfix">

          <a href="/" title="Home" rel="home" id="logo">
        <img src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/themes/ceogbbartik7/logo.png" alt="Home" />
      </a>
    
          <div id="name-and-slogan">

                              <h1 id="site-name">
              <a href="/" title="Home" rel="home"><span>Cycling Embassy of Great Britain</span></a>
            </h1>
                  
                  <div id="site-slogan">
            Making riding a bike as easy as riding a bike.          </div>
        
      </div> <!-- /#name-and-slogan -->
    
      <div class="region region-header">
    <div id="block-block-5" class="block block-block">

    <h2 class="element-invisible">Social links</h2>
  
  <div class="content">
    <div id="social_media_links"><a href="https://www.facebook.com/Cycling-Embassy-of-Great-Britain-177456432272905" target="_blank"><img src="/sites/cycling-embassy.org.uk/files/icon-social-facebook.png" alt="Facebook" /></a><a href="https://twitter.com/GBCycleEmbassy" target="_blank"><img src="/sites/cycling-embassy.org.uk/files/icon-social-twitter.png" alt="Twitter" /></a></div>  </div>
</div>
<div id="block-search-form" class="block block-search">

    
  <div class="content">
    <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-QzWynqMaf2j4ETnm57uQMzKSdYuuobkZilbt0agz3JI" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>  </div>
</div>
  </div>

          <div id="main-menu" class="navigation">
        <div id="main-menu-links"><ul class="menu clearfix"><li class="first leaf"><a href="/" title="" class="active">Home</a></li>
<li class="leaf"><a href="/news" title="">News</a></li>
<li class="leaf"><a href="/blog" title="">Blog</a></li>
<li class="leaf"><a href="/forum/active" title="">Forums</a></li>
<li class="expanded"><a href="/wiki/knowledge-base" title="Wiki">Knowledge base</a><ul class="menu clearfix"><li class="first leaf"><a href="/documents" title="">Document library</a></li>
<li class="leaf"><a href="http://www.cycling-embassy.org.uk/node/3237/" title="Report from the 2014 AGM">2014 AGM</a></li>
<li class="leaf"><a href="/node/3446">2015 AGM Summary</a></li>
<li class="leaf"><a href="http://www.cycling-embassy.org.uk/dictionary" title="Dictionary of terms and concepts">Dictionary</a></li>
<li class="leaf"><a href="http://www.cycling-embassy.org.uk/wiki/knowledge-base" title="Knowledge Home">Knowledge Home</a></li>
<li class="leaf"><a href="http://www.cycling-embassy.org.uk/wiki-index?order=title&amp;sort=asc" title="Index to our Wiki">Wiki Index</a></li>
<li class="last leaf"><a href="/iloh" title="">Insert Loved One Here</a></li>
</ul></li>
<li class="expanded"><a href="/about" title="">About us ▿</a><ul class="menu clearfix"><li class="first leaf"><a href="/about" title="About the Cycling Embassy">About us</a></li>
<li class="leaf"><a href="/mission">Manifesto &amp; Mission Statement</a></li>
<li class="leaf"><a href="/campaigns-we-support" title="">Campaigns we support</a></li>
<li class="leaf"><a href="/people" title="People">People</a></li>
<li class="leaf"><a href="/faq" title="Frequently Asked Questions">FAQ</a></li>
<li class="last leaf"><a href="/contact" title="">Contact</a></li>
</ul></li>
<li class="last leaf"><a href="/join-us" title="">Join Us</a></li>
</ul></div>      </div> <!-- /#main-menu -->
    
          <div id="secondary-menu" class="navigation">
        <h2 class="element-invisible">Secondary menu</h2><ul id="secondary-menu-links" class="links inline clearfix"><li class="menu-19446 first"><a href="/user/register" title="Apply for an account on this website, to get involved with Forum discussions, and more.">Register</a></li>
<li class="menu-19447 last"><a href="/user/login" title="Log into the site to get involved.">Log in</a></li>
</ul>      </div> <!-- /#secondary-menu -->
    
  </div></div> <!-- /.section, /#header -->

  
  
  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    
    
    <div id="content" class="column"><div class="section">
            <a id="main-content"></a>
                                <div class="tabs">
                  </div>
                          <div class="region region-content">
    <div id="block-system-main" class="block block-system">

    
  <div class="content">
    <div class="panel-flexible panels-flexible-1 clearfix" >
<div class="panel-flexible-inside panels-flexible-1-inside">
<div class="panels-flexible-row panels-flexible-row-1-1 panels-flexible-row-first clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-1-inside panels-flexible-row-inside-first clearfix">
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-main-row clearfix">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-main-row-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-1-center panels-flexible-region-first ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-center-inside panels-flexible-region-inside-first">
<div class="panel-pane pane-node"  >
  
        <h2 class="pane-title">
      Cycling for the Rest of Us    </h2>
    
  
  <div class="pane-content">
    <div id="node-2287" class="node node-page clearfix" about="/node/2287" typeof="sioc:Item foaf:Document">

        <h2>
      <a href="/node/2287"></a>
    </h2>
    <span property="dc:title" content="" class="rdf-meta element-hidden"></span><span property="sioc:num_replies" content="0" datatype="xsd:integer" class="rdf-meta element-hidden"></span>
  
  <div class="content clearfix">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Cycling should be the simplest, easiest and most convenient way to get around in most places in the UK, and yet the reality is that it&#39;s not &ndash; the roads are busy, and feel unsafe or unpleasant to ride on, and what cycle paths there are seem to take the long way round, or run out just when you need them most.</p><p>The Dutch and the Danes have demonstrated that it is easy to provide safe, attractive and efficient routes for cycling, away from fast traffic, which anybody and everybody &ndash; men and women, old and young, and rich and poor &ndash; want to use.</p><p>We at the Cycling Embassy of Great Britain want to see an end to cycling being pushed to the margins; we want to see a network of direct, well-designed, separated cycle routes that are safe even for young children to use.</p><ul><li><a href="/mission">Manifesto &amp; Mission Statement</a></li><li><a href="/join-us">Join us</a> and <a href="/situations-vacant">get involved</a></li></ul>

</div></div></div><span class="submitted-by"></span>  </div>

  
  
</div>
  </div>

  
  </div>
  </div>
</div>
<div class="panels-flexible-region panels-flexible-region-1-right panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-right-inside panels-flexible-region-inside-last">
<div class="panel-pane pane-views pane-front-page-photos"  >
  
      
  
  <div class="pane-content">
    <div class="view view-front-page-photos view-id-front_page_photos view-display-id-default view-dom-id-b258bedae08bc2e92c360d870e5d2faf">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/styles/front_page/public/photos/two-girls-ride-on-dutch-cycle-path-beside-busy-road.jpg?itok=fgLdvnXy" width="240" height="160" alt="Two girls use a cycle path which is separated from the busy road by a wide grass verge and trees" /></div>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/styles/front_page/public/photos/dscn9172.jpg?itok=CRun5_N8" width="240" height="160" alt="Middle-aged women riding on a cycle path which is separated from the road by a wide grass verge and two rows of trees" /></div>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-gallery-image">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/styles/front_page/public/photos/three-dutch-teenagers-ride-bikes-outside-bike-park.jpg?itok=zvx5omfV" width="240" height="160" alt="Three teenagers on Dutch &quot;omafiets&quot; bikes ride past hundreds of bikes parked in racks" /></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-2 clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-2-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-1-bottom_left panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-bottom_left-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-views pane-public-reasons"  >
  
        <h2 class="pane-title">
      Why I joined the Cycling Embassy...    </h2>
    
  
  <div class="pane-content">
    <div class="view view-public-reasons view-id-public_reasons view-display-id-block view-dom-id-e93bdbc15e00a690f3d34377fe3c35a6">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-edited-reason">        <div class="field-content display_reason"><em>&ldquo;I find I&#039;m cycling less as I get older – I want the roads to be safe for me to cycle on with a light heart, not with a warrior mindset!&rdquo;</em><br /><a href="/join-us" class="join_us_link">Join us!</a></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views pane-good-cycling-facility-of-the-week"  >
  
        <h2 class="pane-title">
      <a href="/good-cycling-facility-of-the-week">Good Cycling Facility of the Week</a>    </h2>
    
  
  <div class="pane-content">
    <div class="view view-good-cycling-facility-of-the-week view-id-good_cycling_facility_of_the_week view-display-id-block view-dom-id-29277c257af9f762f8d652bba5580cc4">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-field-gallery-image">        <div class="field-content"><a href="/photos/good-cycling-facility-of-the-week/good-cycling-facility-of-the-week-28th-november-2019"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/sites/cycling-embassy.org.uk/files/styles/main_left_panel_wide/public/photos/Screenshot%202019-11-27%20at%2023.12.52.jpg?itok=W8pW9Z8U" width="683" height="455" alt="Protected cycleway with filtered side road, Waltham Forest, London, England" title="Protected cycleway with filtered side road, Waltham Forest, London, England" /></a></div>  </div>  
  <div class="views-field views-field-view-node">        <span class="field-content node-readmore"><a href="/photos/good-cycling-facility-of-the-week/good-cycling-facility-of-the-week-28th-november-2019">Read more about this photo…</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-3 clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-3-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-1-bottom_left_ panels-flexible-region-first ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-bottom_left_-inside panels-flexible-region-inside-first">
  </div>
</div>
<div class="panels-flexible-region panels-flexible-region-1-bottom_right_ panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-bottom_right_-inside panels-flexible-region-inside-last">
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-1-4 panels-flexible-row-last clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-1-4-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-region panels-flexible-region-1-bottom panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-1-bottom-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
  </div>
</div>
  </div>
</div>
</div>
</div>
  </div>
</div>
  </div>
      
    </div></div> <!-- /.section, /#content -->

          <div id="sidebar-second" class="column sidebar"><div class="section">
          <div class="region region-sidebar-second">
    <div id="block-block-1" class="block block-block">

    <h2>Donate</h2>
  
  <div class="content">
    CEoGB is a volunteer-run organisation, funded entirely from voluntary donations.
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick" /><input type="hidden" name="hosted_button_id" value="TVTL3YR444Z3N" /><input type="image" src="https://www.paypal.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online." /></form>
  </div>
</div>
<div id="block-views-latest-news-block-1" class="block block-views">

    <h2>Embassy News</h2>
  
  <div class="content">
    <div class="view view-latest-news view-id-latest_news view-display-id-block_1 view-dom-id-36c5f2e8f7930d15bcd969f7260cbbb9">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/news/2019/05/15/save-the-date-cycling-embassy-agm-19th-21st-july-2019">Save the Date - Cycling Embassy AGM, 19th-21st July 2019</a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Wednesday 15th May 2019</span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/news/2018/06/11/the-agm-is-coming-to-manchester">The AGM is coming to Manchester!</a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Monday 11th June 2018</span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/news/2018/06/10/embassy-response-to-cycling-and-walking-investment-strategy-safety-review">Embassy response to Cycling and Walking Investment Strategy Safety Review</a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Sunday 10th June 2018</span>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/news">
    more news items...  </a>
</div>
  
  
      <div class="feed-icon">
      <a href="https://www.cycling-embassy.org.uk/news/feed" class="feed-icon" title="Subscribe to CEoGB News"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/misc/feed.png" width="16" height="16" alt="Subscribe to CEoGB News" /></a>    </div>
  
</div>  </div>
</div>
<div id="block-views-blog-block-1" class="block block-views">

    <h2>Embassy Blog</h2>
  
  <div class="content">
    <div class="view view-blog view-id-blog view-display-id-block_1 view-dom-id-1bb9076631d8dd21b0f1228f62f34343">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/blog/2019/07/01/a-bike-for-people-who-arent-interested-in-cycling">A bike for people who aren&#039;t interested in cycling</a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Monday 1st July 2019 by</span>  </div>  
  <div class="views-field views-field-field-display-name">        <div class="field-content">Mark Treasure</div>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/blog/2019/06/17/help-%E2%80%93-my-council-has-just-won-megabucks-for-cycling">Help – my council has just won megabucks for cycling! </a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Monday 17th June 2019 by</span>  </div>  
  <div class="views-field views-field-field-display-name">        <div class="field-content"></div>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
      
  <div class="views-field views-field-title">        <h3 class="field-content"><a href="/blog/2018/01/08/health-physical-inactivity-obesity-and-cycling">Health, physical inactivity, obesity and cycling</a></h3>  </div>  
  <div class="views-field views-field-created">        <span class="field-content">Monday 8th January 2018 by</span>  </div>  
  <div class="views-field views-field-field-display-name">        <div class="field-content">Mark Treasure</div>  </div>  </div>
    </div>
  
  
  
      
<div class="more-link">
  <a href="/blog">
    more blog posts...  </a>
</div>
  
  
      <div class="feed-icon">
      <a href="https://www.cycling-embassy.org.uk/blog/feed" class="feed-icon" title="Subscribe to CEoGB Blog"><img typeof="foaf:Image" src="https://www.cycling-embassy.org.uk/misc/feed.png" width="16" height="16" alt="Subscribe to CEoGB Blog" /></a>    </div>
  
</div>  </div>
</div>
<div id="block-user-login" class="block block-user">

    <h2>User login</h2>
  
  <div class="content">
    <form action="/home?destination=home" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Username or e-mail <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list"><ul><li class="first"><a href="/user/register" title="Create a new user account.">Create new account</a></li>
<li class="last"><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li>
</ul></div><input type="hidden" name="form_build_id" value="form-nZ1GlMlrE6C14Y5du2wAXtGnZrfepvMwEw7wcjoBzSo" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Log in" class="form-submit" /></div></div></form>  </div>
</div>
  </div>
      </div></div> <!-- /.section, /#sidebar-second -->
    
  </div></div> <!-- /#main, /#main-wrapper -->

  
  <div id="footer-wrapper"><div class="section">

    
          <div id="footer" class="clearfix">
          <div class="region region-footer">
    <div id="block-block-3" class="block block-block">

    
  <div class="content">
    	<p>© Cycling Embassy of Great Britain</p>  </div>
</div>
  </div>
      </div> <!-- /#footer -->
    
  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
  </body>
</html>
