<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
  <head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <base href="">

  <!--[if IE]><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://lobsterpictures.tv/sites/default/files/lobster-icon.png" type="image/png" />
<meta name="description" content="Lobster Pictures Time Lapse systems allows you to watch the progress of your long term projects with online remote site monitoring from anywhere in the world." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://lobsterpictures.tv/" />
<link rel="shortlink" href="https://lobsterpictures.tv/" />
<meta property="og:site_name" content="Lobster Pictures" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://lobsterpictures.tv/" />
<meta property="og:title" content="Lobster Pictures" />
<meta property="og:image" content="http://www.lobsterpictures.tv/sites/default/files/lobster_pictures_logo_v4_rgb_no_text_square.png" />
  <title>Lobster Pictures | HD and above time-lapse cameras for construction, engineering and media</title>

  <!-- IE8 Style Sheets -->
  <!--[if lt IE 10]>
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/css/bootstrap-main.css"/>
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/css/styles-lt-ie10.css"/>
    <script src="/sites/all/themes/bower_components/html5shiv/dist/html5shiv.js"></script>
    <script src="/sites/all/themes/bower_components/selectivizr/selectivizr.js"></script>
    <script src="/sites/all/themes/bower_components/respond/dest/respond.min.js"></script>

  <![endif]-->

  <link type="text/css" rel="stylesheet" href="https://lobsterpictures.tv/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://lobsterpictures.tv/sites/default/files/css/css_vZ_wrMQ9Og-YPPxa1q4us3N7DsZMJa-14jShHgRoRNo.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://lobsterpictures.tv/sites/default/files/css/css_3o4ITkSqy8eY9IvOW5jzmYHuj-rzcDo08Go47fDtB6c.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://lobsterpictures.tv/sites/default/files/css/css_xvarCUoqaH3v-qHnsoFE26yPyHMzyj5T8pkNogSpHUQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://lobsterpictures.tv/sites/default/files/css/css_u-ZrhTFKcfWBmTGB8dwlu3U-t7esl46uJ88SguSgX-Y.css" media="all" />

  <script scr="sites/all/themes/custom/bower_components/modernizr/modernizr.js"></script>
  <script>(function(h,o,t,j,a,r){
  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  h._hjSettings={hjid:"286207",hjsv:5};
  a=o.getElementsByTagName('head')[0];
  r=o.createElement('script');r.async=1;
  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');</script>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_dYOGV15VEcvR5zyP3UKzyEH3a6wcYkaWkQCEZ8Wpd8c.js"></script>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_obFUyTbqtcoGQtszVI46l1_oD927PzlDtlr_ilpYBqE.js"></script>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_k4UQA_UccMfK3ccZ-RLlRSZjO_y2l8HEgysi_7NApJc.js"></script>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_uJqunffG9ENYHLkdBvfIdBTimi6NM2wqYo0R8RTKaZ8.js"></script>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_lI52rMh0DIhasy6wnwSWMgQT3y4xgu_5YA0bJ0dUzjY.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"custom","theme_token":"3fwwHwo62L2ZZ_ZtMzTGm6gbKOVTeCMgyNmWz9NtY9M","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"0":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.8\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/views_slideshow\/js\/views_slideshow.js":1,"sites\/all\/modules\/colorbox_responsive_addon\/js\/colorbox-plugin.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.js":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"sites\/all\/themes\/custom\/bower_components\/bootstrap-sass\/js\/alert.js":1,"sites\/all\/themes\/custom\/bower_components\/bootstrap-sass\/js\/button.js":1,"sites\/all\/themes\/custom\/bower_components\/bootstrap-sass\/js\/collapse.js":1,"sites\/all\/themes\/custom\/bower_components\/bootstrap-sass\/js\/tab.js":1,"sites\/all\/themes\/custom\/bower_components\/imagesloaded\/imagesloaded.pkgd.min.js":1,"sites\/all\/themes\/custom\/js\/script.js":1,"sites\/all\/themes\/custom\/js\/vimeo.ga.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"sites\/all\/themes\/custom\/css\/style.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"viewsSlideshow":{"quote_carousel-block":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_quote_carousel-block":{"num_divs":4,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"quote_carousel-block","effect":"scrollLeft","transition_advanced":0,"timeout":5000,"speed":700,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"action_advanced":0,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":3000,"cleartype":0,"cleartypenobg":0,"advanced_options":"{}"}},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":0,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":0,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>


  <!-- Fonts -->
  <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7979574/800828/css/fonts.css" />

</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-homepage-ct" >
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MB7CL6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script type="text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0];var j=d.createElement(s);var dl=l!='dataLayer'?'&l='+l:'';j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;j.type='text/javascript';j.async=true;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MB7CL6');</script>
<!-- End Google Tag Manager -->
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
<!--[if lt IE 9]>
    <div class="browsehappy"><p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p></div>
<![endif]-->


<div class="scroll-fix-container"></div>

<header class="header" role="header">
  <nav class="navbar container navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">

                <a class="navbar-brand" href="/" title="Home">
          <img src="https://lobsterpictures.tv/sites/default/files/lobster-pictures-logo-retina.png" alt="Home" />
        </a>
        
        <div class="scroll-fix">
            <div class="call">
                <span class="icon">Call</span><span class="rTapNumber173212">+44 (0)117 370 4217</span>
            </div>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
              <span>Menu</span>
              <span class="caret"></span>
            </button>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="main-menu">
                                                                                              <div class="region region-navigation">
    <section id="block-menu-block-1" class="block block-menu-block clearfix">

      
  <div class="menu-block-wrapper menu-block-1 menu-name-menu-main-menu-left parent-mlid-0 menu-level-1">
  <div class="nav-collapse"><ul class="menu nav"><li class="first expanded menu-mlid-914 dropdown"><a href="/our-services" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Our Services <span class="caret"></span></a><ul class="dropdown-menu menu-level-1"><li class="first leaf menu-mlid-812"><a href="/our-services/construction-time-lapse">Time Lapse</a></li>
<li class="leaf menu-mlid-813"><a href="/our-services/construction-site-monitoring">Site Monitoring</a></li>
<li class="last leaf menu-mlid-814"><a href="/our-services/media-production">Media Production</a></li>
</ul></li>
<li class="expanded menu-mlid-858 dropdown"><a href="/our-work" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Our Work <span class="caret"></span></a><ul class="dropdown-menu menu-level-1"><li class="first leaf menu-mlid-844"><a href="/our-work/sectors">Sectors</a></li>
<li class="leaf menu-mlid-818"><a href="/our-work/case-studies">Case Studies</a></li>
<li class="last leaf menu-mlid-816"><a href="/our-work/clients">Clients</a></li>
</ul></li>
<li class="leaf menu-mlid-922"><a href="/time-lapse-bim-integration">BIM</a></li>
<li class="expanded menu-mlid-897 dropdown"><a href="/why-choose-us" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Why Choose Us? <span class="caret"></span></a><ul class="dropdown-menu menu-level-1"><li class="first leaf menu-mlid-1129"><a href="https://blog.lobsterpictures.tv/get-to-know-your-time-lapse-viewer" title="">Lobster Vision Viewer</a></li>
<li class="leaf menu-mlid-905"><a href="/why-choose-us/technology-and-cameras">Technology &amp; Cameras</a></li>
<li class="leaf menu-mlid-980"><a href="/why-choose-us/content-creation">Content Creation</a></li>
<li class="leaf menu-mlid-846"><a href="/why-choose-us/safety-accreditation">Safety &amp; Accreditation</a></li>
<li class="leaf menu-mlid-1365"><a href="/why-choose-us/data-security">Data Security</a></li>
<li class="leaf menu-mlid-838"><a href="/why-choose-us/international">International</a></li>
<li class="leaf menu-mlid-925"><a href="/why-choose-us/comparison-tools">Comparison Tools</a></li>
<li class="leaf menu-mlid-989"><a href="https://blog.lobsterpictures.tv/time-lapse-site-monitoring-frequently-asked-questions" title="">FAQ</a></li>
<li class="last leaf menu-mlid-1366"><a href="/why-choose-us/testimonials">Testimonials</a></li>
</ul></li>
<li class="leaf menu-mlid-806"><a href="/showreel">Showreel</a></li>
<li class="leaf menu-mlid-911"><a href="/about-us">About Us</a></li>
<li class="leaf menu-mlid-982"><a href="https://blog.lobsterpictures.tv/" title="">Blog</a></li>
<li class="last leaf menu-mlid-1128"><a href="https://lobstervision.tv/login" title="Lobster Vision client login">Client Login</a></li>
</ul></div></div>

</section> <!-- /.block -->
<section id="block-menu-block-2" class="block block-menu-block clearfix">

      
  <div class="menu-block-wrapper menu-block-2 menu-name-menu-main-menu-right parent-mlid-0 menu-level-1">
  <div class="nav-collapse"><ul class="menu nav"><li class="first last leaf menu-mlid-986"><a href="/contact-us" title="">Contact Us</a></li>
</ul></div></div>

</section> <!-- /.block -->
  </div>
                                        </div><!-- /.navbar-collapse -->
                      </div>
      </div>

  </nav>
</header>

  <section class="banner" role="banner">
      <div class="region region-banner">
    <section id="block-views-masthead-block" class="block block-views clearfix">

      
  <div class="view view-masthead view-id-masthead view-display-id-block view-dom-id-063506e8a37d0fede8de08705f82a6c4">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
    
	
	<div class="masthead html_video" style="background-image: url('/sites/default/files/styles/banner_large/public/mastheads/lobster-picture-time-lapse-video_0.jpg?itok=zpRPzI31');">
		<video loop autoplay preload muted class="html_video" poster="/sites/default/files/styles/banner_large/public/mastheads/lobster-picture-time-lapse-video_0.jpg?itok=zpRPzI31">
										<source src="/sites/default/files/timelapse-lobster-pictures-overview.mp4" type="video/mp4" />
					</video>
		<div class="masthead-title-wrap">
			<div class="container masthead-container">
				<div class="row">
					<div class="masthead-title col-st-10 col-st-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
						<h1>Time Lapse and Monitoring</h1>						<p>HD, 4K and 8K time lapse and monitoring. BIM integration. Construction specialists. The only ISO 9001:2015 certified provider.</p>
					</div>
				</div>
			</div>
		</div>
	</div>


  </div>
    </div>
  
  
  
  
  
  
</div>
</section> <!-- /.block -->
  </div>
  </section>  <!-- /#page-banner -->

<section id="main" class="container" role="main" >
  <div class="main-wrapper">
   
              <div class="region region-highlighted">
    <section id="block-views-feature-blocks-block-2" class="block block-views clearfix">

      
  <div class="view view-feature-blocks view-id-feature_blocks view-display-id-block_2 view-dom-id-3cdc36144b185f8f1bf5051a2a3141cc">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
    	<a href="/our-services/construction-site-monitoring">
<img typeof="foaf:Image" class="img-responsive" src="https://lobsterpictures.tv/sites/default/files/styles/service_block_thumbnails/public/fb_thumbs/London%20Olympic%20Stadium%20Time%20Lapse%20-%20Small.jpg?itok=gY5uLfcJ" width="380" height="190" alt="London Olympic Stadium Time Lapse and Site Monitoring" /><div class="field-container">
    <div class="field-content">
        <h3>Site Monitoring</h3>        <p>Remote construction site monitoring. View your site online from anywhere in the world, on any device.</p>
    </div>
</div>
</a>
  </div>
  <div class="views-row views-row-2 views-row-even">
    	<a href="https://lobsterpictures.tv/demo/">
<img typeof="foaf:Image" class="img-responsive" src="https://lobsterpictures.tv/sites/default/files/styles/service_block_thumbnails/public/fb_thumbs/Lobster%20Vision%20Screens%20380x190.jpg?itok=w4MUcAb6" width="380" height="190" alt="Lobster Vision Site Monitoring Screens" /><div class="field-container">
    <div class="field-content">
        <h3>View A Live Camera</h3>        <p>Never miss a moment with our live web interface, Lobster Vision.</p>
    </div>
</div>
</a>
  </div>
  <div class="views-row views-row-3 views-row-odd views-row-last">
    	<a href="https://lobsterpictures.tv/time-lapse-bim-integration">
<img typeof="foaf:Image" class="img-responsive" src="https://lobsterpictures.tv/sites/default/files/styles/service_block_thumbnails/public/fb_thumbs/Screen%20Shot%202019-04-23%20at%2011.34.57.png?itok=Ib2_ko2N" width="380" height="190" alt="Pioneering BIM Integration with Lobster Pictures" /><div class="field-container">
    <div class="field-content">
        <h3>BIM Integration</h3>        <p>Our pioneering, best-in-class BIM integration. Track progress, visualise and share your model with your entire team.</p>
    </div>
</div>
</a>
  </div>
    </div>
  
  
  
  
  
  
</div>
</section> <!-- /.block -->
  </div>
        
    
    <a id="main-content"></a>
                    <div class="row admin-tabs">
          </div>
    
    
    
      <div class="row">

            
                          <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  <article id="node-1" class="node node-homepage-ct node-promoted clearfix" about="/time-lapse-and-monitoring" typeof="sioc:Item foaf:Document">
    <header>
            <span property="dc:title" content="Time Lapse and Monitoring" class="rdf-meta element-hidden"></span>      </header>
    <div class="field field-name-field-body field-type-text-long field-label-hidden">
    <div class="field-items">
          <div class="field-item even"><h2 class="body-pull-block">Time Lapse and Monitoring</h2>
<p>Lobster Pictures can transform how your project is seen and managed. As the world leader in high-quality, remote time lapse camera systems, we can deliver you cost savings, PR material and stakeholder engagement — all in one package.</p>
<p>Our pioneering <a href="/why-choose-us/technology/cameras" target="_blank">time lapse camera</a>, the Lobster Pot - gives the best possible quality for long-term time lapse and remote site monitoring and we were the first company to develop an 8K version - the highest resolution monitored time lapse camera available in the world. </p>
<p> </p>
<h4 class="body-pull-block-alt">View Online, On Any Device</h4>
<p>With <a href="https://lobsterpictures.tv/demo/">Lobster Vision,</a> our world-beating bespoke<a href="/why-choose-us/technology/lobster-vision"> </a>site monitoring tool and online viewing platform, you can monitor and promote any kind of construction or infrastructure project. Plus share your above-HD images and time lapse videos on any device - making it a unique platform for collaboration and communication. Our trusted clients use the viewer for <a href="https://blog.lobsterpictures.tv/get-to-know-your-time-lapse-viewer">project management,</a> <a href="https://lobsterpictures.tv/our-services/construction-site-monitoring">site monitoring,</a> dispute resolution, <a href="https://lobsterpictures.tv/why-choose-us/content-creation">marketing and PR material</a> and more. So much more than just an online image archive - try it for yourself below!</p>
<p><iframe height="675" src="https://viewer.lobstervision.tv/demo" width="940" xml="lang"></iframe></p>
<p> </p>
<h4>BIM Integration</h4>
<p>We are the first company to develop and offer <a href="https://lobsterpictures.tv/building-information-modelling">BIM model integration</a> with our as-built records. See a live BIM-enabled camera <a href="https://lobsterpictures.tv/bimdemo/">here.</a></p>
<p style="text-align:center;"><a href="https://lobsterpictures.tv/bimdemo/"><img alt="" src="/sites/default/files/New%20screens%20showing%20LV%20png.png" style="width: 800px; height: 509px;" /></a></p>
<h4 class="body-pull-block-alt"> </h4>
<h4>Safety As Standard</h4>
<p>We've worked in all kinds of challenging environments. We always produce and review Method Statements and Risk Assessments for our work, and we never take risks with the safety of our team or others. As the only provider of time lapse and monitoring with <a href="https://blog.lobsterpictures.tv/iso90012015-and-why-it-matters">ISO 9001:2015 certification</a>, you have the reassurance of working with a team that is customer-focussed, quality led and constantly striving to exceed your expectations. See our accreditations <a href="https://lobsterpictures.tv/why-choose-us/safety-accreditation">here.</a></p>
<p> </p>
<h4 class="body-pull-block-alt">Guaranteed Worldwide</h4>
<p>Our expertly trained technicians professionally and safely install <a href="/why-choose-us/international" target="_blank">time lapse and site monitoring cameras globally</a>. So whether you're in the UK, New Zealand, the USA or Saudi Arabia - we've got you covered.</p>
<p> </p>
<h3 class="body-pull-block-alt">Would You Like A Quote?</h3>
<p>For a free no-obligation proposal, simply fill out <a href="https://info.lobsterpictures.tv/request-a-quotation-website-cta">this form</a>, visit our <a href="http://lobsterpictures.tv/contact-us">Contact Us</a> page, or call us on the number at the top of the page - we'd love to hear from you.</p>
<p style="margin: 1em 0px; padding: 0px; border: 0px; vertical-align: baseline; font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, sans-serif; font-size: 13.008px;"> </p>
<h3 class="body-pull-block"><span class="wrap">Trusted by the biggest names in construction</span></h3>
<div class="view view-clients view-id-clients view-display-id-block_1 view-dom-id-2754f8c7c91abd4596555db6eb5b469c">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" src="/sites/default/files/Kier-website-logo.jpg?itok=JF3iy58b" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" height="147" src="/sites/default/files/multiplex_0.jpg" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<div class="views-row views-row-3 views-row-odd">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" height="147" src="/sites/default/files/styles/client_logo/public/mclarenconstruction_0.jpg?itok=viTFVbFN" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<div class="views-row views-row-4 views-row-even">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" height="147" src="/sites/default/files/Balfour.jpg?itok=JF3iy58b" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<div class="views-row views-row-5 views-row-odd">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" height="147" src="/sites/default/files/mace.jpg" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<div class="views-row views-row-6 views-row-even">
<div class="views-field views-field-field-image">
<div class="field-content"><img alt="" height="147" src="/sites/default/files/ISG.jpg" typeof="foaf:Image" width="147" /></div>
</div>
</div>
<p style="text-align: right;"><a class="more-link" href="/clients">View all Clients</a></p>
</div>
</div>
</div>
      </div>
</div>
    </article>

</section> <!-- /.block -->
<section id="block-views-quote-carousel-block" class="block block-views clearfix">

      
  <div class="view view-quote-carousel view-id-quote_carousel view-display-id-block field-name-body view-dom-id-0b082c1ecc4de9d8e99182175836fb4f">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
       <blockquote>
      <div id="views_slideshow_cycle_main_quote_carousel-block" class="views_slideshow_cycle_main views_slideshow_main"><div id="views_slideshow_cycle_teaser_section_quote_carousel-block" class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
  <div id="views_slideshow_cycle_div_quote_carousel-block_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
    
  <div>        <p class="carousel-blockquote">I just wanted to say a massive thank you to all at Lobster Pictures for providing such a great service and amazing images of the project over the past six years. I know we will be getting the final edit soon which will be great I am sure.  It has been a pleasure to work with such a friendly, professional, helpful and always accommodating team, thank you!</p>  </div>  
  <div class="views-field views-field-field-quote-byline">        <div class="field-content">
<small>
  </small>
</div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_quote_carousel-block_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-1 views-row-even">
    
  <div>        <p class="carousel-blockquote">&quot;A big thank you for the film - it looks great and was extremely well received. So good that I&#039;ve now been asked to present it  at another event. Your work has set a benchmark for quality.&quot;</p>  </div>  
  <div class="views-field views-field-field-quote-byline">        <div class="field-content">
<small>
  George Amy, Project Manager, Construction at Multiplex Aldgate Tower</small>
</div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_quote_carousel-block_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-odd">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-2 views-row-odd">
    
  <div>        <p class="carousel-blockquote">&quot;Really happy with the product and service – hope to work again with you in the future.&quot;</p>  </div>  
  <div class="views-field views-field-field-quote-byline">        <div class="field-content">
<small>
  Dan Ambrose, Dartford Crossing</small>
</div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_quote_carousel-block_3" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-4 views_slideshow_cycle_hidden views-row-even">
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-3 views-row-even views-row-last">
    
  <div>        <p class="carousel-blockquote">&quot;Your help and assistance to date has been exceptional and we really appreciate your continued support. The camera has had amazing response from our 38 Senior managers and directors, who are really impressed. I look forward to looking at your next proposal and meeting to go through options&quot;</p>  </div>  
  <div class="views-field views-field-field-quote-byline">        <div class="field-content">
<small>
  Toni Hennessy, Assistant Project Manager, Alfred H Knight</small>
</div>  </div></div>
</div>
</div>
</div>    </blockquote>

      </div>
    </div>
  
  
  
  
  
  
</div>
</section> <!-- /.block -->
<section id="block-block-6" class="block block-block clearfix">

      
  <div class="field-name-body">
<h2 class="body-pull-block">Our Services</h2>
<p> </p>
<div class="video-container">
<iframe allowfullscreen="" frameborder="0" height="360" mozallowfullscreen="" src="https://player.vimeo.com/video/296647835" webkitallowfullscreen="" width="640"></iframe></div>
</div>

</section> <!-- /.block -->
  </div>
              
      </div>
    </div>

      </div>
</section>

<footer class="footer">

  <div class="footer-main-wrap container">
    <div class="footer-main row">
        <div class="region region-footer">
    <section id="block-block-1" class="block block-block clearfix">

      
  <p> </p>
<p><img src="https://s3-eu-west-1.amazonaws.com/lobster-email-images/isocert-web-retina.gif" style="width: 220px; height: 202px;" /></p>

</section> <!-- /.block -->
<section id="block-block-2" class="block block-block clearfix">

      
  <p>	 </p>
<p>Email: <a class="email" href="mailto:info@lobsterpictures.tv?subject=Lobster Pictures Time-Lapse and Monitoring">info@lobsterpictures.tv</a><br />
	Phone: +44 (0)117 370 4217</p>
<p>Lobster Pictures Ltd<br />
	26 Queen Square, Bristol,<br />
	BS1 4ND, UK</p>
<ul class="social-row"><li><a class="twitter" href="https://twitter.com/lobsterpictures"></a></li>
<li><a class="youtube" href="https://www.youtube.com/user/lobsterpicturestv"></a></li>
<li><a class="vimeo" href="http://vimeo.com/lobsterpictures"></a></li>
</ul>
</section> <!-- /.block -->
<section id="block-menu-menu-footer-menu" class="block block-menu clearfix">

      
  <div class="nav-collapse"><ul class="menu nav"><li class="first leaf"><a href="/our-work/sectors" title="">Sectors</a></li>
<li class="leaf"><a href="/why-choose-us/technology/lobster-vision">Lobster Vision</a></li>
<li class="leaf"><a href="/our-services/construction-time-lapse" title="">Time Lapse</a></li>
<li class="leaf"><a href="/our-services/construction-site-monitoring" title="">Site Monitoring</a></li>
<li class="leaf"><a href="/our-services/media-production" title="">Media Production</a></li>
<li class="leaf"><a href="/showreel" title="">Showreel</a></li>
<li class="leaf"><a href="/our-work/case-studies" title="">Case Studies</a></li>
<li class="leaf"><a href="/privacy">Privacy Policy</a></li>
<li class="leaf"><a href="/vacancies">Vacancies</a></li>
<li class="last leaf"><a href="/contact-us">Contact Us</a></li>
</ul></div>
</section> <!-- /.block -->
  </div>
    </div>
  </div>
  
  <div class="footer-red">
    <div class="footer-underline-wrap container">
      <div class="footer-underline row">
                          <div class="footer-legal pull-right col-sm-8">
              <div class="region region-footer-legal">
    <section id="block-block-3" class="block block-block clearfix">

      
  <p>© Lobster Pictures Ltd 2019. All rights reserved. Registered in England &amp; Wales no. 6220856</p>

</section> <!-- /.block -->
  </div>
          </div>
              </div>
    </div>
  </div>

</footer>

      <div class="region region-page-bottom">
    <!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4128888.js"></script>
<!-- End of HubSpot Embed Code -->  </div>
    <div class="region region-page-bottom">
    <!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4128888.js"></script>
<!-- End of HubSpot Embed Code -->  </div>
<script src="https://lobsterpictures.tv/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</script>
</body>
</html>
