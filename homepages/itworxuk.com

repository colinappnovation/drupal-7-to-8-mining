<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ITWORX is a fully integrated IT & Communications firm based in Aberdeen. We provide strategy and support on Cisco, Communications, IT Support & Cloud.">
    <meta name="theme-color" content="#191f1f" />
    <title>ITWORX - IT Support Services and Cisco Communications - Aberdeen</title>
    <!-- Favicons-->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">
    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7cPlayfair+Display:400i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function() {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#191f1f",
                        "text": "#ffffff"
                    },
                    "button": {
                        "background": "#f57421",
                        "text": "#ffffff"
                    }
                },
                "position": "bottom-right",
                "content": {
                    "message": "We use cookies to ensure you get the best experience on our website."
                }
            })
        });
    </script>
    <link href="assets/css/plugins.min.css" rel="stylesheet">
    <link href="assets/css/blacknavtemplate.css" rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-27599062-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-27599062-1');
    </script>
    <link rel="canonical" href="https://www.itworxuk.com" />
</head>

<body>
    <div class="page-loader">
        <div class="page-loader-inner">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <header class="header header-transparent">
        <div class="container-fluid">
            <div class="inner-header">
                <a class="inner-brand" href="index.html"></a>
                <a href="index.html">
                    <img class="brand-dark" src="assets/images/ITWORX-logo.png" width="225px" alt="ITWORX">
                </a>
            </div>
            <div class="inner-navigation collapse">
                <div class="inner-nav">
                    <ul>
                        <li><a href="index.html"><span class="menu-item-span">Home</span></a></li>
                        <li class="menu-item-has-children"><a href="services.html"><span class="menu-item-span">Services</span></a>
                            <ul class="sub-menu">
                                <li><a href="services/it-support.html">IT Support</a></li>
                                <li><a href="services/equipment-supply.html">Equipment Supply</a></li>
                                <li><a href="services/asset-disposal.html">Asset Disposal</a></li>
                                <li><a href="services/data-connectivity.html">Data Connectivity</a></li>
                                <li><a href="services/commercial-finance.html">Commercial/Finance</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="solutions.html"><span class="menu-item-span">Solutions</span></a>
                            <ul class="sub-menu">
                                <li><a href="solutions/cyber-security.html">Cyber Security</a></li>
                                <li><a href="solutions/business-continuity.html">Business Continuity</a></li>
                                <li><a href="solutions/wireless.html">Wireless</a></li>
                                <li><a href="solutions/telephony.html">Telephony</a></li>
                                <li class="menu-item-has-children"><a href="solutions/cloud.html">Cloud</a>
                                    <ul class="sub-menu">
                                        <li><a href="solutions/cloud/office-365.html">Office 365</a></li>
                                        <li><a href="solutions/business-continuity.html#backups">Backups</a></li>
                                        <li><a href="solutions/cyber-security.html">Cyber Security</a></li>
                                        <li><a href="solutions/wireless.html">Wireless</a></li>
                                        <li><a href="solutions/telephony.html">Telephony</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="solutions/on-premise.html">On-Premise</a>
                                    <ul class="sub-menu">
                                        <li><a href="solutions/on-premise/network-infrastructure.html">Network Infrastructure</a></li>
                                        <li><a href="solutions/cyber-security.html">Cyber Security</a></li>
                                        <li><a href="solutions/wireless.html">Wireless</a></li>
                                        <li><a href="solutions/telephony.html">Telephony</a></li>
                                        <li><a href="solutions/on-premise/server-infrastructure.html">Server Infrastructure</a></li>
                                        <li><a href="solutions/on-premise/virtualisation.html">Virtualisation</a></li>
                                        <li><a href="solutions/business-continuity.html#backups">Backups</a></li>
                                    </ul>
                                </li>
                                <li><a href="services/it-support.html">Support</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="case-studies.html"><span class="menu-item-span">Case Studies</span></a>
                            <ul class="sub-menu">
                                <li class="menu-item-has-children"><a href="case-studies/industry.html">Industry</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children"><a href="case-studies/industry/tourism.html">Tourism</a>
                                            <ul class="sub-menu">
                                                <li><a href="case-studies/industry/tourism/caravan-park.html">Caravan Park</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="case-studies/technology.html">Technology</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children"><a href="case-studies/technology/meraki-wireless.html">Meraki Wireless</a>
                                            <ul class="sub-menu">
                                                <li><a href="case-studies/industry/tourism/caravan-park.html">Caravan Park</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="about.html"><span class="menu-item-span">About Us</span></a></li>
                        <li><a href="blog.html"><span class="menu-item-span">Blog</span></a></li>
                        <li><a href="contact-us.html"><span class="menu-item-span">Contact Us</span></a></li>
                        <li><a href="remote-support.html"><span class="menu-item-span" style="color:#f57421;">Remote Support</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="extra-nav">
                <ul>
                    <li class="nav-toggle"><a href="#" data-toggle="collapse" data-target=".inner-navigation"><span class="menu-item-span"><i class="ti-menu"></i></span></a></li>
                </ul>
            </div>
        </div>
    </header>
    <div class="wrapper">
        <section class="module-cover-slides fullscreen" data-module-cover-slides-options="{&quot;play&quot;:11000}">
            <ul class="slides-container">
                <li class="parallax" data-overlay="0.45"><img src="assets/images/dancers.jpg" alt="ITWORX Tango Dancers">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 strips">
                                <h1 class="m-b-10 first-header"><strong>One contact point <br> for all your <div class="orange">IT</div> needs</strong></h1>
                                <p class="m-b-20">
                                    <div class="orange">ITWORX</div> is a fully integrated IT & Communications consultancy </br>firm based in Aberdeen. We provide strategy, direction & advice </br>on communications, security and IT technologies.</p>
                                <p><a class="smoothscroll btn btn-lg btn-circle btn-warning" href="#findoutmore">Find out more</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="parallax text-center" data-overlay="0.7"><img src="assets/images/testimonial-1.jpg" alt="Testimonial from NorDan UK">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 strips">
                                <h2 class="m-b-10"><strong>"We see <div class="orange">ITWORX</div> as an essential part of our team"</strong></h2>
                                <p class="m-b-0">"They demonstrate great expertise, flexibility, professionalism and always have our best interests at heart. We see <span style="color:#f57421;">ITWORX</span> as an essential part of our team to help provide the platform for our business to grow."</p>
                                <p>- Gillian Irvine - Finance Director - NorDan UK</p>
                                <p><a class="smoothscroll btn btn-lg btn-circle btn-warning" href="#findoutmore">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="parallax text-center" data-overlay="0.7"><img src="assets/images/testimonial-2.jpg" alt="Testimonial from Waterloo Quay Properties">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 strips">
                                <h2 class="m-b-10"><strong>"<div class="orange">Positive thinking</div> topped with a generous sprinkling of '<div class="orange">can do</div>' attitude."</strong></h2>
                                <p class="m-b-0">"We rarely get anything other than a prompt, efficient and informative response from the
                                    <div class="orange">ITWORX</div> team, this being subtly reinforced by positive thinking topped with a </br>generous sprinkling of 'can do' attitude."</p>
                                <p>- Shaun Eardley - Director - Waterloo Quay Properties</p>
                                <p><a class="smoothscroll btn btn-lg btn-circle btn-warning" href="#findoutmore">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="parallax" data-overlay="0.7"><img src="assets/images/testimonial-3.jpg" alt="Testimonial from Bold St Media">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 strips">
                                <h2 class="m-b-10"><strong>"What would we do </br>without <div class="orange">ITWORX</div>?"</strong></h2>
                                <p class="m-b-0">"IT support which is on-hand quickly for guidance and providing </br>solutions to any IT challenges we are experiencing is crucial to ensure </br>we deliver the best service for our clients. <span style="color:#f57421;">ITWORX</span> deliver on this</br> for us time and time again."</p>
                                <p>- Eilidh McCluskie - Founder and Managing Director - Bold St Media</p>
                                <p><a class="smoothscroll btn btn-lg btn-circle btn-warning" href="#findoutmore">Find out more</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="parallax text-center" data-overlay="0.7"><img src="assets/images/testimonial-1.jpg" alt="Testimonial from Granite Occupational Health">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 strips">
                                <h2 class="m-b-10"><strong>"<div class="orange">ITWORX</div> have an endless amount of patience for their clients."</strong></h2>
                                <p class="m-b-0">"Always available to assist with any query, issue or urgent request for support that we have. A special thank you to Luke, who on a couple of occasions has come to our office within minutes of our phone calls to offer support and guidance."</p>
                                <p>- Jen McHugh - Director/Business Manager - Granite Occupational Health</p>
                                <p><a class="smoothscroll btn btn-lg btn-circle btn-warning" href="#findoutmore">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="slides-navigation"><a class="prev" href="#"><span class="ti-angle-left"></span></a><a class="next" href="#"><span class="ti-angle-right"></span></a></div>
        </section>
        <section class="module-lp bg-itworx m-auto" id="findoutmore">
            <div class="container">
                <div class="row strips">
                    <div class="col-md-12 text-center m-auto">
                        <p>ITWORX improve your business performance by making technology work smarter for you. Our valued people are here to drive positive business outcome through quality service delivery.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="module-lp-o">
            <div class="container">
                <div class="row row-post-masonry biggerfont">
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="about.html"><img src="assets/images/people.jpg" alt="People"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="about.html">People</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>Our highly skilled, friendly team can provide you with the expert advice and assistance you need to solve your tech problems. We make technology work smarter for you.</p>
                                    <p><a href="about.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="case-studies.html"><img src="assets/images/partnership.jpg" alt="Partnership"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="case-studies.html">Partnership</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>We pride ourselves on the relationships we have built with our clients and our partners, to ensure our clients receive the highest quality products and services on the market.</p>
                                    <p><a href="case-studies.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="solutions.html"><img src="assets/images/solutions.jpg" alt="Solutions"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="solutions.html">Solutions</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>We want to be a part of your business, to see it grow and succeed, which is why we will ensure our clients receive customised, affordable solutions that give optimum results.</p>
                                    <p><a href="solutions.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="video-container12">
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/wWlx_D2YjY0" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="space" data-MY="60px"></div>
                    </div>
                </div>
                <div class="row row-post-masonry biggerfont">
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="services/it-support.html"><img src="assets/images/it-support.jpg" alt="IT Support"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="services/it-support.html">IT Support</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>We understand every business has different IT support needs which is why we tailor our services to ensure your specific business requirements are met-leaving you to focus on your business.</p>
                                    <p><a href="services/it-support.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="solutions/cyber-security.html"><img src="assets/images/security.jpg" alt="Cyber Security"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="solutions/cyber-security.html">Cyber Security</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>At ITWORX our aim is to protect your business from Cyber Security threats. Using the latest technologies we have solutions to prevent, alert and remediate where appropriate.</p>
                                    <p><a href="solutions/cyber-security.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="solutions/business-continuity.html"><img src="assets/images/business-continuity.jpg" alt="Business Continuity"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="solutions/business-continuity.html">Business Continuity</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>With statistics continuing to show the risk of cyber-attack on the increase, it is important to ensure your business remains protected. Safeguard your data from file deletions to server failures.</p>
                                    <p><a href="solutions/business-continuity.html">Find out more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="module-lp bg-itworx m-auto">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center m-auto strips">
                        <h2 class="text-center officefontheaderw">Dundee</h2>
                        <p>ITWORX are excited to announce expansion of their IT and Communications Services into Dundee and Angus. Through our long-term experience in supporting and managing IT estates across SMB to corporate, the ITWORX team are on hand to provide expert advice and support to your local area from the North East through to Tayside.</p>
                        <p>Call our Dundee team on +44 (0) 1382 768 666</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="module-lp-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 m-auto text-center">
                        <h2 class="officefontheaderb">Latest Blog posts</h2>
                        <p class="stripsb">Find out about our latest tech tips and news.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="space" data-MY="60px"></div>
                    </div>
                </div>
                <div class="row row-post-masonry biggerfont">
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="blog/virtualrealitea.html"><img src="assets/images/virtualrealitea.jpg" alt="Teambuilding with virtualrealiTEA" title="Teambuilding with virtualrealiTEA"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="blog/virtualrealitea.html">Teambuilding with virtualrealiTEA</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>What an experience, who knew virtual reality would feel THAT real.</p>
                                    <p><a href="blog/virtualrealitea.html">Read more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="blog/datto-partner-of-the-year-uk-2019.html"><img src="assets/images/datto-partner-of-the-year-uk.jpg" alt="Datto Partner of the Year UK" title="ITWORX win Datto Partner of the Year UK"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="blog/datto-partner-of-the-year-uk-2019.html">ITWORX win Datto Partner of the Year UK.</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>ITWORX are proud to announce that we have won Datto Partner of the Year for UK and Ireland!</p>
                                    <p><a href="blog/datto-partner-of-the-year-uk-2019.html">Read more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 post-item">
                        <article class="post">
                            <div class="post-preview text-center">
                                <a href="blog/meet-luke-and-steven.html"><img src="assets/images/meet-luke-and-steven.jpg" title="Meet - Luke and Steven" alt="Luke and Steven - ITWORX Systems Engineers"></a>
                            </div>
                            <div class="post-wrapper">
                                <div class="post-header">
                                    <h3 class="post-title"><a href="blog/meet-luke-and-steven.html">Meet - Luke and Steven</a></h3>
                                </div>
                                <div class="post-content">
                                    <p>Watch our latest Meet the Team video, where two Systems Engineers go head to head in a battle of who knows each other best.</p>
                                    <p><a href="blog/meet-luke-and-steven.html">Read more</a></p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="module-lp bg-gray">
            <div class="container">
                <div class="row biggerfont">
                    <div class="col-md-4">
                        <div class="counter">
                            <div class="counter-icon"><span class="biggericon icon-earphones-alt icons"></span></div>
                            <div class="counter-number">
                                <h6><strong class="counter-timer" data-from="0" data-to="13492">0</strong></h6>
                            </div>
                            <div class="counter-title">Calls handled this year</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="counter">
                            <div class="counter-icon"><span class="biggericon icon-note icons"></span></div>
                            <div class="counter-number">
                                <h6><strong class="counter-timer" data-from="0" data-to="99">0</strong>%</h6>
                            </div>
                            <div class="counter-title">SLA's Achieved</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="counter">
                            <div class="counter-icon"><span class="biggericon icon-briefcase icons"></span></div>
                            <div class="counter-number">
                                <h6><strong class="counter-timer" data-from="0" data-to="253">0</strong></h6>
                            </div>
                            <div class="counter-title">Companies worked with</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="module-lp-cert bg-white no-gutters">
            <div class="container-fluid">
                <div class="row row-post-office">
                    <div class="col-md-2 col-sm-5 col-5 post-item">
                        <article class="post">
                            <div class="post-preview">
                                <a target="_blank" href="https://www.datto.com/uk/" rel="nofollow"><img src="assets/images/certifications/datto-blue.jpg" alt="Datto Blue Partner"></a>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-2 col-sm-5 col-5 post-item">
                        <article class="post">
                            <div class="post-preview">
                                <a target="_blank" href="https://www.cisco.com/c/en_uk/index.html" rel="nofollow"><img src="assets/images/certifications/cisco-premier-partner.jpg" alt="Cisco Premier Partner"></a>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 col-sm-10 col-10 offset-md-0 offset-sm-1 offset-1 post-item">
                        <article class="post">
                            <div class="post-preview">
                                <a target="_blank" href="https://www.microsoft.com/en-gb" rel="nofollow"><img src="assets/images/certifications/microsoft-partner.jpg" alt="Microsoft Partner"></a>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-2 col-sm-5 col-5 offset-md-0 offset-sm-1 offset-1 post-item">
                        <article class="post">
                            <div class="post-preview">
                                <a target="_blank" href="https://www.vmware.com/uk.html" rel="nofollow"><img src="assets/images/certifications/vmware-partner.jpg" alt="VMware Partner"></a>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-2 col-sm-5 col-5 post-item">
                        <article class="post">
                            <div class="post-preview">
                                <a target="_blank" href="https://www.highnet.com" rel="nofollow"><img src="assets/images/certifications/highnet-telecoms.jpg" alt="Highnet Telecoms"></a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer">
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <aside class="widget widget-text">
                                <div class="widget-title">
                                    <a href="services.html"><h4 class="h5">Services</h4></a>
                                </div>
                                <div class="textwidget">
                                    <a href="services/it-support.html">IT Support</br></a>
                                    <a href="services/equipment-supply.html">Equipment Supply</br></a>
                                    <a href="services/asset-disposal.html">Asset Disposal</br></a>
                                    <a href="services/data-connectivity.html">Data Connectivity</br></a>
                                    <a href="services/commercial-finance.html">Commercial/Finance</a>
                                </div>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="widget widget-recent-entries">
                                <div class="widget-title">
                                    <a href="solutions.html"><h4 class="h5">Solutions</h4></a>
                                </div>
                                <div class="textwidget">
                                    <a href="solutions/cyber-security.html">Cyber Security</br></a>
                                    <a href="solutions/business-continuity.html">Business Continuity</br></a>
                                    <a href="solutions/wireless.html">Wireless</br></a>
                                    <a href="solutions/telephony.html">Telephony</br></a>
                                    <a href="solutions/cloud.html">Cloud</br></a>
                                    <a href="solutions/on-premise.html">On-Premise</br></a>
                                    <a href="services/it-support.html">Support</a>
                                </div>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="widget widget-text">
                                <div class="widget-title">
                                    <h4 class="h5">Connect</h4>
                                </div>
                                <div class="break-footer">
                                    <p>
                                        Enquiries: </br><a href="mailto:enquiries@itworxuk.com">enquiries@<br class="d-none d-md-block d-lg-none">itworxuk.com</a></br>
                                        Support: </br><a href="mailto:support@itworxuk.com">support@<br class="d-none d-md-block d-lg-none">itworxuk.com</a></br>
                                        Recruitment: </br><a href="mailto:recruitment@itworxuk.com">recruitment@<br class="d-none d-md-block d-xl-none">itworxuk.com</a></br>
                                        <br>
                                    </p>
                                    <ul class="social-icons">
                                        <li><a href="https://en-gb.facebook.com/IT-Worx-Solutions-Ltd-304203459630635/" target="_blank" rel="nofollow"><i class="fab fa-facebook fa-2x"></i></a></li>
                                        <li><a href="https://twitter.com/itworxuk?lang=en" target="_blank" rel="nofollow"><i class="fab fa-twitter fa-2x"></i></a></li>
                                        <li><a href="https://www.linkedin.com/company/it-worx-uk/about/" target="_blank" rel="nofollow"><i class="fab fa-linkedin fa-2x"></i></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="widget widget-recent-works">
                                <div class="widget-title">
                                    <h5>IT-Worx Solutions Ltd</h5>
                                </div>
                                <div class="textwidget">
                                    <p>
                                        5-19 Holland Street
                                        <br> Aberdeen
                                        <br> AB25 3UJ
                                        <br> United Kingdom
                                        <br>
                                        <br> +44 (0) 1224 518 500</p>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>© IT-Worx Solutions Limited 2019 / <a href="privacy-policy.html">Privacy Policy</a> / <a href="terms-and-conditions.html">Terms and Conditions</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <!-- Wrapper end-->
    <!-- To top button--><a class="scroll-top" href="#top"><i class="fas fa-angle-up"></i></a>
    <!-- Scripts-->
    <script src="https://www.itworxuk.com/assets/js/custom/jquery.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="assets/js/custom/plugins.min.js"></script>
    <script src="assets/js/custom/custom.min.js"></script>
</body>

</html>