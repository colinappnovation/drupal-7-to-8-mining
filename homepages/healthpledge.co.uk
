<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="en" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!--><html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta name="msvalidate.01" content="661EAA0092C3D5725B6D895D53A85714" />
<link rel="shortcut icon" href="https://www.healthpledge.co.uk/sites/all/themes/healthpledge/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.healthpledge.co.uk/" />
<link rel="shortlink" href="https://www.healthpledge.co.uk/" />
<title>Health Pledge | Small Steps lead to Big Changes</title>
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_5pO73qc-z-zv4xoH8aIAp_Prq1thKg1qz9beR7eKaZg.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_ZuNHGfxZFTUtLgKo8Gf7qz7QfFWwkCVcBbZCJUSPZCs.css" media="all" />
<style>#sliding-popup.sliding-popup-bottom{background:#C0FF00;}#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text p{color:#5F2066 !important;}
</style>
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_MlDknpmQuqZbUwjhy0yN0toKZx_MBT167MOQAA6ZFvg.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_WkIQlS2V8h4Vh16HARhGTGX8u2nA_FzvuTZ5-DXLwaQ.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_dKaOqbTRqUbDXFgmYiLMnHANNB0xbd3jU5mBr_Se-tI.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_x5wW85zTQt3M02T8ypFoWawebSZ4u6k5zvLqex4SFlQ.css" media="only screen" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_4FhwkOMz4J7AeRAc6CgG9cWeeWhQJu0SE9OIyXGBGjY.css" media="screen" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_plfAkoc5exQ9sI9F6XDQq0-8ckxOziNCZjqxh78IUCs.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU.css" media="only screen" />

<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="https://www.healthpledge.co.uk/sites/default/files/css/css_oBlOIcLbrZzud003ohrRadTtBq5SOgEh_tcsRJLeZrA.css" media="screen" />
<![endif]-->
<style>#header{background:url('https://www.healthpledge.co.uk/sites/all/themes/healthpledge/header-images/HealthPledgeLogo-p.png') center top no-repeat   !important;height:284px;-o-background-size:auto auto;-webkit-background-size:auto auto;-khtml-background-size:auto auto;-moz-background-size:auto auto;background-size:auto auto;background-origin:border-box;}
</style>
<script src="https://www.healthpledge.co.uk/sites/default/files/js/js_qikmINIYTWe4jcTUn8cKiMr8bmSDiZB9LQqvceZ6wlM.js"></script>
<script src="https://www.healthpledge.co.uk/sites/default/files/js/js_0sVvj00hPP8-fUPlvKK0CnUtBMF6i2FOouDL91aHa1g.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-56572937-1", {"cookieDomain":"auto"});ga("send", "pageview");</script>
<script src="https://www.healthpledge.co.uk/sites/default/files/js/js_TWd_CxRitIh9W4g7EFj9znFXwcLersQxjFFgXCTrY_I.js"></script>
<script src="https://www.healthpledge.co.uk/sites/default/files/js/js_O6k7T-lOSpr-GChUfOQ_p5q8WLwaifNS2Y14UANXU8M.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"healthpledge","theme_token":"gdy3PeYZ5qsVKQY9TeXAlsqDbz-7MQhgHiiU3iPBPJ4","js":{"sites\/all\/modules\/eu-cookie-compliance\/js\/eu_cookie_compliance.js":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/libraries\/superfish\/jquery.hoverIntent.minified.js":1,"sites\/all\/libraries\/superfish\/sftouchscreen.js":1,"sites\/all\/libraries\/superfish\/sfsmallscreen.js":1,"sites\/all\/libraries\/superfish\/supposition.js":1,"sites\/all\/libraries\/superfish\/superfish.js":1,"sites\/all\/libraries\/superfish\/supersubs.js":1,"sites\/all\/modules\/superfish\/superfish.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/scalefix.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/outside-events.js":1,"sites\/all\/themes\/adaptivetheme\/at_core\/scripts\/menu-toggle.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"0":1,"sites\/all\/modules\/eu-cookie-compliance\/css\/eu_cookie_compliance.css":1,"sites\/all\/libraries\/superfish\/css\/superfish.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.headings.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.settings.style.image.css":1,"sites\/all\/themes\/adaptivetheme\/at_core\/css\/at.layout.css":1,"sites\/all\/themes\/healthpledge\/color\/colors.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.css":1,"sites\/all\/themes\/pixture_reloaded\/css\/pixture_reloaded.settings.style.css":1,"sites\/all\/themes\/healthpledge\/css\/healthpledge.css":1,"public:\/\/adaptivetheme\/healthpledge_files\/healthpledge.responsive.layout.css":1,"public:\/\/adaptivetheme\/healthpledge_files\/healthpledge.fonts.css":1,"public:\/\/adaptivetheme\/healthpledge_files\/healthpledge.menutoggle.css":1,"public:\/\/adaptivetheme\/healthpledge_files\/healthpledge.responsive.styles.css":1,"public:\/\/adaptivetheme\/healthpledge_files\/healthpledge.lt-ie9.layout.css":1,"1":1}},"lightbox2":{"rtl":0,"file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":true,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":false,"disable_resize":false,"disable_zoom":false,"force_show_nav":false,"show_caption":true,"loop_items":false,"node_link_text":"View Image Details","node_link_target":false,"image_count":"Image !current of !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":false,"useragent":"Python-urllib\/3.8"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":1,"popup_clicking_confirmation":1,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003E\tWe use cookies on this site to enhance your user experience\u003C\/h2\u003E\n\u003Cp\u003EBy clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button\u0022\u003EOK, I agree\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022\u003ENo, give me more info\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003E\tThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button\u0022\u003EHide\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003EMore info\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"http:\/\/www.healthpledge.co.uk\/privacy","popup_link_new_window":1,"popup_position":null,"popup_language":"en","domain":""},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/search\/node":true,"\/":true,"\/home?destination=node\/6":true},"superfish":{"1":{"id":"1","sf":{"delay":"1200","animation":{"opacity":"show","height":"show"},"speed":"\u0027normal\u0027","autoArrows":true,"dropShadows":true,"disableHI":false},"plugins":{"touchscreen":{"mode":"useragent_predefined"},"smallscreen":{"mode":"window_width","addSelected":false,"menuClasses":false,"hyperlinkClasses":false,"title":"Main menu"},"supposition":true,"bgiframe":false,"supersubs":{"minWidth":"12","maxWidth":"27","extraWidth":1}}}},"adaptivetheme":{"healthpledge":{"layout_settings":{"bigscreen":"three-col-grail","tablet_landscape":"three-col-grail","tablet_portrait":"one-col-vert","smalltouch_landscape":"one-col-vert","smalltouch_portrait":"one-col-stack"},"media_query_settings":{"bigscreen":"only screen and (min-width:1025px)","tablet_landscape":"only screen and (min-width:769px) and (max-width:1024px)","tablet_portrait":"only screen and (min-width:481px) and (max-width:768px)","smalltouch_landscape":"only screen and (min-width:321px) and (max-width:480px)","smalltouch_portrait":"only screen and (max-width:320px)"},"menu_toggle_settings":{"menu_toggle_tablet_portrait":"true","menu_toggle_tablet_landscape":"false"}}}});</script>
<!--[if lt IE 9]>
<script src="https://www.healthpledge.co.uk/sites/all/themes/adaptivetheme/at_core/scripts/html5.js?prgouk"></script>
<![endif]-->
</head>
<body class="html front not-logged-in two-sidebars page-node page-node- page-node-6 node-type-health-pledge-article site-name-hidden atr-7.x-3.x atv-7.x-3.0-rc1 site-name-health-pledge color-scheme-custom healthpledge bs-n bb-n mb-dd mbp-l rc-0">
  <div id="skip-link" class="nocontent">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="texture-overlay">
  <div id="page" class="container page ssc-n ssw-n ssa-l sss-n btc-n btw-b bta-l bts-n ntc-n ntw-b nta-l nts-n ctc-n ctw-b cta-l cts-n ptc-n ptw-b pta-l pts-n at-mt">

    <header  id="header" class="clearfix" role="banner">
      <div class="header-inner clearfix">

                  <!-- start: Branding -->
          <div  id="branding" class="branding-elements clearfix">

            
                          <!-- start: Site name and Slogan hgroup -->
              <div  class="h-group" id="name-and-slogan">

                                  <h1 class="element-invisible" id="site-name"><a href="/" title="Home page" class="active">Health Pledge</a></h1>
                
                                  <p  id="site-slogan">Small Steps lead to Big Changes</p>
                
              </div><!-- /end #name-and-slogan -->
            
          </div><!-- /end #branding -->
        
        
      </div>

    </header> <!-- /header -->

    <div id="menu-bar" class="nav clearfix"><nav id="block-superfish-1" class="block block-superfish menu-wrapper menu-bar-wrapper clearfix at-menu-toggle odd first last block-count-1 block-region-menu-bar block-1" >  
      <h2 class="element-invisible block-title">Main menu</h2>
  
  <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-9 sf-parent-items-3 sf-single-items-6"><li id="menu-218-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="/" class="sf-depth-1 active">Home</a></li><li id="menu-657-1" class="middle even sf-item-2 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent"><a href="/health-pledge-big-idea" title="All about Health Pledge and how it started" class="sf-depth-1 menuparent">The Big Idea</a><ul><li id="menu-1000-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/health-pledge-team" class="sf-depth-2">The Health Pledge Team</a></li><li id="menu-999-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/privacy" class="sf-depth-2">Privacy</a></li><li id="menu-1013-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/frequently-asked-questions" class="sf-depth-2">Frequently Asked Questions</a></li><li id="menu-1087-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/mayor-tamesides-health-pledge" class="sf-depth-2">Mayor of Tameside&#039;s Health Pledge</a></li><li id="menu-1014-1" class="last odd sf-item-5 sf-depth-2 sf-no-children"><a href="/video-pledges" class="sf-depth-2">Video Pledges</a></li></ul></li><li id="menu-1161-1" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="/success-stories" class="sf-depth-1">Success Stories</a></li><li id="menu-653-1" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="/health-pledges" title="What is a Health Pledge and how to make one" class="sf-depth-1">How to pledge</a></li><li id="menu-1001-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="/group-pledges" title="Tips for group organisers who wish to use Health Pledge in their group" class="sf-depth-1">Group Pledges</a></li><li id="menu-658-1" class="middle even sf-item-6 sf-depth-1 sf-total-children-5 sf-parent-children-0 sf-single-children-5 menuparent"><a href="/tips-small-steps" title="The Small Steps you can take to make a Big Change to your health" class="sf-depth-1 menuparent">Tips - The Small Steps</a><ul><li id="menu-807-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="/tips-lifestyle" class="sf-depth-2">Tips - Lifestyle</a></li><li id="menu-735-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="/tips-food-drink" class="sf-depth-2">Tips - Food &amp; Drink</a></li><li id="menu-808-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="/tips-activity" class="sf-depth-2">Tips - Activity</a></li><li id="menu-948-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="/tips-random" class="sf-depth-2">Tips - Random</a></li><li id="menu-1269-1" class="last odd sf-item-5 sf-depth-2 sf-no-children"><a href="/how-stop-procrastinating-using-%E2%80%9C2-minute-rule%E2%80%9D" class="sf-depth-2">How to Stop Procrastinating the “2-Minute Rule”</a></li></ul></li><li id="menu-645-1" class="middle odd sf-item-7 sf-depth-1 sf-total-children-12 sf-parent-children-0 sf-single-children-12 menuparent"><a href="/resources" title="Resources for groups wanting to join in with or promote Health Pledge" class="sf-depth-1 menuparent">Resources</a><ul><li id="menu-2223-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Self%20Care%20week%20recipes.pdf" title="Recipes from MMU student nurses for self care week" class="sf-depth-2">Self Care Week Recipes</a></li><li id="menu-1515-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health%20Pledge%20for%20Patient%20Participaion%20Groups.pdf" title="How to run a Health Pledge campaign at your PPG" class="sf-depth-2">Guidance for PPGs</a></li><li id="menu-1516-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health%20Pledge%20for%20Groups.pdf" title="How to run a Health Pledge campaign for your group" class="sf-depth-2">Guidance for groups</a></li><li id="menu-1002-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health_Pledge_Slips.pdf" title="Health Pledge Slips in pdf format for you to make your Health Pledge on" class="sf-depth-2">Health Pledge Slips</a></li><li id="menu-1016-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/docs/Health_Pledge_Launch_Press_Release.pdf" title="The Press Release for the Health Pledge Launch on 22nd Nov 2014" class="sf-depth-2">Launch Press Release (pdf)</a></li><li id="menu-879-1" class="middle even sf-item-6 sf-depth-2 sf-no-children"><a href="/health-pledge-logos" class="sf-depth-2">Health Pledge logos</a></li><li id="menu-647-1" class="middle odd sf-item-7 sf-depth-2 sf-no-children"><a href="/support-links" class="sf-depth-2">Links</a></li><li id="menu-1088-1" class="middle even sf-item-8 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health_Pledge_Flyer_v3.pdf" title="A leaflet about Health Pledge that you can download to pass on to others" class="sf-depth-2">Health Pledge Flyer</a></li><li id="menu-1162-1" class="middle odd sf-item-9 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health_Pledge_Flyer_with_pledge_slipV2.pdf" title="A flyer explaining Health Pledge including a pledge slip" class="sf-depth-2">Health Pledge flyer with pledge slip</a></li><li id="menu-1233-1" class="middle even sf-item-10 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/Health_Pledge_poster.pdf" title="" class="sf-depth-2">Health Pledge Poster</a></li><li id="menu-1089-1" class="middle odd sf-item-11 sf-depth-2 sf-no-children"><a href="http://www.healthpledge.co.uk/sites/default/files/docs/Do_Pledges_Work.pdf" title="" class="sf-depth-2">Do Pledges Work? (pdf)</a></li><li id="menu-1160-1" class="last even sf-item-12 sf-depth-2 sf-no-children"><a href="http://jamesclear.com/marginal-gains" title="" class="sf-depth-2">Small Steps - Marginal Gains at British Cycling</a></li></ul></li><li id="menu-998-1" class="middle even sf-item-8 sf-depth-1 sf-no-children"><a href="/health-pledge-blog" title="The latest news from the Health Pledge Team" class="sf-depth-1">Blog</a></li><li id="menu-991-1" class="last odd sf-item-9 sf-depth-1 sf-no-children"><a href="/contact" title="Contact the Health Pledge Team" class="sf-depth-1">Contact us</a></li></ul>
  </nav></div>
    <!-- Messages and Help -->
        
    <!-- Breadcrumbs -->
    
    
    <!-- Three column 3x33 Gpanel -->
    
    <div id="columns">
      <div class="columns-inner clearfix">

        <div id="content-column">
          <div class="content-inner">

            
            <section id="main-content" role="main">

                                            <header  id="main-content-header" class="clearfix">

                                      <h1 id="page-title">Welcome to Health Pledge</h1>
                  
                  
                </header>
                            
                              <div id="content">
                  <div id="block-system-main" class="block block-system no-title odd first last block-count-2 block-region-content block-main" >  
  
  <article id="node-6" class="node node-health-pledge-article node-promoted article odd node-full ia-r clearfix" about="/home" typeof="sioc:Item foaf:Document" role="article">
  
  
  
  <div class="node-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden view-mode-full"><div class="field-items"><div class="field-item even" property="content:encoded"><p class="rtecenter"><a href="http://www.healthpledge.co.uk/pledge"><img alt="" src="/sites/default/files/ClickHereToPledge.png" /></a></p>
<p>Health Pledge is a campaign led by ordinary people.</p>
<p>We are <strong><em>not </em></strong>doing this on behalf of the NHS, medical professionals, politicians or civil servants.</p>
<p>We are hoping to inspire people to take action to improve their health.</p>
<p>The campaign aims to encourage and support people to take small steps to make big improvements.</p>
<p><span>Health Pledge is an initiative of Thornley House Patient Participation Group. It is the brainchild of Ingrid Brindle who assembled a crack team to bring it to life</span>. The team is pictured below, from the left Dominic Sexton, Jacqui Gladwin and Ingrid Brindle.</p>
<p><a href="/sites/default/files/HealthPledgeTeam2016-2L.jpg" rel="lightbox"><img alt="" src="/sites/default/files/HealthPledgeTeam2016-2s.jpg" /></a></p>
<p>
	We have worked very hard to put the campaign together and hope you will reward us with your support.</p>
<h3>
	Supporters</h3>
<div>
	The Health Pledge Team would like to thank the following for their support in the campaign.</div>
<div>
	 </div>
<div>
	<a href="http://htmc.co.uk/">Haughton Thornley Medical Centres</a></div>
<div>
	 </div>
<div>
	<a href="http://www2.mmu.ac.uk/nursing/">Manchester Metropolitan University's Department of Nursing</a></div>
<div>
	 </div>
<div>
	<a href="http://your.morrisons.com/store-finder/store-details/?recordid=119">Morrisons, Hyde</a></div>
<div>
	 </div>
<div>
	<a href="http://www.pdprint.com/">PD Print</a></div>
<div>
	 </div>
<div>
	Tameside Health and Wellbeing Fund</div>
<div>
	 </div>
<div>
	Dave leach for help with the art work</div>
<div>
	 </div>
<div>
	<a href="http://www.cvat.org.uk/">Community and Voluntary Action Tameside</a></div>
<h3>
	Disclaimer</h3>
<p>Information provided on this website is available only as guidance. The Health Pledge team are not registered health professionals and therefore if you have any concerns regarding your health or the advice provided please check with your local health care provider. If you have an existing health condition you should discuss any specific changes you would like to make with the health professional responsible for your care prior to implementing them.</p>
<p> </p>
</div></div></div>  </div>

  
  
  <span property="dc:title" content="Welcome to Health Pledge" class="rdf-meta element-hidden"></span></article>

  </div>                </div>
              
              <!-- Feed icons (RSS, Atom icons etc -->
              
            </section> <!-- /main-content -->

            
          </div>
        </div> <!-- /content-column -->

        <div class="region region-sidebar-first sidebar"><div class="region-inner clearfix"><nav id="block-menu-menu-pledges" class="block block-menu odd first block-count-3 block-region-sidebar-first block-menu-pledges"  role="navigation"><div class="block-inner clearfix">  
      <h2 class="block-title">Health Pledges</h2>
  
  <div class="block-content content"><ul class="menu clearfix"><li class="first leaf menu-depth-1 menu-item-655"><a href="/pledge" title="">Make your Health Pledge</a></li><li class="last leaf menu-depth-1 menu-item-656"><a href="/view-health-pledges" title="">View Health Pledges</a></li></ul></div>
  </div></nav><div id="block-search-form" class="block block-search no-title even block-count-4 block-region-sidebar-first block-form"  role="search"><div class="block-inner clearfix">  
  
  <div class="block-content content"><form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="search" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-huVQyeekFMI9lO9YjbBj3IVNESMxy0V9-hSepbw4xlU" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div></div><section id="block-user-login" class="block block-user odd last block-count-5 block-region-sidebar-first block-login"  role="form"><div class="block-inner clearfix">  
      <h2 class="block-title">User login</h2>
  
  <div class="block-content content"><form action="/home?destination=node/6" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list"><ul><li class="even first"><a href="/user/register" title="Create a new user account.">Create new account</a></li><li class="odd last"><a href="/user/password" title="Request new password via e-mail.">Request new password</a></li></ul></div><input type="hidden" name="form_build_id" value="form-Ms3CCIMurp6-ko25L5kRAzZ-dT6L-2_ckDzDHf3jUW0" />
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Log in" class="form-submit" /></div></div></form></div>
  </div></section></div></div>        <div class="region region-sidebar-second sidebar"><div class="region-inner clearfix"><section id="block-views-tips-block-block" class="block block-views odd first block-count-6 block-region-sidebar-second block-tips-block-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Tip of the day</h2>
  
  <div class="block-content content"><div class="view view-tips-block view-id-tips_block view-display-id-block view-dom-id-6941002968071b1c19287d89a8a97d28">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/if-you-have-asthma">If you have asthma</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div>
  </div></section><section id="block-views-pledge-block-block" class="block block-views even block-count-7 block-region-sidebar-second block-pledge-block-block" ><div class="block-inner clearfix">  
      <h2 class="block-title">Latest Health Pledges</h2>
  
  <div class="block-content content"><div class="view view-pledge-block view-id-pledge_block view-display-id-block view-dom-id-306a12dcb241a7666aa95a79aa1a020d">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/get-well-and-active">Get well and active</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/be-alcohol-free-two-months-and-lose-least-two-stone">To be alcohol free for two months and to lose at least two stone</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/give-alcohol-two-months">Give up alcohol for two months</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/920-communications">9:20 Communications</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/striding-out-support-others-and-myself">Striding out to support others and myself</a></span>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/i-pledge-drink-more-bottled-water">I pledge to drink more bottled water</a></span>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/driver-my-life">Driver of my life.</a></span>  </div>  </div>
  <div class="views-row views-row-8 views-row-even views-row-last">
      
  <div class="views-field views-field-title">        <span class="field-content"><a href="/making-time-selfcare">Making time for selfcare</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></div>
  </div></section><div id="block-block-1" class="block block-block no-title odd last block-count-8 block-region-sidebar-second block-1" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p class="rtecenter"><a href="https://www.facebook.com/healthpledge"><img alt="" src="/sites/default/files/asset.find_.us_.on_.facebook.lg_.png" /></a></p>
<p class="rtecenter"> <a href="http://twitter.com/HealthPledgeUK"><img alt="" src="/sites/default/files/twitter.png" /></a> <a href="http://twitter.com/HealthPledgeUK">@HealthPledgeUK</a></p>
</div>
  </div></div></div></div>
      </div>
    </div> <!-- /columns -->

    
    <!-- four-4x25 Gpanel -->
    
          <footer  id="footer" class="clearfix" role="contentinfo">
        <div id="footer-inner" class="clearfix">
          <div class="region region-footer"><div class="region-inner clearfix"><div id="block-block-2" class="block block-block no-title odd first last block-count-9 block-region-footer block-2" ><div class="block-inner clearfix">  
  
  <div class="block-content content"><p><img alt="It's for every body footer image" src="/sites/default/files/images/HealthPledgeFooterImage.png" /></p>
<p>All content is copyright the Health Pledge Team © 2014-2016 The content and design of this website may not be reproduced in whole or part, all rights reserved.</p>
<p>Site design and hosting by <a href="http://matley.co.uk">Matley</a></p>
</div>
  </div></div></div></div>          <p class="attribute-creator"></p>
        </div>
      </footer>
    
  </div> <!-- /page -->
</div> <!-- /texture overlay -->
  <script src="https://www.healthpledge.co.uk/sites/default/files/js/js_paiTaEsc1HNeBS2rkLc1NrprKrFnApv70kuY8awAaTI.js"></script>
</body>
</html>
