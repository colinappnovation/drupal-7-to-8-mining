<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <!--[if IE]><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"0deacef229",applicationID:"14221471"};window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(3),u=e(4),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],t),e}finally{f.emit("fn-end",[c.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e,n){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now(),!1,n])}},{}],2:[function(e,n,t){function r(e,n){if(!o)return!1;if(e!==o)return!1;if(!n)return!0;if(!i)return!1;for(var t=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var u=navigator.userAgent,f=u.match(a);f&&u.indexOf("Chrome")===-1&&u.indexOf("Chromium")===-1&&(o="Safari",i=f[1])}n.exports={agent:o,version:i,match:r}},{}],3:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],4:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],5:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=v(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){h[e]=v(e).concat(n)}function m(e,n){var t=h[e];if(t)for(var r=0;r<t.length;r++)t[r]===n&&t.splice(r,1)}function v(e){return h[e]||[]}function g(e){return p[e]=p[e]||o(t)}function w(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(3),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!E++){var e=x.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+x.offset],null,"api");var t=l.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===l.readyState&&i()}function i(){f("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-x.offset}var u=(new Date).getTime(),f=e("handle"),c=e(3),s=e("ee"),p=e(2),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1130.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:u,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};e(1),l[m]?(l[m]("DOMContentLoaded",i,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",o),d[v]("onload",r)),f("mark",["firstbyte",u],null,"api");var E=0,O=e(5)},{}]},{},["loader"]);</script>
<link rel="apple-touch-icon" sizes="180x180" href="/o2forumkentishtown/sites/default/files/favicons/apple-touch-icon.png"/>
<link rel="icon" type="image/png" sizes="32x32" href="/o2forumkentishtown/sites/default/files/favicons/favicon-32x32.png"/>
<link rel="icon" type="image/png" sizes="16x16" href="/o2forumkentishtown/sites/default/files/favicons/favicon-16x16.png"/>
<link rel="manifest" href="/o2forumkentishtown/sites/default/files/favicons/site.webmanifest"/>
<link rel="mask-icon" href="/o2forumkentishtown/sites/default/files/favicons/safari-pinned-tab.svg" color="#012d6b"/>
<meta name="msapplication-TileColor" content="#012d6b"/>
<meta name="theme-color" content="#012d6b"/>
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://academymusicgroup.com/" />
<link rel="shortlink" href="https://academymusicgroup.com/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://academymusicgroup.com/" />
<meta name="twitter:title" content="Academy Music Group" />
<meta name="twitter:description" content="Academy Music Group" />
  <title>Academy Music Group | Academy Music Group</title>

  <!--  <link href="https://unpkg.com/basscss@8.0.2/css/basscss.min.css" rel="stylesheet">-->

  <link type="text/css" rel="stylesheet" href="//academymusicgroup.com/sites/default/files/css/css_aGqMcQ-1Apd5VXfyHYCEuo18WXax3XYJ0xG9XURLKyQ.css" media="all" />
<link type="text/css" rel="stylesheet" href="//academymusicgroup.com/sites/default/files/css/css_8x7uRwI4cuX08OVwhXCdrERsLTDg9WVpez3QJ9wdvRo.css" media="all" />
<link type="text/css" rel="stylesheet" href="//academymusicgroup.com/sites/default/files/css/css_HwOp0qPxhozgdOTZTtXeOFw77DSqYSJf_byvpOeldyU.css" media="all" />
<link type="text/css" rel="stylesheet" href="//academymusicgroup.com/sites/default/files/css/css_OjN7fyRePgau3at58ffIplCD6_Tx5gPEFcVtHYDWt2Y.css" media="all" />
<link type="text/css" rel="stylesheet" href="//academymusicgroup.com/sites/default/files/css/css_47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU.css" media="print" />


    <script type="text/javascript">
    //uncomment and change this to false if you're having trouble with WOFFs
    // var woffEnabled = false;
    // to place your webfonts in a custom directory
    // uncomment this and set it to where your webfonts are.
    var customPath = "https://academymusicgroup.com/sites/all/themes/AMG/fonts";
  </script>


  <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous" ></script>


  <script type="text/javascript" src="//academymusicgroup.com/sites/default/files/js/js_yoJLjDOPLK3xe2wzX1cg0yL-vYAB72E9qIt0dz_9wa8.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='/sites/all/modules/contrib/jquery_update/replace/jquery/1.8/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<script type="text/javascript" src="//academymusicgroup.com/sites/default/files/js/js_dWhBODswdXXk1M5Z5nyqNfGljmqwxUwAK9i6D0YSDNs.js"></script>
<script type="text/javascript" src="//academymusicgroup.com/sites/default/files/js/js_5eyyqd1-Lx5w-gJAfeM7ZAxC5ESwa_zupyS80-93sUY.js"></script>
<script type="text/javascript" src="//academymusicgroup.com/sites/default/files/js/js_puU1BORPIq8m5i06KWlxmLH4o8atD2nxU2JCsiJ6if4.js"></script>
<script type="text/javascript" src="//academymusicgroup.com/sites/default/files/js/js_wIrrr8PUrtcDPCxlTVWl8Afg8TL_XQKfbByJ-KKsoc0.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"amg","theme_token":"S8IYjVrnsIOBsXwnT_F7e2MmUTYkccyPJMg7ZeaNHUQ","js":{"public:\/\/google_tag\/google_tag.script.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.8.3\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/lazyloader\/jquery.lazyloader.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/themes\/AMG\/js\/build\/AMG_Frutiger_JS.js":1,"sites\/all\/themes\/AMG\/js\/build\/jquery.mmenu.min.all.js":1,"sites\/all\/themes\/AMG\/js\/libs\/selectize.min.js":1,"sites\/all\/themes\/AMG\/js\/build\/modernizr.js":1,"sites\/all\/themes\/AMG\/js\/build\/jquery.matchHeight.js":1,"sites\/all\/themes\/AMG\/js\/build\/tmads.min.js":1,"sites\/all\/themes\/AMG\/js\/build\/polyfill.object-fit.min.js":1,"sites\/all\/themes\/AMG\/js\/build\/respond.min.js":1,"sites\/all\/themes\/AMG\/js\/build\/jquery.validate.js":1,"sites\/all\/themes\/AMG\/js\/build\/jquery.textfill.min.js":1,"sites\/all\/themes\/AMG\/js\/build\/scripts.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/calendar\/css\/calendar_multiday.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/domain\/domain_nav\/domain_nav.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/contrib\/footer_sitemap\/footer_sitemap.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/user_prune\/css\/user_prune.css":1,"sites\/all\/modules\/contrib\/youtube\/css\/youtube.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/lazyloader\/lazyloader.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/themes\/AMG\/layouts\/amg_landing\/amg-landing.css":1,"sites\/all\/themes\/AMG\/styles\/css\/AMG_Frutiger.css":1,"sites\/all\/themes\/AMG\/fonts\/styles.css":1,"sites\/all\/themes\/AMG\/styles\/css\/normalize.css":1,"sites\/all\/themes\/AMG\/styles\/basscss\/css\/basscss.min.css":1,"sites\/all\/themes\/AMG\/styles\/css\/jquery.mmenu.all.css":1,"sites\/all\/themes\/AMG\/styles\/css\/selectize.css":1,"sites\/all\/themes\/AMG\/styles\/css\/polyfill.object-fit.css":1,"sites\/all\/themes\/AMG\/styles\/css\/style.css":1,"sites\/all\/themes\/AMG\/styles\/css\/print.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"currentPath":"amg-landing","currentPathIsAdmin":false});
//--><!]]>
</script>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
</head>
<body class="html front not-logged-in no-sidebars page-amg-landing domain-academymusicgroup-com role-anonymous-user" >

  <noscript aria-hidden="true"><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKRSJJC" height="0" width="0" style="display:none;visibility:hidden" title="Google Tag Manager">Google Tag Manager</iframe></noscript>
<!-- ADVERTISING -->
<div id="ad-slot-501" class="ad-wrapper"></div>

<div id="page" class="page">

  <div id="mobile-nav" class="p2 mt4">
            <div>
      <!-- @TODO This is trying to pull in some secondary nav 'secondary-m' undefined -->
      <ul class="menu amg-user-menu"><li><a href="/user/api/register">Register</a></li>
      <li><a href="/user/api/login">Sign in</a></li></ul>          </div>
      </div>

  <div class="wrapper clearfix">
        
<header id="header" class="col col-12">

  <div class="top-links col col-12 p2 mt2">
    <ul class="social-media left mb0">
              <li>
          <a href="http://facebook.com/AcademyMusicGroup" target="_blank">
            <i class="icon-facebook"></i>
          </a>
        </li>
                    <li>
          <a href="http://twitter.com/academyamg" target="_blank">
            <i class="icon-twitter"></i>
          </a>
        </li>
                    <li>
          <a href="http://instagram.com/academymusicgroup" target="_blank">
            <i class="icon-instagram"></i>
          </a>
        </li>
          </ul>
    <div class="right">
              <ul class="menu amg-user-menu"><li><a href="/user/api/register">Register</a></li>
      <li><a href="/user/api/login">Sign in</a></li></ul>          </div>
  </div>


  <div class="col col-12 py2 top-head">
    <div class="lg-hide col col-1">
      <a href="#mobile-nav" class="left menu-icon">
        <span class="mm1"></span>
        <span class="mm2"></span>
        <span class="mm3"></span>
      </a>
    </div>
                <a href="/" title="Academy Music Group"
         rel="home" id="logo" class="col col-10 lg-col-3">
        <img src="https://academymusicgroup.com/sites/all/themes/AMG/logo.png" alt="Academy Music Group"/>
      </a>
    
        <div id="desktop-nav" class="col col-7 lg-show">
          </div>
            <div id="header-region" class="col col-2 right search-bar py2">
      <div class="block" data-bid="43">
        <form action="https://academymusicgroup.com/search/" method="post"
              id="search-block-form" accept-charset="UTF-8">
          <div>
            <div class="container-inline">
              <h2 class="element-invisible">Search form</h2>
              <div
                  class="form-item form-type-textfield form-item-search-block-form">
                <label class="element-invisible"
                       for="edit-search-block-form--2">Search </label>
                <input title="Enter the terms you wish to search for."
                       placeholder="Search" type="text"
                       id="edit-search-block-form--2" name="search_block_form"
                       size="40" maxlength="128"
                       value=""
                       class="form-text">
              </div>
              <div class="form-actions form-wrapper" id="edit-actions">
                <input type="image" id="edit-submit" name="submit"
                       src="https://academymusicgroup.com/sites/all/themes/AMG/images/source/search-icon.png"
                       class="form-submit">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="lg-hide col col-1">
      <a class="icon-search right"></a>
    </div>

  </div>

</header>
    
        <div id="main" class="col col-12">
      <div class="col col-12">
        <section id="content">

          
            
            
                                    
            
          
          <div id="content-area">
              
<div class="panel-display panel-amg-landing clearfix" >
    <!-- Top hero block  -->
    <div class="inside">
        <div class="homepage-panel">
            <div class="panel-pane pane-fieldable-panels-pane pane-current-38 pane-bundle-hero-panel"  >
  
  
  <div class="pane-content">
    <div class="fieldable-panels-pane">
    <div class="field field-name-field-panel-hero-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="//academymusicgroup.com/sites/default/files/JackGarratt_ReliveTheNight-25-2.jpg" width="3000" height="2000" alt="" /></div></div></div><div class="field field-name-field-corporate-button field-type-link-field field-label-above"><div class="field-label">Corporate Button	:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="https://academymusicgroup.com/company">Visit Company Site</a></div></div></div><div class="field field-name-field-venues-button field-type-link-field field-label-above"><div class="field-label">Venues Button:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="https://academymusicgroup.com/venues">Looking to see what's on at our venues?</a></div></div></div><div class="field field-name-field-logo field-type-image field-label-above"><div class="field-label">Logo:&nbsp;</div><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="//academymusicgroup.com/sites/default/files/AMG-white.png" width="1500" height="605" alt="" /></div></div></div></div>
  </div>

  
  </div>
        </div>
    </div>
</div>
          </div>

          
        </section> 
      </div>
    </div>
    
              
<footer id="footer">
  <div class="col col-12">
    <div class="col col-12 md-col-3 left-col p2 footer-panel">
              <a href="/" title="Academy Music Group"
           rel="home" id="logo" class="p2 center col col-12 sm-col-12 px2 py3">
          <img src="https://academymusicgroup.com/sites/all/themes/AMG/logo.png" alt="Academy Music Group"/>
        </a>
            <div class="col col-12 px2 mb3">

        

        <p>

          Academy Music Group Limited          <br/><br/>
          <!-- Registered Address -->
                        Company number: 3463738<br/>
              VAT number: 710648159<br/>
              Registered in England and Wales<br/>
              211 Stockwell Road<br/>
              London SW9 9SL<br/>
              

        </p>
      </div>
    </div>
    <div class="col col-12 md-col-9 right-col footer-panel">
      <div class="col col-12 p2 footer-menus menu-footer-links">
        <ul class="menu"><li class="first expanded partners mid-1051"><a href="/" title="" class="active">Partners</a><ul class="menu"><li class="first leaf o2 mid-1052"><a href="http://www.o2.co.uk/" target="_blank">O2</a></li>
<li class="leaf tuborg mid-2549"><a href="http://www.tuborg.com/" target="_blank">Tuborg</a></li>
<li class="leaf pepsi mid-2550"><a href="http://www.pepsi.com/" target="_blank">Pepsi</a></li>
<li class="leaf barclaycard mid-9916"><a href="https://www.barclaycard.co.uk/entertainment/barclaycard-entertainment.html">Barclaycard</a></li>
<li class="leaf ticketmaster mid-2553"><a href="https://www.ticketmaster.co.uk/" target="_blank">Ticketmaster</a></li>
<li class="leaf amazon-music mid-11286"><a href="https://www.amazon.co.uk/gp/dmusic/promotions/AmazonMusicUnlimited/?tag=livenationukweb-21">Amazon Music</a></li>
<li class="leaf badoo mid-11337"><a href="https://bdo.to/u/3dXwwJI">Badoo</a></li>
<li class="last leaf uber mid-9917"><a href="https://www.uber.com/en-GB/">Uber</a></li>
</ul></li>
<li class="expanded charity mid-3759"><a href="/" title="" class="active">Charity</a><ul class="menu"><li class="first last leaf teenage-cancer-trust mid-3750"><a href="https://www.teenagecancertrust.org/" target="_blank">Teenage Cancer Trust</a></li>
</ul></li>
<li class="expanded sitemap mid-1053"><a href="/" target="_self" class="active">Sitemap</a><ul class="menu"><li class="first leaf faqs mid-3727"><a href="/faq" target="_self">FAQs</a></li>
<li class="last leaf work-with-us mid-3729"><a href="https://www.academymusicgroup.com/casual-work" target="_self">Work With Us</a></li>
</ul></li>
<li class="last expanded legal mid-3758"><a href="/" target="_self" class="active">Legal</a><ul class="menu"><li class="first leaf terms-of-use mid-3731"><a href="/terms-use" target="_self">Terms of Use</a></li>
<li class="leaf privacy-policy mid-3732"><a href="/privacy-policy" target="_self">Privacy Policy</a></li>
<li class="leaf cookies-policy mid-4474"><a href="/cookies-policy" target="_self">Cookies Policy</a></li>
<li class="leaf modern-slavery-statement mid-9817"><a href="/modern-slavery-statement">Modern Slavery Statement</a></li>
<li class="last leaf sustainability-charter mid-10971"><a href="/sustainability-charter">Sustainability Charter</a></li>
</ul></li>
</ul>      </div>


      <div class="col col-12 p2 footer-menus menu-venue-list">
        <ul class="menu"><li class="first expanded our-venues mid-10825"><a href="/" target="_self" class="active">Our Venues</a><ul class="menu"><li class="first leaf arts-club-liverpool mid-10826"><a href="https://academymusicgroup.com/artsclubliverpool/">Arts Club Liverpool</a></li>
<li class="leaf o2-abc-glasgow mid-10827"><a href="https://academymusicgroup.com/o2abcglasgow" target="_self">O2 ABC Glasgow</a></li>
<li class="leaf o2-academy-birmingham mid-10828"><a href="https://academymusicgroup.com/o2academybirmingham" target="_self">O2 Academy Birmingham</a></li>
<li class="leaf o2-academy-bournemouth mid-10829"><a href="https://academymusicgroup.com/o2academybournemouth" target="_self">O2 Academy Bournemouth</a></li>
<li class="leaf o2-academy-bristol mid-10830"><a href="https://academymusicgroup.com/o2academybristol" target="_self">O2 Academy Bristol</a></li>
<li class="leaf o2-academy-brixton mid-10831"><a href="https://www.academymusicgroup.com/o2academybrixton" target="_self">O2 Academy Brixton</a></li>
<li class="leaf o2-academy-glasgow mid-10832"><a href="https://academymusicgroup.com/o2academyglasgow" target="_self">O2 Academy Glasgow</a></li>
<li class="leaf o2-academy-islington mid-10833"><a href="https://academymusicgroup.com/o2academyislington" target="_self">O2 Academy Islington</a></li>
<li class="leaf o2-academy-leeds mid-10834"><a href="https://academymusicgroup.com/o2academyleeds" target="_self">O2 Academy Leeds</a></li>
<li class="leaf o2-academy-leicester mid-10835"><a href="https://academymusicgroup.com/o2academyleicester" target="_self">O2 Academy Leicester</a></li>
<li class="leaf o2-academy-liverpool mid-10836"><a href="https://academymusicgroup.com/o2academyliverpool" target="_self">O2 Academy Liverpool</a></li>
<li class="leaf o2-academy-newcastle mid-10837"><a href="https://academymusicgroup.com/o2academynewcastle" target="_self">O2 Academy Newcastle</a></li>
<li class="leaf o2-academy-oxford mid-10838"><a href="https://academymusicgroup.com/o2academyoxford" target="_self">O2 Academy Oxford</a></li>
<li class="leaf o2-academy-sheffield mid-10839"><a href="https://academymusicgroup.com/o2academysheffield" target="_self">O2 Academy Sheffield</a></li>
<li class="leaf o2-city-hall-newcastle mid-10840"><a href="https://academymusicgroup.com/o2cityhallnewcastle">O2 City Hall Newcastle</a></li>
<li class="leaf o2-forum-kentish-town mid-10841"><a href="https://academymusicgroup.com/o2forumkentishtown">O2 Forum Kentish Town</a></li>
<li class="leaf o2-institute-birmingham mid-10842"><a href="https://academymusicgroup.com/o2institutebirmingham">O2 Institute Birmingham</a></li>
<li class="leaf o2-ritz-manchester mid-10843"><a href="https://academymusicgroup.com/o2ritzmanchester">O2 Ritz Manchester</a></li>
<li class="leaf o2-shepherd-s-bush-empire mid-10844"><a href="https://academymusicgroup.com/o2shepherdsbushempire/" target="_self">O2 Shepherd&#039;s Bush Empire</a></li>
<li class="last leaf o2-victoria-warehouse-manchester mid-10845"><a href="https://academymusicgroup.com/o2victoriawarehousemanchester">O2 Victoria Warehouse Manchester</a></li>
</ul></li>
<li class="expanded managed-venues mid-10846"><a href="/" class="active">Managed Venues</a><ul class="menu"><li class="first leaf liverpool-guild-of-students mid-10847"><a href="http://www.liverpoolguild.org/" target="_blank">Liverpool Guild of Students</a></li>
<li class="last leaf the-old-fire-station-bournemouth mid-10848"><a href="http://www.oldfirestation.co.uk/" target="_blank">The Old Fire Station Bournemouth</a></li>
</ul></li>
<li class="last expanded partner-venues mid-10849"><a href="/" class="active">Partner Venues</a><ul class="menu"><li class="first leaf id-o2-guildhall-southampton mid-10850"><a href="http://www.o2guildhallsouthampton.co.uk/" target="_blank"> O2 Guildhall Southampton</a></li>
<li class="last leaf o2-apollo-manchester mid-10851"><a href="http://www.o2apollomanchester.co.uk/" target="_blank">O2 Apollo Manchester</a></li>
</ul></li>
</ul>
      </div>


      <div class="col col-12 bottom-footer p2">
                <p class="left mb0 copyright">
          <a href="http://www.academymusicgroup.com"target="_blank">&copy;&nbsp;Academy Music Group Limited&nbsp;2019</a>
        </p>

        <ul class="social-media right mb0">
                      <li>
              <a href="http://facebook.com/AcademyMusicGroup" target="_blank">
                <i class="icon-facebook"></i>
              </a>
            </li>
                                <li>
              <a href="http://twitter.com/academyamg" target="_blank">
                <i class="icon-twitter"></i>
              </a>
            </li>
                                <li>
              <a href="http://instagram.com/academymusicgroup" target="_blank">
                <i class="icon-instagram"></i>
              </a>
            </li>
          
        </ul>
      </div>
    </div>
  </div>

</footer>
        
  </div>

</div>





<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.eu01.nr-data.net","licenseKey":"0deacef229","applicationID":"14221471","transactionName":"MhBSZQoZVhcAWxdeXgtaZUMRV1EKBV0bGUENBQ==","queueTime":0,"applicationTime":11,"atts":"HldRE0IDRRk=","errorBeacon":"bam.eu01.nr-data.net","agent":""}</script></body>
</html>
