<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">




<html class="no-js"> 
<head>
    <title>Your Journey | Welsh Highland Heritage Railway</title>
    
        <meta name="description" content="Your visit to the Welsh Highland Heritage Railway"/>
    
    <link rel="shortcut icon" href="/s3/icons/whr16x16.png"/>

    <link rel="stylesheet" type='text/css' href="/s3/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css"
          media="all"/>
    <link rel="stylesheet" type='text/css' href="/s3/css/css_1RiAN66wP5pnYIv9m8mRccugSfiNj1qomcqY9Q7X7zc.css"
          media="all"/>
    <link rel="stylesheet" type='text/css' href="/s3/css/css_XHWSuVJpN_kim6V8r3j4DNjNeH251Nh8MK0njqalvMM.css"
          media="all"/>
    <link rel="stylesheet" type='text/css' href="/s3/css/css_tjb5gqQL_HcNuWa3CbKE2JFHA70r9ngf3GNZCEhYB1E.css"
          media="print"/>
    <link rel="stylesheet" type='text/css' href="/s3/css/css_bKdkw-KKmtjXB2PUe7m68vjKgk325hXJSIaYid2f2sw.css"
          media="all"/>
    <link rel="stylesheet" type='text/css' href="/s3/wf/fonts.css" media="all"/>
    <script type='text/javascript' src="/s3/js/js_ri60QNjcazMKdlAmWmDsmRb0iKaVkkXUQxH2MsgpmVo.js"></script>
    <script type='text/javascript' src="/s3/js/js_gJSCwQELXiaKJIsloXVQmSHVpGHkEnkuOa5zs6vg318.js"></script>

    
    <link rel='stylesheet' type='text/css' href='/_aKI9/css/_min.css'/>

    <script type='text/javascript' src="/s3/mkportal/jquery.zaccordion.js"></script>
    <script type='text/javascript' src="/s3/mkportal/accordion.js"></script>


    <meta name="generator" content="Hugo 0.54.0" />
</head>

<body class="html not-front not-logged-in two-sidebars page-node page-node-42 node-type-commercial-page baseof">
<div id="skip">
    <a href="#main-menu"></a>
</div>

<div id="pagetpl" class="">
    <div id="header">
        <div id="header-region">
            <div class="region region-header">
                <div class="block block-odd clearfix">
                    <div class="content">
                        <div id="googleSearchBlock">
                            <form action="//www.google.com/custom" method="get">
                                <div>
                                    <input type="text" id="textSearch" name="q"
                                           style="width:140px; border:1px inset #aaa;"
                                           maxlength="150" value="" placeholder="&#128269; search"/>
                                    
                                    
                                    <input name="cof" type="hidden"
                                           value="BGC:#FFFFFE;AH:left;S:http://www.whr.co.uk/;AWFID:b7ba4a60049084c1;"/>
                                    <input name="domains" type="hidden" value="whr.co.uk"/>
                                    <input name="sitesearch" type="hidden" value="whr.co.uk"/>
                                    <input name="ie" type="hidden" value="utf-8"/>
                                    <input name="oe" type="hidden" value="utf-8"/>
                                    <input type="submit" style="display:none"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="main" class="clearfix">

        <div id="content">
            <div id="content-inner" class="inner column center">
                

                <section class="post">
                    

                    

                    
                        
                    

                    
                    

                    

                    

                    <div id="content-area">
                        <div class="region region-content">
                            <div id="block-system-main" class="block block-system block-even clearfix">
                                <div class="content">
                                    <div id="node-42" class="node node-commercial-page node-odd">
                                        <div class="node-inner">
                                            <div class="content">
                                                <div class="field field-name-body field-type-text-with-summary field-label-hidden">
                                                    <div class="field-items">
                                                        <div class="field-item even">
                                                            
                                                                <div class="region region-content">
    <div class="block block-even clearfix">
        <div class="block-inner">
            <div class="content" style="text-align:center">
                <h2>The Welsh Highland Heritage Railway is a bit different from other railways.......</h2>
                <div class="block-like">
                    <ul id="slider">
                        <li><img alt="your Journey" src="/s3/slider/images/1.png"/></li>
                        <li><a href="/timetable"><img alt="Timetable &amp; Events"
                                                      src="/s3/slider/images/2.png"/></a></li>
                        <li><a href="/driverExperience"><img alt="Driver Experience"
                                                             src="/s3/slider/images/3.png"/></a></li>
                        <li><a href="/howToFindUs"><img alt="How to find us"
                                                        src="/s3/slider/images/4.png"/></a></li>
                        <li><a href="/aboutUs"><img alt="About Us" src="/s3/slider/images/5.png"/></a>
                        </li>
                    </ul>
                </div>
                <h2>The journey takes about an hour, but you won't be on the train for most of the time!</h2>
            </div>
        </div>
    </div>

<p><a href="/yourTrain/"><strong>Your train</strong></a> will take you for a short ride, then we stop at the <a href="/engineSheds"><strong>Engine Sheds</strong></a> where you get to climb into the cabs of the locomotives and see how things work.</p>

<p>Find out about the narrow gauge railways of Porthmadog and how they became famous around the world with your free visitor guide.</p>

<p>Thought our railway was small? Try the <a href="/miniatureRailway/"><strong>miniature version</strong></a> at the Engine Sheds. If you haven&rsquo;t visited us before, click <a href="/s3/pix/Misc/WHHR-Review-Daily-Post.pdf" target="_blank"><strong>here</strong></a> to find out what one family thought.</p>

<p><strong>A whole family can ride up and down all day for just £25.00!</strong> You can find full fare details <a href="/fares/"><strong>here</strong></a>.</p>

<p>Most of the site is level, and there&rsquo;s a wheelchair accessible carriage on every train. Click <a href="/ourFacilities/"><strong>here</strong></a> for more practical information.</p>

<p>And before you go home, why not treat yourself to something in <a href="/ourShop"><strong>our shop</strong></a> or have something to eat in the <a href="/ourTeaRoom/"><strong>Russell Tea Room</strong></a>?</p>

<p style="text-align: center;"></p>

<div class="box" style="text-align:center">
    <table class="centred eq3col" style="width:620px; height:122px; margin:0 auto">
        <tbody>
        <tr>
            <td style="width:125px">
                <a href="/jump/to/www.visitwales.com/"><img title="Visit Wales Accredited Attraction" alt="Visit Wales Accredited Attraction" src="/s3/pix/Logos/VAQAS-logo-168.jpg"
                                                            width="84"/></a>
            </td>
            <td style="width:125px">
                <a href="/jump/to/www.greatlittletrainsofwales.co.uk/"><img width="100" title="Great Little Trains of Wales" alt="Great Little Trains of Wales" src="/s3/pix/Logos/gltw.jpg"/></a>
            </td>
            <td style="width:125px">
                <a href="/jump/to/www.artscouncil.org.uk/funding/"><img title="Prism" alt="Prism" src="/s3/pix/Logos/Prism-Logo.gif"/></a>
            </td>
            <td style="width:125px">
                <a href="/jump/to/www.tripadvisor.co.uk/Attraction_Review-g616282-d3342166-Reviews-Welsh_Highland_Heritage_Railway"><img width="116" height="46"
                                                                                                                                         src="/s3/pix/Logos/TA_logo_primary.svg"
                                                                                                                                         width="108"></a>
            </td>
            <td style="width:125px">
                <a href="/jump/to/www.facebook.com/WHHRly"><img src="/s3/pix/Logos/like-us-on-facebook.jpg" width="108"/></a>
            </td>
        </tr>
        <tr class="small">
            <td>Accredited <br> Tourist Attraction</td>
            <td>One of the Great Little <br> Trains of Wales</td>
            <td>Receiver of <br> Prism Funding</td>
            <td>
                <a href="/jump/to/www.tripadvisor.co.uk/Attraction_Review-g616282-d3342166-Reviews-Welsh_Highland_Heritage_Railway">We are on<br/>Tripadviser</a>
            </td>
            <td>
                <a href="/jump/to/www.facebook.com/WHHRly/">facebook.com/WHHRly</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>

                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    

                    

                    
                </section>
            </div>
        </div>

        
<div id="sidebar-first" class="column sidebar first">
    <div id="sidebar-first-inner" class="inner">
        <div class="region region-sidebar-first">
    <aside id="block-menu-menu-commercial-menu" class="block block-menu block-odd clearfix">
    <div class="content">
        <ul class="menu">
            
            
                
                    <li class="leaf"><a href="/cymraeg/"><span>Cymraeg</span></a></li>
                
            
                
                    <li class="leaf"><a href="/"><span>Your Journey</span></a></li>
                
            
                
                    <li class="leaf"><a href="/timetable/"><span>Timetable &amp; Events</span></a></li>
                
            
                
                    <li class="leaf"><a href="/driverExperience/"><span>Driver Experience</span></a></li>
                
            
                
                    <li class="leaf"><a href="/driveADiesel/"><span>Drive a Diesel</span></a></li>
                
            
                
                    <li class="leaf"><a href="/info/howToFindUs/"><span>How to find us</span></a></li>
                
            
                
                    <li class="leaf"><a href="/info/aboutUs/"><span>About Us</span></a></li>
                
            
                
                    <li class="leaf"><a href="/fares/"><span>Fares</span></a></li>
                
            
                
                    <li class="leaf"><a href="/yourTrain/"><span>Your Train</span></a></li>
                
            
                
                    <li class="leaf"><a href="/about/membership/"><span>Membership</span></a></li>
                
            
                
                    <li class="leaf"><a href="/about/access/"><span>Access To The Railway</span></a></li>
                
            
        </ul>
    </div>
</aside>

    <div class="block block-even clearfix">
    <div class="content">
        <h2 class="block-title"><a href="/railway/"><img
                        src="/s3/pix/Blocks/railway-enthusiasts.png"
                        alt="Railway enthusiasts &amp; volunteers"
                        title="Railway Enthusiasts &amp; Volunteers - click here" height="77" width="150"/></a>
        </h2>
    </div>
</div>

    <div class="block block-odd clearfix">
    <div class="block-inner">
        <div class="content block-title">
            <h2 class='block-title'><a href="/leaflet/"><img alt="2019 Leaflet"
                                                             src="/s3/2019/Leaflet-block.png"
                                                             style="width: 148px; height: 148px;"
                                                             title="2019 leaflet"></a></h2>
        </div>
    </div>
</div>

    <div class="block block-even clearfix">
    <div class="content">
        <h2>
            <a href="http://maps.google.co.uk/?q=LL49+9DY" target="_blank"
            ><img alt="Contact us" src="/s3/2012/Contact-us-transparent.png" title="Contact us"/></a>
        </h2>
    </div>
</div>

</div>

    </div>
</div>


        
<div id="sidebar-second" class="column sidebar second">
    <div id="sidebar-second-inner" class="inner">
        <div class="region region-sidebar-second">
    <div class="block block-even clearfix">
    <div class="content">
        <div id="mini-timetable">
            <h2 class="block-title"><a href="/timetable">November 2019<br/>Timetable</a></h2>
<table class="timetable">
    <tbody>
    
    <tr class="headings">
        <th class='daySa'>Sa</th><th class='daySu'>Su</th><th class='dayMo'>Mo</th><th class='dayTu'>Tu</th><th class='dayWe'>We</th><th class='dayTh'>Th</th><th class='dayFr'>Fr</th>
    </tr>
    
    <tr class="tt">
        <td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='kdiesel-2' title='Diesel-2'>1</td>
    </tr>
    
    <tr class="tt">
        <td class='kdiesel-2' title='Diesel-2'>2</td><td class='noservice' title='No public service'>3</td><td class='noservice' title='No public service'>4</td><td class='noservice' title='No public service'>5</td><td class='noservice' title='No public service'>6</td><td class='noservice' title='No public service'>7</td><td class='noservice' title='No public service'>8</td>
    </tr>
    
    <tr class="tt">
        <td class='noservice' title='No public service'>9</td><td class='noservice' title='No public service'>10</td><td class='noservice' title='No public service'>11</td><td class='noservice' title='No public service'>12</td><td class='noservice' title='No public service'>13</td><td class='noservice' title='No public service'>14</td><td class='noservice' title='No public service'>15</td>
    </tr>
    
    <tr class="tt">
        <td class='noservice' title='No public service'>16</td><td class='noservice' title='No public service'>17</td><td class='noservice' title='No public service'>18</td><td class='noservice' title='No public service'>19</td><td class='noservice' title='No public service'>20</td><td class='noservice' title='No public service'>21</td><td class='noservice' title='No public service'>22</td>
    </tr>
    
    <tr class="tt">
        <td class='noservice' title='No public service'>23</td><td class='noservice' title='No public service'>24</td><td class='noservice' title='No public service'>25</td><td class='noservice' title='No public service'>26</td><td class='noservice' title='No public service'>27</td><td class='noservice' title='No public service'>28</td><td class='noservice' title='No public service'>29</td>
    </tr>
    
    <tr class="tt">
        <td class='noservice' title='No public service'>30</td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td><td class='blank'></td>
    </tr>
    
    </tbody>
</table>



        </div>
    </div>
</div>

    <div class="block block-even clearfix">
    <div class="content">
        <div>~&nbsp;<a style="font-family:'PlayfairDisplay-BoldItalic,serif'; font-variant:small-caps" href="/events/" target="_blank">Next Event</a>&nbsp;~</div>
        <h2 style="font-size:1.2rem"><a href="/events/">Santa Specials</a></h2>
        <div>14th/15th December</div>
    </div>
</div>

    <div class="block block-even clearfix">
    <div class="content">
        <h2 class="block-title">
            <a href="/ourTeaRoom/"><img alt="Our Tea Room" src="/s3/pix/Blocks/Tea-Room-Adv.png"
                                        title="Our Tea Room" width="150" height="62"/><br/>
                Why not visit our Tea Room</a>!</h2></div>
</div>

    <div class="block block-odd clearfix">
    <div class="content">
        <h2 class="block-title"><a href="/ourShop/"><img alt="Time for a treat?"
                                                         src="/s3/pix/Blocks/Green-Shop-Banner.png"
                                                         width="150" height="60" title="Time for a treat?"/></a>
        </h2>
        <p>Whatever you're looking for, you're bound<br/>to find it in <a href="/ourShop/">our shop</a></p>
    </div>
</div>

    <div class="block block-even clearfix">
    <div class="content">
        <h4><a style="font-family:'PlayfairDisplay-BoldItalic';" href="/jump/to/bigtrainlittletrain.com/"
               target="_blank">Big Train Little Train</a></h4>
        <div>visiting little trains by big trains</div>
    </div>
</div>

</div>

    </div>
</div>

    </div>

    <div id="footer">
        <div class="region region-footer">
            <div class="block block-odd clearfix">
                <div class="content">
                    <div id="footer-content">
                        <p id="footer-address">Welsh Highland Railway Ltd., Tremadog Road, Porthmadog LL49 9DY.
                            Telephone: 01766 513402.</p>
                        <p id="footer-eqops">The Welsh Highland Railway Ltd. is an Equal Opportunities railway.
                            Details on request.</p>
                        <p>"Welsh Highland Railway" is a registered trademark of Welsh Highland Railway Ltd
                            (Registered Charity No. 1039817)</p>
                        <p id="footer-copyright">© Copyright 1995-2019 Richard Beton &amp; the
                            Welsh Highland Railway Ltd.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

</div>

</body>
</html>
