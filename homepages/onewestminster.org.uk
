<!DOCTYPE html>
<html lang="en" dir="ltr" class="no-js">
  <head>
    <meta charset="utf-8" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113298689-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments)};gtag("js", new Date());gtag("config", "UA-113298689-1", {"groups":"default","anonymize_ip":true});</script>
<meta name="title" content="One Westminster - Charity and volunteering work in London’s Westminster communities | One Westminster" />
<link rel="shortlink" href="https://www.onewestminster.org.uk/" />
<link rel="canonical" href="https://www.onewestminster.org.uk/" />
<meta name="description" content="One Westminster is a registered charity whose vision is &#039;A community where all lead fulfilling and self-sufficient lives.&#039;" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta content="ie=edge, chrome=1" http-equiv="x-ua-compatible" />
<meta name="google-site-verification" content="ZboVHyzyHQafYcztRrFGEWP_CNSsj8GMx05GHVOqwr4" />
<link rel="shortcut icon" href="/sites/default/files/favicon.png" type="image/png" />
<link rel="revision" href="https://www.onewestminster.org.uk/node/1" />

    <title>One Westminster - Charity and volunteering work in London’s Westminster communities | One Westminster</title>
    <link rel="stylesheet" media="all" href="/sites/default/files/css/css_nEA4PMlcamMIo8r8mFn_HFip0q252qarbkdv7IFIVbA.css?q1orpz" />
<link rel="stylesheet" media="all" href="/sites/default/files/css/css_JC64IpfwW08mHjOEmORyt2obkW7olpOnyIdqZHZvo54.css?q1orpz" />
<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Oswald:400,500%7CRoboto:300,400,500&amp;display=swap" />

    
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

  </head>
  <body class="lang-en pagetype-top path-frontpage node--type-page">
  <a href="#main-content" class="visually-hidden focusable skip-link">
    Skip to main content
  </a>
  
    <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    <div id="mobile-menu" class="show-for-small-only">
	<a href="javascript:;" class="mobile-menu-open">Menu</a>

		<div id="social-icons-mobile">
		<a href="https://www.facebook.com/OneWestminster?ref=br_rs" target="_blank"><img src="/themes/custom/ow_theme/images/icon-facebook.svg" alt="Facebook Icon" /></a>
		<a href="https://twitter.com/One_Westminster" target="_blank"><img src="/themes/custom/ow_theme/images/icon-twitter.svg" alt="Twitter Icon" /></a>
	</div>

	<div id="mobile-menu-inner">
		<ul><li><a href="/">Home</a></li><li><a href="/about-us">About Us</a></li><li><a href="/jobs">Jobs</a></li><li><a href="https://localgiving.org/charity/onewestminster/">Donate</a></li><li><a href="/contact-us">Contact Us</a></li><li><a href="/e-bulletin">E-Bulletin</a></li><li><a href="/training-and-events">Training/Events</a></li></ul>
	</div>
</div>

<header id="owheader" class="row">
	<div class="columns large-4 medium-4 text-center medium-text-left" data-mh="header">
		<a href="https://www.onewestminster.org.uk/"><img src="/themes/custom/ow_theme/images/logo.jpg" alt="One Westminster Logo" id="owlogo" /></a>
	</div>
	<div id="header-right" class="columns large-8 medium-8 text-center medium-text-right">
		<div id="header-right-inner" data-mh="header">
			<div id="google_translate_element" class="show-for-medium"></div>

						<div id="social-icons" class="show-for-medium">
				<a href="https://www.facebook.com/OneWestminster?ref=br_rs" target="_blank"><img src="/themes/custom/ow_theme/images/icon-facebook.svg" alt="Facebook Icon" /></a>
				<a href="https://twitter.com/One_Westminster" target="_blank"><img src="/themes/custom/ow_theme/images/icon-twitter.svg" alt="Twitter Icon" /></a>
			</div>

			<div id="main-menu" class="show-for-medium">
									  <div>
    <nav role="navigation" aria-labelledby="block-mainnavigation-menu" id="block-mainnavigation" class="block-mainnavigation">
            
  <h2 class="block-title visually-hidden" id="block-mainnavigation-menu">Main navigation</h2>
  

        

                        <ul class="menu dropdown" data-dropdown-menu>
                          <li>
        <a href="/" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Home</a>
              </li>
                <li>
        <a href="/about-us" data-drupal-link-system-path="node/42">About Us</a>
              </li>
                <li>
        <a href="/jobs" data-drupal-link-system-path="node/43">Jobs</a>
              </li>
                <li>
        <a href="https://localgiving.org/charity/onewestminster/">Donate</a>
              </li>
                <li>
        <a href="/contact-us" data-drupal-link-system-path="node/45">Contact Us</a>
              </li>
                <li>
        <a href="/e-bulletin" data-drupal-link-system-path="node/46">E-Bulletin</a>
              </li>
                <li>
        <a href="/training-and-events" data-drupal-link-system-path="training-and-events">Training/Events</a>
              </li>
        </ul>
  


  </nav>

  </div>

							</div>
		</div>
	</div>
</header>

<main id="owmain" class="">
			  <div>
    	<section id="tabarea">
		<div class="row">
			<div class="columns large-12">
				<div data-drupal-messages-fallback class="hidden"></div>

			</div>
		</div>
	</section>

<div class="field-wrapper field field-node--field-blocks field-name-field-blocks field-type-entity-reference-revisions field-label-hidden">
    <div class="field-items">
          <div class="field-item"><div class="row hide-for-small-only">
	<div class="columns large-12">
		<div id="para-1" class="para para-hero para-hero-large" style="background-image:url(/sites/default/files/styles/hero/public/images/MyWestminsterDay-FINAL%2050%20PERCENT%20CROP.jpg?itok=spWBhhaR);" >
			<div class="para-hero-overlay"></div>
			<div class="para-hero-content">
															<h1>What we do</h1>
									
									<p>One Westminster exists to serve the voluntary sector and volunteering in the City of Westminster</p>
				
									<div class="hero-button">
													<a href="/about-us" class="ow-button-clear">Read More</a>
											</div>
							</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-3" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6" data-mh="halfhalf-3">
							<img src="/sites/default/files/styles/half/public/images/Volunteering%20hands.jpg?itok=k4lYMwev" alt="Volunteering" />
					</div>
		<div class="columns large-6 medium-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-3">
				<div class="para-imgtext-content vcenter">
																		<h2>Volunteering</h2>
											
																		<p>We find volunteers for local charities and also source volunteering opportunities in Westminster for people of all ages.</p>

											
																		<a href="/volunteering" class="ow-button-blue">Read more</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-4" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6 large-push-6 medium-push-6" data-mh="halfhalf-4">
							<img src="/sites/default/files/styles/half/public/images/BEIS%20_Antonio%20team%20at%20work%20cropped.jpg?itok=3a4CrYxi" alt="Employee Volunteering" />
					</div>
		<div class="columns large-6 medium-6 large-pull-6 medium-pull-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-4">
				<div class="para-imgtext-content vcenter">
																		<h2>Employee Volunteering</h2>
											
																		<p>Our corporate team, Time &amp; Talents, organises programmes that match business employees with local charities to carry out projects to benefit the Westminster community.</p>

											
																		<a href="/employee-volunteering" class="ow-button-blue">Read More</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-5" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6" data-mh="halfhalf-5">
							<img src="/sites/default/files/styles/half/public/images/Funders%20Fair%202%20Organisation%20Support.jpg?itok=aX4n8IvS" alt="Organisation Support" />
					</div>
		<div class="columns large-6 medium-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-5">
				<div class="para-imgtext-content vcenter">
																		<h2>Organisation Support</h2>
											
																		<p>We can help setup charities and offer support with fundraising, governance, marketing, monitoring and evaluation. We also offer a DBS checking service and a mail box facility.</p>

											
																		<a href="/organisation-support" class="ow-button-blue">Read More</a>
													<a href="https://drive.google.com/file/d/1f-DZ7ClXTQer9R_u16cIkup1bR3qH8R5/view" class="ow-button-blue">Councils for Voluntary Services</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-6" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6 large-push-6 medium-push-6" data-mh="halfhalf-6">
					</div>
		<div class="columns large-6 medium-6 large-pull-6 medium-pull-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-6">
				<div class="para-imgtext-content ">
																		<h2>Networks</h2>
											
																		<p>We coordinate a range of networks that meet regularly to share indeas and discuss issues relevant to their areas of interest.</p>

											
																		<a href="/networks" class="ow-button-blue">Read More</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div class="row">
	<div class="columns large-12">
		<div id="para-10" class="para para-circles para-body-content" style="background-image:url(/sites/default/files/styles/hero/public/images/map_westminster_Final-4.jpg?itok=5W7rgAbM);" >
			<div class="para-hero-overlay"></div>

			<div class="row circle-row">
				<div class="columns large-12 text-center">
					<div class="circle-col">
						<div class="circle-col-inner">
															<h3>Register</h3>
																						<p>Please fill in this survey if you&#039;d like your Westminster organisation to become a member of One Westminster and receive our support services.</p>
																						<a href="/register" class="ow-button-blue">Read More</a>
													</div>
					</div>
					<div class="circle-col">
						<div class="circle-col-inner">
															<h3>Directory</h3>
																						<p>A searchable listing of Westminster&#039;s voluntary and community organisations.</p>
																						<a href="/directory" class="ow-button-blue">Read More</a>
													</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-7" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6" data-mh="halfhalf-7">
							<img src="/sites/default/files/styles/half/public/images/LD%20jumble%20sale%202019%20crop.jpg?itok=W4K5SsWq" alt="Projects" />
					</div>
		<div class="columns large-6 medium-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-7">
				<div class="para-imgtext-content vcenter">
																		<h2>Projects</h2>
											
																		<p>Our specialist projects focus on key needs such as people with learning disabilities or supporting isolated older people.</p>

											
																		<a href="/projects" class="ow-button-blue">Read More</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-8" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6 large-push-6 medium-push-6" data-mh="halfhalf-8">
							<img src="/sites/default/files/styles/half/public/images/Social%20prescribing%20Bromley%20by%20Bow%20meeting.jpg?itok=qZz8ZkN3" alt="Training/Events" />
					</div>
		<div class="columns large-6 medium-6 large-pull-6 medium-pull-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-8">
				<div class="para-imgtext-content vcenter">
																		<h2>Training/Events</h2>
											
											<div class="civi-events">
							<div class="views-element-container">
    <p><time datetime="2019-12-03T12:30:00Z">03 Dec 2019</time>
 - <a href="/civicrm/event/info?id=1514&reset=1">Meet the Funder - Neighbourhood Keepers Programme</a></p>
    <p><time datetime="2020-01-15T10:30:00Z">15 Jan 2020</time>
 - <a href="/civicrm/event/info?id=1515&reset=1">Children and Young People Network</a></p>



</div>

						</div>
					
																		<a href="/training-and-events" class="ow-button-blue">Read More</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item"><div id="para-9" class="para para-imgtext para-body-content">
												
	<div class="row" >
		<div class="columns large-6 medium-6" data-mh="halfhalf-9">
							<img src="/sites/default/files/styles/half/public/images/2018%2011%208%20Volunteers%20one%20westminster%20st%20john%27s%20wood%20%281%29%20bright.jpg?itok=b1UY25B6" alt="News" />
					</div>
		<div class="columns large-6 medium-6 para-colour-plain">
			<div class="para-imgtext-text" data-mh="halfhalf-9">
				<div class="para-imgtext-content vcenter">
																		<h2>News</h2>
											
																		<p>All the latest voluntary sector news from the borough.</p>

											
																		<a href="/articles" class="ow-button-blue">Read More</a>
															</div>
			</div>
		</div>
	</div>
</div></div>
          <div class="field-item">	
<div class="row">
	<div class="columns large-12">
		<div id="para-11" class="para para-twitter para-body-content para-twitter-blue row collapse">
							<div class="columns large-1 medium-2">
					<img src="/themes/custom/ow_theme/images/icon-twitter.png" alt="Twitter Icon" />
				</div>
			
			<div class="columns large-8 medium-8 twitter-tweet">
				<p><span>@<a href="https://www.twitter.com/One_Westminster" target="_blank">One_Westminster</a></span><br/>
				Following up from our last tweet for <a target="_blank" href="http://www.twitter.com/wlm">@wlm</a>_uk <a target="_blank" href="https://t.co/zAIElFP2DQ">https://t.co/zAIElFP2DQ</a></p>
			</div>

			<div class="columns large-2 medium-2">
				<ul>
					<li><a class="twitter-reply" href="https://twitter.com/intent/tweet?in_reply_to=1199698211893645312" target="_blank">Reply</a></li>
					<li><a class="twitter-retweet" href="https://twitter.com/intent/retweet?tweet_id=1199698211893645312" target="_blank">Retweet</a></li>
					<li><a class="twitter-favourite" href="https://twitter.com/intent/favorite?tweet_id=1199698211893645312" target="_blank">Favourite</a></li>
				</ul>
			</div>
		</div>
	</div>
</div></div>
      </div>
</div>

  </div>

	</main>

<footer id="owfooter">
	<div class="row">
		<div class="columns large-4 medium-4 contact-column">
			<h4>Contact</h4>
			<p>
				One Westminster<br/>
				37 Chapel Street<br/>
				London<br/>
				NW1 5DP
			</p>
			<p>
				Tel: <a href="tel:02077231216">020 7723 1216</a><br/>
				Email: <a href="mailto:info@onewestminster.org.uk">info@onewestminster.org.uk</a>
			</p>
			<p style="font-size:12px;">Registered charity number: 295501 - Company number: 02052268</p>
			<img src="/themes/custom/ow_theme/images/city-of-westminster.png" alt="City of Westminster" />
		</div>
		<div class="columns large-3 medium-3">
			<h4>Links</h4>
			<ul>
				<li><a href="">Home</a></li>
									<li><a href="/volunteering">Volunteering</a></li>
									<li><a href="/employee-volunteering">Employee Volunteering</a></li>
									<li><a href="/organisation-support">Organisation Support</a></li>
									<li><a href="/networks">Networks</a></li>
									<li><a href="/register">Register</a></li>
									<li><a href="/directory">Directory</a></li>
									<li><a href="/projects">Projects</a></li>
									<li><a href="/training-and-events">Training/Events</a></li>
									<li><a href="/articles">News</a></li>
							</ul>
		</div>
		<div class="columns large-3 medium-3">
			<h4>Legal</h4>
			<ul>
				<li><a href="/cookie-policy">Cookie Policy</a></li>
				<li><a href="/privacy-policy">Privacy Policy</a></li>
				
									<li><a href="/user/login">Login</a></li>
							</ul>
		</div>
		<div class="columns large-2 medium-2 text-center logo-column">
			<a href="https://www.livingwage.org.uk/" target="_blank"><img src="/themes/custom/ow_theme/images/living-wage.png" alt="Living Wage Employer" /></a>
			<img src="/themes/custom/ow_theme/images/lgbt-flag.png" alt="LGBT Flag" />
		</div>
	</div>
</footer>
<footer id="wdlfooter">
	<div class="row">
		<div class="columns large-12 text-center medium-text-left">
			<p>Drupal by <a href="https://www.websitedevelopment.ltd.uk" target="_blank">Website Development Ltd</a></p>
		</div>
	</div>
</footer>

	<nav id="floating-menu">
		<div class="row">
			<ul class="columns large-12 text-center">
									<li><a href="#para-3" class="scrollto">Volunteering</a></li>
									<li><a href="#para-4" class="scrollto">Employee Volunteering</a></li>
									<li><a href="#para-5" class="scrollto">Organisation Support</a></li>
									<li><a href="#para-6" class="scrollto">Networks</a></li>
									<li><a href="#para-10" class="scrollto">Register</a></li>
									<li><a href="#para-10" class="scrollto">Directory</a></li>
									<li><a href="#para-7" class="scrollto">Projects</a></li>
									<li><a href="#para-8" class="scrollto">Training/Events</a></li>
									<li><a href="#para-9" class="scrollto">News</a></li>
							</ul>
		</div>
	</nav>

  </div>

  
  <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/1","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","cookieconsent":{"link":"","path":"\/","expiry":365,"target":"_self","domain":".www.onewestminster.org.uk","markup":"\u003Cdiv class=\u0022cc_banner cc_container cc_container--open\u0022\u003E\n    \u003Ca href=\u0022#null\u0022 data-cc-event=\u0022click:dismiss\u0022 target=\u0022_blank\u0022 class=\u0022cc_btn cc_btn_accept_all\u0022\u003EGot it!\u003C\/a\u003E\n    \u003Cp class=\u0022cc_message\u0022\u003EThis website uses cookies to ensure you get the best experience on our website \u003Ca data-cc-if=\u0022options.link\u0022 target=\u0022_self\u0022 class=\u0022cc_more_info\u0022 href=\u0022\u0022\u003EMore info\u003C\/a\u003E\u003C\/p\u003E\n    \u003Ca class=\u0022cc_logo\u0022 target=\u0022_blank\u0022 href=\u0022http:\/\/silktide.com\/cookieconsent\u0022\u003ECookie Consent plugin for the EU cookie law\u003C\/a\u003E\n\u003C\/div\u003E\n","container":null,"theme":false},"google_analytics":{"account":"UA-113298689-1","trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"user":{"uid":0,"permissionsHash":"569964002cc00d18239282d58c92d8344f5d85e3c26820f8102e3656bd921226"}}</script>
<script src="/sites/default/files/js/js_bHx5F7yH50x-Pby4A0pSfFnmKA4dym1hGOXWDDaf56M.js"></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script src="/sites/default/files/js/js_epuvclMXc5rqX9fLMTAgtGwvhs8Qt_-FHT-SiQ007Yk.js"></script>

  </body>
</html>
