<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="og: http://ogp.me/ns#">
<head>
  <meta charset="utf-8" />
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-3524246-4", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<meta name="title" content="Brighton &amp; Hove City Council" />
<meta name="twitter:card" content="summary" />
<link rel="shortlink" href="https://new.brighton-hove.gov.uk/" />
<link rel="canonical" href="https://new.brighton-hove.gov.uk/" />
<meta name="twitter:title" content="Brighton &amp; Hove City Council" />
<meta name="twitter:site" content="@BrightonHoveCC" />
<meta name="twitter:description" content="Delivering 24 hour council services to residents, visitors and businesses in Brighton &amp; Hove" />
<meta name="twitter:creator" content="@BrightonHoveCC" />
<meta name="twitter:image" content="https://new.brighton-hove.gov.uk/themes/custom/bhcc/images/BHCC-Logo-twitter.jpg" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="revision" href="https://new.brighton-hove.gov.uk/_front" />

  <title>Brighton &amp; Hove City Council</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="google-site-verification" content="whpL-pBc0Hexn-Gz9egGV8YJcNMHZxlFSf9CeNr99dc"/>
  <link rel="apple-touch-icon" sizes="180x180" href="/themes/custom/bhcc/images/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/themes/custom/bhcc/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/themes/custom/bhcc/images/favicon-16x16.png">
  <link rel="manifest" href="/themes/custom/bhcc/images/manifest.json">
  <link rel="mask-icon" href="/themes/custom/bhcc/images/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#000000">

          <meta name="fb:admins" content="54869840070">

  <link rel="stylesheet" media="all" href="/sites/default/files/css/css_5wLP407upMzBMKwDBT1l0bH5gyBOPEbhTQlEfQHV52k.css?q1dnrn" />
<link rel="stylesheet" media="all" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" />
<link rel="stylesheet" media="all" href="https://use.fontawesome.com/releases/v5.0.13/js/v4-shims.js" />
<link rel="stylesheet" media="all" href="/sites/default/files/css/css_v52pnKvzGMwJsCsbQob1gLXKl-3n3S7iFQHISwvruAA.css?q1dnrn" />
<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i" />
<link rel="stylesheet" media="all" href="https://cloud.typography.com/6624672/7938172/css/fonts.css" />
<link rel="stylesheet" media="all" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" />
<link rel="stylesheet" media="all" href="/sites/default/files/css/css_WuK4f5ovVfR3hWMNJk0jtFe7tWUFf1oIzN9xgxTWBvg.css?q1dnrn" />

  
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->


  <!--[if lt IE 9 ]>
  <script src="/themes/custom/bhcc/assets/js/respond.min.js"></script>
  <![endif]-->

  <!--[if lt IE 10]>
  <style>
    @media only screen and (min-width: 768px){
      .services-list--multi li {
        width: 33.33%;
        float: left;
      }
    }
  </style>
  <![endif]-->



  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NPBPXBW');</script>
  <!-- End Google Tag Manager -->
</head>

  <body class="node node--homepage">
    <a href="#main-content" class="visually-hidden focusable skip-link">
    <i class="fas fa-fast-forward"></i>
    Skip to main content
  </a>
    
  <div class="site-content-wrapper">
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    
  <!-- HEADER -->
  <header class="header--container">
    <div class="wrapper">
      <div class="header--inner-homepage">
        <div class="header--desktop">
          <div class="header--desktop-left">
            <div class="site-logo">
              <img src="/themes/custom/bhcc/assets/img/brighton-and-hove-city-council@3x.png" alt="Home Brighton and Hove City Council"/>
            </div>
            <div class="beta-notice beta-notice-full">
              <div class="first-line">
                <span class="beta-badge">NEW</span>
                <a href="https://services.brighton-hove.gov.uk/link/websitefeedback">Give feedback</a>
              </div>
            </div>
          </div>
                                                                                                                </div>

        <div class="header--mobile">
          <div class="header--mobile-left">
            <div class="site-logo">
              <img src="/themes/custom/bhcc/assets/img/brighton-and-hove-city-council@3x.png" alt="Home Brighton and Hove City Council"/>
            </div>
          </div>
                                                                                                                </div>
      </div><!--/header-inner-->
    </div><!--/wrapper-->
  </header><!--/header-container-->
  <!--/HEADER-->

 

  
        

  <section class="homepage--hero-wrapper">
  <div class="homepage--hero" style="background-image: url(/themes/custom/bhcc/assets/img/home-pic-1.jpg)">
    <div class="wrapper">
      <div class="homepage--hero-inner">
        <div class="homepage--welcome">
          <h1>
            <span>Welcome to</span>
            <span>Brighton &amp; Hove</span>          </h1>
        </div>
      </div>
    </div>
  </div>
</section>



  

  <div class="wrapper">
    <a id="main-content"></a>
    <div class="homepage--search" role="search" aria-labelledby="homepage-search-header">

      <div class="search-left">
        <h2 id="homepage-search-header">How can we help?</h2>
      </div>

      <form action="/search/google" method="get">
        <div class="search-right">
          <fieldset>
            <legend class="sr-only">Search our site</legend>
            <div id="home-site-search" class="input-group">
              <label for="homepage-search-input" class="sr-only">Search</label>
              <input id="homepage-search-input" type="text" name="keys" />
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Search <span class="fa fa-search"></span></button>
              </span>
            </div>
          </fieldset>
        </div>
      </form>
    </div>

    <div class="page">
      
        <div class="site-messages"><div data-drupal-messages-fallback class="hidden"></div>
</div>

        



<section id="field--field_homepage_icons" class="services--feature owl-carousel owl-theme">
      <div>
              
<div  class="services--feature-block">
  <a href="/council-tax/paying-your-council-tax-0/pay-your-council-tax-direct-debit">
    <span class="services--feature-icon fa fa-home"></span>
    <span class="services--feature-title">
            Pay council tax
      </span>
  </a>
</div>

              
<div  class="services--feature-block">
  <a href="/check-your-bin-and-box-collection-days">
    <span class="services--feature-icon fa fa-calendar-alt"></span>
    <span class="services--feature-title">
            Check bin collection days
      </span>
  </a>
</div>

          </div>
      <div>
              
<div  class="services--feature-block">
  <a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking/parking-permits">
    <span class="services--feature-icon fa fa-pound-sign"></span>
    <span class="services--feature-title">
            Buy parking permits
      </span>
  </a>
</div>

              
<div  class="services--feature-block">
  <a href="https://planningapps.brighton-hove.gov.uk/online-applications/">
    <span class="services--feature-icon fa fa-clipboard-check"></span>
    <span class="services--feature-title">
            Search planning applications
      </span>
  </a>
</div>

          </div>
      <div>
              
<div  class="services--feature-block">
  <a href="/topic/report-problem">
    <span class="services--feature-icon fa fa-exclamation-triangle"></span>
    <span class="services--feature-title">
            Report a problem
      </span>
  </a>
</div>

              
<div  class="services--feature-block">
  <a href="/children-and-learning/apply-school/apply-school-place">
    <span class="services--feature-icon fa fa-pencil-alt"></span>
    <span class="services--feature-title">
            Apply for school
      </span>
  </a>
</div>

          </div>
      <div>
              
<div  class="services--feature-block">
  <a href="/children-and-learning/brighton-hove-schools/school-term-and-holiday-dates">
    <span class="services--feature-icon fa fa-child"></span>
    <span class="services--feature-title">
            Check school term dates
      </span>
  </a>
</div>

              
<div  class="services--feature-block">
  <a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking/pay-your-penalty-charge-notice-including-bus-lanecctv">
    <span class="services--feature-icon fa fa-car"></span>
    <span class="services--feature-title">
            Pay your parking fine
      </span>
  </a>
</div>

          </div>
  </section>

<section id="field--field_ia_blocks" class="services--list">
      
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking">
            Parking
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking/car-parks">Find a car park</a></li><li><a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking/parking-suspension-reserve-a-parking-bay">Reserve a parking bay</a></li><li><a href="https://www.brighton-hove.gov.uk/content/parking-and-travel/parking/apply-or-renew-a-blue-badge">Apply for a blue badge</a></li></ul>
</div>

      
<div class="services--list-block">
  <h3><a href="/council-tax">
            Council tax
      </a></h3>
  <ul ><li><a href="https://new.brighton-hove.gov.uk/node/1516">Tell us you&#039;re moving</a></li><li><a href="https://new.brighton-hove.gov.uk/node/1721">Get a discount</a></li><li><a href="https://new.brighton-hove.gov.uk/node/1676">Get help with Council Tax</a></li></ul>
</div>

      
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/environment/recycling-rubbish-and-street-cleaning-0">
            Rubbish, recycling and streets
      </a></h3>
  <ul ><li><a href="/report-missed-bin-or-box-collection">Report a missed collection</a></li><li><a href="/rubbish-recycling-and-streets/rubbish/garden-waste-collections">Get garden waste collections</a></li><li><a href="/report-problem-and-request-clean">Request a clean up</a></li></ul>
</div>

      
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/children-and-education">
            Children and learning
      </a></h3>
  <ul ><li><a href="http://www.familyinfobrighton.org.uk/kb5/brighton/fsd/family.page?familychannel=1&amp;sorttype=field&amp;sortfield=title">Find childcare</a></li><li><a href="/children-and-learning/apply-school">Apply for school</a></li><li><a href="https://www.brighton-hove.gov.uk/content/children-and-education/local-offer">Help with special needs and disabilities</a></li></ul>
</div>

      
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/planning">
            Planning and building regulations
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/content/planning/planning-applications/do-i-need-planning-permission">Get advice before you build</a></li><li><a href="https://www.brighton-hove.gov.uk/content/planning/local-requirements-planning-application-forms/planning-application-forms-and">Apply for planning permission</a></li><li><a href="https://www.brighton-hove.gov.uk/content/planning/heritage/listed-buildings-brighton-hove">Help with listed buildings</a></li></ul>
</div>

      
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/leisure-and-libraries">
            Libraries, leisure and arts
      </a></h3>
  <ul ><li><a href="https://brighton-hove.spydus.co.uk/cgi-bin/spydus.exe/MSGTRN/OPAC/HOME">Borrow library items</a></li><li><a href="https://www.brighton-hove.gov.uk/node/351">Find sports clubs and activities</a></li><li><a href="https://www.brighton-hove.gov.uk/node/1546">Find parks and green spaces</a></li></ul>
</div>

  
    <div id="services--expand-container">
          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/parking-and-travel">
            Roads and travel
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/node/16546">Find public transport</a></li><li><a href="https://www.brighton-hove.gov.uk/node/1231781">Get travel news</a></li><li><a href="https://www.brighton-hove.gov.uk/node/19936/">Report road damage</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://new.brighton-hove.gov.uk/health-and-wellbeing">
            Health and wellbeing
      </a></h3>
  <ul ><li><a href="https://new.brighton-hove.gov.uk/health-and-wellbeing-draft/support-improve-your-health">Support to improve your health</a></li><li><a href="https://new.brighton-hove.gov.uk/health-and-wellbeing-draft/support-be-active">Get active</a></li><li><a href="https://new.brighton-hove.gov.uk/adult-social-care/care-and-support-adults">Get care and support</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/social-care">
            Adult Social Care
      </a></h3>
  <ul ><li><a href="https://new.brighton-hove.gov.uk/node/2931">Get care and support</a></li><li><a href="https://new.brighton-hove.gov.uk/node/2146">Report a concern</a></li><li><a href="https://new.brighton-hove.gov.uk/node/34851">Support to improve your health</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://new.brighton-hove.gov.uk/node/2036">
            Housing
      </a></h3>
  <ul ><li><a href="https://new.brighton-hove.gov.uk/node/2061">Apply for housing</a></li><li><a href="https://new.brighton-hove.gov.uk/node/2151">Pay your rent</a></li><li><a href="https://new.brighton-hove.gov.uk/node/2221">Ask for a repair</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="/benefits">
            Benefits
      </a></h3>
  <ul ><li><a href="/benefits/housing-benefit-universal-credit-and-council-tax-reduction/changes-housing-benefit">Apply for housing benefit</a></li><li><a href="http://new.brighton-hove.gov.uk/node/716">Get help in an emergency</a></li><li><a href="/changes">Tell us about a change</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/environment">
            Environment
      </a></h3>
  <ul ><li><a href="/pest-control">Get help with pests</a></li><li><a href="https://www.brighton-hove.gov.uk/content/environment/noise-pollution-and-air-quality/report-a-noise-problem">Report a noise problem</a></li><li><a href="https://new.brighton-hove.gov.uk/node/971">Get an allotment</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/node/46">
            Births, deaths and marriages
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/node/20061">Get married</a></li><li><a href="https://www.brighton-hove.gov.uk/node/19946">Register a birth</a></li><li><a href="https://www.brighton-hove.gov.uk/node/15326">Tell us someone has died</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/life-events-and-communities">
            City support and grants
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/node/1964581">Volunteer in your area</a></li><li><a href="https://www.brighton-hove.gov.uk/node/61">Support refugees</a></li><li><a href="https://www.brighton-hove.gov.uk/node/12796">Apply for funding</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/council-and-democracy">
            Your council
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/node/361521">Register to vote</a></li><li><a href="https://www.brighton-hove.gov.uk/node/521">Have your say</a></li><li><a href="https://www.brighton-hove.gov.uk/node/1967836">About council meetings</a></li></ul>
</div>

          
<div class="services--list-block">
  <h3><a href="https://www.brighton-hove.gov.uk/content/jobs">
            Jobs
      </a></h3>
  <ul ><li><a href="https://www.brighton-hove.gov.uk/node/1668851">Find a council job</a></li><li><a href="https://www.brighton-hove.gov.uk/node/793121">Find an apprenticeship</a></li><li><a href="https://www.brighton-hove.gov.uk/node/1429221">Why work for us</a></li></ul>
</div>

          
  <div class="services--business services--list-block">
    <h3><a href="https://www.brighton-hove.gov.uk/content/business-and-trade" style="color: #000000 !important;">
            Business
      </a></h3>
    <ul  class="business-list"><li><a href="https://www.brighton-hove.gov.uk/content/business-and-trade/business-rates/pay-business-rates">Pay business rates</a></li><li><a href="https://www.brighton-hove.gov.uk/content/business-and-trade/health-and-safety">Help with health and safety</a></li><li><a href="https://www.brighton-hove.gov.uk/content/council-and-democracy/contracts-and-tenders-council">Tendering for council work</a></li><li><a href="https://www.brighton-hove.gov.uk/content/business-and-trade/food-safety">Food safety guidance</a></li><li><a href="https://www.brighton-hove.gov.uk/content/leisure-and-libraries/events-and-filming/council-buildings-conferences-and-events">Find conference facilities</a></li></ul>
  </div>

      </div>


  <div class="services--expand">
    <div class="action-link-container">
      <button id="services--expand-link" class="action-link action-link--expand show"><i class="action-link--expand-icon fas fa-chevron-down"></i>Show more services</button>
    </div>
  </div>
  </section>
<section class="top-story--homepage">
  <div class="top-story--title">
    <h3><a href="/news">Newsroom</a></h3>
    <span>Latest news from Brighton & Hove City Council</span>
  </div>

  <div class="top-story--slice">
          
<div class="top-story--block_3up">

  <a href="/news/2019/how-council-acting-climate-crisis">
    <div class="top-story--image">
        
              <img src="/sites/default/files/styles/341x256/public/2019-11/thumb-climate-emergency.jpg?itok=TVtA4nHg" width="341" height="256" alt="Child holding up a model of the planet against a sunset sky." />



      
    </div>&nbsp;

    <div class="top-story--content">
      <h3>
        
            How the council is acting on the climate crisis
      
      </h3>

      
            Since declaring a climate emergency we have been working hard on practical ways to reduce carbon and climate change across our services and how we can support communities.
      
    </div>
  </a>
</div>

          
<div class="top-story--block_3up">

  <a href="/news/2019/help-shape-plans-tackle-homelessness-and-rough-sleeping">
    <div class="top-story--image">
        
              <img src="/sites/default/files/styles/341x256/public/news/thumb-housing-mix.jpg?itok=ZW8bZR-S" width="341" height="256" alt="Housing in Brighton &amp; Hove" />



      
    </div>&nbsp;

    <div class="top-story--content">
      <h3>
        
            Help shape plans to tackle homelessness and rough sleeping
      
      </h3>

      
            We&#039;re asking for views to help develop the city’s Homelessness &amp; Rough Sleeping Strategy 2020 to 2025. The survey is open until 8 December.
      
    </div>
  </a>
</div>

          
<div class="top-story--block_3up">

  <a href="https://www.brighton-hove.gov.uk/content/council-and-democracy/voting-and-elections/general-election-thursday-12-december-2019">
    <div class="top-story--image">
        
              <img src="/sites/default/files/styles/341x256/public/news/thumbnails/thumb-election-vote-graphic.png?itok=mbmj7n5r" width="341" height="256" alt="" />



      
    </div>&nbsp;

    <div class="top-story--content">
      <h3>
        
            Register to vote
      
      </h3>

      
            A general election will take place on Thursday 12 December 2019. You must be registered by Tuesday 26 November to vote in this election.
      
    </div>
  </a>
</div>

      </div>

  <a href="/news" class="action-link action-link--expand margin-top--20 margin-bottom--40">
    <i class="action-link--expand-icon fas fa-chevron-right"></i>Visit the Newsroom
  </a>
</section>



        

  <div class="bhcc--pull_councillor bhcc--pull blue-left margin-bottom--40">
  <img src="themes/custom/bhcc/assets/img/brighton-and-hove-city-council-black.png" alt="Brighton & Hove City Council logo">
  <p>Find your local councillor and their contact details</p>
  <form class="find-your-councillor-form site-search site-search--desktop" id="site-search" data-drupal-selector="find-your-councillor-form" action="/" method="post" accept-charset="UTF-8" role="search" aria-labelledby="find-your-local-councillor-label">
  

<fieldset>
  <legend class="sr-only" id="find-your-local-councillor-label">Find your councillor</legend>
  <div class="input-group">
    <label for="find-councillor" class="sr-only">Enter your postcode</label>
    <input placeholder="Enter your postcode" class="form-control form-text" id="find-councillor" data-drupal-selector="edit-find-your-councillor-postcode" type="text" name="find_your_councillor_postcode" value="" size="60" maxlength="128" />
    <span class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <span class="fa fa-search"></span><span class="sr-only">Search</span>
      </button>
    </span>
  </div>
</fieldset>

<div class="hidden"><input data-drupal-selector="edit-submit" type="submit" id="edit-submit" name="op" value="" class="button js-form-submit form-submit" />
</div><input autocomplete="off" data-drupal-selector="form-x4wxagvlqpbadpyjfc4cf95fgaq8crzqvti6lkajtm8" type="hidden" name="form_build_id" value="form-X4WXAgVlQpbAdPyJFc4cf95FGaq8cRZqvti6LkajtM8" class="form-control" />
<input data-drupal-selector="edit-find-your-councillor-form" type="hidden" name="form_id" value="find_your_councillor_form" class="form-control" />

</form>

  </div>
</div>



    </div>
  </div>

<footer role="contentinfo">
      <div class="site-main-footer">
      <div class="wrapper">
        <div class="site-footer-inner row">
          <nav class="col-sm-6" role="navigation" aria-labelledby="footer-about-header">
            <div class="footer-col">
                
      <h2 class="footer-heading" id="footer-about-header">Find out more</h2>
  
    
        <ul class="footer-about">
            <li>
        <a href="/about-website" target="_self" data-drupal-link-system-path="node/161">About this website</a>
              </li>
          <li>
        <a href="/about-website/accessibility-statement" target="_self" data-drupal-link-system-path="node/41211">Accessibility statement</a>
              </li>
          <li>
        <a href="/about-website/privacy-and-data" target="_self" data-drupal-link-system-path="node/521">Privacy and data</a>
              </li>
          <li>
        <a href="/about-website/how-we-use-cookies" target="_self" data-drupal-link-system-path="node/536">Cookies</a>
              </li>
          <li>
        <a href="/rubbish-recycling-and-streets/recycling-guide" data-drupal-link-system-path="node/41772">Recycling A to Z</a>
              </li>
      </ul>
    




            </div>
          </nav>
          <nav class="col-sm-6" role="navigation" aria-labelledby="footer-contact-header">
            <div class="footer-right">
              <div class="footer-col">
                <h2 class="footer-heading" id="footer-contact-header">Contact</h2>
                <ul class="footer-contact">
                  <li><a href="/node/1776"><span class="fa fa-envelope"></span>Contact us</a></li>
                  <li><a href="/node/1771"><span class="fa fa-map-marker-alt"></span>Visit us in person</a></li>
                </ul>
                <div class="footer-social">
                  <a href="https://www.facebook.com/BrightonandHoveCityCouncil" class="icon-social" target="_blank">
                    <span class="fab fa-facebook-f"></span><span class="sr-only">Facebook</span>
                  </a>
                  <a href="https://twitter.com/BrightonHoveCC" class="icon-social" target="_blank">
                    <span class="fab fa-twitter"></span><span class="sr-only">Twitter</span>
                  </a>
                  <a href="https://www.youtube.com/user/BrightonandHoveGovUk" class="icon-social" target="_blank">
                    <span class="fab fa-youtube"></span><span class="sr-only">Youtube</span>
                  </a>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  
    <div class="site-sub-footer">
    <div class="wrapper">
      <div class="site-footer-inner">
        <p>&copy; Brighton & Hove City Council</p>
      </div>
    </div>
  </div>
  </footer>

  </div>

  </div>
  

  <!-- Hotjar Tracking Code for https://new.brighton-hove.gov.uk/ -->
  <script>
    (function (h, o, t, j, a, r) {
      h.hj = h.hj || function () {
        (h.hj.q = h.hj.q || []).push(arguments)
      };
      h._hjSettings = {hjid: 592169, hjsv: 5};
      a = o.getElementsByTagName('head')[0];
      r = o.createElement('script');
      r.async = 1;
      r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
      a.appendChild(r);
    })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
  </script>

  <!-- flexbox polyfill -->
  <!--[if lte IE 9 ]>
  <script src="/themes/custom/bhcc/assets/js/flexibility.js"></script>
  <script type='text/javascript'>
    window.onload=function(){
      flexibility(document.body);
    }
  </script>
  <![endif]-->

  <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/2531","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","ajaxPageState":{"libraries":"bhcc\/global,bhcc\/homepage,bhcc_sitewide_alert\/library,core\/html5shiv,eu_cookie_compliance\/eu_cookie_compliance_bare,fontawesome\/fontawesome.webfonts.shim,google_analytics\/google_analytics,paragraphs\/drupal.paragraphs.unpublished,system\/base","theme":"bhcc","theme_token":null},"ajaxTrustedUrl":{"form_action_p_pvdeGsVG5zNF_XLGPTvYSKCf43t8qZYSwcfZl2uzM":true},"google_analytics":{"trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"eu_cookie_compliance":{"popup_enabled":true,"popup_agreed_enabled":false,"popup_hide_agreed":false,"popup_clicking_confirmation":false,"popup_scrolling_confirmation":false,"popup_html_info":"\u003Cdiv class=\u0022cookies-notice\u0022\u003E\n  \u003Cdiv class=\u0022wrapper\u0022\u003E\n    \u003Cdiv class=\u0022cookies-notice-inner\u0022\u003E\n      \u003Cspan\u003EWe use cookies to improve our service.\u003C\/span\u003E\n      \u003Cspan\u003EBy using this site, you agree to this use.\u003C\/span\u003E\n      \u003Ca href=\u0022\/about-website\/how-we-use-cookies\u0022\u003ESee our cookie policy\u003C\/a\u003E\n      \u003Ca href=\u0022#\u0022 class=\u0022agree-button button-accept\u0022\u003EAccept and close \u003Ci class=\u0022fa fa-check\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E\u003C\/a\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022cookies-notice\u0022\u003E\n  \u003Cdiv class=\u0022wrapper\u0022\u003E\n    \u003Cdiv class=\u0022cookies-notice-inner\u0022\u003E\n      \u003Cspan\u003EWe use cookies to improve our service.\u003C\/span\u003E\n      \u003Cspan\u003EBy using this site, you agree to this use.\u003C\/span\u003E\n      \u003Ca href=\u0022\/about-website\/how-we-use-cookies\u0022\u003ESee our cookie policy\u003C\/a\u003E\n      \u003Ca href=\u0022#\u0022 class=\u0022agree-button button-accept\u0022\u003EAccept and close \u003Ci class=\u0022fa fa-check\u0022 aria-hidden=\u0022true\u0022\u003E\u003C\/i\u003E\u003C\/a\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","mobile_breakpoint":768,"popup_html_agreed":false,"popup_use_bare_css":true,"popup_height":"auto","popup_width":"100%","popup_delay":0,"popup_link":"\/node\/331","popup_link_new_window":true,"popup_position":true,"fixed_top_position":false,"popup_language":"en","store_consent":false,"better_support_for_screen_readers":false,"cookie_name":"","reload_page":false,"domain":"","domain_all_sites":false,"popup_eu_only_js":false,"cookie_lifetime":100,"cookie_session":0,"disagree_do_not_show_popup":false,"method":"opt_in","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003E\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info eu-cookie-compliance-content\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022 class=\u0022eu-cookie-compliance-message\u0022\u003E\n      \n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022eu-cookie-compliance-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003E\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","withdraw_enabled":false,"withdraw_button_on_info_popup":null,"cookie_categories":[],"enable_save_preferences_button":null,"fix_first_cookie_category":null,"select_all_categories_by_default":null},"bhcc_sitewide_alert":{"token":"PypDiha7kK"},"user":{"uid":0,"permissionsHash":"98346a8b22cf2f9e8255c243a10f2c1ecfe6abdee524a2ca2d9c07437cbc9513"}}</script>
<script src="/sites/default/files/js/js_sLt19COn6t0Epi_94Vi_5wtFNQqDJUXjNo0mT1BivpM.js"></script>

</body>
</html>
