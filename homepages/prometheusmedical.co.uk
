<!DOCTYPE html>
<head>
  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.prometheusmedical.co.uk/misc/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="description" content="Prometheus is a leading supplier of pre-hospital emergency medical equipment and high-quality medical training courses for a wide audience; from the layperson through to the healthcare professional." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.prometheusmedical.co.uk/" />
<link rel="shortlink" href="https://www.prometheusmedical.co.uk/" />
  <title>Pre-hospital emergency medical equipment and high-quality medical training courses | Prometheus Medical UK</title>

      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="cleartype" content="on">
  <!-- Start of favicons -->
  <link rel="apple-touch-icon" sizes="120x120" href="/sites/all/themes/prometheus/assets/favicons/apple-touch-icon.png?v=yyxj0mow2Y">
  <link rel="icon" type="image/png" href="/sites/all/themes/prometheus/assets/favicons/favicon-32x32.png?v=yyxj0mow2Y" sizes="32x32">
  <link rel="icon" type="image/png" href="/sites/all/themes/prometheus/assets/favicons/favicon-16x16.png?v=yyxj0mow2Y" sizes="16x16">
  <link rel="manifest" href="/sites/all/themes/prometheus/assets/favicons/manifest.json?v=yyxj0mow2Y">
  <link rel="mask-icon" href="/sites/all/themes/prometheus/assets/favicons/safari-pinned-tab.svg?v=yyxj0mow2Y" color="#5bbad5">
  <link rel="shortcut icon" href="/sites/all/themes/prometheus/assets/favicons/favicon.ico?v=yyxj0mow2Y">
  <meta name="apple-mobile-web-app-title" content="Prometheus">
  <meta name="application-name" content="Prometheus">
  <meta name="msapplication-config" content="/sites/all/themes/prometheus/assets/favicons/browserconfig.xml?v=yyxj0mow2Y">
  <meta name="theme-color" content="#ffffff">
  <!-- End of favicons -->
  <link type="text/css" rel="stylesheet" href="https://www.prometheusmedical.co.uk/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.prometheusmedical.co.uk/sites/default/files/css/css_gQNnDwLuNoKQivNl9ooF-I_ooL-cVxbrTIPulL1emxw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.prometheusmedical.co.uk/sites/default/files/css/css_PGbJgHCUCBf4dg7K9Kt8aAwsApndP4GZ9RuToPy3-Fk.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.prometheusmedical.co.uk/sites/default/files/css/css_5P7cWkoOujnHOcLdmlI5Kc80rX91AoQQruCPm_UPJCc.css" media="all" />
  <script src="https://www.prometheusmedical.co.uk/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script src="https://www.prometheusmedical.co.uk/sites/default/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-2698409-2", {"cookieDomain":"auto"});ga("require", "displayfeatures");ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.prometheusmedical.co.uk/sites/default/files/js/js_ljPYDJKrUmRfv8nPjyrAJX7w2g5efUp31fO9sf5gusA.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"prometheus","theme_token":"J7NErD4ayViX7meBsDshshc4gUZNCEky8uYs-2ymKuc","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/prometheus\/js\/main.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/domain\/domain_nav\/domain_nav.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/themes\/prometheus\/system.menus.css":1,"sites\/all\/themes\/prometheus\/system.messages.css":1,"sites\/all\/themes\/prometheus\/system.theme.css":1,"sites\/all\/themes\/prometheus\/css\/styles.css":1}},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackCrossDomains":["www.prometheusmedical.co.uk","www.prometheusmed.com.au","www.prometheusmed.ae","www.prometheusmed.co.za"]},"urlIsAjaxTrusted":{"\/search-results":true}});</script>
  <script>
      /* grunticon Stylesheet Loader | https://github.com/filamentgroup/grunticon | (c) 2012 Scott Jehl, Filament Group, Inc. | MIT license. */
      window.grunticon=function(e){if(e&&3===e.length){var t=window,n=!(!t.document.createElementNS||!t.document.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect||!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")||window.opera&&-1===navigator.userAgent.indexOf("Chrome")),o=function(o){var r=t.document.createElement("link"),a=t.document.getElementsByTagName("script")[0];r.rel="stylesheet",r.href=e[o&&n?0:o?1:2],a.parentNode.insertBefore(r,a)},r=new t.Image;r.onerror=function(){o(!1)},r.onload=function(){o(1===r.width&&1===r.height)},r.src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="}};
      grunticon(["/sites/all/themes/prometheus/assets/icons/icons.data.svg.css", "/sites/all/themes/prometheus/assets/icons/icons.data.png.css", "/sites/all/themes/prometheus/assets/icons/icons.fallback.css"]);
  </script>
  <noscript><link href="/sites/all/themes/prometheus/assets/icons/icons.fallback.css" rel="stylesheet"></noscript>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-147 node-type-homepage-uk domain-www-prometheusmedical-co-uk i18n-en" >
      <p id="skip-link">
      <a href="#main-menu" class="element-invisible element-focusable">Jump to navigation</a>
    </p>
      
<div id="navigation">
  <nav id="menu_wrapper">
    <div class="menu-content">
      <a class="brand uk" href="/"><span class="sr-only">Prometheus Medical Ltd</span></a>

      <div class="search clearfix">
        <form action="/search-results" method="get" id="views-exposed-form-search-page" accept-charset="UTF-8"><div><div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
          <div id="edit-search-api-views-fulltext-wrapper" class="views-exposed-widget views-widget-filter-search_api_views_fulltext">
                  <label for="edit-search-api-views-fulltext">
            Fulltext search          </label>
                        <div class="views-widget">
          <div class="form-item form-type-textfield form-item-search-api-views-fulltext">
 <input type="text" id="edit-search-api-views-fulltext" name="search_api_views_fulltext" value="" size="30" maxlength="128" class="form-text" />
</div>
        </div>
              </div>
                    <div class="views-exposed-widget views-submit-button">
      <input type="submit" id="edit-submit-search" name="" value="Apply" class="form-submit" />    </div>
      </div>
</div>
</div></form>      </div>

      <ul class="menu"><li class="menu__item is-active-trail is-leaf menu__item is-active-trail is-leaf first leaf active-trail"><a href="/" class="menu__link is-active-trail menu__link is-active-trail active-trail active">Home</a></li>
<li class="menu__item is-expanded menu__item is-expanded expanded"><button class="toggle-dropdown icon icon-dropdown-inactive"><span class="sr-only">Show sub menu</span></button><a href="/prometheus-training" class="menu__link menu__link">Courses</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-views"><div class="view view-courses view-id-courses view-display-id-courses_menu col-md-12 view-dom-id-5d1d88bbdc742115b42c275c70266996">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/e-advanced-life-support">E Advanced Life Support</a></div>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/emergencies-general-practice-gp-course">Emergencies In General Practice</a></div>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/emergency-first-aid-work-efaw">Emergency First Aid at Work (EFAW)</a></div>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/expemed-expedition-medicine-course">ExpeMed™ Expedition Medicine</a></div>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/first-aid-work-faw">First Aid at Work (FAW)</a></div>  </div>  </div>
  <div class="views-row views-row-6 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/viper-frec-level-3">FREC Level 3 / VIPER™</a></div>  </div>  </div>
  <div class="views-row views-row-7 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/viper-frec-level-3-refresher">FREC Level 3 Refresher / VIPER™</a></div>  </div>  </div>
  <div class="views-row views-row-8 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/frec-level-4">FREC Level 4</a></div>  </div>  </div>
  <div class="views-row views-row-9 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/uk-hems-course">HEMS Helicopter Crew</a></div>  </div>  </div>
  <div class="views-row views-row-10 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/mimms-major-incident-medical-management-and-support-course">MIMMS (Major Incident Medical Management and Support) Course</a></div>  </div>  </div>
  <div class="views-row views-row-11 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/pre-hospital-emergency-ultrasound-course-0">Pre-Hospital Emergency Ultrasound Course</a></div>  </div>  </div>
  <div class="views-row views-row-12 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/prometheus-team-medic-course">Prometheus Team Medic Course</a></div>  </div>  </div>
  <div class="views-row views-row-13 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/rtc-road-traffic-collision-course">Remote Road Traffic Collision Management &amp; Casualty Extrication</a></div>  </div>  </div>
  <div class="views-row views-row-14 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/selfsave">selfSAVE and selfSAVE+</a></div>  </div>  </div>
  <div class="views-row views-row-15 views-row-odd">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/sphere-emergency-resuscitation-course">SPHERe™ (Specialist Pre-Hospital Emergency Resuscitation) Course</a></div>  </div>  </div>
  <div class="views-row views-row-16 views-row-even">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/surviving-adventure-course">Surviving Adventure</a></div>  </div>  </div>
  <div class="views-row views-row-17 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-course-display-title">        <div class="field-content"><a href="/courses/tactical-care-officer-course">Tactical Care Officer</a></div>  </div>  </div>
    </div>
  
  
  
  
  
  
</div></li>
<li class="menu__item is-leaf menu__item is-leaf last leaf"><a href="/accommodation" class="menu__link menu__link">Accommodation</a></li>
</ul></li>
<li class="menu__item is-expanded menu__item is-expanded expanded"><button class="toggle-dropdown icon icon-dropdown-inactive"><span class="sr-only">Show sub menu</span></button><a href="/equipment" title="" class="menu__link menu__link">Equipment</a><ul class="menu"><li class="menu__item is-leaf first last leaf menu-views"><div class="view view-equipment view-id-equipment view-display-id-block_1 clearfix view-dom-id-b8fcd81384f74ff0c4fd3597e3606dd7">
            <div class="view-header">
      <div id="block-taxonomy-menu-block-1" class="block block-taxonomy-menu-block first odd">

        <h2 class="block__title block-title">Equipment Links</h2>
    
  <ul><li class="first"><a href="/equipment/prometheus-equipment">Prometheus Equipment</a></li><li><a href="/equipment/bleed-control">Bleed Control</a></li><li><a href="/equipment/simulation">Simulation</a><ul><li class="first"><a href="/equipment/simbodies">SIMBODIES</a></li><li><a href="/equipment/pro-simbodies">PRO SIMBODIES</a></li><li><a href="/equipment/task-trainers">Task Trainers</a></li><li><a href="/equipment/simbodies-simsleeves">SIMSLEEVES</a></li><li><a href="/equipment/simwounds">SIMWOUNDS</a></li><li><a href="/equipment/simsuits-simmasks-simpants">SIMSUITS &amp; SIMMASKS &amp; SIMPANTS</a></li><li class="last"><a href="/equipment/moulage-accessories">Moulage Accessories</a></li></ul></li><li><a href="/equipment/airway-breathing">Airway &amp; Breathing</a></li><li><a href="/equipment/training-education-equipment">Training &amp; Education Equipment</a></li><li><a href="/equipment/haemorrhage-control">Haemorrhage Control</a></li><li><a href="/equipment/splintage-immobilisation">Splintage &amp; Immobilisation</a></li><li><a href="/equipment/medical-bags-kits">Medical Bags &amp; Kits</a></li><li><a href="/equipment/monitors-ultrasound-defibrillators">Monitors, Ultrasound &amp; Defibrillators</a></li><li><a href="/equipment/specialist-bags-kits">Specialist Bags &amp; Kits</a></li><li><a href="/equipment/casualty-evacuation">Casualty Evacuation</a></li><li><a href="/equipment/hypothermia-prevention-management">Hypothermia Prevention &amp; Management</a></li><li><a href="/equipment/trauma-accessories">Trauma Accessories</a></li><li><a href="/equipment/intraosseous-access">Intraosseous Access</a></li><li><a href="/equipment/personal-protection">Personal Protection</a></li><li class="last"><a href="/equipment/miscellaneous">Miscellaneous</a></li></ul>
</div>
    </div>
  
  
  
      <div class="view-content">
      <div class="item-list">    <ul>          <li class="views-row views-row-1 views-row-odd views-row-first">  
  <div class="views-field views-field-name">        <span class="field-content">Prometheus Equipment</span>  </div></li>
          <li class="views-row views-row-2 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">SIMBODIES</span>  </div></li>
          <li class="views-row views-row-3 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Bleed Control</span>  </div></li>
          <li class="views-row views-row-4 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">PRO SIMBODIES</span>  </div></li>
          <li class="views-row views-row-5 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Simulation</span>  </div></li>
          <li class="views-row views-row-6 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Task Trainers</span>  </div></li>
          <li class="views-row views-row-7 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Airway &amp; Breathing</span>  </div></li>
          <li class="views-row views-row-8 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">SIMSLEEVES</span>  </div></li>
          <li class="views-row views-row-9 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">SIMWOUNDS</span>  </div></li>
          <li class="views-row views-row-10 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Training &amp; Education Equipment</span>  </div></li>
          <li class="views-row views-row-11 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Haemorrhage Control</span>  </div></li>
          <li class="views-row views-row-12 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">SIMSUITS &amp; SIMMASKS &amp; SIMPANTS</span>  </div></li>
          <li class="views-row views-row-13 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Moulage Accessories</span>  </div></li>
          <li class="views-row views-row-14 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Splintage &amp; Immobilisation</span>  </div></li>
          <li class="views-row views-row-15 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Medical Bags &amp; Kits</span>  </div></li>
          <li class="views-row views-row-16 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Monitors, Ultrasound &amp; Defibrillators</span>  </div></li>
          <li class="views-row views-row-17 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Specialist Bags &amp; Kits</span>  </div></li>
          <li class="views-row views-row-18 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Casualty Evacuation</span>  </div></li>
          <li class="views-row views-row-19 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Hypothermia Prevention &amp; Management</span>  </div></li>
          <li class="views-row views-row-20 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Trauma Accessories</span>  </div></li>
          <li class="views-row views-row-21 views-row-odd">  
  <div class="views-field views-field-name">        <span class="field-content">Intraosseous Access</span>  </div></li>
          <li class="views-row views-row-22 views-row-even">  
  <div class="views-field views-field-name">        <span class="field-content">Personal Protection</span>  </div></li>
          <li class="views-row views-row-23 views-row-odd views-row-last">  
  <div class="views-field views-field-name">        <span class="field-content">Miscellaneous</span>  </div></li>
      </ul></div>    </div>
  
  
  
  
  
  
</div></li>
</ul></li>
<li class="menu__item is-expanded menu__item is-expanded expanded"><button class="toggle-dropdown icon icon-dropdown-inactive"><span class="sr-only">Show sub menu</span></button><a href="/consultancy" class="menu__link menu__link">Consultancy</a><ul class="menu"><li class="menu__item is-leaf menu__item is-leaf first leaf"><a href="/consultancy/major-incident" class="menu__link menu__link">Major Incident Management</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/consultancy/medical-management-service" class="menu__link menu__link">Medical Management Service</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/consultancy/medical-needs-risk-assessment-and-operations-planning" class="menu__link menu__link">Medical Needs Risk Assessment and Operations Planning</a></li>
<li class="menu__item is-leaf menu__item is-leaf last leaf"><a href="/consultancy/remote-medical-assistance-and-deployable-medical-support" class="menu__link menu__link">Remote Medical Assistance</a></li>
</ul></li>
<li class="menu__item is-expanded menu__item is-expanded expanded"><button class="toggle-dropdown icon icon-dropdown-inactive"><span class="sr-only">Show sub menu</span></button><a href="/about-us" class="menu__link menu__link">About Us</a><ul class="menu"><li class="menu__item is-leaf menu__item is-leaf first leaf"><a href="/prometheus-people" class="menu__link menu__link">Prometheus People</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/about-us/working-prometheus-careers" class="menu__link menu__link">Working with Prometheus</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/about-us/find-us" class="menu__link menu__link">Conferences</a></li>
<li class="menu__item is-leaf menu__item is-leaf last leaf"><a href="/about-us/corporate-responsibility" class="menu__link menu__link">Corporate Responsibility</a></li>
</ul></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/news" class="menu__link menu__link">News</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/welcome-our-blog" title="" class="menu__link menu__link">Blog</a></li>
<li class="menu__item is-leaf menu__item is-leaf leaf"><a href="/contact-us" class="menu__link menu__link">Contact Us</a></li>
<li class="menu__item is-leaf menu__item is-leaf last leaf"><a href="/prometheus-mobile-applications" title="" class="menu__link menu__link">Prometheus Mobile APP</a></li>
</ul>     </div>

  </nav>
  </div>




<div id="page">
  <header class="header" id="header" role="banner">

        <div class="top-navbar clearfix">

            <div class="menu-trigger fixed">
      </div>

      <div id="mobile-header">
          <a href="#sidr-main" id="responsive-menu-button" class="icon icon-hamburger"><span class="sr-only">Trigger Menu</span></a>
      </div>

      <ul class="partner-sites">
        <li class="active"><a href="https://www.prometheusmedical.co.uk">UK</a></li>
        <li class=""><a href="http://www.prometheusmed.com.au">Australia</a></li>
        <li class=""><a href="http://www.prometheusmed.qa">MENA</a></li>
        <li class=""><a href="http://www.prometheusmed.co.za">Africa</a></li>
        <li><a href="/contact-us">Contact</a></li>
      </ul>

              <a class="cart-text" href="/cart">View your cart</a>
      
      <ul class="social">
                        <li><a class="icon-twitter" href="https://twitter.com/PrometheusMed" target="_blank"><span class="sr-only">Twitter</span></a></li>
                                  <li><a class="icon-facebook" href="https://www.facebook.com/pages/Prometheus-Medical-Ltd/136493926378315" target="_blank"><span class="sr-only">Facebook</span></a></li>
                                  <li><a class="icon-youtube" href="https://www.youtube.com/channel/UCGUHCGqfTePWZefXzHW7iqg" target="_blank"><span class="sr-only">Youtube</span></a></li>
                              <li><a class="icon-blog" href="/blog"><span class="sr-only">Blog</span></a></li>
      </ul>

              <div class="cart-summary mobile">
          <a href="/cart" class="cart-icon">
            <i class="icon icon-basket"></i>
            <span class="no-of-items">0</span>
                      </a>
        </div>
      
    </div>


    
                  <div class="banner-wrapper">


          <div class="banner-carousel">

                          <div class="cycle-nav">
                <div class="cycle-prev"><i class="icon icon-scenario-nav-left"><span class="sr-only">Previous</span></i></div>
                <div class="cycle-next"><i class="icon icon-scenario-nav-right"><span class="sr-only">Next</span></i></div>
              </div>
            
                          <div class="slide">

                                  <a href="major-incident/consultancy">
                                  <img src="https://www.prometheusmedical.co.uk/sites/default/files/styles/banner_large/public/Consultancy%20Banner%202.jpg?itok=ZbqocaFG" alt="" />
                                  </a>
                
                                    <div class="caption">
                      <div>
                        <p><strong>CONSULTANCY</strong></p>
                        <div class="caption-text">International expertise to assess, plan and manage routine medical risks or large scale emergencies</div>
                      </div>
                    </div>
                

              </div>
                          <div class="slide">

                                  <a href="equipment/monitors-ultrasound-defibrillators">
                                  <img src="https://www.prometheusmedical.co.uk/sites/default/files/styles/banner_large/public/Philips-AED-HS1v2.jpg?itok=KzYzzNu5" alt="" />
                                  </a>
                
                                    <div class="caption">
                      <div>
                        <p><strong>Philips Heartstart Defibrillators</strong></p>
                        <div class="caption-text">Extremely durable, well made and simple to use defibrillators.</div>
                      </div>
                    </div>
                

              </div>
                          <div class="slide">

                                  <a href="courses">
                                  <img src="https://www.prometheusmedical.co.uk/sites/default/files/styles/banner_large/public/Training%20Banner%201.jpg?itok=0OmcTAxS" alt="" />
                                  </a>
                
                                    <div class="caption">
                      <div>
                        <p><strong>TRAINING</strong></p>
                        <div class="caption-text">High quality training for a wide range of audiences; from basic first aid to advanced medical training</div>
                      </div>
                    </div>
                

              </div>
                          <div class="slide">

                                  <a href="equipment">
                                  <img src="https://www.prometheusmedical.co.uk/sites/default/files/styles/banner_large/public/Equipment%20Banner%201.jpg?itok=kdoS9Clz" alt="" />
                                  </a>
                
                                    <div class="caption">
                      <div>
                        <p><strong>EQUIPMENT</strong></p>
                        <div class="caption-text">An award-winning designer, manufacturer and supplier of state-of-the-art medical equipment</div>
                      </div>
                    </div>
                

              </div>
            
          </div>
          </div>

          
    
  </header>
  
  <div id="main" class="clearfix">

    <div id="content" class="container-fluid clearfix " role="main">
            <a id="main-content"></a>


                        
      
      
      <div class="row">
      




<article class="node-147 node node-homepage-uk node-promoted view-mode-full clearfix" about="/" typeof="sioc:Item foaf:Document">

  <div class="clearfix home-padding">

    <div class="col-md-12 align-center intro">
      <h1>Welcome to Prometheus Medical UK</h1>
      <div class="field field-name-field-sub-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even">Specialist Medical Equipment, Training &amp; Support by Elite Medical Professionals</div></div></div>              <div class="intro-spacing"></div>
          </div>

    <div class="welcome-content">
      <div class="col-md-6">
        <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p class="rtejustify"><strong>Prometheus is a leading supplier of pre-hospital emergency medical equipment and high-quality medical training courses for a wide audience.</strong></p>
<p class="rtejustify">Our specialist team has expertise in providing medical support under the most challenging and unique circumstances. Our in-depth knowledge, experience and credibility within both emergency medicine and the military environment means that we can offer our clients the best solution to their needs, from consultancy and strategic advice to the latest medical equipment and medical training.</p>
<p class="rtejustify">Whatever sector you belong to, whatever service you are looking for, our internationally respected medical consultants can provide you with cutting-edge advice, tailored to your exact requirements.</p>
<p class="rtejustify"><strong>We look forward to working with you.</strong></p>
</div></div></div>              </div>

      <div class="col-md-6">
        <div class="field field-name-field-home-right-col-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.prometheusmedical.co.uk/sites/default/files/styles/large/public/PneumoFix-8%20PDF109%20on%20SIMBODIES%20Male%203.jpg?itok=Kp2YVY9w" width="480" height="320" alt="" /></div></div></div>              </div>
    </div>

  </div>
  
    
      <div class="specialist clearfix">
      <div class="field field-name-field-specialist-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" src="https://www.prometheusmedical.co.uk/sites/default/files/styles/homepage_specialist/public/Image.jpg?itok=KAIp72C3" width="1400" height="500" alt="" /></div></div></div>      <div class="text">
        <div class="table">
          <div class="table-row">
            <div class="table-cell">
              <div class="content">
                <div class="field field-name-field-specialist-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even"><p>Specialist medical training</p>
</div></div></div>                <div class="field field-name-field-specialist-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p>High quality medical training is delivered by clinically accredited instructors, whose abilities range from delivering emergency first aid courses for the lay person, to teaching advanced techniques to the medical professional. Recognised and approved by professional and academic bodies including Qualsafe, University of Worcester, Anglia Ruskin University, IHCD, ALSG, MCA, RCS, WMS, UK HEMS, BAC and others, all courses offered are delivered at the highest possible standard, whatever qualification you are requiring.</p>
</div></div></div>                                  <a class="btn" href="/courses">View Courses</a>
                              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  <div class="action-buttons">
    <ul>
      <li class="team"><a class="icon icon-action-team" href="/prometheus-people">Meet the Team</a></li>
      <li class="working"><a class="icon icon-action-working" href="/about-us/working-prometheus-careers">Working with Prometheus</a></li>
      <li class="blog"><a class="icon icon-action-blog" href="/welcome-our-blog">Read our Blog</a></li>
    </ul>
  </div>

  <div class="equipment-row">
      </div>

</article>


      </div>
    </div>


    
  </div>
  
<footer id="footer" class="region region-footer" style="background-image: url(/sites/all/themes/prometheus/assets/images/map_uk.jpg);">

    <div class="company">
        <div class="content">
            <h4>Get in touch</h4>
            <ul class="address">
                <li>Prometheus Medical Ltd,</li><li>The Old Rectory,</li> <li>Hope Under Dinmore,</li> <li>Herefordshire,</li> <li>HR6 0PW</li>            </ul>
            <ul class="address">
                <li>Prometheus Medical Ltd,</li> <li>29 Bunhill Row,</li> <li>London,</li> <li>EC1Y 8LP</li>            </ul>
            <ul class="address">
                <li><strong>T:</strong> +44(0)1568 613942</li>                <li><strong>F:</strong> +44(0)1568 620032</li>                <li><a href="mailto:enquiries@prometheusmed.com"><strong>E:</strong> enquiries@prometheusmed.com</a></li>
            </ul>
            <div class="company-footer clearfix">
                <ul class="social">
                                            <li><a class="icon-twitter" href="https://twitter.com/PrometheusMed" target="_blank"><span class="sr-only">Twitter</span></a></li>
                                                                <li><a class="icon-facebook" href="https://www.facebook.com/pages/Prometheus-Medical-Ltd/136493926378315" target="_blank"><span class="sr-only">Facebook</span></a></li>
                                                                <li><a class="icon-youtube" href="https://www.youtube.com/channel/UCGUHCGqfTePWZefXzHW7iqg" target="_blank"><span class="sr-only">Youtube</span></a></li>
                                                            <li><a class="icon-blog" href="/blog" ><span class="sr-only">Blog</span></a></li>
                </ul>
                <div class="accreditation-queens">
                    <div class="accreditation">
                        <img src="/sites/all/themes/prometheus/assets/images/accreditation-queens.jpg" width="67" height="110" />
                        <img src="/sites/all/themes/prometheus/assets/images/accreditation-queens2.jpg" width="67" height="110" />

                                            </div>
                    <img class="awards" src="/sites/all/themes/prometheus/assets/images/awards.png" width="96" height="93" />
                    <img class="wage" src="/sites/all/themes/prometheus/assets/images/living_wage.png" width="116" height="93" />
                </div>
                <ul class="accreditation-list">
                    <li class="iip"><img src="/sites/all/themes/prometheus/assets/images/investors.jpg" width="150" height="50" /></li>
                    <li class="iip"><img src="/sites/all/themes/prometheus/assets/images/covenant.jpg" width="150" height="50" /></li>
                                        <li><img src="/sites/all/themes/prometheus/assets/images/accreditation-urs.jpg" width="116" height="52" /></li>
                </ul>
                                  <img src="/sites/all/themes/prometheus/assets/images/games-medical.jpg" class="games" title="Official Medical Consultant for the Special Olympics World Games" alt="Official Medical Consultant for the Special Olympics World Games">
                                <div class="accreditation-queens right">
                    <img class="cyber" src="/sites/all/themes/prometheus/assets/images/cyber_essentials_plus.png" width="116" height="93" />
                    <img class="wage" src="/sites/all/themes/prometheus/assets/images/living_wage.png" width="116" height="93" />
                </div>
                <ul class="terms">
                        <li><a href="/terms-conditions">Terms &amp; Conditions</a><li>
                        <li class="divide">&bull;<li>
                                                <li><a href="/privacy-policy">Privacy Policy</a></li>
                 </ul>
                <ul class="details">
                                            <li>Company Number 05379264</li>
                                                                <li><span>&bull;</span> VAT Number 881228418</li>
                                        <li></li>
                    <li><span>|&nbsp;</span><a href="https://www.orphans.co.uk" target="_blank">Website Design by Orphans</a></li>
                </ul>
            </div>
                        </div>
        </div>
    </footer>
</div>

    <script type="text/javascript" src="https://secure.leadforensics.com/js/103526.js" ></script>
  <noscript><img alt="" src="https://secure.leadforensics.com/103526.png" style="display:none;" /></noscript>
</body>
</html>
