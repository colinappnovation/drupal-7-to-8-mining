<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#">
  <head>
    <meta charset="utf-8" />
    <title>Webarchitects Co-operative | Webarchitects</title>
    <link rel="canonical" href="https://www.webarchitects.coop/" />
    <meta name="description" content="Webarchitects is a Sheffield based, small and friendly, multi-stakeholder co-operative which has been providing ethical and green, web hosting, virtual servers and GNU/Linux sysadmin support services for over 20 years." />
    <meta name="keywords" content="green web hosting, environmental web hosting, ethical web hosting, virtual servers, GNU/Linux, sysadmin, support" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Webarchitects Co-operative" />
    <meta property="og:description" content="Webarchitects is a Sheffield based, small and friendly, multi-stakeholder co-operative which has been providing ethical and green, web hosting, virtual servers and GNU/Linux sysadmin support services for over 20 years." />
    <meta property="og:site_name" content="Webarchitects" />
    <meta property="og:image" content="/wsh/webarch_500.png" />
    <meta property="og:locale" content="en_GB" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@webarchcoop" />
    <meta name="twitter:image" content="https://www.webarch.net/wsh/webarch_500.png" />
    <link rel="shortcut icon" href="/wsh/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="apple-touch-icon" sizes="80x80" href="/wsh/apple-touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/wsh/apple-touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="167x167" href="/wsh/apple-touch-icon-ipad-retina.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/wsh/apple-touch-icon-iphone-retina.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/pure-min.css" media="screen" />
    <!--[if lte IE 8]>
      <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/grids-responsive-old-ie-min.css" media="screen" />
    <![endif]-->
    <!--[if gt IE 8]><!-->
      <link type="text/css" rel="stylesheet" href="/wsh/lib/pure/0.6.0/grids-responsive-min.css" media="screen" />
    <!--<![endif]-->
    <link type="text/css" rel="stylesheet" href="/wsh/webarch.css?d=2017-12-20" media="screen" />
    <link type="text/css" rel="stylesheet" href="/wsh/webarch-print.css" media="print" />
    <script>
      // https://github.com/pure-css/pure/blob/master/LICENSE.md
      /*
      @licstart  The following is the entire license notice for the JavaScript code in this page.
      
      Software License Agreement (BSD License)
      ========================================
      
      Copyright 2013 Yahoo! Inc.
      
      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions are met:
      
          * Redistributions of source code must retain the above copyright
            notice, this list of conditions and the following disclaimer.
      
          * Redistributions in binary form must reproduce the above copyright
            notice, this list of conditions and the following disclaimer in the
            documentation and/or other materials provided with the distribution.
      
          * Neither the name of the Yahoo! Inc. nor the
            names of its contributors may be used to endorse or promote products
            derived from this software without specific prior written permission.
      
      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL YAHOO! INC. BE LIABLE FOR ANY
      DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
      ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
      SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
      @licend  The above is the entire license notice for the JavaScript code in this page.
      */
    </script>
    <script>
      // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPLv3 or later
      document.getElementById('wa-noscript-head').innerHTML = "";
      // @license-end
    </script>
    <link type="text/css" rel="stylesheet" href="/wsh/lib/highlight/9.14.2/styles/agate.css" media="all" />
    <script src="/wsh/lib/highlight/9.14.2/highlight.pack.js"></script>
    <script>
      // @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD 3-Clause License 
      hljs.initHighlightingOnLoad();
      // @license-end
    </script>
    <noscript id="wa-noscript-head">
      <style>
        @media (max-width:47.99em) { 
        .custom-wrapper { height: 12.2em; }
        div#menu div.pure-menu a#toggle { display: none; }
        }
      </style>
    </noscript>
  </head>
  <body> 
    <div class="pure-g"><div class="custom-wrapper" id="menu" itemscope itemtype="https://schema.org/WPHeader"><div class="pure-u-1 pure-u-md-1-3">
          <header>
            <div class="pure-menu">
              <a class="pure-menu-heading custom-brand" href="https://www.webarch.net/"><img class="wa-logo" itemprop="thumbnailUrl" src="/wsh/logo.png" alt="Webarchitects" title="Webarchitects" height="120" width="137" /><img class="wa-logo" src="/wsh/coop.png" alt="coop" title="Co-operative" height="120" width="209" /></a>
              <a href="#" class="custom-toggle" id="toggle"><span class="bar"></span><span class="bar"></span></a>
            </div>
          </header>
        </div><div class="pure-u-1 pure-u-md-2-3">
          <p class="wa-strapline">Sustainable, co-operative hosting you can depend on</p>
          <div class="pure-menu pure-menu-horizontal custom-can-transform">
            <nav itemscope itemtype="https://schema.org/SiteNavigationElement">
              <ul class="pure-menu-list">
                <li class="pure-menu-item wa-hosting"><a class="pure-menu-link" href="https://www.webarch.net/hosting" itemprop="url" title="Website &amp; email hosting, servers, virtual servers, colocation">Hosting</a></li>
                 <li class="pure-menu-item wa-support"><a class="pure-menu-link" href="https://www.webarch.net/support" itemprop="url" title="Webarchitects system adminstration &amp; development operations services.">Support</a></li>
                 <li class="pure-menu-item wa-about"><a class="pure-menu-link" href="https://www.webarch.net/about" itemprop="url" title="About Webarchitects co-op &amp; our ethics, principals &amp; organisational structure.">About</a></li>
                 <li class="pure-menu-item wa-help"><a class="pure-menu-link" href="https://www.webarch.net/help"  itemprop="url" title="Webarchitects help documentation.">Help</a></li>
              </ul>
            </nav>
          </div>
        </div><script>
          // http://purecss.io/layouts/tucked-menu-vertical/
          (function (window, document) {
            var menu = document.getElementById('menu'),
                WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange':'resize';
            function toggleHorizontal() {
              [].forEach.call(
                document.getElementById('menu').querySelectorAll('.custom-can-transform'),
                function(el){
                    el.classList.toggle('pure-menu-horizontal');
                }
              );
            };
            function toggleMenu() {
              // set timeout so that the panel has a chance to roll up
              // before the menu switches states
              if (menu.classList.contains('open')) {
                setTimeout(toggleHorizontal, 500);
              }
              else {
                toggleHorizontal();
              }
              menu.classList.toggle('open');
              document.getElementById('toggle').classList.toggle('x');
            };
            function closeMenu() {
              if (menu.classList.contains('open')) {
                toggleMenu();
              }
            }
            document.getElementById('toggle').addEventListener('click', function (e) {
              toggleMenu();
            });
            window.addEventListener(WINDOW_CHANGE_EVENT, closeMenu);
          })(this, this.document);
        </script></div>
    </div><div class="pure-g">
      <div class="pure-u-1 pure-u-md-3-4 pure-u-lg-4-5 pure-u-xl-5-6">
        <div class="wa-content wa-title">
          <h1 id="title">Webarchitects Co-operative <a href="#title" class="a">#</a></h1>
        </div>
        <div class="wa-content wa-description" itemscope itemtype="https://schema.org/Thing">
          <p id="description" itemprop="description">Webarchitects is a Sheffield based, small and friendly, multi-stakeholder co-operative which has been providing ethical and green, web hosting, virtual servers and GNU/Linux sysadmin support services for over 20 years.</p>
        </div>
        <div itemscope itemtype="http://schema.org/WebPage">
          <div class="wa-content" itemprop="mainContentOfPage">


<p>See our <a href="https://www.webarch.info/">server status page</a> for the
latest news regarding the status of our systems and please <a href="/contact">get in
touch</a> if you have any issues, questions or think we might be able to
provide a service to you.</p>

</div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-hosting">
  <h2 id="hosting">Hosting</h2>
  <ul>

<li>Secure, <a href="/co-operative">co-operative</a>, <a
href="/ethics">ethical</a> and <a href="/green-energy">green</a> web <a
href="/shared-hosting">hosting</a> and <a href="/email">email</a>
services</li>

<li><a href="/wordpress">WordPress</a>, <a href="/discourse">Discourse</a>, <a
href="/mediawiki">MediaWiki</a>, <a href="/nextcloud">Nextcloud</a> and many
other applications can be installed and <a href="/support">supported</a> on our
<a href="/shared-hosting">shared hosting servers</a> and <a
href="/virtual-servers">virtual private servers</a></li>

<li><a href="/certs#letsencrypt">HTTPS certificates</a> provided with all
hosting packages</li>

<li>Powered by <a href="/green-energy">renewable energy</a> and hosted in the UK.</li>

<li><a href="/hosting" class="pure-button pure-button-primary
wa-button">Hosting Packages</a></li>

  </ul>





  <!-- Seconds: 44 -->
  </div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-hosting">
  <div class="wa-fragment">
  <ul>
    <li><a id="discourse" href="/discourse"><img src="/img/discourse.png" alt="Discourse" /></a></li>
    <li><a href="/discourse">Discourse</a> is an elegant, modern and easy to use web based discussion board / forum application for building communities 
    <li>An ideal platform for enabling peer to peer support and building online communities</li>
    <!-- li>Discourse / Docker install <a href="https://git.coop/webarch/discourse">automated with Ansible<</a>/li -->
    <li>Choice of <a href="/discourse">virtual private server packages and support</a> to suit the needs of the site</li>
    <li><a title="Webarchitects Discourse Hosting" href="/discourse" class="pure-button pure-button-primary wa-button">Discourse</a></li>
  </ul>
</div>

  </div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-hosting">
  <div class="wa-fragment">
  <ul>
    <li><a id="nextcloud" href="/nextcloud"><img src="/img/nextcloud.png" alt="Nextcloud" /></a></li>
    <li><a href="/nextcloud">Nextcloud</a> is a web based office application server, offering document editing, sharing, contacts, calendar and more</li>
    <li>Optional Collabora <abbr title="what you see if what you get">WYSIWYG</abbr> editor for editing documents and spreadsheets via the web</li>
    <li>Choice of <a href="/nextcloud">virtual private server packages and support</a></li>
    <li><a title="Webarchitects Nextcloud Hosting" href="/nextcloud" class="pure-button pure-button-primary wa-button">Nextcloud</a></li>
  </ul>
</div>




</div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-support">

<h2 id="support">Support from <span class="wa-price">&pound;90</span> per hour</h2>
      
<ul>

<li>Debian and Ubuntu GNU/Linux <a href="/support">system administration</a>,
devops, testing, installing, operating, <a href="/monitoring">monitoring</a>,
performance, data <a href="/backups">backups</a> and security</li>   

<li>Years of in depth, hands-on, experience of <a href="/web-servers">Apache,
Nginx, PHP</a>, MySQL, Exim, Postfix and other server applications</li>

<li>Server virtualization using the Xen hypervisor and Docker containers</li> 

<li>Devops &mdash; automation of server configuration and maintenance using <a
href="/development">Ansible and Bash</a></li>

<li>Extensive <a href="/hardware">hardware experience</a>, installing open
source operating systems on laptops, desktops, phones, tablets, servers and
routers</li> 

<li><a href="/support" class="pure-button pure-button-primary wa-button">Support</a></li>

</ul>

</div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-about">

<h2 id="about">About</h2>

<ul>

<li>Webarchitects has been providing services for <a href="/20th">over 20 years</a>
to individuals and progressive organisations</li>

<li>Webarchitects is a <a href="/co-operative">multi-stakeholder
co-operative</a> and <a href="/ethics">ethical</a> social enterprise which <a
href="/join">you can join</a> as a client, partner or investor</li>

<li>Dedicated to the philosophy, use, development and promotion of <a
href="/floss">Free/Libre Open Source Software</a> and the wider
sharing/peer-to-peer economy and co-operative mode of production</li> 

<li>Membership grants access to our <a href="https://git.coop/">GitLab CE servers at git.coop</a></li>

<li>All our Sheffield based systems are powered by <a href="/green-energy">Green energy</a>
from Good Energy</li>

<li><a href="/about" class="pure-button pure-button-primary wa-button">About
our co-op</a></li>

</ul>
    
</div></div><div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3"><div class="wa-box wa-box-help">

<h2 id="help">Help</h2>

<ul>
<li>News about our <a href="https://www.webarch.info/">server status</a> is posted to <a href="https://www.webarch.info/">webarch.info</a></li>
<li>The best way to contact us is via email, open a support ticket: <br /><a href="mailto:support@webarchitects.coop">support@webarchitects.coop</a></li>
<li>Live IRC chat:<br /><a href="https://webchat.freenode.net/?channels=%23webarch">irc.freenode.net#webarch</a></li>
<li>Call, weekdays, 9am - 3pm:<br />0114 276 9709</li>
<li>Server &amp; systems help:<br /><a href="https://docs.webarch.net/wiki/Main_Page">documentation wiki</a></li>
<li><a href="https://members.webarchitects.coop/">Members forum</a></li>
<li><a href="/help" class="pure-button pure-button-primary wa-button">Help</a></li>
</ul>

</div></div><div class="pure-u-1"><div class="wa-content">

<p>Webarchitects is a <a href="/rules">multi-stakeholder co-operative</a> made
up of workers, partners, clients and investors that <a href="/join">you can
join</a> in order to participate in decision making and receive membership
benefits, the purpose of our <a href="/co-operative">co-operative</a> is:</p>
<blockquote>
<p>To enable the provision of internet based services for socially responsible groups and individuals, using free open source software wherever possible, in a manner that aims to minimise fossil fuel usage and ecological impacts and which also provides sustainable employment</p>
</blockquote>


          </div></div>
        <div class="wa-content wa-updated">
            <p id="updated" itemscope itemtype="http://schema.org/CreativeWork">Last updated: <time itemprop="dateModified" datetime="2019-05-22T17:49">Wednesday, 22 May 2019, 05:49:01 PM</time></p>
        </div>
      </div><div class="pure-u-1 pure-u-md-1-4 pure-u-lg-1-5 pure-u-xl-1-6 wa-section-menu">
            <nav itemscope itemtype="https://schema.org/WPSideBar">
              <ul class="pure-menu-list wa-hosting">
                <li class="pure-menu-item"><a class="pure-menu-link" href="/hosting">Hosting</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/wordpress">WordPress</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/discourse">Discourse</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/mediawiki">MediaWiki</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/mailcow">Mailcow</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/mailman">Mailman</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/nextcloud">Nextcloud</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/matomo">Matomo</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/git">Git Hosting</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/shared-hosting">Web Hosting</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/virtual-servers">Virtual Servers</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/email">Email &amp; Lists</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/domain-names">Domain Names</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/archiving">Website Archiving</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/certs">Certificates</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/ip-addresses">IP Addresses</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/backup-space">Backup Space</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/colocation">Colocation</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/ecohost">Ecohost</a></li>
                <!-- li class="pure-menu-item"><a class="pure-menu-link" href="/ecodissident">Ecodissident</a></li -->
                <li class="pure-menu-item"><a class="pure-menu-link" href="/payments">Payments</a></li>
              </ul>
              <ul class="pure-menu-list wa-support">
                <li class="pure-menu-item"><a class="pure-menu-link" href="/support">Support</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/development">Development</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/contracts">Contracts</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/hardware">Hardware</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/applications">Applications</a></li>
                <!-- li class="pure-menu-item"><a class="pure-menu-link" href="/design">Design</a></li -->
                <li class="pure-menu-item"><a class="pure-menu-link" href="/gnu-linux">GNU/Linux</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/web-servers">Web Servers</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/monitoring">Monitoring</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/backups">Backups</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/testimonials">Testimonials</a></li>
              </ul>
              <ul class="pure-menu-list wa-about">
                <li class="pure-menu-item"><a class="pure-menu-link" href="/about">About</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/ethics">Ethics</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/co-operative">Co-operative</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/join">Join Us</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/partners">Partners</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/floss">Free Software</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/free-speech">Free Speech</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/green-energy">Green Energy</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/policies">Policies</a></li>
              </ul>
              <ul class="pure-menu-list wa-help">
                <li class="pure-menu-item"><a class="pure-menu-link" href="/help">Help</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/docs">Documentation</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/contact">Contact</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/complaints">Complaints</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/abuse">Abuse</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="/response-times">Response Times</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.info/">Service Status</a></li>
              </ul>
          </nav>
      </div></div>
    <div class="pure-g wa-footer"><div class="pure-u-1 pure-u-md-1-4 wa-footer">
        <div class="wa-footer" id="footer">
          <nav>
            <div class="pure-menu">
              <ul class="pure-menu-list">
		<li class="pure-menu-item"><a class="pure-menu-link" href="https://www.blog.webarchitects.coop/">Blog</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.net/about">About</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.net/contact">Contact Us</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.info/">Service Status</a></li>
		<li class="pure-menu-item"><a class="pure-menu-link" href="https://members.webarchitects.coop/">Members Forum</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.net/sitemap">Sitemap</a></li>
              </ul>
            </div>
          </nav>
        </div>
      </div><div class="pure-u-1 pure-u-md-3-4 wa-footer">
          <div class="wa-footer" id="footer-address" itemscope itemtype="http://schema.org/Organization">
            <div class="pure-menu">
              <ul class="pure-menu-list">
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled"><span itemprop="name">Webarchitects</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled"><span itemprop="telephone">+44 114 276 9709</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="mailto:support@webarchitects.coop" itemprop="email">support@webarchitects.coop</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">68a John Street</span>, <span itemprop="addressRegion">Sheffield</span>, <span itemprop="postalCode">S2 4QU</span>, <span itemprop="addressCountry">UK</span></a></li>
                <li class="pure-menu-item"><a class="pure-menu-link pure-menu-disabled">Registration no. 31305R</a></li>
                <li class="pure-menu-item"><a class="pure-menu-link" href="https://www.webarch.net/privacy">Privacy Policy</a></li>
              </ul>
            </div>
          </div>
      </div><div class="pure-u-1 pure-u-md-5-6 wa-footer">
          <div class="wa-footer" id="footer-membership">
            <a href="https://webarch.coops.tech/" title="Co-operative Technologists Member"><img id="cotech" class="desaturate" src="/wsh/cotech_colour.png" alt="CoTech" width="100" height="100" /></a>
            <a href="http://www.radicalroutes.org.uk/" title="Radical Routes Associate Member"><img id="radicalroutes" class="desaturate" src="/wsh/rr_colour.png" alt="Radical Routes" width="185" height="100" /></a>
            <a href="https://www.openrightsgroup.org/about/sponsors" title="Open Rights Group Corporate Sponsor"><img id="org" class="desaturate" src="/wsh/org_colour.png" alt="Open Rights Group" width="100" height="100" /></a>
            <a href="https://www.uk.coop/directory/location/webarchitects-r006394" title="Co-operatives UK Member"><img id="coopuk" class="desaturate" src="/wsh/coopuk_colour.png" alt="Co-operatives UK" width="184" height="100" /></a>
            <a href="https://www.nominet.uk/" title="Nominet Registrar Member"><img id="nominet" class="desaturate" src="/wsh/nominet_colour.png" alt="Nominet" width="100" height="100" /></a>
            <a href="https://community.jisc.ac.uk/library/janet-services-documentation/registrar-membership" title="Jisc Registrar Member"><img id="jisc" class="desaturate" src="/wsh/jisc_colour.png" alt="Jisc" width="172" height="100" /></a>
          </div>
      </div><div class="pure-u-1 pure-u-md-1-6 wa-footer">
          <div class="wa-footer" id="footer-socialmedia">
            <a href="https://www.linkedin.com/company/webarchcoop/" title="Webarchitects on LinkedIn"><img id="linkedin" class="desaturate" src="/wsh/linkedin_colour.png" alt="LinkedIn" width="25" height="25" /></a>
            <a href="https://twitter.com/webarchcoop" title="Webarchitects on Twitter"><img id="twitter" class="desaturate" src="/wsh/twitter_colour.png" alt="Twitter" width="25" height="25" /></a>
            <a href="https://github.com/webarch-coop" title="Webarchitects on GitHub"><img id="github" class="desaturate" src="/wsh/github_colour.png" alt="GitHub" width="25" height="25" /></a>
            <a href="https://gitlab.com/webarch" title="Webarchitects on GitLab"><img id="gitlab" class="desaturate" src="/wsh/gitlab_colour.png" alt="GitLab" width="25" height="25" /></a>
          </div>
      </div><div class="pure-u-1 wa-footer">
          <div class="wa-footer" id="footer-copyright" itemscope itemtype="http://schema.org/Organization">
            <p>&copy; 2019 <span itemprop="legalName">Webarch Co-operative Limited</span></p> 
	  </div>
          <p id="wa-footer-javascript"><small><a class="pure-menu-link" href="https://www.webarch.net/js" rel="jslicense">JavaScript license information</a></small></p>
      </div>
    <script>
      // @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD-3-Clause
      var _paq = _paq || [];
      _paq.push(['disableCookies']);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//stats.webarch.net/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', 1]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
      // @license-end
    </script>
    </div>
  </body>
</html>




