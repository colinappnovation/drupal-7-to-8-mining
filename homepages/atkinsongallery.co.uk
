<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta charset="utf-8"><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"a2040dbc33",applicationID:"227532046"};window.NREUM||(NREUM={}),__nr_require=function(n,e,t){function r(t){if(!e[t]){var i=e[t]={exports:{}};n[t][0].call(i.exports,function(e){var i=n[t][1][e];return r(i||e)},i,i.exports)}return e[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<t.length;i++)r(t[i]);return r}({1:[function(n,e,t){function r(){}function i(n,e,t){return function(){return o(n,[u.now()].concat(f(arguments)),e?null:this,t),e?void 0:this}}var o=n("handle"),a=n(4),f=n(5),c=n("ee").get("tracer"),u=n("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(n,e){s[e]=i(d+e,!0,"api")}),s.addPageAction=i(d+"addPageAction",!0),s.setCurrentRouteName=i(d+"routeName",!0),e.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(n,e){var t={},r=this,i="function"==typeof e;return o(l+"tracer",[u.now(),n,t],r),function(){if(c.emit((i?"":"no-")+"fn-start",[u.now(),r,i],t),i)try{return e.apply(this,arguments)}catch(n){throw c.emit("fn-err",[arguments,this,n],t),n}finally{c.emit("fn-end",[u.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(n,e){m[e]=i(l+e)}),newrelic.noticeError=function(n,e){"string"==typeof n&&(n=new Error(n)),o("err",[n,u.now(),!1,e])}},{}],2:[function(n,e,t){function r(n,e){var t=n.getEntries();t.forEach(function(n){"first-paint"===n.name?a("timing",["fp",Math.floor(n.startTime)]):"first-contentful-paint"===n.name&&a("timing",["fcp",Math.floor(n.startTime)])})}function i(n){if(n instanceof c&&!s){var e,t=Math.round(n.timeStamp);e=t>1e12?Date.now()-t:f.now()-t,s=!0,a("timing",["fi",t,{type:n.type,fid:e}])}}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var o,a=n("handle"),f=n("loader"),c=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){o=new PerformanceObserver(r);try{o.observe({entryTypes:["paint"]})}catch(u){}}if("addEventListener"in document){var s=!1,p=["click","keydown","mousedown","pointerdown","touchstart"];p.forEach(function(n){document.addEventListener(n,i,!1)})}}},{}],3:[function(n,e,t){function r(n,e){if(!i)return!1;if(n!==i)return!1;if(!e)return!0;if(!o)return!1;for(var t=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var f=navigator.userAgent,c=f.match(a);c&&f.indexOf("Chrome")===-1&&f.indexOf("Chromium")===-1&&(i="Safari",o=c[1])}e.exports={agent:i,version:o,match:r}},{}],4:[function(n,e,t){function r(n,e){var t=[],r="",o=0;for(r in n)i.call(n,r)&&(t[o]=e(r,n[r]),o+=1);return t}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],5:[function(n,e,t){function r(n,e,t){e||(e=0),"undefined"==typeof t&&(t=n?n.length:0);for(var r=-1,i=t-e||0,o=Array(i<0?0:i);++r<i;)o[r]=n[e+r];return o}e.exports=r},{}],6:[function(n,e,t){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(n,e,t){function r(){}function i(n){function e(n){return n&&n instanceof r?n:n?c(n,f,o):o()}function t(t,r,i,o){if(!d.aborted||o){n&&n(t,r,i);for(var a=e(i),f=v(t),c=f.length,u=0;u<c;u++)f[u].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(n,e){h[n]=v(n).concat(e)}function m(n,e){var t=h[n];if(t)for(var r=0;r<t.length;r++)t[r]===e&&t.splice(r,1)}function v(n){return h[n]||[]}function g(n){return p[n]=p[n]||i(t)}function w(n,e){u(n,function(n,t){e=e||"feature",y[t]=e,e in s||(s[e]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:e,buffer:w,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var f="nr@context",c=n("gos"),u=n(4),s={},p={},d=e.exports=i();d.backlog=s},{}],gos:[function(n,e,t){function r(n,e,t){if(i.call(n,e))return n[e];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return n[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(n,e,t){function r(n,e,t,r){i.buffer([n],r),i.emit(n,e,t)}var i=n("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(n,e,t){function r(n){var e=typeof n;return!n||"object"!==e&&"function"!==e?-1:n===window?0:a(n,o,function(){return i++})}var i=1,o="nr@id",a=n("gos");e.exports=r},{}],loader:[function(n,e,t){function r(){if(!x++){var n=E.info=NREUM.info,e=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(n&&n.licenseKey&&n.applicationID&&e))return s.abort();u(y,function(e,t){n[e]||(n[e]=t)}),c("mark",["onload",a()+E.offset],null,"api");var t=l.createElement("script");t.src="https://"+n.agent,e.parentNode.insertBefore(t,e)}}function i(){"complete"===l.readyState&&o()}function o(){c("mark",["domContent",a()+E.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(f=Math.max((new Date).getTime(),f))-E.offset}var f=(new Date).getTime(),c=n("handle"),u=n(4),s=n("ee"),p=n(3),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1153.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),E=e.exports={offset:f,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};n(1),n(2),l[m]?(l[m]("DOMContentLoaded",o,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",i),d[v]("onload",r)),c("mark",["firstbyte",f],null,"api");var x=0,O=n(6)},{}],"wrap-function":[function(n,e,t){function r(n){return!(n&&n instanceof Function&&n.apply&&!n[a])}var i=n("ee"),o=n(5),a="nr@original",f=Object.prototype.hasOwnProperty,c=!1;e.exports=function(n,e){function t(n,e,t,i){function nrWrapper(){var r,a,f,c;try{a=this,r=o(arguments),f="function"==typeof t?t(r,a):t||{}}catch(u){d([u,"",[r,a,i],f])}s(e+"start",[r,a,i],f);try{return c=n.apply(a,r)}catch(p){throw s(e+"err",[r,a,p],f),p}finally{s(e+"end",[r,a,c],f)}}return r(n)?n:(e||(e=""),nrWrapper[a]=n,p(n,nrWrapper),nrWrapper)}function u(n,e,i,o){i||(i="");var a,f,c,u="-"===i.charAt(0);for(c=0;c<e.length;c++)f=e[c],a=n[f],r(a)||(n[f]=t(a,u?f+i:i,o,f))}function s(t,r,i){if(!c||e){var o=c;c=!0;try{n.emit(t,r,i,e)}catch(a){d([a,t,r,i])}c=o}}function p(n,e){if(Object.defineProperty&&Object.keys)try{var t=Object.keys(n);return t.forEach(function(t){Object.defineProperty(e,t,{get:function(){return n[t]},set:function(e){return n[t]=e,e}})}),e}catch(r){d([r])}for(var i in n)f.call(n,i)&&(e[i]=n[i]);return e}function d(e){try{n.emit("internal-error",e)}catch(t){}}return n||(n=i),t.inPlace=u,t.flag=a,t}},{}]},{},["loader"]);</script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_16x16.gif" sizes="16x16">
  <link rel="shortcut icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_24x24.gif" sizes="24x24">
  <link rel="shortcut icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_32x32.gif" sizes="32x32">
  <link rel="shortcut icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_64x64.gif" sizes="64x64">

  <link rel="apple-touch-icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_16x16.gif" sizes="16x16">
  <link rel="apple-touch-icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_24x24.gif" sizes="24x24">
  <link rel="apple-touch-icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_32x32.gif" sizes="32x32">
  <link rel="apple-touch-icon" href="/sites/all/themes/bootstrap3_millfield/millfield_fav_64x64.gif" sizes="64x64">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-site-verification" content="w1DfMC9NKEHFub-e9kKqUPfTe0-mVgslxEf9B-47Zbc" />
<!-- Global site tag (gtag.js) - Google AdWords: 990561941 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-990561941"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'AW-990561941');
</script>
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://atkinsongallery.co.uk/" />
<link rel="shortlink" href="https://atkinsongallery.co.uk/" />
  <title>Atkinson Gallery |</title>
  <style>
@import url("https://atkinsongallery.co.uk/modules/system/system.base.css?q165lg");
</style>
<style>
@import url("https://atkinsongallery.co.uk/sites/all/modules/contrib/date/date_api/date.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/all/modules/contrib/date/date_popup/themes/datepicker.1.7.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/all/modules/contrib/domain/domain_nav/domain_nav.css?q165lg");
@import url("https://atkinsongallery.co.uk/modules/field/theme/field.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/all/modules/custom/millfield_popup/css/popup.css?q165lg");
@import url("https://atkinsongallery.co.uk/modules/node/node.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/all/modules/contrib/views/css/views.css?q165lg");
</style>
<style>
@import url("https://atkinsongallery.co.uk/sites/all/modules/contrib/ctools/css/ctools.css?q165lg");
</style>
<style>#sliding-popup.sliding-popup-bottom{background:#0779BF;}#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text p{color:#ffffff !important;}
</style>
<style>
@import url("https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/supersized/slideshow/css/supersized.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/supersized/slideshow/theme/supersized.shutter.css?q165lg");
</style>
<style>
@import url("https://atkinsongallery.co.uk/sites/default/files/less/define-fonts.jHnyTC7JVrDCRa_nsfm0Csa3k7TkC1uuUeT9K_5ySkw.css?q165lg");
@import url("https://atkinsongallery.co.uk/sites/default/files/less/style.EVGlv2LApHAClii5ufN4mlhwbTKOScB8BoOn7C5YNXA.css?q165lg");
</style>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/contrib/jquery_update/replace/jquery/1.7/jquery.min.js'>\x3C/script>")</script>
<script src="https://atkinsongallery.co.uk/misc/jquery-extend-3.4.0.js?v=1.7.2"></script>
<script src="https://atkinsongallery.co.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://atkinsongallery.co.uk/misc/drupal.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/modules/contrib/webform_steps/webform_steps.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/modules/contrib/google_analytics/googleanalytics.js?q165lg"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-49810072-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script>var rl_siteid = "54c861e3-ea5e-4f65-b27c-221ffe40284c";</script>
<script src="//cdn.rlets.com/capture_static/mms/mms.js"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/supersized/slideshow/js/jquery.easing.min.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/supersized/slideshow/js/supersized.3.2.7.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/supersized/slideshow/theme/supersized.shutter.min.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/js/vendor/jquery.placeholder.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/js/plugins.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/js/main.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/js/analytics-events.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/js/lunametrics-youtube-v6.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/affix.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/alert.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/button.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/carousel.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/collapse.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/dropdown.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/modal.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/tooltip.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/popover.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/scrollspy.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/tab.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield/bootstrap/js/transition.js?q165lg"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap3_millfield_atkinson","theme_token":"YzuxyHEK7xgQ_O_NkYzQvkIySoZBdbZa5dibO8RmmNA","js":{"sites\/all\/modules\/contrib\/eu-cookie-compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.7.2\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/webform_steps\/webform_steps.js":1,"sites\/all\/modules\/contrib\/google_analytics\/googleanalytics.js":1,"1":1,"2":1,"\/\/cdn.rlets.com\/capture_static\/mms\/mms.js":1,"sites\/all\/themes\/bootstrap3_millfield\/supersized\/slideshow\/js\/jquery.easing.min.js":1,"sites\/all\/themes\/bootstrap3_millfield\/supersized\/slideshow\/js\/supersized.3.2.7.js":1,"sites\/all\/themes\/bootstrap3_millfield\/supersized\/slideshow\/theme\/supersized.shutter.min.js":1,"sites\/all\/themes\/bootstrap3_millfield\/js\/vendor\/jquery.placeholder.js":1,"sites\/all\/themes\/bootstrap3_millfield\/js\/plugins.js":1,"sites\/all\/themes\/bootstrap3_millfield\/js\/main.js":1,"sites\/all\/themes\/bootstrap3_millfield\/js\/analytics-events.js":1,"sites\/all\/themes\/bootstrap3_millfield\/js\/lunametrics-youtube-v6.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/affix.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/alert.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/button.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/carousel.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/collapse.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/dropdown.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/modal.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/tooltip.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/popover.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/scrollspy.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/tab.js":1,"sites\/all\/themes\/bootstrap3_millfield\/bootstrap\/js\/transition.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"sites\/all\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/contrib\/domain\/domain_nav\/domain_nav.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/custom\/millfield_popup\/css\/popup.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"0":1,"sites\/all\/themes\/bootstrap3_millfield\/supersized\/slideshow\/css\/supersized.css":1,"sites\/all\/themes\/bootstrap3_millfield\/supersized\/slideshow\/theme\/supersized.shutter.css":1,"sites\/all\/themes\/bootstrap3_millfield\/less\/define-fonts.less":1,"sites\/all\/themes\/bootstrap3_millfield_atkinson\/less\/style.less":1}},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":0,"popup_html_info":"\n\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info alert alert-info row\u0022\u003E\n    \u003Cdiv class=\u0022popup-inner\u0022\u003E\n      \u003Cdiv id=\u0022popup-text\u0022 class=\u0022col-md-8\u0022\u003E\n        \u003Cp\u003EWe use cookies on this site to enhance your user experience. By clicking any link on this page you are giving your consent for us to set cookies.\u003C\/p\u003E      \u003C\/div\u003E\n      \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022col-md-4\u0022\u003E\n        \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button btn btn-default\u0022\u003EDon\u0027t show this message again\u003C\/button\u003E\n      \u003C\/div\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button\u0022\u003E\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003E\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/www.example.com","popup_link_new_window":1,"popup_position":null,"popup_language":"en","domain":""},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true},"bootstrap":{"anchorsFix":1,"anchorsSmoothScrolling":1,"formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-162 node-type-ag-homepage-t2ag- domain-ismith-gallery-positivestudio-co-uk no-touch" >
    
<div class="main-container container">

    <header role="banner" id="page-header">
        <div class="logo-container">
      <div id="skip-link">
        <a href="#navbar" class="element-invisible element-focusable">Skip to navigation</a>
      </div>
      <div class="logo-container-inner">
        <div class="logo">
          <a  href="/" title="Home">
            <img src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap3_millfield_atkinson/images_atkinson/atkinson_gallery_logo.png" alt="Millfield logo" />
          </a>
        </div>
      </div>
    </div>
        <div id='mobile-menu-toggle'><span class='text'>Menu</span><span class='glyphicon glyphicon-menu-hamburger open-menu'></span><span class='glyphicon glyphicon-remove close-menu'></span></div>
  </header> <!-- /#header -->

  <div class="row">

    
    <section class="col-sm-12">
                        <!-- <div class="page-title">
        <h1 class="page-header sr-only">Welcome to the Atkinson Gallery</h1>
      </div> -->
            
      <div class="section-inner">
        <div id="caption_holder" class="row inactive">
          <div id="slidecaption" class="col-md-12"></div>
          <div id="slidecaption-bg"></div>
        </div>
        <a id="main-content"></a>
                                                                </div>

      <!-- <div class="breadcrumb-container">
        <div class="breadcrumb-container-inner">
                  </div>
      </div>

      <div class="page-title">
        <h1 class="page-header">Welcome to the Atkinson Gallery</h1>
      </div> -->

      
<article id="node-162" class="node node-ag-homepage-t2ag- node-full clearfix" about="/atkinson-gallery-home" typeof="sioc:Item foaf:Document">
  <div class="article-inner">

    <div class="row">

      <div class="col-md-8 col-sm-8">

        <div class="breadcrumb-container">
          <div class="breadcrumb-container-inner">
                    </div>
        </div>

        <div class="page-title">
          <h1 class="page-header">Welcome to the Atkinson Gallery</h1>
        </div>

        <div class="wysiwyg-area">
          <div class="field field-name-body field-type-text-with-summary field-label-hidden">
    <div class="field-items">
          <div class="field-item even" property="content:encoded"><p class="wysiwyg-intro-copy">Established in 1992, the Atkinson Gallery is nationally recognised and holds major contemporary art exhibitions which are open to the general public. It aims to support young, aspiring artists and bring high calibre, international art to the South West region.</p><p class="wysiwyg-body-copy">Millfield&rsquo;s stimulating arts landscape is also enhanced by a unique sculpture collection within our extensive parkland that features large scale permanent works from artists such as Glynn Williams and Peter Randall-Page.</p><p class="wysiwyg-body-copy"><strong>Some past exhibitors</strong></p><p class="wysiwyg-body-copy">Andy Warhol,&nbsp;Albert Irvin OBE, Sir Anthony Caro, Lynn Chadwick CBE, Gillian Ayres, Don McCullin, Graham Crowley, Marc Quinn, Dame Elisabeth Frink, Sophie Ryder, Peter Randall-Page, Doug Cocker, Glynn Williams, Anthony Green, Tom Wood, Basil Beattie, Clyde Hopkins and Terry Frost.</p></div>
      </div>
</div>        </div>
        
      </div>

      <div class="col-md-4 col-sm-4">

        <div class="gallery-details">
          <h2>Gallery details</h2>
          <div class="field field-name-field-sidebar-text-item-t2ag- field-type-text-long field-label-hidden">
    <div class="field-items">
          <div class="field-item even"><h3 class="wysiwyg-body-copy">Opening Hours</h3><p class="wysiwyg-body-copy">Monday - Saturday: 9.30am - 5.00pm<br />(only during exhibitions)<br />Sunday - Closed</p><p class="wysiwyg-intro-copy">&nbsp;</p></div>
          <div class="field-item odd"><p><a href="https://atkinsongallery.co.uk/contact-us"><span class="s2" style="line-height: 1.53em;">Contact Us</span></a></p><p>Atkinson Gallery<br />Millfield School<br />Street,&nbsp;Somerset, BA16 0YD<br />Tel: +44 (0)1458 444322<br /><span class="s1" style="line-height: 1.53em;"><a href="mailto:Atkinsongallery@millfieldschool.com">atkinsongallery@millfieldschool.com</a></span><span class="s2" style="line-height: 1.53em;"><a href="mailto:Atkinsongallery@millfieldschool.com ?subject=Enquiry%20from%20website" style="line-height: 1.53em;">&nbsp;</a></span></p><p>View our <a href="https://www.facebook.com/atkinsongallery.co.uk">Facebook page </a><br />Follow us on Twitter <a href="https://twitter.com/GalleryAtkinson">@GalleryAtkinson</a></p></div>
      </div>
</div>        </div>

      </div>

    </div><!-- row 1 -->

    <div class="row promo-tier1">

      <div class="col-md-4">
	<div class="promo-large">
		<a href="http://atkinsongallery.co.uk/currentexhibitions">	<div class="promo-image">
		<img typeof="foaf:Image" class="img-responsive" src="https://atkinsongallery.co.uk/sites/default/files/styles/320x320_scale_and_crop/public/images/Senior/Atkinson_Gallery/Leonard_Green/Angel_Baby_web_no_bkground.jpg?itok=lW2qGJqc" width="320" height="320" alt="" />	</div>
	<div class="promo-desc-bk">
  		<span class="copy">
			Current Exhibitions		</span>
  	</div>
	  	</a>	</div>
</div>
<div class="col-md-4">
	<div class="promo-large">
		<a href="http://atkinsongallery.co.uk/future-exhibitions">	<div class="promo-image">
		<img typeof="foaf:Image" class="img-responsive" src="https://atkinsongallery.co.uk/sites/default/files/styles/320x320_scale_and_crop/public/images/Senior/Atkinson_Gallery/MA_2018/pill_painting_7.jpg?itok=A2q0zFrV" width="320" height="320" alt="" />	</div>
	<div class="promo-desc-bk">
  		<span class="copy">
			Future Exhibitions		</span>
  	</div>
	  	</a>	</div>
</div>
<div class="col-md-4">
	<div class="promo-large">
		<a href="http://atkinsongallery.co.uk/sculpture-tour">	<div class="promo-image">
		<img typeof="foaf:Image" class="img-responsive" src="https://atkinsongallery.co.uk/sites/default/files/styles/320x320_scale_and_crop/public/images/Senior/Atkinson_Gallery/TimHarrison_Portal_Crop_0.jpg?itok=JJS5uOW1" width="320" height="320" alt="Tim Harrison" />	</div>
	<div class="promo-desc-bk">
  		<span class="copy">
			Sculpture Tour		</span>
  	</div>
	  	</a>	</div>
</div>

    </div><!-- row 2 -->

  </div>
</article> <!-- /.node -->


  

<script type="text/javascript">
(function ($) { /* required to use the $ in jQuery */
  $(document).ready(function () {  
    $(window).load(function () {
      // Only autoplay carousel on desktop.
      var play = 0;
      // Width value taken from less variables.
      if ($(window).width() >= 768) {
        play = 1;
      }
      $.supersized( {
          // Functionality
          slide_interval          :   5000,       // Length between transitions
          transition              :   1,          // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
          autoplay                :   play,
          transition_speed        :   2000,        // Speed of transition
          vertical_center         :   0,
          horizontal_center       :   1,
          // Components                           
          slide_links             :   'blank',    // Individual links for each slide (Options: false, 'num', 'name', 'blank')
          slides                  :   [{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/Duncan-Elliott/Duncan_Elliott_2014_-_gallery_view.jpg?itok=GOsMJoJg',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/SamanthaCary_Horse.jpg?itok=ijr5J5Nx',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/Website_frogalt.jpg?itok=6TQFob4-',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/website_barrycawston_crop4.jpg?itok=chLcW9p5',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/PeterDavies_Cocoon_crop.jpg?itok=vs91Rikg',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/FreyaMorgan_FairytalesforaMendedEarth_Triptych.jpg?itok=h2K6zuQk',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},{image : 'https://atkinsongallery.co.uk/sites/default/files/styles/1920x1080_scale_and_crop/public/images/Senior/Atkinson_Gallery/MA_2017/Lucy_Delano_400_Eggs.jpg?itok=yHgvtJCg',title : 'Click here to see our Current Exhibitions <a class="more-link" target="" href="http://atkinsongallery.co.uk/currentexhibitions" title="Click here to see our Current Exhibitions">>></a>'},]// Slideshow Images
      });
    });
  }); // end of $(document).ready()
}(jQuery));
</script>          </section>

    
  </div>
</div>
<footer class="footer container">
  <div class="footer-inner row">
    <div class="col-md-8">
            <ul class="clearfix">
      <li><a href="/" class="active">Opening Hours</a> &nbsp;|</li>
      <li><a href="/how-to-find-us">How to find us</a>&nbsp;|</li>
      <li><a href="http://millfieldschool.com">Millfield School</a></li>
    </ul>
    <ul class="clearfix">
      <li>&copy; Copyright 2019 Atkinson Gallery&nbsp;|</li>
      <li>All rights reserved&nbsp;|</li>
      <li><a href="/terms-and-conditions">Terms &amp; Conditions</a>&nbsp;|</li>
      <li><a href="/cookie-policy">Privacy &amp; Cookies</a></li>
    </ul>
    <ul class="clearfix">
      <li>Atkinson Gallery&nbsp;|</li>
      <li>Millfield School&nbsp;|</li>
      <li>Street, Somerset, BA16 0YD</li>
    </ul>
    <ul class="clearfix">
      <li>Tel: +44 (0)1458 444332&nbsp;|</li>
      <li>Email: <a href="mailto:atkinsongallery@millfieldschool.com">atkinsongallery@millfieldschool.com</a></li>
    </ul>
    </div>
    <div class="col-md-4">
      <a href="http://millfieldschool.com" class="footer-logo"><img src="/sites/all/themes/bootstrap3_millfield_atkinson/images_atkinson/millfield-blue-logo.jpg" width="300" height="70" /></a>
    </div>
  </div>
</footer>
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5320985"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5320985&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript><header id="navbar" role="banner" class="navbar navbar-default">
  <div class="container">
    <div id="quicklinks-bar" class="navbar-header navbar-default">
        <form class="form-search content-search navbar-form" role="search" action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
  <label for="search_block_form">Search</label><div class="form-group"><input title="Enter the terms you wish to search for." placeholder="Search" class="form-control form-text" type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" /><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button></div><div class="form-actions form-wrapper form-group" id="edit-actions"><button class="element-invisible btn btn-primary form-submit" type="submit" id="edit-submit" name="op" value="Search">Search</button>
</div><input type="hidden" name="form_build_id" value="form-MWx__qHCV12HWqysCk4vBKEt7BEy-PP6AkdGfEpsWY0" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>    </div>
      <div id="main-desktop-menu" class="navbar-nav main-desktop-menu">
          <ul class="main-menu"><li class="first leaf active depth-1"><a href="/" class="menu-item active">Welcome</a></li>
<li class="leaf depth-1"><a href="/currentexhibitions" title="Current Exhibitions" class="menu-item">Current Exhibitions</a></li>
<li class="leaf depth-1"><a href="/future-exhibitions" title="Future Exhibitions" class="menu-item">Future Exhibitions</a></li>
<li class="leaf depth-1"><a href="/past-exhibitions" title="Past Exhibitions" class="menu-item">Past Exhibitions</a></li>
<li class="leaf depth-1"><a href="/sculpture-tour" title="Sculpture Tour" class="menu-item">Sculpture Tour</a></li>
<li class="leaf depth-1"><a href="/node/2722" class="menu-item">Limited Edition Prints</a></li>
<li class="leaf depth-1"><a href="/how-to-find-us" title="How to Find Us" class="menu-item">How to Find Us</a></li>
<li class="last leaf depth-1"><a href="/contact-us" title="Contact us" class="menu-item">Contact us</a></li>
</ul>      </div>
  </div>
</header>
<div id='mobile-menu-container'>
  <ul class="main-menu"><li class="first leaf active depth-1"><a href="/" class="menu-item active">Welcome</a></li>
<li class="leaf depth-1"><a href="/currentexhibitions" title="Current Exhibitions" class="menu-item">Current Exhibitions</a></li>
<li class="leaf depth-1"><a href="/future-exhibitions" title="Future Exhibitions" class="menu-item">Future Exhibitions</a></li>
<li class="leaf depth-1"><a href="/past-exhibitions" title="Past Exhibitions" class="menu-item">Past Exhibitions</a></li>
<li class="leaf depth-1"><a href="/sculpture-tour" title="Sculpture Tour" class="menu-item">Sculpture Tour</a></li>
<li class="leaf depth-1"><a href="/node/2722" class="menu-item">Limited Edition Prints</a></li>
<li class="leaf depth-1"><a href="/how-to-find-us" title="How to Find Us" class="menu-item">How to Find Us</a></li>
<li class="last leaf depth-1"><a href="/contact-us" title="Contact us" class="menu-item">Contact us</a></li>
</ul></div>
  <script src="https://atkinsongallery.co.uk/sites/all/modules/contrib/eu-cookie-compliance/js/eu_cookie_compliance.js?q165lg"></script>
<script src="https://atkinsongallery.co.uk/sites/all/themes/bootstrap/js/bootstrap.js?q165lg"></script>

    <!-- Google Code for Remarketing Tag -->
  <!--------------------------------------------------
  Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
  --------------------------------------------------->
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 990561941;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
    <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/990561941/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
  </noscript>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"a2040dbc33","applicationID":"227532046","transactionName":"YQFbN0FXC0NTBkNfV1hLeABHXwpeHQtYUl1pFFgEVmkTWVcS","queueTime":0,"applicationTime":242,"atts":"TUZYQQlNGE0=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
