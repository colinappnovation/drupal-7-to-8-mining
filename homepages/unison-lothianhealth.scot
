<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.unison-lothianhealth.scot/sites/default/files/unison.png" type="image/png" />
<meta name="description" content="Welcome to the UNISON Lothian Health Branch website. We&#039;re the largest trade union in NHS Lothian, representing over 12,000 members across the range of health professions." />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="canonical" href="https://www.unison-lothianhealth.scot/home" />
<link rel="shortlink" href="https://www.unison-lothianhealth.scot/home" />
<meta property="fb:app_id" content="420868498329142" />
<meta property="fb:pages" content="383448675096143" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="UNISON Lothian Health Branch" />
<meta property="og:title" content="Home | UNISON Lothian Health Branch" />
<meta property="og:url" content="https://www.unison-lothianhealth.scot/home" />
<meta property="og:description" content="We&#039;re the largest trade union in NHS Lothian, representing over 12,000 members across the range of health professions." />
<meta property="og:image" content="https://www.unison-lothianhealth.scot/sites/default/files/styles/header__1200x630_/public/general/22308851_1471442429630090_3973841791378334820_n.jpg?itok=ottJhyaK" />
<meta property="og:image:secure_url" content="https://www.unison-lothianhealth.scot/sites/default/files/styles/header__1200x630_/public/general/22308851_1471442429630090_3973841791378334820_n.jpg?itok=ottJhyaK" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />
<meta name="theme-color" content="#1e003d" />
<meta name="msvalidate.01" content="6E24952F5C1C1285131D352EBA44E2BE" />
<meta name="google-site-verification" content="hAWtvijEo5lAUL6nFLnhQ_Ig2YtFPluIOHc1fHER16A" />
  <title>Home | UNISON Lothian Health Branch</title>
  <link type="text/css" rel="stylesheet" href="https://www.unison-lothianhealth.scot/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.unison-lothianhealth.scot/sites/default/files/css/css_XOWVgbdfOk5-YoF4JAskChCF8nCW-zUrTNNLlpo5In0.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.unison-lothianhealth.scot/sites/default/files/css/css_cEAck8HAZ2CmMrp7X0pKfBiw9u1h91TkehEMsOEHmq4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.unison-lothianhealth.scot/sites/default/files/css/css_by-laKXAnFW417t8k06kD_mniv6m_3ONwQN68XbyJJA.css" media="all" />
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <script src="https://www.unison-lothianhealth.scot/sites/default/files/js/js_gYWhuqN0Wsid8HOSk0yFzqMg-2NTv-xegwc2ACYMGws.js"></script>
<script src="https://www.unison-lothianhealth.scot/sites/default/files/js/js_KsR-3YlnJlYg8UlXCaaVvfQaJAEAzScFApIzR8UBweM.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-134197010-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");</script>
<script src="https://www.unison-lothianhealth.scot/sites/default/files/js/js_DzqesLsGDWZQm-7B5wHcxXyK7cp7ovpzDt9-bVKaecA.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"freeze","theme_token":"iuo2pUcobBT5Ny4ZsBmsosILaG29qrRGnvJZ2SEPxT0","js":{"sites\/all\/modules\/panopoly_widgets\/panopoly-widgets.js":1,"sites\/all\/modules\/panopoly_widgets\/panopoly-widgets-spotlight.js":1,"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.9\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/fb_likebox\/fb_likebox.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/affix.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/alert.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/button.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/carousel.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/collapse.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/dropdown.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/modal.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/tooltip.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/popover.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/scrollspy.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/tab.js":1,"sites\/all\/themes\/freeze\/bootstrap\/js\/transition.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/election\/election.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/panopoly_widgets\/panopoly-widgets.css":1,"sites\/all\/modules\/panopoly_widgets\/panopoly-widgets-spotlight.css":1,"sites\/all\/modules\/radix_layouts\/radix_layouts.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/civicrm\/css\/crm-i.css":1,"sites\/all\/modules\/civicrm\/css\/civicrm.css":1,"sites\/all\/libraries\/fontawesome\/css\/font-awesome.css":1,"sites\/all\/themes\/freeze\/css\/style.css":1}},"fb_likebox_app_id":"","fb_likebox_language":"en_EN","googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="navbar-is-fixed-top html front not-logged-in no-sidebars page-home">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <header id="navbar" role="banner" class="navbar navbar-fixed-top navbar-default">
  <div class="container">
    <div class="navbar-header">
              <a class="logo navbar-btn pull-left" href="/" title="Home">
          <img src="https://www.unison-lothianhealth.scot/sites/default/files/lothianhealth_2.png" alt="Home" />
        </a>
      
      
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first expanded dropdown"><a href="/node/24" title="" class="dropdown-toggle" data-toggle="dropdown"><span class="fa-fw"><i class="fa fa-users" aria-hidden="true"></i></span>
Members <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/members/help">Help at work</a></li>
<li class="last leaf"><a href="/members/benefits">Member benefits</a></li>
</ul></li>
<li class="expanded dropdown"><a href="/node/28" title="" class="dropdown-toggle" data-toggle="dropdown"><span class="active"><i class="fa fa-wrench" aria-hidden="true"></i></span>
Get Involved <span class="caret"></span></a><ul class="dropdown-menu"><li class="first leaf"><a href="/active/getactive">Become an activist</a></li>
<li class="leaf"><a href="/active/black">Black Members</a></li>
<li class="leaf"><a href="/active/disabled">Disabled members</a></li>
<li class="leaf"><a href="/active/lgbt">LGBT Members</a></li>
<li class="leaf"><a href="/active/retired">Retired Members</a></li>
<li class="leaf"><a href="/active/women">Women Members</a></li>
<li class="last leaf"><a href="/active/young">Young Members</a></li>
</ul></li>
<li class="last leaf"><a href="/contact"><span class="fa-fw"><i class="fa fa-phone" aria-hidden="true"></i></span>
<span class="title">Contact us</span></a></li>
</ul>                                <ul class="menu nav navbar-nav secondary"><li class="first leaf"><a href="http://www.unison-scotland.org/join-us/" title=""><span class="fa-fw"><i class="fa fa-bolt" aria-hidden="true"></i></span>
<span class="title">Join UNISON</span></a></li>
<li class="leaf"><a href="/user/login" title=""><span class="fa-fw"><i class="fa fa-sign-in" aria-hidden="true"></i></span>
Log in</a></li>
<li class="last leaf"><a href="/user/register" title=""><span class="fa-fw"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
Register</a></li>
</ul>                            </nav>
      </div>
      </div>
</header>

<div class="main-container container">

  <header role="banner" id="page-header">
    
      </header> <!-- /#page-header -->

  <div class="row">

    
    <section class="col-sm-12">
                  <a id="main-content"></a>
                                                                <div class="region region-content">
    <section id="block-system-main" class="block block-system clearfix">

      
  
<div class="panel-display burr-flipped clearfix radix-burr-flipped" >
  
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 radix-layouts-content panel-panel">
        <div class="panel-panel-inner">
          <div class="panel-pane pane-fieldable-panels-pane pane-vid-21 pane-bundle-image pane-fpid-9"  >
  
      
  
  <div class="pane-content">
    <div class="fieldable-panels-pane">
    <div class="field field-name-field-basic-image-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/header__1200x630_/public/general/22308851_1471442429630090_3973841791378334820_n.jpg?itok=ottJhyaK" width="1200" height="630" alt="The branch at the October 2017 pay demonstration" /></div></div></div></div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-fieldable-panels-pane pane-vid-13 pane-bundle-text pane-fpid-3"  >
  
      
  
  <div class="pane-content">
    <div class="fieldable-panels-pane">
    <div class="field field-name-field-basic-text-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p><strong><br /><h1 class="display-1">UNISON Lothian Health Branch</h1>
<p></p></strong></p>
</div></div></div></div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-fieldable-panels-pane pane-vid-224 pane-bundle-text pane-fpid-2"  >
  
      
  
  <div class="pane-content">
    <div class="fieldable-panels-pane">
    <div class="field field-name-field-basic-text-text field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even"><p class="lead">Welcome to the UNISON Lothian Health Branch website. We're the largest trade union in NHS Lothian, representing over 12,000 members across the range of health professions.</p>
<p>We're also the trade union for staff working in the third and private sectors delivering healthcare across the Lothian region. Whether you're having problems at work, need advice and support, or want to make your workplace better, we're here to help.</p>
</div></div></div></div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-custom pane-1"  >
  
      
  
  <div class="pane-content">
    <h2>Latest news from the branch</h2>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views-panes pane-news-media-object-list-panel-pane-1"  >
  
      
  
  <div class="pane-content">
    <div class="view view-news-media-object-list view-id-news_media_object_list view-display-id-panel_pane_1 view-dom-id-1b74c0dcdfe491d18187696637da6c09">
        
  
  
      <div class="view-content">
      
<div id="views-bootstrap-media-1" class="views-bootstrap-media-plugin-style">
  <ul class="media-list">
          <li class="media">
                  <div class="media-left pull-left">
            <a href="/news/stay-warm-winter"><img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/field/image/instrafitter_20191129_115808.jpg?itok=Jc6ticl3" width="64" height="64" alt="" /></a>          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/news/stay-warm-winter"><strong>29th November </strong>| STAY WARM THIS WINTER</a>            </h4>
          
          
GET HELP STAYING WARM THIS WINTER 
Are you on a low income? Help is on hand!
There for You (UNISON's welfare charity) is launching this year's Winter Fuel Grant Programme and has...        </div>
      </li>
          <li class="media">
                  <div class="media-left pull-left">
            <a href="/news/sleep-park-2019"><img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/field/image/73322770_2553404158100573_7204206236244377600_n.jpg?itok=9elmD_dr" width="64" height="64" alt="" /></a>          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/news/sleep-park-2019"><strong>29th November </strong>| Sleep In The Park 2019</a>            </h4>
          
          This year's 'Sleep in the Park' event to combat homelessness will be held on Saturday 7th December in Princes Street Edinburgh
As you are aware we have some members and stewards...        </div>
      </li>
          <li class="media">
                  <div class="media-left pull-left">
            <a href="/news/branch-stewards-win-stunning-victory-stop-hike-professional-fees"><img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/field/image/hcpc.png?itok=hJatzW4b" width="64" height="64" alt="" /></a>          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/news/branch-stewards-win-stunning-victory-stop-hike-professional-fees"><strong>11th November </strong>| Branch Stewards Win Stunning Victory to Stop Hike in Professional Fees </a>            </h4>
          
          UNISON Lothian Health Branch Lead Stewards Reg Lloyd and Debbie Reilly have led a successful campaign to stop the Health and Care Professions Council (HCPC) from imposing a...        </div>
      </li>
          <li class="media">
                  <div class="media-left pull-left">
            <a href="/news/scotland-unison-newsletter-november-2019"><img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/field/image/siunov19-146x206.jpg?itok=qxThuvNj" width="64" height="64" alt="" /></a>          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/news/scotland-unison-newsletter-november-2019"><strong>6th November </strong>| Scotland in Unison Newsletter November 2019</a>            </h4>
          
          The latest copy Scotland in Unison contains news on:
UNISON Scotland influences the political debate; branches celebrate unsung local service champions; menopause work place guide...        </div>
      </li>
      </ul>
</div>
    </div>
  
  
  
  
      <div class="view-footer">
      <h4><a href="/news">More news...</a></h4>    </div>
  
  
</div>  </div>

  
  </div>
        </div>
      </div>
      <div class="col-md-4 radix-layouts-sidebar panel-panel">
        <div class="panel-panel-inner">
          <div class="panel-pane pane-custom pane-2"  >
  
      
  
  <div class="pane-content">
    <div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="100%" src="https://www.youtube.com/embed/jrorU32KzNI" width="auto"></iframe>
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views-panes pane-upcoming-events-panel-pane-1"  >
  
        <h2 class="pane-title">
      Upcoming Events    </h2>
    
  
  <div class="pane-content">
    <div class="view view-upcoming-events view-id-upcoming_events view-display-id-panel_pane_1 view-dom-id-9b77f3eea024ad5adcaf12d947c57eac">
        
  
  
      <div class="view-content">
      
<div id="views-bootstrap-accordion-1" class="views-bootstrap-accordion-plugin-style panel-group">
            <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle collapsed"
              data-toggle="collapse"
              data-parent="#views-bootstrap-accordion-1"
              href="#collapse-1-0">
                          </a>
          </h4>
        </div>
  
        <div id="collapse-1-0" class="panel-collapse collapse">
          <div class="panel-body">
              
  <div class="views-field views-field-start-date">    <strong class="views-label views-label-start-date">Starts: </strong>    <span class="field-content"></span>  </div>  
  <div class="views-field views-field-end-date">    <strong class="views-label views-label-end-date">Ends: </strong>    <span class="field-content"></span>  </div>  
  <p class="views-field views-field-summary">        <span class="field-content">Save the date! Friday 22nd February 2019. You are invited to attend our Annual General Meeting and take part in setting the agenda for the future of your branch and union.</span>  </p>  
  <div class="views-field views-field-id">        <span class="field-content"><a href="/civicrm/event/info?reset=1&amp;id=20" class="btn btn-default">More info...</a></span>  </div>          </div>
        </div>
      </div>
                <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle collapsed"
              data-toggle="collapse"
              data-parent="#views-bootstrap-accordion-1"
              href="#collapse-1-1">
                          </a>
          </h4>
        </div>
  
        <div id="collapse-1-1" class="panel-collapse collapse">
          <div class="panel-body">
              
  <div class="views-field views-field-start-date">    <strong class="views-label views-label-start-date">Starts: </strong>    <span class="field-content"></span>  </div>  
  <div class="views-field views-field-end-date">    <strong class="views-label views-label-end-date">Ends: </strong>    <span class="field-content"></span>  </div>  
  <p class="views-field views-field-summary">        <span class="field-content">Monthly branch meeting</span>  </p>  
  <div class="views-field views-field-id">        <span class="field-content"><a href="/civicrm/event/info?reset=1&amp;id=21" class="btn btn-default">More info...</a></span>  </div>          </div>
        </div>
      </div>
                <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle collapsed"
              data-toggle="collapse"
              data-parent="#views-bootstrap-accordion-1"
              href="#collapse-1-2">
                          </a>
          </h4>
        </div>
  
        <div id="collapse-1-2" class="panel-collapse collapse">
          <div class="panel-body">
              
  <div class="views-field views-field-start-date">    <strong class="views-label views-label-start-date">Starts: </strong>    <span class="field-content"></span>  </div>  
  <div class="views-field views-field-end-date">    <strong class="views-label views-label-end-date">Ends: </strong>    <span class="field-content"></span>  </div>  
  <p class="views-field views-field-summary">        <span class="field-content">Information and Recruitment Day</span>  </p>  
  <div class="views-field views-field-id">        <span class="field-content"><a href="/civicrm/event/info?reset=1&amp;id=22" class="btn btn-default">More info...</a></span>  </div>          </div>
        </div>
      </div>
                <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle collapsed"
              data-toggle="collapse"
              data-parent="#views-bootstrap-accordion-1"
              href="#collapse-1-3">
                          </a>
          </h4>
        </div>
  
        <div id="collapse-1-3" class="panel-collapse collapse">
          <div class="panel-body">
              
  <div class="views-field views-field-start-date">    <strong class="views-label views-label-start-date">Starts: </strong>    <span class="field-content"></span>  </div>  
  <div class="views-field views-field-end-date">    <strong class="views-label views-label-end-date">Ends: </strong>    <span class="field-content"></span>  </div>  
  <p class="views-field views-field-summary">        <span class="field-content">Monthly branch committee meeting for stewards</span>  </p>  
  <div class="views-field views-field-id">        <span class="field-content"><a href="/civicrm/event/info?reset=1&amp;id=23" class="btn btn-default">More info...</a></span>  </div>          </div>
        </div>
      </div>
                <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle collapsed"
              data-toggle="collapse"
              data-parent="#views-bootstrap-accordion-1"
              href="#collapse-1-4">
                          </a>
          </h4>
        </div>
  
        <div id="collapse-1-4" class="panel-collapse collapse">
          <div class="panel-body">
              
  <div class="views-field views-field-start-date">    <strong class="views-label views-label-start-date">Starts: </strong>    <span class="field-content"></span>  </div>  
  <div class="views-field views-field-end-date">    <strong class="views-label views-label-end-date">Ends: </strong>    <span class="field-content"></span>  </div>  
  <p class="views-field views-field-summary">        <span class="field-content">All members are invited to attend the UNISON Lothian Health Branch AGM to be held on the 7th of February 2020 at the Danderhall Miners&#039; Club, 88 Edmonstone Road, Danderhall, EH22 1QU.</span>  </p>  
  <div class="views-field views-field-id">        <span class="field-content"><a href="/civicrm/event/info?reset=1&amp;id=24" class="btn btn-default">More info...</a></span>  </div>          </div>
        </div>
      </div>
      </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views pane-the-breakroom"  >
  
        <h2 class="pane-title">
      The Breakroom    </h2>
    
  
  <div class="pane-content">
    <div class="view view-the-breakroom view-id-the_breakroom view-display-id-block_1 view-dom-id-2273e60bb591ca8ac703446e85e74263">
        
  
  
      <div class="view-content">
      
<div id="views-bootstrap-media-2" class="views-bootstrap-media-plugin-style">
  <ul class="media-list">
          <li class="media">
                  <div class="media-left pull-left">
            <img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/71096966_2468343999939923_3452873695591137280_o.jpg?itok=hdUG6B00" width="64" height="64" alt="" />          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/breakroom/practice-self-care-autumn">Practice self-care this autumn</a>            </h4>
          
                  </div>
      </li>
          <li class="media">
                  <div class="media-left pull-left">
            <img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/25322_young_workers_typo_roundel_pink-150x150.png?itok=bgau0V_w" width="64" height="64" alt="" />          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/breakroom/young-members-mindfulness-mood-manual">Young member&#039;s mindfulness &#039;Mood Manual&#039;</a>            </h4>
          
                  </div>
      </li>
          <li class="media">
                  <div class="media-left pull-left">
            <img typeof="foaf:Image" class="img-responsive" src="https://www.unison-lothianhealth.scot/sites/default/files/styles/media_object__64x64_scale___crop_/public/calm-businesswoman-relaxing-with-breath-gymnastics_1163-5375.jpg?itok=nyzfDxEF" width="64" height="64" alt="" />          </div>
        
        <div class="media-body">
                      <h4 class="media-heading">
              <a href="/breakroom/5-ways-improve-wellbeing-workplace">5 Ways to Improve Wellbeing In The Workplace</a>            </h4>
          
                  </div>
      </li>
      </ul>
</div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-custom pane-3"  >
  
      
  
  <div class="pane-content">
    <hr />  </div>

  
  </div>
<div class="panel-separator"></div><div  class="panel-bootstrap-pane panel panel-success hidden-xs clearfix pane-block pane-fb-likebox-0">
  
      
  
  
  <div  class="panel-body">
    <div class="fb-page" data-href="https://www.facebook.com/Unison-Lothian-Health-Branch-383448675096143/" data-width="340" data-height="500" data-tabs="" data-hide-cover="0" data-show-facepile="1" data-hide-cta="0" data-small-header="0" data-adapt-container-width="1"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Unison-Lothian-Health-Branch-383448675096143/"><a href="https://www.facebook.com/Unison-Lothian-Health-Branch-383448675096143/">Like us on Facebook</a></blockquote></div></div>  </div>

  
  
  </div>
        </div>
      </div>
    </div>
  
  </div>
</div><!-- /.burr-flipped -->

</section>
  </div>
    </section>

    
  </div>
</div>

  <footer class="footer container">
      <div class="region region-footer">
    <section id="block-block-1" class="block block-block clearfix">

      
  <p class="text-right">This site is run by UNISON Lothian Health Branch. <a href="https://unison-lothianhealth.scot/privacy"><strong>Privacy Policy.</strong></a></p>

</section>
  </div>
  </footer>
  <script src="https://www.unison-lothianhealth.scot/sites/default/files/js/js_29qYXJz8NLGg8Aomg-RZPjJcj9yEdEst1BMZ9gZbs-4.js"></script>
<script src="https://www.unison-lothianhealth.scot/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
