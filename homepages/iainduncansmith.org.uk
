<!DOCTYPE html>
<!-- Sorry no IE7 support! -->
<!-- @see http://foundation.zurb.com/docs/index.html#basicHTMLMarkup -->

<!--[if IE 8]><html class="no-js lt-ie9" lang="en" dir="ltr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" dir="ltr"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
<link rel="shortcut icon" href="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/favicon_0_0.ico" />
<meta name="viewport" content="width=device-width, maximum-scale = 1.0" />
<meta http-equiv="ImageToolbar" content="false" />
<meta name="description" content="Iain Duncan Smith, Member of Parliament for Chingford and Woodford Green. Speeches, articles and biographical details." />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.iainduncansmith.org.uk/" />
<link rel="shortlink" href="https://www.iainduncansmith.org.uk/" />
  <title>Iain Duncan Smith, MP for Chingford and Woodford Green</title>
  <style>
@import url("https://www.iainduncansmith.org.uk/modules/system/system.base.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/system/system.messages.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/system/system.theme.css?pzi7c8");
</style>
<style>
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/date/date_api/date.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/field/theme/field.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/node/node.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/search/search.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/modules/user/user.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/views/css/views.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/ckeditor/css/ckeditor.css?pzi7c8");
</style>
<style>
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/colorbox/styles/default/colorbox_style.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/ctools/css/ctools.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/panels/css/panels.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/all/modules/contrib/panels/plugins/layouts/onecol/onecol.css?pzi7c8");
</style>
<style>
@import url("https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/css/normalize.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/css/buttermountain_foundation.css?pzi7c8");
@import url("https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/css/slick.css?pzi7c8");
</style>
  <script src="https://www.iainduncansmith.org.uk/sites/all/modules/contrib/jquery_update/replace/jquery/1.7/jquery.min.js?v=1.7.2"></script>
<script src="https://www.iainduncansmith.org.uk/misc/jquery.once.js?v=1.2"></script>
<script src="https://www.iainduncansmith.org.uk/misc/drupal.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/all/modules/contrib/cookiecontrol/js/cookieControl-5.1.min.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/all/libraries/colorbox/jquery.colorbox-min.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/all/modules/contrib/colorbox/js/colorbox.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/all/modules/contrib/colorbox/styles/default/colorbox_style.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/js/vendor/modernizr.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/js/scripts.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/js/slick.js?pzi7c8"></script>
<script src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/themes/buttermountain_foundation/js/foundation.min.js?pzi7c8"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"buttermountain_foundation","theme_token":"L65Nt-lkqnA7tAsQeJ4AUigShJmCZo93-ckKeGtR37U","js":{"0":1,"1":1,"2":1,"3":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.7\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/cookiecontrol\/js\/cookieControl-5.1.min.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/js\/vendor\/modernizr.js":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/js\/scripts.js":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/js\/slick.js":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/js\/foundation.min.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/contrib\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/contrib\/views\/css\/views.css":1,"sites\/all\/modules\/contrib\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/contrib\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/modules\/contrib\/panels\/plugins\/layouts\/onecol\/onecol.css":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/css\/normalize.css":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/css\/buttermountain_foundation.css":1,"sites\/www.iainduncansmith.org.uk\/themes\/buttermountain_foundation\/css\/slick.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"urlIsAjaxTrusted":{"\/":true}});</script>
  <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-landing-page node-promoted" >
  <div class="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    
  <div id="wrapper">
    <div id="container" class="clearfix">
      <div id="header">
                <div id="header-bar">
           <div class="squeeze">
              <section class="block block-panels-mini block-panels-mini-header-bar">
  
      
    <div id="mini-panel-header_bar">  
  <div class="row top-panel-2col">
    <div class="large-9 medium-12 small-12 columns col-first">
      <div class="panel-pane pane-node"  >
  
      
  
  <div class="pane-content">
    <div id="node-18" class="node node-page view-mode-full" about="/content/social-media" typeof="foaf:Document">

              <h2 class="node-title">
        <a href="/content/social-media"></a></h2>
        <span property="dc:title" content="" class="rdf-meta element-hidden"></span>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p class="sm-tw"><a class="sm-tw" href="https://twitter.com/MPIainDS" target="_blank">twitter</a></p>
<p class="sm-fb"><a class="sm-fb" href="https://www.facebook.com/iainduncansmithmp/" target="_blank">facebook</a></p>
<p class="sm-in"><a class="sm-in" href="https://www.instagram.com/iain_duncan_smith_mp/" target="_blank">instagram</a></p>
</div>
  
    
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-search-form"  >
  
      
  
  <div class="pane-content">
    <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Search </label>
 <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-AJSsCtU9wti6bhLsO-_K_OR0DaFWqSt7ZRJnrTsPwCs" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>  </div>

  
  </div>
    </div>
    <div class="large-3 medium-0 small-12 columns col-last">
      <div class="col-last-wrapper">
              </div>
    </div>
  </div>

  </div>  
  </section>           </div> <!-- /.squeeze -->
         </div> <!-- /#header-bar -->
              <div class="squeeze">
        <div id="logo-floater">
                              <h1 id="branding"><a href="/">
                          <img src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/iainduncansmith_election2019.png" alt="Iain Duncan Smith" title="Iain Duncan Smith" id="logo" />
                        </a></h1>
                        </div>

                  <section class="block block-panels-mini block-panels-mini-header-responsive-menu header">
  
      
    <div class="panel-display panel-1col clearfix" id="mini-panel-header_responsive_menu">
  <div class="panel-panel panel-col">
    <div><div class="panel-pane pane-topmenu-mobile"  >
  
      
  
  <div class="pane-content">
    <ul class="menu"><li class="first leaf"><a href="/" class="side-menu-item level-1 active">Home</a></li>
<li class="leaf"><a href="/content/news" class="side-menu-item level-1">News</a></li>
<li class="leaf"><a href="/content/my-work" class="side-menu-item level-1 has-dropdown">Parliament</a></li>
<li class="leaf"><a href="/content/about-chingford-and-woodford-green" class="side-menu-item level-1">Chingford &amp; Woodford Green</a></li>
<li class="leaf"><a href="/campaigns" title="" class="side-menu-item level-1">Campaigns</a></li>
<li class="last leaf"><a href="/content/news-gallery" title="Iain Duncan Smith Photo Gallery" class="side-menu-item level-1">Gallery</a></li>
</ul>  </div>

  
  </div>
</div>
  </div>
</div>
  
  </section>
      </div> <!-- /.squeeze -->

      <!--dropdown main menu -->
      
		    <nav class="main-dropdown-menu-wrapper show-for-medium-up">
		    	<div class="main-dropdown-menu">
		      	<ul id="main-menu" class="main-nav left"><li class="first leaf"><a href="/" class="mlid-218 active">Home</a></li><li class="side-menu-item level-1"><a href="/content/news" class="side-menu-item level-1 mlid-784">News</a></li><li class="side-menu-item level-1 has-dropdown"><a href="/content/my-work" class="side-menu-item level-1 has-dropdown mlid-786">Parliament</a></li><li class="side-menu-item level-1"><a href="/content/about-chingford-and-woodford-green" class="side-menu-item level-1 mlid-955">Chingford &amp; Woodford Green</a></li><li class="side-menu-item level-1" title=""><a href="/campaigns" title="" class="side-menu-item level-1 mlid-1421">Campaigns</a></li><li class="side-menu-item level-1" title="Iain Duncan Smith Photo Gallery"><a href="/content/news-gallery" title="Iain Duncan Smith Photo Gallery" class="side-menu-item level-1 mlid-883">Gallery</a></li></ul>		      </div>
		    </nav>
     
     	<nav class="mobile-header show-for-small-only">
				<a class="toggle-mobile-menu mobile-icon mobile-menu-icon" href="#"><span></span></a>
		  </nav>

     </div> <!-- /#header -->


      <div id="center"><div id="squeeze" class="squeeze">
                    <a id="main-content"></a>
          <div id="tabs-wrapper" class="clearfix">                                        </div>                                                  <div class="clearfix">
              
      
      
  <div class="row panel-2col">
    <div class="large-9 medium-8 small-12 columns col-first">
      <div class="panel-pane pane-custom pane-1 custom-title"  >
  
      
  
  <div class="pane-content">
    <h2 class="pane-title">
	About Iain Duncan Smith </h2>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-node slideshow"  >
  
      
  
  <div class="pane-content">
    
<div class="panelizer-view-mode node node-teaser node-page node-3">
        <div class="panel-display panel-1col clearfix" >
  <div class="panel-panel panel-col">
    <div><div class="panel-pane pane-node-title"  >
  
      
  
  <div class="pane-content">
    <h2><a href="/content/about-iain-duncan-smith-mp"></a></h2>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-entity-field pane-node-body"  >
  
      
  
  <div class="pane-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p style="margin: 13px 0px; padding: 0px; color: rgb(19, 48, 90); font-family: Arial, 'Arial Unicode MS', Helvetica, sans-serif; font-size: 13px; line-height: 22.75px;"><img alt="The Rt Hon Iain Duncan Smith MP" src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/Portrait_0.jpg" style="float: right; height: 373px; width: 300px;" /></p>
<div>
	This website has been designed to give you an up to date picture of my work.
<!-- in Chingford and Woodford Green, and in Parliament.--></div>
<div>
	 </div>
<div>
	On this site you will find local news and insights into my campaigning on local and national issues.</div>
<div>
	 </div>
<div>
<!--Thank you for visiting and please click on the <a href="http://www.iainduncansmith.org.uk/content/contact">HELP</a> link if you are a constituent and wish to raise an issue with me.--></div>
<div>
	 </div>
<div>
	<strong>Iain Duncan Smith</strong></div>
<div>
	 </div>
<div>
	<a href="/content/about-iain-duncan-smith">More About Iain Duncan Smith</a></div>
</div>  </div>

  
  </div>
</div>
  </div>
</div>
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views-panes pane-list-standard-panel-pane-2 two-block"  >
  
        <h2 class="pane-title">
      NEWS    </h2>
    
  
  <div class="pane-content">
    <div class="view view-list-standard view-id-list_standard view-display-id-panel_pane_2 view-dom-id-3616ce5996694b650de093061088dab2">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/whipps-cross-hospital-0"><div class="list-image"><img typeof="foaf:Image" src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/styles/bm_medium/public/iain_duncan_smith_matt_hancock_whipps_cross_3.jpg?itok=EdA3ol0Y" width="332" height="190" alt="Secretary of State for Health,  Matt Hancock, visits Whipps Cross Hospital" title="Secretary of State for Health,  Matt Hancock, visits Whipps Cross Hospital" /></div>
<div class="list-title"><h3>Whipps Cross Hospital</h3></div>
<div class="list-summary"><p>Whipps Cross Hospital redevelopment given green light by Government.</p>
</div>
<div class="list-date">30th September 2019</div></a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/public-meeting"><div class="list-image"><img typeof="foaf:Image" src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/styles/bm_medium/public/iain_duncansmith_police_public_meeting.jpg?itok=e3crOHMp" width="332" height="190" alt="Public Meeting about crime with residents &amp; Police in South Woodford." title="Public Meeting about crime with residents &amp; Police in South Woodford." /></div>
<div class="list-title"><h3>Public Meeting</h3></div>
<div class="list-summary"><p>Public Meeting about crime with residents &amp; Police in South Woodford.</p>
</div>
<div class="list-date">12th August 2019</div></a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/boris-johnson"><div class="list-image"><img typeof="foaf:Image" src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/styles/bm_medium/public/boris_johnson.jpg?itok=Cn7wHFkD" width="332" height="190" alt="Boris Johnson" title="Boris Johnson" /></div>
<div class="list-title"><h3>Boris Johnson</h3></div>
<div class="list-summary"><p>Congratulations to Boris Johnson on winning the race today to become Conservative Party Leader.</p>
</div>
<div class="list-date">23rd July 2019</div></a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/ridgeway-park-anti-social-behaviour"><div class="list-image"><img typeof="foaf:Image" src="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/styles/bm_medium/public/iain_duncan_smith_ridgeway_park.jpg?itok=Zg_YJuYa" width="332" height="190" alt="Iain Duncan Smith MP and Cllr Roy Berg" title="Iain Duncan Smith MP and Cllr Roy Berg" /></div>
<div class="list-title"><h3>Ridgeway Park Anti-Social Behaviour</h3></div>
<div class="list-summary"><p>I am extremely concerned about the recent rise in dangerous anti-social behaviour in Ridgeway Park.</p>
</div>
<div class="list-date">22nd July 2019</div></a></span>  </div>  </div>
    </div>
  
  
  
  
      <div class="view-footer">
      <p><a class="list-more" href="/node/7">more news...</a></p>
    </div>
  
  
</div>  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views-panes pane-list-standard-panel-pane-4 one-block"  >
  
        <h2 class="pane-title">
      Parliamentary Work    </h2>
    
  
  <div class="pane-content">
    <div class="view view-list-standard view-id-list_standard view-display-id-panel_pane_4 view-dom-id-c40528569f4e316cb6b0e06ecfd3b514">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/iain-duncan-smith-intervenes-general-election-debate"><div class="list-title"><h3>Iain Duncan Smith intervenes in the General Election debate</h3></div>
<div class="list-date">29th October 2019</div>
<div class="list-summary"><p>Speaking in the debate on the General Election, Iain Duncan Smith intervenes to question why it makes a difference where a student casts their vote and to raise concerns at rushing changes to the constitution as demonstrated by the failure of the Fixed-term Parliaments Act</p>
</div>
</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even views-row-last">
      
  <div class="views-field views-field-nid">        <span class="field-content"><a href="/content/iain-duncan-smith-condemns-suggestion-uk-workers-rights-are-only-safe-if-we-stay-eu"><div class="list-title"><h3>Iain Duncan Smith condemns suggestion the UK workers rights are only safe if we stay in the EU</h3></div>
<div class="list-date">29th October 2019</div>
<div class="list-summary"><p>Following the Government statement on the plans for workers’ rights after the UK leaves the EU, Iain Duncan Smith rejects the suggestion that the British workers rights are only safe in the EU, highlighting an European Court of Justice ruling that, with imported workers, puts business rights over workers rights.</p>
</div>
</a></span>  </div>  </div>
    </div>
  
  
  
  
      <div class="view-footer">
      <p><a class="list-more" href="/node/9">more work...</a></p>
    </div>
  
  
</div>  </div>

  
  </div>
    </div>
    <div class="large-3 medium-4 small-12 columns col-last">
      <div class="col-last-wrapper">
        <div class="panel-pane pane-panels-mini pane-right-hand-column"  >
  
      
  
  <div class="pane-content">
    <div class="panel-display panel-1col clearfix" id="mini-panel-right_hand_column">
  <div class="panel-panel panel-col">
    <div><div class="panel-pane pane-node"  >
  
        <h2 class="pane-title">
      DISSOLUTION OF PARLIAMENT    </h2>
    
  
  <div class="pane-content">
    
<div class="panelizer-view-mode node node-teaser node-page node-96">
        <div class="panel-display panel-1col clearfix" >
  <div class="panel-panel panel-col">
    <div><div class="panel-pane pane-entity-field pane-node-body"  >
  
      
  
  <div class="pane-content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p>I’m not currently an MP, as Parliament has been dissolved until after the General Election on 12th December 2019. This website will not be updated during the election campaign and is for reference of my work when I was a Member of Parliament.</p>
<p>To contact me during the campaign please visit my <a href="https://www.facebook.com/iainduncansmithmp/">Facebook page</a> or call my campaign office at 0208 5244344 or email <a href="mailto:IDS@cwgca.org">IDS@cwgca.org</a>.</p>
</div>  </div>

  
  </div>
</div>
  </div>
</div>
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-node"  id="links" >
  
        <h2 class="pane-title">
      Local Surveys    </h2>
    
  
  <div class="pane-content">
    <div id="node-309" class="node node-page view-mode-full" about="/content/local-surveys" typeof="foaf:Document">

              <h2 class="node-title">
        <a href="/content/local-surveys"></a></h2>
        <span property="dc:title" content="" class="rdf-meta element-hidden"></span>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p><a href="/car_crime">Car Crime Survey</a></p>
</div>
  
    
</div>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-node"  id="links" >
  
        <h2 class="pane-title">
      Links    </h2>
    
  
  <div class="pane-content">
    <div id="node-13" class="node node-page view-mode-full" about="/content/links" typeof="foaf:Document">

              <h2 class="node-title">
        <a href="/content/links"></a></h2>
        <span property="dc:title" content="" class="rdf-meta element-hidden"></span>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p><a href="http://conservatives.com" target="_blank">Conservatives</a></p>
<p>	<a href="http://www.cwgca.org/" target="_blank">Chingford &amp; Woodford Green Conservatives</a></p>
<p>	<a href="http://www.gov.uk" target="_blank">GOV.UK</a></p>
<p>	<a href="http://parliament.uk" target="_blank">Parliament</a></p>
</div>
  
    
</div>
  </div>

  
  </div>
</div>
  </div>
</div>
  </div>

  
  </div>
      </div>
    </div>
  </div>

    
            </div>
      </div></div> <!-- /#squeeze, /#center -->
      <div id="footer">
       <div class="squeeze">
           <section class="block block-panels-mini block-panels-mini-footer">
  
      
    <div id="mini-panel-footer">  
  <div class="row panel-2col">
    <div class="large-9 medium-8 small-12 columns col-first">
      <div class="panel-pane pane-node"  >
  
        <h2 class="pane-title">
      The Rt Hon Iain Duncan Smith    </h2>
    
  
  <div class="pane-content">
    <div id="node-17" class="node node-page view-mode-full" about="/content/rt-hon-iain-duncan-smith" typeof="foaf:Document">

              <h2 class="node-title">
        <a href="/content/rt-hon-iain-duncan-smith"></a></h2>
        <span property="dc:title" content="" class="rdf-meta element-hidden"></span>
  
  <div class="field field-name-body field-type-text-with-summary field-label-hidden field-wrapper body field"><p>Copyright © 2019. All rights reserved. <a href="/privacy">Privacy Notice</a>. House of Commons Chamber photos and videos courtesy of <a href="http://parliamentlive.tv/">parliamentlive.tv</a>. This website is funded from Parliamentary allowances.<br /><a href="https://www.iainduncansmith.org.uk/sites/www.iainduncansmith.org.uk/files/attachments/Business_Expenses_Letter_19012015.pdf" target="_blank">Publication of Business Costs and Expenses 12th May 2016, click here</a><br />
	 </p>
</div>
  
    
</div>
  </div>

  
  </div>
    </div>
    <div class="large-3 medium-4 small-12 columns col-last">
      <div class="col-last-wrapper">
              </div>
    </div>
  </div>

  </div>  
  </section>      </div></div> <!-- /.squeeze /#footer -->

    </div> <!-- /#container -->
  </div> <!-- /#wrapper -->
  <script>
    jQuery(document).ready(function($) {
    cookieControl({
        introText: '<p>This site uses cookies to store information on your computer.</p>',
        fullText: '<p>Some cookies on this site are essential, and the site won\'t work as expected without them. These cookies are set when you submit a form, login or interact with the site by doing something that goes beyond clicking on simple links.</p><p>We also use some non-essential cookies to anonymously track visitors or enhance your experience of the site. If you\'re not happy with this, we won\'t set these cookies but some nice features of the site may be unavailable.</p>',
        theme: 'dark',
        html: '<div id="cccwr"><div id="ccc-state" class="ccc-pause"><div id="ccc-icon"><button><span>Cookie Control</span></button></div><div class="ccc-widget"><div class="ccc-outer"><div class="ccc-inner"><h2>Cookie Control</h2><div class="ccc-content"><p class="ccc-intro"></p><div class="ccc-expanded"></div><div id="ccc-cookies-switch" style="background-position-x: 0;"><a id="cctoggle" href="#" style="background-position-x: 0;" name="cctoggle"><span id="cctoggle-text">Cookies test</span></a></div><div id="ccc-implicit-warning">(One cookie will be set to store your preference)</div><div id="ccc-explicit-checkbox"><label><input id="cchide-popup" type="checkbox" name="ccc-hide-popup" value="Y" /> Do not ask me again<br /></label> (Ticking this sets a cookie to hide this popup if you then hit close. This will not store any personal information)</div><p class="ccc-about"><small><a href="http://www.civicuk.com/cookie-law" target="_blank">About this tool</a></small></p><a class="ccc-icon" href="http://www.civicuk.com/cookie-law" target="_blank"title="About Cookie Control">About Cookie Control</a><button class="ccc-close">Close</button></div></div></div><button class="ccc-expand">read more</button></div></div></div>',
        position: 'left',
        shape: 'triangle',
        startOpen: true,
        autoHide: 60000,
        onAccept: function(cc){cookiecontrol_accepted(cc)},
        onReady: function(){},
        onCookiesAllowed: function(cc){cookiecontrol_cookiesallowed(cc)},
        onCookiesNotAllowed: function(cc){cookiecontrol_cookiesnotallowed(cc)},
        countries: '',
        subdomains: true,
        cookieName: 'iain-duncan-smith_cookiecontrol',
        iconStatusCookieName: 'ccShowCookieIcon',
        consentModel: 'information_only'
        });
      });
    </script>
<script>function cookiecontrol_accepted(cc) {

}</script>
<script>function cookiecontrol_cookiesallowed(cc) {

}</script>
<script>function cookiecontrol_cookiesnotallowed(cc) {

}</script>
    <script>
    (function ($, Drupal, window, document, undefined) {
      $(document).foundation();
    })(jQuery, Drupal, this, this.document);
  </script>
</body>
</html>
