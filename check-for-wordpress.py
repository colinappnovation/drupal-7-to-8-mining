#!/usr/bin/env python3

import os
import mmap
from bs4 import BeautifulSoup
import re

# access the directory and search string definition
directory = os.listdir("homepages")
searchstring = "drupal"

# list for later
found = []

for fname in directory:
    if os.path.isfile('./homepages' + os.sep + fname):
        try:
            with open('./homepages' + os.sep + fname, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                if s.find(b'wp-content') != -1:
                    found.append(fname)                
                                    
        except ValueError:
           print(f'Failed processing: {fname} due to zero byte file')
     

# sort alpha
found.sort()

# print number found
print(f"Found Drupal in {len(found)} files out of {len(directory)}")

with open('wordpress-matches.txt', 'w') as f:
    print(found, file=f)
