#!/usr/bin/env python3

import urllib.request as req
import urllib
import http
import sys
import pathlib as p

def getHomepage(hp):
    try:
        # Check for homepage existing
        file = p.Path(f"homepages-2/{hp}")
        if file.exists():
            print(f"Skipping processing: {hp} ...")
        else:
            # Grab the homepage
            req.urlretrieve(f"https://www.{hp}", f"homepages-2/{hp}")
            print(f"Processed: {hp}")
    #except urllib.error.HTTPError as err:
    #    print(f"Cannot process: {hp} with error: {err}")
    except urllib.error.URLError as err:        
        print(err.args)
   # except http.client.RemoteDisconnected as err:
    #    print(f"Cannot process: {hp} with error: {err}")


getHomepage(sys.argv[1])
